/**
 * @author pkandasa
 */
import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.FiniteDuration
import play.api.Application
import play.api.GlobalSettings
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.mvc.WithFilters
import play.filters.gzip.GzipFilter
import play.filters.headers.SecurityHeadersFilter
import akka.actor.Props
import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.libs.json._
import play.filters.gzip.GzipFilter
import play.filters.headers.SecurityHeadersFilter
import scala.concurrent.duration._
import play.api.Play.current
import play.api.libs.concurrent.Akka
import akka.actor.Props
import scala.concurrent.duration._
import java.util.Calendar
import akka.dispatch.OnComplete
import org.jets3t.service.CloudFrontService
import scala.concurrent.Future
import models.com.hcue.doctors.helper.CalenderDateConvertor
import controllers.doctors.API.Doctor.HospitalAppointmentAPI
import controllers.API.scheduler.schedule.SonographerSchedulerAPI
import models.com.hcue.doctors.helper.hCueCalenderHelper

object Global extends WithFilters(new GzipFilter(), SecurityHeadersFilter(), CorsAction) with GlobalSettings {

  override def onStart(app: Application) {
    Logger.info("-------------------Application has started-------------------")
    new GzipFilter(shouldGzip = (request, response) =>
      response.headers.get("Content-Type").exists(_.startsWith("application/json")))
    
    println("application started ----------")
     
   val actor = Akka.system.actorOf(Props(classOf[SonographerSchedulerAPI.sonographerSchedulerActor], "appointmentDetailScheduler"))
   val actor2 = Akka.system.actorOf(Props(classOf[SonographerSchedulerAPI.sonographerSchedulerActor], "cswappointmentDetailScheduler"))
   val actor3 = Akka.system.actorOf(Props(classOf[SonographerSchedulerAPI.sonographerSchedulerActor], "appointmentDetailscancenterScheduler"))
   val actor4 = Akka.system.actorOf(Props(classOf[SonographerSchedulerAPI.sonographerSchedulerActor], "appointmentSmsRemainder"))
   
    val system = akka.actor.ActorSystem("system")
    import system.dispatcher

    Logger.info("::::::::::::::::::Calling Schedular::::::::::::::::::::")

    var initialDelay: FiniteDuration = 0.seconds //default time
    val interval: FiniteDuration = 24 hour //scheduler should run for every 24 hours

    //val now = CalenderDateConvertor.getISDTimeZoneDate()
    val now = hCueCalenderHelper.getUKTimeZoneDate()
    val hour: Int = now.get(Calendar.HOUR_OF_DAY);
    val min: Int = now.get(Calendar.MINUTE)


    //Setting the inital time to start the scheduled 
    if (hour < 6) {
      println("inside initial delay calculation")
      initialDelay = ((6 - hour) * 60 * 60 - min * 60).seconds
    } else {
      println("inside initial delay calculation - else part")
      initialDelay = (((24 - hour) * 60 * 60 - min * 60) + 6 * 60 * 60).seconds
    }

    var initialDelay2: FiniteDuration = 0.seconds //default time

    //Setting the inital time to start the scheduled
    // modified schedule time to 18 hrs each day 
    if (hour < 18) {
      initialDelay2 = ((18 - hour) * 60 * 60 - min * 60).seconds
    } else {
      initialDelay2 = (((24 - hour) * 60 * 60 - min * 60) + 18 * 60 * 60).seconds
    }
    
        var initialDelay3: FiniteDuration = 0.seconds //default time

    //Setting the inital time to start the scheduled 
    if (hour < 17) {
      initialDelay3 = ((17 - hour) * 60 * 60 - min * 60).seconds
    } else {
      initialDelay3 = (((24 - hour) * 60 * 60 - min * 60) + 17 * 60 * 60).seconds
    }

    Logger.info("::Current hour:minunts::" + hour + ":" + min)
    Logger.info("::initialDelay::" + initialDelay)
    Logger.info("::interval::" + interval)
    
    println("::Current hour:minunts::" + hour + ":" + min)
    println("::initialDelay::" + initialDelay)
    println("::initialDelay 2::" + initialDelay2)
    println("::initialDelay 3::" + initialDelay3)
    println("::interval::" + interval)
    
   //system.scheduler.schedule(5 second, interval, actor, "updateProfileSignedURL") 
   //temporary disable emails 
   system.scheduler.schedule(initialDelay, interval, actor, "appointmentDetailScheduler")
   system.scheduler.schedule(initialDelay2, interval, actor2, "cswappointmentDetailScheduler")
   system.scheduler.schedule(initialDelay3, interval, actor3, "appointmentDetailscancenterScheduler")
   
  // -- val places = Geocoding.encodeAddress("17 rue Lanoue Bras de Fer, Nantes")


  }

  override def onStop(app: Application) {
    Logger.info("Application shutdown...")
  }

  // called when a route is found, but it was not possible to bind the request parameters
  override def onBadRequest(request: RequestHeader, error: String) = {
    Future.successful(BadRequest("Bad Request: " + error))
  }

  // 500 - internal server error
  override def onError(request: RequestHeader, throwable: Throwable) = {
    Future.successful(InternalServerError("Internal Server Error: " + throwable.getMessage))
  }

  // 404 - page not found error
  override def onHandlerNotFound(request: RequestHeader) = {
    Future.successful(NotFound("Requested Service Path " + request.path + "Not Found"))
  }

}