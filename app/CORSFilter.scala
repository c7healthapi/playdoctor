import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.jets3t.service.CloudFrontService

import controllers.Default
import play.Logger
import play.api.mvc.Filter
import play.api.mvc.RequestHeader
import play.api.mvc.Result
import controllers.doctors.security.authentication.HcueSecurityConstants
import controllers.doctors.security.authentication.JWTAuthentication
import models.com.hcue.doctors.helper.hCueCalenderHelper
import java.util.Calendar

object CorsAction extends Filter {

  lazy val allowedDomain = play.api.Play.current.configuration.getString("cors.allowed.domain")
  def isPreFlight(r: RequestHeader) = (
    r.method.toLowerCase.equals("options")
    &&
    r.headers.get("Access-Control-Request-Method").nonEmpty)

  def apply(nextFilter: (RequestHeader) => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    
    var result = HcueSecurityConstants.STR_RESULT_SUCCESS

    if (!requestHeader.path.contains(HcueSecurityConstants.STR_SKIP_REQUEST_PATH)) {
      val accessToken = requestHeader.headers.get("X-Access-Token")
      val invalidResult = Future.successful(Default.Ok.withHeaders("WWW-Authenticate" -> """Basic realm="Secured""""))
      if (accessToken != None) {
         println("come to cross filter accesstoken if")
        result = JWTAuthentication.validateToken(accessToken.get)
      } /*else {
        println("come to cross filter accesstoken else")
        result = HcueSecurityConstants.STR_RESULT_AUTHENTICATION_FAILED
      }*/
    }

    if (!result.equalsIgnoreCase(HcueSecurityConstants.STR_RESULT_SUCCESS)) {
      return Future.successful(Default.Ok(result))
    }


    if (isPreFlight(requestHeader)) {
      Future.successful(Default.Ok.withHeaders(
        "Access-Control-Allow-Origin" -> allowedDomain.orElse(requestHeader.headers.get("Origin")).getOrElse(""),
        "Access-Control-Allow-Methods" -> requestHeader.headers.get("Access-Control-Request-Method").getOrElse("*"),
        "Access-Control-Allow-Headers" -> requestHeader.headers.get("Access-Control-Request-Headers").getOrElse(""),
        "Access-Control-Allow-Credentials" -> "true"))

    } else {
      nextFilter(requestHeader).map { x =>
        (x.withHeaders(
          "Access-Control-Allow-Origin" -> allowedDomain.orElse(requestHeader.headers.get("Origin")).getOrElse(""),
          "Access-Control-Allow-Methods" -> requestHeader.headers.get("Access-Control-Request-Method").getOrElse("*"),
          "Access-Control-Allow-Headers" -> requestHeader.headers.get("Access-Control-Request-Headers").getOrElse(""),
          "Access-Control-Allow-Credentials" -> "true"))
      }

    }
  }
}