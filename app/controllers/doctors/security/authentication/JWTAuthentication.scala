package controllers.doctors.security.authentication

import java.text.SimpleDateFormat

import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.optionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.productQueryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.stringColumnType
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import authentikat.jwt.JsonWebToken
import authentikat.jwt.JwtClaimsSet
import authentikat.jwt.JwtHeader
import models.com.hcue.doctors.slick.transactTable.doctor.login.AccessToken
import models.com.hcue.doctors.slick.transactTable.doctor.login.DoctorLogin
import models.com.hcue.doctors.traits.dbProperties.db

object JWTAuthentication {

  val doctorLogin = DoctorLogin.DoctorLogin
  val accessToken = AccessToken.AccessToken

  var dateFormat = DateTimeFormat.forPattern(HcueSecurityConstants.DATE_TIME_FORMAT);
  //val algo = JwtAlgorithm.HS256
  val header = JwtHeader("HS256")

  def getToken(username: String, patientloginid: String): String = {

    var currentTime = new DateTime

    var strDate: String = dateFormat.print(currentTime);

    val claimsSet = JwtClaimsSet(Map("name" -> username, "patientloginid" -> patientloginid, "time" -> strDate, "appsource" -> "PATIENT"))

    return getEncodedToken(claimsSet)
  }

  def getEncodedToken(payload: JwtClaimsSet): String = {
    //val encodedToken = JWT.encode(HcueSecurityConstants.SECRET_KEY, payload)
    //val token = JwtJson.encode(payload, HcueSecurityConstants.SECRET_KEY, algo)
    val token: String = JsonWebToken(header, payload, HcueSecurityConstants.SECRET_KEY)

    return token
  }

  def isTokenValid(token: String): Boolean = {
    val isValid = JsonWebToken.validate(token, HcueSecurityConstants.SECRET_KEY)
    return isValid
  }

  def isTokenValidFromDb(loginId: String, token: String): Boolean = db withSession { implicit session =>
    var isValid = false

    val docId = doctorLogin.filter { x => x.DoctorLoginID === loginId }.map { x => x.DoctorID }.firstOption

    val accessTokn = accessToken.filter { x => x.UserID === docId }.map { x => x.Token }.firstOption.getOrElse("")

    if (accessTokn != "") {
      if (accessTokn.equals(token)) {
        isValid = true
      }
    }

    return isValid
  }

  def updateTime(loginId: String) = db withSession { implicit session =>

    val docId = doctorLogin.filter { x => x.DoctorLoginID === loginId }.map { x => x.DoctorID }.firstOption

    val expiryTime = accessToken.filter { x => x.UserID === docId }.map { x => x.TTL }.firstOption.getOrElse("")

    println("Expiry Time   " + expiryTime)

    import java.text.SimpleDateFormat

    val inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    import java.util.Calendar
    var currentTime = new DateTime
    val now = Calendar.getInstance().getTime()

    val myTime = "14:10";
    val df = new SimpleDateFormat("HH:mm");
    val d = df.parse(myTime);
    val cal = Calendar.getInstance();
    cal.setTime(d);
    cal.add(Calendar.MINUTE, 20);
    val newTime = df.format(cal.getTime());

    println("CurrentTime " + newTime)
    println("Twenty <Min Time " + currentTime.plusMinutes(20))
    accessToken.filter { x => x.UserID === docId }.map { x => (x.TTL) }.update(currentTime.plusMinutes(20).toString())

    //  val expiryDateTime = DateTimeFormat.forPattern(expiryTime)

    //  println("ExpiryDateTime  "+expiryDateTime)

    val expiryMinutes = HcueSecurityConstants.TOKEN_VALIDITY_MINS

  }

  def validateToken(jwttoken: String): String = {
    println("come to validateToken method")
    /*  if (!isTokenValid(jwttoken)) {
      return HcueSecurityConstants.ERR_INVALID_TOKEN
    }
*/

    val claims: Option[Map[String, String]] = jwttoken match {
      case JsonWebToken(header, claimsSet, signature) =>
        claimsSet.asSimpleMap.toOption
      case x =>
        None
    }

    var currentTime = new DateTime

    if (claims != None) {
      var patientLoginid = claims.get("patientloginid").toString()

      if (!isTokenValidFromDb(patientLoginid, jwttoken)) {
        return HcueSecurityConstants.ERR_INVALID_TOKEN
      }

      updateTime(patientLoginid)
      var strTokenGenTime = claims.get("time").toString()

      val tokenGenTime: DateTime = dateFormat.parseDateTime(strTokenGenTime);

      val expiryMinutes = HcueSecurityConstants.TOKEN_VALIDITY_MINS

      val tokenValdity: DateTime = tokenGenTime.plusMinutes(expiryMinutes.toString().toInt)
      if (currentTime.getMillis() > tokenValdity.getMillis()) {
        return HcueSecurityConstants.STR_RESULT_TOKEN_EXPIRED
      } else {
        return HcueSecurityConstants.STR_RESULT_SUCCESS
      }
    } else {
      return HcueSecurityConstants.STR_RESULT_AUTHENTICATION_FAILED
    }
  }
}
