package controllers.doctors.security.authentication

object HcueSecurityConstants {

  final val SECRET_KEY: String = "123ugo-free"

  final val DATE_TIME_FORMAT: String = "yyyy-MM-dd HH:mm:ss"

  final val TOKEN_VALIDITY_HOURS: Long = 12

  final val TOKEN_VALIDITY_MINS: Long = 10

  final val STR_RESULT_SUCCESS: String = "SUCCESS"

  final val STR_RESULT_TOKEN_EXPIRED: String = "TOKEN_EXPIRED"

  final val STR_RESULT_AUTHENTICATION_FAILED: String = "AUTHENTICATION_FAILED"

  final val STR_SKIP_REQUEST_PATH: String = "/doctors/validate/doctorLogin"

  final val ARR_SKIP_REQUEST_PATH: Array[String] = Array("/patients/validate/patientLogin", "/patients/verifyPatientLogin")

  final val ERR_INVALID_TOKEN: String = "INVALID_TOKEN"

}