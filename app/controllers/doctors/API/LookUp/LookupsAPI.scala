package controllers.doctors.API.LookUp

import play.api.mvc.{ Action, Controller }
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.Form
import play.api.data.Forms.{ mapping, text, of }
import play.api.data.format.Formats.doubleFormat
import play.api.data.validation.Constraints
import play.api.libs.json.JsError
import java.sql.Date
import models.com.hcue.doctors.slick.lookupTable.TAddressTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TContactTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TCountryLkup
import models.com.hcue.doctors.slick.lookupTable.TDayLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorQualfLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorVisitRsnLkyp
import models.com.hcue.doctors.slick.lookupTable.TFamilyRltnLkup
import models.com.hcue.doctors.slick.lookupTable.TOtherIDTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TPhoneTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TStateLkup
import models.com.hcue.doctors.slick.lookupTable.TUSRTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TAppointmentStatusLkup
import models.com.hcue.doctors.slick.lookupTable.TVisitorTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TCityLkup
import models.com.hcue.doctors.slick.lookupTable.TLocationLkup
import models.com.hcue.doctors.slick.lookupTable.TCurrencyLkup
import models.com.hcue.doctors.Json.Request.getLkupObj
import models.com.hcue.doctors.Json.Request.getDoctorVisitRsnLkupObj
import models.com.hcue.doctors.Json.Request.allergensInfoObj
import models.com.hcue.doctors.traits.lookup.LookupDAO
import models.com.hcue.doctors.Json.Response.DoctorSpecialityTypeObj
import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.readlkupreq

import scala.slick.jdbc.{ GetResult, StaticQuery }
import StaticQuery.interpolation
import models.com.hcue.doctors.Json.Request.specialitylkupreq

object LookupsAPI extends Controller {

  implicit val readsCreateAddressType = (
    (__ \ "AddressTypeID").read[String] and
    (__ \ "AddressTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TAddressTypeLkup.apply _)

  implicit val writesAddressType = Json.writes[TAddressTypeLkup]

  implicit val readsCreateContactType = (
    (__ \ "ContactTypeID").read[String] and
    (__ \ "ContactTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TContactTypeLkup.apply _)

  implicit val writesContactType = Json.writes[TContactTypeLkup]

  implicit val readsCreateCountry = (
    (__ \ "CountryID").read[String] and
    (__ \ "CountryDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "CountryCode").readNullable[String] and
    (__ \ "isPrivate").read[Option[String]] and
    (__ \ "CrtUSR").read[Long] and
    (__ \ "CrtUSRType").read[String])(TCountryLkup.apply _)

  implicit val writesCountry = Json.writes[TCountryLkup]

  implicit val readsCreateDayLkup = (
    (__ \ "DayID").read[String] and
    (__ \ "DayDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TDayLkup.apply _)

  implicit val writesDay = Json.writes[TDayLkup]

  implicit val readsCreateDoctorQualfLkup = (
    (__ \ "DoctorQualfID").read[String] and
    (__ \ "DoctorQualfDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TDoctorQualfLkup.apply _)

  implicit val writesDoctorQualf = Json.writes[TDoctorQualfLkup]

  implicit val readsCreateDoctorSpecialityLkup = (
    (__ \ "DoctorSpecialityID").read[String] and
    (__ \ "DoctorSpecialityDesc").read[String] and
    (__ \ "SpecialityDesc").read[Option[String]] and
    (__ \ "ActiveIND").read[String])(TDoctorSpecialityLkup.apply _)

  implicit val writesDoctorSpeciality = Json.writes[TDoctorSpecialityLkup]
  implicit val getTDoctorSpecialityLkupResult = GetResult(r => DoctorSpecialityTypeObj(r.<<, r.<<, r.<<, r.<<, r.<<))

  implicit val readsCreateDoctorVisitRsnLkyp = (
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "DoctorSpecialityID").read[String] and
    (__ \ "DoctorVisitRsnDesc").read[String] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "SequenceRow").read[String] and
    (__ \ "ActiveIND").read[String])(TDoctorVisitRsnLkyp.apply _)

  implicit val writesDoctorVisitRsn = Json.writes[TDoctorVisitRsnLkyp]

  implicit val readsCreateFamilyRltnLkup = (
    (__ \ "FamilyRltnID").read[String] and
    (__ \ "FamilyRltnDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TFamilyRltnLkup.apply _)

  implicit val writesFamilyRltn = Json.writes[TFamilyRltnLkup]

  implicit val readsCreateOtherIDTypeLkup = (
    (__ \ "OtherIDTypeID").read[String] and
    (__ \ "OtherIDTypeID").read[String] and
    (__ \ "ActiveIND").read[String])(TOtherIDTypeLkup.apply _)

  implicit val writesOtherIDType = Json.writes[TOtherIDTypeLkup]

  implicit val readsCreatePhoneTypeLkup = (
    (__ \ "PhoneTypeID").read[String] and
    (__ \ "PhoneTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TPhoneTypeLkup.apply _)

  implicit val writesPhoneType = Json.writes[TPhoneTypeLkup]

  implicit val readsCreateStateLkup = (
    (__ \ "StateID").read[String] and
    (__ \ "StateDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "CountryID").read[String] and
    (__ \ "isPrivate").read[Option[String]] and
    (__ \ "CrtUSR").read[Long] and
    (__ \ "CrtUSRType").read[String])(TStateLkup.apply _)

  implicit val writesState = Json.writes[TStateLkup]

  implicit val readsCreateUSRTypeLkup = (
    (__ \ "USRTypeID").read[String] and
    (__ \ "USRTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TUSRTypeLkup.apply _)

  implicit val writesUSRType = Json.writes[TUSRTypeLkup]

  implicit val readsCreateAppointmentStatus = (
    (__ \ "AppointmentStatusID").read[String] and
    (__ \ "AppointmentStatusDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TAppointmentStatusLkup.apply _)

  implicit val writesAppointmentStatus = Json.writes[TAppointmentStatusLkup]

  implicit val readsCreateVisitorTypeLkup = (
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "VisitUserTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(TVisitorTypeLkup.apply _)

  implicit val writesVisitorType = Json.writes[TVisitorTypeLkup]

  implicit val readsCreateCityLkup = (
    (__ \ "CityID").read[String] and
    (__ \ "CityIDDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "StateID").read[String] and
    (__ \ "isPrivate").read[Option[String]] and
    (__ \ "CrtUSR").read[Long] and
    (__ \ "CrtUSRType").read[String])(TCityLkup.apply _)

  implicit val writesCityLkup = Json.writes[TCityLkup]

  implicit val readsCreateLocationLkup = (
    (__ \ "LocationID").read[String] and
    (__ \ "LocationIDDesc").read[String] and
    (__ \ "CityID").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "isPrivate").read[Option[String]] and
    (__ \ "CrtUSR").read[Long] and
    (__ \ "CrtUSRType").read[String])(TLocationLkup.apply _)

  implicit val writesLocationLkup = Json.writes[TLocationLkup]

  def readLookUp() = Action { implicit request =>

    try {
      val lookups = LookupDAO.loadLookupTables()
      Ok(Json.toJson(lookups))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def getreadLookUp() = Action(parse.json) { implicit request =>

    request.body.validate[readlkupreq] match {

      case JsSuccess(readlkupreq, _) =>
        try {
          val lookups = LookupDAO.loadLookupTables()
          Ok(Json.toJson(lookups))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, None)
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>

        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }
  def readVisitorTypeLkupUp(Active: String, visitorType: String) = Action { implicit request =>

    try {

      if (Active.equalsIgnoreCase("active")) {
        val lookups = LookupDAO.loadVisitorTypeLkupTables("Y", visitorType)
        Ok(Json.toJson(lookups))
      } else if (Active.equalsIgnoreCase("inactive")) {
        val lookups = LookupDAO.loadVisitorTypeLkupTables("N", visitorType)
        Ok(Json.toJson(lookups))
      } else {
        val lookups = LookupDAO.loadVisitorTypeLkupTables("", visitorType)
        Ok(Json.toJson(lookups))
      }

    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def readSpecialityTypeLkupUp(Active: String) = Action { implicit request =>
    try {
      if (Active.equalsIgnoreCase("active")) {
        val lookups = LookupDAO.loadDoctorSpecialityTypeTables("Y")
        Ok(Json.toJson(lookups))
      } else if (Active.equalsIgnoreCase("inactive")) {
        val lookups = LookupDAO.loadDoctorSpecialityTypeTables("N")
        Ok(Json.toJson(lookups))
      } else {
        val lookups = LookupDAO.loadDoctorSpecialityTypeTables("")
        Ok(Json.toJson(lookups))
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def getSpecialityLkupUp() = Action(parse.json) { implicit request =>

    request.body.validate[specialitylkupreq] match {

      case JsSuccess(getspecialitylkupreq, _) =>
        try {
          if (getspecialitylkupreq.SpecialityCD != None && getspecialitylkupreq.SpecialityCD.get.equalsIgnoreCase("ALL") && getspecialitylkupreq.ActiveIND.equalsIgnoreCase("Y")) {
            val lookups = LookupDAO.loadDoctorSpecialityTypeTables("Y")
            Ok(Json.toJson(lookups))
          } else if (getspecialitylkupreq.SearchText != None) {
            val lookups = LookupDAO.searchDoctorSpeciality(getspecialitylkupreq.SearchText.get)
            Ok(Json.toJson(lookups))
          } else {
            val lookups = "Invalid Request"
            Ok(Json.toJson(lookups))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, None)
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>

        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def readDoctorVisitRsnLkup = Action(parse.json) { implicit request =>

    request.body.validate[getDoctorVisitRsnLkupObj] match {
      case JsSuccess(getDoctorVisitRsnLkupObj, _) =>
        try {
          val doctorAppointment = LookupDAO.loadDoctorVisitRsnTypeTables(getDoctorVisitRsnLkupObj)
          Ok(Json.toJson(doctorAppointment))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def readCityLkUp(Active: String) = Action { implicit request =>

    try {
      if (Active.equalsIgnoreCase("active")) {

        val lookups = LookupDAO.loadCityLookup("Y")
        Ok(Json.toJson(lookups))
      } else if (Active.equalsIgnoreCase("inactive")) {
        val lookups = LookupDAO.loadCityLookup("N")
        Ok(Json.toJson(lookups))
      } else {
        val lookups = LookupDAO.loadCityLookup("")
        Ok(Json.toJson(lookups))
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def readCountryLkUp(Active: String) = Action { implicit request =>

    try {
      if (Active.equalsIgnoreCase("active")) {

        val lookups = LookupDAO.loadCountryLookup("Y")
        Ok(Json.toJson(lookups))
      } else if (Active.equalsIgnoreCase("inactive")) {
        val lookups = LookupDAO.loadCountryLookup("N")
        Ok(Json.toJson(lookups))
      } else {
        val lookups = LookupDAO.loadCountryLookup("")
        Ok(Json.toJson(lookups))
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }

  }

  def readStateLkUp(Active: String) = Action { implicit request =>

    try {
      if (Active.equalsIgnoreCase("active")) {

        val lookups = LookupDAO.loadStateLookup("Y")
        Ok(Json.toJson(lookups))
      } else if (Active.equalsIgnoreCase("inactive")) {
        val lookups = LookupDAO.loadStateLookup("N")
        Ok(Json.toJson(lookups))
      } else {
        val lookups = LookupDAO.loadStateLookup("")
        Ok(Json.toJson(lookups))
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def readLocationLkUp(Active: String) = Action { implicit request =>

    try {
      if (Active.equalsIgnoreCase("active")) {
        val lookups = LookupDAO.loadLocationLookup("Y")
        Ok(Json.toJson(lookups))
      } else if (Active.equalsIgnoreCase("inactive")) {
        val lookups = LookupDAO.loadLocationLookup("N")
        Ok(Json.toJson(lookups))
      } else {
        val lookups = LookupDAO.loadLocationLookup("")
        Ok(Json.toJson(lookups))
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }

  }

  def readCurrencyLkUp(Active: String) = Action { implicit request =>

    try {

      if (Active.equalsIgnoreCase("active")) {
        val lookups = LookupDAO.loadCurrencyLookup("Y")
        Ok(Json.toJson(lookups))
      } else if (Active.equalsIgnoreCase("inactive")) {
        val lookups = LookupDAO.loadCurrencyLookup("N")
        Ok(Json.toJson(lookups))
      } else {
        val lookups = LookupDAO.loadCurrencyLookup("")
        Ok(Json.toJson(lookups))
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }

  }

  def searchSpecialityDesc(searchValue: String) = Action { implicit request =>

    try {
      val doctorAppointment = LookupDAO.specialityDesc(searchValue)
      Ok(Json.toJson(doctorAppointment))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def doctorSearch() = Action { implicit request =>

    try {
      val doctorSearch = LookupDAO.doctorSearch("firstName", "LastName")
      Ok(Json.toJson(doctorSearch))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def getAdminLookup(HospitalId: Long) = Action { implicit request =>

    try {
      val adminLookup = LookupDAO.loadAdminLookup(HospitalId)
      Ok(Json.toJson(adminLookup))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }

  }

  def readLocationLkUps = Action(parse.json) { implicit request =>

    request.body.validate[getLkupObj] match {
      case JsSuccess(getLkupObj, _) =>
        try {
          val lookups = LookupDAO.loadLocationLookups(getLkupObj)
          Ok(Json.toJson(lookups))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def readStateLkUps = Action(parse.json) { implicit request =>

    request.body.validate[getLkupObj] match {
      case JsSuccess(getLkupObj, _) =>
        try {
          val lookups = LookupDAO.loadStateLookups(getLkupObj)
          Ok(Json.toJson(lookups))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))

            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def readCountryLkUps = Action(parse.json) { implicit request =>

    request.body.validate[getLkupObj] match {
      case JsSuccess(getLkupObj, _) =>
        try {
          val lookups = LookupDAO.loadCountryLookups(getLkupObj)
          Ok(Json.toJson(lookups))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def readCityLkUps = Action(parse.json) { implicit request =>

    request.body.validate[getLkupObj] match {
      case JsSuccess(getLkupObj, _) =>

        try {
          val lookups = LookupDAO.loadCityLookups(getLkupObj)
          Ok(Json.toJson(lookups))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

}  

