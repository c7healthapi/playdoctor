package controllers.doctors.API.LookUp

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.DashboardInfoInputTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.DashboardInfoTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.NewDashboardInfoInputTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.NewDashboardInfoTypeObj
import models.com.hcue.doctors.traits.dashboard.DashboardDAO
import play.Logger
import play.api.data.Forms.mapping
import play.api.data.Forms.of
import play.api.data.Forms.text
import play.api.data.format.Formats.doubleFormat
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.json.JsError
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.EnquiryDashboardInfoInputTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getDailyReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getDailySpecialityReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getCostReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getReferralReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getCostReportwithregionInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getReferralwithoutIndicationObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getclinicauditReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getAppointmentpracticeInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getCalenderlistviewInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getAgentstatsInfoObj
import models.com.hcue.doctors.traits.dashboard.CustomreportDAO
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getReferralReportInfoObjv1

object DashboardAPI extends Controller {

  implicit val pagesWrite1 = (
    (__ \ 'DashboardListing).write[NewDashboardInfoTypeObj] and
    (__ \ 'MedicineListing).writeNullable[JsValue]).tupled: Writes[(NewDashboardInfoTypeObj, Option[JsValue])]

  private val log = Logger.of("application")

  def getDashboardInfo = Action(parse.json) { implicit request =>
    request.body.validate[DashboardInfoInputTypeObj] match {
      case JsSuccess(dashboardInfoInputTypeObj, _) =>

        try {
          val dashBoardInfo = DashboardDAO.getDashboardInfo(dashboardInfoInputTypeObj.hospitalId, dashboardInfoInputTypeObj.doctorid, dashboardInfoInputTypeObj.addressid, dashboardInfoInputTypeObj.startDate, dashboardInfoInputTypeObj.endDate, dashboardInfoInputTypeObj.includeBranch)

          Ok(Json.toJson(dashBoardInfo))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def getNewDashboardInfo = Action(parse.json) { implicit request =>
    request.body.validate[NewDashboardInfoInputTypeObj] match {
      case JsSuccess(newDashboardIndfoReqObj, _) =>
        
    try {  
        val dashBoardInfo =DashboardDAO.getNewDashBoardInfo(newDashboardIndfoReqObj.hospitalId, newDashboardIndfoReqObj.doctorid, newDashboardIndfoReqObj.addressid, newDashboardIndfoReqObj.startDate, newDashboardIndfoReqObj.endDate, newDashboardIndfoReqObj.includeBranch,newDashboardIndfoReqObj.dashBoardType,newDashboardIndfoReqObj.InventoryType.getOrElse(""),newDashboardIndfoReqObj.ReferralID,newDashboardIndfoReqObj.SubReferralID,newDashboardIndfoReqObj.AppointmentVisit)
        Ok(Json.toJson(dashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }

  
  def getEnquiryDashboardInfo = Action(parse.json) { implicit request =>
    request.body.validate[EnquiryDashboardInfoInputTypeObj] match {
      case JsSuccess(enquiryDashboardInfoInputTypeObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getEnquiryNewDashBoardInfo(enquiryDashboardInfoInputTypeObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }

  
    def getDailyReportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getDailyReportInfoObj] match {
      case JsSuccess(getDailyReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getDailyReportInfo(getDailyReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
    
    
        def getDailySpecialityReportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getDailySpecialityReportInfoObj] match {
      case JsSuccess(getDailySpecialityReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getDailySpecialityReportInfo(getDailySpecialityReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
        
        
   def getCostReportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCostReportInfoObj] match {
      case JsSuccess(getCostReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getCostReportInfo(getCostReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
   
   
      def getForecastCostReportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCostReportInfoObj] match {
      case JsSuccess(getCostReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getForecastCostReportInfo(getCostReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
   
 
     
     def getCostReportwithregionInfo = Action(parse.json) { implicit request =>

    request.body.validate[getCostReportwithregionInfoObj] match {
      case JsSuccess(getCostReportwithregionInfoObj, _) =>

        try {

             val CostReportwithregion = DashboardDAO.getCostReportwithregion(getCostReportwithregionInfoObj)
             Ok(Json.toJson(CostReportwithregion))  
          
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }
     
   
      def getReferralwithoutindication = Action(parse.json) { implicit request =>

    request.body.validate[getReferralwithoutIndicationObj] match {
      case JsSuccess(getCostReportwithregionInfoObj, _) =>

        try {

             val CostReportwithregion = DashboardDAO.getReferral(getCostReportwithregionInfoObj)
             Ok(Json.toJson(CostReportwithregion))  
          
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }
      
      def getGeneralreport = Action(parse.json) { implicit request =>

    request.body.validate[getReferralwithoutIndicationObj] match {
      case JsSuccess(getReferralwithoutIndicationObj, _) =>

        try {

             val Generealreport = DashboardDAO.getGeneralreport(getReferralwithoutIndicationObj)
             Ok(Json.toJson(Generealreport))  
          
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }
      
    def getGeneralreportfile = Action(parse.json) { implicit request =>

    request.body.validate[getReferralwithoutIndicationObj] match {
      case JsSuccess(getReferralwithoutIndicationObj, _) =>

        try {

             val Generealreport = DashboardDAO.getGeneralreportfile(getReferralwithoutIndicationObj)
             //val Generealreport = DashboardDAO.excelfilegeneratewithformula(getReferralwithoutIndicationObj)
             Ok(Json.toJson(Generealreport))  
         
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def getGeneralreportfileList(date : String) = Action { implicit request =>
    try {
      val Generealreport1 = DashboardDAO.getGeneralreportfileList(date)
      Ok(Json.toJson(Generealreport1))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }
  
   
   
   def getReferralReportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getReferralReportInfoObj] match {
      case JsSuccess(getReferralReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getReferralReportInfo(getReferralReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
   
   
   def getAppointmentPracticereport = Action(parse.json) { implicit request =>
    request.body.validate[getAppointmentpracticeInfoObj] match {
      case JsSuccess(getAppointmentpracticeInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getAppointmentPracticereportInfo(getAppointmentpracticeInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
   
   
   
      def getAppointmentPracticeindicationreport = Action(parse.json) { implicit request =>
    request.body.validate[getReferralReportInfoObj] match {
      case JsSuccess(getReferralReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getAppointmentPracticeindicationreportInfo(getReferralReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
   
   
      def getPatientinSystemreport = Action(parse.json) { implicit request =>
    request.body.validate[getReferralReportInfoObj] match {
      case JsSuccess(getReferralReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getPatientinSystemReportInfo(getReferralReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
           def getCalendarlistviewInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCalenderlistviewInfoObj] match {
      case JsSuccess(getCalenderlistviewInfoObj, _) =>
        
    try {  
        val CalendarlistviewInfo =DashboardDAO.getCalendarlistview(getCalenderlistviewInfoObj)
        Ok(Json.toJson(CalendarlistviewInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
           
            def getCustombookingreportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCostReportwithregionInfoObj] match {
      case JsSuccess(getCostReportwithregionInfoObj, _) =>
        
    try {  
        val CustombookingreportInfo =CustomreportDAO.getCustombookingreportview(getCostReportwithregionInfoObj)
        Ok(Json.toJson(CustombookingreportInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
            
            
            def getCustomadminreportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCostReportwithregionInfoObj] match {
      case JsSuccess(getCostReportwithregionInfoObj, _) =>
        
    try {  
        val CustomadminreportInfo =CustomreportDAO.getCustomadminreportview(getCostReportwithregionInfoObj)
        Ok(Json.toJson(CustomadminreportInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
     
           
           def getAgentstatsreportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getAgentstatsInfoObj] match {
      case JsSuccess(getAgentstatsInfoObj, _) =>
        
    try {  
        val AgentstatsreportInfo =DashboardDAO.getAgentstatsreport(getAgentstatsInfoObj)
        Ok(Json.toJson(AgentstatsreportInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
           
            def getExceptionnotesreportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCostReportwithregionInfoObj] match {
      case JsSuccess(getCostReportwithregionInfoObj, _) =>
        
    try {  
        val AgentstatsreportInfo =DashboardDAO.getExceptionnotesreport(getCostReportwithregionInfoObj)
        Ok(Json.toJson(AgentstatsreportInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
           
           
            def getallPracticesreportInfo = Action(parse.json) { implicit request =>
    request.body.validate[getAgentstatsInfoObj] match {
      case JsSuccess(getAgentstatsInfoObj, _) =>
        
    try {  
        val PracticesreportInfo =DashboardDAO.getallPracticereport(getAgentstatsInfoObj)
        Ok(Json.toJson(PracticesreportInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
      
           
                     def getAgentstatsreporttotalInfo = Action(parse.json) { implicit request =>
    request.body.validate[getAgentstatsInfoObj] match {
      case JsSuccess(getAgentstatsInfoObj, _) =>
        
    try {  
        val AgentstatsreporttotalInfo =DashboardDAO.getAgentstatsreporttotal(getAgentstatsInfoObj)
        Ok(Json.toJson(AgentstatsreporttotalInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
      
      
      
      
      def getPatientinSystemreportnew = Action(parse.json) { implicit request =>
    request.body.validate[getReferralReportInfoObj] match {
      case JsSuccess(getReferralReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getPatientinSystemReportInfonew(getReferralReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
     
      
      def getPatientinSystemreportnewv1 = Action(parse.json) { implicit request =>
    request.body.validate[getReferralReportInfoObjv1] match {
      case JsSuccess(getReferralReportInfoObjv1, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getPatientinSystemReportInfonewv1(getReferralReportInfoObjv1)
        print("ssd")
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
      
       def getPatientinSystemreporttotal = Action(parse.json) { implicit request =>
    request.body.validate[getReferralReportInfoObjv1] match {
      case JsSuccess(getReferralReportInfoObjv1, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getPatientinSystemReporttotal(getReferralReportInfoObjv1)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
      
      
      
       def getClinicauditcriteriareport = Action(parse.json) { implicit request =>
    request.body.validate[getclinicauditReportInfoObj] match {
      case JsSuccess(getclinicauditReportInfoObj, _) =>
        
    try {  
        val enquirydashBoardInfo =DashboardDAO.getClinicauditcriteriaReportInfo(getclinicauditReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
       
           def getCostanalyticsreport = Action(parse.json) { implicit request =>
    request.body.validate[getCostReportInfoObj] match {
      case JsSuccess(getCostReportInfoObj, _) =>
        
    try { 
      println("getCostanalyticsreport   ----------- ")
        val enquirydashBoardInfo =DashboardDAO.getCostanalyticsreportInfo(getCostReportInfoObj)
        Ok(Json.toJson(enquirydashBoardInfo))
      
    } catch {
             case e: Exception =>
             e.printStackTrace()
             val status = ErrorMessageBean.onError(request,e,Some(Json.toJson(request.body)))
             Ok(Json.toJson(status))
        }
       case JsError(errors) => 
          val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
          BadRequest(JsError.toFlatJson(errors))
    
    }
  }
   
  
}