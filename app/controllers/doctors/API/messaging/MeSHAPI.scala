package controllers.doctors.API.messageing

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.DAO.messaging.MeSHIntegrationBean
import models.com.hcue.doctors.Json.Request.messaging.postMeshMessageObj
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object MeSHAPI extends Controller {

  def send2mesh = Action(parse.json) { implicit request =>

    request.body.validate[postMeshMessageObj] match {
      case JsSuccess(postMeshMessageObj, _) =>

        try {

          val status = MeSHIntegrationBean.Initialized(postMeshMessageObj.hl7ReferenceID, "R7QOT003", postMeshMessageObj.toMailBoxID,
            postMeshMessageObj.usrid, postMeshMessageObj.usrtype)
          Ok(Json.toJson(status))

        } catch {

          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

}