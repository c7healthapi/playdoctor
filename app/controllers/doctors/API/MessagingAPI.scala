package controllers.doctors.API.messaging

import scala.concurrent.Future
import com.hcue.hl7.xml.parser.Message
import com.hcue.hl7.xml.parser.xmlToJson
import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.DAO.messaging.messagingDAO
import play.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctors.Json.Request.messaging.messagelogobject
import play.api.data.Forms.mapping
import play.api.data.Forms.of
import play.api.data.Forms.text
import play.api.data.format.Formats.doubleFormat
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.json.JsError
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctors.Json.Request.messaging.getHL7Obj



object MessagingAPI extends Controller {

  def sayHello(id: Long) = Action { implicit request =>
    try {
      throw new Exception()
      Ok("Hello ::")
    } catch {
      case e: Exception =>
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def processXML = Action { implicit request =>
    try {
      val xmlStr: String = request.body.asXml.toString();
      val xmltojson: xmlToJson = new xmlToJson();
      val message: Message = xmltojson.doConvertToObject(xmlStr);
      val addedRecord: Long = messagingDAO.addDiagnosticsResponse(message)
      println("addedRecord::" + addedRecord)
      if (addedRecord > 0) {
        messagingDAO.updateAppoointmentStatus(addedRecord)
        val generatepdfcontext: Future[String] = scala.concurrent.Future {
          //messagingDAO.generateDiagnsoticsReport(addedRecord)
          messagingDAO.generateDiagnsoticsReportV1(addedRecord)
        }
        Ok("Record Added Successfully")
      } else {
        Ok("Failure- Please check request")
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        println()
        val status = ErrorMessageBean.onError(request, e, Some(Json.toJson("xml inputs")))
        Ok(Json.toJson(status))
    }
  }

  def processMessage = Action { implicit request =>
    val xmlStr: String = request.body.toString();
    println("xmlstr " + xmlStr)
    Ok("Hello " + xmlStr)
  }

  def sendHL7Message(appointmentid: Long) = Action { implicit request =>
    try {
      val response = messagingDAO.invokeHL7Webservice(appointmentid)
      Ok(Json.toJson(response))
    } catch {
      case e: Exception =>
        println("Exception occured")
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  /*def listHL7Messages() = Action { implicit request =>
    try {
      println("inside api")
      val response = messagingDAO.listHL7MessagesDAO()
      Ok(Json.toJson(response))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }*/

  def listHL7Messages = Action(parse.json) { implicit request =>
    request.body.validate[getHL7Obj] match {
      case JsSuccess(_getHL7Obj, _) =>

        try {
          val dashBoardInfo = messagingDAO.listHL7Messages(_getHL7Obj)

          Ok(Json.toJson(dashBoardInfo))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def addMessagingLog = Action(parse.json) { implicit request =>
    request.body.validate[messagelogobject] match {
      case JsSuccess(messagelogobjectObj, _) =>

        try {
          val dashBoardInfo = messagingDAO.updateMessagingLog(messagelogobjectObj)

          Ok(Json.toJson(dashBoardInfo))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }
}