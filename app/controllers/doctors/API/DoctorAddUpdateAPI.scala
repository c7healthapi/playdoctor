package controllers.doctors.API

import java.sql.Date
import java.util.Date
import scala.concurrent.Future
import org.postgresql.util.PSQLException
import models.com.hcue.doctors.Json.Request.AddUpdate.addDoctorObj
import models.com.hcue.doctors.Json.Request.AddUpdate.getDoctorInfoObj
import models.com.hcue.doctors.Json.Request.AddUpdate.getHospitalInfoObj
import models.com.hcue.doctors.Json.Request.AddUpdate.updateDoctorObj
import models.com.hcue.doctors.Json.Response.furtherCareDoctorObj
import models.com.hcue.doctors.Json.Response.getResponseObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn

import models.com.hcue.doctors.traits.add.doctor.AddDoctorDao
import models.com.hcue.doctors.traits.common.CommonDao
import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao
import models.com.hcue.doctors.traits.update.UpdateDoctorDao
import models.com.hcue.doctors.Bean.ErrorMessageBean
import play.Logger
import play.api.data.Forms.mapping
import play.api.data.Forms.of
import play.api.data.Forms.text
import play.api.data.format.Formats.doubleFormat
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.functional.syntax._
import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json._
import play.api.libs.json.JsError
import play.api.libs.json.JsValue
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctors.helper.HCueUtils
import scala.util.control.Breaks
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorEmailDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.updateDoctorLoginIDObj
import models.com.hcue.doctors.Json.Request.AddUpdate.updateDoctorLastLoginObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctor.Json.Request.Careteam.getPracticeObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctordeactiveObj

object DoctorAddUpdateAPI extends Controller {

  implicit val readsCreateDoctorExtn = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "Qualification").read[Option[Map[String, String]]] and
    (__ \ "About").read[Option[Map[String, String]]] and
    (__ \ "HcueScore").read[Option[Float]] and
    (__ \ "Awards").read[Option[Map[String, String]]] and
    (__ \ "Membership").read[Option[Map[String, String]]] and
    (__ \ "Services").read[Option[Map[String, String]]] and
    (__ \ "MemberID").read[Option[Map[String, String]]] and
    (__ \ "Prospect").read[Option[String]] and
    (__ \ "ReferredBy").read[Option[Long]] and
    (__ \ "ProfilImage").read[Option[String]] and
    (__ \ "ProfilImageExpDt").read[Option[java.sql.Date]] and
    (__ \ "CareerImages").read[Option[Map[String, String]]] and
    (__ \ "CareerDocuments").read[Option[JsValue]] and
    (__ \ "CareerImageExpDt").read[Option[java.sql.Date]] and
    (__ \ "DoctorSettings").read[Option[Map[String, String]]] and
    (__ \ "CrtUSR").read[Long] and
    (__ \ "CrtUSRType").read[String] and
    (__ \ "UpdtUSR").read[Option[Long]] and
    (__ \ "UpdtUSRType").read[Option[String]])(TDoctorExtn.apply _)

  implicit val pagesWrite = (
    (__ \ 'rows).write[Array[furtherCareDoctorObj]] and
    (__ \ 'count).write[Int]).tupled: Writes[(Array[furtherCareDoctorObj], Int)]
  implicit val PharmaPartnerObjReads = Json.reads[furtherCareDoctorObj]
  implicit val PharmaPartnerObjWrites = Json.writes[furtherCareDoctorObj]

  def completeResponse(id: Long) = Action { implicit request =>

    try {
      val fullDetails = ListDoctorDao.completeResponse(id, true, None, false)
      Ok(Json.toJson(fullDetails))

    } catch {

      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))

    }
  }

  def completeDoctorInfo = Action(parse.json) { implicit request =>

    request.body.validate[getDoctorInfoObj] match {
      case JsSuccess(getDoctorInfoObj, _) =>

        try {
          val doctorDetails = ListDoctorDao.completeResponse(getDoctorInfoObj.DoctorID, getDoctorInfoObj.profileImage.getOrElse(false), getDoctorInfoObj.AddressID, false)
          Ok(Json.toJson(doctorDetails))

        } catch {

          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def doctorlist = Action(parse.json) { implicit request =>

    request.body.validate[getPracticeObj] match {
      case JsSuccess(getDoctorInfoObj, _) =>
        try {
          val doctorLists = ListDoctorDao.getDoctorList(getDoctorInfoObj.hospitalcd.getOrElse(""), getDoctorInfoObj.pageNumber.getOrElse(0), getDoctorInfoObj.pageSize.getOrElse(20), getDoctorInfoObj.searchtext.getOrElse(""))
          Ok(Json.toJson(doctorLists))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = "Error"
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def createDoctor = Action(parse.json) { implicit request =>

    request.body.validate[addDoctorObj] match {
      case JsSuccess(addDoctorObj, _) =>

        try {

          var status = HCueUtils.StrConstSuccess
          var loginID = HCueUtils.StrConstSuccess
          //Validating Doctor License
          status = AddDoctorDao.validateDocLicense(addDoctorObj)

          //validateEmail
          if (status.equalsIgnoreCase(HCueUtils.StrConstSuccess)) {
            if (addDoctorObj.DoctorEmail != None) {
              addDoctorObj.DoctorEmail.foreach { x =>
                val doctorEmailDetails: Array[DoctorEmailDetailsObj] = x
                val loop = new Breaks;
                loop.breakable {
                  for (i <- 0 until doctorEmailDetails.length) {
                    if (HCueUtils.validateEmail(doctorEmailDetails(i).EmailID)) {
                      status = HCueUtils.StrConstSuccess
                    } else {
                      status = HCueUtils.StrConstInvalidEmail
                      loop.break()
                    }
                  }
                }
              }
            }
          }

          //Check for Doctor Login ID Exists or Not
          if (addDoctorObj.doctorDetails.DoctorLoginID != None) {
            if (UpdateDoctorDao.checkExistingLoginID(addDoctorObj.doctorDetails.DoctorLoginID.get)) {
              loginID = HCueUtils.StrConstIDAlreadyExist
            }
          }

          //checkExistingEmailID

          if (addDoctorObj.isPracticeDoctor.getOrElse(false)) {

            if (addDoctorObj.DoctorEmail != None) {
              addDoctorObj.DoctorEmail.foreach { x =>
                val doctorEmailDetails: Array[DoctorEmailDetailsObj] = x
                val loop = new Breaks;
                loop.breakable {
                  for (i <- 0 until doctorEmailDetails.length) {
                    if (UpdateDoctorDao.checkExistingEmailID(doctorEmailDetails(i).EmailID)) {
                      status = HCueUtils.StrConstEmailAlreadyExist
                    } else {
                      status = HCueUtils.StrConstSuccess
                      loop.break()
                    }
                  }
                }
              }
            }

          }

          if (status.equalsIgnoreCase(HCueUtils.StrConstSuccess) && loginID.equalsIgnoreCase(HCueUtils.StrConstSuccess)) {

            val addedRecord = AddDoctorDao.addDoctor(addDoctorObj)

            if (addedRecord.doctor.length > 0) {
              //Send SMS and Email
              val futureString: Future[String] = scala.concurrent.Future {
                if (addDoctorObj.doctorDetails.DoctorLoginID.getOrElse("") != "") {
                  AddDoctorDao.newDoctorEmail(addedRecord, addDoctorObj)
                }
                "Success"
              }
            }

            Ok(Json.toJson(addedRecord))

          } else if (status.equalsIgnoreCase(HCueUtils.StrConstLicLimitExceed)) {
            Ok(Json.toJson(status))
          } else if (loginID.equalsIgnoreCase(HCueUtils.StrConstIDAlreadyExist)) {
            Ok(Json.toJson(loginID))
          } else {
            Ok(Json.toJson(status))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def updateDoctor = Action(parse.json) { implicit request =>
    request.body.validate[updateDoctorObj] match {
      case JsSuccess(updateDoctorObj, _) =>

        try {
          var status = HCueUtils.StrConstSuccess

          status = UpdateDoctorDao.validateDocLicense(updateDoctorObj)
          //validateEmail
          if (status.equalsIgnoreCase(HCueUtils.StrConstSuccess)) {

            if (updateDoctorObj.DoctorEmail != None) {
              updateDoctorObj.DoctorEmail.foreach { x =>
                val doctorEmailDetails: Array[DoctorEmailDetailsObj] = x
                val loop = new Breaks;
                loop.breakable {
                  for (i <- 0 until doctorEmailDetails.length) {
                    if (HCueUtils.validateEmail(doctorEmailDetails(i).EmailID)) {
                      status = HCueUtils.StrConstSuccess
                    } else {
                      status = HCueUtils.StrConstFailure
                      loop.break()
                    }
                  }
                }
              }
            }
          }

          if (status.equalsIgnoreCase(HCueUtils.StrConstSuccess)) {
            val UpdateRecord = UpdateDoctorDao.updateDoctor(updateDoctorObj)

            if (UpdateRecord) {
              val doctor = ListDoctorDao.completeResponse(updateDoctorObj.doctorDetails.DoctorID, true, None, false)
              Ok(Json.toJson(doctor))
            } else {
              Ok(Json.toJson("No Record Found to Update"))
            }
          } else if (status.equalsIgnoreCase(HCueUtils.StrConstLicLimitExceed)) {
            Ok(Json.toJson(status))
          } else {
            Ok(Json.toJson(status))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def deactiveDoctor = Action(parse.json) { implicit request =>
    request.body.validate[DoctordeactiveObj] match {
      case JsSuccess(doctordeactiveObj, _) =>

        try {
          var status = HCueUtils.StrConstSuccess

          status = UpdateDoctorDao.validateDocLicensecheck(doctordeactiveObj)

          if (status.equalsIgnoreCase(HCueUtils.StrConstSuccess)) {

            status = UpdateDoctorDao.getshedulecout(doctordeactiveObj)

          }

          if (status.equalsIgnoreCase("Success")) {
            val doctordesctiveaddress = UpdateDoctorDao.deactivedoctoraddress(doctordeactiveObj)

            if (doctordesctiveaddress) {
              val doctor = ListDoctorDao.completeResponse(doctordeactiveObj.DoctorID.get, true, None, false)
              Ok(Json.toJson(doctor))
            } else {
              Ok(Json.toJson("No Record Found to Deactive"))
            }
          } else if (status.equalsIgnoreCase(HCueUtils.StrConstLicLimitExceed)) {
            Ok(Json.toJson(status))
          } else {
            Ok(Json.toJson("Feature Schedule available for this Sonographer"))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def updateSEOName(DoctorID: Long) = Action { implicit request =>
    try {
      val response = AddDoctorDao.addSEOName(DoctorID)
      Ok(Json.toJson(response))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }

  def updateDoctorLoginID = Action(parse.json) { implicit request =>
    request.body.validate[updateDoctorLoginIDObj] match {
      case JsSuccess(updateDoctorLoginIDObj, _) =>

        try {

          var status = HCueUtils.StrConstSuccess
          val CheckDoctorID = UpdateDoctorDao.ValidCheckDoctorID(updateDoctorLoginIDObj)
          if (CheckDoctorID) {
            val validcheckDocLoginID = HCueUtils.validateString(Some(updateDoctorLoginIDObj.DoctorLoginID))

            if (validcheckDocLoginID) {
              val checkExstDocLoginID = UpdateDoctorDao.checkExistingLoginID(updateDoctorLoginIDObj.DoctorLoginID)
              if (!checkExstDocLoginID) {

                val updateDocLoginID = UpdateDoctorDao.updateDoctorLoginID(updateDoctorLoginIDObj)
                Ok(Json.toJson(HCueUtils.StrConstIDLoginIDUpd))
              } else {

                Ok(Json.toJson(HCueUtils.StrConstIDAlreadyExist))
              }
            } else {
              Ok(Json.toJson(HCueUtils.StrConstInvalidDoctorLoginID))
            }
          } else {
            Ok(Json.toJson(HCueUtils.StrConstNoDoctorID))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def updateDoctorLastLogin = Action(parse.json) { implicit request =>
    request.body.validate[updateDoctorLastLoginObj] match {
      case JsSuccess(updateDoctorLastLoginObj, _) =>

        try {
          val result = UpdateDoctorDao.updateDoctorLastLogin(updateDoctorLastLoginObj.DoctorID, Some("N"))
          Ok(Json.toJson(HCueUtils.StrConstSuccess))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

}  
  
 


