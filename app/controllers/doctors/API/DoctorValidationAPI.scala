package controllers.doctors.API

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.doctorLoginDetailsObj
import models.com.hcue.doctors.Json.Request.forgotPasswordRequest
import models.com.hcue.doctors.Json.Request.updateDoctorPasswordObj
import models.com.hcue.doctors.Json.Request.updateDoctorPinPassObj
import models.com.hcue.doctors.Json.Request.validateDoctorPinPassObj
import models.com.hcue.doctors.helper.HCueUtils
import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao
import models.com.hcue.doctors.traits.login.AunthenticationDao
import models.com.hcue.doctors.traits.remainders.DoctorEMailDao
import models.com.hcue.doctors.traits.update.UpdateDoctorDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctors.Json.Request.updatepasswordObj

object DoctorValidationAPI extends Controller {

  def validateDoctorLogin = Action(parse.json) { implicit request =>
    request.body.validate[doctorLoginDetailsObj] match {
      case JsSuccess(doctorLoginDetailsObj, _) =>

        try {
          if (doctorLoginDetailsObj.googleLogin == None) {

            val doctorLogin = AunthenticationDao.validateDoctorLogin(doctorLoginDetailsObj)

            if (doctorLogin._2 == 0) {
              Ok(Json.toJson(doctorLogin._1))
            } else {

              if (doctorLoginDetailsObj.action != None && doctorLoginDetailsObj.action.get == "login") {

                val resultSet = ListDoctorDao.completeResponse(doctorLogin._2, false, None, false)
                Ok(Json.toJson(resultSet))

              } else {
                val resultSet = ListDoctorDao.completeResponse(doctorLogin._2, true, None, false)
                if (doctorLoginDetailsObj.LoggedIn != None && doctorLoginDetailsObj.LoggedIn.get != "" && resultSet.doctor.length > 0) {
                  UpdateDoctorDao.updateDoctorLastLogin(resultSet.doctor(0).DoctorID, None)
                }

                if (resultSet.doctorAddress.length > 0 && resultSet.doctorAddress(0).ExtDetails != None && resultSet.doctorAddress(0).ExtDetails.get.GroupID == None && resultSet.doctorAddress(0).ExtDetails.get.RoleID == None) {
                  Ok(Json.toJson(HCueUtils.strAccountInactive))
                } else {
                  Ok(Json.toJson(resultSet))
                }

              }

            }

          } else {

            //Validate User ID
            val doctorLogin = AunthenticationDao
              .validateGoogleLogin(doctorLoginDetailsObj)

            if (doctorLogin == null) {
              Ok(Json.toJson("Password MisMatch for the User Name " + doctorLoginDetailsObj.DoctorLoginID))
            } else {
              Ok(Json.toJson(doctorLogin))
            }

          }
        } catch {

          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def validateforgotPassword = Action(parse.json) { implicit request =>

    request.body.validate[forgotPasswordRequest] match {
      case JsSuccess(forgotPasswordRequest, _) =>

        try {

          if (forgotPasswordRequest.SendMail != None && forgotPasswordRequest.SendMail.get.equals("Y")) {
            //Send Mail to Email ID

            val status = DoctorEMailDao.getDoctorPassword(forgotPasswordRequest.DoctorLoginID, forgotPasswordRequest.PreferedEmailID)

            Ok(Json.toJson(status))
          } else {

            val doctorDetails = AunthenticationDao.validateforgotPassword(forgotPasswordRequest)

            Ok(Json.toJson(doctorDetails))

          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }

  }
  
  
  
    def forgotPassword = Action(parse.json) { implicit request =>

    request.body.validate[forgotPasswordRequest] match {
      case JsSuccess(forgotPasswordRequest, _) =>

        try {
          val status = DoctorEMailDao.forgotpassword(forgotPasswordRequest)
          Ok(Json.toJson(status))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            InternalServerError(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }

  }

  def updatePassword = Action(parse.json) { implicit request =>
    request.body.validate[updatepasswordObj] match {
      case JsSuccess(updatepasswordObj, _) =>

        try {
          val addedRecord = DoctorEMailDao.updatepassword(updatepasswordObj)

          Ok(Json.toJson(addedRecord))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def validateDoctorPinPass = Action(parse.json) { implicit request =>
    request.body.validate[validateDoctorPinPassObj] match {
      case JsSuccess(validateDoctorPinPassObj, _) =>

        try {
          val doctorPinPass = AunthenticationDao.validateDoctorPinPass(validateDoctorPinPassObj)
          Ok(Json.toJson(doctorPinPass))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def updateDoctorPinPass = Action(parse.json) { implicit request =>
    request.body.validate[updateDoctorPinPassObj] match {
      case JsSuccess(updateDoctorPinPassObj, _) =>

        try {
          val DoctorPinPass = UpdateDoctorDao.updateDoctorPinPass(updateDoctorPinPassObj)
          Ok(Json.toJson(DoctorPinPass))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

}  
