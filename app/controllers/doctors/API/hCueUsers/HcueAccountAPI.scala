package controllers.doctors.API.hCueUsers

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccount.addUpdateHcueAccountObj
import models.com.hcue.doctors.traits.add.hCueUsers.AddHcueAccountDao
import models.com.hcue.doctors.traits.list.hCueUsers.ListHcueAccountDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object HcueAccountAPI extends Controller {

  def addUpdateHcueAccount = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateHcueAccountObj] match {
      case JsSuccess(addUpdateHcueAccountObj, _) =>

        try {

          val hcueAccount = AddHcueAccountDao.addUpdateHcueAccount(addUpdateHcueAccountObj)

          val account = ListHcueAccountDao.ListHcueAccount()
          Ok(Json.toJson(account))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listHcueAccount = Action { implicit request =>
    val account = ListHcueAccountDao.ListHcueAccount()
    Ok(Json.toJson(account))
  }

}  
  
 

