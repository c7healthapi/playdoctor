package controllers.doctors.API.hCueUsers

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHospitalRole.addDefaultHospitalRoleObj
import models.com.hcue.doctors.traits.add.hCueUsers.AddHcueHospitalRoleSettingDao
import models.com.hcue.doctors.traits.list.hCueUsers.ListHcueHospitalRoleSettingDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object HcueHospitalRoleSettingAPI extends Controller {

  def addHospitalRole = Action(parse.json) { implicit request =>

    request.body.validate[addDefaultHospitalRoleObj] match {
      case JsSuccess(addDefaultHospitalRoleObj, _) =>

        try {

          val hcueAccount = AddHcueHospitalRoleSettingDao.addHospitalRole(addDefaultHospitalRoleObj)

          val account = ListHcueHospitalRoleSettingDao.ListHospitalRoles(addDefaultHospitalRoleObj.HospitalID)
          Ok(Json.toJson(account))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listHospitalRole(HospitalID: Long) = Action { implicit request =>
    val hospital = ListHcueHospitalRoleSettingDao.ListHospitalRoles(HospitalID)
    Ok(Json.toJson(hospital))
  }

}  
