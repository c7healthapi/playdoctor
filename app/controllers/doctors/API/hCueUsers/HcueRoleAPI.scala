package controllers.doctors.API.hCueUsers

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueRole.addUpdateRoleObj
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueRole.listHospitalRoleObj
import models.com.hcue.doctors.traits.add.hCueUsers.AddHcueRoleDao
import models.com.hcue.doctors.traits.list.hCueUsers.ListHcueRoleDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object HcueRoleAPI extends Controller {

  def addUpdateHcueRole = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateRoleObj] match {
      case JsSuccess(addUpdateRoleObj, _) =>

        try {

          val hcueAccount = AddHcueRoleDao.addUpdateRole(addUpdateRoleObj)

          val roles = ListHcueRoleDao.ListHcueRole()
          Ok(Json.toJson(roles))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def getHospitalRoleLukp = Action(parse.json) { implicit request =>

    request.body.validate[listHospitalRoleObj] match {
      case JsSuccess(listHospitalRoleObj, _) =>
        try {
          val doctorSearch = ListHcueRoleDao.listHospitalRoles(listHospitalRoleObj)
          Ok(Json.toJson(doctorSearch))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def listHcueRole = Action { implicit request =>
    val roles = ListHcueRoleDao.ListHcueRole()
    Ok(Json.toJson(roles))
  }
}  
