package controllers.doctors.API.hCueUsers

import scala.concurrent.Future

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateProfileAccessRights.addUpdateProfileAccessRightsObj
import models.com.hcue.doctors.traits.add.hCueUsers.AddUpdateHcueProfileGroupsDao
import models.com.hcue.doctors.traits.list.hCueUsers.ListHcueProfileAccessRightsDao
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object HcueHospitalUserRoleGroupAPI extends Controller {

  def AddUpdateHcueProfileGroups = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateProfileAccessRightsObj] match {
      case JsSuccess(addUpdateProfileAccessRightsObj, _) =>
        try {
          val hcueAccount = AddUpdateHcueProfileGroupsDao.AddUpdateHcueProfileGroups(addUpdateProfileAccessRightsObj)

          if (hcueAccount != None) {
            val futureString: Future[String] = scala.concurrent.Future {
              try {
                AddUpdateHcueProfileGroupsDao.addUpdateHospitalConsultantFlag(addUpdateProfileAccessRightsObj)
              } catch {

                case e: Exception =>
                  e.printStackTrace()
                  val status = ErrorMessageBean.onErrorInFuture(null, "Play Doctor" + this.getClass.getName + ".AddUpdateHcueProfileGroups.futureString", "InFutureBlock", e, None)

              }
              ""
            }
          }
          val account = ListHcueProfileAccessRightsDao.ListHcueAccessRights(addUpdateProfileAccessRightsObj.HospitalID)
          Ok(Json.toJson(account))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def ListHcueProfileGroups(hospitalID: Long) = Action { implicit request =>
    val accountAccess = ListHcueProfileAccessRightsDao.ListHcueAccessRights(hospitalID)
    Ok(Json.toJson(accountAccess))
  }

}  
