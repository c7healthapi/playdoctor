package controllers.doctors.API.hCueUsers

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccessRights.HcueAccessRightsReqObj
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccessRights.addUpdateHcueAccessRightsObj
import models.com.hcue.doctors.traits.add.hCueUsers.AddHcueAccessRightsDao
import models.com.hcue.doctors.traits.list.hCueUsers.ListHcueAccessRightsDao
import play.Logger
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object HcueAccessRightsAPI extends Controller {

  private val log = Logger.of("application")

  def addUpdateHcueAccessRights = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateHcueAccessRightsObj] match {
      case JsSuccess(addUpdateHcueAccessRightsObj, _) =>

        try {
          val hcueAccessRights = AddHcueAccessRightsDao.addUpdateHcueAccessRights(addUpdateHcueAccessRightsObj)
          val account = ListHcueAccessRightsDao.ListHcueAccessRights()
          Ok(Json.toJson(account))

          Ok(Json.toJson(""))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listHcueAccessRights = Action { implicit request =>
    val account = ListHcueAccessRightsDao.ListHcueAccessRights()
    Ok(Json.toJson(account))
  }

  def listHcueAdminAccessRights = Action(parse.json) { implicit request =>

    request.body.validate[HcueAccessRightsReqObj] match {
      case JsSuccess(hcueAccessRightsReqObj, _) =>

        try {
          val account = ListHcueAccessRightsDao.ListHcueAdminAccessRights(hcueAccessRightsReqObj.hospitalId, hcueAccessRightsReqObj.USRType)

          Ok(Json.toJson(account))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

}  
