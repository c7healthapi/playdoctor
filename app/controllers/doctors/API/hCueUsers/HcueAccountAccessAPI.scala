package controllers.doctors.API.hCueUsers

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccountAccess.addUpdateAccountAccessObj
import models.com.hcue.doctors.traits.add.hCueUsers.AddHcueAccountAccesstDao
import models.com.hcue.doctors.traits.list.hCueUsers.ListHcueAccountAccessDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object HcueAccountAccessAPI extends Controller {

  def addUpdateHcueAccountAccess = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateAccountAccessObj] match {
      case JsSuccess(addUpdateAccountAccessObj, _) =>

        try {

          val hcueAccount = AddHcueAccountAccesstDao.addUpdateHcueAccountAccess(addUpdateAccountAccessObj)

          val account = ListHcueAccountAccessDao.ListHcueAccountAccess()
          Ok(Json.toJson(account))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listHcueAccountAccess = Action { implicit request =>
    val accountAccess = ListHcueAccountAccessDao.ListHcueAccountAccess()
    Ok(Json.toJson(accountAccess))
  }

}  
