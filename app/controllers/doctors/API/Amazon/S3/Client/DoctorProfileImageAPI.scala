package controllers.doctors.API.Amazon.S3.Client

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.S3.Amazon.doctorProfileImageObj
import models.com.hcue.doctors.traits.Amazon.S3.Image.DoctorS3ImageDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object DoctorProfileImageAPI extends Controller {

  def upload = Action(parse.json) { implicit request =>

    request.body.validate[doctorProfileImageObj] match {
      case JsSuccess(doctorProfileImageObj, _) =>

        try { //DoctorID:Long,imageStr:Option[String],filePath:String,fileName:String,updateURL:Boolean

          if (doctorProfileImageObj.isDeleted != None && doctorProfileImageObj.isDeleted.get == "Y") {
            //val result = DoctorS3ImageSchedularDao.deleteDoctorProfileImage(doctorProfileImageObj.DoctorID)
            val result = ""
            Ok(Json.toJson(result))
          } else {
            val result = DoctorS3ImageDao.updateHelperService(doctorProfileImageObj.DoctorID, doctorProfileImageObj.ImageStr, doctorProfileImageObj.DoctorID.toString, "profileImage", true, None)
            Ok(Json.toJson(result))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def download = Action(parse.json) { implicit request =>

    request.body.validate[doctorProfileImageObj] match {
      case JsSuccess(doctorProfileImageObj, _) =>

        try {
          val result = DoctorS3ImageDao.downloadHelperService(doctorProfileImageObj.DoctorID, doctorProfileImageObj.DoctorID.toString() + "profileImage.txt")
          if (result == None) {
            Ok(Json.toJson("Image Not Found"))
          } else {
            var s3Response: doctorProfileImageObj =
              new doctorProfileImageObj(result, doctorProfileImageObj.DoctorID, None)
            Ok(Json.toJson(s3Response))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

}