package controllers.doctors.API.Amazon.S3.Client

import java.io.FileInputStream

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectMetadata
import com.google.common.io.Files

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.S3.Amazon.deleteHospitalClinicImageObj
import models.com.hcue.doctors.Json.Response.responseObj
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.Play.current

object HospitalClinicImageAPI extends Controller {
  
  val s3AccessKey = current.configuration.getString("aws.s3.accessKey").get
  val s3SecretKey = current.configuration.getString("aws.s3.secretKey").get

  val yourAWSCredentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey)
  val amazonS3Client = new AmazonS3Client(yourAWSCredentials)

  var input: FileInputStream = null

  def uploadHospitalFile = Action(parse.multipartFormData) { implicit request =>

    var fileName = String.valueOf(System.currentTimeMillis())
    var hospitalId = ""
    var isLogoImage = ""
    var fileExtn = ""

    request.body.file("fileUpload") match {
      case Some(patientFile) => {
        try {

          isLogoImage = request.body.dataParts.get("isLogoImage").get.head;
          hospitalId = request.body.dataParts.get("hospitalID").get.head;
          fileExtn = request.body.dataParts.get("fileExtn").get.head;

          val hospitalID = hospitalId.toString()
          val byteArray = Files.toByteArray(patientFile.ref.file)
          input = new FileInputStream(patientFile.ref.file)
          val metadata = new ObjectMetadata();
          metadata.setContentType(patientFile.contentType.get)
          metadata.setContentLength(byteArray.length);
          metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);

          amazonS3Client.setEndpoint("https://s3.amazonaws.com")

          val amazonResponse = amazonS3Client.putObject("", "hospital/" + hospitalID + "/Images/" + fileName + ".png", input, metadata);
          var returnMessage = "Failure"

          if (amazonResponse.getVersionId != None && fileExtn.toString() != "") {
          }
          Ok(Json.toJson("success"))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, None)
            Ok(Json.toJson(status))
        } finally {
          if (input != null)
            input.close()
        }
      }

      case _ => { Ok(Json.toJson(new responseObj("Failure", None, None))) }
    }
  }

  def deleteImages = Action(parse.json) { implicit request =>

    request.body.validate[deleteHospitalClinicImageObj] match {
      case JsSuccess(deleteHospitalClinicImageObj, _) =>

        try {

          //DoctorS3ImageSchedularDao.deleteClinicImage(deleteHospitalClinicImageObj.HospitalID, deleteHospitalClinicImageObj.ImageName)
          Ok(Json.toJson("Success"))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

}