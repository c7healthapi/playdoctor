package controllers.doctors.API.Amazon.S3.Client

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.S3.Amazon.addUpdateDoctorCareerImageObj
import models.com.hcue.doctors.Json.Request.S3.Amazon.addUpdateDoctorPrintSettingImageObj
import models.com.hcue.doctors.Json.Request.S3.Amazon.getImageStringObj
import models.com.hcue.doctors.traits.Amazon.S3.Image.DoctorS3ImageDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object DoctorPersonalImageAPI extends Controller {

  def upload = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateDoctorCareerImageObj] match {
      case JsSuccess(addUpdateDoctorCareerImageObj, _) =>

        try {
          val fileName = addUpdateDoctorCareerImageObj.ImageName.getOrElse(String.valueOf(System.currentTimeMillis()))
          val filePath = addUpdateDoctorCareerImageObj.DoctorID + "/Career"

          val result = DoctorS3ImageDao.updateHelperService(addUpdateDoctorCareerImageObj.DoctorID, addUpdateDoctorCareerImageObj.ImageStr, filePath, fileName, false, None)

          if ("Success".equals(result)) {
            Ok(Json.toJson(fileName))
          } else {
            Ok(Json.toJson(result))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def download = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateDoctorCareerImageObj] match {
      case JsSuccess(addUpdateDoctorCareerImageObj, _) =>

        try {
          var filePath = addUpdateDoctorCareerImageObj.DoctorID.toString() + "/Career/" + addUpdateDoctorCareerImageObj.ImageName.get + ".txt"
          val result = DoctorS3ImageDao.downloadHelperService(addUpdateDoctorCareerImageObj.DoctorID, filePath)
          if (result == None) {
            Ok(Json.toJson("Image Not Found"))
          } else {
            var s3Response: addUpdateDoctorCareerImageObj =
              new addUpdateDoctorCareerImageObj(result, addUpdateDoctorCareerImageObj.DoctorID, addUpdateDoctorCareerImageObj.ImageName)
            Ok(Json.toJson(s3Response))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def uploadSettingImages = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateDoctorPrintSettingImageObj] match {
      case JsSuccess(addUpdateDoctorPrintSettingImageObj, _) =>

        try {
          val fileName = addUpdateDoctorPrintSettingImageObj.ImageName.getOrElse(String.valueOf(System.currentTimeMillis()))
          var filePath = addUpdateDoctorPrintSettingImageObj.DoctorID + "/PrintSetting"
          if (addUpdateDoctorPrintSettingImageObj.ScreenType == "PRES") {
            filePath = filePath + "/Prescription"
          } else if (addUpdateDoctorPrintSettingImageObj.ScreenType == "BILL") {
            filePath = filePath + "/Billing"
          } else if (addUpdateDoctorPrintSettingImageObj.ScreenType == "BILLRECP") {
            filePath = filePath + "/Billing"
          } else {
            filePath = filePath + "/History"
          }

          val result = DoctorS3ImageDao.uploadFileHelperService(addUpdateDoctorPrintSettingImageObj.DoctorID, addUpdateDoctorPrintSettingImageObj.ImageStr, filePath, fileName.replaceAll("\\s+", "_"), true)

          Ok(Json.toJson(result))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def getImageString = Action(parse.json) { implicit request =>

    request.body.validate[getImageStringObj] match {
      case JsSuccess(getImageStringObj, _) =>

        try {
          val result = DoctorS3ImageDao.createImageString(getImageStringObj.ImageURL)
          Ok(Json.toJson(result))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

}