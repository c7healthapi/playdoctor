package controllers.doctors.API.Amazon.S3.Client

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.S3.Amazon.addUpdateDoctorClinicImageObj
import models.com.hcue.doctors.Json.Request.S3.Amazon.deleteDoctorClinicImageObj
import models.com.hcue.doctors.traits.Amazon.S3.Image.DoctorS3ImageDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object DoctorClinicImageAPI extends Controller {

  def upload = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateDoctorClinicImageObj] match {
      case JsSuccess(addUpdateDoctorClinicImageObj, _) =>

        try {

          val fileName = addUpdateDoctorClinicImageObj.ImageName.getOrElse(String.valueOf(System.currentTimeMillis()))

          val filePath = addUpdateDoctorClinicImageObj.DoctorID + "/Clinic/" + addUpdateDoctorClinicImageObj.AddressID.toString()

          // val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

          val result = DoctorS3ImageDao.updateHelperService(addUpdateDoctorClinicImageObj.DoctorID, addUpdateDoctorClinicImageObj.ImageStr, filePath, fileName, true, Some(addUpdateDoctorClinicImageObj.AddressID))

          if ("Success".equals(result)) {
            Ok(Json.toJson(fileName))
          } else {
            Ok(Json.toJson(result))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def delete = Action(parse.json) { implicit request =>

    request.body.validate[deleteDoctorClinicImageObj] match {
      case JsSuccess(deleteDoctorClinicImageObj, _) =>

        try {

          val fileName = deleteDoctorClinicImageObj.ImageName

          val deleteall = deleteDoctorClinicImageObj.DeleteAll

          val filePath = deleteDoctorClinicImageObj.DoctorID + "/Clinic/" + deleteDoctorClinicImageObj.AddressID.toString()

          val result = DoctorS3ImageDao.deleteHelperService(deleteDoctorClinicImageObj.DoctorID, filePath, fileName, deleteall, true, deleteDoctorClinicImageObj.AddressID)

          if ("Success".equals(result)) {
            Ok(Json.toJson(result))
          } else {
            Ok(Json.toJson(result))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def download = Action(parse.json) { implicit request =>

    request.body.validate[addUpdateDoctorClinicImageObj] match {
      case JsSuccess(addUpdateDoctorClinicImageObj, _) =>

        try {

          var filePath = addUpdateDoctorClinicImageObj.DoctorID + "/Clinic/" + addUpdateDoctorClinicImageObj.AddressID.toString() + "/" +
            addUpdateDoctorClinicImageObj.ImageName.get + ".txt"

          val result = DoctorS3ImageDao.downloadHelperService(addUpdateDoctorClinicImageObj.DoctorID, filePath)

          if (result == None) {
            Ok(Json.toJson("Image Not Found"))
          } else {
            var s3Response: addUpdateDoctorClinicImageObj =
              new addUpdateDoctorClinicImageObj(result, addUpdateDoctorClinicImageObj.DoctorID, addUpdateDoctorClinicImageObj.AddressID, addUpdateDoctorClinicImageObj.ImageName)
            Ok(Json.toJson(s3Response))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

}