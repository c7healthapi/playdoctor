package controllers.doctors.API.Doctor

import play.api.mvc.{ Action, Controller }
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.Form
import play.api.data.Forms.{ mapping, text, of }
import play.api.data.format.Formats.doubleFormat
import play.api.data.validation.Constraints
import play.api.data.validation.ValidationError
import play.api.libs.json.JsError
import java.sql.Date
import java.util.Date
import play.api.libs.json.JsValue
import org.postgresql.util.PSQLException
import models.com.hcue.doctors.Json.Request.doctorSearchDetailsObj
import models.com.hcue.doctors.Json.Response.SearchOutPutObj
import models.com.hcue.doctors.Json.Response.getResponseObj
import models.com.hcue.doctors.traits.common.CommonDao
import models.com.hcue.doctors.traits.list.search.SearchDoctorDao
import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Response.furtherCareDoctorObj
import models.com.hcue.doctors.Json.Response.DoctorDetailsObj
import models.com.hcue.doctors.Json.Request.PatientAppDoctorSearchDetailsObj
import models.com.hcue.doctors.Json.Response.SearchDoctorDetailsObj
import models.com.hcue.doctors.Json.Response.DoctorDetailObj

object SearchAPI extends Controller {

  implicit val writesPatient = Json.writes[TPatient]

  //Added For Search
  implicit val pagesWrite = (
    (__ \ 'rows).write[Array[getResponseObj]] and
    (__ \ 'count).write[Int]).tupled: Writes[(Array[getResponseObj], Int)]

  implicit val pagesWrite2 = (
    (__ \ 'rows).write[Array[furtherCareDoctorObj]] and
    (__ \ 'count).write[Int]).tupled: Writes[(Array[furtherCareDoctorObj], Int)]
  implicit val searchfurtherCareDoctorObjReads = Json.reads[furtherCareDoctorObj]
  implicit val searchfurtherCareDoctorObjWrites = Json.writes[furtherCareDoctorObj]

  implicit val pagesWrite3 = (
    (__ \ 'DoctorDetails).write[Array[DoctorDetailObj]] and
    (__ \ 'count).write[Int]).tupled: Writes[(Array[DoctorDetailObj], Int)]
  implicit val DoctorDetailObjReads = Json.reads[DoctorDetailObj]
  implicit val DoctorDetailObjWrites = Json.writes[DoctorDetailObj]

  implicit val pagesWrite4 = (
    (__ \ 'rows).write[Array[SearchDoctorDetailsObj]] and
    (__ \ 'count).write[Int]).tupled: Writes[(Array[SearchDoctorDetailsObj], Int)]
  implicit val SearchDoctorDetailsObjReads = Json.reads[SearchDoctorDetailsObj]
  implicit val SearchDoctorDetailsObjWrites = Json.writes[SearchDoctorDetailsObj]

  import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao

  def searchBy = Action(parse.json) { implicit request =>

    request.body.validate[doctorSearchDetailsObj] match {
      case JsSuccess(doctorSearchDetailsObj, _) =>
        //val searchList = SearchDoctorDao.searchByIDS(doctorSearchDetailsObj)

        try {

          if (doctorSearchDetailsObj.HospitalID != None) {

            val searchList = SearchDoctorDao.searchByHospitalIDS(
              doctorSearchDetailsObj.DoctorID,
              doctorSearchDetailsObj.HospitalID.get, doctorSearchDetailsObj.PageNumber, doctorSearchDetailsObj.PageSize,
              doctorSearchDetailsObj.filterByDate, doctorSearchDetailsObj.filterByEndDate, doctorSearchDetailsObj.ActiveDoctors)
            Ok(Json.toJson(searchList))

          } else if (doctorSearchDetailsObj.HospitalCD != None) {

            val searchList = SearchDoctorDao.searchByHospitalCode(doctorSearchDetailsObj.DoctorID, doctorSearchDetailsObj.HospitalID,
              doctorSearchDetailsObj.HospitalCD.get, doctorSearchDetailsObj.PageNumber, doctorSearchDetailsObj.PageSize,
              doctorSearchDetailsObj.ActiveDoctors)
            Ok(Json.toJson(searchList))

          } else {
            Ok(Json.toJson("PLEASE_CHECK_THE_INPUT_CONDITION"))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def getDoctorsList() = Action(parse.json) { implicit request =>
    request.body.validate[PatientAppDoctorSearchDetailsObj] match {
      case JsSuccess(patientAppDoctorSearchDetailsObj, _) =>

        try {
          val list = SearchDoctorDao.getDoctorDetails(patientAppDoctorSearchDetailsObj)
          Ok(Json.toJson(list))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, None)
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def getDoctorNameInfo(seoname: String) = Action { implicit request =>
    try {  
    val doctorID = SearchDoctorDao.getSEODoctorId(seoname)
    val response = doctorID match {
      case _ => ListDoctorDao.completeResponse(doctorID, false, None,false)
    }
    Ok(Json.toJson(response))
     }catch {
    case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request,e,None)
            Ok(Json.toJson(status))
        }

  }

} 
