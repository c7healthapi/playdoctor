package controllers.doctors.API.Doctor

import play.api.mvc.{ Action, Controller }
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.Form
import play.api.data.Forms.{ mapping, text, of }
import play.api.data.format.Formats.doubleFormat
import play.api.data.validation.Constraints
import play.api.data.validation.ValidationError
import play.api.libs.json.JsError
import java.sql.Date
import java.util.Date

import play.api.libs.json.JsValue
import org.postgresql.util.PSQLException

import models.com.hcue.doctors.Json.Request.VisitReason.doctorResonSearchDetailsObj
import models.com.hcue.doctors.Json.Response.Doctor.VisitReason.getDoctorVisitRsnObj

import models.com.hcue.doctors.traits.common.CommonDao
import models.com.hcue.doctors.traits.list.search.SearchDoctorReasonDao
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorVisitRsnLkyp
import models.com.hcue.doctors.Bean.ErrorMessageBean

object VisitReasonAPI extends Controller {

  implicit val readsCreateVisitReason = ((__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "DoctorSpecialityID").read[String] and (__ \ "DoctorVisitRsnDesc").read[String] and
    (__ \ "VisitUserTypeID").read[String] and (__ \ "SequenceRow").read[Int] and
    (__ \ "ActiveIND").read[String])(TDoctorVisitRsnLkyp.apply _)

  implicit val writesDoctorVisitReason = Json.writes[TDoctorVisitRsnLkyp]

  //Added For Search
  implicit val pagesWrite = (
    (__ \ 'rows).write[Array[getDoctorVisitRsnObj]] and
    (__ \ 'count).write[Int]).tupled: Writes[(Array[getDoctorVisitRsnObj], Int)]
  implicit val searchDoctorVisitRsnObjReads = Json.reads[getDoctorVisitRsnObj]
  implicit val searchDoctorVisitRsnObjWrites = Json.writes[getDoctorVisitRsnObj]

  def searchByspecialityID = Action(parse.json) { implicit request =>

    request.body.validate[doctorResonSearchDetailsObj] match {
      case JsSuccess(doctorResonSearchDetailsObj, _) =>

        try {
          //val searchList = SearchDoctorDao.searchByIDS(doctorResonSearchDetailsObj)
          val searchList = SearchDoctorReasonDao.searchBySpecialityID(doctorResonSearchDetailsObj)
          Ok(Json.toJson(searchList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

}  
