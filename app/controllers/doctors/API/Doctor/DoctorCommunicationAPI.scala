package controllers.doctors.API.Doctor

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.doctorPatientSearchDetailsObj
import models.com.hcue.doctors.Json.Response.buildPatientSearchResponseObj
import models.com.hcue.doctors.Json.Response.errorObj
import models.com.hcue.doctors.Json.Response.responseObj
import models.com.hcue.doctors.traits.list.patient.ListPatientDao
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.libs.json.Writes
import play.api.libs.json.__
import play.api.mvc.Action
import play.api.mvc.Controller

object DoctorCommunicationAPI extends Controller {


  implicit val pagesWriteDoctor = (
    (__ \ 'rows).write[Array[buildPatientSearchResponseObj]] and
    (__ \ 'count).write[Int]).tupled: Writes[(Array[buildPatientSearchResponseObj], Int)]
  implicit val searchDoctorPatientObjReads = Json.reads[buildPatientSearchResponseObj]
  implicit val searchDoctorPatientObjWrites = Json.writes[buildPatientSearchResponseObj]

  def getDoctorPatients = Action(parse.json) { implicit request =>
    request.body.validate[doctorPatientSearchDetailsObj] match {
      case JsSuccess(doctorPatientSearchDetailsObj, _) =>
        try {

          if (doctorPatientSearchDetailsObj.MessageType != None && (doctorPatientSearchDetailsObj.MessageType.get == "SMS" || doctorPatientSearchDetailsObj.MessageType.get == "EMAIL" || doctorPatientSearchDetailsObj.MessageType.get == "BOTH")) {
            val searchList = ListPatientDao.getCommuncationPatients(doctorPatientSearchDetailsObj, "N", None, None)
            Ok(Json.toJson(searchList))
          } else if (doctorPatientSearchDetailsObj.LocalSearchFlag != None) {
            val searchList = ListPatientDao.getPatientsBySearchText(doctorPatientSearchDetailsObj)
            if (searchList._1.length > 0) {
              val response: responseObj = new responseObj("success", Some(Json.toJson(searchList)), None)
              Ok(Json.toJson(response))
            } else if (searchList._1.length == 0 && searchList._2 > 0) {
              val error: errorObj = new errorObj("No Records found for the page", "")
              val response: responseObj = new responseObj("failure", None, Some(error))
              Ok(Json.toJson(response))
            } else {
              val error: errorObj = new errorObj("No Records found for the given input", "")
              val response: responseObj = new responseObj("failure", None, Some(error))
              Ok(Json.toJson(response))
            }
          } else if (doctorPatientSearchDetailsObj.SearchText != None) {
            val searchList = ListPatientDao.getMyPatients(doctorPatientSearchDetailsObj)
            Ok(Json.toJson(searchList))
          } else {
            Ok(Json.toJson("Enter Valid Input MessageType"))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }
}