package controllers.API.scheduler.schedule

import akka.actor.Actor
import models.com.hcue.doctors.traits.add.hospitalappointment.AddHospitalAppointmentDao
import play.api.mvc.Controller
import models.com.hcue.doctors.traits.SMSContent

object SonographerSchedulerAPI extends Controller {

  class sonographerSchedulerActor(myMessage: Object) extends Actor {

    def receive = {
      case "appointmentDetailScheduler" => {

        val responseList = AddHospitalAppointmentDao.sendsonographerInfo
        
      }
      
      
      case "cswappointmentDetailScheduler" => {
         
        
        val responseList = AddHospitalAppointmentDao.sendsupportworkerInfo

      }
      
      case "appointmentDetailscancenterScheduler" => {
         
        val days=1
        val responseList = AddHospitalAppointmentDao.sendappointmentdetailstoscancenter(days)

      }
      
      
      case "appointmentSmsRemainder" => {
         
        val days=7
        val responseList = SMSContent.sendAppointmentReminder(days)

      }
      
      

      case _ => {
        play.api.Logger.warn("unsupported message type")
      }

    }
  }
}
