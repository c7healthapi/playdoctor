package controllers.doctors.API.Doctor.HospitalSchedule

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.addScheduleObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.getCalendarObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.getScheduleObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.getavailappointmentObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.updateScheduleObj
import models.com.hcue.doctors.traits.add.hospitalschedule.HospitalScheduleDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.addScheduleRecord
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.delobj

object HospitalScheduleAPI extends Controller {

  def addHospitalSchedule = Action(parse.json) { implicit request =>

    request.body.validate[addScheduleObj] match {
      case JsSuccess(addScheduleObj, _) =>

        try {

          val scheduleRec = addScheduleObj.scheduleRecord
          var strub: (Boolean, String) = (false, "");

          strub = HospitalScheduleDao.addValidateConsultation(scheduleRec)

          if (strub._1) {
            if (addScheduleObj.Scheduledateflag == "Y") {
              val addedRecord = HospitalScheduleDao.addHospitalSchedule1withdays(addScheduleObj)
            } else {

              val addedRecord = HospitalScheduleDao.addHospitalSchedule(addScheduleObj)
            }

            Ok(Json.toJson("Success"))

          } else {
            Ok(Json.toJson("Sonographer Already mapped to another Address"))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def updateHospitalSchedule = Action(parse.json) { implicit request =>
    request.body.validate[updateScheduleObj] match {
      case JsSuccess(updateScheduleObj, _) =>

        try {

          val scheduleRec = updateScheduleObj.scheduleRecord
          var strub: (Boolean, String) = (false, "");
          var slotvaliationcount = 0;

          strub = HospitalScheduleDao.updateValidateConsultation(scheduleRec, updateScheduleObj.HospitalID)
          
          slotvaliationcount = HospitalScheduleDao.scheduleslotValidation(scheduleRec, updateScheduleObj.HospitalID)

          if (strub._1) {
            
            if(slotvaliationcount>0) {
            
            if (updateScheduleObj.Scheduledateflag == "Y") {
              val UpdateRecord = HospitalScheduleDao.updateHospitalSchedule1withdays(updateScheduleObj)
            } else {
              val UpdateRecord = HospitalScheduleDao.updateHospitalSchedule(updateScheduleObj)
            } 

            Ok(Json.toJson("Success"))
            
            }else {
              Ok(Json.toJson("Appointment already booked for this schedule.Please give proper starttime,endtime and slot minutes"))
            }
            
            
          } else {
            Ok(Json.toJson("Sonographer or Support worker already mapped to another Scan Center"))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  
  
  
  def update = Action(parse.json) { implicit request =>
    request.body.validate[delobj] match {
      case JsSuccess(addScheduleRecord, _) =>

        try {

          val schedule = addScheduleRecord
          
          val dele = HospitalScheduleDao.update(addScheduleRecord)

          if (dele.equals("Updated Successfully")) {
           
            Ok(Json.toJson("Success"))
          } else {
            InternalServerError(Json.toJson("Appointment Exists for this schedule"))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  def listAllScheduleInfo = Action(parse.json) { implicit request =>
    request.body.validate[getScheduleObj] match {
      case JsSuccess(getScheduleObj, _) =>
        try {

          val ScheduleList = HospitalScheduleDao.listAllSchedule(getScheduleObj)
          Ok(Json.toJson(ScheduleList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = "Error"
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listAllCalendarInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCalendarObj] match {
      case JsSuccess(getCalendarObj, _) =>
        try {

          val CalendarList = HospitalScheduleDao.listAllCalendartest(getCalendarObj)
          Ok(Json.toJson(CalendarList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = "Error"
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listAvailableAppointment = Action(parse.json) { implicit request =>
    request.body.validate[getavailappointmentObj] match {
      case JsSuccess(getavailappointmentObj, _) =>
        try {

          val CalendarList = HospitalScheduleDao.listAllAppointment(getavailappointmentObj)
          Ok(Json.toJson(CalendarList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = "Error"
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def getAppointmentDetails(id: Long) = Action { implicit request =>

    try {

      val fullDetails = HospitalScheduleDao.getAppointmentInfo(id)
      Ok(Json.toJson(fullDetails))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))

    }
  }

}  














