package controllers.doctors.API.Doctor

import scala.concurrent.Future
import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.DAO.messaging.messagingDAO
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.addScheduleAppointmentDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.appointmentEmailerObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.getHospitalAppointmentDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.sendAppointmentmailObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.sendFileAttachmentObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.sonographerinfoObj
import models.com.hcue.doctors.Json.Request.AppointmentStatusUpdateObj
import models.com.hcue.doctors.Json.Request.listCalenderComponentRequestObj
import models.com.hcue.doctors.Json.Request.updateDtAppointmentDetailsObj
import models.com.hcue.doctors.traits.add.hospitalappointment.AddHospitalAppointmentDao
import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao
import models.com.hcue.doctors.traits.update.UpdateDoctorDao
import play.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctors.traits.mobileSMS.Appointment.AppointmentStatusDao
import models.com.hcue.doctors.Json.Request.sendAppointmentSMSReq
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.hospitalAppointmentResp

object HospitalAppointmentAPI extends Controller {

  private val log = Logger.of("application")

  def addupdateHospitalAppointments = Action(parse.json) { implicit request =>

    request.body.validate[addScheduleAppointmentDetailsObj] match {
      case JsSuccess(addScheduleAppointmentDetailsObj, _) =>

        try {

          var validationcheck: Boolean = true;
          if (addScheduleAppointmentDetailsObj.AppointmentID == None) {
            validationcheck = AddHospitalAppointmentDao.appointmentbookingvalidation(addScheduleAppointmentDetailsObj)
          }
          if (validationcheck) {

            val addAppointment = AddHospitalAppointmentDao.addUpdateAppointments(addScheduleAppointmentDetailsObj, None)

            log.info("AppointmentAPI ->  addAppointments " + addAppointment)

            if (addAppointment._2 == 0) {
              Ok(Json.toJson(addAppointment._1))
            } else {

              addAppointment._3 match {
                case "B" | "C" =>
                  println("HL7 Message Invocation Starts");
                  log.info("HL7 Message Invocation Starts")
                  val invokeHL7Webservice: Future[String] = scala.concurrent.Future {
                    try {
                      val respone = messagingDAO.invokeHL7Webservice(addAppointment._2)
                      println("HL7 Message Response :: " + respone);
                      log.info("HL7 Message Response :: " + respone)
                      respone
                    } catch {
                      case e: Exception =>
                        e.printStackTrace()
                        val status = ErrorMessageBean.onErrorInFuture(request, "Play Doctor :" + this.getClass.getName + ".addupdateHospitalAppointments.invokeHL7Webservice", "InFutureBlock", e, Some(Json.toJson(request.body)))
                        "Failure"
                    }
                  }
                case _ => "Nothing"
              }
              val response = new hospitalAppointmentResp(addAppointment._2,addAppointment._3)
              Ok(Json.toJson(response))
            }

          } else {
            Ok(Json.toJson("Time Slot Already mapped"))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def sendAppointmentSMS = Action(parse.json) { implicit request =>
    request.body.validate[sendAppointmentSMSReq] match {
      case JsSuccess(_sendAppointmentSMSReq, _) =>
        try {
          val response = AppointmentStatusDao.sendAppointmentSMS_v1(_sendAppointmentSMSReq)
          Ok(Json.toJson(response))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def updateAppointments = Action(parse.json) { implicit request =>

    request.body.validate[updateDtAppointmentDetailsObj] match {
      case JsSuccess(updateDtAppointmentDetailsObj, _) =>

        try {

          val updateAppointment = UpdateDoctorDao.updateAppointments(updateDtAppointmentDetailsObj)
          if (updateAppointment._2 != 0) { // If Invalid Appointment number entered
            val bookedStatus = ListDoctorDao.appointmentStatus(updateAppointment._2)
            Ok(Json.toJson(bookedStatus))
          } else {
            Ok(Json.toJson(updateAppointment._1))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))

        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def getAppointments = Action(parse.json) { implicit request =>

    request.body.validate[getHospitalAppointmentDetailsObj] match {
      case JsSuccess(getHospitalAppointmentDetailsObj, _) =>

        try {
          if (getHospitalAppointmentDetailsObj.AddressConsultID == None && getHospitalAppointmentDetailsObj.AddressID == None && getHospitalAppointmentDetailsObj.HospitalID == None && getHospitalAppointmentDetailsObj.HospitalCD == None) {
            BadRequest("Please send the AddressConsultID")
          } else {
            val doctorAppointment = AddHospitalAppointmentDao.getHospitalAppointments(getHospitalAppointmentDetailsObj)
            Ok(Json.toJson(doctorAppointment))
          }
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def updateAppointmentStatus = Action(parse.json) { implicit request =>

    request.body.validate[Array[AppointmentStatusUpdateObj]] match {
      case JsSuccess(appointmentStatusUpdateObj, _) =>

        try {
          val specialityval = AddHospitalAppointmentDao.updateAppointments(appointmentStatusUpdateObj)
          Ok(Json.toJson(specialityval))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }

  }

  def listCalenderComponents = Action(parse.json) { implicit request =>

    request.body.validate[listCalenderComponentRequestObj] match {
      case JsSuccess(listCalenderComponentRequestObj, _) =>

        try {

          val addAppointment = AddHospitalAppointmentDao.listCalenderComponentDtls(listCalenderComponentRequestObj)

          Ok(Json.toJson(addAppointment))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendPatientAppointmentEmail = Action(parse.json) { implicit request =>

    request.body.validate[appointmentEmailerObj] match {
      case JsSuccess(appointmentEmailerObj, _) =>

        try {

          val addAppointment = AddHospitalAppointmentDao.sendAppointmentMail(appointmentEmailerObj)

          Ok(Json.toJson(addAppointment))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendSonographerinfoEmail = Action(parse.json) { implicit request =>

    request.body.validate[sonographerinfoObj] match {
      case JsSuccess(sonographerinfoObj, _) =>

        try {

          val addAppointment = AddHospitalAppointmentDao.sendsonographerMail(sonographerinfoObj)

          Ok(Json.toJson(addAppointment))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendSupportworkerinfoEmail = Action(parse.json) { implicit request =>

    request.body.validate[sonographerinfoObj] match {
      case JsSuccess(sonographerinfoObj, _) =>

        try {

          val addAppointment = AddHospitalAppointmentDao.sendsupportworkerMail(sonographerinfoObj)

          Ok(Json.toJson(addAppointment))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def dailysonographerInfoSchedule(id: Integer) = Action { implicit request =>

    try {
      println("dailySupportworkerinfoSchedule method ")
      //val responseList = AddHospitalAppointmentDao.sendsonographerInfo()
      val responseList = AddHospitalAppointmentDao.sendsupportworkerInfo()

      Ok(Json.toJson(responseList))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }

  }

  def sendPatientDeclinedEmail = Action(parse.json) { implicit request =>

    request.body.validate[appointmentEmailerObj] match {
      case JsSuccess(appointmentEmailerObj, _) =>

        try {

          val addAppointment = AddHospitalAppointmentDao.sendPatientDelinedMail(appointmentEmailerObj, Some(0L))

          Ok(Json.toJson(addAppointment))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendContactUsEmail = Action(parse.json) { implicit request =>

    request.body.validate[appointmentEmailerObj] match {
      case JsSuccess(appointmentEmailerObj, _) =>

        try {

          val contactUs = AddHospitalAppointmentDao.sendContactUs(appointmentEmailerObj, Some(0L))

          Ok(Json.toJson(contactUs))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendExclusionEmail = Action(parse.json) { implicit request =>

    request.body.validate[appointmentEmailerObj] match {
      case JsSuccess(appointmentEmailerObj, _) =>

        try {

          val exclusion = AddHospitalAppointmentDao.sendDwExclusionMail(appointmentEmailerObj, Some(0L))

          Ok(Json.toJson(exclusion))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendPatientWithdrawalMail = Action(parse.json) { implicit request =>

    request.body.validate[appointmentEmailerObj] match {
      case JsSuccess(appointmentEmailerObj, _) =>
        try {
          val withdrawal = AddHospitalAppointmentDao.sendPatientWithdrawalMail(appointmentEmailerObj, Some(0L))
          Ok(Json.toJson(withdrawal))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendNoContactToGP = Action(parse.json) { implicit request =>

    request.body.validate[appointmentEmailerObj] match {
      case JsSuccess(appointmentEmailerObj, _) =>

        try {

          val noContact = AddHospitalAppointmentDao.sendNoContactToGP(appointmentEmailerObj, Some(0L))

          Ok(Json.toJson(noContact))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendPatientDNAMail = Action(parse.json) { implicit request =>

    request.body.validate[appointmentEmailerObj] match {
      case JsSuccess(appointmentEmailerObj, _) =>

        try {

          val dnaMail = AddHospitalAppointmentDao.sendPatientDNAMail(appointmentEmailerObj, Some(0L))

          Ok(Json.toJson(dnaMail))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def sendUrgentNoContactToGP = Action(parse.json) { implicit request =>

    request.body.validate[appointmentEmailerObj] match {
      case JsSuccess(appointmentEmailerObj, _) =>

        try {

          val urgentNoContact = AddHospitalAppointmentDao.sendUrgentNoContactToGP(appointmentEmailerObj, Some(0L))

          Ok(Json.toJson(urgentNoContact))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def sendFileAttachment = Action(parse.json) { implicit request =>
    request.body.validate[sendFileAttachmentObj] match {
      case JsSuccess(sendFileAttachmentObj, _) =>
        try {
          val sendAttchment = AddHospitalAppointmentDao.sendAttachment(sendFileAttachmentObj.responseId, sendFileAttachmentObj.PracticeID.getOrElse(0))
          Ok(Json.toJson(sendAttchment))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def sendAppointmentdetailsMail = Action(parse.json) { implicit request =>

    request.body.validate[sendAppointmentmailObj] match {
      case JsSuccess(sendAppointmentmailObj, _) =>
        try {
          val sendAppointment = AddHospitalAppointmentDao.appointmentdetailstoscancenter(sendAppointmentmailObj.hospitalid, sendAppointmentmailObj.sonographerid, sendAppointmentmailObj.addressconsultid, sendAppointmentmailObj.scheduledate)
          Ok(Json.toJson(sendAppointment))
        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }
      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def appointmentdetailstoscancenter(id: Integer) = Action { implicit request =>

    try {

      val senddetails = AddHospitalAppointmentDao.sendappointmentdetailstoscancenter(id)
      Ok(Json.toJson(senddetails))

    } catch {

      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))

    }
  }

}  
