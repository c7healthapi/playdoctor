package controllers.doctors.API.Doctor

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.Json.Request.addDtAppointmentDetailsObj
import models.com.hcue.doctors.Json.Request.updateDtAppointmentDetailsObj
import models.com.hcue.doctors.Json.Request.updateDtAppointmentStatusObj
import models.com.hcue.doctors.traits.add.appointment.AddAppointmentDao
import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao
import models.com.hcue.doctors.traits.update.UpdateDoctorDao
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object AppointmentAPI extends Controller {

  def addAppointments = Action(parse.json) { implicit request =>

    request.body.validate[addDtAppointmentDetailsObj] match {
      case JsSuccess(addDtAppointmentDetailsObj, _) =>

        try {

          val addAppointment = AddAppointmentDao.addAppointments(addDtAppointmentDetailsObj)

          if (addAppointment._2 == 0) {
            Ok(Json.toJson(addAppointment._1))
          } else {

            val bookedAppoiment = ListDoctorDao.appointmentStatus(addAppointment._2)

            Ok(Json.toJson(bookedAppoiment))

          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }

  def updateAppointments = Action(parse.json) { implicit request =>

    request.body.validate[updateDtAppointmentDetailsObj] match {
      case JsSuccess(updateDtAppointmentDetailsObj, _) =>

        try {

          val updateAppointment = UpdateDoctorDao.updateAppointments(updateDtAppointmentDetailsObj)
          if (updateAppointment._2 != 0) { // If Invalid Appointment number entered
            val bookedStatus = ListDoctorDao.appointmentStatus(updateAppointment._2)
            Ok(Json.toJson(bookedStatus))
          } else {
            Ok(Json.toJson(updateAppointment._1))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))

        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def updateAppointmentStatus = Action(parse.json) { implicit request =>

    request.body.validate[updateDtAppointmentStatusObj] match {
      case JsSuccess(updateDtAppointmentStatusObj, _) =>

        try {
          var updateAppointment = UpdateDoctorDao.updateAppointmentStatus(updateDtAppointmentStatusObj)

          val bookedAppoiment = ListDoctorDao.appointmentStatus(updateDtAppointmentStatusObj.AppointmentID)

          if (updateAppointment != None && updateDtAppointmentStatusObj.AppointmentStatus == "C") { // If Invalid Appointment number entered
            val bookedStatus = ListDoctorDao.appointmentStatus(updateAppointment.get.AppointmentID)
          }

          if (updateAppointment == None) {
            Ok(Json.toJson("-Failed-"))
          } else {
            Ok(Json.toJson("-Success-"))
          }

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))

    }
  }

  def appointmentStatus(id: Long) = Action { implicit request =>

    try {

      val doctor = ListDoctorDao.appointmentStatus(id)
      Ok(Json.toJson(doctor))

    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }

  }

}  
