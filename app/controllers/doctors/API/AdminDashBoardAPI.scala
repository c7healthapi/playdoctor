package controllers.doctors.API

import models.com.hcue.doctors.Bean.ErrorMessageBean
import models.com.hcue.doctors.DAO.Doctor.AdminDashBoardDao
import models.com.hcue.doctors.Json.Request.AdminDashboardInfoRequestObj
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object AdminDashBoardAPI extends Controller {

  def getAdminDashBoardInfo = Action(parse.json) { implicit request =>

    request.body.validate[AdminDashboardInfoRequestObj] match {
      case JsSuccess(adminDashboardInfoRequestObj, _) =>

        try {

          val dashboardInfoDetails = AdminDashBoardDao.adminDashBoardDetails(adminDashboardInfoRequestObj)
          Ok(Json.toJson(dashboardInfoDetails))

        } catch {

          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) =>
        val status = ErrorMessageBean.onBadRequest(request, Json.toJson(request.body), JsError.toFlatJson(errors))
        BadRequest(JsError.toFlatJson(errors))
    }
  }
} 
