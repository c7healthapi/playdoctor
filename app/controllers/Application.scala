package controllers

import play.api._
import play.api.mvc._

import models._

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Doctor APIs - application is ready."))
  }

  def getCookies = Action {
    Ok("Success")
  }

  def options(path: String) = Action { Ok("") }

}