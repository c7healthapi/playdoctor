package controllers.doctor.API.Careteam

import models.com.hcue.doctor.Json.Request.Careteam.getCcgroupsObj
import models.com.hcue.doctor.Json.Request.Careteam.specialityLkupDetailsObj
import models.com.hcue.doctor.traits.add.Careteam.CcgroupsDao
import models.com.hcue.doctor.traits.add.Careteam.SpecialityLkupDao
import models.com.hcue.doctors.Bean.ErrorMessageBean
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctor.Json.Request.Careteam.SpecialityCtgyDetailsObj


object SpecialityLkupAPI extends Controller {

  def addspeciality = Action(parse.json) { implicit request =>

    request.body.validate[specialityLkupDetailsObj] match {
      case JsSuccess(specialityLkupDetailsObj, _) =>

        try {
          val specialityval = SpecialityLkupDao.addSpecialityWithSubSpeciality(specialityLkupDetailsObj)
          Ok(Json.toJson(specialityval))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def addSpecialityCategory = Action(parse.json) { implicit request =>
    request.body.validate[SpecialityCtgyDetailsObj] match {
      case JsSuccess(specialityCtgyDetailsObj, _) =>

        try {
          val specialityval2 = SpecialityLkupDao.addSpecialityCategory(specialityCtgyDetailsObj)
          Ok(Json.toJson(specialityval2))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }
    def getSpecialityCategoryList(hospitalID : Long) = Action { implicit request =>

    try {
      val lookups = SpecialityLkupDao.getSpecialityCategoryList(hospitalID)
      Ok(Json.toJson(lookups))
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))
    }
  }
   
     def updateSpeciality = Action(parse.json) { implicit request =>

    request.body.validate[specialityLkupDetailsObj] match {
      case JsSuccess(specialityLkupDetailsObj, _) =>

        try {
          val specialityval = SpecialityLkupDao.updatespeciality(specialityLkupDetailsObj)
          Ok(Json.toJson(specialityval))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def specialitylist(Active: Boolean, hospitalId: Long) = Action { implicit request =>
    try {

      val doctorList = SpecialityLkupDao.getDoctorList(hospitalId)
      Ok(Json.toJson(doctorList))

    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = "Error"
        Ok(Json.toJson(status))
    }
  }
  
    def specialitylistv1(Active: Boolean, hospitalId: Long) = Action { implicit request =>
    try {

      val doctorList = SpecialityLkupDao.getDoctorSpecialityList_v1(hospitalId)
      Ok(Json.toJson(doctorList))

    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = "Error"
        Ok(Json.toJson(status))
    }
  }

  def listCcgroupsInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCcgroupsObj] match {
      case JsSuccess(getCcgroupsObj, _) =>
        try {

          val CcgroupsList = CcgroupsDao.listCcgroups(getCcgroupsObj)
          Ok(Json.toJson(CcgroupsList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

}  
