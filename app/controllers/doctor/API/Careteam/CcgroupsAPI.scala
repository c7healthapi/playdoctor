package controllers.doctor.API.Careteam

import models.com.hcue.doctor.Json.Request.Careteam.addCcgroupsObj
import models.com.hcue.doctor.Json.Request.Careteam.getCcgroupsObj
import models.com.hcue.doctor.traits.add.Careteam.CcgroupsDao
import models.com.hcue.doctors.Bean.ErrorMessageBean
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object CcgroupsAPI extends Controller {


  def addupdateCcgroups = Action(parse.json) { implicit request =>

    request.body.validate[addCcgroupsObj] match {
      case JsSuccess(addCcgroupsObj, _) =>

        try {

          val Ccgroupsval = CcgroupsDao.addUpdateCcgroups(addCcgroupsObj)
          Ok(Json.toJson(Ccgroupsval))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listCcgroupsInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCcgroupsObj] match {
      case JsSuccess(getCcgroupsObj, _) =>
        try {

          val CcgroupsList = CcgroupsDao.listCcgroups(getCcgroupsObj)
          Ok(Json.toJson(CcgroupsList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listAllCcgroupsInfo = Action(parse.json) { implicit request =>
    request.body.validate[getCcgroupsObj] match {
      case JsSuccess(getCcgroupsObj, _) =>
        try {

          val CcgroupsList = CcgroupsDao.listAllCcgroups(getCcgroupsObj)
          Ok(Json.toJson(CcgroupsList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

}  
