package controllers.doctor.API.Careteam

import models.com.hcue.doctor.Json.Request.Careteam.addPracticeObj
import models.com.hcue.doctor.Json.Request.Careteam.getPostcodehospitalObj
import models.com.hcue.doctor.Json.Request.Careteam.getPracticeObj
import models.com.hcue.doctor.Json.Request.Careteam.getRejectReqObj
import models.com.hcue.doctor.Json.Request.Careteam.getdeactivePracticeObj
import models.com.hcue.doctor.traits.add.Careteam.PracticeDao
import models.com.hcue.doctors.Bean.ErrorMessageBean
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import models.com.hcue.doctors.traits.SMSContent
import models.com.hcue.doctor.Json.Request.Careteam.SmsRequestValidation.appointmentremindersmsObj



object PracticeAPI extends Controller {

  def addupdatePractice = Action(parse.json) { implicit request =>

    request.body.validate[addPracticeObj] match {
      case JsSuccess(addPracticeObj, _) =>

        try {

          val Practiceval = PracticeDao.addUpdatePractice(addPracticeObj)
          Ok(Json.toJson(Practiceval))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def deactivedoctorPractice = Action(parse.json) { implicit request =>

    request.body.validate[getdeactivePracticeObj] match {
      case JsSuccess(getdeactivePracticeObj, _) =>

        try {

          val Practiceval = PracticeDao.deactivePractice(getdeactivePracticeObj)
          Ok(Json.toJson(Practiceval))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def doctorPractices(id: Long) = Action { implicit request =>

    try {
      val result = PracticeDao.listdoctorPractice(id)
      Ok(Json.toJson(result))

    } catch {

      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))

    }
  }

  def doctorhospital(id: Long) = Action { implicit request =>

    try {
      val result = PracticeDao.listdoctorScancenter(id)
      Ok(Json.toJson(result))

    } catch {

      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))

    }
  }
  
  
    def getpostcodedetails(postcode: String) = Action { implicit request =>
    try {
      val postcodeval = postcode.replaceAll("\\s", "")
      val result = PracticeDao.listpostcodedetail(postcodeval)
      Ok(Json.toJson(result))

    } catch {

      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onError(request, e, None)
        Ok(Json.toJson(status))

    }
  }

  def appointmentremindersms = Action(parse.json) { implicit request =>
    request.body.validate[appointmentremindersmsObj] match {
      case JsSuccess(appointmentremindersmsObj, _) =>
        try {

          //val HospitalList = PracticeDao.appointmentremindersmssend(appointmentremindersmsObj)
          val HospitalList = SMSContent.sendappointmentconfirmation(appointmentremindersmsObj)
          
          Ok(Json.toJson(HospitalList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listPostcodehospitalsearch = Action(parse.json) { implicit request =>
    request.body.validate[getPostcodehospitalObj] match {
      case JsSuccess(getPostcodehospitalObj, _) =>
        try {

          val HospitalList = PracticeDao.listpostcodehospital(getPostcodehospitalObj)
          Ok(Json.toJson(HospitalList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listRejectPatients = Action(parse.json) { implicit request =>
    request.body.validate[getRejectReqObj] match {
      case JsSuccess(getRejectReqObj, _) =>
        try {

          val rejectList = PracticeDao.listRejectedPatients(getRejectReqObj)
          Ok(Json.toJson(rejectList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listPracticeInfo = Action(parse.json) { implicit request =>
    request.body.validate[getPracticeObj] match {
      case JsSuccess(getPracticeObj, _) =>
        try {

          val PracticeList = PracticeDao.listPractice(getPracticeObj)
          Ok(Json.toJson(PracticeList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listAllPracticeInfo = Action(parse.json) { implicit request =>
    request.body.validate[getPracticeObj] match {
      case JsSuccess(getPracticeObj, _) =>
        try {

          val CcgroupsList = PracticeDao.listAllPractice(getPracticeObj)
          Ok(Json.toJson(CcgroupsList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

}  
