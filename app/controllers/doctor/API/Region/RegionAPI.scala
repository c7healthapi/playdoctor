package controllers.doctor.API.Region

import models.com.hcue.doctor.Json.Request.AddUpdateRegion.addRegionObj
import models.com.hcue.doctor.Json.Request.AddUpdateRegion.getRegionObj
import models.com.hcue.doctor.traits.add.Region.RegionDao
import models.com.hcue.doctors.Bean.ErrorMessageBean
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller

object RegionAPI extends Controller {

  def addupdateRegion = Action(parse.json) { implicit request =>

    request.body.validate[addRegionObj] match {
      case JsSuccess(addRegionObj, _) =>

        try {

          val Regionval = RegionDao.addUpdateRegion(addRegionObj)
          Ok(Json.toJson(Regionval))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listRegionInfo = Action(parse.json) { implicit request =>
    request.body.validate[getRegionObj] match {
      case JsSuccess(getRegionObj, _) =>
        try {

          val regionList = RegionDao.listRegion(getRegionObj)
          Ok(Json.toJson(regionList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

  def listAllRegionInfo = Action(parse.json) { implicit request =>
    request.body.validate[getRegionObj] match {
      case JsSuccess(getRegionObj, _) =>
        try {

          val regionList = RegionDao.listAllRegion(getRegionObj)
          Ok(Json.toJson(regionList))

        } catch {
          case e: Exception =>
            e.printStackTrace()
            val status = ErrorMessageBean.onError(request, e, Some(Json.toJson(request.body)))
            Ok(Json.toJson(status))
        }

      case JsError(errors) => BadRequest(JsError.toFlatJson(errors))

    }
  }

}  
