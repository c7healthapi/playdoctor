package models.com.hcue.doctor.traits.add.Region

import java.text.SimpleDateFormat
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.StaticQuery
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.interpolation
import java.util.Calendar
import play.Logger
import play.api.db.slick.DB
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctor.Json.Request.AddUpdateRegion.addRegionObj
import models.com.hcue.doctor.Json.Request.AddUpdateRegion.listRegionObj
import models.com.hcue.doctor.Json.Request.AddUpdateRegion.listAllRegionObj
import models.com.hcue.doctor.Json.Request.AddUpdateRegion.addSlaRecord
import models.com.hcue.doctor.Json.Request.AddUpdateRegion.getRegionObj
import models.com.hcue.doctor.slick.transactTable.region.RegionLkup
import models.com.hcue.doctor.slick.transactTable.region.TRegionLkup
import models.com.hcue.doctor.slick.transactTable.Careteam.ServiceLevelAgreement
import models.com.hcue.doctor.slick.transactTable.Careteam.TServiceLevelAgreement
import play.api.libs.json.JsValue
import play.api.libs.json
import scala.util.parsing.json._
import play.api.libs.json.Json
import models.com.hcue.doctor.Json.Request.AddUpdateRegion.ExpiredSlaRecordObj
import models.com.hcue.doctors.helper.CalenderDateConvertor

object RegionDao {

  val region = RegionLkup.RegionLkup
  val servicelevelagreement = ServiceLevelAgreement.ServiceLevelAgreement

  def addUpdateRegion(a: addRegionObj): String = db withSession { implicit session =>
    var regionId: Long = 0
    var slaId: Long = 0

    if (a.regionid != None) {
      val insertedRecord: Option[TRegionLkup] = region.filter { x => x.regionid === a.regionid.get }.firstOption
      if (insertedRecord.nonEmpty) {
        regionId = a.regionid.get
        region.filter { x => x.regionid === a.regionid.get }.map { x => (x.regionname, x.contactperson, x.contactnumber, x.email, x.postcode, x.active, x.updtusr, x.updtusrtype) }.update(a.regionname, a.contactperson,
          a.contactnumber, a.email, a.postcode, a.active, Some(a.usrid), Some(a.usrtype))
       
        val updateActiveFlag = activeInactiveReferralteams(a.regionid.get, 0L, 0L, a.active.getOrElse(true)).first      
      }
    } else {
      regionId = (region.map { x =>
        (x.regionname, x.contactperson, x.contactnumber, x.email, x.postcode, x.active,
          x.crtusr, x.crtusrtype, x.hospitalcd)
      } returning region.map(_.regionid)) += (a.regionname, a.contactperson, a.contactnumber,
        a.email, a.postcode, a.active, a.usrid, a.usrtype, a.hospitalcd)
    }

    if (a.SlaRecord != None) {
      var slarecord: addSlaRecord = a.SlaRecord.get

      if (slarecord.regionid != None) {
        regionId = slarecord.regionid.get

      }

      if (slarecord.slaid != None) {
        println("come to update")
        val slainsertedRecord: Option[TServiceLevelAgreement] = servicelevelagreement.filter { x => x.slaid === slarecord.slaid.get && x.regionid === regionId }.firstOption
        if (slainsertedRecord.nonEmpty) {
          slaId = slarecord.slaid.get
          servicelevelagreement.filter { x => x.slaid === slaId && x.regionid === regionId }.map { x => (x.routinereftriage, x.routinerefscan, x.routineappsendback, x.urgentreftriage, x.urgentrefscan, x.urgentrefdatebook, x.urgentscanrepsendback, x.dvtreftriage, x.dvtrefscan, x.dvtrefdatebook, x.dvtscanrepsendback, x.expiredstatus, x.regionid, x.slavalidfrom, x.slavalidto, x.updtusr, x.updtusrtype) }.update(slarecord.routinereftriage, slarecord.routinerefscan, slarecord.routineappsendback, slarecord.urgentreftriage, slarecord.urgentrefscan, slarecord.urgentrefdatebook, slarecord.urgentscanrepsendback, slarecord.dvtreftriage, slarecord.dvtrefscan, slarecord.dvtrefdatebook, slarecord.dvtscanrepsendback, slarecord.expiredstatus, regionId, slarecord.slavalidfrom, slarecord.slavalidto, Some(a.usrid), Some(a.usrtype))

        }
      } else {

        println("come to insert")

        slaId = (servicelevelagreement.map { x =>
          (x.routinereftriage, x.routinerefscan, x.routineappsendback, x.urgentreftriage, x.urgentrefscan, x.urgentrefdatebook, x.urgentscanrepsendback, x.dvtreftriage, x.dvtrefscan, x.dvtrefdatebook, x.dvtscanrepsendback, x.expiredstatus, x.regionid, x.slavalidfrom, x.slavalidto,
            x.crtusr, x.crtusrtype)
        } returning servicelevelagreement.map(_.slaid)) += (slarecord.routinereftriage, slarecord.routinerefscan, slarecord.routineappsendback, slarecord.urgentreftriage, slarecord.urgentrefscan, slarecord.urgentrefdatebook, slarecord.urgentscanrepsendback, slarecord.dvtreftriage, slarecord.dvtrefscan, slarecord.dvtrefdatebook, slarecord.dvtscanrepsendback, slarecord.expiredstatus, regionId, slarecord.slavalidfrom, slarecord.slavalidto, a.usrid, a.usrtype)
      }

    }

    "Success"
  }

  def listRegion(a: getRegionObj): Array[listRegionObj] = db withSession { implicit session =>

    val Record = if (a.regionid != None && a.regionid.get != 0) {
      region.filter { x => x.regionid === a.regionid.get  && x.hospitalcd === a.hospitalcd}.list
    } else {
      
      region.filter { x => x.hospitalcd === a.hospitalcd}.sortBy { x => x.regionid }.list

    }

    var resultSet: Array[listRegionObj] = new Array[listRegionObj](Record.length);
    var Slarecord: List[TServiceLevelAgreement] = Nil;

    for (i <- 0 until Record.length) {

      val currentDate: java.sql.Date = new java.sql.Date(CalenderDateConvertor.getISDDate().getTime())

      val record = if (a.regionid != None && a.regionid.get != 0) {
        Slarecord = servicelevelagreement.filter { x => x.regionid === a.regionid.get && x.slavalidto >= currentDate }.list
      } else {
        Slarecord = servicelevelagreement.filter { x => x.regionid === Record(i).regionid && x.slavalidto >= currentDate }.list
      }

      val slaresultSet: Array[addSlaRecord] = new Array[addSlaRecord](Slarecord.length)

      for (i <- 0 until Slarecord.length) {
        slaresultSet(i) = new addSlaRecord(Some(Slarecord(i).slaid), Slarecord(i).routinereftriage, Slarecord(i).routinerefscan, Slarecord(i).routineappsendback, Slarecord(i).urgentreftriage, Slarecord(i).urgentrefscan, Slarecord(i).urgentrefdatebook, Slarecord(i).urgentscanrepsendback, Slarecord(i).dvtreftriage, Slarecord(i).dvtrefscan, Slarecord(i).dvtrefdatebook, Slarecord(i).dvtscanrepsendback, Slarecord(i).expiredstatus, Some(Slarecord(i).regionid), Slarecord(i).slavalidfrom, Slarecord(i).slavalidto)
      }

      val ExpiredSlarecord = if (a.regionid != None && a.regionid.get != 0) {
        servicelevelagreement.filter { x => x.regionid === a.regionid.get && x.slavalidto < currentDate }.list
      } else {
        servicelevelagreement.filter { x => x.regionid === Record(i).regionid && x.slavalidto < currentDate }.list
      }

      val ExpiredresultSet: Array[ExpiredSlaRecordObj] = new Array[ExpiredSlaRecordObj](ExpiredSlarecord.length)

      for (i <- 0 until ExpiredSlarecord.length) {
        ExpiredresultSet(i) = new ExpiredSlaRecordObj(Some(ExpiredSlarecord(i).slaid), ExpiredSlarecord(i).routinereftriage, ExpiredSlarecord(i).routinerefscan, ExpiredSlarecord(i).routineappsendback, ExpiredSlarecord(i).urgentreftriage, ExpiredSlarecord(i).urgentrefscan, ExpiredSlarecord(i).urgentrefdatebook, ExpiredSlarecord(i).urgentscanrepsendback, ExpiredSlarecord(i).dvtreftriage, ExpiredSlarecord(i).dvtrefscan, ExpiredSlarecord(i).dvtrefdatebook, ExpiredSlarecord(i).dvtscanrepsendback, ExpiredSlarecord(i).expiredstatus, Some(ExpiredSlarecord(i).regionid), ExpiredSlarecord(i).slavalidfrom, ExpiredSlarecord(i).slavalidto)
      }

      resultSet(i) = new listRegionObj(Some(Record(i).regionid), Record(i).regionname, Record(i).contactperson, Record(i).contactnumber, Record(i).email, Record(i).postcode, Record(i).active, Some(slaresultSet), Some(ExpiredresultSet))

    }

    return resultSet
  }

  
  def activeInactiveReferralteams(regionid: Long, ccgid: Long, practiceid: Long, activeflag: Boolean) = sql"select * from inactivereferralteams('#$regionid', '#$ccgid', '#$practiceid', '#$activeflag')".as[(Boolean)]

  def allregionQuery(regionid: Long, usrid: Long, pageNumber: Int, pageSize: Int, hospitalcd: String, searchtext: String) = sql"select * from getallregionval('#$regionid', '#$usrid', '#$pageNumber', '#$pageSize', '#$hospitalcd', '#$searchtext')".as[(Long, String, String, String, String, String, Boolean, java.sql.Date, String)]

  def listAllRegion(a: getRegionObj): Array[listAllRegionObj] = db withSession { implicit session =>

    val allregionList = allregionQuery(1, 1, a.pageNumber.getOrElse(1), a.pageSize.getOrElse(20), a.hospitalcd.getOrElse(""), a.searchtext.getOrElse("")).list

    val resultSet: Array[listAllRegionObj] = new Array[listAllRegionObj](allregionList.length)

    for (i <- 0 until resultSet.length) {

      var json1: Option[JsValue] = None

      if (allregionList(i)._9 != None) {
        json1 = Some(Json.parse(allregionList(i)._9))
      }

      resultSet(i) = new listAllRegionObj(Some(allregionList(i)._1), allregionList(i)._2, Some(allregionList(i)._3), Some(allregionList(i)._4), Some(allregionList(i)._5), Some(allregionList(i)._6), Some(allregionList(i)._7),
        if (allregionList(i)._8 != null) {
          Some(allregionList(i)._8)
        } else { None }, json1)

    }

    return resultSet

  }

}

