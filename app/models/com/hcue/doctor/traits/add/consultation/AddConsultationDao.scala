package models.com.hcue.doctor.traits.add.consultation

import scala.slick.driver.PostgresDriver.simple.booleanColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.productQueryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.queryToInsertInvoker
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn

import models.com.hcue.doctor.Json.Request.AddUpdateConsultation.addConsultationObj
import models.com.hcue.doctor.Json.Request.AddUpdateConsultation.addConsultationRecord
import models.com.hcue.doctor.Json.Request.AddUpdateConsultation.validateConsultationRecord
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorConsultations
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorConsultation
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalConsultation
import models.com.hcue.doctors.slick.transactTable.hospitals.THospitalConsultation
import models.com.hcue.doctors.traits.dbProperties.db
import play.Logger

object AddConsultationDao {

  val log = Logger.of("application")

  val doctorConsultations = DoctorConsultations.DoctorConsultations

  val doctorsAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

  val hospitalConsultation = HospitalConsultation.HospitalConsultation

  import scala.util.control.Breaks._
  def addValidateConsultation(DoctorID: Long, AddressID: Long, consultationRecord: Array[addConsultationRecord]): (Boolean, String) = db withSession { implicit session =>

    val storedData: List[TDoctorConsultation] = doctorConsultations.filter(c => c.AddressID === AddressID && c.DoctorID === DoctorID).list

    var storedLst: Array[validateConsultationRecord] = new Array[validateConsultationRecord](storedData.length)

    if (storedData.length > 0) {

      for (i <- 0 until storedData.length) {
        val startTime = (storedData(i).StartTime.replaceAll(":", "").toString()).toInt
        val endTime = (storedData(i).EndTime.replaceAll(":", "").toString()).toInt
        storedLst(i) = new validateConsultationRecord(storedData(i).DayCD, startTime, endTime, storedData(i).MinPerCase, storedData(i).Fees, storedData(i).Active)
      }
      //storedLst = storedLst.sortBy { x => x.StartTime }

    }

    var status = true
    var dtCd = ""
    var tempLst: Array[validateConsultationRecord] = new Array[validateConsultationRecord](consultationRecord.length)

    if (consultationRecord.length > 0) {

      for (i <- 0 until consultationRecord.length) {
        val startTime = (consultationRecord(i).StartTime.replaceAll(":", "").toString()).toInt
        val endTime = (consultationRecord(i).EndTime.replaceAll(":", "").toString()).toInt
        tempLst(i) = new validateConsultationRecord(consultationRecord(i).DayCD, startTime, endTime, consultationRecord(i).MinPerCase, consultationRecord(i).Fees, consultationRecord(i).Active)
      }
      //tempLst = tempLst.sortBy { x => x.StartTime }

    }

    var resultset = storedLst.toList ::: tempLst.toList

    resultset = resultset.filter { x => x.Active == Some("Y") }.sortBy { x => x.StartTime }

    log.info("Resultset : " + resultset)

    breakable {

      val dayCode = resultset.map { x => x.DayCD }.distinct.toList

      for (j <- 0 until dayCode.length) {

        val dayCodeList = resultset.filter { x => x.DayCD == dayCode(j) }.toList
        if (dayCodeList.length > 0) {

          for (i <- 1 until dayCodeList.length) {

            if (dayCodeList(i).StartTime.toInt < dayCodeList(i - 1).EndTime.toInt) {
              log.info("Start Time : " + dayCodeList(i).StartTime)
              log.info("End Time : " + dayCodeList(i - 1).EndTime)

              status = false
              dtCd = dayCode(j)
              break
            }

          }

        }

      }

    }
    return (status, dtCd)
  }

  def addConsultation(addConsultObj: addConsultationObj) = db withTransaction { implicit session =>

    // Check for record in tPatientPharma
    // val doctorConsultationAddress:List[TDoctorConsultation] =  doctorConsultations.filter(c => c.AddressID === addConsultObj.AddressID && c.DoctorID === addConsultObj.DoctorID)

    if (addConsultObj.consultationRecord.length > 0) {
      doctorsAddressExtn.filter(c => c.AddressID === addConsultObj.AddressID && c.DoctorID === addConsultObj.DoctorID)
        .map(x => x.MinPerCase).update(addConsultObj.consultationRecord(0).MinPerCase)
    }

    addConsultObj.consultationRecord.foreach { x =>

      val doctorConsultationDetails: addConsultationRecord = x

      var minpercase = doctorConsultationDetails.MinPerCase

      //Some(doctorConsultationDetails.MinPerCase.getOrElse(10))
      val mappedValue: TDoctorConsultation = new TDoctorConsultation(0, addConsultObj.AddressID, addConsultObj.DoctorID,
        doctorConsultationDetails.DayCD, doctorConsultationDetails.StartTime, doctorConsultationDetails.EndTime, Some(minpercase.getOrElse(10)),
        Some(doctorConsultationDetails.Fees.getOrElse(0)), doctorConsultationDetails.Active, addConsultObj.USRId, addConsultObj.USRType, null, null)

      val addressConsultID = (doctorConsultations returning doctorConsultations.map(_.AddressConsultID)) += mappedValue

    }

  }

  def addHiddenConsultation(doctorId: Long, addressId: Long, dayCd: String, startTime: String, endTime: String, minPerCase: Int, crtUsr: Long, crtUsrType: String): TDoctorConsultation = db withTransaction { implicit session =>

    val addressConsultID = (doctorConsultations.map { x =>
      (x.AddressConsultID, x.AddressID, x.DoctorID, x.DayCD, x.StartTime, x.EndTime, x.Active, x.MinPerCase, x.CrtUSR,
        x.CrtUSRType)
    } returning doctorConsultations.map(_.AddressConsultID)) += (0, addressId, doctorId, dayCd, startTime, endTime, Some("H"), Some(minPerCase), crtUsr, crtUsrType)

    new TDoctorConsultation(addressConsultID, addressId, doctorId, dayCd, startTime, endTime, Some(minPerCase), Some(0), Some("H"), crtUsr, crtUsrType, None, None)

  }

  def addHiddenHospitalConsultation(hospitalId: Long, dayCd: String, startTime: String, endTime: String, minPerCase: Int, crtUsr: Long, crtUsrType: String): THospitalConsultation = db withTransaction { implicit session =>

    val addressConsultID = (hospitalConsultation.map { x =>
      (x.AddressConsultID, x.HospitalID, x.DayCD, x.StartTime, x.EndTime, x.Active, x.MinPerCase, x.CrtUSR,
        x.CrtUSRType)
    } returning hospitalConsultation.map(_.AddressConsultID)) += (0, hospitalId, dayCd, startTime, endTime, Some("H"), Some(minPerCase), crtUsr, crtUsrType)

    new THospitalConsultation(addressConsultID, hospitalId, dayCd, startTime, endTime, Some(minPerCase), Some(0), Some("H"), crtUsr, crtUsrType, None, None)

  }

}