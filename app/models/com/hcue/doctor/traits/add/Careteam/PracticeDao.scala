package models.com.hcue.doctor.traits.add.Careteam

import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.booleanOptionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnToOrdered
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.productQueryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.queryToInsertInvoker
import scala.slick.driver.PostgresDriver.simple.queryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn
import scala.slick.jdbc.StaticQuery.interpolation

import models.com.hcue.doctor.Json.Request.Careteam.addPracticeObj
import models.com.hcue.doctor.Json.Request.Careteam.addressdetl
import models.com.hcue.doctor.Json.Request.Careteam.emaildetl
import models.com.hcue.doctor.Json.Request.Careteam.getPostcodehospitalObj
import models.com.hcue.doctor.Json.Request.Careteam.getPracticeObj
import models.com.hcue.doctor.Json.Request.Careteam.getRejectListObj
import models.com.hcue.doctor.Json.Request.Careteam.getRejectReqObj
import models.com.hcue.doctor.Json.Request.Careteam.getdeactivePracticeObj
import models.com.hcue.doctor.Json.Request.Careteam.listAllPracticeObj
import models.com.hcue.doctor.Json.Request.Careteam.listPracticeObj
import models.com.hcue.doctor.Json.Request.Careteam.listdoctorPracticeObj
import models.com.hcue.doctor.Json.Request.Careteam.listpostcodehospitalObj
import models.com.hcue.doctor.Json.Request.Careteam.phonedetail
import models.com.hcue.doctor.slick.transactTable.Careteam.DoctorPracties
import models.com.hcue.doctor.slick.transactTable.Careteam.PracticeExtnLkup
import models.com.hcue.doctor.slick.transactTable.Careteam.PracticeLkup
import models.com.hcue.doctor.slick.transactTable.Careteam.TPracticeExtnLkup
import models.com.hcue.doctor.slick.transactTable.Careteam.TPracticeLkup
import models.com.hcue.doctor.slick.transactTable.doctor.ExtnDoctor
import models.com.hcue.doctors.traits.dbProperties.db
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import models.com.hcue.doctor.Json.Request.Careteam.appointmentremindersmsObj

object PracticeDao {

  val Practice = PracticeLkup.PracticeLkup
  val PracticeExtn = PracticeExtnLkup.PracticeExtnLkup
  val doctorpractiesdetails = DoctorPracties.DoctorPracties
  val extndoctor = ExtnDoctor.ExtnDoctor

  def addUpdatePractice(a: addPracticeObj): String = db withSession { implicit session =>
    var PracticeId: Long = 0
    var address1 = a.addressdetl.get.address1
    var address2 = a.addressdetl.get.address2
    var address3 = a.addressdetl.get.address3
    var address4 = a.addressdetl.get.address4

    var practiceemail = a.emaildetl.get.practiceemail
    var repadminemail = a.emaildetl.get.repadminemail
    var appadminemail = a.emaildetl.get.appadminemail

    if (a.practiceid != None) {
      val insertedRecord: Option[TPracticeLkup] = Practice.filter { x => x.practiceid === a.practiceid.get }.firstOption
      if (insertedRecord.nonEmpty) {
        PracticeId = a.practiceid.get

        Practice.filter { x => x.practiceid === a.practiceid.get }.map { x =>
          (x.ccgid, x.practicename, x.practicecode, x.postcode,
            x.address1, x.address2, x.address3, x.address4, x.citytown, x.county, x.country, x.description, x.updtusr)
        }.update(a.ccgid, a.practicename, a.practicecode, a.postcode,
          address1, address2, address3, address4, a.citytown, a.county, a.country, a.description, Some(a.usrid))

      }

      val insertedRecord2: Option[TPracticeExtnLkup] = PracticeExtn.filter { x => x.practiceid === a.practiceid.get }.firstOption
      if (insertedRecord2.nonEmpty) {
        PracticeId = a.practiceid.get

        PracticeExtn.filter { x => x.practiceid === a.practiceid.get }.map { x =>
          (x.ccgid, x.faxno, x.telephoneday1, x.telephoneday2, x.practiceemail,
            x.repadminemail, x.appadminemail, x.acceptsfaxmsg, x.odscode, x.autopostcom, x.practicepopulation, x.meshidentifier, x.active)
        }.update(a.ccgid, a.faxno, a.phonedetail.get.telephoneday1, a.phonedetail.get.telephoneday2,
          practiceemail, repadminemail, appadminemail, a.acceptsfaxmsg, a.odscode, a.autopostcom, a.practicepopulation, a.meshidentifier, a.active)

        val updateActiveFlag = activeInactiveReferralteams(0L, 0L, a.practiceid.get, a.active.getOrElse(true)).first

      }

    } else {

      PracticeId = (Practice.map { x =>
        (x.ccgid, x.practicename, x.practicecode, x.postcode, x.address1,
          x.address2, x.address3, x.address4, x.citytown, x.county, x.country, x.description, x.crtusr, x.crtusrtype, x.hospitalcd)
      } returning Practice.map(_.practiceid)) +=
        (a.ccgid, a.practicename, a.practicecode, a.postcode, address1,
          address2, address3, address4, a.citytown, a.county, a.country, a.description, a.usrid, "ADMIN", a.hospitalcd)

      var result: TPracticeExtnLkup = new TPracticeExtnLkup(PracticeId, a.ccgid, a.faxno, a.phonedetail.get.telephoneday1, a.phonedetail.get.telephoneday2,
        practiceemail, repadminemail, appadminemail, a.acceptsfaxmsg, a.odscode, a.autopostcom, a.practicepopulation, a.meshidentifier, a.active)

      PracticeExtn.filter(c => c.practiceid === PracticeId).update(result)
    }
    "Success"
  }

  def listPractice(a: getPracticeObj): Array[listPracticeObj] = db withSession { implicit session =>

    val Record = if (a.practiceid != None && a.practiceid.get != 0) {
      Practice.filter { x => x.practiceid === a.practiceid.get }.list
    } else {
      Practice.sortBy { x => x.practiceid }.list
    }

    val Recordval = if (a.practiceid != None && a.practiceid.get != 0) {
      PracticeExtn.filter { x => x.practiceid === a.practiceid.get }.list
    } else {
      PracticeExtn.sortBy { x => x.practiceid }.list
    }

    val resultSet: Array[listPracticeObj] = new Array[listPracticeObj](Record.length)

    for (i <- 0 until Record.length) {

      var addressdtlresult: addressdetl = new addressdetl(Record(i).address1, Record(i).address2, Record(i).address3, Record(i).address4)

      var emailresult: emaildetl = new emaildetl(Recordval(i).practiceemail, Recordval(i).repadminemail, Recordval(i).appadminemail)

      var phoneresult: phonedetail = new phonedetail(Recordval(i).telephoneday1, Recordval(i).telephoneday2)

      resultSet(i) = new listPracticeObj(Some(Record(i).practiceid), Record(i).ccgid, Record(i).practicename,
        Record(i).practicecode, Record(i).postcode,
        Some(addressdtlresult),
        Record(i).citytown, Record(i).county, Record(i).country, Record(i).description,
        Recordval(i).faxno, Some(phoneresult),
        Some(emailresult),
        Recordval(i).acceptsfaxmsg, Recordval(i).odscode,
        Recordval(i).autopostcom, Recordval(i).practicepopulation, Recordval(i).meshidentifier, Recordval(i).active)

    }

    return resultSet
  }

  def activeInactiveReferralteams(regionid: Long, ccgid: Long, practiceid: Long, activeflag: Boolean) = sql"select * from inactivereferralteams('#$regionid', '#$ccgid', '#$practiceid', '#$activeflag')".as[(Boolean)]

  def allpracticeQuery(practiceid: Long, usrid: Long, pageNumber: Int, pageSize: Int, hospitalcd: String, searchtext: String) = sql"select * from getallpracticeval('#$practiceid', '#$usrid', '#$pageNumber', '#$pageSize', '#$hospitalcd', '#$searchtext')".as[(Long, String, String, Long, String, String, String, String, Boolean, String)]

  def alldoctorpracticeQuery(practiceid: Long) = sql"select * from getpracticedoctors('#$practiceid')".as[(Long, String)]

  def alldoctorhospitalQuery(hospitalid: Long) = sql"select * from getscancenterdoctors('#$hospitalid')".as[(Long, String, Long)]

  def listAllPractice(a: getPracticeObj): Array[listAllPracticeObj] = db withSession { implicit session =>

    /*   val Record = if(a.practiceid != None && a.practiceid.get != 0) {
     Practice.filter { x => x.practiceid === a.practiceid.get }.list
   } else {
     Practice.sortBy { x => x.practiceid}.list
   }*/

    val allpracticeList = allpracticeQuery(1, 1, a.pageNumber.getOrElse(0), a.pageSize.getOrElse(20), a.hospitalcd.getOrElse(""), a.searchtext.getOrElse("")).list

    val resultSet: Array[listAllPracticeObj] = new Array[listAllPracticeObj](allpracticeList.length)

    for (i <- 0 until allpracticeList.length) {

      var json1: Option[JsValue] = None

      if (allpracticeList(i)._10 != None) {
        json1 = Some(Json.parse(allpracticeList(i)._10))
      }
      val practicewithccgname = allpracticeList(i)._2 + "(" + allpracticeList(i)._5 + ")"
      resultSet(i) = new listAllPracticeObj(Some(allpracticeList(i)._1), allpracticeList(i)._2, Some(allpracticeList(i)._3), allpracticeList(i)._4, Some(allpracticeList(i)._5), Some(allpracticeList(i)._6), Some(allpracticeList(i)._7), Some(allpracticeList(i)._8), Some(allpracticeList(i)._9), Some(practicewithccgname), json1)

    }

    return resultSet
  }

  def listdoctorPractice(a: Long): Array[listdoctorPracticeObj] = db withSession { implicit session =>

    val alldoctorpracticeList = alldoctorpracticeQuery(a).list

    val resultSet: Array[listdoctorPracticeObj] = new Array[listdoctorPracticeObj](alldoctorpracticeList.length)

    for (i <- 0 until alldoctorpracticeList.length) {

      resultSet(i) = new listdoctorPracticeObj(alldoctorpracticeList(i)._1, alldoctorpracticeList(i)._2, None)

    }

    return resultSet
  }

  def listdoctorScancenter(a: Long): Array[listdoctorPracticeObj] = db withSession { implicit session =>

    val alldoctorhospitalList = alldoctorhospitalQuery(a).list

    val resultSet: Array[listdoctorPracticeObj] = new Array[listdoctorPracticeObj](alldoctorhospitalList.length)

    for (i <- 0 until alldoctorhospitalList.length) {

      resultSet(i) = new listdoctorPracticeObj(alldoctorhospitalList(i)._1, alldoctorhospitalList(i)._2, Some(alldoctorhospitalList(i)._3))

    }

    return resultSet
  }

  def deactivePractice(a: getdeactivePracticeObj): String = db withSession { implicit session =>

    if (a.practiceid != None && a.doctorid != None) {

      if (a.practiceid.get > 0 && a.doctorid.get > 0) {
        doctorpractiesdetails.filter { x => x.doctorid === a.doctorid && x.practiceid === a.practiceid }.map { x => (x.active, x.updtusr, x.updtusrtype) }.update(a.active, Some(a.usrid), Some(a.usrtype))
      } else if (a.doctorid.get > 0) {
        doctorpractiesdetails.filter { x => x.doctorid === a.doctorid }.map { x => (x.active, x.updtusr, x.updtusrtype) }.update(a.active, Some(a.usrid), Some(a.usrtype))

        extndoctor.filter { x => x.DoctorID === a.doctorid }.map { x => (x.PracticeDoctorActive) }.update(a.active)

      }

    }
    "Success"
  }

  def listpostcodedetail(postcode: String): JsValue = db withSession { implicit session =>

    //val URL ="https://api.getAddress.io/find/"+postcode+"?api-key=rNoRy0gWBEC3RwL8wHJDTg14159" //sabari

    //val URL ="https://api.getAddress.io/find/"+postcode+"?api-key=tk0jIrqDgUWnnC3bZyEkFA14543"//venkates

    val URL = "https://api.getAddress.io/find/" + postcode + "?api-key=rNoRy0gWBEC3RwL8wHJDTg14159" //phil

    val result = scala.io.Source.fromURL(URL).mkString.replace("\\", "")

    println("result value " + result)

    var jsonObj: Option[JsValue] = None
    jsonObj = Some(Json.parse(result))
    jsonObj.get

  }
  
  
    def appointmentremindersmssend(a: appointmentremindersmsObj): String = db withSession { implicit session =>

    val URL = "https://api.smsbroadcast.co.uk/api-adv.php?username=c7health&password=SMSC7H38lth&to="+a.smsto.get+"&from="+a.smsfrom.get+"&message=Test%20message%20from%20development%20server&ref=112233&maxsplit=5&delay=15" //phil

    println("URL value " + URL)
    
    val result = scala.io.Source.fromURL(URL).mkString.replace("\\", "")
    
    val resultarr=result.split(':')
    
   println("result value " + resultarr(0))
    
    resultarr(0)

  }

  def allpostcodehospitalQuery(postcode: String, latitude: String, longitude: String, specialitycd: String, parenthospitalid: Long, meterval: Long) = sql"select * from gethospitalpostcodesearch('#$postcode', '#$latitude','#$longitude','#$specialitycd','#$parenthospitalid','#$meterval')".as[(String, Long, String, String, String, String, Long, String, String, Long)]

  def getallrejectlist(hospitalCode: String, hospitalId: Long, ccgId: Long, fromdate: java.sql.Date, todate: java.sql.Date) = sql"select * from getallrejectlist('#$hospitalCode','#$hospitalId','#$ccgId','#$fromdate','#$todate')".as[(java.sql.Date, Long, String, Long, String)]
  def listpostcodehospital(a: getPostcodehospitalObj): Array[listpostcodehospitalObj] = db withSession { implicit session =>

    val subspecialityidval = a.SpecialityCD.get.mkString(", ")

    println("subspecialityidval values " + subspecialityidval)

    val allpostcodehospitalList = allpostcodehospitalQuery(a.postcode.get, a.latitude.get, a.longitude.get, subspecialityidval, a.ParentHospitalID, a.meterval.get).list.distinct

    val resultSet: Array[listpostcodehospitalObj] = new Array[listpostcodehospitalObj](allpostcodehospitalList.length)
    val latitude = a.latitude.get
    val longitude = a.longitude.get
    var text = ""

    for (i <- 0 until allpostcodehospitalList.length) {

      /*      text += allpostcodehospitalList(i)._6

      val URL ="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="+latitude+","+longitude+"&destinations="+text+"&key=AIzaSyCSUu6XdIjnUDj3-4wxi4jCcx7ol77zNa4"

      val result = scala.io.Source.fromURL(URL).mkString

      val parsed = Json.parse(result)

      val records: distancepostcodeObj = ((parsed).as[distancepostcodeObj])

      val record = records.rows

       val rowSet: Array[rowsObj] = new Array[rowsObj](record.length)

     var distance: Option[String]=None
     if(record.length > 0){
     for(i <-0 until record.length){

      val element =  record(i).elements

      if(element.length > 0){
      for (j <-0 until element.length){

        distance =  element(j).distance.get.text

         }
       }

     }
   }*/

      var distance: Option[String] = None

      distance = Some(String.valueOf(allpostcodehospitalList(i)._7))

      resultSet(i) = new listpostcodehospitalObj(Some(allpostcodehospitalList(i)._1), Some(allpostcodehospitalList(i)._2), Some(allpostcodehospitalList(i)._3), distance, Some(allpostcodehospitalList(i)._8), Some(allpostcodehospitalList(i)._9), Some(allpostcodehospitalList(i)._10))

    }

    return resultSet
  }

  def listRejectedPatients(a: getRejectReqObj): Array[getRejectListObj] = db withSession { implicit session =>

    val allRejectList = getallrejectlist(a.hospitalCode.getOrElse(""), a.hospitalID.getOrElse(0L), a.ccgID.getOrElse(0L), a.fromdate.get, a.todate.get).list

    val resultSet: Array[getRejectListObj] = new Array[getRejectListObj](allRejectList.length)

    for (i <- 0 until allRejectList.length) {

      resultSet(i) = new getRejectListObj(allRejectList(i)._4, allRejectList(i)._1, allRejectList(i)._5, Some(allRejectList(i)._3))

    }

    return resultSet
  }

  def findByID(Id: Long): Option[TPracticeLkup] = db withSession { implicit session =>
    Practice.filter { x => x.practiceid === Id }.firstOption
  }

}

