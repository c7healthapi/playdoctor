package models.com.hcue.doctor.traits.add.Careteam

import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker

import models.com.hcue.doctor.Json.Request.Careteam.SpecialityCtgyDetailsObj
import models.com.hcue.doctor.Json.Request.Careteam.specialityLkupDetailsObj
import models.com.hcue.doctor.Json.Request.Careteam.specialityLkupObj
import models.com.hcue.doctor.slick.transactTable.Careteam.SpecialityCategory
import models.com.hcue.doctor.slick.transactTable.Careteam.TSpecialityCategory
import models.com.hcue.doctor.slick.transactTable.Careteam.TSpecialityLkup
import models.com.hcue.doctor.slick.transactTable.Careteam.SpecialityLkup
import models.com.hcue.doctors.Json.Response.getSpecialityLkupObj
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanOptionColumnExtensionMethods
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.columnExtensionMethods
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.optionColumnExtensionMethods
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.productQueryToUpdateInvoker
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.queryToAppliedQueryInvoker
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.queryToInsertInvoker
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.queryToUpdateInvoker
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.valueToConstColumn
import models.com.hcue.doctors.slick.transactTable.patient.EnquirySpeciality
import models.com.hcue.doctors.slick.transactTable.patient.TEnquirySpeciality
import models.com.hcue.doctors.traits.dbProperties.db

import play.api.libs.json.JsValue
import play.api.libs.json.Json

object SpecialityLkupDao {

  val specialityLkup = SpecialityLkup.SpecialityLkup

  val enquirySpeciality = EnquirySpeciality.EnquirySpeciality
  
  val specialityCategory = SpecialityCategory.SpecialityCategory  

  def addspeciality(a: specialityLkupDetailsObj): String = db withSession { implicit session =>

    val specialityDetails: Array[specialityLkupObj] = a.speciality

    var specialitytype: Option[String] = None

    for (i <- 0 until specialityDetails.length) {

      if (a.specialityID != None) {

        specialitytype = Some("C")

        specialityLkup.map { x => (x.DoctorSpecialityDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType, x.ParentID, x.dicomcode, x.usernotes, x.hospitalId, x.Specialitytype) } +=
          (specialityDetails(i).specialitydesc, specialityDetails(i).activeIND, a.updtusr, a.updtusrtype, a.specialityID, specialityDetails(i).dicomcode, specialityDetails(i).usernotes, specialityDetails(i).hospitalId, specialitytype)

      } else {

        specialitytype = Some("P")

        specialityLkup.map { x => (x.DoctorSpecialityDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType, x.dicomcode, x.usernotes, x.hospitalId, x.Specialitytype) } +=
          (specialityDetails(i).specialitydesc, specialityDetails(i).activeIND, a.updtusr, a.updtusrtype, specialityDetails(i).dicomcode, specialityDetails(i).usernotes, specialityDetails(i).hospitalId, specialitytype)

      }

    }

    "Success"

  }

  def addSpecialityWithSubSpeciality(a: specialityLkupDetailsObj): String = db withSession { implicit session =>

    val specialityDetails: Array[specialityLkupObj] = a.speciality

    var parentID: Option[String] = None

    if (a.specialityID != None) {
      parentID = a.specialityID
    }
    var specialitytype: Option[String] = None

    for (i <- 0 until specialityDetails.length) {

      if (specialityDetails(i).dicomcode != null && specialityDetails(i).dicomcode != None && specialityDetails(i).dicomcode != "" && specialityDetails(i).dicomcode != Some("")) {

        specialitytype = Some("C")

        specialityLkup.map { x => (x.DoctorSpecialityDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType, x.ParentID, x.dicomcode, x.usernotes, x.hospitalId, x.Specialitytype, x.CategoryID) } +=
          (specialityDetails(i).specialitydesc, specialityDetails(i).activeIND, a.updtusr, a.updtusrtype, parentID, specialityDetails(i).dicomcode, specialityDetails(i).usernotes, specialityDetails(i).hospitalId, specialitytype,a.CategoryID.get)

      } else {
        specialitytype = Some("P")
        
                 val mappedValue: TSpecialityLkup = new TSpecialityLkup(a.specialityID, specialityDetails(i).specialitydesc, specialityDetails(i).activeIND, a.updtusr, a.updtusrtype, None, specialityDetails(i).dicomcode, specialityDetails(i).usernotes, specialityDetails(i).hospitalId, specialitytype,a.CategoryID.get)

        parentID = (specialityLkup returning specialityLkup.map(_.DoctorSpecialityID)) += mappedValue

      }

    }

    "Success"

  }

  def addSpecialityCategory(a: SpecialityCtgyDetailsObj): String = db withSession { implicit session =>
    val record = specialityCategory.filter(x => x.CategoryID.getOrElse(0L) === a.CategoryID.getOrElse(0L)).firstOption

    if (record == None) {
      val result = specialityCategory.map { x => (x.CategoryName, x.Description, x.HospitalID, x.CrtUSR, x.CrtUSRType) } +=
        (a.CategoryName, a.Description, a.HospitalID, a.CrtUSR.getOrElse(0), a.CrtUSRType.getOrElse(""))
      return "Category added succesfully"
    } else {
      specialityCategory.filter(x => x.CategoryID.get === a.CategoryID.get).map { x => (x.CategoryName, x.Description, x.UpdtUSR, x.UpdtUSRType) }.update((
        a.CategoryName, a.Description, a.UpdtUSR, a.UpdtUSRType))
      return "Category Updated succesfully"
    }
  }
  
  
  def updatespeciality(a: specialityLkupDetailsObj): String = db withSession { implicit session =>

    val specialityDetails: Array[specialityLkupObj] = a.speciality

    for (i <- 0 until specialityDetails.length) {

      if (a.specialityID != None && specialityDetails(i).subSpecialityID != None) {
        specialityLkup.filter { x => x.DoctorSpecialityID === specialityDetails(i).subSpecialityID }.map { x => (x.DoctorSpecialityDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType, x.ParentID, x.dicomcode, x.usernotes, x.hospitalId) }.update((
          specialityDetails(i).specialitydesc,
          specialityDetails(i).activeIND, a.updtusr, a.updtusrtype, a.specialityID, specialityDetails(i).dicomcode, specialityDetails(i).usernotes, specialityDetails(i).hospitalId))
      } else {
        specialityLkup.filter { x => x.DoctorSpecialityID === a.specialityID }.map { x => (x.DoctorSpecialityDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType, x.dicomcode, x.usernotes, x.hospitalId) }.update((
          specialityDetails(i).specialitydesc,
          specialityDetails(i).activeIND, a.updtusr, a.updtusrtype, specialityDetails(i).dicomcode, specialityDetails(i).usernotes, specialityDetails(i).hospitalId))

        specialityLkup.filter { x => x.ParentID === a.specialityID }.map { x => (x.ActiveIND) }.update((specialityDetails(i).activeIND))

      }

    }

    "Success"

  }

  def allspecialityquery(hospitalid: Long) = sql"select * from getallspeciality('#$hospitalid')".as[(String, String, String, String, Long, String, String, String, Long)]

    def allspecialityquery_v1(hospitalid: Long) = sql"select * from getallspeciality_v1('#$hospitalid')".as[(String)]

  
  def getDoctorSpecialityList_v1(hospitalID: Long): JsValue = db withSession { implicit session =>
    val stats = Json.parse(allspecialityquery_v1(hospitalID).firstOption.get)
    return stats
  }

  /*
  def getGeneralreportfileList(date :String): JsValue = db withSession { implicit session =>
      val stats = Json.parse(allreportstats1("GeneralStats", true,date).firstOption.get)
      return stats
    }*/
     
  

  def getSpecialityCategoryList(HospitalID: Long): Array[SpecialityCtgyDetailsObj] = db withSession { implicit session =>

    val CategoryList: List[TSpecialityCategory] = specialityCategory.filter { x => x.HospitalID === HospitalID }.sortBy { x => x.CategoryName.asc }.list
    val categoryTypeDetailsList: Array[SpecialityCtgyDetailsObj] = new Array[SpecialityCtgyDetailsObj](CategoryList.length)

    for (i <- 0 until CategoryList.length) {
      categoryTypeDetailsList(i) = new SpecialityCtgyDetailsObj(
        CategoryList(i).CategoryID,
        CategoryList(i).CategoryName,
        CategoryList(i).Description,
        CategoryList(i).HospitalID, None, None, None, None, None, None)
    }

    return categoryTypeDetailsList
  }

  
 def getDoctorList(hospitalID: Long): Array[getSpecialityLkupObj] = db withSession { implicit session =>

    val queryResult = allspecialityquery(hospitalID).list

    val result: Array[getSpecialityLkupObj] = new Array[getSpecialityLkupObj](queryResult.length)

    for (i <- 0 until queryResult.length) {

      var subspecialityjson: Option[JsValue] = None

      if (queryResult(i)._6 != None) {
        subspecialityjson = Some(Json.parse(queryResult(i)._6))
      }

      result(i) = new getSpecialityLkupObj(queryResult(i)._1, queryResult(i)._2, queryResult(i)._3,
        queryResult(i)._4, Some(queryResult(i)._5), subspecialityjson, Some(queryResult(i)._7), Some(queryResult(i)._8), Some(queryResult(i)._9))

    }
    return result
  }

  def getSpecialityByIDandParentID(parentID: String, specialityId: String): Option[TSpecialityLkup] = db withSession { implicit session =>
    specialityLkup.filter { x => x.DoctorSpecialityID === specialityId && x.ParentID === parentID }.firstOption
  }

  def getEnquirySpeciality(patientEnquiryId: Long): TEnquirySpeciality = db withSession { implicit session =>
    enquirySpeciality.filter { x => x.PatientEnquiryId === patientEnquiryId }.first
  }


}
