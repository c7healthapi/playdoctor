package models.com.hcue.doctor.traits.add.Careteam

import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.booleanOptionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnToOrdered
import scala.slick.driver.PostgresDriver.simple.dateColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.optionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.queryToInsertInvoker
import scala.slick.driver.PostgresDriver.simple.queryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.stringColumnType
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn
import scala.slick.jdbc.StaticQuery.interpolation

import models.com.hcue.doctor.Json.Request.Careteam.AlllistCcgroupsObj
import models.com.hcue.doctor.Json.Request.Careteam.addCcgroupsObj
import models.com.hcue.doctor.Json.Request.Careteam.addContractdetails
import models.com.hcue.doctor.Json.Request.Careteam.addContractspeciality
import models.com.hcue.doctor.Json.Request.Careteam.addexpiredContractdetails
import models.com.hcue.doctor.Json.Request.Careteam.getCcgroupsObj
import models.com.hcue.doctor.Json.Request.Careteam.listCcgroupsObj
import models.com.hcue.doctor.slick.transactTable.Careteam.CcgroupsLkup
import models.com.hcue.doctor.slick.transactTable.Careteam.Contractdetails
import models.com.hcue.doctor.slick.transactTable.Careteam.Contractspecialitydetails
import models.com.hcue.doctor.slick.transactTable.Careteam.SpecialityLkup
import models.com.hcue.doctor.slick.transactTable.Careteam.TCcgroupsLkup
import models.com.hcue.doctor.slick.transactTable.Careteam.TContractdetails
import models.com.hcue.doctor.slick.transactTable.Careteam.TContractspecialitydetails
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.traits.dbProperties.db
import play.api.libs.json.JsValue
import play.api.libs.json.Json

object CcgroupsDao {

  val Ccgroups = CcgroupsLkup.CcgroupsLkup
  val contractdetails = Contractdetails.Contractdetails
  val contractspecialitydetails = Contractspecialitydetails.Contractspecialitydetails
  val specialityLkup = SpecialityLkup.SpecialityLkup

  def addUpdateCcgroups(a: addCcgroupsObj): String = db withSession { implicit session =>
    var CcgroupsId: Long = 0
    var contractId: Long = 0
    var contractspecialityId: Long = 0

    if (a.ccgid != None) {
      val insertedRecord: Option[TCcgroupsLkup] = Ccgroups.filter { x => x.ccgid === a.ccgid.get }.firstOption
      if (insertedRecord.nonEmpty) {
        CcgroupsId = a.ccgid.get
        Ccgroups.filter { x => x.ccgid === a.ccgid.get }.map { x => (x.regionid, x.ccgname, x.ccgreference, x.primarycontactperson, x.primarycontactno, x.primarycontactemail, x.officesserved, x.emponsystem, x.localcontact, x.assistant, x.invoice, x.contractrenewalremain, x.paymentterms, x.active, x.updtusr, x.updtusrtype) }.update(a.regionid, a.ccgname, a.ccgreference, a.primarycontactperson,
          a.primarycontactno, a.primarycontactemail, a.officesserved, a.emponsystem, a.localcontact, a.assistant, a.invoice, a.contractrenewalremain, a.paymentterms, a.active, Some(a.usrid), Some(a.usrtype))

        val updateActiveFlag = activeInactiveReferralteams(0L, a.ccgid.get, 0L, a.active.getOrElse(true)).first

      }
    } else {
      CcgroupsId = (Ccgroups.map { x =>
        (x.regionid, x.ccgname, x.ccgreference, x.primarycontactperson, x.primarycontactno, x.primarycontactemail,
          x.officesserved, x.emponsystem, x.localcontact, x.assistant, x.invoice, x.contractrenewalremain, x.paymentterms, x.active,
          x.crtusr, x.crtusrtype, x.hospitalcd)
      } returning Ccgroups.map(_.ccgid)) += (a.regionid, a.ccgname, a.ccgreference, a.primarycontactperson, a.primarycontactno, a.primarycontactemail, a.officesserved, a.emponsystem, a.localcontact, a.assistant, a.invoice, a.contractrenewalremain, a.paymentterms, a.active, a.usrid, a.usrtype, a.hospitalcd)

    }

    if (a.Contractdetails != None) {
      var contractrecord: addContractdetails = a.Contractdetails.get

      if (contractrecord.ccgid != None) {
        CcgroupsId = contractrecord.ccgid.get
      }

      if (contractrecord.contactdetailsid != None) {
        val contractinsertedRecord: Option[TContractdetails] = contractdetails.filter { x => x.contactdetailsid === contractrecord.contactdetailsid && x.ccgid === CcgroupsId }.firstOption
        if (contractinsertedRecord.nonEmpty) {
          contractId = contractrecord.contactdetailsid.get
          contractdetails.filter { x => x.contactdetailsid === contractId }.map { x => (x.expiredstatus, x.contractstartdate, x.contractenddate, x.minimunageforref, x.lessthanminutes, x.greaterthanminutes, x.tariffinfodvt, x.speciality, x.ccgid, x.updtusr, x.updtusrtype) }.update(Some(contractrecord.expiredstatus), contractrecord.contractstartdate, contractrecord.contractenddate, contractrecord.minimunageforref, contractrecord.lessthanminutes, contractrecord.greaterthanminutes, contractrecord.tariffinfodvt, contractrecord.speciality, Some(CcgroupsId), Some(a.usrid), Some(a.usrtype))
        }
      } else {
        contractId = (contractdetails.map { x => (x.expiredstatus, x.contractstartdate, x.contractenddate, x.minimunageforref, x.lessthanminutes, x.greaterthanminutes, x.tariffinfodvt, x.speciality, x.ccgid, x.crtusr, x.crtusrtype) } returning contractdetails.map(_.contactdetailsid)) += (Some(contractrecord.expiredstatus), contractrecord.contractstartdate, contractrecord.contractenddate, contractrecord.minimunageforref, contractrecord.lessthanminutes, contractrecord.greaterthanminutes, contractrecord.tariffinfodvt, contractrecord.speciality, Some(CcgroupsId), a.usrid, a.usrtype)
      }

      if (a.Contractdetails.get.Contractspeciality != None) {

        var contractspecialityrecord: Array[addContractspeciality] = a.Contractdetails.get.Contractspeciality.get

        for (j <- 0 until contractspecialityrecord.length) {

          var specialityName: Option[String] = null

          specialityName = specialityLkup.filter { x => x.DoctorSpecialityID === contractspecialityrecord(j).Speciality }.map { x => (x.DoctorSpecialityDesc) }.firstOption

          if (contractspecialityrecord(j).contactdetailsid != None) {
            contractId = contractspecialityrecord(j).contactdetailsid.get
          }

          if (contractspecialityrecord(j).contactspecialityid != None) {
            val contractspecialitinsertedRecord: Option[TContractspecialitydetails] = contractspecialitydetails.filter { x => x.contactspecialityid === contractspecialityrecord(j).contactspecialityid && x.contactdetailsid === contractId }.firstOption
            if (contractspecialitinsertedRecord.nonEmpty) {

              contractspecialityId = contractspecialityrecord(j).contactspecialityid.get
              println("contractspecialityId  " + contractspecialityId)

              contractspecialitydetails.filter { x => x.contactspecialityid === contractspecialityId }.map { x => (x.speciality, x.specialityName, x.lessthanminutes, x.greaterthanminutes, x.fixedfee, x.contactdetailsid, x.ccgid, x.active, x.updtusr, x.updtusrtype) }.update(contractspecialityrecord(j).Speciality, specialityName, contractspecialityrecord(j).lessthanminutes, contractspecialityrecord(j).greaterthanminutes, contractspecialityrecord(j).fixedfee, contractId, Some(CcgroupsId), contractspecialityrecord(j).active, Some(a.usrid), Some(a.usrtype))
            }
          } else {

            contractspecialityId = (contractspecialitydetails.map { x => (x.speciality, x.specialityName, x.lessthanminutes, x.greaterthanminutes, x.fixedfee, x.ccgid, x.contactdetailsid, x.active, x.crtusr, x.crtusrtype) } returning contractspecialitydetails.map(_.contactspecialityid)) += (contractspecialityrecord(j).Speciality, specialityName, contractspecialityrecord(j).lessthanminutes, contractspecialityrecord(j).greaterthanminutes, contractspecialityrecord(j).fixedfee, Some(CcgroupsId), contractId, a.active, a.usrid, a.usrtype)
          }
        }

      }

    }

    "Success"
  }

  def listCcgroups(a: getCcgroupsObj): Array[listCcgroupsObj] = db withSession { implicit session =>

    val Record = if (a.ccgid != None && a.ccgid.get != 0) {
      Ccgroups.filter { x => x.ccgid === a.ccgid.get }.list
    } else {
      Ccgroups.sortBy { x => x.ccgid }.list
    }

    println("record leangth value " + Record.length)

    val resultSet: Array[listCcgroupsObj] = new Array[listCcgroupsObj](Record.length)

    for (i <- 0 until Record.length) {

      //get current contract details

      val currentDate: java.sql.Date = new java.sql.Date(CalenderDateConvertor.getISDDate().getTime())

      println("current date value " + currentDate)

      val currentDateFormat1 = CalenderDateConvertor.getDateFormatter(currentDate)
      val istTime = CalenderDateConvertor.getISDTime(currentDate)

      val Contractrecord = if (a.ccgid != None && a.ccgid.get != 0) {
        contractdetails.filter { x => x.ccgid === a.ccgid.get && x.contractenddate >= currentDate }.list
      } else {
        contractdetails.filter { x => x.ccgid === Record(i).ccgid && x.contractenddate >= currentDate }.list
      }

      println("Contractrecord leangth value " + Contractrecord.length)

      val contractresultSet: Array[addContractdetails] = new Array[addContractdetails](Contractrecord.length)

      for (i <- 0 until Contractrecord.length) {

        val Contractspecialityrecord = if (Contractrecord(i).contactdetailsid != 0) {
          contractspecialitydetails.filter { x => x.contactdetailsid === Contractrecord(i).contactdetailsid }.list
        } else {
          contractspecialitydetails.filter { x => x.contactdetailsid === Contractrecord(i).contactdetailsid }.list
        }
        val contractspecialityresultSet: Array[addContractspeciality] = new Array[addContractspeciality](Contractspecialityrecord.length)
        for (j <- 0 until contractspecialityresultSet.length) {
          contractspecialityresultSet(j) = new addContractspeciality(Some(Contractspecialityrecord(j).contactspecialityid), Contractspecialityrecord(j).speciality, Contractspecialityrecord(j).specialityName, Contractspecialityrecord(j).lessthanminutes, Contractspecialityrecord(j).greaterthanminutes, Contractspecialityrecord(j).fixedfee, Contractspecialityrecord(j).ccgid, Some(Contractspecialityrecord(j).contactdetailsid), Contractspecialityrecord(j).active)
        }
        contractresultSet(i) = new addContractdetails(Some(Contractrecord(i).contactdetailsid), Contractrecord(i).expiredstatus.get, Contractrecord(i).contractstartdate, Contractrecord(i).contractenddate,
          Contractrecord(i).minimunageforref, Contractrecord(i).lessthanminutes, Contractrecord(i).greaterthanminutes, Contractrecord(i).tariffinfodvt, Contractrecord(i).speciality, Contractrecord(i).ccgid, Some(contractspecialityresultSet))
      }

      //get expired contract details

      val ExpContractrecord = if (a.ccgid != None && a.ccgid.get != 0) {
        contractdetails.filter { x => x.ccgid === a.ccgid.get && x.contractenddate < currentDate }.list
      } else {
        contractdetails.filter { x => x.ccgid === Record(i).ccgid && x.contractenddate < currentDate }.list
      }

      println("ExpContractrecord leangth value " + ExpContractrecord.length)

      val expcontractresultSet: Array[addexpiredContractdetails] = new Array[addexpiredContractdetails](ExpContractrecord.length)

      for (i <- 0 until ExpContractrecord.length) {

        val Contractspecialityrecord = if (ExpContractrecord(i).contactdetailsid != 0) {
          contractspecialitydetails.filter { x => x.contactdetailsid === ExpContractrecord(i).contactdetailsid }.list
        } else {
          contractspecialitydetails.filter { x => x.contactdetailsid === ExpContractrecord(i).contactdetailsid }.list
        }

        val contractspecialityresultSet: Array[addContractspeciality] = new Array[addContractspeciality](Contractspecialityrecord.length)

        for (j <- 0 until contractspecialityresultSet.length) {

          println("value 1" + Contractspecialityrecord(j).contactspecialityid)

          contractspecialityresultSet(j) = new addContractspeciality(Some(Contractspecialityrecord(j).contactspecialityid), Contractspecialityrecord(j).speciality, Contractspecialityrecord(j).specialityName, Contractspecialityrecord(j).lessthanminutes, Contractspecialityrecord(j).greaterthanminutes, Contractspecialityrecord(j).fixedfee, Contractspecialityrecord(j).ccgid, Some(Contractspecialityrecord(j).contactdetailsid), Contractspecialityrecord(j).active)
        }

        expcontractresultSet(i) = new addexpiredContractdetails(Some(ExpContractrecord(i).contactdetailsid), ExpContractrecord(i).expiredstatus.get, ExpContractrecord(i).contractstartdate, ExpContractrecord(i).contractenddate,
          ExpContractrecord(i).minimunageforref, ExpContractrecord(i).lessthanminutes, ExpContractrecord(i).greaterthanminutes, ExpContractrecord(i).tariffinfodvt, ExpContractrecord(i).speciality, ExpContractrecord(i).ccgid, Some(contractspecialityresultSet))
      }
      resultSet(i) = new listCcgroupsObj(Some(Record(i).ccgid), Record(i).regionid, Record(i).ccgname, Record(i).ccgreference, Record(i).primarycontactperson,
        Record(i).primarycontactno, Record(i).primarycontactemail, Record(i).officesserved,
        Record(i).emponsystem, Record(i).localcontact, Record(i).assistant,
        Record(i).invoice, Record(i).contractrenewalremain, Record(i).paymentterms, Record(i).active, Some(contractresultSet), Some(expcontractresultSet))

    }

    return resultSet
  }

  def activeInactiveReferralteams(regionid: Long, ccgid: Long, practiceid: Long, activeflag: Boolean) = sql"select * from inactivereferralteams('#$regionid', '#$ccgid', '#$practiceid', '#$activeflag')".as[(Boolean)]

  def allccgQuery(ccgid: Long, usrid: Long, pageNumber: Int, pageSize: Int, hospitalcd: String, searchtext: String) = sql"select * from getallccgval('#$ccgid', '#$usrid', '#$pageNumber', '#$pageSize', '#$hospitalcd', '#$searchtext')".as[(Long, Long, String, String, String, String, String, Boolean, String)]

  def listAllCcgroups(a: getCcgroupsObj): Array[AlllistCcgroupsObj] = db withSession { implicit session =>

    val allccgList = allccgQuery(1, 1, a.pageNumber.getOrElse(0), a.pageSize.getOrElse(20), a.hospitalcd.getOrElse(""), a.searchtext.getOrElse("")).list

    val resultSet: Array[AlllistCcgroupsObj] = new Array[AlllistCcgroupsObj](allccgList.length)

    for (i <- 0 until allccgList.length) {

      var json1: Option[JsValue] = None

      if (allccgList(i)._9 != None) {
        json1 = Some(Json.parse(allccgList(i)._9))
      }

      resultSet(i) = new AlllistCcgroupsObj(Some(allccgList(i)._1), allccgList(i)._2, Some(allccgList(i)._3), allccgList(i)._4, Some(allccgList(i)._5), Some(allccgList(i)._6), Some(allccgList(i)._7), Some(allccgList(i)._8), json1)

    }

    return resultSet

  }

}

