package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TServiceLevelAgreement(
  slaid:                 Long,
  routinereftriage:      Option[Long],
  routinerefscan:        Option[Long],
  routineappsendback:    Option[Long],
  urgentreftriage:       Option[Long],
  urgentrefdatebook:     Option[Long],
  urgentrefscan:         Option[Long],
  urgentscanrepsendback: Option[Long],
  dvtreftriage:          Option[Long],
  dvtrefdatebook:        Option[Long],
  dvtrefscan:            Option[Long],
  dvtscanrepsendback:    Option[Long],
  expiredstatus:         Option[Boolean],
  regionid:              Long,
  slavalidfrom:          java.sql.Date,
  slavalidto:            java.sql.Date,
  crtusr:                Long,
  crtusrtype:            String,
  updtusr:               Option[Long],
  updtusrtype:           Option[String])

class ServiceLevelAgreement(tag: Tag) extends Table[TServiceLevelAgreement](tag, "servicelevelagreement") {

  def slaid = column[Long]("slaid", O.PrimaryKey, O.AutoInc)
  def routinereftriage = column[Option[Long]]("routinereftriage", O Nullable)
  def routinerefscan = column[Option[Long]]("routinerefscan", O Nullable)
  def routineappsendback = column[Option[Long]]("routineappsendback", O Nullable)
  def urgentreftriage = column[Option[Long]]("urgentreftriage", O Nullable)
  def urgentrefdatebook = column[Option[Long]]("urgentrefdatebook", O Nullable)
  def urgentrefscan = column[Option[Long]]("urgentrefscan", O Nullable)
  def urgentscanrepsendback = column[Option[Long]]("urgentscanrepsendback", O Nullable)
  def dvtreftriage = column[Option[Long]]("dvtreftriage", O Nullable)
  def dvtrefdatebook = column[Option[Long]]("dvtrefdatebook", O Nullable)
  def dvtrefscan = column[Option[Long]]("dvtrefscan", O Nullable)
  def dvtscanrepsendback = column[Option[Long]]("dvtscanrepsendback", O Nullable)
  def expiredstatus = column[Option[Boolean]]("expiredstatus", O Nullable)
  def regionid = column[Long]("regionid", O NotNull)
  def slavalidfrom = column[java.sql.Date]("slavalidfrom", O Nullable)
  def slavalidto = column[java.sql.Date]("slavalidto", O NotNull)
  def crtusr = column[Long]("crtusr", O NotNull)
  def crtusrtype = column[String]("crtusrtype", O NotNull)
  def updtusr = column[Option[Long]]("updtusr", O.Nullable)
  def updtusrtype = column[Option[String]]("updtusrtype", O Nullable)

  def * = (slaid, routinereftriage, routinerefscan, routineappsendback, urgentreftriage, urgentrefdatebook, urgentrefscan, urgentscanrepsendback, dvtreftriage, dvtrefdatebook, dvtrefscan, dvtscanrepsendback, expiredstatus, regionid, slavalidfrom, slavalidto, crtusr, crtusrtype, updtusr, updtusrtype).<>((TServiceLevelAgreement.apply _).tupled, TServiceLevelAgreement.unapply)

}

object ServiceLevelAgreement {
  def ServiceLevelAgreement =
    scala.slick.driver.PostgresDriver.simple.TableQuery[ServiceLevelAgreement]
}