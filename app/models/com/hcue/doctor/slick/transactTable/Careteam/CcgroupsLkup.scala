package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TCcgroupsLkup(
  ccgid:                 Long,
  regionid:              Long,
  ccgname:               String,
  ccgreference:          Option[String],
  primarycontactperson:  Option[String],
  primarycontactno:      Option[String],
  primarycontactemail:   Option[String],
  officesserved:         Option[String],
  emponsystem:           Option[String],
  localcontact:          Option[String],
  assistant:             Option[String],
  invoice:               Option[String],
  contractrenewalremain: Option[java.sql.Date],
  paymentterms:          Option[String],
  active:                Option[Boolean],
  crtusr:                Long,
  crtusrtype:            String,
  updtusr:               Option[Long],
  updtusrtype:           Option[String],
  hospitalcd:            Option[String])

class CcgroupsLkup(tag: Tag) extends Table[TCcgroupsLkup](tag, "ccgroups") {

  def ccgid = column[Long]("ccgid", O.PrimaryKey, O.AutoInc)
  def regionid = column[Long]("regionid", O NotNull)
  def ccgname = column[String]("ccgname", O NotNull)
  def ccgreference = column[Option[String]]("ccgreference", O Nullable)
  def primarycontactperson = column[Option[String]]("primarycontactperson", O Nullable)
  def primarycontactno = column[Option[String]]("primarycontactno", O Nullable)
  def primarycontactemail = column[Option[String]]("primarycontactemail", O Nullable)
  def officesserved = column[Option[String]]("officesserved", O Nullable)
  def emponsystem = column[Option[String]]("emponsystem", O Nullable)
  def localcontact = column[Option[String]]("localcontact", O Nullable)
  def assistant = column[Option[String]]("assistant", O Nullable)
  def invoice = column[Option[String]]("invoice", O Nullable)
  def contractrenewalremain = column[Option[java.sql.Date]]("contractrenewalremain", O NotNull)
  def paymentterms = column[Option[String]]("paymentterms", O Nullable)
  def active = column[Option[Boolean]]("active", O Nullable)
  def crtusr = column[Long]("crtusr", O NotNull)
  def crtusrtype = column[String]("crtusrtype", O NotNull)
  def updtusr = column[Option[Long]]("updtusr", O.Nullable)
  def updtusrtype = column[Option[String]]("updtusrtype", O Nullable)
  def hospitalcd = column[Option[String]]("hospitalcd", O Nullable)

  def * = (ccgid, regionid, ccgname, ccgreference, primarycontactperson, primarycontactno, primarycontactemail, officesserved, emponsystem, localcontact, assistant, invoice, contractrenewalremain, paymentterms, active, crtusr, crtusrtype, updtusr, updtusrtype, hospitalcd).<>((TCcgroupsLkup.apply _).tupled, TCcgroupsLkup.unapply)

}

object CcgroupsLkup {
  def CcgroupsLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[CcgroupsLkup]
}