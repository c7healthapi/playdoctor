package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctorPracties(
  doctorid:     Long,
  practiceid:   Long,
  practicename: Option[String],
  practicecode: Option[String],
  postcode:     Option[String],
  ccgname:      Option[String],
  telephone:    Option[String],
  active:       Option[Boolean],
  crtusr:       Long,
  crtusrtype:   String,
  updtusr:      Option[Long],
  updtusrtype:  Option[String])

class DoctorPracties(tag: Tag) extends Table[TDoctorPracties](tag, "doctorpractice") {

  def doctorid = column[Long]("doctorid", O Nullable)
  def practiceid = column[Long]("practiceid", O Nullable)
  def practicename = column[Option[String]]("practicename", O Nullable)
  def practicecode = column[Option[String]]("practicecode", O Nullable)
  def postcode = column[Option[String]]("postcode", O Nullable)
  def ccgname = column[Option[String]]("ccgname", O Nullable)
  def telephone = column[Option[String]]("telephone", O Nullable)
  def active = column[Option[Boolean]]("active", O Nullable)
  def crtusr = column[Long]("crtusr", O NotNull)
  def crtusrtype = column[String]("crtusrtype", O NotNull)
  def updtusr = column[Option[Long]]("updtusr", O.Nullable)
  def updtusrtype = column[Option[String]]("updtusrtype", O Nullable)

  def * = (doctorid, practiceid, practicename, practicecode, postcode, ccgname, telephone, active, crtusr, crtusrtype, updtusr, updtusrtype).<>((TDoctorPracties.apply _).tupled, TDoctorPracties.unapply)

}

object DoctorPracties {
  def DoctorPracties =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorPracties]
}