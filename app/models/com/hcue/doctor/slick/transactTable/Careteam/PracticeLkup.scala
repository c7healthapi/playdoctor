package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TPracticeLkup(
  practiceid:    Long,
  ccgid:         Long,
  practicename:  String,
  practicecode:  Option[String],
  postcode:      Option[String],
  address1:      Option[String],
  address2:      Option[String],
  address3:      Option[String],
  address4:      Option[String],
  citytown:      Option[String],
  county:        Option[String],
  country:       Option[String],
  description:   Option[String],
  repadminemail: Option[String],
  crtusr:        Long,
  crtusrtype:    String,
  updtusr:       Option[Long],
  updtusrtype:   Option[String],
  hospitalcd:    Option[String])

class PracticeLkup(tag: Tag) extends Table[TPracticeLkup](tag, "practice") {

  def practiceid = column[Long]("practiceid", O.PrimaryKey, O.AutoInc)
  def ccgid = column[Long]("ccgid", O NotNull)
  def practicename = column[String]("practicename", O NotNull)
  def practicecode = column[Option[String]]("practicecode", O Nullable)
  def postcode = column[Option[String]]("postcode", O Nullable)
  def address1 = column[Option[String]]("address1", O Nullable)
  def address2 = column[Option[String]]("address2", O Nullable)
  def address3 = column[Option[String]]("address3", O Nullable)
  def address4 = column[Option[String]]("address4", O Nullable)
  def citytown = column[Option[String]]("citytown", O Nullable)
  def county = column[Option[String]]("county", O Nullable)
  def country = column[Option[String]]("country", O Nullable)
  def description = column[Option[String]]("description", O Nullable)
  def repadminemail = column[Option[String]]("repadminemail", O Nullable)
  def crtusr = column[Long]("crtusr", O NotNull)
  def crtusrtype = column[String]("crtusrtype", O NotNull)
  def updtusr = column[Option[Long]]("updtusr", O.Nullable)
  def updtusrtype = column[Option[String]]("updtusrtype", O Nullable)
  def hospitalcd = column[Option[String]]("hospitalcd", O Nullable)

  def * = (practiceid, ccgid, practicename, practicecode, postcode, address1, address2, address3, address4, citytown, county, country, description, repadminemail, crtusr, crtusrtype, updtusr, updtusrtype, hospitalcd).<>((TPracticeLkup.apply _).tupled, TPracticeLkup.unapply)

}

object PracticeLkup {
  def PracticeLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PracticeLkup]
}