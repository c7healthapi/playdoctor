package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.intColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TContractdetails(
  contactdetailsid:   Long,
  expiredstatus:      Option[Boolean],
  contractstartdate:  java.sql.Date,
  contractenddate:    java.sql.Date,
  minimunageforref:   Option[Int],
  lessthanminutes:    Option[Long],
  greaterthanminutes: Option[Long],
  tariffinfodvt:      Option[String],
  speciality:         Option[Map[String, String]],
  ccgid:              Option[Long],
  crtusr:             Long,
  crtusrtype:         String,
  updtusr:            Option[Long],
  updtusrtype:        Option[String])

class Contractdetails(tag: Tag) extends Table[TContractdetails](tag, "contractdetails") {

  def contactdetailsid = column[Long]("contactdetailsid", O.PrimaryKey, O.AutoInc)
  def expiredstatus = column[Option[Boolean]]("expiredstatus", O Nullable)
  def contractstartdate = column[java.sql.Date]("contractstartdate", O Nullable)
  def contractenddate = column[java.sql.Date]("contractenddate", O NotNull)
  def minimunageforref = column[Option[Int]]("minimunageforref", O Nullable)
  def lessthanminutes = column[Option[Long]]("lessthanminutes", O Nullable)
  def greaterthanminutes = column[Option[Long]]("greaterthanminutes", O Nullable)
  def tariffinfodvt = column[Option[String]]("tariffinfodvt", O Nullable)
  def speciality = column[Option[Map[String, String]]]("speciality", O Nullable)
  def ccgid = column[Option[Long]]("ccgid", O Nullable)
  def crtusr = column[Long]("crtusr", O NotNull)
  def crtusrtype = column[String]("crtusrtype", O NotNull)
  def updtusr = column[Option[Long]]("updtusr", O.Nullable)
  def updtusrtype = column[Option[String]]("updtusrtype", O Nullable)

  def * = (contactdetailsid, expiredstatus, contractstartdate, contractenddate, minimunageforref, lessthanminutes, greaterthanminutes, tariffinfodvt, speciality, ccgid, crtusr, crtusrtype, updtusr, updtusrtype).<>((TContractdetails.apply _).tupled, TContractdetails.unapply)

}

object Contractdetails {
  def Contractdetails =
    scala.slick.driver.PostgresDriver.simple.TableQuery[Contractdetails]
}