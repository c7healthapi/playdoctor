package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.bigDecimalColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TContractspecialitydetails(
  contactspecialityid: Long,
  speciality:          Option[String],
  specialityName:      Option[String],
  lessthanminutes:     Option[BigDecimal],
  greaterthanminutes:  Option[BigDecimal],
  fixedfee:            Option[BigDecimal],
  ccgid:               Option[Long],
  contactdetailsid:    Long,
  active:              Option[Boolean],
  crtusr:              Long,
  crtusrtype:          String,
  updtusr:             Option[Long],
  updtusrtype:         Option[String])

class Contractspecialitydetails(tag: Tag) extends Table[TContractspecialitydetails](tag, "tcontractspecialitydetails") {

  def contactspecialityid = column[Long]("contactspecialityid", O.PrimaryKey, O.AutoInc)
  def speciality = column[Option[String]]("speciality", O Nullable)
  def specialityName = column[Option[String]]("specialityName", O Nullable)
  def lessthanminutes = column[Option[BigDecimal]]("lessthanminutes", O Nullable)
  def greaterthanminutes = column[Option[BigDecimal]]("greaterthanminutes", O Nullable)
  def fixedfee = column[Option[BigDecimal]]("fixedfee", O Nullable)
  def ccgid = column[Option[Long]]("ccgid", O Nullable)
  def contactdetailsid = column[Long]("contactdetailsid", O Nullable)
  def active = column[Option[Boolean]]("active", O Nullable)
  def crtusr = column[Long]("crtusr", O NotNull)
  def crtusrtype = column[String]("crtusrtype", O NotNull)
  def updtusr = column[Option[Long]]("updtusr", O.Nullable)
  def updtusrtype = column[Option[String]]("updtusrtype", O Nullable)

  def * = (contactspecialityid, speciality, specialityName, lessthanminutes, greaterthanminutes, fixedfee, ccgid, contactdetailsid, active, crtusr, crtusrtype, updtusr, updtusrtype).<>((TContractspecialitydetails.apply _).tupled, TContractspecialitydetails.unapply)

}

object Contractspecialitydetails {
  def Contractspecialitydetails =
    scala.slick.driver.PostgresDriver.simple.TableQuery[Contractspecialitydetails]
}