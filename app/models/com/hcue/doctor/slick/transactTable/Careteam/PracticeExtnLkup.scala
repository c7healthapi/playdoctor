package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TPracticeExtnLkup(
  practiceid:         Long,
  ccgid:              Long,
  faxno:              Option[String],
  telephoneday1:      Option[String],
  telephoneday2:      Option[String],
  practiceemail:      Option[String],
  repadminemail:      Option[String],
  appadminemail:      Option[String],
  acceptsfaxmsg:      Option[Boolean],
  odscode:            Option[String],
  autopostcom:        Option[Boolean],
  practicepopulation: Option[String],
  meshidentifier:     Option[String],
  active:             Option[Boolean])

class PracticeExtnLkup(tag: Tag) extends Table[TPracticeExtnLkup](tag, "practice") {

  def practiceid = column[Long]("practiceid", O.PrimaryKey, O.AutoInc)
  def ccgid = column[Long]("ccgid", O NotNull)
  def faxno = column[Option[String]]("faxno", O Nullable)
  def telephoneday1 = column[Option[String]]("telephoneday1", O Nullable)
  def telephoneday2 = column[Option[String]]("telephoneday2", O Nullable)
  def practiceemail = column[Option[String]]("practiceemail", O Nullable)
  def repadminemail = column[Option[String]]("repadminemail", O Nullable)
  def appadminemail = column[Option[String]]("appadminemail", O Nullable)
  def acceptsfaxmsg = column[Option[Boolean]]("acceptsfaxmsg", O Nullable)
  def odscode = column[Option[String]]("odscode", O Nullable)
  def autopostcom = column[Option[Boolean]]("autopostcom", O Nullable)
  def practicepopulation = column[Option[String]]("practicepopulation", O Nullable)
  def meshidentifier = column[Option[String]]("meshidentifier", O Nullable)
  def active = column[Option[Boolean]]("active", O Nullable)

  def * = (practiceid, ccgid, faxno, telephoneday1, telephoneday2, practiceemail, repadminemail, appadminemail, acceptsfaxmsg, odscode, autopostcom, practicepopulation, meshidentifier, active).<>((TPracticeExtnLkup.apply _).tupled, TPracticeExtnLkup.unapply)

}

object PracticeExtnLkup {
  def PracticeExtnLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PracticeExtnLkup]
}