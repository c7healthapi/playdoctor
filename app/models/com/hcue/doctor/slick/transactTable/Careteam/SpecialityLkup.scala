package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType


case class TSpecialityLkup(
  DoctorSpecialityID:   Option[String],
  DoctorSpecialityDesc: String,
  ActiveIND:            String,
  UpdtUSR:              Option[Long],
  UpdtUSRType:          String,
  ParentID:             Option[String],
  dicomcode:            Option[String],
  usernotes:            Option[String],
  hospitalId:           Option[Long],
  Specialitytype:       Option[String],
  CategoryID:            Long)

class SpecialityLkup(tag: Tag) extends Table[TSpecialityLkup](tag, "tDoctorSpecialityLkup") {

  def DoctorSpecialityID = column[Option[String]]("DoctorSpecialityID", O.PrimaryKey, O.AutoInc)
  def DoctorSpecialityDesc = column[String]("DoctorSpecialityDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[String]("UpdtUSRType", O Nullable)
  def ParentID = column[Option[String]]("ParentID", O Nullable)
  def dicomcode = column[Option[String]]("dicomcode", O Nullable)
  def usernotes = column[Option[String]]("usernotes", O Nullable)
  def hospitalId = column[Option[Long]]("hospitalid", O Nullable)
  def Specialitytype = column[Option[String]]("Specialitytype", O Nullable)
  def CategoryID = column[Long]("CategoryID", O Nullable)

  def * = (DoctorSpecialityID, DoctorSpecialityDesc, ActiveIND, UpdtUSR, UpdtUSRType, ParentID, dicomcode, usernotes, hospitalId, Specialitytype,CategoryID).<>((TSpecialityLkup.apply _).tupled, TSpecialityLkup.unapply)

}

object SpecialityLkup {
  def SpecialityLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[SpecialityLkup]
}