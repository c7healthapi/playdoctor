package models.com.hcue.doctor.slick.transactTable.Careteam

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TSpecialityCategory(
  CategoryID: Option[Long],
  CategoryName: String,
  Description: Option[String],
  Created: Option[java.sql.Date],
  CrtUSR: Long,
  CrtUSRType: String,
  Modified: Option[java.sql.Date],
  UpdtUSR: Option[Long],
  UpdtUSRType: Option[String], 
  HospitalID: Long)

class SpecialityCategory(tag: Tag) extends Table[TSpecialityCategory](tag, "tSpecialityCategory") {

  def CategoryID = column[Option[Long]]("CategoryID", O.PrimaryKey, O.AutoInc)
  def CategoryName = column[String]("CategoryName", O NotNull)
  def Description = column[Option[String]]("Description", O NotNull)
  def Created = column[Option[java.sql.Date]]("Created", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def Modified = column[Option[java.sql.Date]]("Modified", O Nullable)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def HospitalID = column[Long]("HospitalID", O NotNull)

  def * = (CategoryID, CategoryName, Description, Created, CrtUSR, CrtUSRType, Modified, UpdtUSR, UpdtUSRType,HospitalID).<>((TSpecialityCategory.apply _).tupled, TSpecialityCategory.unapply)
}

object SpecialityCategory {
  def SpecialityCategory =
    scala.slick.driver.PostgresDriver.simple.TableQuery[SpecialityCategory]
}