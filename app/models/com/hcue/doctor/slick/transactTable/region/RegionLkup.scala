package models.com.hcue.doctor.slick.transactTable.region

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TRegionLkup(
  regionid:      Long,
  regionname:    String,
  contactperson: Option[String],
  contactnumber: Option[String],
  email:         Option[String],
  postcode:      Option[String],
  active:        Option[Boolean],
  hospitalcd:    Option[String],
  crtusr:        Long,
  crtusrtype:    String,
  updtusr:       Option[Long],
  updtusrtype:   Option[String])

class RegionLkup(tag: Tag) extends Table[TRegionLkup](tag, "region") {

  def regionid = column[Long]("regionid", O.PrimaryKey, O.AutoInc)
  def regionname = column[String]("regionname", O NotNull)
  def contactperson = column[Option[String]]("contactperson", O Nullable)
  def contactnumber = column[Option[String]]("contactnumber", O Nullable)
  def email = column[Option[String]]("email", O Nullable)
  def postcode = column[Option[String]]("postcode", O NotNull)
  def active = column[Option[Boolean]]("active", O NotNull)
  def hospitalcd = column[Option[String]]("hospitalcd", O Nullable)
  def crtusr = column[Long]("crtusr", O NotNull)
  def crtusrtype = column[String]("crtusrtype", O NotNull)
  def updtusr = column[Option[Long]]("updtusr", O.Nullable)
  def updtusrtype = column[Option[String]]("updtusrtype", O Nullable)

  def * = (regionid, regionname, contactperson, contactnumber, email, postcode, active, hospitalcd, crtusr, crtusrtype, updtusr, updtusrtype).<>((TRegionLkup.apply _).tupled, TRegionLkup.unapply)

}

object RegionLkup {
  def RegionLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[RegionLkup]
}