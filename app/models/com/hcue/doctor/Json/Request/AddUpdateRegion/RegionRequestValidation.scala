/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctor.Json.Request.AddUpdateRegion

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime
import play.api.libs.json.JsValue

case class addRegionObj(SlaRecord:Option[addSlaRecord],regionid:Option[Long],regionname: String,contactperson: Option[String], 
    contactnumber: Option[String],email: Option[String], 
     postcode: Option[String],active: Option[Boolean],usrid: Long, usrtype: String, hospitalcd: Option[String])

object addRegionObj {
  val addRegionObjReads = (
    (__ \ "SlaRecord").readNullable[addSlaRecord] and
    (__ \ "regionid").readNullable[Long] and
    (__ \ "regionname").read[String] and
    (__ \ "contactperson").readNullable[String] and
    (__ \ "contactnumber").readNullable[String] and
    (__ \ "email").readNullable[String] and
    (__ \ "postcode").readNullable[String] and
    (__ \ "active").readNullable[Boolean] and
    (__ \ "usrid").read[Long] and
    (__ \ "usrtype").read[String] and 
    (__ \ "hospitalcd").readNullable[String])(addRegionObj.apply _)

  val addRegionObjWrites = (
    (__ \ "SlaRecord").writeNullable[addSlaRecord] and
    (__ \ "regionid").writeNullable[Long] and
    (__ \ "regionname").write[String] and
    (__ \ "contactperson").writeNullable[String] and
    (__ \ "contactnumber").writeNullable[String] and
    (__ \ "email").writeNullable[String] and
    (__ \ "postcode").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean] and
    (__ \ "usrid").write[Long] and
    (__ \ "usrtype").write[String] and 
    (__ \ "hospitalcd").writeNullable[String])(unlift(addRegionObj.unapply))
  implicit val addRegionObjFormat: Format[addRegionObj] = Format(addRegionObjReads, addRegionObjWrites)

}



case class listAllRegionObj(regionid:Option[Long],regionname: String,contactperson: Option[String], 
    contactnumber: Option[String],email: Option[String], 
    postcode: Option[String],active: Option[Boolean],slaDate: Option[java.sql.Date],ccgsdetail:Option[JsValue])

object listAllRegionObj {
  val listAllRegionObjReads = (
    (__ \ "regionid").readNullable[Long] and
    (__ \ "regionname").read[String] and
    (__ \ "contactperson").readNullable[String] and
    (__ \ "contactnumber").readNullable[String] and
    (__ \ "email").readNullable[String] and
    (__ \ "postcode").readNullable[String] and
    (__ \ "active").readNullable[Boolean] and
    (__ \ "slaDate").readNullable[java.sql.Date] and 
    (__ \ "ccgsdetail").readNullable[JsValue])(listAllRegionObj.apply _)

  val listAllRegionObjWrites = (
    (__ \ "regionid").writeNullable[Long] and
    (__ \ "regionname").write[String] and
    (__ \ "contactperson").writeNullable[String] and
    (__ \ "contactnumber").writeNullable[String] and
    (__ \ "email").writeNullable[String] and
    (__ \ "postcode").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean] and
     (__ \ "slaDate").writeNullable[java.sql.Date] and 
    (__ \ "ccgsdetail").writeNullable[JsValue])(unlift(listAllRegionObj.unapply))
  implicit val listAllRegionObjFormat: Format[listAllRegionObj] = Format(listAllRegionObjReads, listAllRegionObjWrites)

}




case class listRegionObj(regionid:Option[Long],regionname: String,contactperson: Option[String], 
    contactnumber: Option[String],email: Option[String], 
    postcode: Option[String],active: Option[Boolean],SlaRecord:Option[ Array[addSlaRecord]],ExpiredSlaRecord:Option[ Array[ExpiredSlaRecordObj]])

object listRegionObj {
  val listRegionObjReads = (
    (__ \ "regionid").readNullable[Long] and
    (__ \ "regionname").read[String] and
    (__ \ "contactperson").readNullable[String] and
    (__ \ "contactnumber").readNullable[String] and
    (__ \ "email").readNullable[String] and
    (__ \ "postcode").readNullable[String] and
    (__ \ "active").readNullable[Boolean] and
    (__ \ "SlaRecord").readNullable[Array[addSlaRecord]] and
    (__ \ "ExpiredSlaRecord").readNullable[Array[ExpiredSlaRecordObj]])(listRegionObj.apply _)

  val listRegionObjWrites = (
    (__ \ "regionid").writeNullable[Long] and
    (__ \ "regionname").write[String] and
    (__ \ "contactperson").writeNullable[String] and
    (__ \ "contactnumber").writeNullable[String] and
    (__ \ "email").writeNullable[String] and
    (__ \ "postcode").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean] and
    (__ \ "SlaRecord").writeNullable[Array[addSlaRecord]] and
    (__ \ "ExpiredSlaRecord").writeNullable[Array[ExpiredSlaRecordObj]])(unlift(listRegionObj.unapply))
  implicit val listRegionObjFormat: Format[listRegionObj] = Format(listRegionObjReads, listRegionObjWrites)

}


case class getRegionObj(regionid: Option[Long],usrid: Long, usrtype: String, pageNumber: Option[Int], hospitalcd: Option[String], pageSize: Option[Int], searchtext: Option[String])

object getRegionObj {
 val getRegionObjReads = (
   (__ \ "regionid").readNullable[Long] and
   (__ \ "usrid").read[Long] and
   (__ \ "usrtype").read[String] and 
   (__ \ "pageNumber").readNullable[Int] and
   (__ \ "hospitalcd").readNullable[String] and 
   (__ \ "pageSize").readNullable[Int] and
   (__ \ "searchtext").readNullable[String])(getRegionObj.apply _)

 val getRegionObjWrites = (
   (__ \ "regionid").writeNullable[Long] and
   (__ \ "usrid").write[Long] and
   (__ \ "usrtype").write[String] and 
   (__ \ "pageNumber").writeNullable[Int] and
   (__ \ "hospitalcd").writeNullable[String] and 
   (__ \ "pageSize").writeNullable[Int] and
   (__ \ "searchtext").writeNullable[String])(unlift(getRegionObj.unapply))
 implicit val getRegionObjFormat: Format[getRegionObj] = Format(getRegionObjReads, getRegionObjWrites)

}


case class addSlaRecord(slaid: Option[Long],routinereftriage: Option[Long], routinerefscan: Option[Long], routineappsendback: Option[Long],
                        urgentreftriage: Option[Long], urgentrefdatebook: Option[Long], urgentrefscan: Option[Long],urgentscanrepsendback: Option[Long],
                        dvtreftriage: Option[Long], dvtrefdatebook: Option[Long], dvtrefscan: Option[Long],dvtscanrepsendback: Option[Long],
                        expiredstatus: Option[Boolean],regionid: Option[Long], slavalidfrom: java.sql.Date,slavalidto: java.sql.Date)

object addSlaRecord {
  val addSlaRecordReads = (
    (__ \ "slaid").readNullable[Long] and  
    (__ \ "routinereftriage").readNullable[Long] and
    (__ \ "routinerefscan").readNullable[Long] and
    (__ \ "routineappsendback").readNullable[Long] and
    (__ \ "urgentreftriage").readNullable[Long] and
    (__ \ "urgentrefdatebook").readNullable[Long] and
    (__ \ "urgentrefscan").readNullable[Long] and
    (__ \ "urgentscanrepsendback").readNullable[Long] and
    (__ \ "dvtreftriage").readNullable[Long] and
    (__ \ "dvtrefdatebook").readNullable[Long] and
    (__ \ "dvtrefscan").readNullable[Long] and
    (__ \ "dvtscanrepsendback").readNullable[Long] and
    (__ \ "expiredstatus").readNullable[Boolean] and 
    (__ \ "regionid").readNullable[Long] and
    (__ \ "slavalidfrom").read[java.sql.Date] and
    (__ \ "slavalidto").read[java.sql.Date])(addSlaRecord.apply _)

  val addSlaRecordWrites = (
    (__ \ "slaid").writeNullable[Long] and  
    (__ \ "routinereftriage").writeNullable[Long] and
    (__ \ "routinerefscan").writeNullable[Long] and
    (__ \ "routineappsendback").writeNullable[Long] and
    (__ \ "urgentreftriage").writeNullable[Long] and
    (__ \ "urgentrefdatebook").writeNullable[Long] and
    (__ \ "urgentrefscan").writeNullable[Long] and
    (__ \ "urgentscanrepsendback").writeNullable[Long] and
    (__ \ "dvtreftriage").writeNullable[Long] and
    (__ \ "dvtrefdatebook").writeNullable[Long] and
    (__ \ "dvtrefscan").writeNullable[Long] and
    (__ \ "dvtscanrepsendback").writeNullable[Long] and
    (__ \ "expiredstatus").writeNullable[Boolean] and
    (__ \ "regionid").writeNullable[Long] and
    (__ \ "slavalidfrom").write[java.sql.Date] and
    (__ \ "slavalidto").write[java.sql.Date])(unlift(addSlaRecord.unapply))
  implicit val addSlaRecordFormat: Format[addSlaRecord] = Format(addSlaRecordReads, addSlaRecordWrites)

}

case class ExpiredSlaRecordObj(slaid: Option[Long],routinereftriage: Option[Long], routinerefscan: Option[Long], routineappsendback: Option[Long],
                        urgentreftriage: Option[Long], urgentrefdatebook: Option[Long], urgentrefscan: Option[Long],urgentscanrepsendback: Option[Long],
                        dvtreftriage: Option[Long], dvtrefdatebook: Option[Long], dvtrefscan: Option[Long],dvtscanrepsendback: Option[Long],
                        expiredstatus: Option[Boolean],regionid: Option[Long], slavalidfrom: java.sql.Date,slavalidto: java.sql.Date)

object ExpiredSlaRecordObj {
  val ExpiredSlaRecordObjReads = (
    (__ \ "slaid").readNullable[Long] and  
    (__ \ "routinereftriage").readNullable[Long] and
    (__ \ "routinerefscan").readNullable[Long] and
    (__ \ "routineappsendback").readNullable[Long] and
    (__ \ "urgentreftriage").readNullable[Long] and
    (__ \ "urgentrefdatebook").readNullable[Long] and
    (__ \ "urgentrefscan").readNullable[Long] and
    (__ \ "urgentscanrepsendback").readNullable[Long] and
    (__ \ "dvtreftriage").readNullable[Long] and
    (__ \ "dvtrefdatebook").readNullable[Long] and
    (__ \ "dvtrefscan").readNullable[Long] and
    (__ \ "dvtscanrepsendback").readNullable[Long] and
    (__ \ "expiredstatus").readNullable[Boolean] and 
    (__ \ "regionid").readNullable[Long] and
    (__ \ "slavalidfrom").read[java.sql.Date] and
    (__ \ "slavalidto").read[java.sql.Date])(ExpiredSlaRecordObj.apply _)

  val ExpiredSlaRecordObjWrites = (
    (__ \ "slaid").writeNullable[Long] and  
    (__ \ "routinereftriage").writeNullable[Long] and
    (__ \ "routinerefscan").writeNullable[Long] and
    (__ \ "routineappsendback").writeNullable[Long] and
    (__ \ "urgentreftriage").writeNullable[Long] and
    (__ \ "urgentrefdatebook").writeNullable[Long] and
    (__ \ "urgentrefscan").writeNullable[Long] and
    (__ \ "urgentscanrepsendback").writeNullable[Long] and
    (__ \ "dvtreftriage").writeNullable[Long] and
    (__ \ "dvtrefdatebook").writeNullable[Long] and
    (__ \ "dvtrefscan").writeNullable[Long] and
    (__ \ "dvtscanrepsendback").writeNullable[Long] and
    (__ \ "expiredstatus").writeNullable[Boolean] and
    (__ \ "regionid").writeNullable[Long] and
    (__ \ "slavalidfrom").write[java.sql.Date] and
    (__ \ "slavalidto").write[java.sql.Date])(unlift(ExpiredSlaRecordObj.unapply))
  implicit val ExpiredSlaRecordObjFormat: Format[ExpiredSlaRecordObj] = Format(ExpiredSlaRecordObjReads, ExpiredSlaRecordObjWrites)

}


case class updateDocHospitalObj(hospitalRecord: Array[updateDocHospitalRecord], DoctorID: Long, USRId: Int, USRType: String)

object updateDocHospitalObj {
  val updateDocHospitalObjReads = (
    (__ \ "hospitalRecord").read[Array[updateDocHospitalRecord]] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updateDocHospitalObj.apply _)

  val updateDocHospitalObjWrites = (
    (__ \ "hospitalRecord").write[Array[updateDocHospitalRecord]] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(updateDocHospitalObj.unapply))
  implicit val updateDocHospitalObjFormat: Format[updateDocHospitalObj] = Format(updateDocHospitalObjReads, updateDocHospitalObjWrites)

}

case class updateDocHospitalRecord(DoctorHospitalID: Long, HospitalID: Option[Long], MyNetworkDoctorID: Long, PrimaryIND: Option[String],ActiveIND: String)

object updateDocHospitalRecord {
  val DocHospitalRecordReads = (
    (__ \ "DoctorHospitalID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "MyNetworkDoctorID").read[Long] and
    (__ \ "PrimaryIND").readNullable[String] and
    (__ \ "ActiveIND").read[String])(updateDocHospitalRecord.apply _)

  val DocHospitalRecordWrites = (
    (__ \ "DoctorHospitalID").write[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "MyNetworkDoctorID").write[Long] and
    (__ \ "PrimaryIND").writeNullable[String] and 
    (__ \ "ActiveIND").write[String])(unlift(updateDocHospitalRecord.unapply))
  implicit val DocHospitalRecordFormat: Format[updateDocHospitalRecord] = Format(DocHospitalRecordReads, DocHospitalRecordWrites)

}
