/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctor.Json.Request.Careteam

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime






case class addCcgroupsObj(Contractdetails: Option[addContractdetails], ccgid: Option[Long], regionid: Long, ccgname: String, ccgreference: Option[String],
                          primarycontactperson: Option[String], primarycontactno: Option[String], primarycontactemail: Option[String],
                          officesserved: Option[String], emponsystem: Option[String], localcontact: Option[String],
                          assistant: Option[String], invoice: Option[String], contractrenewalremain: Option[java.sql.Date], paymentterms: Option[String],
                          active: Option[Boolean], usrid: Long, usrtype: String, hospitalcd: Option[String])

object addCcgroupsObj {
  val addCcgroupsObjReads = (
    (__ \ "Contractdetails").readNullable[addContractdetails] and
    (__ \ "ccgid").readNullable[Long] and
    (__ \ "regionid").read[Long] and
    (__ \ "ccgname").read[String] and
    (__ \ "ccgreference").readNullable[String] and
    (__ \ "primarycontactperson").readNullable[String] and
    (__ \ "primarycontactno").readNullable[String] and
    (__ \ "primarycontactemail").readNullable[String] and
    (__ \ "officesserved").readNullable[String] and
    (__ \ "emponsystem").readNullable[String] and
    (__ \ "localcontact").readNullable[String] and
    (__ \ "assistant").readNullable[String] and
    (__ \ "invoice").readNullable[String] and
    (__ \ "contractrenewalremain").readNullable[java.sql.Date] and
    (__ \ "paymentterms").readNullable[String] and
    (__ \ "active").readNullable[Boolean] and
    (__ \ "usrid").read[Long] and
    (__ \ "usrtype").read[String] and
    (__ \ "hospitalcd").readNullable[String])(addCcgroupsObj.apply _)

  val addCcgroupsObjWrites = (
    (__ \ "Contractdetails").writeNullable[addContractdetails] and
    (__ \ "ccgid").writeNullable[Long] and
    (__ \ "regionid").write[Long] and
    (__ \ "ccgname").write[String] and
    (__ \ "ccgreference").writeNullable[String] and
    (__ \ "primarycontactperson").writeNullable[String] and
    (__ \ "primarycontactno").writeNullable[String] and
    (__ \ "primarycontactemail").writeNullable[String] and
    (__ \ "officesserved").writeNullable[String] and
    (__ \ "emponsystem").writeNullable[String] and
    (__ \ "localcontact").writeNullable[String] and
    (__ \ "assistant").writeNullable[String] and
    (__ \ "invoice").writeNullable[String] and
    (__ \ "contractrenewalremain").writeNullable[java.sql.Date] and
    (__ \ "paymentterms").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean] and
    (__ \ "usrid").write[Long] and
    (__ \ "usrtype").write[String] and
    (__ \ "hospitalcd").writeNullable[String])(unlift(addCcgroupsObj.unapply))
  implicit val addCcgroupsObjFormat: Format[addCcgroupsObj] = Format(addCcgroupsObjReads, addCcgroupsObjWrites)

}

case class listCcgroupsObj(ccgid: Option[Long], regionid: Long, ccgname: String, ccgreference: Option[String],
                           primarycontactperson: Option[String], primarycontactno: Option[String], primarycontactemail: Option[String],
                           officesserved: Option[String], emponsystem: Option[String], localcontact: Option[String],
                           assistant: Option[String], invoice: Option[String], contractrenewalremain: Option[java.sql.Date], paymentterms: Option[String],
                           active: Option[Boolean], Contractdetails: Option[Array[addContractdetails]], ExpiredContractdetails: Option[Array[addexpiredContractdetails]])

object listCcgroupsObj {
  val listCcgroupsObjReads = (
    (__ \ "ccgid").readNullable[Long] and
    (__ \ "regionid").read[Long] and
    (__ \ "ccgname").read[String] and
    (__ \ "ccgreference").readNullable[String] and
    (__ \ "primarycontactperson").readNullable[String] and
    (__ \ "primarycontactno").readNullable[String] and
    (__ \ "primarycontactemail").readNullable[String] and
    (__ \ "officesserved").readNullable[String] and
    (__ \ "emponsystem").readNullable[String] and
    (__ \ "localcontact").readNullable[String] and
    (__ \ "assistant").readNullable[String] and
    (__ \ "invoice").readNullable[String] and
    (__ \ "contractrenewalremain").readNullable[java.sql.Date] and
    (__ \ "paymentterms").readNullable[String] and
    (__ \ "active").readNullable[Boolean] and
    (__ \ "Contractdetails").readNullable[Array[addContractdetails]] and
    (__ \ "ExpiredContractdetails").readNullable[Array[addexpiredContractdetails]])(listCcgroupsObj.apply _)

  val listCcgroupsObjWrites = (
    (__ \ "ccgid").writeNullable[Long] and
    (__ \ "regionid").write[Long] and
    (__ \ "ccgname").write[String] and
    (__ \ "ccgreference").writeNullable[String] and
    (__ \ "primarycontactperson").writeNullable[String] and
    (__ \ "primarycontactno").writeNullable[String] and
    (__ \ "primarycontactemail").writeNullable[String] and
    (__ \ "officesserved").writeNullable[String] and
    (__ \ "emponsystem").writeNullable[String] and
    (__ \ "localcontact").writeNullable[String] and
    (__ \ "assistant").writeNullable[String] and
    (__ \ "invoice").writeNullable[String] and
    (__ \ "contractrenewalremain").writeNullable[java.sql.Date] and
    (__ \ "paymentterms").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean] and
    (__ \ "Contractdetails").writeNullable[Array[addContractdetails]] and
    (__ \ "ExpiredContractdetails").writeNullable[Array[addexpiredContractdetails]])(unlift(listCcgroupsObj.unapply))
  implicit val listCcgroupsObjFormat: Format[listCcgroupsObj] = Format(listCcgroupsObjReads, listCcgroupsObjWrites)

}

case class AlllistCcgroupsObj(ccgid: Option[Long], regionid: Long, regionname: Option[String], ccgname: String, contactperson: Option[String],
                              contactnumber: Option[String], email: Option[String], active: Option[Boolean], practicedetailval: Option[JsValue])

object AlllistCcgroupsObj {
  val AlllistCcgroupsObjReads = (
    (__ \ "ccgid").readNullable[Long] and
    (__ \ "regionid").read[Long] and
    (__ \ "regionname").readNullable[String] and
    (__ \ "ccgname").read[String] and
    (__ \ "contactperson").readNullable[String] and
    (__ \ "contactnumber").readNullable[String] and
    (__ \ "email").readNullable[String] and
    (__ \ "active").readNullable[Boolean] and
    (__ \ "practicedetailval").readNullable[JsValue])(AlllistCcgroupsObj.apply _)

  val AlllistCcgroupsObjWrites = (
    (__ \ "ccgid").writeNullable[Long] and
    (__ \ "regionid").write[Long] and
    (__ \ "regionname").writeNullable[String] and
    (__ \ "ccgname").write[String] and
    (__ \ "contactperson").writeNullable[String] and
    (__ \ "contactnumber").writeNullable[String] and
    (__ \ "email").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean] and
    (__ \ "practicedetailval").writeNullable[JsValue])(unlift(AlllistCcgroupsObj.unapply))
  implicit val AlllistCcgroupsObjFormat: Format[AlllistCcgroupsObj] = Format(AlllistCcgroupsObjReads, AlllistCcgroupsObjWrites)

}

case class specialityLkupObj(subSpecialityID: Option[String], specialitydesc: String, activeIND: String, dicomcode: Option[String], usernotes: Option[String], hospitalId: Option[Long])

object specialityLkupObj {
  val specialityLkupObjReads = (
    (__ \ "subSpecialityID").readNullable[String] and
    (__ \ "specialitydesc").read[String] and
    (__ \ "activeIND").read[String] and
    (__ \ "dicomcode").readNullable[String] and
    (__ \ "usernotes").readNullable[String] and
    (__ \ "hospitalId").readNullable[Long])(specialityLkupObj.apply _)

  val specialityLkupObjWrites = (  
    (__ \ "subSpecialityID").writeNullable[String] and
    (__ \ "specialitydesc").write[String] and
    (__ \ "activeIND").write[String] and
    (__ \ "dicomcode").writeNullable[String] and
    (__ \ "usernotes").writeNullable[String] and
    (__ \ "hospitalId").writeNullable[Long])(unlift(specialityLkupObj.unapply))
  implicit val specialityLkupObjFormat: Format[specialityLkupObj] = Format(specialityLkupObjReads, specialityLkupObjWrites)

}


case class specialityLkupDetailsObj(CategoryID: Option[Long],updtusr: Option[Long], updtusrtype: String, specialityID: Option[String], specialitytype: Option[String], speciality: Array[specialityLkupObj])

object specialityLkupDetailsObj {
  val specialityLkupDetailsObjReads = (
    (__ \ "CategoryID").readNullable[Long] and
    (__ \ "updtusr").readNullable[Long] and
    (__ \ "updtusrtype").read[String] and
    (__ \ "specialityID").readNullable[String] and
    (__ \ "specialitytype").readNullable[String] and
    (__ \ "speciality").read[Array[specialityLkupObj]])(specialityLkupDetailsObj.apply _)

  val specialityLkupDetailsObjWrites = (
    (__ \ "CategoryID").writeNullable[Long] and
    (__ \ "updtusr").writeNullable[Long] and
    (__ \ "updtusrtype").write[String] and
    (__ \ "specialityID").writeNullable[String] and
    (__ \ "specialitytype").writeNullable[String] and
    (__ \ "speciality").write[Array[specialityLkupObj]])(unlift(specialityLkupDetailsObj.unapply))
  implicit val specialityLkupDetailsObjFormat: Format[specialityLkupDetailsObj] = Format(specialityLkupDetailsObjReads, specialityLkupDetailsObjWrites)

}
//new typed
case class  SpecialityCtgyDetailsObj(CategoryID: Option[Long], CategoryName: String,Description: Option[String],HospitalID : Long, Created: Option[java.sql.Date],CrtUSR: Option[Long],CrtUSRType: Option[String],Modified: Option[java.sql.Date],UpdtUSR: Option[Long], UpdtUSRType: Option[String])

object SpecialityCtgyDetailsObj {
  val SpecialityCtgyDetailsObjReads = (
    (__ \ "CategoryID").readNullable[Long] and
    (__ \ "CategoryName").read[String] and
    (__ \ "Description").readNullable[String] and
    (__ \ "HospitalID").read[Long] and
    (__ \ "Created").readNullable[java.sql.Date] and
    (__ \ "CrtUSR").readNullable[Long] and
    (__ \ "CrtUSRType").readNullable[String] and
    (__ \ "Modified").readNullable[java.sql.Date] and
    (__ \ "UpdtUSR").readNullable[Long] and
    (__ \ "UpdtUSRType").readNullable[String] )(SpecialityCtgyDetailsObj.apply _)

  val SpecialityCtgyDetailsObjWrites = (
    (__ \ "CategoryID").writeNullable[Long] and
    (__ \ "CategoryName").write[String] and
    (__ \ "Description").writeNullable[String] and
    (__ \ "HospitalID").write[Long] and
    (__ \ "Created").writeNullable[java.sql.Date] and
    (__ \ "CrtUSR").writeNullable[Long] and
    (__ \ "CrtUSRType").writeNullable[String] and
    (__ \ "Modified").writeNullable[java.sql.Date] and
    (__ \ "UpdtUSR").writeNullable[Long] and
    (__ \ "UpdtUSRType").writeNullable[String] )(unlift(SpecialityCtgyDetailsObj.unapply))
  implicit val SpecialityCtgyDetailsObjFormat: Format[SpecialityCtgyDetailsObj] = Format(SpecialityCtgyDetailsObjReads, SpecialityCtgyDetailsObjWrites)

}

case class getCcgroupsObj(ccgid: Option[Long], usrid: Long, usrtype: String, pageNumber: Option[Int], pageSize: Option[Int], hospitalcd: Option[String], searchtext: Option[String])

object getCcgroupsObj {
  val getCcgroupsObjReads = (
    (__ \ "ccgid").readNullable[Long] and
    (__ \ "usrid").read[Long] and
    (__ \ "usrtype").read[String] and
    (__ \ "pageNumber").readNullable[Int] and
    (__ \ "pageSize").readNullable[Int] and
    (__ \ "hospitalcd").readNullable[String] and
    (__ \ "searchtext").readNullable[String])(getCcgroupsObj.apply _)

  val getCcgroupsObjWrites = (
    (__ \ "ccgid").writeNullable[Long] and
    (__ \ "usrid").write[Long] and
    (__ \ "usrtype").write[String] and
    (__ \ "pageNumber").writeNullable[Int] and
    (__ \ "pageSize").writeNullable[Int] and
    (__ \ "hospitalcd").writeNullable[String] and
    (__ \ "searchtext").writeNullable[String])(unlift(getCcgroupsObj.unapply))
  implicit val getCcgroupsObjFormat: Format[getCcgroupsObj] = Format(getCcgroupsObjReads, getCcgroupsObjWrites)

}

case class addContractdetails(contactdetailsid: Option[Long], expiredstatus: Boolean,
                              contractstartdate: java.sql.Date, contractenddate: java.sql.Date,
                              minimunageforref: Option[Int], lessthanminutes: Option[Long], greaterthanminutes: Option[Long], tariffinfodvt: Option[String], speciality: Option[Map[String, String]], ccgid: Option[Long], Contractspeciality: Option[Array[addContractspeciality]])

object addContractdetails {
  val addContractdetailsReads = (
    (__ \ "contactdetailsid").readNullable[Long] and
    (__ \ "expiredstatus").read[Boolean] and
    (__ \ "contractstartdate").read[java.sql.Date] and
    (__ \ "contractenddate").read[java.sql.Date] and
    (__ \ "minimunageforref").readNullable[Int] and
    (__ \ "lessthanminutes").readNullable[Long] and
    (__ \ "greaterthanminutes").readNullable[Long] and
    (__ \ "tariffinfodvt").readNullable[String] and
    (__ \ "speciality").readNullable[Map[String, String]] and
    (__ \ "ccgid").readNullable[Long] and
    (__ \ "Contractspeciality").readNullable[Array[addContractspeciality]])(addContractdetails.apply _)

  val addContractdetailsWrites = (
    (__ \ "contactdetailsid").writeNullable[Long] and
    (__ \ "expiredstatus").write[Boolean] and
    (__ \ "contractstartdate").write[java.sql.Date] and
    (__ \ "contractenddate").write[java.sql.Date] and
    (__ \ "minimunageforref").writeNullable[Int] and
    (__ \ "lessthanminutes").writeNullable[Long] and
    (__ \ "greaterthanminutes").writeNullable[Long] and
    (__ \ "tariffinfodvt").writeNullable[String] and
    (__ \ "speciality").writeNullable[Map[String, String]] and
    (__ \ "ccgid").writeNullable[Long] and
    (__ \ "Contractspeciality").writeNullable[Array[addContractspeciality]])(unlift(addContractdetails.unapply))
  implicit val addContractdetailsFormat: Format[addContractdetails] = Format(addContractdetailsReads, addContractdetailsWrites)

}

case class addexpiredContractdetails(contactdetailsid: Option[Long], expiredstatus: Boolean,
                                     contractstartdate: java.sql.Date, contractenddate: java.sql.Date,
                                     minimunageforref: Option[Int], lessthanminutes: Option[Long], greaterthanminutes: Option[Long], tariffinfodvt: Option[String], speciality: Option[Map[String, String]], ccgid: Option[Long], Contractspeciality: Option[Array[addContractspeciality]])

object addexpiredContractdetails {
  val addexpiredContractdetailsReads = (
    (__ \ "contactdetailsid").readNullable[Long] and
    (__ \ "expiredstatus").read[Boolean] and
    (__ \ "contractstartdate").read[java.sql.Date] and
    (__ \ "contractenddate").read[java.sql.Date] and
    (__ \ "minimunageforref").readNullable[Int] and
    (__ \ "lessthanminutes").readNullable[Long] and
    (__ \ "greaterthanminutes").readNullable[Long] and
    (__ \ "tariffinfodvt").readNullable[String] and
    (__ \ "speciality").readNullable[Map[String, String]] and
    (__ \ "ccgid").readNullable[Long] and
    (__ \ "Contractspeciality").readNullable[Array[addContractspeciality]])(addexpiredContractdetails.apply _)

  val addexpiredContractdetailsWrites = (
    (__ \ "contactdetailsid").writeNullable[Long] and
    (__ \ "expiredstatus").write[Boolean] and
    (__ \ "contractstartdate").write[java.sql.Date] and
    (__ \ "contractenddate").write[java.sql.Date] and
    (__ \ "minimunageforref").writeNullable[Int] and
    (__ \ "lessthanminutes").writeNullable[Long] and
    (__ \ "greaterthanminutes").writeNullable[Long] and
    (__ \ "tariffinfodvt").writeNullable[String] and
    (__ \ "speciality").writeNullable[Map[String, String]] and
    (__ \ "ccgid").writeNullable[Long] and
    (__ \ "Contractspeciality").writeNullable[Array[addContractspeciality]])(unlift(addexpiredContractdetails.unapply))
  implicit val addexpiredContractdetailsFormat: Format[addexpiredContractdetails] = Format(addexpiredContractdetailsReads, addexpiredContractdetailsWrites)

}

case class addContractspeciality(contactspecialityid: Option[Long], Speciality: Option[String], SpecialityName: Option[String],
                                 lessthanminutes: Option[BigDecimal], greaterthanminutes: Option[BigDecimal], fixedfee: Option[BigDecimal], ccgid: Option[Long], contactdetailsid: Option[Long], active: Option[Boolean])

object addContractspeciality {
  val addContractspecialityReads = (
    (__ \ "contactspecialityid").readNullable[Long] and
    (__ \ "Speciality").readNullable[String] and
    (__ \ "SpecialityName").readNullable[String] and
    (__ \ "lessthanminutes").readNullable[BigDecimal] and
    (__ \ "greaterthanminutes").readNullable[BigDecimal] and
    (__ \ "fixedfee").readNullable[BigDecimal] and
    (__ \ "ccgid").readNullable[Long] and
    (__ \ "contactdetailsid").readNullable[Long] and
    (__ \ "active").readNullable[Boolean])(addContractspeciality.apply _)

  val addContractspecialityWrites = (
    (__ \ "contactspecialityid").writeNullable[Long] and
    (__ \ "Speciality").writeNullable[String] and
    (__ \ "SpecialityName").writeNullable[String] and
    (__ \ "lessthanminutes").writeNullable[BigDecimal] and
    (__ \ "greaterthanminutes").writeNullable[BigDecimal] and
    (__ \ "fixedfee").writeNullable[BigDecimal] and
    (__ \ "ccgid").writeNullable[Long] and
    (__ \ "contactdetailsid").writeNullable[Long] and
    (__ \ "active").writeNullable[Boolean])(unlift(addContractspeciality.unapply))
  implicit val addContractdetailsFormat: Format[addContractspeciality] = Format(addContractspecialityReads, addContractspecialityWrites)

}

case class updateDocHospitalObj(hospitalRecord: Array[updateDocHospitalRecord], DoctorID: Long, USRId: Int, USRType: String)

object updateDocHospitalObj {
  val updateDocHospitalObjReads = (
    (__ \ "hospitalRecord").read[Array[updateDocHospitalRecord]] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updateDocHospitalObj.apply _)

  val updateDocHospitalObjWrites = (
    (__ \ "hospitalRecord").write[Array[updateDocHospitalRecord]] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(updateDocHospitalObj.unapply))
  implicit val updateDocHospitalObjFormat: Format[updateDocHospitalObj] = Format(updateDocHospitalObjReads, updateDocHospitalObjWrites)

}

case class updateDocHospitalRecord(DoctorHospitalID: Long, HospitalID: Option[Long], MyNetworkDoctorID: Long, PrimaryIND: Option[String], ActiveIND: String)

object updateDocHospitalRecord {
  val DocHospitalRecordReads = (
    (__ \ "DoctorHospitalID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "MyNetworkDoctorID").read[Long] and
    (__ \ "PrimaryIND").readNullable[String] and
    (__ \ "ActiveIND").read[String])(updateDocHospitalRecord.apply _)

  val DocHospitalRecordWrites = (
    (__ \ "DoctorHospitalID").write[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "MyNetworkDoctorID").write[Long] and
    (__ \ "PrimaryIND").writeNullable[String] and
    (__ \ "ActiveIND").write[String])(unlift(updateDocHospitalRecord.unapply))
  implicit val DocHospitalRecordFormat: Format[updateDocHospitalRecord] = Format(DocHospitalRecordReads, DocHospitalRecordWrites)

}
