package models.com.hcue.doctor.Json.Request.Careteam

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime


object SmsRequestValidation {
  
case class appointmentremindersmsObj(smsto: Option[String],smsfrom: Option[String],smsmessage: Option[String], appointemntid : Option[Long], hospitalid : Option[Long], patientid: Option[Long], doctorid : Option[Long])

object appointmentremindersmsObj {
  val appointmentremindersmsObjReads = (
    (__ \ "smsto").readNullable[String] and
    (__ \ "smsfrom").readNullable[String] and
    (__ \ "smsmessage").readNullable[String] and 
    (__ \ "appointmentid").readNullable[Long] and 
    (__ \ "hospitalid").readNullable[Long] and
    (__ \ "patientid").readNullable[Long] and
    (__ \ "doctorid").readNullable[Long])(appointmentremindersmsObj.apply _)

  val appointmentremindersmsObjWrites = (
    (__ \ "smsto").writeNullable[String] and
    (__ \ "smsfrom").writeNullable[String] and
    (__ \ "smsmessage").writeNullable[String] and 
    (__ \ "appointmentid").writeNullable[Long] and
    (__ \ "hospitalid").writeNullable[Long] and
    (__ \ "patientid").writeNullable[Long] and
    (__ \ "doctorid").writeNullable[Long])(unlift(appointmentremindersmsObj.unapply))
  implicit val getPostcodehospitalObjFormat: Format[appointmentremindersmsObj] = Format(appointmentremindersmsObjReads, appointmentremindersmsObjWrites)

}

}