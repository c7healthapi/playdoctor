/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctor.Json.Request.Careteam

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime

case class addPracticeObj(practiceid:Option[Long],ccgid:Long,practicename: String,
    practicecode: Option[String],postcode: Option[String],
    addressdetl: Option[addressdetl],
    citytown: Option[String],
    county: Option[String], country: Option[String],description: Option[String],
    faxno: Option[String], phonedetail: Option[phonedetail],
    emaildetl:Option[emaildetl], 
    acceptsfaxmsg: Option[Boolean],odscode: Option[String],
    autopostcom: Option[Boolean], practicepopulation: Option[String],meshidentifier: Option[String],
    active: Option[Boolean],usrid: Long, hospitalcd: Option[String])


object addPracticeObj {
  val addPracticeObjReads = (
    (__ \ "practiceid").readNullable[Long] and
    (__ \ "ccgid").read[Long] and
    (__ \ "practicename").read[String] and
    (__ \ "practicecode").readNullable[String] and
    (__ \ "postcode").readNullable[String] and
    (__ \ "addressdetl").readNullable[addressdetl] and
    (__ \ "citytown").readNullable[String] and
    (__ \ "county").readNullable[String] and
    (__ \ "country").readNullable[String] and
    (__ \ "description").readNullable[String] and  
    (__ \ "faxno").readNullable[String] and
    (__ \ "phonedetail").readNullable[phonedetail] and
    (__ \ "emaildetl").readNullable[emaildetl] and
    (__ \ "acceptsfaxmsg").readNullable[Boolean] and
    (__ \ "odscode").readNullable[String] and
    (__ \ "autopostcom").readNullable[Boolean] and
    (__ \ "practicepopulation").readNullable[String] and
    (__ \ "meshidentifier").readNullable[String] and
    (__ \ "active").readNullable[Boolean] and
    (__ \ "usrid").read[Long] and
    (__ \ "hospitalcd").readNullable[String])(addPracticeObj.apply _)

  val addPracticeObjWrites = (
    (__ \ "practiceid").writeNullable[Long] and
    (__ \ "ccgid").write[Long] and
    (__ \ "practicename").write[String] and
    (__ \ "practicecode").writeNullable[String] and
    (__ \ "postcode").writeNullable[String] and
    (__ \ "addressdetl").writeNullable[addressdetl] and
    (__ \ "citytown").writeNullable[String] and
    (__ \ "county").writeNullable[String] and
    (__ \ "country").writeNullable[String] and
    (__ \ "description").writeNullable[String] and  
    (__ \ "faxno").writeNullable[String] and
    (__ \ "phonedetail").writeNullable[phonedetail] and
    (__ \ "emaildetl").writeNullable[emaildetl] and
    (__ \ "acceptsfaxmsg").writeNullable[Boolean] and
    (__ \ "odscode").writeNullable[String] and
    (__ \ "autopostcom").writeNullable[Boolean] and
    (__ \ "practicepopulation").writeNullable[String] and
    (__ \ "meshidentifier").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean] and
    (__ \ "usrid").write[Long] and
    (__ \ "hospitalcd").writeNullable[String])(unlift(addPracticeObj.unapply))
  implicit val addPracticeObjFormat: Format[addPracticeObj] = Format(addPracticeObjReads, addPracticeObjWrites)

}



case class listPracticeObj(practiceid:Option[Long],ccgid:Long,practicename: String,
    practicecode: Option[String],postcode: Option[String],
    addressdetl: Option[addressdetl],
    citytown: Option[String],
    county: Option[String], country: Option[String],description: Option[String],
    faxno: Option[String], phonedetail: Option[phonedetail],
    emaildetl:Option[emaildetl], 
    acceptsfaxmsg: Option[Boolean],odscode: Option[String],
    autopostcom: Option[Boolean], practicepopulation: Option[String],meshidentifier: Option[String],active: Option[Boolean])

object listPracticeObj {
  val listPracticeObjReads = (
    (__ \ "practiceid").readNullable[Long] and
    (__ \ "ccgid").read[Long] and
    (__ \ "practicename").read[String] and
    (__ \ "practicecode").readNullable[String] and
    (__ \ "postcode").readNullable[String] and
    (__ \ "addressdetl").readNullable[addressdetl] and
    (__ \ "citytown").readNullable[String] and
    (__ \ "county").readNullable[String] and
    (__ \ "country").readNullable[String] and
    (__ \ "description").readNullable[String] and  
    (__ \ "faxno").readNullable[String] and
    (__ \ "phonedetail").readNullable[phonedetail] and
    (__ \ "emaildetl").readNullable[emaildetl] and
    (__ \ "acceptsfaxmsg").readNullable[Boolean] and
    (__ \ "odscode").readNullable[String] and
    (__ \ "autopostcom").readNullable[Boolean] and
    (__ \ "practicepopulation").readNullable[String] and
    (__ \ "meshidentifier").readNullable[String] and 
    (__ \ "active").readNullable[Boolean])(listPracticeObj.apply _)

  val listPracticeObjWrites = (
    (__ \ "practiceid").writeNullable[Long] and
    (__ \ "ccgid").write[Long] and
    (__ \ "practicename").write[String] and
    (__ \ "practicecode").writeNullable[String] and
    (__ \ "postcode").writeNullable[String] and
    (__ \ "addressdetl").writeNullable[addressdetl] and
    (__ \ "citytown").writeNullable[String] and
    (__ \ "county").writeNullable[String] and
    (__ \ "country").writeNullable[String] and
    (__ \ "description").writeNullable[String] and  
    (__ \ "faxno").writeNullable[String] and
    (__ \ "phonedetail").writeNullable[phonedetail] and
    (__ \ "emaildetl").writeNullable[emaildetl] and
    (__ \ "acceptsfaxmsg").writeNullable[Boolean] and
    (__ \ "odscode").writeNullable[String] and
    (__ \ "autopostcom").writeNullable[Boolean] and
    (__ \ "practicepopulation").writeNullable[String] and
    (__ \ "meshidentifier").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean])(unlift(listPracticeObj.unapply))
  implicit val listPracticeObjFormat: Format[listPracticeObj] = Format(listPracticeObjReads, listPracticeObjWrites)

}

case class listAllPracticeObj(practiceid:Option[Long],practicename: String,practicecode: Option[String],ccgid:Long,
    ccgname: Option[String],postcode:Option[String],contactnumber: Option[String],email: Option[String],active: Option[Boolean],practicewithccgname: Option[String],doctordetailval:Option[JsValue])

object listAllPracticeObj {
  val listAllPracticeObjReads = (
    (__ \ "practiceid").readNullable[Long] and
    (__ \ "practicename").read[String] and
    (__ \ "practicecode").readNullable[String] and
    (__ \ "ccgid").read[Long] and
    (__ \ "ccgname").readNullable[String] and
    (__ \ "postcode").readNullable[String] and
    (__ \ "contactnumber").readNullable[String] and
    (__ \ "email").readNullable[String] and
    (__ \ "active").readNullable[Boolean] and 
    (__ \ "practicewithccgname").readNullable[String] and 
    (__ \ "doctordetailval").readNullable[JsValue])(listAllPracticeObj.apply _)

  val listAllPracticeObjWrites = (
    (__ \ "practiceid").writeNullable[Long] and
    (__ \ "practicename").write[String] and
    (__ \ "practicecode").writeNullable[String] and
    (__ \ "ccgid").write[Long] and
    (__ \ "ccgname").writeNullable[String] and
    (__ \ "postcode").writeNullable[String] and
    (__ \ "contactnumber").writeNullable[String] and
    (__ \ "email").writeNullable[String] and
    (__ \ "active").writeNullable[Boolean] and
    (__ \ "practicewithccgname").writeNullable[String] and
    (__ \ "doctordetailval").writeNullable[JsValue])(unlift(listAllPracticeObj.unapply))
  implicit val listAllPracticeObjFormat: Format[listAllPracticeObj] = Format(listAllPracticeObjReads, listAllPracticeObjWrites)

}

case class listdoctorPracticeObj(doctorid:Long,doctorname: String,addressid:Option[Long])

object listdoctorPracticeObj {
  val listdoctorPracticeObjReads = (
    (__ \ "doctorid").read[Long] and
    (__ \ "doctorname").read[String] and
    (__ \ "addressid").readNullable[Long])(listdoctorPracticeObj.apply _)

  val listdoctorPracticeObjWrites = (
    (__ \ "doctorid").write[Long] and
    (__ \ "doctorname").write[String] and
    (__ \ "addressid").writeNullable[Long])(unlift(listdoctorPracticeObj.unapply))
  implicit val listdoctorPracticeObjFormat: Format[listdoctorPracticeObj] = Format(listdoctorPracticeObjReads, listdoctorPracticeObjWrites)

}


case class listpostcodehospitalObj(HospitalName:Option[String],HospitalID: Option[Long],PostCode: Option[String],Distance: Option[String],
                                   ClinicAddress: Option[String],ClinicNotes: Option[String],ClinicPhnumber: Option[Long])

object listpostcodehospitalObj {
  val listpostcodehospitalObjReads = (
    (__ \ "HospitalName").readNullable[String] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "PostCode").readNullable[String] and
    (__ \ "Distance").readNullable[String] and
    (__ \ "ClinicAddress").readNullable[String] and
    (__ \ "ClinicNotes").readNullable[String] and
    (__ \ "ClinicPhnumber").readNullable[Long])(listpostcodehospitalObj.apply _)

  val listpostcodehospitalObjWrites = (
    (__ \ "HospitalName").writeNullable[String] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "PostCode").writeNullable[String] and
    (__ \ "Distance").writeNullable[String] and
    (__ \ "ClinicAddress").writeNullable[String] and
    (__ \ "ClinicNotes").writeNullable[String] and
    (__ \ "ClinicPhnumber").writeNullable[Long])(unlift(listpostcodehospitalObj.unapply))
  implicit val listpostcodehospitalObjFormat: Format[listpostcodehospitalObj] = Format(listpostcodehospitalObjReads, listpostcodehospitalObjWrites)

}

case class distancepostcodeObj(destination_addresses:Option[Array[String]],origin_addresses: Option[Array[String]],
    rows: Array[rowsObj],status: Option[String])

object distancepostcodeObj {
  val distancepostcodeObjReads = (
    (__ \ "destination_addresses").readNullable[Array[String]] and
    (__ \ "origin_addresses").readNullable[Array[String]] and
    (__ \ "rows").read[Array[rowsObj]] and
    (__ \ "status").readNullable[String])(distancepostcodeObj.apply _)

  val distancepostcodeObjWrites = (
    (__ \ "destination_addresses").writeNullable[Array[String]] and
    (__ \ "origin_addresses").writeNullable[Array[String]] and
    (__ \ "rows").write[Array[rowsObj]] and
    (__ \ "status").writeNullable[String])(unlift(distancepostcodeObj.unapply))
  implicit val listpostcodehospitalObjFormat: Format[distancepostcodeObj] = Format(distancepostcodeObjReads, distancepostcodeObjWrites)

}

case class rowsObj(elements:Array[elementObj],status:Option[String])

object rowsObj {
  val rowsObjReads = (
    (__ \ "elements").read[Array[elementObj]] and
    (__ \ "status").readNullable[String])(rowsObj.apply _)

  val rowsObjWrites = (
    (__ \ "elements").write[Array[elementObj]] and
    (__ \ "status").writeNullable[String])(unlift(rowsObj.unapply))
  implicit val elementObjFormat: Format[rowsObj] = Format(rowsObjReads, rowsObjWrites)

}

case class elementObj(distance:Option[distanceObj],duration: Option[durationObj],status: Option[String])

object elementObj {
  val elementObjReads = (
    (__ \ "distance").readNullable[distanceObj] and
    (__ \ "duration").readNullable[durationObj] and
    (__ \ "status").readNullable[String])(elementObj.apply _)

  val elementObjWrites = (
    (__ \ "distance").writeNullable[distanceObj] and
    (__ \ "duration").writeNullable[durationObj] and
    (__ \ "status").writeNullable[String])(unlift(elementObj.unapply))
  implicit val elementObjFormat: Format[elementObj] = Format(elementObjReads, elementObjWrites)

}

case class distanceObj(text:Option[String],value: Option[Long])

object distanceObj {
  val distanceObjReads = (
    (__ \ "text").readNullable[String] and
    (__ \ "value").readNullable[Long])(distanceObj.apply _)

  val distanceObjWrites = (
    (__ \ "text").writeNullable[String] and
    (__ \ "value").writeNullable[Long])(unlift(distanceObj.unapply))
  implicit val elementObjFormat: Format[distanceObj] = Format(distanceObjReads, distanceObjWrites)

}

case class durationObj(text:Option[String],value: Option[Long])

object durationObj {
  val distanceObjReads = (
    (__ \ "text").readNullable[String] and
    (__ \ "value").readNullable[Long])(durationObj.apply _)

  val durationObjWrites = (
    (__ \ "text").writeNullable[String] and
    (__ \ "value").writeNullable[Long])(unlift(durationObj.unapply))
  implicit val elementObjFormat: Format[durationObj] = Format(distanceObjReads, durationObjWrites)

}


case class getPracticeObj(practiceid: Option[Long],usrid: Long, usrtype: String, pageNumber: Option[Int], hospitalcd: Option[String], pageSize: Option[Int], searchtext: Option[String])

object getPracticeObj {
  val getPracticeObjReads = (
    (__ \ "practiceid").readNullable[Long] and
    (__ \ "usrid").read[Long] and
    (__ \ "usrtype").read[String] and
    (__ \ "pageNumber").readNullable[Int] and
    (__ \ "hospitalcd").readNullable[String] and 
    (__ \ "pageSize").readNullable[Int] and
    (__ \ "searchtext").readNullable[String])(getPracticeObj.apply _)

  val getPracticeObjWrites = (
    (__ \ "practiceid").writeNullable[Long] and
    (__ \ "usrid").write[Long] and
    (__ \ "usrtype").write[String] and
    (__ \ "pageNumber").writeNullable[Int] and
    (__ \ "hospitalcd").writeNullable[String] and 
    (__ \ "pageSize").writeNullable[Int] and 
    (__ \ "searchtext").writeNullable[String])(unlift(getPracticeObj.unapply))
  implicit val getPracticeObjFormat: Format[getPracticeObj] = Format(getPracticeObjReads, getPracticeObjWrites)

}



case class getRejectListObj(dwid: Long, dateReceived: java.sql.Date, ccgname: String, reason: Option[String])

object getRejectListObj {
  val getRejectListObjReads = (
    (__ \ "dwid").read[Long] and
    (__ \ "dateReceived").read[java.sql.Date] and
    (__ \ "ccgname").read[String] and
    (__ \ "reason").readNullable[String])(getRejectListObj.apply _)

  val getRejectListObjWrites = (
    (__ \ "dwid").write[Long] and
    (__ \ "dateReceived").write[java.sql.Date] and
    (__ \ "ccgname").write[String] and
    (__ \ "reason").writeNullable[String])(unlift(getRejectListObj.unapply))
  implicit val getRejectListObjFormat: Format[getRejectListObj] = Format(getRejectListObjReads, getRejectListObjWrites)
}




case class getRejectReqObj(hospitalCode: Option[String],  hospitalID: Option[Long],ccgID: Option[Long],fromdate: Option[java.sql.Date],todate: Option[java.sql.Date])

object getRejectReqObj {
  val getRejectReqObjReads = (
    (__ \ "hospitalCode").readNullable[String] and
    (__ \ "hospitalID").readNullable[Long] and
    (__ \ "ccgID").readNullable[Long] and
    (__ \ "fromdate").readNullable[java.sql.Date] and
    (__ \ "todate").readNullable[java.sql.Date])(getRejectReqObj.apply _)

  val getRejectReqObjWrites = (
    (__ \ "hospitalCode").writeNullable[String] and
    (__ \ "hospitalID").writeNullable[Long] and
    (__ \ "ccgID").writeNullable[Long] and
    (__ \ "fromdate").writeNullable[java.sql.Date] and
    (__ \ "todate").writeNullable[java.sql.Date])(unlift(getRejectReqObj.unapply))
  implicit val getRejectReqObjFormat: Format[getRejectReqObj] = Format(getRejectReqObjReads, getRejectReqObjWrites)
}




case class getPostcodehospitalObj(postcode: Option[String],latitude: Option[String],longitude: Option[String],SpecialityCD:Option[Array[String]],ParentHospitalID:Long,usrid: Long, usrtype: String,meterval: Option[Long])

object getPostcodehospitalObj {
  val getPostcodehospitalObjReads = (
    (__ \ "postcode").readNullable[String] and
    (__ \ "latitude").readNullable[String] and
    (__ \ "longitude").readNullable[String] and
    (__ \ "SpecialityCD").readNullable[Array[String]] and
    (__ \ "ParentHospitalID").read[Long] and
    (__ \ "usrid").read[Long] and
    (__ \ "usrtype").read[String] and 
    (__ \ "meterval").readNullable[Long])(getPostcodehospitalObj.apply _)

  val getPostcodehospitalObjWrites = (
    (__ \ "postcode").writeNullable[String] and
    (__ \ "latitude").writeNullable[String] and
    (__ \ "longitude").writeNullable[String] and
    (__ \ "SpecialityCD").writeNullable[Array[String]] and
    (__ \ "ParentHospitalID").write[Long] and
    (__ \ "usrid").write[Long] and
    (__ \ "usrtype").write[String] and
    (__ \ "meterval").writeNullable[Long])(unlift(getPostcodehospitalObj.unapply))
  implicit val getPostcodehospitalObjFormat: Format[getPostcodehospitalObj] = Format(getPostcodehospitalObjReads, getPostcodehospitalObjWrites)

}



case class appointmentremindersmsObj(smsto: Option[String],smsfrom: Option[String],smsmessage: Option[String], appointemntid : Option[Long], hospitalid : Option[Long], patientid: Option[Long], doctorid : Option[Long])

object appointmentremindersmsObj {
  val appointmentremindersmsObjReads = (
    (__ \ "smsto").readNullable[String] and
    (__ \ "smsfrom").readNullable[String] and
    (__ \ "smsmessage").readNullable[String] and 
    (__ \ "appointmentid").readNullable[Long] and 
    (__ \ "hospitalid").readNullable[Long] and
    (__ \ "patientid").readNullable[Long] and
    (__ \ "doctorid").readNullable[Long])(appointmentremindersmsObj.apply _)

  val appointmentremindersmsObjWrites = (
    (__ \ "smsto").writeNullable[String] and
    (__ \ "smsfrom").writeNullable[String] and
    (__ \ "smsmessage").writeNullable[String] and 
    (__ \ "appointmentid").writeNullable[Long] and
    (__ \ "hospitalid").writeNullable[Long] and
    (__ \ "patientid").writeNullable[Long] and
    (__ \ "doctorid").writeNullable[Long])(unlift(appointmentremindersmsObj.unapply))
  implicit val getPostcodehospitalObjFormat: Format[appointmentremindersmsObj] = Format(appointmentremindersmsObjReads, appointmentremindersmsObjWrites)

}

case class getdeactivePracticeObj(practiceid: Option[Long],doctorid: Option[Long],usrid: Long, usrtype: String,active:Option[Boolean])

object getdeactivePracticeObj {
  val getdeactivePracticeObjReads = (
    (__ \ "practiceid").readNullable[Long] and
    (__ \ "doctorid").readNullable[Long] and
    (__ \ "usrid").read[Long] and
    (__ \ "usrtype").read[String] and 
    (__ \ "active").readNullable[Boolean])(getdeactivePracticeObj.apply _)

  val getdeactivePracticeObjWrites = (
    (__ \ "practiceid").writeNullable[Long] and
    (__ \ "doctorid").writeNullable[Long] and
    (__ \ "usrid").write[Long] and
    (__ \ "usrtype").write[String] and 
    (__ \ "active").writeNullable[Boolean])(unlift(getdeactivePracticeObj.unapply))
  implicit val getdeactivePracticeObjFormat: Format[getdeactivePracticeObj] = Format(getdeactivePracticeObjReads, getdeactivePracticeObjWrites)

}



case class addressdetl(address1: Option[String],address2: Option[String],
    address3: Option[String],address4: Option[String])

object addressdetl {
  val addressdetlReads = (
    (__ \ "address1").readNullable[String] and
    (__ \ "address2").readNullable[String] and
    (__ \ "address3").readNullable[String] and
    (__ \ "address4").readNullable[String])(addressdetl.apply _)

  val addressdetlWrites = (
    (__ \ "address1").writeNullable[String] and
    (__ \ "address2").writeNullable[String] and
    (__ \ "address3").writeNullable[String] and
    (__ \ "address4").writeNullable[String])(unlift(addressdetl.unapply))
  implicit val addressdetlFormat: Format[addressdetl] = Format(addressdetlReads, addressdetlWrites)

}


case class emaildetl(practiceemail: Option[String], repadminemail: Option[String],
    appadminemail: Option[String])

object emaildetl {
  val emaildetlReads = (
    (__ \ "practiceemail").readNullable[String] and
    (__ \ "repadminemail").readNullable[String] and
    (__ \ "appadminemail").readNullable[String])(emaildetl.apply _)

  val emaildetlWrites = (
    (__ \ "practiceemail").writeNullable[String] and
    (__ \ "repadminemail").writeNullable[String] and
    (__ \ "appadminemail").writeNullable[String])(unlift(emaildetl.unapply))
  implicit val emaildetlFormat: Format[emaildetl] = Format(emaildetlReads, emaildetlWrites)

}


case class phonedetail(telephoneday1: Option[String],telephoneday2: Option[String])

object phonedetail {
  val phonedetailReads = (
    (__ \ "telephoneday1").readNullable[String] and
    (__ \ "telephoneday2").readNullable[String])(phonedetail.apply _)

  val phonedetailWrites = (
    (__ \ "telephoneday1").writeNullable[String] and
    (__ \ "telephoneday2").writeNullable[String])(unlift(phonedetail.unapply))
  implicit val emaildetlFormat: Format[phonedetail] = Format(phonedetailReads, phonedetailWrites)

}





