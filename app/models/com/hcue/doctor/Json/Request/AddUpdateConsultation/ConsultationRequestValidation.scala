/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctor.Json.Request.AddUpdateConsultation

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime

case class addConsultationObj(consultationRecord: Array[addConsultationRecord], AddressID: Long, DoctorID: Long, USRId: Int, USRType: String)

object addConsultationObj {
  val addConsultationObjReads = (
    (__ \ "consultationRecord").read[Array[addConsultationRecord]] and
    (__ \ "AddressID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(addConsultationObj.apply _)

  val addConsultationObjWrites = (
    (__ \ "consultationRecord").write[Array[addConsultationRecord]] and
    (__ \ "AddressID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(addConsultationObj.unapply))
  implicit val addConsultationObjFormat: Format[addConsultationObj] = Format(addConsultationObjReads, addConsultationObjWrites)

}

case class validateConsultationRecord(DayCD: String, StartTime: Int, EndTime: Int,
                                 MinPerCase: Option[Int], Fees: Option[Int], Active: Option[String])

object validateConsultationRecord {
  val validateConsultationRecordReads = (
    (__ \ "DayCD").read[String] and
    (__ \ "StartTime").read[Int] and
    (__ \ "EndTime").read[Int] and
    (__ \ "MinPerCase").readNullable[Int] and
    (__ \ "Fees").readNullable[Int] and
    (__ \ "Active").readNullable[String])(validateConsultationRecord.apply _)

  val validateConsultationRecordWrites = (
    (__ \ "DayCD").write[String] and
    (__ \ "StartTime").write[Int] and
    (__ \ "EndTime").write[Int] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "Fees").writeNullable[Int] and
    (__ \ "Active").writeNullable[String])(unlift(validateConsultationRecord.unapply))
  implicit val validateConsultationFormat: Format[validateConsultationRecord] = Format(validateConsultationRecordReads, validateConsultationRecordWrites)

}


case class addConsultationRecord(AddressConsultID: Option[Long],DayCD: String, StartTime: String, EndTime: String,
                                 MinPerCase: Option[Int], Fees: Option[Int], Active: Option[String])

object addConsultationRecord {
  val consultationRecordReads = (
    (__ \ "AddressConsultID").readNullable[Long] and  
    (__ \ "DayCD").read[String] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "MinPerCase").readNullable(Reads.min[Int](5)) and
    (__ \ "Fees").readNullable[Int] and
    (__ \ "Active").readNullable[String])(addConsultationRecord.apply _)

  val consultationRecordWrites = (
    (__ \ "AddressConsultID").writeNullable[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "Fees").writeNullable[Int] and
    (__ \ "Active").writeNullable[String])(unlift(addConsultationRecord.unapply))
  implicit val consultationRecordFormat: Format[addConsultationRecord] = Format(consultationRecordReads, consultationRecordWrites)

}

case class updateConsultationObj(consultationRecord: Array[updateConsultationRecord], AddressID: Long, DoctorID: Long, USRId: Int, USRType: String)

object updateConsultationObj {
  val addConsultationObjReads = (
    (__ \ "consultationRecord").read[Array[updateConsultationRecord]] and
    (__ \ "AddressID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updateConsultationObj.apply _)

  val addConsultationObjWrites = (
    (__ \ "consultationRecord").write[Array[updateConsultationRecord]] and
    (__ \ "AddressID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(updateConsultationObj.unapply))
  implicit val addConsultationObjFormat: Format[updateConsultationObj] = Format(addConsultationObjReads, addConsultationObjWrites)

}

case class getConsultationObj(AddressID: Long, DoctorID: Long, USRId: Long, USRType: String)

object getConsultationObj {
  val getConsultationObjReads = (
    (__ \ "AddressID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(getConsultationObj.apply _)

  val getConsultationObjWrites = (
    (__ \ "AddressID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(getConsultationObj.unapply))
  implicit val getConsultationObjFormat: Format[getConsultationObj] = Format(getConsultationObjReads, getConsultationObjWrites)

}

case class updateConsultationRecord(AddressConsultID: Long, DayCD: String, StartTime: String, EndTime: String,
                                    MinPerCase: Option[Int], Fees: Option[Int], Active: Option[String])

object updateConsultationRecord {
  val updateConsultationObjReads = (
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "MinPerCase").readNullable(Reads.min[Int](5)) and 
    (__ \ "Fees").readNullable[Int] and
    (__ \ "Active").readNullable[String])(updateConsultationRecord.apply _)

  val updateConsultationObjWrites = (
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "Fees").writeNullable[Int] and
    (__ \ "Active").writeNullable[String])(unlift(updateConsultationRecord.unapply))
  implicit val updateConsultationObjFormat: Format[updateConsultationRecord] = Format(updateConsultationObjReads, updateConsultationObjWrites)

}

