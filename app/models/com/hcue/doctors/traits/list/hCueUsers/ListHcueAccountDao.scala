package models.com.hcue.doctors.traits.list.hCueUsers

import scala.slick.driver.PostgresDriver.simple._

import play.Logger

import models.com.hcue.doctors.traits.dbProperties.db
import play.api.db.slick.DB

import models.com.hcue.doctors.slick.transactTable.hCueUsers.THcueAccountLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccountLkup
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccount.listHcueAccountObj

object ListHcueAccountDao  {

  val log = Logger.of("application")
  val hcueAccountLkup = HcueAccountLkup.HcueAccountLkup

  def ListHcueAccount(): Array[listHcueAccountObj] = db withSession { implicit session =>

    val storedRecord = hcueAccountLkup.list

    val resultSet: Array[listHcueAccountObj] = new Array[listHcueAccountObj](storedRecord.length)

    for (i <- 0 until storedRecord.length) {

      resultSet(i) = new listHcueAccountObj(storedRecord(i).AccountID, storedRecord(i).AccountDesc, storedRecord(i).ActiveIND)

    }

    return resultSet
  }

}