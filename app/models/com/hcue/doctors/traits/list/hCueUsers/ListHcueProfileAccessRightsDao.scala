package models.com.hcue.doctors.traits.list.hCueUsers

import scala.slick.driver.PostgresDriver.simple._

import play.Logger

import models.com.hcue.doctors.traits.dbProperties.db
import play.api.db.slick.DB

import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateProfileAccessRights.listHospitalProfileAccessRightsObj
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateProfileAccessRights.listProfileAccessRightsObj
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalGrpSetting
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueRoleLkup

object ListHcueProfileAccessRightsDao {

  val log = Logger.of("application")
  val hospitalGrpSetting = HospitalGrpSetting.HospitalGrpSetting
  val hcueRoleLkup = HcueRoleLkup.HcueRoleLkup

  def ListHcueAccessRights(HospitalID: Long): listHospitalProfileAccessRightsObj = db withSession { implicit session =>

    val storedRecord = hospitalGrpSetting.filter { x => x.HospitalID === HospitalID && x.Active === "Y" }.sortBy { x => x.GroupCode }.list

    val resultSet: Array[listProfileAccessRightsObj] = new Array[listProfileAccessRightsObj](storedRecord.length)

    val RoledescList = hcueRoleLkup.filter { x => x.HospitalID === HospitalID }.list

    for (i <- 0 until storedRecord.length) {
      var Roledesc = "";
      if(storedRecord(i).RoleID != None){
         val roledesclist = RoledescList.filter { x => x.RoleID == storedRecord(i).RoleID.get }.map { x => x.RoleDesc }      
         if(roledesclist.length !=0){ Roledesc = roledesclist(0) }       
       } 
       //val Roledesc=hcueRoleLkup.filter { x => x.HospitalID === HospitalID && x.RoleID === storedRecord(i).RoleID }.map{x=>x.RoleDesc}.firstOption
      resultSet(i) = new listProfileAccessRightsObj(storedRecord(i).GroupCode, storedRecord(i).GroupName,
        storedRecord(i).AccountAccessID, storedRecord(i).RoleID, Some(Roledesc), storedRecord(i).Active,storedRecord(i).ReadOnly)
    }

    return new listHospitalProfileAccessRightsObj(HospitalID, resultSet)
  }
 
}