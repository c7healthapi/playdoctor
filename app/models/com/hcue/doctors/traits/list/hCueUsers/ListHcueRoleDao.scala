package models.com.hcue.doctors.traits.list.hCueUsers

import scala.slick.driver.PostgresDriver.simple._

import play.Logger

import models.com.hcue.doctors.traits.dbProperties.db
import play.api.db.slick.DB

import models.com.hcue.doctors.slick.transactTable.hCueUsers.THcueRoleLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueRoleLkup
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueRole.listRoleObj
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueRole.listHospitalRoleObj

object ListHcueRoleDao {

  val log = Logger.of("application")
  val hcueRoleLkup = HcueRoleLkup.HcueRoleLkup

  def ListHcueRole(): Array[listRoleObj] = db withSession { implicit session =>

    val storedRecord = hcueRoleLkup.list

    val resultSet: Array[listRoleObj] = new Array[listRoleObj](storedRecord.length)

    for (i <- 0 until storedRecord.length) {

      resultSet(i) = new listRoleObj(storedRecord(i).RoleID, storedRecord(i).RoleDesc, storedRecord(i).ActiveIND, storedRecord(i).IsPrivate, storedRecord(i).HospitalID)

    }

    return resultSet
  }
  
   def listHospitalRoles(a: listHospitalRoleObj): Array[listRoleObj] = db withSession { implicit session =>

   val storedRecord = if(a.ActiveIND != None) {
      hcueRoleLkup.filter { x => (x.HospitalID === a.ParentHospitalID) && x.ActiveIND === a.ActiveIND.get }.sortBy(_.RoleDesc.asc).list
    } else {
      hcueRoleLkup.filter { x => (x.HospitalID === a.ParentHospitalID) }.sortBy(_.RoleDesc.asc).list
    }

    val resultSet: Array[listRoleObj] = new Array[listRoleObj](storedRecord.length)

    for (i <- 0 until storedRecord.length) {

      resultSet(i) = new listRoleObj(storedRecord(i).RoleID, storedRecord(i).RoleDesc, storedRecord(i).ActiveIND, storedRecord(i).IsPrivate, storedRecord(i).HospitalID)

    }

    return resultSet
  }

}