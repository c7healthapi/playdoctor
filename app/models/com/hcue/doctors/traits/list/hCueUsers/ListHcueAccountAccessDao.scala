package models.com.hcue.doctors.traits.list.hCueUsers

import scala.slick.driver.PostgresDriver.simple._

import play.Logger

import models.com.hcue.doctors.traits.dbProperties.db
import play.api.db.slick.DB

import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccountAccessLkup
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccountAccess.listAccountAccessObj

object ListHcueAccountAccessDao {

  val log = Logger.of("application")
  val hcueAccountAccessLkup = HcueAccountAccessLkup.HcueAccountAccessLkup

  def ListHcueAccountAccess(): Array[listAccountAccessObj] = db withSession { implicit session =>

    val storedRecord = hcueAccountAccessLkup.list

    val resultSet: Array[listAccountAccessObj] = new Array[listAccountAccessObj](storedRecord.length)

    for (i <- 0 until storedRecord.length) {

      resultSet(i) = new listAccountAccessObj(storedRecord(i).AcntAccessID, storedRecord(i).AccessID, storedRecord(i).ActiveIND, storedRecord(i).AccountID)

    }

    return resultSet
  }

}