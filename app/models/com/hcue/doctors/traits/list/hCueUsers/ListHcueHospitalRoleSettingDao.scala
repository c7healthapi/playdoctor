package models.com.hcue.doctors.traits.list.hCueUsers

import scala.slick.driver.PostgresDriver.simple._

import models.com.hcue.doctors.traits.dbProperties.db
import play.api.db.slick.DB

import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalRoleSetting
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalGrpSetting
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHospitalRole.listRoleArrayObj
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHospitalRole.listHospitalRoleSettingObj
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateProfileAccessRights.listProfileAccessRightsObj

object ListHcueHospitalRoleSettingDao {

  val hospitalRoleSetting = HospitalRoleSetting.HospitalRoleSetting
 
  def ListHospitalRoles(HospitalID: Long): listHospitalRoleSettingObj = db withSession { implicit session =>

    val storedRecord = hospitalRoleSetting.filter { x => x.HospitalID === HospitalID}.list
    val resultSet: Array[listRoleArrayObj] = new Array[listRoleArrayObj](storedRecord.length)

    for (i <- 0 until storedRecord.length) {
            resultSet(i) = new listRoleArrayObj(storedRecord(i).RoleID,storedRecord(i).ActiveIND)
    }

    return new listHospitalRoleSettingObj(HospitalID,resultSet)
  }

}