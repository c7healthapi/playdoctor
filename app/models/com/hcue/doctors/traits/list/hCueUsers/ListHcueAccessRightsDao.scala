package models.com.hcue.doctors.traits.list.hCueUsers

import scala.slick.driver.PostgresDriver.simple._

import play.Logger

import models.com.hcue.doctors.traits.dbProperties.db
import play.api.db.slick.DB

import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccessLkup
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccessRights.listHcueAccessRightsObj
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccessRights.listHcueAccessAdminRightsObj
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccessCategory
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccessRights.HcueAccessAdminRigthsObj
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital

object ListHcueAccessRightsDao  {

  val log = Logger.of("application")
  val hcueAccessLkup = HcueAccessLkup.HcueAccessLkup
  val hcueAccessCategory = HcueAccessCategory.HcueAccessCategory
  val hospital = Hospital.Hospital
  def ListHcueAccessRights(): Array[listHcueAccessRightsObj] = db withSession { implicit session =>

    val storedRecord = hcueAccessLkup.list

    val resultSet: Array[listHcueAccessRightsObj] = new Array[listHcueAccessRightsObj](storedRecord.length)

    for (i <- 0 until storedRecord.length) {

      resultSet(i) = new listHcueAccessRightsObj(storedRecord(i).AccessID, storedRecord(i).AccessDesc, storedRecord(i).ActiveIND,storedRecord(i).ParentAccessID)

    }

    return resultSet
  }

  def ListHcueAdminAccessRights(hospitalId: Option[Long], usrType: String): Array[HcueAccessAdminRigthsObj] = db withSession { implicit session =>

    var dentalAccessFlag = false
    
    if(hospitalId != None && hospitalId.get != 0) {
       val hospitalSetting = hospital.filter { x => x.HospitalID === hospitalId }.first
       if(hospitalSetting.Preference != None && hospitalSetting.Preference.get.contains("showOnlyDentalAccess")) {
        dentalAccessFlag = true
       }
    }
    
    val storedRecord = if(dentalAccessFlag) {
      if(usrType == "ADMIN") {
         hcueAccessLkup.filter { x => x.ActiveIND === "Y" && x.AccessType =!= "OTHERSPECIALITY" }.list.sortBy { x => (x.AccessCategoryID,x.AccessDesc,x.Order) }
      } else {
         hcueAccessLkup.filter { x => x.ActiveIND === "Y" && x.AccessType =!= "OTHERSPECIALITY" && x.GrpType =!= "ADMINISTRATOR" }.list.sortBy { x => (x.AccessCategoryID,x.AccessDesc,x.Order) }
      }
    } else {
      if(usrType == "ADMIN") {
         hcueAccessLkup.filter { x => x.ActiveIND === "Y" }.list.sortBy { x => (x.AccessCategoryID,x.AccessDesc,x.Order) }
      } else {
         hcueAccessLkup.filter { x => x.ActiveIND === "Y"  && x.GrpType =!= "ADMINISTRATOR" }.list.sortBy { x => (x.AccessCategoryID,x.AccessDesc,x.Order) }
      }
    }

    val CategoryRecords = hcueAccessCategory.list

    val records = storedRecord.groupBy { x => x.AccessCategoryID }
    val recordKeys = records.keys.toList.sortBy { x => x }

    val resultSet: Array[HcueAccessAdminRigthsObj] = new Array[HcueAccessAdminRigthsObj](recordKeys.length)
    for (i <- 0 until recordKeys.size) {
      
      val accessCategoryDesc = CategoryRecords.filter { x => x.AccessCategoryID == recordKeys(i).getOrElse(0L) }.map { x => x.AccessCategoryDesc.getOrElse("") }.headOption
      val listRecords = records.get(recordKeys(i)).getOrElse(Nil)
      val accessIDObj: Array[listHcueAccessAdminRightsObj] = new Array[listHcueAccessAdminRightsObj](listRecords.length)
      
      for (j <- 0 until listRecords.length) {
        accessIDObj(j) = new listHcueAccessAdminRightsObj(listRecords(j).AccessID, listRecords(j).AccessDesc, listRecords(j).ActiveIND,
            listRecords(j).ParentAccessID, listRecords(j).AccessCategoryID, listRecords(j).Order)
      }
      resultSet(i) = new HcueAccessAdminRigthsObj(recordKeys(i), accessCategoryDesc, accessIDObj)
    }

    return resultSet
  }

}