package models.com.hcue.doctors.traits.list.availableAppointment

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import scala.slick.driver.PostgresDriver.simple._
import org.postgresql.util.PSQLException
import play.Logger
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.addQuickAppointmentReq
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getDoctorConsultationResList
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getAddressConsultInfo
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getConsultDtLst
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getAddressConsultIDLst
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.helper.hcueHelperClass
import models.com.hcue.doctors.helper.RequestResponseHelper
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointments
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.slick.transactTable.patient.PatientPhones
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn

import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import models.com.hcue.doctors.slick.transactTable.patient.TPatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.TPatientAddress
import models.com.hcue.doctors.slick.transactTable.patient.TPatientEmail
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getPatientConsultationInfo
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getDoctorConsultationInfoList
import models.com.hcue.doctors.Json.Request.patientAddressDetailsObj
import models.com.hcue.doctors.Json.Request.patientEmailDetailsObj
import models.com.hcue.doctors.slick.transactTable.patient.TPatientPhones
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.buildDoctorAppointmentList
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.buildAppointmentObj
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getHospitalInfo
import scala.collection.mutable.ListBuffer

import models.com.hcue.doctors.Json.Response.Doctor.Consultation.doctorInfo
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getCalenderViewReqList
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.profileInfodetobj
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.ConsultationInfo
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.clinicInfodetobj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.patient.PatientAddress
import models.com.hcue.doctors.slick.transactTable.patient.PatientEmail
import models.com.hcue.doctors.helper.HCueUtils
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital

object ListAvailableDoctorAppointmentDao {

  val log = Logger.of("application")

  val doctorAppointments = DoctorAppointments.DoctorAppointments

  val patients = Patients.patients
  
  val patientPhones = PatientPhones.patientsPhones

  val patientAddress = PatientAddress.patientsAddress
  
  val patientEmail = PatientEmail.patientsEmail

  val doctorAddress = DoctorAddress.DoctorsAddress

  val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

  val hostpital = Hospital.Hospital


  def insertRecord(arrayList: getDoctorConsultationResList, obj: addQuickAppointmentReq, dayCode: String): Option[buildAppointmentObj] = db withSession { implicit session =>


    val autoGenDoctorID = (doctorAppointments.map { x => (x.AppointmentID, x.AddressConsultID, x.DayCD, x.ConsultationDt, 
        x.StartTime, x.EndTime, x.PatientID, x.VisitUserTypeID, x.DoctorID, x.FirstTimeVisit,
      x.DoctorVisitRsnID, x.AppointmentStatus, x.TokenNumber,x.ParentAppointmentID,x.HospitalID,
      x.HospitalCode,x.BranchCode,x.CrtUSR, x.CrtUSRType) }  returning doctorAppointments.map(_.AppointmentID)) += 
         (0, obj.AddressConsultID, dayCode,
      obj.FilterByDate, arrayList.StartTime, arrayList.EndTime, Some(obj.PatientID), "OPATIENT",
      Some(obj.DoctorID), "N", "ALLMRDRR", "B", arrayList.TokenNumber.toString,0, None, None,None,obj.USRId, obj.USRType)

    val insertedRecord = doctorAppointments.filter(c => c.AppointmentID === autoGenDoctorID).firstOption

    if(insertedRecord == None) 
       return None
       
       val data = new buildAppointmentObj(insertedRecord.get.AppointmentID,insertedRecord.get.AddressConsultID,insertedRecord.get.DayCD,insertedRecord.get.ConsultationDt,
           insertedRecord.get.StartTime,insertedRecord.get.EndTime,insertedRecord.get.PatientID,insertedRecord.get.VisitUserTypeID, insertedRecord.get.FirstTimeVisit,insertedRecord.get.DoctorVisitRsnID,
    insertedRecord.get.AppointmentStatus,insertedRecord.get.TokenNumber,None)
      
      return Some(data)

  }

  def buildPatientList(appointmentLst: List[buildDoctorAppointmentList], patInfo: List[TPatient], patPhoneInfo: List[TPatientPhones], patAddressInfo: List[TPatientAddress], patEmailInfo: List[TPatientEmail], Available: String, lst: ListBuffer[getDoctorConsultationResList]): ListBuffer[getDoctorConsultationResList] = {

    var list:ListBuffer[getDoctorConsultationResList] = lst
    if (Available == "A" || Available == "N") { //change the flag to status

      if (appointmentLst.length > 0) {

        for (j <- 0 until appointmentLst.length) {

          val patientInfo: List[TPatient] = patInfo.filter { x => x.PatientID == appointmentLst(j).PatientID.getOrElse(0.toLong) }.toList
          val patientPhoneInfo: List[TPatientPhones] = patPhoneInfo.filter { x => x.PatientID == appointmentLst(j).PatientID.getOrElse(0.toLong) && x.PhType == "M" }.toList
          val patientAddressInfo: List[TPatientAddress] = patAddressInfo.filter { x => x.PatientID == appointmentLst(j).PatientID.getOrElse(0.toLong) }.toList
          val patientEmailInfo: List[TPatientEmail] = patEmailInfo.filter { x => x.PatientID == appointmentLst(j).PatientID.getOrElse(0.toLong) }.toList
          
          var phNumber = if (patientPhoneInfo.length > 0) { Some(patientPhoneInfo(0).PhNumber) } else { None }
         
          val patientsAddressInfoList: Array[patientAddressDetailsObj] = new Array[patientAddressDetailsObj](patientAddressInfo.length)
          
            for(i <- 0 until patientAddressInfo.length){
             patientsAddressInfoList(i) = new patientAddressDetailsObj(patientAddressInfo(i).Address1.getOrElse(""), patientAddressInfo(i).Address2.getOrElse(""), patientAddressInfo(i).Street.getOrElse(""), patientAddressInfo(i).Location, patientAddressInfo(i).CityTown.getOrElse(""), patientAddressInfo(i).DistrictRegion.getOrElse(""),
                  patientAddressInfo(i).State.getOrElse(""), patientAddressInfo(i).PinCode, patientAddressInfo(i).Country, patientAddressInfo(i).AddressType, patientAddressInfo(i).PrimaryIND, patientAddressInfo(i).LandMark.getOrElse(""), patientAddressInfo(i).Latitude.getOrElse(""), patientAddressInfo(i).Longitude.getOrElse(""))
              } 

          val patientsEmailInfoList: Array[patientEmailDetailsObj] = new Array[patientEmailDetailsObj](patientEmailInfo.length)
           
            for(i <- 0 until patientEmailInfo.length){
           patientsEmailInfoList(i) = new patientEmailDetailsObj(patientEmailInfo(i).EmailID, patientEmailInfo(i).EmailIDType, patientEmailInfo(i).PrimaryIND)
              } 

          val age = RequestResponseHelper.setAgeFormat(patientInfo(0).Age)

          val getPatientInfo = Some(new getPatientConsultationInfo(patientInfo(0).PatientID, patientInfo(0).Title, patientInfo(0).FullName, appointmentLst(j).AppointmentID, appointmentLst(j).AppointmentStatus,
            phNumber,patientsAddressInfoList,patientsEmailInfoList, age, Some(patientInfo(0).Gender), RequestResponseHelper.ageCalculation(patientInfo(0).DOB)))

          val appointmentTmpTime = (appointmentLst(j).StartTime.replaceAll(":", "")).toInt 
          
          list  +=  new getDoctorConsultationResList(appointmentTmpTime, appointmentLst(j).StartTime, appointmentLst(j).EndTime, "N", appointmentLst(j).TokenNumber.toInt, getPatientInfo)

        }
      }
    }

    list
  }
  
}