package models.com.hcue.doctors.traits.list.lab

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import play.Logger

import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.slick.transactTable.doctor.lab.TDoctorLab
import models.com.hcue.doctors.slick.transactTable.doctor.lab.DoctorLab

import models.com.hcue.doctors.dataCasting.DataCasting

object ListDoctorLabDao extends ListDoctorLab {

  val log = Logger.of("application")

  val doctorLab = DoctorLab.DoctorLab

  def getDoctorLab(DoctorID: Long): List[TDoctorLab] = db withSession { implicit session =>

    doctorLab.filter(c => c.DoctorID === DoctorID).list
  }

  def getDoctorLabByID(DoctorLabID: Long): Option[TDoctorLab] = db withSession { implicit session =>

    doctorLab.filter(c => c.DoctorLabID === DoctorLabID).firstOption
  }

}