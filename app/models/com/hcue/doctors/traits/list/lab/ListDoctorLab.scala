package models.com.hcue.doctors.traits.list.lab

import org.postgresql.ds.PGSimpleDataSource
import play.api.db.DB
import play.api.db.slick.DB

import play.Logger

import models.com.hcue.doctors.slick.transactTable.doctor.lab.TDoctorLab

trait ListDoctorLab {

  def getDoctorLab(DoctorID: Long): List[TDoctorLab]
  
  def getDoctorLabByID(DoctorlabID: Long): Option[TDoctorLab]

}