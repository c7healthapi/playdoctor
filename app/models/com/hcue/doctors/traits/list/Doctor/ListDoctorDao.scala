package models.com.hcue.doctors.traits.list.Doctor

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB
import play.Logger
import models.com.hcue.doctors.dataCasting.DataCasting
import models.com.hcue.doctors.Json.Response.DoctorAddressDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorEmailDetailsObj
import models.com.hcue.doctors.Json.Response.AddressImagesObj
import models.com.hcue.doctors.Json.Response.DoctorAddressExtnObj
import models.com.hcue.doctors.Json.Response.DoctorCommunicationObj
import models.com.hcue.doctors.Json.Response.AddressImagesObj
import models.com.hcue.doctors.Json.Request.AddUpdate.hospitalInfoObj
import models.com.hcue.doctors.Json.Request.doctorSearchDetailsObj
import models.com.hcue.doctors.Json.Request.getDtAppointmentDetailsObj
import models.com.hcue.doctors.Json.Response.DoctorSpecialityDetails
import models.com.hcue.doctors.traits.Amazon.S3.Image.DoctorS3ImageDao
import models.com.hcue.doctors.helper.CalenderDateConvertor
import play.api.libs.json.Json
import play.api.libs.json.JsValue
import models.com.hcue.doctors.helper.RequestResponseHelper
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.slick.transactTable.patient.TPatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.PatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.TPatientEmail
import models.com.hcue.doctors.slick.transactTable.patient.PatientEmail
import models.com.hcue.doctors.slick.transactTable.patient.TPatientAddress
import models.com.hcue.doctors.slick.transactTable.patient.PatientAddress
import models.com.hcue.doctors.slick.transactTable.patient.TPatientOtherID
import models.com.hcue.doctors.slick.transactTable.patient.PatientOtherID
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressPhone
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddressPhone
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.login.TDoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.login.DoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointments
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorSpecialityCD
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorSpecialityCD
import models.com.hcue.doctors.slick.lookupTable.TLocationLkup
import models.com.hcue.doctors.slick.lookupTable.LocationLkup
import models.com.hcue.doctors.Json.Response.DoctorAppointmentResponseObj
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.buildDoctorAppointmentList
import models.com.hcue.doctors.Json.Response.PatientDetailsObj
import models.com.hcue.doctors.Json.Request.patientPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.patientAddressDetailsObj
import models.com.hcue.doctors.Json.Request.patientEmailDetailsObj
import models.com.hcue.doctors.Json.Request.patientOtherIdDetailsObj
import models.com.hcue.doctors.Json.Request.patientCaseBillingAmtInfo
import models.com.hcue.doctors.Json.Response.forgotPasswordObj
import models.com.hcue.doctors.Json.Response.buildResponseObj
import models.com.hcue.doctors.Json.Response.getResponseObj
import models.com.hcue.doctors.Json.Response.DoctorConsultationDetailsObj
import models.com.hcue.doctors.Json.Response.DoctorDetailsObj
import models.com.hcue.doctors.Json.Response.SearchOutPutObj
import models.com.hcue.doctors.Json.Response.DoctorEducationObj
import models.com.hcue.doctors.Json.Response.DoctorPublicationAchievementObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctors.traits.delete.apppointment.DeleteDoctorAppointmentDao
import models.com.hcue.doctors.traits.mobileSMS.Appointment.AppointmentStatusDao
import models.com.hcue.doctors.traits.lookup.LookupDAO
import scala.collection.generic.Sorted
import play.api.libs.Crypto
import scala.collection.mutable.ListBuffer
import models.com.hcue.doctors.slick.lookupTable.DoctorSpecialityLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalGrpSetting
import models.com.hcue.doctors.Json.Response.getMinimalResponseObj
import models.com.hcue.doctors.Json.Response.MinimalDoctorAddressDetailsObj
//import models.com.hcue.doctors.traits.hcueProfileImgCloudFront
//import models.com.hcue.doctors.traits.hcueCookieHost
import models.com.hcue.doctors.slick.transactTable.patient.PatientOtherDetails
import models.com.hcue.doctors.slick.transactTable.patient.TPatientOtherDetails
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.clinicPreferenceObj
import models.com.hcue.doctors.slick.lookupTable.StateLkup
import models.com.hcue.doctors.slick.lookupTable.Country
import models.com.hcue.doctors.slick.lookupTable.CityLkup
import models.com.hcue.doctors.slick.lookupTable.TStateLkup
import models.com.hcue.doctors.slick.lookupTable.TCityLkup
import models.com.hcue.doctors.slick.lookupTable.TCountryLkup
import models.com.hcue.doctors.Json.Response.statusResponseObj
import org.joda.time.DateTime
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorDepartmentsObj
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointmentExtn
import models.com.hcue.doctors.Json.Request.sourceDetailsObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointmentExtn
import models.com.hcue.doctors.helper.HCueUtils
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalAddress
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalExtn
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.hospitals.THospital
//import controllers.doctors.security.authentication.JWTAuthentication
import models.com.hcue.doctors.Json.Response.DoctorOtherDetailsObj
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker
import java.sql.Date
import models.com.hcue.doctors.Json.Response.SpecialityDetailsObj
import models.com.hcue.doctors.Json.Response.OtherDetailsObj
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.getDoctorsLstObj
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorConsultations
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorConsultation
import controllers.doctors.security.authentication.JWTAuthentication
import models.com.hcue.doctors.Json.Response.PractiesDetails
import models.com.hcue.doctor.slick.transactTable.Careteam.DoctorPracties
import models.com.hcue.doctors.Json.Response.CareerImagesObj
import models.com.hcue.doctors.slick.transactTable.doctor.login.AccessToken
import models.com.hcue.doctors.slick.transactTable.doctor.login.TAccessToken

object ListDoctorDao extends ListDoctor {

  val log = Logger.of("application")

  log.info("Created c3p0 connection pool")

  //By default the Result will be in Sort By Patient ID  
  val doctors = Doctors.Doctors
  val doctorsExtn = DoctorsExtn.DoctorsExtn
  val doctorLogin = DoctorLogin.DoctorLogin

  val doctorPhones = DoctorPhones.DoctorsPhones

  val doctorEmails = DoctorEmail.DoctorEmails

  val patientOtherDetails = PatientOtherDetails.PatientOtherDetail

  val doctorAddress = DoctorAddress.DoctorsAddress

  val doctorAddressPhone = DoctorAddressPhone.DoctorAddressPhone

  val doctorAppointments = DoctorAppointments.DoctorAppointments.sortBy(_.StartTime.asc.nullsFirst)

  val doctorAppointmentExtn = DoctorAppointmentExtn.DoctorAppointmentExtn

  val doctorsAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

  val patients = Patients.patients

  val patientPhones = PatientPhones.patientsPhones

  val patientEmails = PatientEmail.patientsEmail

  val patientAddress = PatientAddress.patientsAddress

  val patientOtherID = PatientOtherID.patientsOtherID

  val doctorSpecialityType = DoctorSpecialityLkup.DoctorSpecialityLkup

  val doctorSpecialityCD = DoctorSpecialityCD.DoctorSpecialityCD

  val hospitalGrpSetting = HospitalGrpSetting.HospitalGrpSetting

  val hospital = Hospital.Hospital

  val hospitalExtn = HospitalExtn.HospitalExtn

  val hospitalAddress = HospitalAddress.HospitalAddress

  val stateLkup = StateLkup.StateLkup

  val locationLkup = LocationLkup.LocationLkup

  val countryLkup = Country.Country

  val cityLkup = CityLkup.CityLkup

  val doctorConsultation = DoctorConsultations.DoctorConsultations

  val doctorpractiesdetails = DoctorPracties.DoctorPracties

  val accessToken = AccessToken.AccessToken

  def getDoctor(id: Long): Option[TDoctor] = db withSession { implicit session =>
    doctors.filter(c => c.DoctorID === id).firstOption
  }

  def getDoctorExt(id: Long): Option[TDoctorExtn] = db withSession { implicit session =>
    doctorsExtn.filter(c => c.DoctorID === id).firstOption
  }

  def completeResponse(id: Long, profileImage: Boolean, AddressId: Option[List[Long]], encryptPassword: Boolean): getResponseObj = db withSession { implicit session =>

    //Get Doctor Details
    val doctorList: List[TDoctor] = doctors.filter(c => c.DoctorID === id).list
    val doctorDetailsList: Array[DoctorDetailsObj] = new Array[DoctorDetailsObj](doctorList.length)
    val doctorLoginPwd = Crypto.decryptAES(doctorLogin.filter { x => x.DoctorID === id }.map { x => x.DoctorPassword }.firstOption.getOrElse(""))
    val doctorExtnList: Option[TDoctorExtn] = doctorsExtn.filter(c => c.DoctorID === id).firstOption
    //log.info("DoctorList " + doctorList)
    var careerImages: Option[Map[String, String]] = None
    var doctorSetting: Option[Map[String, String]] = None
    var profileImageUrl: Option[String] = None
    val searchable: Option[String] = if (doctorList.length > 0) { doctorList(0).Searchable } else { None }
    //val  identiy = if(doctorList.length >0){ JWTAuthentication.getToken(doctorList(0).FirstName,doctorList(0).DoctorLoginID.getOrElse("")) }else {""}
    val docOtherDetaislObj: Option[DoctorOtherDetailsObj] = doctorList.length match {
      case x if x > 0 => Some(new DoctorOtherDetailsObj(doctorList(0).FirstName, doctorList(0).LastName, doctorList(0).faxno,
        doctorList(0).gpcode, doctorList(0).typeval, doctorList(0).notes))
      case _ => None
    }
      
    var faxno: Option[String] = None
    var gpcode: Option[String] = None
    var typevalue: Option[String] = None
    var notes: Option[String] = None
    var title: Option[String] = None
    var RegistrationNo: Option[String] = None
    val identiy = if (doctorList.length > 0) {
      JWTAuthentication.getToken(doctorList(0).FirstName, doctorList(0).DoctorLoginID.getOrElse(""))

    } else {
      ""
    }
    if (doctorList.length > 0) {
      val tokenJWT = JWTAuthentication.getToken(doctorList(0).FirstName, doctorList(0).DoctorLoginID.getOrElse(""))

      println("********* 3 ********")

      var currentTime = new DateTime

      val lstlength = accessToken.filter { x => x.UserID === id }.list.length
      if (lstlength == 0) {
        val aceessTokenObj: TAccessToken = new TAccessToken(tokenJWT, currentTime.plusMinutes(20).toString(), Some(id), id, "USER", Some(0L), Some("USER"))

        accessToken += aceessTokenObj

      }
    }

    for (i <- 0 until doctorList.length) {

      faxno = doctorList(i).faxno

      gpcode = doctorList(i).gpcode

      typevalue = doctorList(i).typeval

      notes = doctorList(i).notes

      title = doctorList(i).Title

      RegistrationNo = doctorList(i).RegistrationNo

      if (doctorExtnList != None) {

        var image: Option[String] = doctorExtnList.get.ProfilImage

        /* if (profileImage) {
          image = DoctorS3ImageDao.downloadHelperService(doctorList(i).DoctorID, doctorList(i).DoctorID + "/profileImage.txt")
        }*/

        doctorSetting = doctorExtnList.get.DoctorSettings

        careerImages = doctorExtnList.get.CareerImages
        doctorDetailsList(i) = new DoctorDetailsObj(doctorList(i).FirstName, doctorList(i).LastName, doctorList(i).FullName, doctorList(i).DoctorID, doctorList(i).Gender, doctorList(i).DOB,
          doctorList(i).SpecialityCD, doctorExtnList.get.MemberID, doctorExtnList.get.Qualification, doctorList(i).Exp,
          doctorList(i).DoctorLoginID, if (encryptPassword == true) { Some(doctorLoginPwd) } else { None }, doctorExtnList.get.About, None, doctorExtnList.get.Awards,
          doctorExtnList.get.Prospect, doctorExtnList.get.ReferredBy, Some(doctorList(i).TermsAccepted), image, title, RegistrationNo)

      }
    }

    for (i <- 0 until doctorList.length) {

      if (doctorExtnList != None) {
        profileImageUrl = doctorExtnList.get.ProfilImage
        var image: Option[String] = doctorExtnList.get.ProfilImage

        if (profileImage && image != None) {
          image = DoctorS3ImageDao.downloadHelperService(doctorList(i).DoctorID, doctorList(i).DoctorID + "/profileImage.txt")
        }

        doctorSetting = doctorExtnList.get.DoctorSettings

        careerImages = doctorExtnList.get.CareerImages
        doctorDetailsList(i) = new DoctorDetailsObj(doctorList(i).FirstName, doctorList(i).LastName, doctorList(i).FullName, doctorList(i).DoctorID, doctorList(i).Gender, doctorList(i).DOB,
          doctorList(i).SpecialityCD, doctorExtnList.get.MemberID, doctorExtnList.get.Qualification, doctorList(i).Exp,
          doctorList(i).DoctorLoginID, if (encryptPassword == true) { Some(doctorLoginPwd) } else { None }, doctorExtnList.get.About, None, doctorExtnList.get.Awards,
          doctorExtnList.get.Prospect, doctorExtnList.get.ReferredBy, Some(doctorList(i).TermsAccepted), image, title, RegistrationNo)

      }
    }

    val doctorAddr = doctorAddress.filter(c => c.DoctorID === id && (c.AddressType === "B" || c.AddressType === "H"))

    //Get Avaiable Address
    var addressList: List[TDoctorAddress] = if (AddressId != None) {
      doctorAddr.filter { x => x.AddressID inSet AddressId.get.toList }.sortBy { x => x.AddressID.desc }.list
    } else {
      doctorAddr.sortBy { x => x.AddressID.desc }.list
    }

    val activelst = addressList.filter { x => x.Active == "Y" }.toList
    val inActivelst = addressList.filter { x => x.Active == "N" }.toList

    addressList = activelst ::: inActivelst

    val addids = addressList.map { x => x.AddressID }.toList
    val doctorAddressList: Array[DoctorAddressDetailsObj] = new Array[DoctorAddressDetailsObj](addressList.length)

    val doctorAddrExtn = doctorsAddressExtn.filter(c => c.AddressID inSet addids)

    val addressExtList: List[TDoctorAddressExtn] = if (AddressId != None) {
      doctorAddrExtn.filter { x => x.AddressID inSet AddressId.get.toList }.list
    } else {
      doctorAddrExtn.list
    }
    val addressPhone: List[TDoctorAddressPhone] = doctorAddressPhone.filter(c => c.DoctorID === id).list

    val addAddPhone = doctorAddressPhone.filter { x => x.DoctorID inSet addids }.list

    //log.info("addressList " + addressList)
    for (i <- 0 until addressList.length) {

      val addressExtObj = addressExtList.find(_x => _x.AddressID == addressList(i).AddressID)

      //log.info(" AddressExtList " +addressExtList)

      if (addressExtObj != None) {

        var addressImagesObj: Option[AddressImagesObj] = None //new AddressImagesObj(addressExtList.get.ClinicImages.get, A)

        //  log.info(" addressExtList.get.ClinicImages " +addressExtObj.get.ClinicImages)

        if (addressExtObj.get.ClinicImages != None && addressExtObj.get.ClinicImages.getOrElse(Map.empty[String, String]).size > 0) {

          addressImagesObj = Some(new AddressImagesObj(addressExtObj.get.ClinicImages.get, addressExtObj.get.ClinicDocuments))

        }

        log.info("Address ID" + addressExtObj.get.AddressID)

        val addressPhoneObj = addressPhone.filter(x => x.AddressID == addressExtObj.get.AddressID).toList

        log.info("addressPhoneObj.length" + addressPhoneObj.length)

        var addressCommunication: Array[DoctorCommunicationObj] = new Array[DoctorCommunicationObj](addressPhoneObj.length)
        for (i <- 0 until addressPhoneObj.length) {
          addressCommunication(i) = new DoctorCommunicationObj(addressPhoneObj(i).PhCntryCD, addressPhoneObj(i).PhStateCD,
            addressPhoneObj(i).PhAreaCD, addressPhoneObj(i).PhNumber, addressPhoneObj(i).PhType, addressPhoneObj(i).RowID, addressPhoneObj(i).PrimaryIND, addressPhoneObj(i).PublicDisplay)
        }

        var hospitalInfo: Option[hospitalInfoObj] = None
        var accessControlID: Option[Map[String, String]] = None
        if (addressExtObj != None && addressExtObj.get.HospitalID != None) {
          val hospitalDtls: Option[THospital] = hospital.filter(c => c.HospitalID === addressExtObj.get.HospitalID).firstOption
          val hospitalExtnDtl = hospitalExtn.filter(c => c.HospitalID === addressExtObj.get.HospitalID).firstOption
          if (hospitalDtls != None) {
            var countryCodes: Option[Map[String, String]] = None
            var hospitalDocuments: Option[JsValue] = None
            if (hospitalExtnDtl != None) {
              hospitalDocuments = hospitalExtnDtl.get.HospitalDocuments
            }

            hospitalInfo = Some(new hospitalInfoObj(hospitalDtls.get.HospitalID, hospitalDtls.get.ParentHospitalID, addressExtObj.get.HospitalCode, hospitalDtls.get.BranchCode, hospitalDtls.get.Preference, countryCodes, hospitalDocuments))

            if (addressExtObj.get.GroupID != None) {
              val profileSettingsInfo = hospitalGrpSetting.filter { x => x.HospitalID === hospitalDtls.get.ParentHospitalID && x.GroupName === addressExtObj.get.GroupID.get && x.Active === "Y" }.firstOption
              if (profileSettingsInfo != None) {
                accessControlID = Some(profileSettingsInfo.get.AccountAccessID)
              }
            }
          }
        }

        val doctorAddressExtList: DoctorAddressExtnObj = new DoctorAddressExtnObj(addressExtObj.get.HospitalID, addressExtObj.get.PrimaryIND,
          addressExtObj.get.Latitude, addressExtObj.get.Longitude, addressCommunication, addressImagesObj, hospitalInfo, addressExtObj.get.RoleID, addressExtObj.get.GroupID,
          addressExtObj.get.RatePerHour, addressExtObj.get.MinPerCase, accessControlID, addressExtObj.get.RateAboveTwenty, addressExtObj.get.RateBelowTwenty)

        doctorAddressList(i) = new DoctorAddressDetailsObj(Some(addressList(i).AddressID), addressList(i).ClinicName, addressList(i).ClinicSettings, addressList(i).Address1,
          addressList(i).Address2, addressList(i).Street, addressList(i).Location, addressList(i).CityTown,
          addressList(i).DistrictRegion, addressList(i).State, addressList(i).PostCode, addressList(i).Country, addressList(i).AddressType, addressList(i).LandMark, Some(addressList(i).Active), Some(doctorAddressExtList), None, addressList(i).Address4)

      } else {

        doctorAddressList(i) = new DoctorAddressDetailsObj(Some(addressList(i).AddressID), addressList(i).ClinicName, addressList(i).ClinicSettings, addressList(i).Address1,
          addressList(i).Address2, addressList(i).Street, addressList(i).Location, addressList(i).CityTown,
          addressList(i).DistrictRegion, addressList(i).State, addressList(i).PostCode, addressList(i).Country, addressList(i).AddressType,
          addressList(i).LandMark, Some(addressList(i).Active), None, None, addressList(i).Address4)

      }
    }

    log.info("------------------------------------------------")
    //Get Avaiable Phones
    val phoneList: List[TDoctorPhones] = doctorPhones.filter(c => c.DoctorID === id).list
    //log.info("---------phoneList------" + phoneList)
    val doctorPhoneList: Array[DoctorPhoneDetailsObj] = new Array[DoctorPhoneDetailsObj](phoneList.length)
    //log.info("phoneList " + phoneList)
    for (i <- 0 until phoneList.length) {

      doctorPhoneList(i) = new DoctorPhoneDetailsObj(None, None, None,
        phoneList(i).PhNumber, phoneList(i).PhType, phoneList(i).PrimaryIND, None)

    }
    //log.info("Phone List :" + phoneList)
    //Get Avaiable Phones
    val emailList: List[TDoctorEmail] = doctorEmails.filter(c => c.DoctorID === id).list
    val doctorEmailList: Array[DoctorEmailDetailsObj] = new Array[DoctorEmailDetailsObj](emailList.length)
    //log.info("emailList " + emailList)
    for (i <- 0 until emailList.length) {

      val crtUSRType: Option[String] = Some(emailList(i).CrtUSRType)
      val crtUSR: Option[Long] = Some(emailList(i).CrtUSR)

      doctorEmailList(i) = new DoctorEmailDetailsObj(emailList(i).EmailID, emailList(i).EmailIDType, emailList(i).PrimaryIND, crtUSR,
        crtUSRType, emailList(i).UpdtUSR, emailList(i).UpdtUSRType, None)

    }

    val queryResult = doctorSpecialityCD.filter { x => x.DoctorID === id }.list

    val result: Array[SpecialityDetailsObj] = new Array[SpecialityDetailsObj](queryResult.length)

    for (i <- 0 until queryResult.length) {

      result(i) = new SpecialityDetailsObj(queryResult(i).SpecialityCD, queryResult(i).CustomSpecialityDesc, queryResult(i).ActiveIND, queryResult(i).SubSpecialityID)

    }

    /* val consultationList: List[TDoctorConsultation] = if (AddressId != None) {
       doctorConsultation.filter(c => (c.AddressID inSet AddressId.get.toList) && c.Active =!= "H").sortBy{ x => x.StartTime.asc }.list
   } else {
       doctorConsultation.filter(c => c.DoctorID === id && c.Active =!= "H").sortBy{ x => x.StartTime.asc }.list
   }*/

    val doctorConsultationDetails: Array[DoctorConsultationDetailsObj] = new Array[DoctorConsultationDetailsObj](0)

    var practiesdetails: Array[PractiesDetails] = new Array[PractiesDetails](0)

    val doctorPractiesDtls = doctorpractiesdetails.filter { x => x.doctorid === id && x.active === true }.list

    if (doctorPractiesDtls.length > 0) {
      practiesdetails = new Array[PractiesDetails](doctorPractiesDtls.length)
      for (i <- 0 until doctorPractiesDtls.length) {
        practiesdetails(i) = new PractiesDetails(doctorPractiesDtls(i).practiceid, doctorPractiesDtls(i).practicename, doctorPractiesDtls(i).practicecode, doctorPractiesDtls(i).postcode, doctorPractiesDtls(i).ccgname, doctorPractiesDtls(i).telephone, doctorPractiesDtls(i).active.get)
      }
    }

    if (careerImages != None) {

      val CareerImagesObj = new CareerImagesObj(careerImages, doctorExtnList.get.CareerDocuments)
      return new getResponseObj(doctorDetailsList, if (doctorPhoneList.length == 0) { None } else { Some(doctorPhoneList) },
        doctorAddressList, if (doctorEmailList.length == 0) { None } else { Some(doctorEmailList) }, doctorConsultationDetails,
        doctorSetting,
        Some(CareerImagesObj), result, searchable, Some(identiy), docOtherDetaislObj, Some(practiesdetails))

    } else {

      return new getResponseObj(doctorDetailsList, if (doctorPhoneList.length == 0) { None } else { Some(doctorPhoneList) }, doctorAddressList,
        if (doctorEmailList.length == 0) { None } else { Some(doctorEmailList) }, doctorConsultationDetails,
        doctorSetting, None, result, searchable, Some(identiy), docOtherDetaislObj, Some(practiesdetails))

    }

  }

  def appointmentStatus(appointmentID: Long): Option[buildDoctorAppointmentList] = db withSession { implicit session =>

    val appointments = doctorAppointments.filter(x => x.AppointmentID === appointmentID).firstOption
    val appointmentExtn = doctorAppointmentExtn.filter(x => x.AppointmentID === appointmentID).firstOption
    val otherDetailsList: List[TPatientOtherDetails] = patientOtherDetails.filter(c => c.PatientID === appointments.get.PatientID).list

    var isEmailEnabled = "Y"
    if (appointments.get.HospitalCode != None) {
      val hospitalDetails = hospitalExtn.filter { x => x.HospitalCode === appointments.get.HospitalCode.get && x.HospitalID === x.ParentHospitalID }.firstOption
    }

    var patDispID: Option[String] = None
    var FamilyDisplayID: Option[String] = None
    var patLastVsted: Option[String] = None

    if (otherDetailsList(0).PatientDisplayID != None) {
      if (appointments.get.HospitalCode != None) {
        patDispID = Some(otherDetailsList(0).PatientDisplayID.get.get(appointments.get.HospitalCode.getOrElse("")).getOrElse(""))
        patLastVsted = Some(otherDetailsList(0).PatientDisplayID.get.get(appointments.get.HospitalCode.getOrElse("") + "_LASTVISITED").getOrElse(""))
      } else {
        patDispID = Some(otherDetailsList(0).PatientDisplayID.get.get("DOCTORID" + appointments.get.DoctorID.getOrElse(0L).toString).getOrElse(""))
        patLastVsted = Some(otherDetailsList(0).PatientDisplayID.get.get("DOCTORID" + appointments.get.DoctorID.getOrElse(0L).toString + "_LASTVISITED").getOrElse(""))

      }
    }
    //converting 159-RAD-101 to RAD-101
    if (otherDetailsList(0).FamilyDisplayID != None) {
      val fullPatFamID = otherDetailsList(0).FamilyDisplayID.get.get(appointments.get.HospitalCode.getOrElse("")).getOrElse("")
      if (fullPatFamID != "") {
        val patFamIDArry = fullPatFamID.split("-")
        if (patFamIDArry.length == 3)
          FamilyDisplayID = Some(patFamIDArry(1) + "-" + patFamIDArry(2))
      }
    }

    val clinicPreference = new clinicPreferenceObj(appointments.get.HospitalID, patDispID, FamilyDisplayID, patLastVsted, Some(isEmailEnabled))

    var visitRsnID: Option[Long] = None
    var premilinaryNotes: Option[String] = None
    var referralObj: Option[sourceDetailsObj] = None

    if (appointmentExtn != None) {
      visitRsnID = appointmentExtn.get.VisitRsnID
      premilinaryNotes = appointmentExtn.get.PreliminaryNotes
      if (appointmentExtn.get.ReferralID != None) {
        referralObj = Some(new sourceDetailsObj(appointmentExtn.get.ReferralID.get, appointmentExtn.get.SubReferralID))
      }
    }

    if (appointments != None) {

      Some(new buildDoctorAppointmentList(0, appointments.get.AppointmentID, appointments.get.AddressConsultID, appointments.get.DayCD,
        appointments.get.ConsultationDt, appointments.get.StartTime, appointments.get.EndTime, appointments.get.PatientID,
        appointments.get.VisitUserTypeID, appointments.get.DoctorID, appointments.get.FirstTimeVisit,
        appointments.get.DoctorVisitRsnID, appointments.get.OtherVisitRsn, appointments.get.AppointmentStatus,
        appointments.get.TokenNumber, visitRsnID, premilinaryNotes, referralObj, Some(clinicPreference)))

    } else {
      None
    }

  }
  def buildPatientAddressResponse(id: List[Long], includePatientDetails: Option[String]): List[TPatientAddress] = db withSession { implicit session =>
    if (Some("N").equals(includePatientDetails)) {
      log.info("Not Included Address")
      val patientAddressList: List[TPatientAddress] = List[TPatientAddress]()
      return patientAddressList
    } else {
      log.info(" Included Address")
      val patientAddressList: List[TPatientAddress] = patientAddress.filter(c => c.PatientID inSet id).list
      return patientAddressList
    }
  }
  def buildPatientPhoneResponse(id: List[Long], includePatientDetails: Option[String]): List[TPatientPhones] = db withSession { implicit session =>
    if (Some("N").equals(includePatientDetails)) {
      log.info("Not Included Phone")
      val patientPhoneList: List[TPatientPhones] = List[TPatientPhones]()
      return patientPhoneList
    } else {
      log.info(" Included Phone")
      val patientPhoneList: List[TPatientPhones] = patientPhones.filter(c => c.PatientID inSet id).list
      return patientPhoneList
    }
  }

  def buildPatientEmailResponse(id: List[Long], includePatientDetails: Option[String]): List[TPatientEmail] = db withSession { implicit session =>
    if (Some("N").equals(includePatientDetails)) {
      log.info("Not Included Phone")
      return List[TPatientEmail]()
    } else {
      log.info(" Included Phone")
      return patientEmails.filter(c => c.PatientID inSet id).list
    }
  }

  def buildPatientResponse(id: List[Long], includePatientDetails: Option[String]): List[TPatient] = db withSession { implicit session =>
    if (Some("N").equals(includePatientDetails)) {
      log.info("Not Included Patient")
      val patientList: List[TPatient] = List[TPatient]()
      return patientList
    } else {
      log.info("Included Patient")
      val patientList: List[TPatient] = patients.filter(c => c.PatientID inSet id).list
      return patientList
    }
  }

  def getDoctorAddressPhone(DoctorID: Long, AddressID: Long): List[TDoctorAddressPhone] = db withSession { implicit session =>
    doctorAddressPhone.filter(c => c.DoctorID === DoctorID && c.AddressID === AddressID).list
  }

  def getDoctorAddress(DoctorID: Long, AddressType: String, AddressID: Long): List[TDoctorAddress] = db withSession { implicit session =>
    doctorAddress.filter(c => c.DoctorID === DoctorID && c.AddressType === AddressType && c.AddressID === AddressID).list
  }

  def getDoctorPhone(DoctorID: Long, PhType: String): List[TDoctorPhones] = db withSession { implicit session =>
    doctorPhones.filter(c => c.DoctorID === DoctorID && c.PhType === PhType).list
  }

  def getDoctorEmail(DoctorID: Long, EmailIDType: String): List[TDoctorEmail] = db withSession { implicit session =>
    doctorEmails.filter(c => c.DoctorID === DoctorID && c.EmailIDType === EmailIDType).list
  }

  def getDoctorEmailID(DoctorID: Long, EmailIDType: String): Option[TDoctorEmail] = db withSession { implicit session =>
    doctorEmails.filter(c => c.DoctorID === DoctorID && c.EmailIDType === EmailIDType).firstOption
  }
  def forgotPasswordResponse(id: Long): forgotPasswordObj = db withSession { implicit session =>

    //Get Doctor Details
    val doctorList: List[TDoctor] = doctors.filter(c => c.DoctorID === id).list
    val doctorDetailsList: Array[DoctorDetailsObj] = new Array[DoctorDetailsObj](doctorList.length)

    for (i <- 0 until doctorList.length) {

      val doctorExtnList: List[TDoctorExtn] = doctorsExtn.filter(c => c.DoctorID === id).list

      for (j <- 0 until doctorExtnList.length) {
        val image: Option[String] = DoctorS3ImageDao.downloadHelperService(doctorList(i).DoctorID, doctorList(i).DoctorID + "/profileImage.txt")

        doctorDetailsList(i) = new DoctorDetailsObj(doctorList(i).FirstName, doctorList(i).LastName, doctorList(i).FullName, doctorList(i).DoctorID, doctorList(i).Gender, doctorList(i).DOB,
          doctorList(j).SpecialityCD, doctorExtnList(j).MemberID, doctorExtnList(i).Qualification, doctorList(i).Exp,
          doctorList(i).DoctorLoginID, None, doctorExtnList(j).About, None, doctorExtnList(j).Awards,
          doctorExtnList(j).Prospect, doctorExtnList(j).ReferredBy, Some(doctorList(i).TermsAccepted), image,
          doctorList(i).Title, doctorList(i).RegistrationNo)
      }

    }

    //Get Avaiable Phones
    val phoneList: List[TDoctorPhones] = doctorPhones.filter(c => c.DoctorID === id).list
    val doctorPhoneList: Array[DoctorPhoneDetailsObj] = new Array[DoctorPhoneDetailsObj](phoneList.length)

    for (i <- 0 until phoneList.length) {

      doctorPhoneList(i) = new DoctorPhoneDetailsObj(None, None, None,
        phoneList(i).PhNumber, phoneList(i).PhType, phoneList(i).PrimaryIND, None)

    }

    //Get Avaiable Emails
    val emailList: List[TDoctorEmail] = doctorEmails.filter(c => c.DoctorID === id).list
    val doctorEmailList: Array[DoctorEmailDetailsObj] = new Array[DoctorEmailDetailsObj](emailList.length)

    for (i <- 0 until emailList.length) {

      val crtUSRType: Option[String] = Some(emailList(i).CrtUSRType)
      val crtUSR: Option[Long] = Some(emailList(i).CrtUSR)

      doctorEmailList(i) = new DoctorEmailDetailsObj(emailList(i).EmailID, emailList(i).EmailIDType, emailList(i).PrimaryIND, crtUSR,
        crtUSRType, emailList(i).UpdtUSR, emailList(i).UpdtUSRType, None)
    }

    //Get Password
    var loginID: String = ""
    var password: String = ""
    val docLogin: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorID === id).firstOption

    if (docLogin != None) {
      loginID = docLogin.get.DoctorLoginID
      password = docLogin.get.DoctorPassword

    }

    return new forgotPasswordObj(doctorDetailsList(0).FullName, doctorDetailsList(0).Gender,
      doctorDetailsList(0).DOB, doctorDetailsList(0).SpecialityCD, doctorDetailsList(0).MemberID, loginID,
      password, doctorPhoneList, doctorEmailList)

  }

  def getTopAppointmentRecords(data: Option[TDoctorAppointment]): String = db withSession { implicit session =>

    if (data.get.AppointmentStatus.equals("S") && data.get.TokenNumber.toLong != 0) {
      //val consultTable: Option[TDoctorConsultation] = doctorConsultation.filter(c => c.AddressConsultID === data.get.AddressConsultID).firstOption
      val buildQuery = (for {
        getByDoctorID <- doctorAppointments.filter(c => c.DoctorID === data.get.DoctorID && c.ConsultationDt === data.get.ConsultationDt && c.AddressConsultID === data.get.AddressConsultID && c.AppointmentStatus === "B")
      } yield (getByDoctorID)).list

      log.info("buildQuery:" + buildQuery);
      val currentToken = data.get.TokenNumber.toLong
      val currentSeq = data.get.StartTime.replaceAll(":", "").toString().toInt

      log.info("currentSeq :" + currentSeq)
      var currentBooKing: List[buildDoctorAppointmentList] = Nil
      val patientIds = buildQuery.map { x => x.PatientID.getOrElse(0.toLong) }
      val otherDetailsList: List[TPatientOtherDetails] = patientOtherDetails.filter(c => c.PatientID inSet patientIds).list

      for (i <- 0 until buildQuery.length) {

        var patDispID: Option[String] = None
        var FamilyDisplayID: Option[String] = None
        var patLastVsted: Option[String] = None
        val otherdetails = otherDetailsList.filter { x => x.PatientID == buildQuery(i).PatientID.getOrElse(0.toLong) }.headOption
        if (otherdetails.get.PatientDisplayID != None) {
          if (buildQuery(i).HospitalCode != None) {
            patDispID = Some(otherdetails.get.PatientDisplayID.get.get(buildQuery(i).HospitalCode.getOrElse("")).getOrElse(""))
            patLastVsted = Some(otherdetails.get.PatientDisplayID.get.get(buildQuery(i).HospitalCode.getOrElse("") + "_LASTVISITED").getOrElse(""))
          } else {
            patDispID = Some(otherdetails.get.PatientDisplayID.get.get("DOCTORID" + buildQuery(i).DoctorID.getOrElse(0L).toString).getOrElse(""))
            patLastVsted = Some(otherdetails.get.PatientDisplayID.get.get("DOCTORID" + buildQuery(i).DoctorID.getOrElse(0L).toString + "_LASTVISITED").getOrElse(""))

          }
        }
        //converting 159-RAD-101 to RAD-101
        if (otherdetails.get.FamilyDisplayID != None) {
          val fullPatFamID = otherDetailsList(0).FamilyDisplayID.get.get(buildQuery(i).HospitalCode.getOrElse("")).getOrElse("")
          if (fullPatFamID != "") {
            val patFamIDArry = fullPatFamID.split("-")
            if (patFamIDArry.length == 3)
              FamilyDisplayID = Some(patFamIDArry(1) + "-" + patFamIDArry(2))
          }
        }

        val clinicPreference = new clinicPreferenceObj(buildQuery(i).HospitalID, patDispID, FamilyDisplayID, patLastVsted, None)

        val seqNo = (buildQuery(i).StartTime.replaceAll(":", "").toString()).toInt

        currentBooKing = currentBooKing ::: List(new buildDoctorAppointmentList(seqNo, buildQuery(i).AppointmentID, buildQuery(i).AddressConsultID,
          buildQuery(i).DayCD, buildQuery(i).ConsultationDt, buildQuery(i).StartTime,
          buildQuery(i).EndTime, buildQuery(i).PatientID, buildQuery(i).VisitUserTypeID,
          buildQuery(i).DoctorID, buildQuery(i).FirstTimeVisit, buildQuery(i).DoctorVisitRsnID,
          buildQuery(i).OtherVisitRsn, buildQuery(i).AppointmentStatus,
          buildQuery(i).TokenNumber, None, None, None, Some(clinicPreference)))

      }

      currentBooKing = currentBooKing.sortBy { x => x.SeqNo }

      val queryResult = currentBooKing.filter(c => c.SeqNo > currentSeq).take(3)

      log.info("data" + data.get.AppointmentID)

      log.info("queryResult.length" + queryResult.length)

      for (i <- 0 until queryResult.length) {

        log.info("queryResult" + i + ": Value:" + queryResult(i).AppointmentID)

        if (i == 0 && queryResult(i).TokenNumber.toLong == (currentToken + 1)) {
          AppointmentStatusDao.sendAppointmentStatus321SMS(data, "next")
        }

        if (i == 1 && queryResult(i).TokenNumber.toLong == (currentToken + 2)) {

          /*   val mappedValue:TDoctorAppointment = new TDoctorAppointment(queryResult(i).AppointmentID, queryResult(i).AddressConsultID, 
              queryResult(i).DayCD, queryResult(i).ConsultationDt, queryResult(i).StartTime, queryResult(i).EndTime, queryResult(i).PatientID, 
              queryResult(i).VisitUserTypeID, queryResult(i).DoctorID, queryResult(i).FirstTimeVisit,
              queryResult(i).DoctorVisitRsnID, queryResult(i).OtherVisitRsn, queryResult(i).AppointmentStatus, queryResult(i).TokenNumber,
              0,None,None,0, "ADMIN",None,None)
       
          AppointmentStatusDao.sendAppointmentStatus321SMS(Some(mappedValue), "2nd") */
        }

        if (i == 2 && queryResult(i).TokenNumber.toLong == (currentToken + 3)) {
          /*val mappedValue:TDoctorAppointment = new TDoctorAppointment(queryResult(i).AppointmentID, queryResult(i).AddressConsultID, 
              queryResult(i).DayCD, queryResult(i).ConsultationDt, queryResult(i).StartTime, queryResult(i).EndTime, queryResult(i).PatientID, 
              queryResult(i).VisitUserTypeID, queryResult(i).DoctorID, queryResult(i).FirstTimeVisit,
              queryResult(i).DoctorVisitRsnID, queryResult(i).OtherVisitRsn, queryResult(i).AppointmentStatus, queryResult(i).TokenNumber,
              0,None,None,0, "ADMIN",None,None)
          AppointmentStatusDao.sendAppointmentStatus321SMS(Some(mappedValue), "3rd")*/
        }

      }

    } else {
      log.info("Cant Accept Status")
    }
    return "Success"
  }

  import models.com.hcue.doctors.helper.CalenderDateConvertor
  import models.com.hcue.doctors.Json.Response.AddressUpdateSchdular.buildAddressResponseObj
  import java.util.Calendar


  def alldoctorquery(hospitalcd: String, pageNumber: Int, pageSize: Int,searchtext: String) = sql"select * from getalldoctors('#$pageNumber', '#$pageSize', '#$hospitalcd', '#$searchtext')".as[(Long, String, String, String, Boolean)]

  def getDoctorList(hospitalcd: String, pagenumber: Int, pagesize: Int,searchtext: String): Array[getDoctorsLstObj] = db withSession { implicit session =>

    val queryResult = alldoctorquery(hospitalcd, pagenumber, pagesize,searchtext).list

    val result: Array[getDoctorsLstObj] = new Array[getDoctorsLstObj](queryResult.length)

    for (i <- 0 until queryResult.length) {

      result(i) = new getDoctorsLstObj(queryResult(i)._1, queryResult(i)._2, Some(queryResult(i)._3), Some(queryResult(i)._4), Some(queryResult(i)._5))

    }
    return result
  }

  def getLocation(id: String): List[TLocationLkup] = db withSession { implicit session =>
    locationLkup.filter(c => c.LocationID === id).list
  }

  def getState(id: Option[String]): List[TStateLkup] = db withSession { implicit session =>
    stateLkup.filter(c => c.StateID === id).list
  }

  def getCounrty(id: String): List[TCountryLkup] = db withSession { implicit session =>
    countryLkup.filter(c => c.CountryID === id).list
  }
  def getCity(id: Option[String]): List[TCityLkup] = db withSession { implicit session =>
    cityLkup.filter(c => c.CityID === id).list
  }

}