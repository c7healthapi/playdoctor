package models.com.hcue.doctors.traits.list.Doctor

import scala.slick.driver.PostgresDriver.simple._
import models.com.hcue.doctors.Json.Request.getDtAppointmentDetailsObj
import models.com.hcue.doctors.Json.Response.DoctorAppointmentResponseObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorEmail

trait ListDoctor {

}

