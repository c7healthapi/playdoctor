package models.com.hcue.doctors.traits.list.search

import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB
import play.Logger
import models.com.hcue.doctors.Json.Response.DoctorAddressDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorEmailDetailsObj
import models.com.hcue.doctors.Json.Response.DoctorAddressExtnObj
import models.com.hcue.doctors.Json.Response.AddressImagesObj
import models.com.hcue.doctors.Json.Request.doctorSearchDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.hospitalInfoObj
import models.com.hcue.doctors.Json.Request.getDtAppointmentDetailsObj
import models.com.hcue.doctors.traits.Amazon.S3.Image.DoctorS3ImageDao
import play.api.libs.json.Json
import org.joda.time.DateTime
import play.api.libs.json.JsValue
import scala.slick.jdbc.{ GetResult, StaticQuery }
import StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.GetResult
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao
import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.slick.transactTable.patient.TPatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.PatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.TPatientEmail
import models.com.hcue.doctors.slick.transactTable.patient.PatientEmail
import models.com.hcue.doctors.slick.transactTable.patient.TPatientAddress
import models.com.hcue.doctors.slick.transactTable.patient.PatientAddress
import models.com.hcue.doctors.slick.transactTable.patient.TPatientOtherID
import models.com.hcue.doctors.slick.transactTable.patient.PatientOtherID
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.search.TDoctorAddressSearch
import models.com.hcue.doctors.slick.transactTable.doctor.search.DoctorAddressSearch
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressPhone
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddressPhone
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.login.TDoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.login.DoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointments
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorSpecialityCD
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorSpecialityCD
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorSpecialityCD
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorSpecialityCD
import models.com.hcue.doctors.traits.lookup.LookupDAO
import models.com.hcue.doctors.Json.Response.DoctorAppointmentResponseObj
import models.com.hcue.doctors.Json.Response.PatientDetailsObj
import models.com.hcue.doctors.Json.Request.patientPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.patientAddressDetailsObj
import models.com.hcue.doctors.Json.Request.patientEmailDetailsObj
import models.com.hcue.doctors.Json.Request.patientOtherIdDetailsObj
import models.com.hcue.doctors.Json.Response.buildResponseObj
import models.com.hcue.doctors.Json.Response.getResponseObj
import models.com.hcue.doctors.Json.Response.DoctorConsultationDetailsObj
import models.com.hcue.doctors.Json.Response.DoctorDetailsObj
import models.com.hcue.doctors.Json.Response.SearchOutPutObj
import models.com.hcue.doctors.Json.Response.DoctorDetailObj
import models.com.hcue.doctors.dataCasting.DataCasting
//import models.com.hcue.doctors.traits.hcueProfileImgCloudFront
//import models.com.hcue.doctors.traits.hcueCookieHost
import models.com.hcue.doctors.Json.Response.DoctorPublicationAchievementObj
import models.com.hcue.doctors.Json.Response.DoctorEducationObj
import models.com.hcue.doctors.Json.Response.furtherCareDoctorObj
import models.com.hcue.doctors.Json.Response.specialityDetails
import models.com.hcue.doctors.Json.Request.PatientAppDoctorSearchDetailsObj
import models.com.hcue.doctors.Json.Response.SearchDoctorDetailsObj
import models.com.hcue.doctors.slick.lookupTable.DoctorSpecialityLkup
import models.com.hcue.doctors.Json.Response.DoctorSpecialityDetails
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.Json.Response.DoctorDetailObj
import models.com.hcue.doctors.slick.lookupTable.TDoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.CityLkup
import scala.util.control.Exception
import models.com.hcue.doctors.Json.Response.DoctorCommunicationObj
import java.util.Calendar
import java.text.SimpleDateFormat
import java.util.TimeZone
import java.util.Locale
import java.util.Arrays.ArrayList
import java.util.ArrayList
import models.com.hcue.doctors.helper.HCueUtils
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalAddress
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.hospitals.THospital
import models.com.hcue.doctors.Json.Response.PhoneDetailObj
import models.com.hcue.doctors.Json.Response.AdditionWebsiteInfoObj
import models.com.hcue.doctors.Json.Response.ConsultInfObj
import models.com.hcue.doctors.Json.Response.AdditionalInfoObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorConsultation
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorConsultations
import models.com.hcue.doctors.Json.Response.SpecialityDetailsObj

object SearchDoctorDao extends SearchDoctor {

  val log = Logger.of("application")

  log.info("Created c3p0 connection pool")

  val doctors = Doctors.Doctors

  val doctorsExtn = DoctorsExtn.DoctorsExtn

  val doctorPhones = DoctorPhones.DoctorsPhones

  val doctorEmails = DoctorEmail.DoctorEmails

  val doctorAddressSearch = DoctorAddressSearch.DoctorAddressSearch

  val doctorAddress = DoctorAddress.DoctorsAddress

  val doctorAddressByDoctorID = DoctorAddress.DoctorsAddress.sortBy(_.DoctorID.asc)

  val doctorAppointments = DoctorAppointments.DoctorAppointments

  val doctorsAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

  val patients = Patients.patients

  val patientPhones = PatientPhones.patientsPhones

  val patientEmails = PatientEmail.patientsEmail

  val patientAddress = PatientAddress.patientsAddress

  val patientOtherID = PatientOtherID.patientsOtherID

  val doctorSpecialityCD = DoctorSpecialityCD.DoctorSpecialityCD

  val doctorSpecialityLkup = DoctorSpecialityLkup.DoctorSpecialityLkup

  val hospital = Hospital.Hospital.sortBy(_.HospitalID.asc.nullsFirst)

  val hospitalAddress = HospitalAddress.HospitalAddress

  val cityLkup = CityLkup.CityLkup

  val doctorSpecialitys = DoctorSpecialityCD.DoctorSpecialityCD

  val doctorSpecialityType = DoctorSpecialityLkup.DoctorSpecialityLkup
  
  var doctorAddressPhone = DoctorAddressPhone.DoctorAddressPhone
  
  val doctorConsultation = DoctorConsultations.DoctorConsultations

  def stringconvertion(value: Option[String]): String = db withSession { implicit session =>

    if (value == None) {
      return "BLANK"
    }
    val valueLength: Int = value.toString().length()
    val valueName: String = value.toString()

    return valueName.substring(5, valueLength - 1)

  }

  import models.com.hcue.doctors.Json.Response.hospitalPartnerObj
  import models.com.hcue.doctors.Json.Response.BranchInformationObj
  import models.com.hcue.doctors.Json.Response.hospitalDetailsObj
  import models.com.hcue.doctors.Json.Response.hospitalAddressObj
  
  def searchByHospitalCode(DoctorIDs: Option[List[Long]], HospitalID: Option[Long], HospitalCD: String, PageNumber: Int, 
                           PageSize: Int,ActiveDoctors:Option[String]): Option[Array[BranchInformationObj]] = db withSession { implicit session =>

    val offset = (PageNumber - 1) * PageSize

    var queryForHospitalInfo = if(HospitalID != None) { for {
      (hospital, hospitalAdd) <- hospital innerJoin hospitalAddress on (_.HospitalID === _.HospitalID)
      if hospital.HospitalID === HospitalID.get
      } yield (hospital, hospitalAdd)
    } else { 
      for { (hospital, hospitalAdd) <- hospital innerJoin hospitalAddress on (_.HospitalID === _.HospitalID)
      if hospital.HospitalCode === HospitalCD
      } yield (hospital, hospitalAdd) 
    }

    val queryResultForHospitalInfo = queryForHospitalInfo.list

    if (queryResultForHospitalInfo.length == 0) {
      return None
    }

    val resultSet: Array[BranchInformationObj] = new Array[BranchInformationObj](queryResultForHospitalInfo.length)
    val hospitalIds = queryResultForHospitalInfo.map(f => f._1.HospitalID)

     var queryForDoctorsInfo =   for {
          (docAdd, docAddExt) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
          if docAddExt.HospitalID inSet hospitalIds 
          if docAddExt.RoleID =!= null.asInstanceOf[Option[String]] && docAddExt.GroupID =!= null.asInstanceOf[Option[String]]
          if docAdd.Active === "Y" && docAddExt.IsConsultant === "Y"
     } yield (docAdd, docAddExt)
       
     if(ActiveDoctors != None && ActiveDoctors.get.equals("Y")) {
         queryForDoctorsInfo = for {
                (docAdd, docAddExt) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
                if docAddExt.HospitalID inSet hospitalIds 
                if docAddExt.RoleID =!= null.asInstanceOf[Option[String]] && docAddExt.GroupID =!= null.asInstanceOf[Option[String]]
                if docAdd.Active === "Y" && docAddExt.IsConsultant === "Y"
             } yield (docAdd, docAddExt)
     }else if(ActiveDoctors != None && ActiveDoctors.get.equals("N")) {

         queryForDoctorsInfo = for {
                (docAdd, docAddExt) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
                if docAddExt.HospitalID inSet hospitalIds 
                if docAddExt.RoleID =!= null.asInstanceOf[Option[String]] && docAddExt.GroupID =!= null.asInstanceOf[Option[String]]
                if docAdd.Active === "N" && docAddExt.IsConsultant === "Y"
             } yield (docAdd, docAddExt)
       
     } else if(ActiveDoctors != None && ActiveDoctors.get.equals("")) {
         queryForDoctorsInfo = for {
                (docAdd, docAddExt) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
                if docAddExt.HospitalID inSet hospitalIds  
                if docAddExt.RoleID =!= null.asInstanceOf[Option[String]] && docAddExt.GroupID =!= null.asInstanceOf[Option[String]]
                if docAddExt.IsConsultant === "Y"
             } yield (docAdd, docAddExt)
       
     } 

    var record = queryForDoctorsInfo
    if (DoctorIDs != None) {
      record = queryForDoctorsInfo.filter(_x => _x._1.DoctorID inSet DoctorIDs.get.toList)
    }

    val queryResultForDoctorsInfo = record.drop(offset).take(PageSize).list

    val count = record.list.length

    val doctorGroup = queryResultForDoctorsInfo.map(f => (f._1.DoctorID, f._1.AddressID,f._1.Active, f._2.HospitalID, f._2.MinPerCase)).toList
    val doctorIDs = doctorGroup.map(f => f._1).toList
    val addressIDs = doctorGroup.map(f => f._2).toList

    val resultInfo = (for {
      (doc, docExtn) <- doctors innerJoin doctorsExtn on (_.DoctorID === _.DoctorID)
      if doc.DoctorID inSet doctorIDs
    } yield (doc, docExtn)).list

    for (i <- 0 until queryResultForHospitalInfo.length) {

      val hospitalDetail: hospitalDetailsObj = new hospitalDetailsObj(queryResultForHospitalInfo(i)._1.HospitalID,
        queryResultForHospitalInfo(i)._1.ParentHospitalID, queryResultForHospitalInfo(i)._1.HospitalName,
        queryResultForHospitalInfo(i)._1.ContactName, queryResultForHospitalInfo(i)._1.About,
        queryResultForHospitalInfo(i)._1.RegsistrationDate, queryResultForHospitalInfo(i)._1.ActiveIND, 
        queryResultForHospitalInfo(i)._1.TermsAccepted)
 
      val hospitalAddres: hospitalAddressObj = new hospitalAddressObj(queryResultForHospitalInfo(i)._2.Address1,
        queryResultForHospitalInfo(i)._2.Address2, queryResultForHospitalInfo(i)._2.Street,
        queryResultForHospitalInfo(i)._2.Location, queryResultForHospitalInfo(i)._2.CityTown,
        queryResultForHospitalInfo(i)._2.DistrictRegion, queryResultForHospitalInfo(i)._2.State,
        queryResultForHospitalInfo(i)._2.PostCode, queryResultForHospitalInfo(i)._2.Country,
        queryResultForHospitalInfo(i)._2.LandMark, queryResultForHospitalInfo(i)._2.Latitude,
        queryResultForHospitalInfo(i)._2.Longitude)

      val doctorGroupInfo = doctorGroup.filter(p => p._4 == Some(queryResultForHospitalInfo(i)._1.HospitalID))
      val hospitalDoctorIDs = doctorGroupInfo.map(f => f._1).toList
      val hospitalAddressIDs = doctorGroupInfo.map(f => f._2).toList

      val branchDoctorDetails: Array[DoctorDetailObj] = new Array[DoctorDetailObj](doctorGroupInfo.length)
      
      for (j <- 0 until doctorGroupInfo.length) {
        val doctorInfo = resultInfo.find(_x => _x._1.DoctorID == doctorGroupInfo(j)._1)
        val minPerCase = doctorGroup.find(p => p._1 == doctorGroupInfo(j)._1 && p._2 == doctorGroupInfo(j)._2).map(f => f._5).getOrElse(Some(15))
        branchDoctorDetails(j) = new DoctorDetailObj(doctorGroupInfo(j)._1, doctorInfo.get._1.FullName, doctorGroupInfo(j)._2, doctorGroup(j)._3,doctorInfo.get._1.Gender, "Y", doctorInfo.get._1.SpecialityCD,
                                                      doctorInfo.get._2.ProfilImage, Some(minPerCase.getOrElse(15)), None, None)
      }
      
      resultSet(i) = new BranchInformationObj(new hospitalPartnerObj(hospitalDetail, Some(hospitalAddres)), branchDoctorDetails, doctorGroupInfo.size, None)
    }
    return Some(resultSet)
  }
  
  
  def searchByHospitalIDS(DoctorIDs: Option[List[Long]], HospitalID: Long, PageNumber: Int, PageSize: Int,
                          FilterByDate: Option[java.sql.Date], FilterByEndDate: Option[java.sql.Date],ActiveDoctors:Option[String]): Option[BranchInformationObj] = db withSession { implicit session =>

    val offset = (PageNumber - 1) * PageSize

    var queryForHospitalInfo = for {
      (hospital, hospitalAdd) <- hospital innerJoin hospitalAddress on (_.HospitalID === _.HospitalID)
      if hospital.HospitalID === HospitalID
    } yield (hospital, hospitalAdd)

    val queryResultForHospitalInfo = queryForHospitalInfo.list

    if (queryResultForHospitalInfo.length == 0) {
      return None
    }

    val resultSet: Array[hospitalPartnerObj] = new Array[hospitalPartnerObj](queryResultForHospitalInfo.length)

    for (i <- 0 until queryResultForHospitalInfo.length) {

                                        
      val hospitalDetail: hospitalDetailsObj = new hospitalDetailsObj(queryResultForHospitalInfo(i)._1.HospitalID,
        queryResultForHospitalInfo(i)._1.ParentHospitalID, queryResultForHospitalInfo(i)._1.HospitalName,
        queryResultForHospitalInfo(i)._1.ContactName, queryResultForHospitalInfo(i)._1.About,
        queryResultForHospitalInfo(i)._1.RegsistrationDate, queryResultForHospitalInfo(i)._1.ActiveIND, 
        queryResultForHospitalInfo(i)._1.TermsAccepted)
 
      val hospitalAddres: hospitalAddressObj = new hospitalAddressObj(queryResultForHospitalInfo(i)._2.Address1,
        queryResultForHospitalInfo(i)._2.Address2, queryResultForHospitalInfo(i)._2.Street,
        queryResultForHospitalInfo(i)._2.Location, queryResultForHospitalInfo(i)._2.CityTown,
        queryResultForHospitalInfo(i)._2.DistrictRegion, queryResultForHospitalInfo(i)._2.State,
        queryResultForHospitalInfo(i)._2.PostCode, queryResultForHospitalInfo(i)._2.Country,
        queryResultForHospitalInfo(i)._2.LandMark, queryResultForHospitalInfo(i)._2.Latitude,
        queryResultForHospitalInfo(i)._2.Longitude)

      resultSet(i) = new hospitalPartnerObj(hospitalDetail, Some(hospitalAddres))
    }

    // query for main branch doctor information
     //val doctorInfo = doctors.filter { x => x.Searchable === "Y"}.list
     //val doctorIds = doctorInfo.map { x => x.DoctorID }

     var queryForDoctorsInfo =   for {
          (docAdd, docAddExt) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
          if docAddExt.HospitalID === HospitalID && docAdd.Active === "Y" && docAddExt.IsConsultant === "Y"
     } yield (docAdd, docAddExt)
       
     if(ActiveDoctors != None && ActiveDoctors.get.equals("Y")) {
         queryForDoctorsInfo = for {
                (docAdd, docAddExt) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
                if docAddExt.HospitalID === HospitalID && docAdd.Active === "Y" && docAddExt.IsConsultant === "Y"
             } yield (docAdd, docAddExt)
     }else if(ActiveDoctors != None && ActiveDoctors.get.equals("N")) {

         queryForDoctorsInfo = for {
                (docAdd, docAddExt) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
                if docAddExt.HospitalID === HospitalID && docAdd.Active === "N" && docAddExt.IsConsultant === "Y"
             } yield (docAdd, docAddExt)
       
     } else if(ActiveDoctors != None && ActiveDoctors.get.equals("")) {
         queryForDoctorsInfo = for {
                (docAdd, docAddExt) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
                if docAddExt.HospitalID === HospitalID && docAddExt.IsConsultant === "Y"
             } yield (docAdd, docAddExt)
       
     } 
     
    var record = queryForDoctorsInfo

    if (DoctorIDs != None) {
      record = queryForDoctorsInfo.filter(_x => _x._1.DoctorID inSet DoctorIDs.get.toList)
    }

    val queryResultForDoctorsInfo = record.drop(offset).take(PageSize).list

    val count = record.list.length

    val doctorGroup = queryResultForDoctorsInfo.map(f => (f._1.DoctorID, f._1.AddressID,f._1.Active, f._2.MinPerCase)).toList
    val doctorIDs = doctorGroup.map(f => f._1).toList
    val addressIDs = doctorGroup.map(f => f._2).toList

    val resultInfo = (for {
      (doc, docExtn) <- doctors innerJoin doctorsExtn on (_.DoctorID === _.DoctorID)
      if doc.DoctorID inSet doctorIDs
    } yield (doc, docExtn)).list

    val branchDoctorDetails: Array[DoctorDetailObj] = new Array[DoctorDetailObj](doctorGroup.length)
    val currentDayCD = CalenderDateConvertor.getDayofTheWeek(CalenderDateConvertor.getISDTimeZoneDate())

    var startDate = new java.sql.Date(CalenderDateConvertor.getISDTimeZoneDate().getTimeInMillis())
    var endDate = new java.sql.Date(CalenderDateConvertor.getISDTimeZoneDate().getTimeInMillis())

    if (FilterByDate != None) {
      startDate = FilterByDate.get
    }

    if (FilterByEndDate != None) {
      endDate = FilterByEndDate.get
    }

    var totalAppointmentCount: Option[Long] = Some(0)
    //New Changes
    for (i <- 0 until doctorGroup.length) {


      var available = "N"
      var count: Option[Int] = None
      
      val doctorInfo = resultInfo.find(_x => _x._1.DoctorID == doctorGroup(i)._1)
   
      branchDoctorDetails(i) = new DoctorDetailObj(doctorGroup(i)._1, doctorInfo.get._1.FullName, doctorGroup(i)._2, doctorGroup(i)._3,doctorInfo.get._1.Gender, available, doctorInfo.get._1.SpecialityCD,
        doctorInfo.get._2.ProfilImage, Some(doctorGroup(i)._4.getOrElse(15)), count, None)

    }
    return Some(new BranchInformationObj(resultSet(0), branchDoctorDetails.toList.sortBy { x => x.FullName}.toArray, count, totalAppointmentCount))

  }
  
  def getDoctorDetailsByOptions(AddressID: Long): getResponseObj = db withSession { implicit session =>

    //Get Avaiable Address
    val addressList: List[TDoctorAddress] = doctorAddress.filter(c => c.AddressID === AddressID).list
    val doctorAddressList: Array[DoctorAddressDetailsObj] = new Array[DoctorAddressDetailsObj](addressList.length)

    for (i <- 0 until addressList.length) {

      //Get Doctor Extentions:

      val addressExtList: Option[TDoctorAddressExtn] = doctorsAddressExtn.filter(c => c.DoctorID === addressList(i).DoctorID && c.AddressID === AddressID).firstOption
      val addressPhineList: Option[TDoctorAddressPhone] = doctorAddressPhone.filter(c => c.DoctorID === addressList(i).DoctorID && c.AddressID === AddressID).firstOption

      if (addressExtList != None) {

        var addressImagesObj: Option[AddressImagesObj] = None //new AddressImagesObj(addressExtList.get.ClinicImages.get, A)

        if (addressExtList.get.ClinicImages != None) {

          import models.com.hcue.doctors.traits.Amazon.S3.Image.DoctorS3ImageSchedularDao

        /* // val AWS_S3_CREDENTIAL = models.com.hcue.doctors.traits.awsCredentials
          val AWS_DOC_PROFILE_CLOUD_FRONT_DOMAIN = hcueProfileImgCloudFront.HCUE_DOC_PROFILE_CLOUD_FRONT_DOMAIN

          val AWS_HCUE_DOMAIN_PROTOCOL = hcueCookieHost.CLOUD_FRONT_URL_PROTOCOL

          val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
          cal.add(java.util.Calendar.MONTH, 1);

          val QueryString = DoctorS3ImageSchedularDao.createQueryString(cal)

          val filePath = addressList(i).DoctorID.toString() + "/Clinic/" + addressList(i).AddressID.toString + "@@FileName" + ".png" + QueryString

          val signedDevPayUrl: String = AWS_HCUE_DOMAIN_PROTOCOL + "://" + AWS_DOC_PROFILE_CLOUD_FRONT_DOMAIN + "/" + filePath

          var A: Map[String, String] = Map()

          addressExtList.get.ClinicImages.get.foreach {
            p =>
             
              val data = signedDevPayUrl.replaceAll("@@FileName", p._2.toString())

              A += (p._1 -> data)

          }
*/
          var addressImagesObj = new AddressImagesObj(addressExtList.get.ClinicImages.get, addressExtList.get.ClinicDocuments)

        }
           val addressPhoneObj = addressPhineList.find(_x => _x.AddressID == addressExtList.get.AddressID).toList
      
           var addressCommunication :Array[DoctorCommunicationObj] = new Array[DoctorCommunicationObj](addressPhoneObj.length)
               for (i <- 0 until addressPhoneObj.length) {

         addressCommunication(i) = new DoctorCommunicationObj(addressPhoneObj(i).PhCntryCD,addressPhoneObj(i).PhStateCD,addressPhoneObj(i).PhAreaCD,addressPhoneObj(i).PhNumber,addressPhoneObj(i).PhType,addressPhoneObj(i).RowID,addressPhoneObj(i).PrimaryIND,addressPhoneObj(i).PublicDisplay)
       
    }
        var hospitalInfo: Option[hospitalInfoObj] = None
        if (addressExtList.get.HospitalID != None) {
          val hospitalDtls: Option[THospital] = hospital.filter(c => c.HospitalID === addressExtList.get.HospitalID ).firstOption
           if(hospitalDtls != None) {
              hospitalInfo = Some(new hospitalInfoObj(addressExtList.get.HospitalID.get, hospitalDtls.get.ParentHospitalID, addressExtList.get.HospitalCode, hospitalDtls.get.BranchCode,None,None,None))
           }
        }
        val doctorAddressExtList: DoctorAddressExtnObj = new DoctorAddressExtnObj(addressExtList.get.HospitalID, addressExtList.get.PrimaryIND,
        	addressExtList.get.Latitude, addressExtList.get.Longitude,addressCommunication, addressImagesObj, hospitalInfo, None, None,None,None, None,None,None)
        doctorAddressList(i) = new DoctorAddressDetailsObj(Some(addressList(i).AddressID), addressList(i).ClinicName,addressList(i).ClinicSettings, addressList(i).Address1,
          addressList(i).Address2, addressList(i).Street, addressList(i).Location, addressList(i).CityTown, addressList(i).DistrictRegion,
          addressList(i).State, addressList(i).PostCode, addressList(i).Country, addressList(i).AddressType, addressList(i).LandMark, Some(addressList(i).Active), 
          Some(doctorAddressExtList),None,addressList(i).Address4)
      } else {
        doctorAddressList(i) = new DoctorAddressDetailsObj(Some(addressList(i).AddressID), addressList(i).ClinicName,addressList(i).ClinicSettings, addressList(i).Address1,
          addressList(i).Address2, addressList(i).Street, addressList(i).Location, addressList(i).CityTown, addressList(i).DistrictRegion,
          addressList(i).State, addressList(i).PostCode, addressList(i).Country, addressList(i).AddressType, addressList(i).LandMark, Some(addressList(i).Active), None,
          None,addressList(i).Address4)
      }

    }

    val DoctorID: Long = addressList(0).DoctorID

    //Get Doctor Details
    val doctorList: List[TDoctor] = doctors.filter(c => c.DoctorID === DoctorID).list
    val doctorDetailsList: Array[DoctorDetailsObj] = new Array[DoctorDetailsObj](doctorList.length)
    val doctorExtnList: List[TDoctorExtn] = doctorsExtn.filter(c => c.DoctorID === DoctorID).list
    for (i <- 0 until doctorList.length) {

      val doctorExtnList: List[TDoctorExtn] = doctorsExtn.filter(c => c.DoctorID === DoctorID).list

      for (j <- 0 until doctorExtnList.length) {

        val image: Option[String] = DoctorS3ImageDao.downloadHelperService(doctorList(i).DoctorID, "/profile.txt")
 
        doctorDetailsList(i) = new DoctorDetailsObj(doctorList(i).FirstName,doctorList(i).LastName,doctorList(i).FullName, doctorList(i).DoctorID, doctorList(i).Gender, doctorList(i).DOB,
          doctorList(j).SpecialityCD, doctorExtnList(j).MemberID, doctorExtnList(i).Qualification, doctorList(i).Exp,
          doctorList(i).DoctorLoginID,None, doctorExtnList(j).About,None, doctorExtnList(j).Awards,
          doctorExtnList(j).Prospect, doctorExtnList(j).ReferredBy, Some(doctorList(i).TermsAccepted), image,doctorList(i).Title,doctorList(i).RegistrationNo)
      }
 
    }

    //Get Avaiable Phones
    val phoneList: List[TDoctorPhones] = doctorPhones.filter(c => c.DoctorID === DoctorID).list
    val doctorPhoneList: Array[DoctorPhoneDetailsObj] = new Array[DoctorPhoneDetailsObj](phoneList.length)

    for (i <- 0 until phoneList.length) {

      doctorPhoneList(i) = new DoctorPhoneDetailsObj(phoneList(i).PhCntryCD, phoneList(i).PhStateCD, 
          phoneList(i).PhAreaCD, phoneList(i).PhNumber, phoneList(i).PhType, phoneList(i).PrimaryIND,None)

    }
   //Get AddressPhones
    val communicationList: List[TDoctorAddressPhone] = doctorAddressPhone.filter(c => c.DoctorID ===DoctorID ).list
    val doctorCommunicationList: Array[DoctorCommunicationObj] = new Array[DoctorCommunicationObj](communicationList.length)

    for (i <- 0 until communicationList.length) {

      doctorCommunicationList(i) = new DoctorCommunicationObj(communicationList(i).PhCntryCD,communicationList(i).PhStateCD, communicationList(i).PhAreaCD, communicationList(i).PhNumber, communicationList(i).PhType,communicationList(i).RowID,communicationList(i).PrimaryIND,communicationList(i).PublicDisplay)

    }
    //Get Avaiable Phones
    val emailList: List[TDoctorEmail] = doctorEmails.filter(c => c.DoctorID === DoctorID).list
    val doctorEmailList: Array[DoctorEmailDetailsObj] = new Array[DoctorEmailDetailsObj](emailList.length)
    //log.info("::Input String Value:11:")
    for (i <- 0 until emailList.length) {

      val crtUSRType: Option[String] = Some(emailList(i).CrtUSRType)
      val crtUSR: Option[Long] = Some(emailList(i).CrtUSR)

      doctorEmailList(i) = new DoctorEmailDetailsObj(emailList(i).EmailID, emailList(i).EmailIDType, emailList(i).PrimaryIND, crtUSR,
        crtUSRType, emailList(i).UpdtUSR, emailList(i).UpdtUSRType,None)

    }
    
     val consultationList: List[TDoctorConsultation] = doctorConsultation.filter(c => c.DoctorID === DoctorID && c.AddressID === AddressID && c.Active =!= "H").list
    val doctorConsultationDetails: Array[DoctorConsultationDetailsObj] = new Array[DoctorConsultationDetailsObj](consultationList.length)

    for (i <- 0 until consultationList.length) {

      doctorConsultationDetails(i) = new DoctorConsultationDetailsObj(consultationList(i).AddressConsultID, consultationList(i).AddressID,
        consultationList(i).DoctorID, consultationList(i).DayCD, consultationList(i).StartTime, consultationList(i).EndTime,
        consultationList(i).MinPerCase, consultationList(i).Fees, consultationList(i).Active)

    }
     
     val result: Array[SpecialityDetailsObj] = new Array[SpecialityDetailsObj](0)
    
    val specialityCd = new Array[DoctorSpecialityDetails](0)
   
    var DoctorSettings: Option[Map[String, String]] = None
    for (i <- 0 until doctorExtnList.length) {
      
     DoctorSettings = doctorExtnList(i).DoctorSettings
    }
 
return new getResponseObj(doctorDetailsList,Some(doctorPhoneList), doctorAddressList, Some(doctorEmailList), doctorConsultationDetails,DoctorSettings,None, result, None, None, None,None)
 
  }

  def getDoctorDetails(obj: PatientAppDoctorSearchDetailsObj): (Array[SearchDoctorDetailsObj], Int) = db withSession { implicit session =>

    var queryCount = obj.Count

    var gender = obj.Gender.getOrElse("")
    var doctorName = obj.DoctorName.getOrElse("")
    var FilterDay = ""
    var StartTime = ""
    var EndTime = ""
    var DisStartRange = 0.0
    var DisEndRange = 0.0
    var Book = ""
    var Available = ""
    var ExactDay_Date = ""
    
    if(obj.AvailabilityInfo != None)
    {
     FilterDay  = obj.AvailabilityInfo.get.FilterDay.getOrElse("")
     StartTime = obj.AvailabilityInfo.get.StartTime.getOrElse("")
     EndTime = obj.AvailabilityInfo.get.EndTime.getOrElse("")
     DisStartRange = obj.AvailabilityInfo.get.DisStartrange.getOrElse(0.0)
     DisEndRange = obj.AvailabilityInfo.get.DisEndRange.getOrElse(1000.0)     
     Book = obj.AvailabilityInfo.get.Book.getOrElse("")
     Available = obj.AvailabilityInfo.get.Available.getOrElse("")    
     if(FilterDay.equalsIgnoreCase("ALLDAY"))
     {
       FilterDay = ""
     }   
     if(Book.equalsIgnoreCase("Y"))
     {
       Book = "N"
     }
     else
     {
       Book = "Y"
     }
     if(Available.equalsIgnoreCase("Y"))
     { 
      val calendar :Calendar = Calendar.getInstance(TimeZone.getDefault());
      calendar.add(Calendar.HOUR, 5)
      calendar.add(Calendar.MINUTE, 30)
      val Hr : Int = calendar.get(Calendar.HOUR_OF_DAY)    
      val now = Calendar.getInstance().getTime()
      val minuteFormat = new SimpleDateFormat("mm")
      val currentMinuteAsString = minuteFormat.format(now)        
      val ExactTime = Hr+":"+currentMinuteAsString     
      val day = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())    
      val ExactDay = day.toUpperCase().substring(0, 3)
      ExactDay_Date = ExactDay+"~" +ExactTime
     }
     else
     {
       ExactDay_Date = "";
     }
    }

    if (gender.equals("F")) {
      gender = "M"
    } else if (gender.equals("M")) {
      gender = "F"
    }

    if (doctorName.length() > 0) {
      doctorName = "%" + doctorName + "%"
    }


   
    var pageNo = obj.PageNumber - 1
    if (pageNo == 0) {
log.info("------------Inside Count If()------------"+queryCount)
log.info("------------FeeStartRange------------"+obj.FeeStartRange.getOrElse(0))
log.info("------------FeeEndRange)------------"+obj.FeeEndRange.getOrElse(1000))
log.info("------------ExpStartRange------------"+obj.ExpStartRange.getOrElse(0))
log.info("------------ExpEndRange------------"+obj.ExpEndRange.getOrElse(100))
      queryCount = doctorSearchCountQuery(obj.CityName,obj.LocalityName.getOrElse(""),obj.Latitude, obj.Longitude,gender, obj.SpecialityID.getOrElse(""),FilterDay, StartTime, EndTime, DisStartRange, DisEndRange, Book, ExactDay_Date, obj.FeeStartRange.getOrElse(0),obj.FeeEndRange.getOrElse(1000),obj.ExpStartRange.getOrElse(0),obj.ExpEndRange.getOrElse(100),obj.Sort,obj.DoctorIDs.getOrElse("")).firstOption.get
      log.info("------------ Inside queryCount------------"+queryCount)
    }
    
log.info("------------Count2------------"+queryCount)
    var orderBy = if (obj.Sort.equals("EXP")) { "DESC" } else { "ASC" }

    //setting the Order By 
    if (obj.OrderBy != None) {
      orderBy = obj.OrderBy.get
    }

    log.info("Query Count :" + queryCount)
    log.info("Gender : " + gender)
    log.info("Latitude : " + obj.Latitude)
    log.info("Longitude : " + obj.Longitude)
    log.info("SpecialityID :" + obj.SpecialityID.getOrElse(""))
    log.info("Sort :" + obj.Sort)
    log.info("OrderBy:" + orderBy)
    log.info("Doctor Name :" + obj.DoctorName.getOrElse(""))
    
    var searchResultDetails: Array[SearchDoctorDetailsObj] = new Array[SearchDoctorDetailsObj](0)
    if(queryCount > 0){
      
    //pageNo = pageNo * obj.PageSize     
    val doctorSearchList = getdoctordetailsinfoQuery(obj.CityName,obj.LocalityName.getOrElse(""),obj.Latitude, obj.Longitude, gender, obj.SpecialityID.getOrElse(""), FilterDay, StartTime, EndTime, DisStartRange, DisEndRange, Book, ExactDay_Date, obj.FeeStartRange.getOrElse(0),obj.FeeEndRange.getOrElse(1000),obj.ExpStartRange.getOrElse(0),obj.ExpEndRange.getOrElse(100),pageNo, obj.PageSize, obj.Sort, orderBy,obj.DoctorIDs.getOrElse("")).list

    searchResultDetails = new Array[SearchDoctorDetailsObj](doctorSearchList.size)

    var arrSpecilaityDesc: Array[DoctorSpecialityDetails] = new Array[DoctorSpecialityDetails](0)
    
    

    /*      ClinicName: String, Location: String, Latitude: String, Longitude: String, FullName: String, Medico: String, DoctorID: Long, Gender: String, 
     *      Exp: Int, Fees: Int, Distance: String, Address: String, SpecialityID: Array[models.com.hcue.doctors.Json.Response.DoctorSpecialityDetails],
     *       Qualification: String, ProfileImage: Option[String], AddressID: Long, PhoneCountryCD: Int, PhoneNumber: Long, Prospect: String
    
             ClinicName: String, Location: String, Latitude: String, Longitude: String, FullName: String,
     Medico: String, DoctorID: Long, Gender: String, Exp: Int, Fees: Int, Distance: String, Address: String, SpecialityID: Array[models.com.hcue.doctors.Json.Response.DoctorSpecialityDetails], Qualification: String, ProfileImage: Option[String], AddressID: Long, PhoneCountryCD: Int, PhoneNumber: Long, Prospect: String, SeoName: String
*/

    val doctorIds = doctorSearchList.map(f => f._6).distinct.toList
    
    val addressIDs = doctorSearchList.map(f => f._15).distinct.toList.distinct
        
    val doctorsAddressExtnInfo = doctorsAddressExtn.filter { x => x.AddressID inSet addressIDs }.list

    val speciality = doctorSpecialityType.list
    
    val buildQuery = (for {
      (doc, docExtn) <- doctors join doctorsExtn on (_.DoctorID === _.DoctorID) //doctorAddressByDoctorID.filter(c => c.ClinicName.toLowerCase === (clinicName.toLowerCase()))
      if doc.DoctorID inSet doctorIds
    } yield (doc, docExtn)).list

    for (i <- 0 until doctorSearchList.size) {

      var qualification = ""
      var registration_Number = ""
      var prospectFlag = ""
      var showTimings : Option[String] = None

      //Get DoctorInfo 
      val doctorInfo = buildQuery.find(p => p._1.DoctorID == doctorSearchList(i)._6)

      var specialityCd = new Array[DoctorSpecialityDetails](0)
          
      if (doctorInfo != None) {

        if (doctorInfo.get._2.Qualification != None) {

          doctorInfo.get._2.Qualification.get.foreach { x =>
            qualification = qualification + x._2 + ","

          }
          
          if(qualification.size > 0){
            qualification = qualification.substring(0, qualification.length()-1)
          }
        }
        
        
        if (doctorInfo.get._2.MemberID != None) {
          
          doctorInfo.get._2.MemberID.get.foreach { x =>
            registration_Number = registration_Number + x._2 + ","
          }
          
          if(registration_Number.size > 0){
            registration_Number=registration_Number.substring(0, registration_Number.length()-1)
          }
          
        }
        
        
        if (doctorInfo.get._1.SpecialityCD != None) {
          
          specialityCd = new Array[DoctorSpecialityDetails](doctorInfo.get._1.SpecialityCD.get.size)
          var record = 0
          doctorInfo.get._1.SpecialityCD.get.foreach { x =>
            
            //Find Desc
            val sepcialityInfo =  speciality.find { _x => _x.DoctorSpecialityID == x._2}
            
            var sepcialityDesc = sepcialityInfo.get.DoctorSpecialityDesc

            var specialityOESDesc = if(sepcialityInfo.get.DoctorSpecialityDesc != None){
              sepcialityDesc.replace(" ", "-")
              
            } else {""}
            
            
            
            specialityCd(record) = new DoctorSpecialityDetails(record.toString, x._2, sepcialityDesc, None, specialityOESDesc)
            record = record + 1
          }
        }

      }

      
        /* val addressExtList: List[TDoctorAddressExtn] = if (doctorSearchList(i)._15 != 0) {
          doctorsAddressExtn.filter { x => x.AddressID === doctorSearchList(i)._15 }.list
        } else {
          doctorsAddressExtn.list
        }*/
        var addressImagesObj: Option[AddressImagesObj] = None
        val addressExtObj = doctorsAddressExtnInfo.find(_x => _x.AddressID == doctorSearchList(i)._15)
        if (addressExtObj != None) {
          if (addressExtObj.get.ClinicImages != None) {
            addressImagesObj = Some(new AddressImagesObj(addressExtObj.get.ClinicImages.get, addressExtObj.get.ClinicDocuments))
          }
        }
        
        
        
        var PhoneDetailObj: Option[PhoneDetailObj] = None
        val phoneObj = doctorSearchList(i)._16        
        var  phone_phCD:String= ""
        var  phone_phNumber:String= ""
        var  phone_phPublicDisplay:String= ""
        if(phoneObj != "")
        {
          val PhoneObj_arr  : Array[String] = phoneObj.split("~")
          if(!PhoneObj_arr.isEmpty)
          {
          phone_phCD = PhoneObj_arr(0)
          phone_phNumber = PhoneObj_arr(1)
          phone_phPublicDisplay = PhoneObj_arr(2)    
          }
          
          PhoneDetailObj = Some(new PhoneDetailObj(phone_phCD,phone_phNumber,phone_phPublicDisplay))
        }
        
       
        var propectTimingObj = doctorSearchList(i)._18
        
        if(propectTimingObj != "")
          
        {
          val propectTimingObj_arr : Array[String] = propectTimingObj.split("~")
          
          if(!propectTimingObj_arr.isEmpty)
          {
            if(propectTimingObj_arr.length >1){   prospectFlag = propectTimingObj_arr(0)
          showTimings = Some(propectTimingObj_arr(1))}
            
            else { prospectFlag = propectTimingObj_arr(0) ; showTimings = Some("Y") }
          
          
          }
        }
      
        var DoctorURL : Option[String] = None
        val URL = "https://www.myhcue.com"
        DoctorURL = Some(URL +"/"+ doctorSearchList(i)._20.toLowerCase() +"/doctor/"+ doctorSearchList(i)._19) 
        
        var additionalWebsiteInfo = new AdditionWebsiteInfoObj(Some(DoctorURL.get),Some(registration_Number),showTimings)
        
     searchResultDetails(i) = new SearchDoctorDetailsObj(doctorSearchList(i)._1, doctorSearchList(i)._2,
        doctorSearchList(i)._3, doctorSearchList(i)._4, doctorSearchList(i)._5, doctorSearchList(i)._6,
        doctorSearchList(i)._7, doctorSearchList(i)._8,
        doctorSearchList(i)._9, doctorSearchList(i)._10, doctorSearchList(i)._11,
        specialityCd, qualification, Some(doctorSearchList(i)._14), doctorSearchList(i)._15,
        PhoneDetailObj, prospectFlag, doctorSearchList(i)._19, addressImagesObj,additionalWebsiteInfo,None)
    }
    }
    (searchResultDetails, queryCount)

  }


  def getdoctordetailsinfoQuery(CityName:String,Location:String,Latitude: String, Longitude: String, Gender: String, SpecialityID: String, FilterDay: String, StartTime: String,EndTime: String,DisStartRange: Double, DisEndRange: Double,Book: String,ExactDay_Date: String,FeeStartRange: Int, FeeEndRange: Int, ExpStartRange: Int, ExpEndRange: Int ,PageNumber: Int, PageSize: Int, Sort: String, OrderBy: String, DoctorIDs: String) =
    sql"SELECT * FROM getdoctors_web('#$CityName','#$Location','#$Latitude','#$Longitude','#$Gender','#$SpecialityID','#$FilterDay','#$StartTime','#$EndTime',#$DisStartRange,#$DisEndRange,'#$Book','#$ExactDay_Date',#$FeeStartRange,#$FeeEndRange,#$ExpStartRange,#$ExpEndRange,#$PageNumber,#$PageSize,'#$Sort','#$OrderBy','#$DoctorIDs')".
    as[(String, String, String, String, String, Long, String, Int, Int, String, String, String, String, String, Long, String, String,String,String,String,String)]

    
 def doctorSearchCountQuery(CityName:String,Location:String,Latitude: String, Longitude: String, Gender: String, SpecialityID: String,FilterDay: String, StartTime: String,EndTime: String,DisStartRange: Double, DisEndRange: Double,Book: String,ExactDay_Date: String,FeeStartRange: Int, FeeEndRange: Int, ExpStartRange: Int, ExpEndRange: Int ,Sort: String,DoctorIDs : String) =
    sql"SELECT * FROM getdoctors_web_count('#$CityName','#$Location','#$Latitude','#$Longitude','#$Gender', '#$SpecialityID','#$FilterDay','#$StartTime','#$EndTime',#$DisStartRange,#$DisEndRange,'#$Book','#$ExactDay_Date',#$FeeStartRange,#$FeeEndRange,#$ExpStartRange,#$ExpEndRange,'#$Sort','#$DoctorIDs')".as[Int]
    

  
def getSEODoctorId(seoName: String): Long = db withSession { implicit session =>
    var doctorInfo = doctors.filter { x => x.SEOName === seoName }.firstOption
    var doctorIds = doctorInfo.map { x => x.DoctorID }.toList

    var doctorID: Long = 0

    if (doctorIds.length > 0) {
      for (i <- 0 until doctorIds.length) {
        doctorID = doctorIds(0).value
        log.info("SEO name matching Doctor ID :: " + doctorID)
      }
    }
    return doctorID
  }

  def blankValueConvertor(sourceVal: Option[String]): String = {
    if (sourceVal.isEmpty) {
      return ""
    } else {
      return sourceVal.get
    }
  }

  def integerDeafaultValueConvertor(sourceVal: Option[Int]): Int = {
    if (sourceVal.isEmpty) {
      return 0
    } else {
      return sourceVal.get
    }
  }

  def mapdefaultValueConvertor(map: Option[Map[String, String]]): Map[String, String] = {
    if (map.isEmpty) {
      return Map()
    } else {
      return map.get
    }
  }

}
