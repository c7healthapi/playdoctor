package models.com.hcue.doctors.traits.list.search

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB
import play.Logger
import play.api.libs.json.Json
import play.api.libs.json.JsValue
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.dataCasting.DataCasting
import models.com.hcue.doctors.Json.Request.VisitReason.doctorResonSearchDetailsObj
import models.com.hcue.doctors.Json.Response.Doctor.VisitReason.getDoctorVisitRsnObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorVisitRsnLkyp
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorVisitReason

object SearchDoctorReasonDao extends SearchDoctorReason {

  val log = Logger.of("application")

  log.info("Created c3p0 connection pool")

  val doctorVisitReason = DoctorVisitReason.DoctorVisitReason.sortBy(_.SequenceRow.asc)

  def searchBySpecialityID(CS: doctorResonSearchDetailsObj): (Array[getDoctorVisitRsnObj], Int) = db withSession { implicit session =>

    val offset = (CS.PageNumber - 1) * CS.PageSize

    val buildQuery = for {
      getSpecialityId <- doctorVisitReason.filter(c => c.DoctorSpecialityID === CS.DoctorSpecialityID && c.VisitUserTypeID === CS.VisitUserTypeID)
    } yield (getSpecialityId)

    val queryResult = buildQuery.drop(offset).take(CS.PageSize).list

    val searchResultDetails: Array[getDoctorVisitRsnObj] = new Array[getDoctorVisitRsnObj](queryResult.length)

    for (i <- 0 until queryResult.length) {

      searchResultDetails(i) = new getDoctorVisitRsnObj(queryResult(i).DoctorVisitRsnID, queryResult(i).DoctorSpecialityID,
        queryResult(i).DoctorVisitRsnDesc, queryResult(i).VisitUserTypeID, queryResult(i).SequenceRow,queryResult(i).ActiveIND)

    }

    // This finds the total count of all hospitals in the system
    val totalPhoneCount = Query(buildQuery.list.length).first

    (searchResultDetails, totalPhoneCount)

  }

}