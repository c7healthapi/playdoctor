package models.com.hcue.doctors.traits.list.patient

import java.sql.Date

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.jdbc.StaticQuery.interpolation

import models.com.hcue.doctors.Json.Request.doctorPatientSearchDetailsObj
import models.com.hcue.doctors.Json.Request.patientCaseBillingAmtInfo
import models.com.hcue.doctors.Json.Response.buildPatientSearchResponseObj
import models.com.hcue.doctors.Json.Response.currentAgeObj
import models.com.hcue.doctors.dataCasting.DataCasting
import models.com.hcue.doctors.helper.RequestResponseHelper
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointments
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.patient.PatientAddress
import models.com.hcue.doctors.slick.transactTable.patient.PatientEmail
import models.com.hcue.doctors.slick.transactTable.patient.PatientOtherID
import models.com.hcue.doctors.slick.transactTable.patient.PatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import models.com.hcue.doctors.slick.transactTable.patient.TPatientPhones
import models.com.hcue.doctors.traits.dbProperties.db
// import models.com.hcue.doctors.traits.hcueConstants.countryID
import play.Logger
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.patient.TPatientOtherDetails
import models.com.hcue.doctors.slick.transactTable.patient.PatientOtherDetails
import java.text.SimpleDateFormat
import models.com.hcue.doctors.Json.Response.ReferralSourceObj

import models.com.hcue.doctors.slick.transactTable.patient.PatientExtn

object ListPatientDao {

  val patients = Patients.patients.sortBy(_.PatientID.desc.nullsFirst)

  val patient = Patients.patients
  
  val patientOtherDetails = PatientOtherDetails.PatientOtherDetail

  val patientPhones = PatientPhones.patientsPhones

  val patientEmails = PatientEmail.patientsEmail

  val patientAddress = PatientAddress.patientsAddress

  val patientOtherID = PatientOtherID.patientsOtherID

  val doctorAppointment = DoctorAppointments.DoctorAppointments.sortBy { x => x.ConsultationDt.desc }

  val doctorAppointments = DoctorAppointments.DoctorAppointments

  val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

  val hospital = Hospital.Hospital

  val doctor = Doctors.Doctors
  val doctorExtn = DoctorsExtn.DoctorsExtn
  val doctorAddress = DoctorAddress.DoctorsAddress
  val doctorPhone = DoctorPhones.DoctorsPhones
  val doctorEmail = DoctorEmail.DoctorEmails
  
  val patientExtn = PatientExtn.PatientExtn

  //Get Doctor Patients For Commnuication
  def searchCommunicationPatientList_cnt(PatientID : String,searchText: String, searchLocation: String, HospitalCode: String, HospitalID: String, DoctorID: Long, Age: Int, AgeOffSet: Int, MessageType: String, GroupID: String, StartDate: String, EndDate: String, Gender: String, referralID: Long, subReferralID: Long, patientType: String) =
    sql"SELECT * FROM getcommunicationpatient_cnt('#$PatientID','#$searchText','#$searchLocation','#$HospitalCode','#$HospitalID','#$DoctorID','#$Age','#$AgeOffSet','#$MessageType','#$GroupID','#$StartDate','#$EndDate','#$Gender','#$referralID', '#$subReferralID', '#$patientType')".as[Int]

  def searchCommunicationPatientList(PatientID : String,searchText: String, searchLocation: String, HospitalCode: String, HospitalID: String, DoctorID: Long, Age: Int, AgeOffSet: Int, MessageType: String,GroupID: String, StartDate: String, EndDate: String, Gender: String, referralID: Long, subReferralID: Long, patientType: String, PageNumber: Long, PageSize: Long) =
    sql"SELECT * FROM getcommunicationpatients('#$PatientID','#$searchText','#$searchLocation','#$HospitalCode','#$HospitalID','#$DoctorID','#$Age','#$AgeOffSet','#$MessageType','#$GroupID','#$StartDate','#$EndDate','#$Gender','#$referralID', '#$subReferralID','#$patientType','#$PageNumber', '#$PageSize')".as[(Long, String, String, Date, Long, Date, String, String, Long, String, String)]

  def getCommuncationPatients(CS: doctorPatientSearchDetailsObj, IncludeDetails: String, IncludeAll: Option[String], PatientIDs: Option[List[Long]]): (Array[buildPatientSearchResponseObj], Int) = db withSession { implicit session =>

    var searchText = ""
    if (CS.SearchText != None) {
      searchText = CS.SearchText.get.replaceAll("\\s+", " ").trim;
    }
    
     var passpatientlist = ""
  
     if(PatientIDs !=None){
     passpatientlist = PatientIDs.get.mkString(",")
     
     }
     
     var searchLocation = ""
     var searchCity = ""
     var searchState = ""
     var searchCoutry = ""
     var listLocation:List[String] = Nil
     var searchlocationText = ""
     var groupid = ""
     var gender = ""
     var referrelid = 0L
     var subreferrelid = 0L
     var patientType = ""

     if(CS.SearchDetails != None){
       groupid = CS.SearchDetails.get.GroupID.getOrElse(Nil).mkString("~")
       gender = CS.SearchDetails.get.Gender.getOrElse("")
       referrelid = CS.SearchDetails.get.ReferralID.getOrElse(0L)
       subreferrelid = CS.SearchDetails.get.SubReferralID.getOrElse(0L)
       patientType =  CS.SearchDetails.get.PatientType.getOrElse("")
       //making search string for location search
       searchLocation = CS.SearchDetails.get.SearchLocation.getOrElse(Nil).mkString(" ").replaceAll("\\s+", "|").trim;
       searchCity = CS.SearchDetails.get.SearchCityTown.getOrElse("").replaceAll("\\s+", "|").trim;
       searchState = CS.SearchDetails.get.SearchState.getOrElse("").replaceAll("\\s+", "|").trim;
       searchCoutry = CS.SearchDetails.get.SearchCountry.getOrElse("").replaceAll("\\s+", "|").trim;
              
       if(searchCity != ""){          listLocation =  "("+ searchCity +")" :: listLocation        }
       if(searchState != ""){          listLocation =  "("+ searchState +")" :: listLocation        }
       if(searchCoutry != ""){          listLocation =  "("+ searchCoutry +")" :: listLocation        }
       if(searchLocation != ""){          listLocation =  "("+ searchLocation +")" :: listLocation        }
       if(listLocation.length > 0){         searchlocationText = listLocation.mkString("&") }
       
     }
     
     val StartDate: String = CS.StartDate.getOrElse("").toString
    val EndDate: String = CS.EndDate.getOrElse("").toString

     
    val HospitalCode: String = CS.HospitalCD.getOrElse("").toString()

   // var HospitalID: Long = if (CS.HospitalID != None) { CS.HospitalID.get } else if (CS.HospitalIDLst != None) { CS.HospitalIDLst.get.toList.last } else { 0L }

    var branches = if( CS.HospitalIDLst != None) { CS.HospitalIDLst.getOrElse(Nil).mkString("~") } else { 
                            if(CS.HospitalID != None)    {   CS.HospitalID.get.toString()      } else{ "" }  
                          }
    val DoctorID: Long = CS.DoctorID
    val PageNumber: Long = CS.PageNumber
    val PageSize: Long = CS.PageSize
    val MessageTyp: String = CS.MessageType.getOrElse("")

    var patientCommSearch: Array[buildPatientSearchResponseObj] = new Array[buildPatientSearchResponseObj](0)
    val resultcnt = if (PageNumber == 1) {
      
      searchCommunicationPatientList_cnt(passpatientlist,searchText, searchlocationText, HospitalCode, branches, DoctorID, CS.StartAge.getOrElse(0), CS.EndAge.getOrElse(0), MessageTyp, groupid, StartDate, EndDate, gender, referrelid, subreferrelid, patientType ).first
    } else {
      CS.Count.getOrElse(0)
    }
 
    if (resultcnt > 0 || PatientIDs != None) {

      var result = searchCommunicationPatientList(passpatientlist,searchText, searchlocationText, HospitalCode, branches, DoctorID, CS.StartAge.getOrElse(0), CS.EndAge.getOrElse(0), MessageTyp, groupid, StartDate, EndDate, gender, referrelid, subreferrelid, patientType, PageNumber, PageSize).list
    
      if (PatientIDs != None) {
        result = result.filter(x => PatientIDs.get.contains(x._1))
      }

      patientCommSearch = new Array[buildPatientSearchResponseObj](result.length)

      for (i <- 0 until result.length) {

        var currentAge: Option[currentAgeObj] = None

        var lastVisited: Option[java.sql.Date] = None
        if (result(i)._6 != null) {
          lastVisited = Some(result(i)._6)
        }

        var phoneNumber: Option[Long] = None
        if (result(i)._5 != 0 ) {
          phoneNumber = Some(result(i)._5)
        }
        var dob: Option[java.sql.Date] = None
        if (result(i)._4 != null) {
          dob = Some(result(i)._4)
          currentAge = models.com.hcue.doctors.helper.RequestResponseHelper.ageCalculation(dob)
        }
        var displayid: Option[String] = None
        if (result(i)._8 != null) {
          displayid = Some(result(i)._8)
        }
        var pid: Option[Long] = None
        if (result(i)._9 != 0) {
          pid = Some(result(i)._9)
        }

        var title: Option[String] = None
        if (result(i)._2 != null) {
          title = Some(result(i)._2)
        }

        var email: Option[String] = None
        if (result(i)._10 != null) {
          email = Some(result(i)._10)
        }

        var profileImage: Option[String] = None
        if (result(i)._11 != null) {
          profileImage = Some(result(i)._11)
        }
          
        patientCommSearch(i) = new buildPatientSearchResponseObj(result(i)._1, profileImage, title, result(i)_3, currentAge,

          result(i)._7, lastVisited, phoneNumber, email, displayid, None, pid, None, None, None,None,None)
      }
    }
    (patientCommSearch, resultcnt)

  } 
  
  def getDoctorsList(doctorID: Long, hospitalID: Option[Long]): List[Long] = db withSession { implicit session =>

  var doctorIDs: List[Long] = if (hospitalID != None) {
    doctorAddressExtn.filter { x => x.HospitalID === hospitalID.get }.list.map { x => x.DoctorID }
  } else {
    List(doctorID)
  }
  return doctorIDs
}

def getMyPatients(req: doctorPatientSearchDetailsObj): (Array[buildPatientSearchResponseObj], Int) = db withSession { implicit session =>

  var searchText = ""
  if (req.SearchText != None) {
    searchText = req.SearchText.get.replaceAll("\\s+", " ").trim;
  }

  var startDOB = ""
  var endDOB = ""
   var listDocPatients = ""
  
  if(req.SearchDetails != None){
    
    startDOB = req.SearchDetails.get.startDOB.getOrElse("")
    endDOB = req.SearchDetails.get.endDOB.getOrElse("")
    listDocPatients = req.SearchDetails.get.listdocpatients.getOrElse("")
  }
 
  var patientType = ""
  if(req.SearchDetails !=None && req.SearchDetails.get.PatientType != None ){
    patientType = req.SearchDetails.get.PatientType.getOrElse("")
    
  }
  val HospitalCode: String = req.HospitalCD.getOrElse("").toString()

  var HospitalID: Array[Long] = new Array[Long](0)
  if (req.HospitalID != None) {
    HospitalID = List(req.HospitalID.get).toArray
  }

  if (req.HospitalIDLst != None) {
    HospitalID = req.HospitalIDLst.get.toArray
  }

  var hospids = ""
  if (req.HospitalID != None || req.HospitalIDLst != None) { hospids = HospitalID.mkString("~") }

  val DoctorID: Long = req.DoctorID
  val PageNumber: Long = req.PageNumber
  val PageSize: Long = req.PageSize
  val StartDate: String = req.StartDate.getOrElse("").toString
  val EndDate: String = req.EndDate.getOrElse("").toString
   var resultcnt = if (PageNumber == 1) {
    
    if (req.IncludeCommDetails.getOrElse("") != "Y") {
    
      searchPatientListCnt(searchText, HospitalCode, hospids, DoctorID, req.StartAge.getOrElse(0), 
          req.EndAge.getOrElse(0), StartDate, EndDate, startDOB, endDOB, listDocPatients,patientType, 
          PageNumber, PageSize).firstOption.get
    
    } else {
      1
    }
  } else {
    req.Count.getOrElse(0)
  }

  var patientsearch: Array[buildPatientSearchResponseObj] = new Array[buildPatientSearchResponseObj](0)

  if (resultcnt > 0) {
    
    val result = searchPatientList(searchText, HospitalCode, hospids, DoctorID, req.StartAge.getOrElse(0), 
        req.EndAge.getOrElse(0), StartDate, EndDate, startDOB, endDOB, listDocPatients,patientType, 
        PageNumber, PageSize).list

    patientsearch = new Array[buildPatientSearchResponseObj](result.size)

    val patientIDs = result.map(f => f._1).toList
   
    if (req.IncludeCommDetails.getOrElse("") != "Y") {

        /* val patientBillingDetails: List[TPatientCaseBillMaster] = if (HospitalID.length != 0) {
        patientCaseBillMaster.filter { x => (x.HospitalID inSet HospitalID.toList) && (x.PatientID inSet patientIDs) }.list
      } else if (HospitalCode != "") {
        patientCaseBillMaster.filter { x => x.HospitalCode === HospitalCode && (x.PatientID inSet patientIDs) }.list
      } else {
        patientCaseBillMaster.filter { x => x.DoctorID === DoctorID && (x.PatientID inSet patientIDs) }.list
      }*/
      // mypatients outstanding  
      /*val appointmentDtls: List[TDoctorAppointment] = if (HospitalID.length != 0) {
          doctorAppointment.filter { x => (x.HospitalID inSet HospitalID.toList) && (x.PatientID inSet patientIDs) }.list
        } else if (HospitalCode != "") {
          doctorAppointment.filter { x => x.HospitalCode === HospitalCode && (x.PatientID inSet patientIDs) }.list
        } else {
          doctorAppointment.filter { x => x.DoctorID === DoctorID && (x.PatientID inSet patientIDs) }.list
        }
           
      val queryPatientBillingInfo = for {
        (patBillAmt, patBillExtn) <- patientCaseBillAmt innerJoin patientCaseBillExtn on (_.AppointmentID === _.AppointmentID)
        if patBillAmt.AppointmentID inSet appointmentDtls.map { x => x.AppointmentID }.toList
      } yield (patBillAmt, patBillExtn)

      
      val patientBilling = queryPatientBillingInfo.sortBy(f => f._2.Created.desc).list*/

      //Patient Membership Info

      for (i <- 0 until result.size) {

        var patientReviewed: Option[Long] = None
        //val reviewedCases = patientCaseRatings.filter { x => x.PatientID == result(i)._1 && x.Reviewed == "Y" && x.HospitalCD == HospitalCode}.toList
        var reviewed = None//if (reviewedCases.length > 0) { Some("Y") } else { None }

        var billigSet: Option[Array[patientCaseBillingAmtInfo]] = None

          val billingInfo: Array[patientCaseBillingAmtInfo] = new Array[patientCaseBillingAmtInfo](1)
          //billingInfo(0) = new patientCaseBillingAmtInfo(procedureCost, BilledCost, discount, balance)
          billingInfo(0) = new patientCaseBillingAmtInfo(0, 0, 0, 0)
          billigSet = Some(billingInfo)
                
        var currentAge: Option[currentAgeObj] = None

        var lastVisited: Option[java.sql.Date] = None
        if (result(i)._6 != null) {
          lastVisited = Some(result(i)._6)
        }
        var phoneNumber: Option[Long] = None
        if (result(i)._5 != 0) {
          phoneNumber = Some(result(i)._5)
        }
        var dob: Option[java.sql.Date] = None
        if (result(i)._4 != null) {
          dob = Some(result(i)._4)
          currentAge = models.com.hcue.doctors.helper.RequestResponseHelper.ageCalculation(dob)
        }
        var displayid: Option[String] = None
        if (result(i)._8 != null) {
          displayid = Some(result(i)._8)
        }
        var pid: Option[Long] = None
        if (result(i)._9 != 0) {
          pid = Some(result(i)._9)
        }

        var title: Option[String] = None
        if (result(i)._2 != null) {
          title = Some(result(i)._2)
        }

        var email: Option[String] = None
        if (result(i)._10 != null) {
          email = Some(result(i)._10)
        }

        var profileImage: Option[String] = None
        if (result(i)._11 != null) {
          profileImage = Some(result(i)._11)
        }

        var familydisplayid: Option[String] = None
        if (!(result(i)._12 == null || result(i)._12 == "-")) {
          familydisplayid = Some(result(i)._12)
        }

        var familyhdid: Option[Long] = None
        if (result(i)._13 != 0) {
          familyhdid = Some(result(i)._13)
        }
         var vipflag: Option[String] = None
        if (result(i)._16 != 0) {
          vipflag = Some(result(i)._16)
        }
        patientsearch(i) = new buildPatientSearchResponseObj(result(i)._1, profileImage, title, result(i)_3, currentAge,
          result(i)._7, lastVisited, phoneNumber, email, displayid, familydisplayid, pid, familyhdid, billigSet,
            Some(new ReferralSourceObj(Some(result(i)_14),Some(result(i)_15))),vipflag,Some(result(i)_17))

      }
    } else {
      for (i <- 0 until result.size) {

        var currentAge: Option[currentAgeObj] = None

        var lastVisited: Option[java.sql.Date] = None
        if (result(i)._6 != null) {
          lastVisited = Some(result(i)._6)
        }
        var phoneNumber: Option[Long] = None
        if (result(i)._5 != 0) {
          phoneNumber = Some(result(i)._5)
        }
        var dob: Option[java.sql.Date] = None
        if (result(i)._4 != null) {
          dob = Some(result(i)._4)
          currentAge = models.com.hcue.doctors.helper.RequestResponseHelper.ageCalculation(dob)
        }
        var displayid: Option[String] = None
        if (result(i)._8 != null) {
          displayid = Some(result(i)._8)
        }
        var pid: Option[Long] = None
        if (result(i)._9 != 0) {
          pid = Some(result(i)._9)
        }

        var title: Option[String] = None
        if (result(i)._2 != null) {
          title = Some(result(i)._2)
        }

        var email: Option[String] = None
        if (result(i)._10 != null) {
          email = Some(result(i)._10)
        }

        var profileImage: Option[String] = None
        if (result(i)._11 != null) {
          profileImage = Some(result(i)._11)
        }

        var familydisplayid: Option[String] = None
        if (!(result(i)._12 == null || result(i)._12 == "-")) {
          familydisplayid = Some(result(i)._12)
        }

        var familyhdid: Option[Long] = None
        if (result(i)._13 != 0) {
          familyhdid = Some(result(i)._13)
        }
         var vipflag: Option[String] = None
        if (result(i)._16 != 0) {
          vipflag = Some(result(i)._16)
        }
        patientsearch(i) = new buildPatientSearchResponseObj(result(i)._1, profileImage, title, result(i)_3, currentAge,
          result(i)._7, lastVisited, phoneNumber, email, displayid, familydisplayid, pid, familyhdid,  None, 
            Some(new ReferralSourceObj(Some(result(i)_14),Some(result(i)_15))),vipflag,Some(result(i)_17))

      }
    }
  }
  return (patientsearch, resultcnt)
}


// Giving list of patients for offline having between date range for update column 
def getMyPatientsOffline(req: doctorPatientSearchDetailsObj): (Array[buildPatientSearchResponseObj], Int) = db withSession { implicit session =>

  var searchText = ""
  if (req.SearchText != None) {
    searchText = req.SearchText.get.replaceAll("\\s+", " ").trim;
  }

  var startDOB = ""
  var endDOB = ""
  
  if(req.SearchDetails != None){
    
    startDOB = req.SearchDetails.get.startDOB.getOrElse("")
    endDOB = req.SearchDetails.get.endDOB.getOrElse("")
    
  }
  
  
  val HospitalCode: String = req.HospitalCD.getOrElse("").toString()

  var HospitalID: Array[Long] = new Array[Long](0)
  if (req.HospitalID != None) {
    HospitalID = List(req.HospitalID.get).toArray
  }

  if (req.HospitalIDLst != None) {
    HospitalID = req.HospitalIDLst.get.toArray
  }

  var hospids = ""
  if (req.HospitalID != None || req.HospitalIDLst != None) { hospids = HospitalID.mkString("~") }

  val DoctorID: Long = req.DoctorID
  val PageNumber: Long = req.PageNumber
  val PageSize: Long = req.PageSize
  val StartDate: String = req.StartDate.getOrElse("").toString
  val EndDate: String = req.EndDate.getOrElse("").toString

  var resultcnt = if (PageNumber == 1) {

    if (req.IncludeCommDetails.getOrElse("") != "Y") {
      searchPatientListCntOffline(searchText, HospitalCode, hospids, DoctorID, req.StartAge.getOrElse(0), req.EndAge.getOrElse(0), StartDate, EndDate, startDOB, endDOB,PageNumber, PageSize).firstOption.get
    } else {
      1
    }
  } else {
    req.Count.getOrElse(0)
  }

  var patientsearch: Array[buildPatientSearchResponseObj] = new Array[buildPatientSearchResponseObj](0)

  if (resultcnt > 0) {

    val result = searchPatientListOffline(searchText, HospitalCode, hospids, DoctorID, req.StartAge.getOrElse(0), req.EndAge.getOrElse(0), StartDate, EndDate, startDOB, endDOB, PageNumber, PageSize).list

    patientsearch = new Array[buildPatientSearchResponseObj](result.size)

    val patientIDs = result.map(f => f._1).toList


      val appointmentDtls: List[TDoctorAppointment] = if (HospitalID.length != 0) {
        doctorAppointment.filter { x => (x.HospitalID inSet HospitalID.toList) && (x.PatientID inSet patientIDs) }.list
      } else if (HospitalCode != "") {
        doctorAppointment.filter { x => x.HospitalCode === HospitalCode && (x.PatientID inSet patientIDs) }.list
      } else {
        doctorAppointment.filter { x => x.DoctorID === DoctorID && (x.PatientID inSet patientIDs) }.list
      }

      for (i <- 0 until result.size) {

        var patientReviewed: Option[Long] = None

        var billigSet: Option[Array[patientCaseBillingAmtInfo]] = None
       
        var currentAge: Option[currentAgeObj] = None

        var lastVisited: Option[java.sql.Date] = None
        if (result(i)._6 != null) {
          lastVisited = Some(result(i)._6)
        }
        var phoneNumber: Option[Long] = None
        if (result(i)._5 != 0) {
          phoneNumber = Some(result(i)._5)
        }
        var dob: Option[java.sql.Date] = None
        if (result(i)._4 != null) {
          dob = Some(result(i)._4)
          currentAge = models.com.hcue.doctors.helper.RequestResponseHelper.ageCalculation(dob)
        }
        var displayid: Option[String] = None
        if (result(i)._8 != null) {
          displayid = Some(result(i)._8)
        }
        var pid: Option[Long] = None
        if (result(i)._9 != 0) {
          pid = Some(result(i)._9)
        }

        var title: Option[String] = None
        if (result(i)._2 != null) {
          title = Some(result(i)._2)
        }

        var email: Option[String] = None
        if (result(i)._10 != null) {
          email = Some(result(i)._10)
        }

        var profileImage: Option[String] = None
        if (result(i)._11 != null) {
          profileImage = Some(result(i)._11)
        }

        var familydisplayid: Option[String] = None
        if (!(result(i)._12 == null || result(i)._12 == "-")) {
          familydisplayid = Some(result(i)._12)
        }

        var familyhdid: Option[Long] = None
        if (result(i)._13 != 0) {
          familyhdid = Some(result(i)._13)
        }
         
        patientsearch(i) = new buildPatientSearchResponseObj(result(i)._1, profileImage, title, result(i)_3, currentAge,
          result(i)._7, lastVisited, phoneNumber, email, displayid, familydisplayid, pid, familyhdid, billigSet, None,None,None)

      }
  }
  return (patientsearch, resultcnt)
}


//searchPatientList(searchText, HospitalCode, HospitalID, DoctorID ,req.Age,req.AgeOffSet,StartDate,EndDate,PageNumber, PageSize).list

def searchPatientList(searchText: String, HospitalCode: String, HospitalID: String, DoctorID: Long, Age: Int, AgeOffSet: Int, StartDate: String, EndDate: String, StartDOB: String, EndDOB: String, ListDocPatients: String,patientType:String, PageNumber: Long, PageSize: Long) =
  sql"SELECT * FROM getpatient_search('#$searchText','#$HospitalCode','#$HospitalID','#$DoctorID','#$Age','#$AgeOffSet','#$StartDate','#$EndDate','#$StartDOB','#$EndDOB','#$ListDocPatients','#$patientType','#$PageNumber', '#$PageSize')".as[(Long, String, String, Date, Long, Date, String, String, Long, String, String, String, Long,Long,Long,String,String)]

def searchPatientListCnt(searchText: String, HospitalCode: String, HospitalID: String, DoctorID: Long, Age: Int, AgeOffSet: Int, StartDate: String, EndDate: String, StartDOB: String, EndDOB: String, ListDocPatients: String,patientType:String, PageNumber: Long, PageSize: Long) =
  sql"SELECT * FROM getpatientssearchcnt('#$searchText','#$HospitalCode','#$HospitalID','#$DoctorID','#$Age','#$AgeOffSet','#$StartDate','#$EndDate','#$StartDOB','#$EndDOB','#$ListDocPatients','#$patientType','#$PageNumber', '#$PageSize')".as[Int]



def searchPatientListOffline(searchText: String, HospitalCode: String, HospitalID: String, DoctorID: Long, Age: Int, AgeOffSet: Int, StartDate: String, EndDate: String, StartDOB: String, EndDOB: String, PageNumber: Long, PageSize: Long) =
  sql"SELECT * FROM getpatient_searchoffline('#$searchText','#$HospitalCode','#$HospitalID','#$DoctorID','#$Age','#$AgeOffSet','#$StartDate','#$EndDate','#$StartDOB','#$EndDOB','#$PageNumber', '#$PageSize')".as[(Long, String, String, Date, Long, Date, String, String, Long, String, String, String, Long)]

def searchPatientListCntOffline(searchText: String, HospitalCode: String, HospitalID: String, DoctorID: Long, Age: Int, AgeOffSet: Int, StartDate: String, EndDate: String, StartDOB: String, EndDOB: String, PageNumber: Long, PageSize: Long) =
  sql"SELECT * FROM getpatientssearchofflinecnt('#$searchText','#$HospitalCode','#$HospitalID','#$DoctorID','#$Age','#$AgeOffSet','#$StartDate','#$EndDate','#$StartDOB','#$EndDOB','#$PageNumber', '#$PageSize')".as[Int]




  def getPatientsBySearchText(CS: doctorPatientSearchDetailsObj): (Array[buildPatientSearchResponseObj], Int) = db withSession { implicit session =>

    val inst: DataCasting = new DataCasting()
    val offset = (CS.PageNumber - 1) * CS.PageSize
    var searchResultDetails: Array[buildPatientSearchResponseObj] = new Array[buildPatientSearchResponseObj](0)
    var resultcnt = 0

    if (CS.PhoneNumber != None && CS.PhoneNumber.get != "") {

      val mobileNumber: String = CS.PhoneNumber.get.trim()
      var phoneRecords: List[Long] =  sql"SELECT * FROM getpatientbyphonenumber('#$mobileNumber')".as[(Long)].list
      val query = for {
        pat <- patients.filter(c => c.PatientID inSet phoneRecords)
      } yield pat
      var queryResult = query.drop(offset).take(CS.PageSize).list.distinct

      val patientIDs = queryResult.map { x => x.PatientID }.toList
      val patientList: List[TPatient] = patients.filter(c => c.PatientID inSet patientIDs).list
      val otherDetailsList: List[TPatientOtherDetails] = patientOtherDetails.filter(c => c.PatientID inSet patientIDs).list
      val phoneList: List[TPatientPhones] = patientPhones.filter(c => c.PatientID inSet patientIDs).list
      
      val appointmentDtls: List[TDoctorAppointment] = if (CS.HospitalCD != None && CS.HospitalCD.get != "") {
        doctorAppointment.filter { x => x.HospitalCode === CS.HospitalCD.get && (x.PatientID inSet patientIDs) }.list
      } else {
        doctorAppointment.filter { x => x.DoctorID === CS.DoctorID && (x.PatientID inSet patientIDs) }.list
      }


      searchResultDetails = new Array[buildPatientSearchResponseObj](patientList.length)

      for (i <- 0 until patientList.length) {
        val age = RequestResponseHelper.ageCalculation(patientList(i).DOB)
        val phoneNumberQuery = phoneList.filter { x => x.PatientID == patientList(i).PatientID }
        val otherdetails = otherDetailsList.filter { x => x.PatientID == patientList(i).PatientID }.headOption
        
        val patientExtndtls = patientExtn.filter { x => x.PatientID === patientList(i).PatientID }.firstOption
        
        var referralID : Long = 0L;
        var subReferralID: Long = 0L;
        
        if(patientExtndtls != None)
        {
          referralID = patientExtndtls.get.ReferralID.getOrElse(0L)          
          subReferralID = patientExtndtls.get.SubReferralID.getOrElse(0L)
        }
        
        var patDispID: Option[String] = None
        var patLastVsted: Option[String] = None
        var lastVisited: Option[java.sql.Date] = None
        var FamilyDisplayID: Option[String] = None
        
        if(otherdetails.get.PatientDisplayID != None) {
            if(CS.HospitalCD != None){      
              patDispID = Some(otherdetails.get.PatientDisplayID.get.get(CS.HospitalCD.get).getOrElse(""))
              patLastVsted = Some(otherdetails.get.PatientDisplayID.get.get(CS.HospitalCD.get+"_LASTVISITED").getOrElse(""))
            } else {
              patDispID = Some(otherdetails.get.PatientDisplayID.get.get("DOCTORID"+ CS.DoctorID.toString).getOrElse(""))
              patLastVsted = Some(otherdetails.get.PatientDisplayID.get.get("DOCTORID"+ CS.DoctorID.toString+"_LASTVISITED").getOrElse(""))
            }
         }
        
         if(otherdetails.get.FamilyDisplayID != None) {
           val fullPatFamID = otherdetails.get.FamilyDisplayID.get.get(CS.HospitalCD.getOrElse("")).getOrElse("")
           if(fullPatFamID != "") {
             val patFamIDArry = fullPatFamID.split("-")
             if(patFamIDArry.length == 3)
             FamilyDisplayID = Some(patFamIDArry(1)+"-"+ patFamIDArry(2))
           }
         }
         
         if(patLastVsted != None && patLastVsted.get != "") {
           val sdf1 = new SimpleDateFormat("dd-MM-yyyy");
           val date = sdf1.parse(patLastVsted.get);
           lastVisited = Some(new java.sql.Date(date.getTime())) 
         }
         
        var billigSet: Option[Array[patientCaseBillingAmtInfo]] = None
        
        val phoneNumber: Option[Long] = if (phoneNumberQuery.length > 0) { Some(phoneNumberQuery(0).PhNumber) } else { None }
        
        val phNumberQuery = phoneNumberQuery.find { x => x.PhType == "M" }
        var phNumber: String = ""
        if(phNumberQuery != None) {
          phNumber = phNumberQuery.get.PhNumber.toString()
        } else {
          val landlineQuery = phoneNumberQuery.find { x => x.PhType == "L" } 
          if(landlineQuery != None) {
            phNumber = landlineQuery.get.PhStdCD.getOrElse("") + " " + landlineQuery.get.PhNumber.toString()
          }
        }
        
       
      }
      resultcnt = Query(query.list.distinct.length).first
    } else {

      var searchText = ""
      if (CS.SearchText != None) {
        searchText = CS.SearchText.get.replaceAll("\\s+", " ").trim;
      }
      val HospitalCode: String = CS.HospitalCD.getOrElse("").toString()
      var HospitalID: Array[Long] = new Array[Long](0)
      var hospids = ""
      val DoctorID: Long = CS.DoctorID
      val PageNumber: Long = CS.PageNumber
      val PageSize: Long = CS.PageSize
      val StartDate: String = ""
      val EndDate: String = ""

      resultcnt = searchPatientListCnt(searchText, HospitalCode, "", DoctorID, CS.StartAge.getOrElse(0), CS.EndAge.getOrElse(0), "", "", "", "", "","", PageNumber, PageSize).firstOption.get

      if (resultcnt > 0) {
        val result = searchPatientList(searchText, HospitalCode, "", DoctorID, CS.StartAge.getOrElse(0), CS.EndAge.getOrElse(0), "", "", "", "", "","", PageNumber, PageSize).list
        searchResultDetails = new Array[buildPatientSearchResponseObj](result.size)
        for (i <- 0 until result.size) {
          var currentAge: Option[currentAgeObj] = None
          var lastVisited: Option[java.sql.Date] = None
          if (result(i)._6 != null) {
            lastVisited = Some(result(i)._6)
          }
          var phoneNumber: Option[Long] = None
          if (result(i)._5 != 0) {
            phoneNumber = Some(result(i)._5)
          }
          var email: Option[String] = None
          if (result(i)._10 != null) {
            email = Some(result(i)._10)
          }
          var dob: Option[java.sql.Date] = None
          if (result(i)._4 != null) {
            dob = Some(result(i)._4)
            currentAge = models.com.hcue.doctors.helper.RequestResponseHelper.ageCalculation(dob)
          }
          var title: Option[String] = None
          if (result(i)._2 != null) {
            title = Some(result(i)._2)
          }
          var profileImage: Option[String] = None
          if (result(i)._11 != null) {
            profileImage = Some(result(i)._11)
          }
          var familydisplayid: Option[String] = None
          if (!(result(i)._12 == null || result(i)._12 == "-")) {
            familydisplayid = Some(result(i)._12)
          }
          var displayid: Option[String] = None
          if (result(i)._8 != null) {
            displayid = Some(result(i)._8)
          }
          var vipflag : Option[String] = None
            if (result(i)._16!= null) {
            vipflag = Some(result(i)._16)
          }
          searchResultDetails(i) = new buildPatientSearchResponseObj(result(i)._1, None, title, result(i)_3, currentAge, result(i)._7, None, phoneNumber,
            email, displayid, familydisplayid, None, None, None, Some(new ReferralSourceObj(Some(result(i)_14),Some(result(i)_15))),vipflag,None)
        }
      }
    }
    (searchResultDetails, resultcnt)
  }

 def getPatient(id: Long): Option[TPatient] = db withSession { implicit session =>
    patients.filter(c => c.PatientID === id).firstOption
  }

}