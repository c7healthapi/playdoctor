package models.com.hcue.doctors.traits.Amazon.S3.Image

import java.io.ByteArrayInputStream
import java.util.Calendar
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.dateColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.optionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.queryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.traits.hcueEmailCredentials
//import models.com.hcue.doctors.traits.hcueProfileImgCloudFront
//import models.com.hcue.doctors.traits.hcueCookieHost
import models.com.hcue.doctors.traits.dbProperties.db
import play.Logger
import models.com.hcue.doctors.traits.update.UpdateDoctorDao
import models.com.hcue.doctors.Json.Request.S3.Amazon.fileDetailsObj
//import models.com.hcue.doctors.traits.hcueConsentFormCloudFront
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import models.com.hcue.doctors.Json.Request.S3.Amazon.doctorDocumentsObj
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalExtn
//import models.com.hcue.doctors.traits.hcueHospitalProfileCloudFront



object DoctorS3ImageSchedularDao {}