/*package models.com.hcue.doctors.traits.Amazon.S3.Image

import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.util.IOUtils
import models.com.hcue.doctors.traits.dbProperties.db
import play.Logger
import models.com.hcue.doctors.traits.hcueHospitalProfileCloudFront
import models.com.hcue.doctors.traits.hcueCookieHost
import com.amazonaws.services.s3.model.S3ObjectSummary
import models.com.hcue.doctors.Json.Request.S3.Amazon.imgStringObj
import scala.slick.driver.PostgresDriver.simple._
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalExtn
import play.api.libs.json._
import models.com.hcue.doctors.Json.Request.S3.Amazon.fileDetailsObj
import models.com.hcue.doctors.Json.Request.S3.Amazon.doctorDocumentsObj

object HospitalS3ImageDao {

  private val log = Logger.of("application")
  
  val hospitalExtn = HospitalExtn.HospitalExtn

  val AWS_S3_BUCKETS = models.com.hcue.doctors.traits.hcueBuckets
  val AWS_ACCESS_KEY = AWS_S3_BUCKETS.AWS_S3_ACCESS_KEY
  val AWS_SECRET_KEY = AWS_S3_BUCKETS.AWS_S3_SECRET_KEY
  val AWS_HOSPITAL_PROFILE_BUCKET = AWS_S3_BUCKETS.HOSPITAL_PROFILE_IMAGE_BUCKET
  val AWS_CLOUD_FRONT_PEM_CERT = hcueCookieHost.CLOUD_FRONT_PEM_CERT
  val AWS_HCUE_DOMAIN_PROTOCOL = hcueCookieHost.CLOUD_FRONT_URL_PROTOCOL
  val AWS_HOSPITAL_PROFILE_CLOUD_FRONT_DOMAIN = hcueHospitalProfileCloudFront.HCUE_HOSPITAL_PROFILE_CLOUD_FRONT_DOMAIN

  val yourAWSCredentials = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY)
  val amazonS3Client = new AmazonS3Client(yourAWSCredentials)
  val profileBucket = amazonS3Client.createBucket(AWS_HOSPITAL_PROFILE_BUCKET)

  
  def updateHelperService(HospitalID: Long, imageStr: Option[String], filePath: String, fileName: String, updateURL: Boolean): String ={ 

    var input: ByteArrayInputStream = null
    try {
      
      input = new ByteArrayInputStream(imageStr.toString().getBytes());

      amazonS3Client.setEndpoint("https://s3.amazonaws.com")

      val contentBytes = IOUtils.toByteArray(input);
      val metadata = new ObjectMetadata();
          metadata.setContentLength(contentBytes.length);
          metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);

      if (input != null) {
        input.close()
      }

      input = new ByteArrayInputStream(imageStr.toString().getBytes());

      //uploading base64 text file  to S3
      val amazonResponse = amazonS3Client.putObject(profileBucket.getName(), filePath + "/" + fileName + ".txt", input, metadata);

      //Creating Image and uploading into S3
      createPNGImage(HospitalID, profileBucket.getName(), filePath, fileName, imageStr.get, fileName + ".png")
      //Create Signed URL
      if (updateURL) {
          updateSignedURL(HospitalID, fileName)
      }
      "Success"
    } catch {
      case e: Exception =>
        e.printStackTrace()
        "Did not Upload"

    } finally {
      if (input != null)
        input.close()
    }

  }

  
  def deleteHelperService(HospitalID: Long, filePath: String, fileName: String, updateURL: Boolean): String = db withSession { implicit session =>

    try {

      amazonS3Client.setEndpoint("https://s3.amazonaws.com")

      if (updateURL) {
          var File = amazonS3Client.listObjects(profileBucket.getName(), filePath).getObjectSummaries()
          for (i <- 0 until File.size()) {
            val amazonResponse = amazonS3Client.deleteObject(profileBucket.getName(), File.get(i).getKey);
          }
          deleteClinicImage(HospitalID, fileName)
      }
      "Success"
    } catch {
      case e: Exception =>
        e.printStackTrace()
        "Did not Delete"

    }

  }

  def downloadHelperService(DoctorID: Long, imageName: String): Option[String] = db withSession { implicit session =>

    try {
      val s3Object = amazonS3Client.getObject(profileBucket.getName(), imageName);
      log.info("Download object encryption status is " +
        s3Object.getObjectMetadata().getSSEAlgorithm());
      val reader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));
      val str = Stream.continually(reader.readLine()).takeWhile(_ != null).mkString
      return Some(str)
    } catch {
      case e: Exception =>
        log.error("No Image found for Hospital")
        return None

    }
    return None
  }

  import org.apache.commons.codec.binary.Base64
  import java.awt.image.BufferedImage
  import java.io.FileOutputStream
  import java.io.File
  import org.jets3t.service.model.S3Object
  import models.com.hcue.doctors.traits.Amazon.S3.Image.HospitalS3ImageDao

  def createPNGImage(HospitalID: Long, BucketName: String, FolderPath: String, FolderName: String, imgString: String, PNGImgName: String) = db withSession { implicit session =>

    try {

      val yourAWSCredentials = new org.jets3t.service.security.AWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY)
      val amazonS3Client = new org.jets3t.service.impl.rest.httpclient.RestS3Service(yourAWSCredentials)

      // Converting a Base64 String into Image byte array
      val imageByteArray: Array[Byte] = Base64.decodeBase64(imgString)

      //Write a image byte array into file system
      val imageOutFile: FileOutputStream = new FileOutputStream(PNGImgName)
      imageOutFile.write(imageByteArray);
      imageOutFile.close();

      val pngImg: File = new File(PNGImgName)
      val fileObject: S3Object = new S3Object(pngImg);

      val amazonResponse = amazonS3Client.putObject(BucketName + "/" + FolderPath, fileObject);

      if (pngImg != null) {
        pngImg.delete()
      }

    } catch {
      case e: Exception =>
        e.printStackTrace()
    } finally {
    }

  }
  
  def updateSignedURL(HospitalID: Long, Url: String) = db withSession { implicit session =>
    
    var image: Map[String,String] = Map()
    
    val existingClinicImage = hospitalExtn.filter(c => c.HospitalID === HospitalID).map(c => (c.HospitalImages)).list
    
    if (existingClinicImage(0) != None) {       
       existingClinicImage(0).get.foreach { x => image += (x._1.toString() -> x._2.toString()) }
    }
      
    image += ((image.size +1).toString() -> Url)
      
    val status = hospitalExtn.filter(c => c.HospitalID === HospitalID).map(c => (c.HospitalImages)).update(Some(image))
      
    var documents: Option[JsValue] = None
        documents = updateDocuments(HospitalID, image)
    
    val status2 = hospitalExtn.filter(c => c.HospitalID === HospitalID).map(c => (c.HospitalDocuments)).update(documents)
    
  }
  
  
  def updateDocuments(HospitalID: Long, a: Map[String, String]): Option[JsValue] = db withSession { implicit session =>
    var documents: Option[JsValue] = None
    val imageList = a.toList
    if (imageList.length > 0) {
      var filePath: String = HospitalID + "/Images"
      var records: Array[fileDetailsObj] = new Array[fileDetailsObj](imageList.length)
      for (i <- 0 until imageList.length) {
        val imageURL = createImageUrl(filePath, imageList(i)._2, ".png", AWS_HOSPITAL_PROFILE_CLOUD_FRONT_DOMAIN)
        records(i) = new fileDetailsObj(Some(imageURL), imageList(i)._2, ".png", Some(imageList(i)._2), None, None)
      }
      val tempObj: doctorDocumentsObj = new doctorDocumentsObj(HospitalID, None, records)
      if (tempObj.Documents.length > 0) {
        val jsonFormat = (Json.arr(tempObj.Documents))(0)
        documents = Some(jsonFormat)
      }
    }
    documents
  }
  
  def createImageUrl(filePath: String, fileName: String, extn: String, cloudFrontURL: String): String = {
     AWS_HCUE_DOMAIN_PROTOCOL + "://" + cloudFrontURL + "/" + filePath + "/" + fileName + extn
  }
  
  def deleteClinicImage(HospitalID: Long, fileName: String) = db withSession { implicit session =>
    
      var image: Map[String,String] = Map()
      
      val existingClinicImage = hospitalExtn.filter(c => c.HospitalID === HospitalID).map(c => (c.HospitalImages)).list

      if (existingClinicImage(0) != None) {   
        var i =0
        existingClinicImage(0).get.foreach { x => if(x._2.toString() != fileName)
            {
              i=i+1;
              image += ((i).toString() -> x._2.toString())
            }          
        }
      }
 
      val status = hospitalExtn.filter(c => c.HospitalID === HospitalID).map(c => (c.HospitalImages)).update(Some(image))
      
      val documents: Option[JsValue] = updateDocuments(HospitalID, image)
      
      val status2 = hospitalExtn.filter(c => c.HospitalID === HospitalID).map(c => (c.HospitalDocuments)).update(documents)
      
  }



}
*/