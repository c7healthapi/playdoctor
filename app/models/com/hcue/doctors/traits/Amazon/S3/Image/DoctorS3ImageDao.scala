package models.com.hcue.doctors.traits.Amazon.S3.Image

import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.InputStreamReader
import org.apache.commons.codec.binary.Base64
import org.jets3t.service.model.S3Object
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.util.IOUtils
import models.com.hcue.doctors.traits.dbProperties.db
import play.Logger
//import models.com.hcue.doctors.traits.hcueProfileImgCloudFront
//import models.com.hcue.doctors.traits.hcueCookieHost
import com.amazonaws.services.s3.model.S3ObjectSummary
import java.io.InputStream
import java.net.URL
import java.io.FileInputStream
import java.io.BufferedInputStream
import java.io.ByteArrayOutputStream
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import sun.misc.BASE64Encoder
import models.com.hcue.doctors.Json.Request.S3.Amazon.imgStringObj
import play.api.Play.current

object DoctorS3ImageDao {

  private val log = Logger.of("application")

 val s3AccessKey = current.configuration.getString("aws.s3.accessKey").get
 val s3SecretKey = current.configuration.getString("aws.s3.secretKey").get
 
 // val AWS_DOCTOR_PROFILE_BUCKET = AWS_S3_BUCKETS.DOCTOR_PROFILE_IMAGE_BUCKET
 // val AWS_CLOUD_FRONT_PEM_CERT = hcueCookieHost.CLOUD_FRONT_PEM_CERT
  //val AWS_HCUE_DOMAIN_PROTOCOL = hcueCookieHost.CLOUD_FRONT_URL_PROTOCOL

  val yourAWSCredentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey)
  val amazonS3Client = new AmazonS3Client(yourAWSCredentials)
  //val profileBucket = amazonS3Client.createBucket(AWS_DOCTOR_PROFILE_BUCKET)
  
  val profileBucket = amazonS3Client.createBucket("")

  
  def updateHelperService(DoctorID: Long, imageStr: Option[String], filePath: String, fileName: String, updateURL: Boolean, AddressID: Option[Long]): String ={ 

    var input: ByteArrayInputStream = null
    try {
      
      input = new ByteArrayInputStream(imageStr.toString().getBytes());

      amazonS3Client.setEndpoint("https://s3.amazonaws.com")

      val contentBytes = IOUtils.toByteArray(input);
      val metadata = new ObjectMetadata();
      metadata.setContentLength(contentBytes.length);
      metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);

      if (input != null) {
        input.close()
      }

      input = new ByteArrayInputStream(imageStr.toString().getBytes());

      //uploading base64 text file  to S3
      val amazonResponse = amazonS3Client.putObject(profileBucket.getName(), filePath + "/" + fileName + ".txt", input, metadata);

      log.info("Uploaded object encryption status is " + amazonResponse.getSSEAlgorithm());

      // createPNGImage(DoctorID,imageName)

      //Creating Image and uploading into S3
      createPNGImage(DoctorID, profileBucket.getName(), filePath, fileName, imageStr.get, fileName + ".png")
      //Create Signed URL
      if (updateURL) {
        
        if(AddressID == None)
      {
        
        val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
        cal.add(java.util.Calendar.MONTH, 6);

        //val QueryString = DoctorS3ImageSchedularDao.createQueryString(cal, hcueProfileImgCloudFront.HCUE_DOC_PROFILE_CLOUD_FRONT_DOMAIN)

       // val signedDevPayUrl: String = AWS_HCUE_DOMAIN_PROTOCOL + "://" + hcueProfileImgCloudFront.HCUE_DOC_PROFILE_CLOUD_FRONT_DOMAIN + "/" + DoctorID.toString() + "/" + fileName + ".png"

        //val url: Option[String] = Some(signedDevPayUrl + QueryString)
        //DoctorS3ImageSchedularDao.updateSignedURL(DoctorID, url, cal,None)
      }
        else{
          val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
          cal.add(java.util.Calendar.MONTH, 6);
          //DoctorS3ImageSchedularDao.updateSignedURL(DoctorID, Some(fileName),cal,AddressID)
        }
      }
        

      "Success"
      
    } catch {
      case e: Exception =>
        e.printStackTrace()
        "Did not Upload"

    } finally {
      if (input != null)
        input.close()
    }

  }

  def deleteHelperService(DoctorID: Long, filePath: String, fileName: Array[String], deleteall: String, updateURL: Boolean, AddressID: Long): String = db withSession { implicit session =>

    try {

      amazonS3Client.setEndpoint("https://s3.amazonaws.com")

      if (updateURL) {

        if (deleteall != "Y") {

          for (j <- 0 until fileName.length) {
            var File = amazonS3Client.listObjects(profileBucket.getName(), filePath + "/" + fileName(j)).getObjectSummaries()

            for (i <- 0 until File.size()) {
              val amazonResponse = amazonS3Client.deleteObject(profileBucket.getName(), File.get(i).getKey);
              //DoctorS3ImageSchedularDao.deleteClinicImage(DoctorID, AddressID, fileName(j), deleteall)
            }
          }

        } else {

          var File = amazonS3Client.listObjects(profileBucket.getName(), filePath).getObjectSummaries()
          //need to delete complite folder in next version 03-10-2016
          for (i <- 0 until File.size()) {
            val amazonResponse = amazonS3Client.deleteObject(profileBucket.getName(), File.get(i).getKey);

          }

          //DoctorS3ImageSchedularDao.deleteClinicImage(DoctorID, AddressID, "deletefile", deleteall)

        }

      }
      "Success"

    } catch {
      case e: Exception =>
        e.printStackTrace()
        "Did not Deleted"

    }

  }
  
  
    def uploadFileHelperService(DoctorID: Long, imageStr: Option[String], filePath: String, fileName: String, updateURL: Boolean): String = db withSession { implicit session =>

    var input: ByteArrayInputStream = null
    var path = ""
    try {
      
      input = new ByteArrayInputStream(imageStr.toString().getBytes());

      amazonS3Client.setEndpoint("https://s3.amazonaws.com")

      val contentBytes = IOUtils.toByteArray(input);
      val metadata = new ObjectMetadata();
      metadata.setContentLength(contentBytes.length);
      metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);

      if (input != null) {
        input.close()
      }

      input = new ByteArrayInputStream(imageStr.toString().getBytes());
      print("filePath1:::"+filePath)
      //uploading base64 text file  to S3
      val amazonResponse = amazonS3Client.putObject(profileBucket.getName(), filePath + "/" + fileName + ".txt", input, metadata);

      log.info("Uploaded object encryption status is " + amazonResponse.getSSEAlgorithm());

      // createPNGImage(DoctorID,imageName)

      //Creating Image and uploading into S3
      createPNGImage(DoctorID, profileBucket.getName(), filePath, fileName, imageStr.get, fileName + ".png")
      //Create Signed URL
      if (updateURL) {
        
       val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
       cal.add(java.util.Calendar.MONTH, 6);

       //val QueryString = DoctorS3ImageSchedularDao.createQueryString(cal, hcueProfileImgCloudFront.HCUE_DOC_PROFILE_CLOUD_FRONT_DOMAIN)

       //val signedDevPayUrl: String = AWS_HCUE_DOMAIN_PROTOCOL + "://" + hcueProfileImgCloudFront.HCUE_DOC_PROFILE_CLOUD_FRONT_DOMAIN + "/" + filePath + "/" + fileName + ".png"

       //path = signedDevPayUrl + QueryString
      }
       path
      
    } catch {
      case e: Exception =>
        e.printStackTrace()
        "Upload Failed"

    } finally {
      if (input != null)
        input.close()
    }

  }


  def downloadHelperService(DoctorID: Long, imageName: String): Option[String] = db withSession { implicit session =>

    try {
      //val profileBucket = amazonS3Client.createBucket(profileBucket)
      log.info("ImagePath" + imageName)
      val s3Object = amazonS3Client.getObject(profileBucket.getName(), imageName);
      log.info("Download object encryption status is " +
        s3Object.getObjectMetadata().getSSEAlgorithm());
      val reader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));
      val str = Stream.continually(reader.readLine()).takeWhile(_ != null).mkString
      return Some(str)
    } catch {
      case e: Exception =>
        //log.error("No Image found for Doctor " + DoctorID)
        //e.printStackTrace()
        return None

    }
    return None
  }

  def isImageFound(imagetype: String, DoctorID: Long): Boolean = {

    var imageFound = false;
    //val bucketName = AWS_DOCTOR_PROFILE_BUCKET 
    /*
    val yourAWSCredentials = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY)
    val amazonS3Client = new AmazonS3Client(yourAWSCredentials)
*/
    try {
      //val profileBucket = amazonS3Client.createBucket(bucketName)
      val s3Object = amazonS3Client.getObject(profileBucket.getName(), DoctorID.toString() + "/profileImage.png");
      log.info("Download object encryption status is " +
        s3Object.getObjectMetadata().getSSEAlgorithm());
      imageFound = true
    } catch {
      case e: Exception =>
        //log.error("No Image found for Doctor " + DoctorID)
        imageFound = false
    }

    return imageFound
  }

  import org.apache.commons.codec.binary.Base64
  import java.awt.image.BufferedImage
  import java.io.FileOutputStream
  import java.io.File
  import org.jets3t.service.model.S3Object
  import models.com.hcue.doctors.traits.Amazon.S3.Image.DoctorS3ImageSchedularDao

  def createPNGImage(DoctorID: Long, BucketName: String, FolderPath: String, FolderName: String, imgString: String, PNGImgName: String) = db withSession { implicit session =>

    try {

      val yourAWSCredentials = new org.jets3t.service.security.AWSCredentials(s3AccessKey, s3SecretKey)
      val amazonS3Client = new org.jets3t.service.impl.rest.httpclient.RestS3Service(yourAWSCredentials)

      //val s3Object = amazonS3Client.getObject(BucketName, FolderPath + "/" + DocumentName);

      //val reader = new BufferedReader(new InputStreamReader(s3Object.getDataInputStream()));

      /*val imgString = Stream.continually(reader.readLine()).takeWhile(_ != null).mkString.toString()
     val str = imgString.subSequence(5, imgString.length - 1).toString()*/

      // Converting a Base64 String into Image byte array
     val imageByteArray: Array[Byte] = Base64.decodeBase64(imgString)

      //Write a image byte array into file system
      val imageOutFile: FileOutputStream = new FileOutputStream(PNGImgName)
      imageOutFile.write(imageByteArray);
      imageOutFile.close();

      val pngImg: File = new File(PNGImgName)
      val fileObject: S3Object = new S3Object(pngImg);

      val amazonResponse = amazonS3Client.putObject(BucketName + "/" + FolderPath, fileObject);

      if (pngImg != null) {
        pngImg.delete()
      }

    } catch {
      case e: Exception =>
        e.printStackTrace()
      //log.info("No Image found for Doctor " + result(resultObj).DoctorID)
      //logger.error("No Image found for Doctor " + result(resultObj).DoctorID)
    } finally {
      /*if (input != null)
                input.close()*/
    }

  }

  def createImageString(pngImgUrl: Array[String]): Array[imgStringObj] = db withSession { implicit session =>
    val result: Array[imgStringObj] = new Array[imgStringObj](pngImgUrl.length)
       try {  
          for(i <- 0 until pngImgUrl.length) {
        	  
             val post = new HttpGet(pngImgUrl(i))
             val client = HttpClientBuilder.create().build();
             // send the post request
             val response = client.execute(post)
             
             //val client = new DefaultHttpClient()
             //val request = new HttpGet(pngImgUrl(i))
             //var response: HttpResponse = client.execute(request)
             val bis = new BufferedInputStream(response.getEntity.getContent)
             val bos = new ByteArrayOutputStream()
             val chunk = Array.ofDim[Byte](1024)
             var read: Int = 0
             do {
                read = bis.read(chunk)
                if (read >= 0) {
                    bos.write(chunk, 0, read)
                }
            } while (read >= 0);
            val encoder = new BASE64Encoder()
            result(i) = new imgStringObj(pngImgUrl(i), encoder.encode(bos.toByteArray()))
           }
      } catch {
       case e: Exception =>
        e.printStackTrace()
      } 
    result
  }

  def createImageBase64(pngImgUrl: String): String = db withSession { implicit session =>
      var result = ""
      try {  
           //   
             val post = new HttpGet(pngImgUrl)
             val client = HttpClientBuilder.create().build();
             // send the post request
             val response = client.execute(post)
          //
             //val client = new DefaultHttpClient()
             //val request = new HttpGet(pngImgUrl)
             //var response: HttpResponse = client.execute(request)
             val bis = new BufferedInputStream(response.getEntity.getContent)
             val bos = new ByteArrayOutputStream()
             val chunk = Array.ofDim[Byte](1024)
             var read: Int = 0
             do {
                read = bis.read(chunk)
                if (read >= 0) {
                    bos.write(chunk, 0, read)
                }
            } while (read >= 0);
            val encoder = new BASE64Encoder()
            result = encoder.encode(bos.toByteArray())
      } catch {
       case e: Exception =>
        e.printStackTrace()
      } 
    result
  }

  /* def createPNGImage(DoctorID: Long,imageName:String) = db withSession { implicit session =>
    try {
      val yourAWSCredentials = new org.jets3t.service.security.AWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY)
      val amazonS3Client = new org.jets3t.service.impl.rest.httpclient.RestS3Service(yourAWSCredentials)
      //val profileBucket = amazonS3Client.createBucket(AWS_DOCTOR_PROFILE_BUCKET)

      val s3Object = amazonS3Client.getObject(profileBucket.getName, DoctorID.toString() + "/"+imageName);
      log.info("DoctorID :" + DoctorID.toString() + " is found")
      val reader = new BufferedReader(new InputStreamReader(s3Object.getDataInputStream()));

      val imgString = Stream.continually(reader.readLine()).takeWhile(_ != null).mkString.toString()
      val str = imgString.subSequence(5, imgString.length - 1).toString()

      // Converting a Base64 String into Image byte array
      val imageByteArray: Array[Byte] = Base64.decodeBase64(str);

      //Write a image byte array into file system
      val imageOutFile: FileOutputStream = new FileOutputStream(DoctorID.toString() + ".png");
      imageOutFile.write(imageByteArray);
      imageOutFile.close();

      val profileImgPng: File = new File(DoctorID.toString() + ".png")
      val fileObject: S3Object = new S3Object(profileImgPng);

      val amazonResponse = amazonS3Client.putObject(profileBucket.getName + "/" + DoctorID.toString(), fileObject);

      val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
      cal.add(java.util.Calendar.MONTH, 6);

      val QueryString = DoctorS3ImageSchedularDao.createQueryString(cal)

      val signedDevPayUrl: String = AWS_HCUE_DOMAIN_PROTOCOL + "://" + AWS_DOC_PROFILE_CLOUD_FRONT_DOMAIN + "/" + DoctorID.toString() + "/" + DoctorID.toString() + ".png"

      DoctorS3ImageSchedularDao.updateSignedURL(DoctorID, Some(signedDevPayUrl.toString() + QueryString), cal)

      if (profileImgPng != null) {
        profileImgPng.delete()
      }

    } catch {
      case e: Exception =>
      // e.printStackTrace()
      //log.info("No Image found for Doctor " + result(resultObj).DoctorID)
      //logger.error("No Image found for Doctor " + result(resultObj).DoctorID)
    } finally {
      if (input != null)
                input.close()
    }
  }*/

}
