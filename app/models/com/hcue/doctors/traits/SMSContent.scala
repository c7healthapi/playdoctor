package models.com.hcue.doctors.traits

import models.com.hcue.doctor.Json.Request.Careteam.SmsRequestValidation.appointmentremindersmsObj
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.HospitalAppointment
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import scala.slick.driver.PostgresDriver.simple._
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalAddress
import java.util.Calendar
import java.text.SimpleDateFormat
import views.html.helper.input
import models.com.hcue.doctors.slick.transactTable.patient.PatientPhones

object SMSContent {

  val patients = Patients.patients
  val patientPhones = PatientPhones.patientsPhones
  val hospital = Hospital.Hospital
  val hospitalAppointment = HospitalAppointment.HospitalAppointment
  val hospitalAddress = HospitalAddress.HospitalAddress
  val doctor = Doctors.Doctors

  def sendappointmentconfirmation(a: appointmentremindersmsObj): String = db withSession { implicit session =>

    val appointmentDetails = hospitalAppointment.filter { x => x.AppointmentID === a.appointemntid.get }.map { x => (x.HospitalID, x.DoctorID, x.ScheduleDate, x.StartTime) }.firstOption.get

    val patientName = patients.filter { x => x.PatientID === a.patientid.get }.map { x => x.FirstName }.firstOption.getOrElse("")

    val hospitalDetails = hospital.filter { x => x.HospitalID === a.hospitalid.get }.map { x => (x.HospitalName) }.firstOption.getOrElse("")

    val hospitalPostCode = hospitalAddress.filter { x => x.HospitalID === a.hospitalid.get }.map { x => x.PostCode }.firstOption.getOrElse("")

    val messageContent = "Hi " + patientName + "," + "your ultrasound appointment is booked" + appointmentDetails._3 + " " + appointmentDetails._4 + " at " + hospitalDetails + " medical centre " + hospitalPostCode + "." + "If you would like to change or cancel this appointment, please call 01329552440 "

    val URL = SMSCredentials.sms_api_url + a.smsto.get + "&from=" + a.smsfrom.get + "&message=" + messageContent + "%20message%20from%20development%20server&ref=112233&maxsplit=5" //phil

    val result = scala.io.Source.fromURL(URL).mkString.replace("\\", "")

    val resultarr = result.split(':')

    resultarr(0)

  }

  def sendAppointmentReminder(input: Integer) = db withSession { implicit session =>

    val smsfrom = "07436545039"

    val formatDate = new SimpleDateFormat("dd-MM-yyyy")
    var dt = new java.util.Date();
    val c = Calendar.getInstance();
    c.setTime(dt);
    c.add(Calendar.DATE, input);

    val sqlDate = new java.sql.Date(c.getTimeInMillis());

    val hospitalAppDetail = hospitalAppointment.filter { x => x.ScheduleDate === sqlDate }.list.distinct

    hospitalAppDetail.foreach { y =>

      val appointmentDetails = hospitalAppointment.filter { x => x.AppointmentID === y.AppointmentID }.map { x => (x.HospitalID, x.DoctorID, x.ScheduleDate, x.StartTime) }.firstOption.get

      val patientName = patients.filter { x => x.PatientID === y.PatientID }.map { x => x.FirstName }.firstOption.getOrElse("")

      val smsto = patientPhones.filter { x => x.PatientID === y.PatientID }.map { x => (x.PhNumber) }.firstOption.getOrElse(0L)

      val hospitalDetails = hospital.filter { x => x.HospitalID === y.HospitalID }.map { x => (x.HospitalName) }.firstOption.getOrElse("")

      val hospitalPostCode = hospitalAddress.filter { x => x.HospitalID === y.HospitalID }.map { x => x.PostCode }.firstOption.getOrElse("")

      val messageContent = "Hi " + patientName + "," + "your ultrasound appointment is tomorrow at " + appointmentDetails._4 + " at " + hospitalDetails + " medical centre " + hospitalPostCode + "." + "If you would like to change or cancel this appointment, please call 01329552440 "

      if (smsto != 0L) {

        val URL = SMSCredentials.sms_api_url + smsto.toString() + "&from=" + smsfrom + "&message=" + messageContent + "%20message%20from%20development%20server&ref=112233&maxsplit=5" //phil

        val result = scala.io.Source.fromURL(URL).mkString.replace("\\", "")

        val resultarr = result.split(':')

        resultarr(0)

      }
    }

  }

  def sendDNASms(a: appointmentremindersmsObj): String = db withSession { implicit session =>

    val patientName = patients.filter { x => x.PatientID === a.patientid.get }.map { x => x.FirstName }.firstOption.getOrElse("")

    val messageContent = "Hi " + patientName + ", " + " You missed your ultrasound appointment today. Please call us on 01329552440 to reschedule."

    val URL = SMSCredentials.sms_api_url + a.smsto.get + "&from=" + a.smsfrom.get + "&message=" + messageContent + "%20message%20from%20development%20server&ref=112233&maxsplit=5" //phil

    val result = scala.io.Source.fromURL(URL).mkString.replace("\\", "")

    val resultarr = result.split(':')

    resultarr(0)

  }

}