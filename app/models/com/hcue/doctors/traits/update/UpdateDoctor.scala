package models.com.hcue.doctors.traits.update

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.Json.Request.AddUpdate.updateDoctorObj
import models.com.hcue.doctors.Json.Request.updateDtAppointmentDetailsObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.Json.Response.getResponseObj

trait UpdateDoctor {

  def updateDoctor(doctor: updateDoctorObj): Boolean

//  def updateAppointments(doctor: updateDtAppointmentDetailsObj): Option[TDoctorAppointment]

}

