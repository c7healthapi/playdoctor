package models.com.hcue.doctors.traits.update

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import scala.concurrent.Future
import models.com.hcue.doctors.helper.RequestResponseHelper.validateString
import scala.slick.driver.PostgresDriver.simple._
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorAddressDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorAddressExtnObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorCommunicationObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorEmailDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorEducationObj
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorPublicationAchievementObj
import models.com.hcue.doctors.Json.Request.AddUpdate.updateDoctorDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.updateDoctorObj
import models.com.hcue.doctors.Json.Request.S3.Amazon.doctorDocumentsObj
import models.com.hcue.doctors.Json.Request.S3.Amazon.fileDetailsObj
import models.com.hcue.doctors.Json.Request.updateDoctorPinPassObj
import models.com.hcue.doctors.Json.Request.updateDtAppointmentDetailsObj
import models.com.hcue.doctors.Json.Request.updateDtAppointmentStatusObj
import models.com.hcue.doctors.Json.Response.AddressUpdateSchdular.buildAddressResponseObj
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.buildAppointmentObj
import models.com.hcue.doctors.dataCasting.DataCasting
import models.com.hcue.doctors.helper.HCueUtils
import models.com.hcue.doctors.helper.RequestResponseHelper.CombineHashMap
import models.com.hcue.doctors.helper.hcueHelperClass
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddressPhone
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointmentExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointments
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorSpecialityCD
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.login.DoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.login.TDoctorLogin
import models.com.hcue.doctors.traits.Amazon.S3.Image.DoctorS3ImageSchedularDao
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.traits.delete.DeleteDoctorDao
//import models.com.hcue.doctors.traits.hcueProfileImgCloudFront
import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao
import play.Logger
import play.api.libs.Crypto
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import models.com.hcue.doctors.slick.lookupTable.CurrencyLkup
import models.com.hcue.doctors.slick.lookupTable.LocationLkup
import models.com.hcue.doctors.slick.lookupTable.StateLkup
import models.com.hcue.doctors.slick.lookupTable.Country
import models.com.hcue.doctors.slick.lookupTable.CityLkup
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.Json.Request.AddUpdate.updateDoctorLoginIDObj
import play.api.libs.Crypto
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorDepartmentsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.addDoctorObj
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.hospitals.THospital
import scala.util.control.Breaks.break
import scala.util.control.Breaks.breakable
import org.apache.velocity.VelocityContext
import models.com.hcue.doctors.Json.Request.AddUpdate.linkDoctorObj
//import models.com.hcue.doctors.Json.Request.AddUpdate.linkDoctorAddressDetailsObj

import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalAddress
import models.com.hcue.doctors.Json.Request.AddUpdate.hospitalInfoObj
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker
import scala.slick.jdbc.StaticQuery.interpolation
import models.com.hcue.doctor.slick.transactTable.Careteam.DoctorPracties
import models.com.hcue.doctors.Json.Request.AddUpdate.PractiesDetails
import models.com.hcue.doctor.slick.transactTable.doctor.ExtnDoctor
import models.com.hcue.doctors.Json.Request.AddUpdate.SpecialityDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctordeactiveObj
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.HospitalSchedule

object UpdateDoctorDao extends UpdateDoctor {

  val log = Logger.of("application")

  log.info("Created c3p0 connection pool")

  val doctors = Doctors.Doctors

  val doctorsExtn = DoctorsExtn.DoctorsExtn

  val doctorPhones = DoctorPhones.DoctorsPhones

  val doctorEmails = DoctorEmail.DoctorEmails

  val doctorAddress = DoctorAddress.DoctorsAddress

  val doctorCommunication = DoctorAddressPhone.DoctorAddressPhone

  val doctorAppointments = DoctorAppointments.DoctorAppointments

  val doctorsAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

  val doctorLogin = DoctorLogin.DoctorLogin

  val doctorSpecialityCD = DoctorSpecialityCD.DoctorSpecialityCD

  val doctorAppointmentExtn = DoctorAppointmentExtn.DoctorAppointmentExtn

  val hospital = Hospital.Hospital

  val countryLkup = Country.Country

  val currencyLkup = CurrencyLkup.CurrencyLkup

  val stateLkup = StateLkup.StateLkup

  val locationLkup = LocationLkup.LocationLkup

  val cityLkup = CityLkup.CityLkup

  val hospitalAddress = HospitalAddress.HospitalAddress

  val doctorpractiesdetails = DoctorPracties.DoctorPracties

  val extndoctor = ExtnDoctor.ExtnDoctor
  
  val hospitalschedule = HospitalSchedule.HospitalSchedule 

  def updateDoctor(a: updateDoctorObj): Boolean = db withSession { implicit session =>

    val upDateStatus: Boolean = false
    val doctorRecords: updateDoctorDetailsObj = a.doctorDetails

    val record = ListDoctorDao.getDoctor(doctorRecords.DoctorID)

    val extRecord = ListDoctorDao.getDoctorExt(doctorRecords.DoctorID)

    if (a.SpecialityDetails != None) {
      println("SpecialityDetails leangth "+a.SpecialityDetails.get.length)
      if(a.SpecialityDetails.get.length>0){
      val status = DeleteDoctorDao.deletedoctorSpecialityCD(doctorRecords.DoctorID)
      }
    }

    doctorRecords.SpecialityCD.foreach { x =>
      val value: Map[String, String] = x
      value.iterator.foreach { y =>

        var customSpeciality: Option[String] = None
        if (a.CustomSpecialityDesc != None) {
          val temp = a.CustomSpecialityDesc.get.get(y._2)
          if (temp != None) {
            customSpeciality = temp
          }
        }

        addSpecialityCode(doctorRecords.DoctorID, y._2, customSpeciality)
      }
    }

    if (record.nonEmpty) {
      val inst: DataCasting = new DataCasting()
      updateDoctorDetails(a, record)
      updateDoctorExtDetails(a, extRecord)

      if (doctorRecords.DoctorLoginID != None) {/*

        if (record.get.DoctorLoginID == None) {
          val loginObj: TDoctorLogin = new TDoctorLogin(doctorRecords.DoctorID, inst.stringconvertion(doctorRecords.DoctorLoginID), Crypto.encryptAES(inst.stringconvertion(doctorRecords.DoctorPassword)), "Y", null, None, "N", None, None)
          doctorLogin += loginObj
        } else {
          println("Doctor ID : "+doctorRecords.DoctorID)
          println("Doctor Login ID : "+inst.stringconvertion(doctorRecords.DoctorLoginID))
          println("Doctor Password  :"+doctorRecords.DoctorPassword)
         
          val loginObj: TDoctorLogin = new TDoctorLogin(doctorRecords.DoctorID, inst.stringconvertion(doctorRecords.DoctorLoginID), Crypto.encryptAES(inst.stringconvertion(doctorRecords.DoctorPassword)), "Y", None, None, "N", None, None)

          doctorLogin.filter(c => c.DoctorID === doctorRecords.DoctorID).update(loginObj)

        }

      */}

      //Update Child Table
      //Insert DoctorAddress
      a.DoctorAddress.foreach { x =>

        val doctorAddressDetails: Array[DoctorAddressDetailsObj] = x
        for (i <- 0 until doctorAddressDetails.length) {
          updateAddress(doctorAddressDetails(i), a)
        }
      }

      //Insert patientPhone
      a.DoctorPhone.foreach { x =>
        val doctorPhoneDetails: Array[DoctorPhoneDetailsObj] = x
        for (i <- 0 until doctorPhoneDetails.length) {
          updatePhoneNumber(doctorPhoneDetails(i), a)
        }
      }

      //Insert patientEmail
      a.DoctorEmail.foreach { x =>
        val doctorEmailDetails: Array[DoctorEmailDetailsObj] = x
        for (i <- 0 until doctorEmailDetails.length) {
          updateEmail(doctorEmailDetails(i), a)
        }
      }

      if (a.SpecialityDetails != None) {
        a.SpecialityDetails.foreach { x =>
          val doctorSpecialityDetails: Array[SpecialityDetailsObj] = x
          for (i <- 0 until doctorSpecialityDetails.length) {
            addSpeciality(doctorSpecialityDetails(i), a, doctorRecords.DoctorID)
          }
        }
      }

      //Insert Practiesdetails

      doctorpractiesdetails.filter { x => x.doctorid === a.doctorDetails.DoctorID }.map { x => (x.active, x.updtusr, x.updtusrtype) }.
        update(Some(false), Some(a.USRId), Some(a.USRType))

      a.PractiesDetails.foreach { x =>
        val practiesDetails: Array[PractiesDetails] = x
        for (i <- 0 until practiesDetails.length) {
          updatePractiesdetails(a, practiesDetails(i), a.doctorDetails.DoctorID)
        }
      }

      true

    } else upDateStatus

  }

  def addSpeciality(b: SpecialityDetailsObj, a: updateDoctorObj, autoGenDoctorID: Long) = db withSession { implicit session =>

    doctorSpecialityCD.map(c => (c.DoctorID, c.SpecialityCD, c.CustomSpecialityDesc, c.SubSpecialityID, c.ActiveIND)) +=
      (autoGenDoctorID, b.specialityID, b.specialityDesc, b.SubSpecialityID, b.ActiveIND)

  }

  def updatePractiesdetails(a: updateDoctorObj, b: PractiesDetails, DoctorID: Long) = db withSession { implicit session =>

    println("doctor id value " + DoctorID)

    //if (a != None) {

    
      println("come to doctor 1")

      var record = doctorpractiesdetails.filter { x => x.doctorid === DoctorID && x.practiceid === b.practiceid }.firstOption

      if (record != None) {

        println("come to if " + b.practiceid)

        println("come to if 2 " + DoctorID)

        doctorpractiesdetails.filter { x => x.doctorid === DoctorID && x.practiceid === b.practiceid }.map { x => (x.practicename, x.practicecode, x.postcode, x.ccgname, x.telephone, x.active, x.updtusr, x.updtusrtype) }.
          update(b.practicename, b.practicecode, b.postcode, b.ccgname, b.contactnumber, Some(true), Some(a.USRId), Some(a.USRType))

      } else {

        println("come to else 1" + b.practiceid)

        println("come to else 2 " + DoctorID)

        doctorpractiesdetails.map { x => (x.doctorid, x.practiceid, x.practicename, x.practicecode, x.postcode, x.ccgname, x.telephone, x.active, x.crtusr, x.crtusrtype) } += (DoctorID, b.practiceid, b.practicename, b.practicecode, b.postcode, b.ccgname, b.contactnumber, Some(b.active), a.USRId, a.USRType)

      }

   // }
  }

  def addSpecialityCode(autoGenDoctorID: Long, code: String, customSpeciality: Option[String]) = db withSession { implicit session =>

    //   val mappedValue: TDoctorSpecialityCD = new TDoctorSpecialityCD(autoGenDoctorID, code, None)

    doctorSpecialityCD.map(c => (c.DoctorID, c.SpecialityCD, c.CustomSpecialityDesc)) +=
      (autoGenDoctorID, code, customSpeciality)

  }

 
  def updateDoctorDetails(a: updateDoctorObj, record: Option[TDoctor]) = db withSession { implicit session =>

    val inst: DataCasting = new DataCasting()
    val doctorRecords: updateDoctorDetailsObj = a.doctorDetails

    val firstName: String = inst.getRequiredValue(doctorRecords.FirstName, record.get.FirstName)

    val lastName: Option[String] = inst.getRequiredValue(doctorRecords.LastName, record.get.LastName)

    val fullName: String = inst.getRequiredValue(doctorRecords.FirstName+" "+doctorRecords.LastName.getOrElse(""), record.get.FirstName +" "+record.get.LastName.getOrElse(""))

    val gender: Option[String] = inst.getRequiredValue(doctorRecords.Gender, record.get.Gender)

    val dob: Option[java.sql.Date] = inst.getRequiredDate(doctorRecords.DOB, record.get.DOB)

    // val age: Option[Int] = inst.getRequiredInt(doctorRecords.Age, record.get.Age)

    val exp: Option[Int] = inst.getRequiredInt(doctorRecords.Exp, record.get.Exp)

    val doctorLoginID: Option[String] = inst.getRequiredValue(doctorRecords.DoctorLoginID, record.get.DoctorLoginID)

    //val doctorPassword: Option[String] = inst.getRequiredValue(a.DoctorPassword, record.get.DoctorPassword)

    var doctorspeciality = doctorSpecialityCD.filter(c => c.DoctorID === doctorRecords.DoctorID).firstOption

    var specialityCD: Option[Map[String, String]] = Option(Map())

    if (!doctorspeciality.isEmpty) {
      specialityCD = inst.getRequiredHStoreValue(doctorRecords.SpecialityCD, record.get.SpecialityCD)
    }

    var serachable: Option[String] = inst.getRequiredValue(a.Searchable, record.get.Searchable)
    
   //Added default Searchable flag - N - Changed by Goutham
    var Searchable_Final: String = "N"
    if(serachable != None)
    {
         Searchable_Final = serachable.get
    }
    
    val nwPanNumber = if(a.doctorOtherDetails != None) { a.doctorOtherDetails.get.PanCardID } else { None }
    //val panCardId: Option[String] = inst.getRequiredValue(nwPanNumber, record.get.PanCardID)
    //val specialityCD: Option[Map[String, String]] = inst.getRequiredHStoreValue(doctorRecords.SpecialityCD, record.get.SpecialityCD)
     val faxno = if(a.doctorOtherDetails != None) { a.doctorOtherDetails.get.faxno } else { None }
     val gpcode = if(a.doctorOtherDetails != None) { a.doctorOtherDetails.get.gpcode } else { None }
     val typevalue = if(a.doctorOtherDetails != None) { a.doctorOtherDetails.get.typevalue } else { None }
     val notes = if(a.doctorOtherDetails != None) { a.doctorOtherDetails.get.notes } else { None }

    val mappedValue: TDoctor = new TDoctor(firstName, lastName, fullName, doctorRecords.DoctorID, gender, dob, doctorLoginID, specialityCD,
      exp, doctorRecords.TermsAccepted, record.get.SEOName, Some(Searchable_Final),doctorRecords.RegistrationNo,record.get.CrtUSR, record.get.CrtUSRType, Some(a.USRId), Some(a.USRType),faxno,gpcode,typevalue,notes,doctorRecords.Title)

    val result = doctors.filter(c => c.DoctorID === doctorRecords.DoctorID).update(mappedValue)
    
    if(a.isPracticeDoctor !=None){
    extndoctor.filter { x => x.DoctorID === doctorRecords.DoctorID }.map { x => x.PracticeDoctorActive}.update(a.isPracticeDoctor)
    }
    //Added doctor account details - included by Goutham
    if (a.AccountDetails != None) {
      var ExpiryDate : Option[java.sql.Date]  =  None
      if(a.AccountDetails.get.ExpiryDate != None)
      {
        ExpiryDate = a.AccountDetails.get.ExpiryDate
      }
    }

    if(a.doctorOtherDetails != None) {
      if(a.doctorOtherDetails.get.isDeleted.getOrElse("N") == "Y") {
          val loginInfo = doctorLogin.filter(c => c.DoctorID === doctorRecords.DoctorID).firstOption
          if(loginInfo != None) {
            val doctorid = doctorRecords.DoctorID
             (sql"select * from updatedoctorloginid('#$doctorid')".as[Int]).firstOption.get
          }
      }
    }

  }
  def updateDoctorExtDetails(a: updateDoctorObj, record: Option[TDoctorExtn]) = db withSession { implicit session =>

    val inst: DataCasting = new DataCasting()
    val doctorRecords: updateDoctorDetailsObj = a.doctorDetails

    val about: Option[Map[String, String]] = inst.getRequiredHStoreValue(doctorRecords.About, None) //record.get.About

    val hcueScore: Option[Float] = inst.getRequiredFloatValue(doctorRecords.HcueScore, None) //record.get.HcueScore

    val awards: Option[Map[String, String]] = inst.getRequiredHStoreValue(doctorRecords.Awards, None) //record.get.Awards

    // val membership: Option[Map[String, String]] = inst.getRequiredHStoreValue(doctorRecords.Membership,None)// record.get.Membership

    val services: Option[Map[String, String]] = inst.getRequiredHStoreValue(doctorRecords.Services, None) //record.get.Services

    //val practice: Option[Map[String, String]] = inst.getRequiredHStoreValue(doctorRecords.Practice, None)//record.get.Practice

    val memberID: Option[Map[String, String]] = inst.getRequiredHStoreValue(doctorRecords.MemberID, None) //record.get.MemberID

    val referredBy: Option[Long] = inst.getRequiredLong(doctorRecords.ReferredBy, None) //record.get.ReferredBy

    val prospect: Option[String] = inst.getRequiredValue(doctorRecords.Prospect, None) //record.get.Prospect

    val qualification: Option[Map[String, String]] = inst.getRequiredHStoreValue(doctorRecords.Qualification, None) //record.get.Qualification

    val careerImages: Option[Map[String, String]] = inst.getRequiredHStoreValue(a.CareerImages, None) //record.get.CareerImages

    val settings: Option[Map[String, String]] = inst.getRequiredHStoreValue(a.DoctorSettings, None) //record.get.DoctorSettings

    //var doctorSetting = collection.mutable.Map.empty[String, String]

    var curentDt: Option[java.sql.Date] = None //record.get.CareerImageExpDt
    var carrerDocuemnts: Option[JsValue] = None //record.get.CareerDocuments
    //Need to handle a.Latitude, a.Longitude
    if (a.CareerImages != None) {
      val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
      cal.add(java.util.Calendar.MONTH, 6);
      curentDt = Some(new java.sql.Date(cal.getTimeInMillis()))
      carrerDocuemnts = updateDoctorDocuments(doctorRecords.DoctorID, None, a.CareerImages.get)
    }

    if (a.DoctorSettings != None) {

      //var doctorSettings: Map[String, String] = a.DoctorSettings.get
      val key1: String = "RecepDiscountUpdate"
      val key2: String = "AppmntTypeMandatory" //Y

      val hospitalCode = doctorsAddressExtn.filter { x => x.DoctorID === doctorRecords.DoctorID }
        .map { x => x.HospitalCode.getOrElse("") }.firstOption

      val hospitalIDs = doctorsAddressExtn.filter { x => x.HospitalCode === hospitalCode }.map { x => x.HospitalID.getOrElse(0) }.list

      if (a.DoctorSettings.get.contains(key1)) {

        updateRecepDiscntUpdt(hospitalIDs, a.DoctorSettings.get(key1))
        //doctorSetting.put(key1, a.DoctorSettings.get(key1))

      }
      if (a.DoctorSettings.get.contains(key2)) {

        //doctorSetting.put(key2, a.DoctorSettings.get(key2))
        updateAppointMndtType(hospitalIDs, a.DoctorSettings.get(key2))

      }

    }
    val doctorExtValue: TDoctorExtn = new TDoctorExtn(doctorRecords.DoctorID, qualification, about, hcueScore,
      awards, None, services, memberID, prospect, referredBy, None, record.get.ProfilImageExpDt, careerImages, carrerDocuemnts, curentDt, Some(CombineHashMap(settings, None)),
      record.get.CrtUSR, record.get.CrtUSRType, Some(a.USRId), Some(a.USRType))

    val result = doctorsExtn.filter(c => c.DoctorID === doctorRecords.DoctorID).update(doctorExtValue)

  }

  def updateAppointMndtType(hospitalID: List[Long], flag: String) = db withSession { implicit request =>

    val doctorIDs = doctorsAddressExtn.filter { x => x.HospitalID inSet hospitalID }.map { x => x.DoctorID }.list.distinct
    val record = doctorsExtn.filter(c => c.DoctorID inSet doctorIDs).list
    for (i <- 0 until doctorIDs.length) {
      var Setting = collection.mutable.Map.empty[String, String]
      Setting.put("AppmntTypeMandatory", flag)
      val docsetting = record.filter { x => x.DoctorID == doctorIDs(i) }.map { x => x.DoctorSettings }.head
      val updt = doctorsExtn.filter(c => c.DoctorID === doctorIDs(i)).map { x => x.DoctorSettings }.update(Some(CombineHashMap(Some(Setting.toMap), docsetting)))

    }

  }
  
  
    def getshedulecout(a: DoctordeactiveObj): String = db withSession { implicit request =>
      
      var Status = "Success"
      var currentDate: Option[java.sql.Date] = None
      val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
      currentDate = Some(new java.sql.Date(cal.getTimeInMillis()))
      
      var record = hospitalschedule.filter { x => x.SonographerDoctorID === a.DoctorID && x.HospitalID === a.HospitalID && x.SonographerAddressID === a.AddressID && x.ScheduleDate >= currentDate}.map { x => x.AddressConsultID }.list
      
      if(a.RoleID.get == "CLINICALSUPPWORKER")
      {
         record = hospitalschedule.filter { x => x.SupportworkerDoctorID === a.DoctorID && x.HospitalID === a.HospitalID && x.SupportworkerAddressID === a.AddressID && x.ScheduleDate >= currentDate}.map { x => x.AddressConsultID }.list
      }
      
    
    if(record.length>0)
    {
      return "Failed"
    }else{
      return Status
    }
    
    
  }
    
    
   def deactivedoctoraddress(a: DoctordeactiveObj): Boolean = db withSession { implicit request =>
      
       val Status: Boolean = false
      var currentDate: Option[java.sql.Date] = None
      val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
      currentDate = Some(new java.sql.Date(cal.getTimeInMillis()))
      
    val record = doctorAddress.filter { x => x.DoctorID === a.DoctorID && x.AddressID=== a.AddressID }.map { x => x.AddressID }.list
    
    if(record.length>0)
    {
     doctorAddress.filter(x => x.DoctorID === a.DoctorID && x.AddressID=== a.AddressID ).map(x => x.Active).update("N")
       return true
    }else{
       return Status
    }
    
    
  }
    
    

  def updateRecepDiscntUpdt(hospitalID: List[Long], flag: String) = db withSession { implicit request =>

    val doctorIDs = doctorsAddressExtn.filter { x => x.HospitalID inSet hospitalID }.map { x => x.DoctorID }.list.distinct
    val record = doctorsExtn.filter(c => c.DoctorID inSet doctorIDs).list
    for (i <- 0 until doctorIDs.length) {
      var Setting = collection.mutable.Map.empty[String, String]
      Setting.put("RecepDiscountUpdate", flag)
      val docsetting = record.filter { x => x.DoctorID == doctorIDs(i) }.map { x => x.DoctorSettings }.head
      val updt = doctorsExtn.filter(c => c.DoctorID === doctorIDs(i)).map { x => x.DoctorSettings }.update(Some(CombineHashMap(Some(Setting.toMap), docsetting)))

    }

  }

  def updateAddress(a: DoctorAddressDetailsObj, b: updateDoctorObj) = db withSession { implicit session =>

    var parentHospitalId: Option[Long] = None

    if (a.ExtDetails != None) {

      val HospitalID = a.ExtDetails.get.HospitalID
      if (HospitalID != None) {
        parentHospitalId = hospital.filter { x => x.HospitalID === HospitalID }.map { x => x.ParentHospitalID.getOrElse(0L) }.firstOption
        doctorsAddressExtn.filter { x => x.HospitalID === HospitalID }.map { x => x.ParentHospitalID }.update(parentHospitalId)
      }
    }
    val doctorRecords: updateDoctorDetailsObj = b.doctorDetails
    val result: List[TDoctorAddress] = ListDoctorDao.getDoctorAddress(doctorRecords.DoctorID, a.AddressType, a.AddressID.getOrElse(0))
    val inst: DataCasting = new DataCasting()
    if (result.length > 0) {

      val doctorAddressphone = doctorCommunication.filter(c => c.DoctorID === doctorRecords.DoctorID).list

      for (i <- 0 until result.length) {

        if (validateString(a.Address2)) {

          val countryCode = a.Country.get
          val country = ListDoctorDao.getCounrty(countryCode)

          val StateCode = a.State
          val state = ListDoctorDao.getState(StateCode)

          val cityCode = a.CityTown
          val city = ListDoctorDao.getCity(cityCode)

          val location = a.Location.get
          val locationID = ListDoctorDao.getLocation(location)

          if (country.length == 0) {

            countryLkup.map(x => (x.CountryID, x.CountryDesc, x.ActiveIND, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (a.Country.get, a.Country.get, "Y", b.USRId, b.USRType, Some("Y"))
          }

          if (state.length == 0) {

            stateLkup.map(x => (x.StateID, x.StateDesc, x.ActiveIND, x.CountryID, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (a.State.get, a.State.get, "Y", a.Country.get, b.USRId, b.USRType, Some("Y"))
          }
          if (city.length == 0) {
            cityLkup.map(x => (x.CityID, x.CityIDDesc, x.ActiveIND, x.StateID, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (a.CityTown.get, a.CityTown.get, "Y", a.State.get, b.USRId, b.USRType, Some("Y"))
          }

          if (locationID.length == 0) {

            locationLkup.map(x => (x.LocationID, x.LocationIDDesc, x.CityID, x.ActiveIND, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (a.Location.get, a.Location.get, a.CityTown.get, "Y", b.USRId, b.USRType, Some("Y"))

          }
        }

        val ClinicName = inst.getRequiredValue(a.ClinicName, result(i).ClinicName)

        val addressId = inst.getRequiredLong(a.AddressID, Some(result(i).AddressID))

        val activeFlag: Option[String] = inst.getRequiredValue(a.Active, Some(result(i).Active))

        val address1: Option[String] = inst.getRequiredValue(a.Address1, result(i).Address1)

        val location: Option[String] = inst.getRequiredValue(a.Location, result(i).Location)

        val cityTown: Option[String] = inst.getRequiredValue(a.CityTown, result(i).CityTown)

        val country: Option[String] = inst.getRequiredValue(a.Country, result(i).Country)

        // val postCode: Option[String] = inst.getRequiredInt(a.PostCode, result(i).PostCode)
        var showTiming = a.ShowTimings
        if (result(i).ShowTimings != None && a.ShowTimings == None) {
          showTiming = result(i).ShowTimings
        }

        //Need to handle a.Latitude, a.Longitude
        val mappedValue: TDoctorAddress = new TDoctorAddress(doctorRecords.DoctorID, addressId.get, ClinicName, Some(CombineHashMap(a.ClinicSettings, result(i).ClinicSettings)), a.Address1, a.Address2, a.Street, a.Location, a.CityTown,
          a.DistrictRegion, a.State, a.PostCode, a.Country, a.AddressType, a.LandMark, activeFlag.get, result(0).CrtUSR, result(0).CrtUSRType, Some(b.USRId), Some(b.USRType), showTiming, a.Address4)

        doctorAddress.filter(c => c.DoctorID === doctorRecords.DoctorID && c.AddressType === a.AddressType && c.AddressID === a.AddressID).update(mappedValue)

        if (activeFlag.getOrElse("") == "N") {

          val activedoctor: List[TDoctorAddress] = doctorAddress.filter { x => x.DoctorID === doctorRecords.DoctorID && x.Active === "Y" }.list

          if (activedoctor.length == 0) {
            doctorLogin.filter(x => x.DoctorID === doctorRecords.DoctorID).map(x => x.Active).update("N")
          }

        }

        if (a.ExtDetails != None) {

          // val HospitalID = a.ExtDetails.get.HospitalID

          //Update Record in Address Extension
          val extentionTable: Option[TDoctorAddressExtn] = doctorsAddressExtn.filter(c => c.DoctorID === doctorRecords.DoctorID &&
            c.AddressID === result(i).AddressID).firstOption
          if (extentionTable != None) {
            val extValue: Option[DoctorAddressExtnObj] = a.ExtDetails

            val minpercase = inst.getRequiredInt(extValue.get.MinPerCase, extentionTable.get.MinPerCase)

            //val fees: Option[Int] = Some(extValue.get.Fees.getOrElse(extentionTable.get.Fees.getOrElse(0)))

            val latitude: Option[String] = inst.getRequiredValue(extValue.get.Latitude, extentionTable.get.Latitude)

            val longitude: Option[String] = inst.getRequiredValue(extValue.get.Longitude, extentionTable.get.Longitude)

            val clinicImages: Option[Map[String, String]] = inst.getRequiredHStoreValue(extValue.get.ClinicImages, extentionTable.get.ClinicImages)

            val roleID: Option[String] = inst.getRequiredValue(extValue.get.RoleID, extentionTable.get.RoleID)

            val groupID: Option[String] = inst.getRequiredValue(extValue.get.GroupID, extentionTable.get.GroupID)

            val ratePerhour = extValue.get.RateperHour

            val rateAboveTwenty = extValue.get.RateAboveTwenty

            val rateBelowTwenty = extValue.get.RateBelowTwenty

            /* var hospitalCode: Option[String] = extentionTable.get.HospitalCode
            //var branchCode: Option[String] = None
            if (extValue.get.HospitalInfo != None) {
              hospitalCode = inst.getRequiredValue(extValue.get.HospitalInfo.get.HospitalCode, extentionTable.get.HospitalCode)
              //branchCode = inst.getRequiredValue(extValue.get.HospitalInfo.get.BranchCode, HospitalInfo.get.BranchCode)
            }*/

            var expDate: Option[java.sql.Date] = None
            val clinicDoc: Option[JsValue] = if (extValue.get.ClinicImages != None) {
              val cal = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
              cal.add(java.util.Calendar.MONTH, 6);
              expDate = Some(new java.sql.Date(cal.getTimeInMillis()))
              updateDoctorDocuments(doctorRecords.DoctorID, Some(result(i).AddressID), extValue.get.ClinicImages.get)
            } else { None }

            doctorsAddressExtn.filter(c => c.DoctorID === doctorRecords.DoctorID &&
              c.AddressID === result(i).AddressID).map { x =>
              (x.DoctorID, x.PrimaryIND,
                x.Latitude, x.Longitude, x.ClinicImages, x.ClinicDocuments, x.ClinicImageExpDt, x.RoleID, x.GroupID, x.RatePerHour, x.RateAboveTwenty, x.RateBelowTwenty, x.MinPerCase, x.UpdtUSR, x.UpdtUSRType)
            }.update(doctorRecords.DoctorID, extentionTable.get.PrimaryIND, latitude, longitude, extValue.get.ClinicImages,
              clinicDoc, expDate, roleID, groupID, ratePerhour, rateAboveTwenty, rateBelowTwenty, Some(minpercase.getOrElse(15)), Some(b.USRId), Some(b.USRType))

            log.info(" DoctorCommunication : " + a.ExtDetails.get.AddressCommunication)
            if (a.ExtDetails.get.AddressCommunication != None && a.ExtDetails.get.AddressCommunication.get.length > 0) {
              val deleteData = doctorCommunication.filter(c => c.DoctorID === doctorRecords.DoctorID && c.AddressID === result(i).AddressID).delete

              val tempDoctorCommunication: Array[DoctorCommunicationObj] = a.ExtDetails.get.AddressCommunication.get

              for (j <- 0 until tempDoctorCommunication.length) {

                addDoctorAddressCommunication(doctorRecords.DoctorID, result(i).AddressID, tempDoctorCommunication(j), b.USRId, b.USRType)

              }
            }
          }
        } else {
          doctorsAddressExtn.map(c => (c.DoctorID, c.AddressID, c.PrimaryIND, c.CrtUSR, c.CrtUSRType)) += (doctorRecords.DoctorID, result(i).AddressID, "Y", b.USRId, b.USRType)
        }
      }
    } else {
      //Insert
      var initval: Option[Long] = None

      if (validateString(a.Address2)) {

        val countryCode = a.Country.get
        val country = ListDoctorDao.getCounrty(countryCode)

        val StateCode = a.State
        val state = ListDoctorDao.getState(StateCode)

        val cityCode = a.CityTown
        val city = ListDoctorDao.getCity(cityCode)

        val location = a.Location.get
        val locationID = ListDoctorDao.getLocation(location)

        if (country.length == 0) {

          countryLkup.map(x => (x.CountryID, x.CountryDesc, x.ActiveIND, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (a.Country.get, a.Country.get, "Y", b.USRId, b.USRType, Some("Y"))
        }

        if (state.length == 0) {

          stateLkup.map(x => (x.StateID, x.StateDesc, x.ActiveIND, x.CountryID, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (a.State.get, a.State.get, "Y", a.Country.get, b.USRId, b.USRType, Some("Y"))
        }
        if (city.length == 0) {
          cityLkup.map(x => (x.CityID, x.CityIDDesc, x.ActiveIND, x.StateID, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (a.CityTown.get, a.CityTown.get, "Y", a.State.get, b.USRId, b.USRType, Some("Y"))
        }

        if (locationID.length == 0) {

          locationLkup.map(x => (x.LocationID, x.LocationIDDesc, x.CityID, x.ActiveIND, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (a.Location.get, a.Location.get, a.CityTown.get, "Y", b.USRId, b.USRType, Some("Y"))

        }
      }

      val mappedValue: TDoctorAddress = new TDoctorAddress(doctorRecords.DoctorID, 0, a.ClinicName, a.ClinicSettings, a.Address1, a.Address2, a.Street, a.Location, a.CityTown,
        a.DistrictRegion, a.State, a.PostCode, a.Country, a.AddressType, a.LandMark, a.Active.getOrElse("Y"), b.USRId, b.USRType, null, null, a.ShowTimings, a.Address4)

      val autoGenAddressID = (doctorAddress returning doctorAddress.map(_.AddressID)) += mappedValue

      val extValue: Option[DoctorAddressExtnObj] = a.ExtDetails

      if (extValue != None) {
        var hospitalCode: Option[String] = None
        var branchCode: Option[String] = None
        if (extValue.get.HospitalInfo != None) {
          hospitalCode = extValue.get.HospitalInfo.get.HospitalCode
          branchCode = extValue.get.HospitalInfo.get.BranchCode
        }

        doctorsAddressExtn.map(c => (c.DoctorID, c.HospitalID, c.ParentHospitalID, c.HospitalCode, c.AddressID, c.PrimaryIND, c.Latitude, c.Longitude,
          c.RoleID, c.GroupID, c.RatePerHour, c.MinPerCase, c.CrtUSR, c.CrtUSRType)) +=
          (doctorRecords.DoctorID, extValue.get.HospitalID, parentHospitalId, hospitalCode, autoGenAddressID, extValue.get.PrimaryIND,
            extValue.get.Latitude, extValue.get.Longitude,
            extValue.get.RoleID, extValue.get.GroupID, extValue.get.RateperHour, extValue.get.MinPerCase, b.USRId, b.USRType)

        if (a.ExtDetails.get.AddressCommunication != None) {
          val doctorCommunication: Array[DoctorCommunicationObj] = a.ExtDetails.get.AddressCommunication.get
          for (j <- 0 until doctorCommunication.length) {
            addDoctorAddressCommunication(doctorRecords.DoctorID, autoGenAddressID, doctorCommunication(j), b.USRId, b.USRType)
          }
        }
      }
    }

  }

  def updatePhoneNumber(a: DoctorPhoneDetailsObj, b: updateDoctorObj) = db withSession { implicit session =>

    val doctorRecords: updateDoctorDetailsObj = b.doctorDetails
    val result: List[TDoctorPhones] = ListDoctorDao.getDoctorPhone(doctorRecords.DoctorID, a.PhType)

    if (result.length > 0) {
      for (i <- 0 until result.length) {

        val mappedValue: TDoctorPhones = new TDoctorPhones(doctorRecords.DoctorID, a.PhCntryCD, a.PhStateCD, a.PhAreaCD, a.PhNumber, a.PhType, a.PrimaryIND,
          result(0).CrtUSR, result(0).CrtUSRType, Some(b.USRId), Some(b.USRType), None)

        doctorPhones.filter(c => c.DoctorID === doctorRecords.DoctorID && c.PhType === a.PhType).update(mappedValue)

      }
    } else {
      //Insert
      doctorPhones.map(c => (c.DoctorID, c.PhCntryCD, c.PhStateCD, c.PhAreaCD, c.PhNumber, c.PhType, c.PrimaryIND, c.CrtUSR, c.CrtUSRType, c.UpdtUSR, c.UpdtUSRType)) +=
        (doctorRecords.DoctorID, a.PhCntryCD, a.PhStateCD, a.PhAreaCD, a.PhNumber, a.PhType, a.PrimaryIND, b.USRId, b.USRType, null, null)
    }
  }

  def addDoctorAddressCommunication(doctorID: Long, addressID: Long, a: DoctorCommunicationObj, usrID: Long, usrType: String) = db withSession { implicit session =>

    log.info("Adding New Phone")

    doctorCommunication.map(c => (c.DoctorID, c.AddressID, c.PhCntryCD, c.PhStateCD, c.PhAreaCD, c.PhNumber, c.PhType, c.RowID, c.PrimaryIND, c.PublicDisplay, c.CrtUSR, c.CrtUSRType)) +=
      (doctorID, addressID, a.PhCntryCD, a.PhStateCD, a.PhAreaCD, a.PhNumber, a.PhType, a.RowID, a.PrimaryIND, a.PublicDisplay, usrID, usrType)

  }

  def updateDoctorAddressCommuniation(doctorID: Long, addressID: Long, rowID: Int, a: DoctorCommunicationObj, usrID: Long, usrType: String) = db withSession { implicit session =>

    doctorCommunication.filter(x => x.DoctorID === doctorID && x.AddressID === addressID && x.RowID === rowID).
      map(_x => (_x.PhCntryCD, _x.PhStateCD, _x.PhAreaCD, _x.PhNumber, _x.PhType, _x.PrimaryIND, _x.PublicDisplay, _x.UpdtUSR, _x.UpdtUSRType))
      .update(a.PhCntryCD, a.PhStateCD, a.PhAreaCD, a.PhNumber, a.PhType, a.PrimaryIND, a.PublicDisplay, Some(usrID), Some(usrType))
  }

  def updateEmail(a: DoctorEmailDetailsObj, b: updateDoctorObj) = db withSession { implicit session =>

    val objDoctorEmail: Option[TDoctorEmail] = ListDoctorDao.getDoctorEmailID(b.doctorDetails.DoctorID, a.EmailIDType)

    if (objDoctorEmail != None) {

      var doctorEmailID = objDoctorEmail.get.EmailID

      if ((!doctorEmailID.equalsIgnoreCase(a.EmailID)) || objDoctorEmail.get.ValidIND.equalsIgnoreCase("N")) {

        var emailValidity = "N"

        val mappedValue: TDoctorEmail = new TDoctorEmail(b.doctorDetails.DoctorID, a.EmailID.toLowerCase(), a.EmailIDType, a.PrimaryIND, emailValidity,
          objDoctorEmail.get.CrtUSR, objDoctorEmail.get.CrtUSRType, Some(b.USRId), Some(b.USRType), None)

        doctorEmails.filter(c => c.DoctorID === b.doctorDetails.DoctorID && c.EmailIDType === a.EmailIDType).update(mappedValue)

      }
    } else {

      var emailValidity = "N"

     

      doctorEmails.map(c => (c.DoctorID, c.EmailID, c.EmailIDType, c.PrimaryIND, c.ValidIND, c.CrtUSR, c.CrtUSRType, c.UpdtUSR, c.UpdtUSRType)) +=
        (b.doctorDetails.DoctorID, a.EmailID, a.EmailIDType, a.PrimaryIND, emailValidity, b.USRId, b.USRType, null, null)

    }
  }


  import java.util.Calendar
  def updateAppointments(a: updateDtAppointmentDetailsObj): (String, Long) = db withSession { implicit session =>

    return ("APPOINTMENT_FAILURE", 0)

  }

  def geTokenNumber(startTime: String, endTime: String, duration: Int): Long = {

    val format: SimpleDateFormat = new SimpleDateFormat("HH:mm:ss");
    val cStartTime: Date = format.parse(startTime); //Doctor Consultation Start Time
    val cEndTime: Date = format.parse(endTime); //Patient Consultation Start Time
    val difference: Long = cEndTime.getTime() - cStartTime.getTime();

    val millis: Long = duration * 60 * 1000;

    return (difference / millis) + 1
  }

  def setTimeFormats(timeformat: String): String = {

    val ary = timeformat.split(":")
    var formatedData = timeformat

    if (ary(0).length() == 1) {
      formatedData = "0" + ary(0) + ":" + ary(1)
    }

    var times: String = formatedData.replace(":", "")
    if (times.length() < 6)
      times = times + "00"
    times = times.substring(0, 2) + ":" + times.substring(2, 4) + ":" + times.substring(4, 6)
    return times
  }

  def updateAppointmentStatus(a: updateDtAppointmentStatusObj): Option[buildAppointmentObj] = db withSession { implicit session =>

    val data = doctorAppointments.filter(c => c.AppointmentID === a.AppointmentID).firstOption
    val extnData = doctorAppointmentExtn.filter(c => c.AppointmentID === a.AppointmentID).firstOption

    if (data.nonEmpty) {

      /* val mappedValue: TDoctorAppointment = new TDoctorAppointment(data.get.AppointmentID, data.get.AddressConsultID, data.get.DayCD, data.get.ConsultationDt,
        data.get.StartTime, data.get.EndTime, data.get.PatientID, data.get.VisitUserTypeID, data.get.DoctorID, data.get.FirstTimeVisit, data.get.DoctorVisitRsnID,
        data.get.OtherVisitRsn, data.get.ActualFees, a.AppointmentStatus, data.get.TokenNumber, data.get.ParentAppointmentID,data.get.HospitalCode, a.USRId, a.USRType, null, null)

      doctorAppointments.filter(c => c.AppointmentID === a.AppointmentID).update(mappedValue)*/

      val otherVisitRsn: Option[String] = if (a.OtherVisitRsn != None) { a.OtherVisitRsn } else { data.get.OtherVisitRsn }

      doctorAppointments.filter { x => x.AppointmentID === a.AppointmentID }.map { x =>
        (x.AddressConsultID, x.DayCD, x.ConsultationDt, x.StartTime, x.EndTime, x.PatientID,
          x.VisitUserTypeID, x.DoctorID, x.FirstTimeVisit, x.DoctorVisitRsnID, x.OtherVisitRsn,
          x.AppointmentStatus, x.TokenNumber, x.ParentAppointmentID, x.HospitalCode, x.BranchCode, x.UpdtUSR, x.UpdtUSRType)
      }.update(data.get.AddressConsultID, data.get.DayCD, data.get.ConsultationDt,
        data.get.StartTime, data.get.EndTime, data.get.PatientID, data.get.VisitUserTypeID, data.get.DoctorID, data.get.FirstTimeVisit, data.get.DoctorVisitRsnID,
        otherVisitRsn, a.AppointmentStatus, data.get.TokenNumber,
        data.get.ParentAppointmentID, data.get.HospitalCode, data.get.BranchCode, Some(a.USRId), Some(a.USRType))

      var refferalId: Option[Long] = extnData.get.ReferralID
      var subRefferalId: Option[Long] = extnData.get.SubReferralID
      val visitRsnId = if (a.VisitRsnID != None) { a.VisitRsnID } else { extnData.get.VisitRsnID }
      val preliminaryNotes = if (a.PreliminaryNotes != None) { a.PreliminaryNotes } else { extnData.get.PreliminaryNotes }

      if (a.ReferralSourceDtls != None) {
        refferalId = Some(a.ReferralSourceDtls.get.ReferralID)
        subRefferalId = a.ReferralSourceDtls.get.SubReferralID
      }

      /*     var checkInTime: Option[java.sql.Timestamp] = extnData.get.CheckInTime
      if(a.AppointmentStatus == "I") {
       checkInTime =  Some(new java.sql.Timestamp(CalenderDateConvertor.getISDDate().getTime()))
      }*/

      doctorAppointmentExtn.filter(x => x.AppointmentID === a.AppointmentID).map { x =>
        (x.Reason, x.VisitRsnID, x.PreliminaryNotes,
          x.ReferralID, x.SubReferralID)
      }.update(a.Reason, visitRsnId, preliminaryNotes, refferalId, subRefferalId)

      return Some(new buildAppointmentObj(data.get.AppointmentID, data.get.AddressConsultID, data.get.DayCD, data.get.ConsultationDt, data.get.StartTime, data.get.EndTime,
        data.get.PatientID, data.get.VisitUserTypeID, data.get.FirstTimeVisit, data.get.DoctorVisitRsnID,
        data.get.AppointmentStatus, data.get.TokenNumber, None))

      // doctorAppointments.filter(c => c.AppointmentID === a.AppointmentID).firstOption

    } else {
      log.info("updateAppointments Record Action failed")
      None
    }

  }

  def stringconvertion(value: Option[String]): String = db withSession { implicit session =>
    val valueLength: Int = value.toString().length()
    val valueName: String = value.toString()

    return valueName.substring(5, valueLength - 1)

  }

  def getDoctorAddressOption(DoctorID: Long, AddressType: String, AddressID: Option[Long]): List[TDoctorAddress] = db withSession { implicit session =>
    doctorAddress.filter(c => c.DoctorID === DoctorID && c.AddressType === AddressType && c.AddressID === AddressID).list
  }

  def updateDoctorPinPass(a: updateDoctorPinPassObj): String = db withSession { implicit session =>

    val doctorRecord: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorLoginID === a.DoctorLoginID.toLowerCase()).firstOption

    if (doctorRecord.nonEmpty) {

      val mappedValue: TDoctorLogin = new TDoctorLogin(doctorRecord.get.DoctorID, doctorRecord.get.DoctorLoginID,
        doctorRecord.get.DoctorPassword, "Y", Some(a.PinPassCode), None, doctorRecord.get.LoggedIn, None, doctorRecord.get.LoginCount)

      val result = doctorLogin.filter(c => c.DoctorLoginID === a.DoctorLoginID.toLowerCase()).update(mappedValue)

      "Success"
    } else "failure"
  }

  import models.com.hcue.doctors.Json.Response.AddressUpdateSchdular.buildAddressResponseObj
  import java.util.Calendar

  //val AWS_DOC_PROFILE_CLOUD_FRONT_DOMAI = hcueProfileImgCloudFront.HCUE_DOC_PROFILE_CLOUD_FRONT_DOMAIN
  
  val AWS_DOC_PROFILE_CLOUD_FRONT_DOMAI = ""

  def updateDoctorDocuments(DoctorID: Long, AddressID: Option[Long], a: Map[String, String]): Option[JsValue] = db withSession { implicit session =>

    var documents: Option[JsValue] = None
    val imageList = a.toList

    if (imageList.length > 0) {

      var filePath: String = if (AddressID != None) {
        DoctorID + "/Clinic/" + AddressID.get.toString()
      } else { DoctorID + "/Career" }

      var records: Array[fileDetailsObj] = new Array[fileDetailsObj](imageList.length)
      for (i <- 0 until imageList.length) {
        //val imageURL = DoctorS3ImageSchedularDao.createImageUrl(filePath, imageList(i)._2, ".png", AWS_DOC_PROFILE_CLOUD_FRONT_DOMAI)
        val imageURL = ""
        records(i) = new fileDetailsObj(Some(imageURL), imageList(i)._2, ".png", Some(imageList(i)._2), None, None)
      }

      val tempObj: doctorDocumentsObj = new doctorDocumentsObj(DoctorID, AddressID, records)
      if (tempObj.Documents.length > 0) {
        val jsonFormat = (Json.arr(tempObj.Documents))(0)
        documents = Some(jsonFormat)
      }
    }
    documents
  }

  import org.json.JSONObject;

  import org.json.JSONArray;

  def updateDoctorDocumentsSignedURL(DoctorID: Long, AddressID: Option[Long], a: JsValue, signedUrl: String, QueryString: String): Option[JsValue] = db withSession { implicit session =>

    var filePath: String = if (AddressID != None) {
      DoctorID + "/Clinic/" + AddressID.get.toString()
    } else { DoctorID + "/Career" }

    var documentJson: Option[JsValue] = None

    val caseDoc = a.toString() // Handle Null Pointer Exception

    val caseDocArry: JSONArray = new JSONArray(caseDoc);
    val fullcaseJson = new Array[Option[JsValue]](caseDocArry.length())

    for (j <- 0 until caseDocArry.length()) {

      val jsonObj: JSONObject = new JSONObject(caseDocArry.get(j).toString())

      if (jsonObj.keySet().contains("FileURL")) {

        val str = jsonObj.get("FileURL").toString()

        val url = str.substring(0, str.indexOf("?"))

        jsonObj.put("FileURL", url + QueryString)

        documentJson = Some(Json.parse(jsonObj.toString()))
        fullcaseJson(j) = documentJson
      }
    }

    return Some(Json.toJson(fullcaseJson))

  }

  def ValidCheckDoctorID(a: updateDoctorLoginIDObj): Boolean = db withSession { implicit session =>
    val checkDoctorIDExist = doctorLogin.filter { x => x.DoctorID === a.DoctorID }.list
    if (checkDoctorIDExist.length > 0) {
      return true
    } else {
      return false
    }
  }

  def checkExistingLoginID(doctorLoginID: String): Boolean = db withSession { implicit session =>

    var existcount: Option[TDoctorLogin] = doctorLogin.filter { x => x.DoctorLoginID === doctorLoginID.trim().toLowerCase() }.firstOption

    if (existcount != None) {
      return true
    } else {
      return false
    }
  }

  def checkExistingEmailID(emailID: String): Boolean = db withSession { implicit session =>

    println("checkExistingEmailID method")
    var existcount: Option[TDoctorEmail] = doctorEmails.filter { x => x.EmailID === emailID.trim().toLowerCase() }.firstOption

    if (existcount != None) {
      return true
    } else {
      return false
    }
  }

  def updateDoctorLoginID(upddocLogID: updateDoctorLoginIDObj): String = db withSession { implicit session =>
    doctorLogin.filter(c => c.DoctorID === upddocLogID.DoctorID).map(x => (x.DoctorLoginID, x.DoctorPassword)).update(upddocLogID.DoctorLoginID.trim().toLowerCase(), Crypto.encryptAES(upddocLogID.DoctorPassword))
    doctors.filter(x => x.DoctorID === upddocLogID.DoctorID).map(x => x.DoctorLoginID).update(Some(upddocLogID.DoctorLoginID.trim().toLowerCase()))
    return "Success"
  }

  def updateDoctorLastLogin(doctorId: Long, timeFlag: Option[String]): String = db withSession { implicit session =>

    if (timeFlag != None && timeFlag.get == "Y") {
      doctorLogin.filter(c => c.DoctorID === doctorId).map(x => (x.LoggedIn, x.LoginCount)).update("Y", Some(1))
    } else {
      doctorLogin.filter(c => c.DoctorID === doctorId).map(x => (x.LoggedIn, x.LoginCount)).update("N", Some(0))
    }

    return "Success"
  }

  def validateDocLicense(a: updateDoctorObj): String = db withSession { implicit session =>

    var licValidationFlag = false

    var licValidationStatus = "Success"

    if (a.DoctorAddress != None ) {

      val addressDtl = a.DoctorAddress.get
      
      if(addressDtl.length != 0){

      if (addressDtl(0).ExtDetails != None) {

        val hospitalId = addressDtl(0).ExtDetails.get.HospitalID

        val roleID = addressDtl(0).ExtDetails.get.RoleID.getOrElse("")

        if (hospitalId != None) {

          val hosInfo: Option[THospital] = hospital.filter { x => x.HospitalID === hospitalId }.firstOption

          if (hosInfo != None) {

            val parentHospitalInfo: Option[THospital] = hospital.filter { x => x.HospitalID === hosInfo.get.ParentHospitalID }.firstOption

            val hosinfoList = hospital.filter { x => x.ParentHospitalID === parentHospitalInfo.get.HospitalID }.map { x => x.HospitalID }.list

            var queryForDoctorAddressInfo = for {
              (doctorAdd, doctorAddExtn) <- doctorAddress innerJoin doctorsAddressExtn on (_.DoctorID === _.DoctorID)
              if doctorAddExtn.AddressID === doctorAdd.AddressID && doctorAddExtn.ParentHospitalID === hosInfo.get.ParentHospitalID
            } yield (doctorAdd, doctorAddExtn)

            //var hospitalDoctorInfo: List[(TDoctorAddress, TDoctorAddressExtn)] = List()
            var hospitalDoctorInfo: List[Long] = List()
            var hospitalRecepInfo: List[Long] = List()

            hospitalDoctorInfo = queryForDoctorAddressInfo.filter { x => x._1.Active === "Y" && x._2.ParentHospitalID.getOrElse(0L) === parentHospitalInfo.get.HospitalID && (x._2.RoleID === "DOCTOR" || x._2.RoleID === "ADMIN") }.map { x => x._2.DoctorID }.list.distinct

            hospitalRecepInfo = queryForDoctorAddressInfo.filter { x => x._1.Active === "Y" && x._2.HospitalID === hosInfo.get.HospitalID && x._2.RoleID === "RECPTNIST" }.map(x => x._2.DoctorID).list

            var usrCount: Int = hospitalDoctorInfo.length + 1

            var recepCount: Int = hospitalRecepInfo.length

            val pref: Option[Map[String, String]] = parentHospitalInfo.get.Preference

            if (pref != None) {

              if (roleID == "DOCTOR") {

                if (pref.get.contains("MaxDoc")) {
                  var count = pref.get("MaxDoc")
                  if (count != null && usrCount <= count.toInt) {
                    licValidationFlag = true

                  } else if (hospitalDoctorInfo.contains(a.doctorDetails.DoctorID)) {
                    licValidationFlag = true
                  } else {
                    licValidationFlag = false
                    licValidationStatus = "DoctorLimit"
                  }
                } else {
                  licValidationFlag = true
                }
              } else if (roleID == "RECPTNIST") {

                if (pref.get.contains("MaxRecep")) {
                  var count = pref.get("MaxRecep")

                  if (count != null && recepCount < count.toInt) {
                    licValidationFlag = true
                  } else if (hospitalRecepInfo.contains(a.doctorDetails.DoctorID)) {
                    licValidationFlag = true
                  } else {
                    licValidationFlag = false
                    licValidationStatus = "RecpLimit"
                  }
                } else {
                  licValidationFlag = true
                }
              } else {
                licValidationFlag = true
              }

              if (!licValidationFlag && licValidationStatus == "DoctorLimit") {
                return HCueUtils.StrConstDoctorLimitExceed
              } else if (!licValidationFlag && licValidationStatus == "RecpLimit") {
                return HCueUtils.StrConstRecLimitExceed
              } else if (!licValidationFlag) {
                return HCueUtils.StrConstLicLimitExceed
              }
            }
          }
        }
      }
    }}
    return HCueUtils.StrConstSuccess
  }
  
  
  
  
  def validateDocLicensecheck(a: DoctordeactiveObj): String = db withSession { implicit session =>

    var licValidationFlag = false

    var licValidationStatus = "Success"


        val hospitalId = a.HospitalID

        val roleID = a.RoleID.getOrElse("")

        if (hospitalId != None) {

          val hosInfo: Option[THospital] = hospital.filter { x => x.HospitalID === hospitalId }.firstOption

          if (hosInfo != None) {

            val parentHospitalInfo: Option[THospital] = hospital.filter { x => x.HospitalID === hosInfo.get.ParentHospitalID }.firstOption

            val hosinfoList = hospital.filter { x => x.ParentHospitalID === parentHospitalInfo.get.HospitalID }.map { x => x.HospitalID }.list

            var queryForDoctorAddressInfo = for {
              (doctorAdd, doctorAddExtn) <- doctorAddress innerJoin doctorsAddressExtn on (_.DoctorID === _.DoctorID)
              if doctorAddExtn.AddressID === doctorAdd.AddressID && doctorAddExtn.ParentHospitalID === hosInfo.get.ParentHospitalID
            } yield (doctorAdd, doctorAddExtn)

            //var hospitalDoctorInfo: List[(TDoctorAddress, TDoctorAddressExtn)] = List()
            var hospitalDoctorInfo: List[Long] = List()
            var hospitalRecepInfo: List[Long] = List()

            hospitalDoctorInfo = queryForDoctorAddressInfo.filter { x => x._1.Active === "Y" && x._2.ParentHospitalID.getOrElse(0L) === parentHospitalInfo.get.HospitalID && (x._2.RoleID === "DOCTOR" || x._2.RoleID === "ADMIN") }.map { x => x._2.DoctorID }.list.distinct

            hospitalRecepInfo = queryForDoctorAddressInfo.filter { x => x._1.Active === "Y" && x._2.HospitalID === hosInfo.get.HospitalID && x._2.RoleID === "RECPTNIST" }.map(x => x._2.DoctorID).list

            var usrCount: Int = hospitalDoctorInfo.length + 1

            var recepCount: Int = hospitalRecepInfo.length

            val pref: Option[Map[String, String]] = parentHospitalInfo.get.Preference

            if (pref != None) {

              if (roleID == "DOCTOR") {

                if (pref.get.contains("MaxDoc")) {
                  var count = pref.get("MaxDoc")
                  if (count != null && usrCount <= count.toInt) {
                    licValidationFlag = true

                  } else if (hospitalDoctorInfo.contains(a.DoctorID)) {
                    licValidationFlag = true
                  } else {
                    licValidationFlag = false
                    licValidationStatus = "DoctorLimit"
                  }
                } else {
                  licValidationFlag = true
                }
              } else if (roleID == "RECPTNIST") {

                if (pref.get.contains("MaxRecep")) {
                  var count = pref.get("MaxRecep")

                  if (count != null && recepCount < count.toInt) {
                    licValidationFlag = true
                  } else if (hospitalRecepInfo.contains(a.DoctorID)) {
                    licValidationFlag = true
                  } else {
                    licValidationFlag = false
                    licValidationStatus = "RecpLimit"
                  }
                } else {
                  licValidationFlag = true
                }
              } else {
                licValidationFlag = true
              }

              if (!licValidationFlag && licValidationStatus == "DoctorLimit") {
                return HCueUtils.StrConstDoctorLimitExceed
              } else if (!licValidationFlag && licValidationStatus == "RecpLimit") {
                return HCueUtils.StrConstRecLimitExceed
              } else if (!licValidationFlag) {
                return HCueUtils.StrConstLicLimitExceed
              }
            }
          }
        }
      
    
    return HCueUtils.StrConstSuccess
  }


}