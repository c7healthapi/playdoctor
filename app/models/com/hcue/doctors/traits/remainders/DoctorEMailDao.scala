package models.com.hcue.doctors.traits.remainders

import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB
import org.postgresql.util.PSQLException
import play.Logger
import models.com.hcue.doctors.dataCasting.DataCasting
import play.api.libs.Crypto
import org.apache.velocity.VelocityContext
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.doctor.login.TDoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.login.DoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.helper.hcueCommunication
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.Json.Request.forgotPasswordRequest
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import java.text.SimpleDateFormat
import models.com.hcue.doctors.Json.Request.updatepasswordObj
import play.api.libs.Crypto
import play.api.Play.current

object DoctorEMailDao {

  private val log = Logger.of("application")

  val doctors = Doctors.Doctors
  val doctorLogin = DoctorLogin.DoctorLogin
  val doctorEmail = DoctorEmail.DoctorEmails
  val doctorsAddress = DoctorAddress.DoctorsAddress
  val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn
  
  
  def getHTML(emailtype: String) = sql"select defaulthtml from emailtemplate where emailtype = '#$emailtype'".as[(String)]

  def getDoctorPassword(doctorLoginID: String,preferedEmailID:Option[String]): String = db withSession { implicit session =>

    val doctorRecord: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorLoginID === doctorLoginID.toLowerCase()).firstOption

    if (doctorRecord != None) {

      val userID = doctorLoginID.toLowerCase()

      val password = Crypto.decryptAES(doctorRecord.get.DoctorPassword)
      
      if (preferedEmailID != None && preferedEmailID.get.equals("noreply@hcue.co")) {
      
      return password
       
      } 

      val doctorDetails = doctors.filter { x => x.DoctorID === doctorRecord.get.DoctorID }.firstOption
      
      val CreatedDetails = doctors.filter { x => x.DoctorID === doctorDetails.get.CrtUSR}.firstOption


      val doctorEmailDetails = doctorEmail.filter { x => x.DoctorID === doctorRecord.get.DoctorID }.firstOption

      val doctorAddressDetails = doctorsAddress.filter { x => x.DoctorID === doctorRecord.get.DoctorID }.firstOption
      
      val doctorAddressExtnDetails = doctorAddressExtn.filter { x => x.DoctorID === doctorRecord.get.DoctorID }.firstOption

      val fullName = doctorDetails.get.FullName
                
      val context = new VelocityContext();
        
      val currentDate: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
      val currentDateFormat = CalenderDateConvertor.getDateFormatter(currentDate)
      val clinicName = doctorAddressDetails.get.ClinicName
      
      var speciality = "N"

      doctorDetails.get.SpecialityCD.foreach { x =>
        val value: Map[String, String] = x
        value.iterator.foreach { y =>

          if (y._2.equals("DNT")) {

             speciality = "Y"
            
          }

        }
      }
     
      
      var hospitalCd = ""
      if(doctorAddressExtnDetails != None) hospitalCd = doctorAddressExtnDetails.get.HospitalCode.getOrElse("")
     
    
      
      
   /*   if(hcueHospitalCode.HCUE_HOSPITAL_JUSTDENTAL_CODE == hospitalCd) {
         websiteUrl = hcueCustomisedEmailCredentials.JUSTDENTAL_WEBSITE_URL
         logoUrl = hcueCustomisedEmailCredentials.JUSTDENTAL_LOGO_URL
         doctorWebUrl = hcueCustomisedEmailCredentials.JUSTDENTAL_DOCTOR_WEB_URL
         productName = hcueCustomisedEmailCredentials.JUSTDENTAL_PRODUCT_NAME
      }*/
      
      context.put("userID", userID)
      context.put("password", password)
      context.put("fullName", fullName)
      context.put("currentDate", currentDateFormat.format(currentDate))
      context.put("clinicName",clinicName.getOrElse(""));
      context.put("speciality",speciality);
      if(CreatedDetails != None){
        context.put("createdBy", CreatedDetails.get.FullName);
      }else {
        context.put("createdBy","Admin");
      }
      
 
      
     /* val html = hcueCommunication.getHcueHTMLTempalte(context, hcueBuckets.HCUE_EMAIL_TEMPLATE_BUCKET, hcueTemplateLocation.doctorForgotPassword)

      if (hospitalCd == hcueHospitalCode.HCUE_HOSPITAL_JUSTDENTAL_CODE) {
        val status = hcueCommunication.sendHcueEmail(html, if(preferedEmailID == None){ doctorEmailDetails.get.EmailID  }else{preferedEmailID.get},None,None, "Dr " + fullName + " " + productName + " Login Credential", "Y", "Y", Some(hospitalCd))
      } else {
        val status = hcueCommunication.sendHcueEmail(html, if(preferedEmailID == None){ doctorEmailDetails.get.EmailID  }else{preferedEmailID.get},None,None, "Dr " + fullName + " " + productName + " Login Credential", "Y", "Y")
      }*/

      return "Success"

    } else {

      return "Failure"

    }

  }
  
    def forgotpassword(a: forgotPasswordRequest):String = db withSession { implicit session =>

    val UserLoginDetails = doctorLogin.filter { x => x.DoctorLoginID === a.DoctorLoginID }.firstOption
    var returnstr="Success"

    if (UserLoginDetails != None) {

      val userid = UserLoginDetails.get.DoctorID

      val userdetails = doctors.filter { x => x.DoctorID === userid}.map { x => (x.Title,x.FullName) }.firstOption

      val title = userdetails.get._1
      val username = userdetails.get._2

      var html = getHTML("FORGOTPASSWORD").first
      
      val forgoturl = current.configuration.getString("forgot_password_url").get + userid

      var emailer = html.replaceAllLiterally("{{title}}", title.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{name}}", username)
      emailer = emailer.replaceAllLiterally("{{link}}", forgoturl)

      val futureString: Future[String] = scala.concurrent.Future {
   
        println("login mail id  "+a.DoctorLoginID);
        returnstr=hcueCommunication.sendHcueEmail(emailer, a.DoctorLoginID, None, None, "ForgotPassword", "Y", "Y",None)
        println("returnstr value  "+returnstr);
        //returnstr="Success"
         returnstr
        
      }
       returnstr
    }else{
      returnstr="Invalid LoginID"
     returnstr
    }
   returnstr
  }
    
    
    def updatepassword(a: updatepasswordObj): String = db withSession { implicit session =>

    if (a.doctorid!= None) {
         
      val updateRecord = doctorLogin.filter(c => c.DoctorID === a.doctorid).map(_x => _x.DoctorPassword).update(Crypto.encryptAES(a.newpassword.get))
          
      "PASSWORD UPDATED"
    } else {
      val updatepwd = doctorLogin.filter { x => x.DoctorLoginID.toLowerCase === a.doctorloginid.get.toLowerCase() && x.DoctorPassword === Crypto.encryptAES(a.password.get) }.firstOption
      if (updatepwd.nonEmpty) {
        val updateRecord = doctorLogin.filter(c => c.DoctorLoginID.toLowerCase === a.doctorloginid.get.toLowerCase()).map(_x => _x.DoctorPassword).update(Crypto.encryptAES(a.newpassword.get))
        "PASSWORD UPDATED"
      } else {
        "INVALID_PASSWORD"
      }

    }

  }


    
   

}