package models.com.hcue.doctors.traits

import scala.slick.driver.PostgresDriver.simple.Database
import com.mchange.v2.c3p0.ComboPooledDataSource
import play.Logger
import play.api.Play.current

object dbProperties {

  private val log = Logger.of("application")

  //val Url = dataBaseCredentials.URL

  val Url = current.configuration.getString("db.default.url")
  val Driver = current.configuration.getString("db.default.driver")
  val user = current.configuration.getString("db.default.user")
  val password = current.configuration.getString("db.default.password")
  val minpoolsize = current.configuration.getInt("db.default.minpoolsize")
  val maxpoolsize = current.configuration.getInt("db.default.maxpoolsize")
  val maxconnectionage = current.configuration.getInt("db.default.maxconnectionage")
  val acquireincrement = current.configuration.getInt("db.default.acquireincrement")

  val database = Database.forURL(Url.get, driver = Driver.get)

  private[this] lazy val instance = {
    log.info("Log Mode Info")
    log.debug("Log Mode debug")
    log.error("Log Mode Error")
    val ds = new ComboPooledDataSource
    ds.setDriverClass(Driver.get)
    ds.setJdbcUrl(Url.get)
    ds.setUser(user.get)
    ds.setPassword(password.get)
    ds.setMinPoolSize(minpoolsize.get)
    ds.setMaxConnectionAge(maxconnectionage.get)
    ds.setAcquireIncrement(acquireincrement.get)
    ds.setMaxPoolSize(maxpoolsize.get)
    //ds.setMaxIdleTime(dataBaseCredentials.MaxIdleTime)
    Database.forDataSource(ds)
  }
  
  def db(): slick.driver.PostgresDriver.backend.DatabaseDef = instance
}