package models.com.hcue.doctors.traits.mobileSMS.Communication.pustWoosh


import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import com.google.gson.Gson
import models.com.hcue.doctors.Json.Response.notificationMsgObj
import models.com.hcue.doctors.Json.Response.requestMsgObj
import models.com.hcue.doctors.Json.Response.requestMsg
//import models.com.hcue.doctors.traits.pushWooshCredentials
import org.apache.http.impl.client.HttpClientBuilder
import play.Logger
import java.io.InputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.IOException
import org.apache.http.util.EntityUtils
import org.json.JSONObject
import models.com.hcue.doctors.Json.Response.deleteRequestMsg
import models.com.hcue.doctors.Json.Response.deleteRequestMsgObj

object PushWooshCommunicationDao {

  private val log = Logger.of("application")
/*  
  val PUSHWOOSH_SERVICE_BASE_URL = pushWooshCredentials.PUSHWOOSH_SERVICE_BASE_URL;
  val AUTH_TOKEN = pushWooshCredentials.AUTH_TOKEN
  val APPLICATION_CODE = pushWooshCredentials.APPLICATION_CODE
  val method = pushWooshCredentials.CREATEMETHOD
  val deleteMethod = pushWooshCredentials.DELETEMETHOD*/
  
  val PUSHWOOSH_SERVICE_BASE_URL ="";
  val AUTH_TOKEN = ""
  val APPLICATION_CODE = ""
  val method = ""
  val deleteMethod = ""
  
  //Doctor Lite      
  def sendPushWooshNotification(android_header:String,contentMsg: String,devices:Array[String],time: Option[String],android_custom_icon:Option[String]): String = { 
    
    var resultMessage = ""
    try {
      
      var platforms:Array[Int] = new Array[Int](1)
      platforms(0) = 3
   
    val notificationTime = time.getOrElse("now") 
    val notifyMsg: Array[notificationMsgObj] = new Array[notificationMsgObj](1)
    
    notifyMsg(0) = notificationMsgObj(android_header,"beep",notificationTime, contentMsg,devices,android_custom_icon) // YYYY-MM-DD HH:mm or "now"
    
     //notifyMsg(0) = notificationMsgObj(android_header,"beep",notificationTime, contentMsg,devices,"http://dowxwb5wphdif.cloudfront.net/testing/Images/smallhcuelogoicon.png") // YYYY-MM-DD HH:mm or "now"
    
    val reqiestMsgobj = requestMsgObj(APPLICATION_CODE, AUTH_TOKEN, notifyMsg)
    val reqiestMsg = requestMsg(reqiestMsgobj)  

    val jsonString = new Gson().toJson(reqiestMsg)

    val httpClient: HttpClient = HttpClientBuilder.create().build();
    
    val request: HttpPost = new HttpPost(PUSHWOOSH_SERVICE_BASE_URL + method);
    val params: StringEntity = new StringEntity(jsonString)
    
    request.addHeader("content-type", "application/json")
    request.setEntity(params)
    
    val response: HttpResponse = httpClient.execute(request)
    val entity = response.getEntity();
 
    if (entity != null) {
     val responseString = EntityUtils.toString(entity, "UTF-8")
     val jsonObj: JSONObject = new JSONObject(responseString);
     val grpObj: JSONObject = jsonObj.getJSONObject("response")
     val message = grpObj.getJSONArray("Messages")
     if(message.length() > 0) {
       resultMessage = message.get(0).toString()
     }
    }
    
    resultMessage
    } catch {
      case e: Exception =>
        e.printStackTrace()
        "FAILED"
    }
  }
  
   def deletePushWooshNotification(message: String) = { 
     try {
       val reqiestMsgobj = deleteRequestMsgObj(AUTH_TOKEN, message)
       val reqiestMsg = deleteRequestMsg(reqiestMsgobj)  
       val jsonString = new Gson().toJson(reqiestMsg)
       val httpClient: HttpClient = HttpClientBuilder.create().build();
       
       val request: HttpPost = new HttpPost(PUSHWOOSH_SERVICE_BASE_URL + deleteMethod);
       val params: StringEntity = new StringEntity(jsonString)
    
       request.addHeader("content-type", "application/json")
       request.setEntity(params)
    
       val response: HttpResponse = httpClient.execute(request)
     
     }  catch {
      case e: Exception =>
        e.printStackTrace()
        "FAILED"
    }
     
   }
   
  def setPushWooshDeviceID(header: String, contentMsg: String, time: Option[String], list: List[String],android_custom_icon:Option[String]): String = { 

    val deviceID = list.toArray

    if (deviceID.length > 0) {

      log.debug("Device IDs " + deviceID.length)
      var devices: Array[String] = new Array[String](10)

      for (i <- 0 until 10) {

        devices(i) = deviceID(0)

      }

      var pushWooshMsg = contentMsg

      if (contentMsg.length > 120) {
        pushWooshMsg = pushWooshMsg.substring(0, 120)
      }

      log.debug("PushWoosh Msg :" + pushWooshMsg)
      val response = sendPushWooshNotification(header, pushWooshMsg, devices, time,android_custom_icon)
      response
    } else {
      "Failure"
    }
  }
 
}