package models.com.hcue.doctors.traits.mobileSMS.Appointment

import java.text.SimpleDateFormat
import java.util.Calendar
import scala.slick.driver.PostgresDriver.simple._
import models.com.hcue.doctors.traits.dbProperties.db
import com.c7.utils.AQLData
import com.c7.utils.AQLResponse
import com.c7.utils.C7JSONUtils
import models.com.hcue.doctors.DAO.messaging.messagingDAO
import models.com.hcue.doctors.Json.Request.sendAppointmentSMSReq
import models.com.hcue.doctors.slick.lookupTable.DoctorJsonSetting
import models.com.hcue.doctors.slick.lookupTable.DoctorSpecialityLkup
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.HospitalAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.PatientEnquiry
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.THospitalAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.TPatientEnquiry
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalAddress
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalExtn
import models.com.hcue.doctors.slick.transactTable.hospitals.THospital
import models.com.hcue.doctors.slick.transactTable.hospitals.THospitalAddress
import models.com.hcue.doctors.slick.transactTable.messaging.AppointmentMsgLog
import models.com.hcue.doctors.slick.transactTable.patient.PatientEmail
import models.com.hcue.doctors.slick.transactTable.patient.PatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import models.com.hcue.doctors.slick.transactTable.patient.TPatientPhones
import play.Logger
import scalaj.http.Http
import scalaj.http.HttpOptions
import scalaj.http.HttpResponse
import models.com.hcue.doctors.slick.lookupTable.AppointmentStatus
import models.com.hcue.doctors.Json.Request.messaging.AppointmentSMSMessageObj
import java.sql.Date
import play.api.Play.current
object AppointmentStatusDao {

  private val log = Logger.of("application")

  val doctors = Doctors.Doctors
  val doctorExtn = DoctorsExtn.DoctorsExtn
  val patients = Patients.patients
  val patientsPhones = PatientPhones.patientsPhones
  val doctorSpecialityLkup = DoctorSpecialityLkup.DoctorSpecialityLkup
  val doctorAddress = DoctorAddress.DoctorsAddress
  val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn
  val doctorEmail = DoctorEmail.DoctorEmails
  val patientEmails = PatientEmail.patientsEmail
  val doctorPhone = DoctorPhones.DoctorsPhones
  val jsonSettingTable = DoctorJsonSetting.DoctorJsonSetting
  val hospital = Hospital.Hospital
  val hospitalExtn = HospitalExtn.HospitalExtn
  //val androidCustomIcon:Option[String] = Some(pushWooshCredentials.ANDROID_CUSTOM_ICON)
  val androidCustomIcon: Option[String] = Some("")
  val hospitalAppointment = HospitalAppointment.HospitalAppointment
  val hospitalAddress = HospitalAddress.HospitalAddress
  val patientEnquiry = PatientEnquiry.PatientEnquiry
  val appointmentMsgLog = AppointmentMsgLog.AppointmentMsgLog
  val url = current.configuration.getString("sms.api.url").get
  val accessToken = current.configuration.getString("sms.accesstoken").get
  
  def sendAppointmentStatus321SMS(data: Option[TDoctorAppointment], mode: String): String = db withSession { implicit request =>

    if (data != None) {
      //Get the mandator Fields
      val doctorID: Long = data.get.DoctorID.get
      val patientID: Long = data.get.PatientID.get
      val appointmentStatus: String = data.get.AppointmentStatus

      val doctor: Option[TDoctor] = doctors.filter(c => c.DoctorID === doctorID).firstOption
      val patient: Option[TPatient] = patients.filter(c => c.PatientID === patientID).firstOption
      val patientPhone: Option[TPatientPhones] = patientsPhones.filter(c => c.PatientID === patientID && c.PhType === "M").firstOption

      // new added:

      import models.com.hcue.doctors.helper.CalenderDateConvertor
      val ConsultationDt1: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
      val currentDateFormat = CalenderDateConvertor.getDateFormatter(ConsultationDt1)
      // context.put("consultDate", currentDateFormat.format(data.get.ConsultationDt))

      try {
        val dateObj = new SimpleDateFormat("H:mm").parse(data.get.StartTime)
        val TwelveHrFormat = new SimpleDateFormat("hh:mm a").format(dateObj)
        val consultDt = new SimpleDateFormat("dd-MMM-yyyy").format(data.get.ConsultationDt)
        var messageBuffer: String = ""

        if (patientPhone != None) {

          log.info("patientPhone : " + patientPhone.get.PhNumber)
          log.info("TwelveHrFormat : " + TwelveHrFormat)
          log.info("mode : " + mode)
          log.info("appointmentStatus : " + appointmentStatus)

          if (mode.equals("3rd") || mode.equals("2nd") || mode.equals("next")) {
            //messageBuffer = "http://trans.kapsystem.com/api/web2sms.php?workingkey=A2e7a2783e11a0de51aee1f6d386bfb4c&sender=HCUEMS&to=" + patientPhone.get.PhNumber + "&message=" + URLEncoder.encode("Dear " + patient.get.FirstName + ", You are " + mode + " in line for your " + consultDt + " " + TwelveHrFormat + " appointment with Dr " + doctor.get.FirstName, "UTF-8")
            // messageBuffer = "Dear " + patient.get.FirstName + ", You are " + mode + " in line for your " + consultDt + " " + TwelveHrFormat + " appointment with Dr " + doctor.get.FirstName
            messageBuffer = "Dear " + patient.get.FirstName + ", You are " + mode + " in line for your " + currentDateFormat.format(data.get.ConsultationDt) + " " + TwelveHrFormat + " appointment with Dr " + doctor.get.FirstName

          }
          log.info("Message : " + messageBuffer)
          if (messageBuffer.equals("")) {
            log.info("Blank Message")
          } else {
            // val status = hcueCommunication.sendHcueSMS(messageBuffer, patientPhone.get.PhNumber,None)
          }
        } else {
          log.info("No Phone Available")
        }

      } catch {
        case e: Exception =>
          e.printStackTrace()
      }

      return "Success"
    } else {

      return "Failure"
    }
  }

  def sendAppointmentSMS_v1(obj: sendAppointmentSMSReq): String = db withSession { implicit request =>
    if (obj.AppointmentID > 0) {
      //Get the mandatory Fields
      println("AppointmentID ::: "+obj.AppointmentID)
      val data: Option[THospitalAppointment] = hospitalAppointment.filter { x => x.AppointmentID === obj.AppointmentID }.firstOption
      if (data != None) {
        val doctorID: Long = data.get.DoctorID.get
        val patientID: Long = data.get.PatientID.get
        val appointmentStatus: String = data.get.AppointmentStatus
        val hospitalID = data.get.HospitalID.get
        val doctor: Option[TDoctor] = doctors.filter(c => c.DoctorID === doctorID).firstOption
        val patient: Option[TPatient] = patients.filter(c => c.PatientID === patientID).firstOption
        val patientPhone: Option[TPatientPhones] = patientsPhones.filter(c => c.PatientID === patientID && c.PhType === "M").firstOption

        if (patientPhone != None) {
          val strPhoneNumber = if (patientPhone.get.PhStdCD != None) { patientPhone.get.PhStdCD.get + "" + patientPhone.get.PhNumber } else { "44" + patientPhone.get.PhNumber }
          println("strPhoneNumber********" + strPhoneNumber)
          val patientPhoneNumber = strPhoneNumber.toLong
          println("patientPhoneNumber********" + patientPhoneNumber)
          val clinic: Option[THospital] = hospital.filter { x => x.HospitalID === hospitalID }.firstOption
          val clinicAddress: Option[THospitalAddress] = hospitalAddress.filter { x => x.HospitalID === clinic.get.HospitalID }.firstOption
          val patientenquiry: Option[TPatientEnquiry] = patientEnquiry.filter { x => x.patientenquiryid === data.get.Patientenquiryid }.firstOption

          val appointmentCategory = patientenquiry.get.appointmenttypeid
          val appointmentDate = data.get.ScheduleDate
          val appointmentTime = if ("00:00".equals(data.get.StartTime)) {
            data.get.EmergencyStartTime
          } else {
            data.get.StartTime
          }
          val scanCentreName = clinic.get.HospitalName
          val scanCentrePostCode = clinicAddress.get.PostCode

          val arrScheduledTime = appointmentTime.split(":");
          println("AppointmentStartTime :: " + arrScheduledTime(0) + ",AppointmentEndTime :: " + arrScheduledTime(1))

          val scheduleDay: Calendar = Calendar.getInstance
          //val scheduleDay: Calendar = CalenderDateConvertor.getUKTimeZoneDate()
          //println("scheduleDay :: " + scheduleDay.getTime)
          //set the appointment date
          scheduleDay.setTime(appointmentDate)
          val f: SimpleDateFormat = new SimpleDateFormat("MMM");
          val f1: SimpleDateFormat = new SimpleDateFormat("dd");
          val f2: SimpleDateFormat = new SimpleDateFormat("a");
          //set the appointment start and end time
          val schedule_hour = Integer.parseInt(arrScheduledTime(0).toString());
          val schedule_minutes = Integer.parseInt(arrScheduledTime(1).toString());
          scheduleDay.set(Calendar.HOUR, schedule_hour)
          scheduleDay.set(Calendar.MINUTE, schedule_minutes)

          val hour: Int = if (scheduleDay.get(Calendar.HOUR) == 0) { 12 }
          else { scheduleDay.get(Calendar.HOUR) }
          val minutes: Int = if (scheduleDay.get(Calendar.MINUTE) == 0) { 0 }
          else { scheduleDay.get(Calendar.MINUTE) }

          val formatedScheduleDate = f1.format(scheduleDay.getTime) + " " + f.format(scheduleDay.getTime) + " " + hour + ":" + minutes + f2.format(scheduleDay.getTime).toLowerCase();
          println("formatedScheduleDate::" + formatedScheduleDate)

          val messageBuffer = if ("B".equalsIgnoreCase(appointmentStatus)) {
            //This will be invoked for both book and reschedule, but will work only reschedule of an appointment
            deleteScheduledSMSandUpdateLogs(obj.AppointmentID, obj.USRId, obj.USRType)
            "Hi " + patient.get.FirstName + ", your appointment is booked on " + formatedScheduleDate + " at " +
              scanCentreName.get + ", " + scanCentrePostCode.get + ". Call 01329552440 for any queries"
          } else if ("DNA".equalsIgnoreCase(appointmentStatus)) {
            "Hi " + patient.get.FirstName + ", your missed your appointment on " + formatedScheduleDate + " at " +
              scanCentreName.get + ". Please Call us on 01329552440 to rebook"
          } else {
            deleteScheduledSMSandUpdateLogs(obj.AppointmentID, obj.USRId, obj.USRType)
            "Hi " + patient.get.FirstName + ", your appointment on " + formatedScheduleDate + " at " +
              scanCentreName.get + ", " + scanCentrePostCode.get + " has been cancelled. Call 01329552440 for any queries"
          }

          val logObject = new AppointmentSMSMessageObj(obj.AppointmentID, appointmentStatus, patientID,
            hospitalID, messageBuffer, "SMS", patientPhoneNumber, None, "IMMEDIATE",
            obj.USRId, obj.USRType)

          println("Message :: " + messageBuffer)
          println("Message-length :: " + messageBuffer.length())

          var sendOneDayBeforeSMS: Boolean = false
          var sendOneHourBeforeSMS: Boolean = false
          val currentDate: Calendar = Calendar.getInstance
          //val currentDate: Calendar = CalenderDateConvertor.getUKTimeZoneDate()
          println("-------------currentDate::" + currentDate.getTime)
          val dayOfCurrentDay = currentDate.get(Calendar.DAY_OF_MONTH)

          val hourOfCurrentDay = currentDate.get(Calendar.HOUR_OF_DAY)
          println("hourOfCurrentDay::" + hourOfCurrentDay)

          println("currentDay :: " + dayOfCurrentDay)
          println("---------------scheduleDay :: " + scheduleDay.getTime)

          val hourOfScheduleDay = scheduleDay.get(Calendar.HOUR_OF_DAY)
          println("hourOfScheduleDay :: " + hourOfScheduleDay)

          val dayOfAppointment = scheduleDay.get(Calendar.DAY_OF_MONTH)
          println("dayOfAppointment" + dayOfAppointment)
          val dayDiff = Math.abs(dayOfAppointment - dayOfCurrentDay)
          println("Day Difference :: " + dayDiff)

          val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

          val oneDayBeforeSchedule = if (dayDiff >= 2) {
            sendOneDayBeforeSMS = true;
            val cal: Calendar = Calendar.getInstance
            //val cal: Calendar = CalenderDateConvertor.getUKTimeZoneDate()
            cal.setTime(scheduleDay.getTime)
            cal.add(Calendar.DAY_OF_MONTH, -1);
            cal.set(Calendar.HOUR, 8);
            cal.set(Calendar.MINUTE, 0);
            dateFormat.format(cal.getTime)
          }

          //scheduleDay.set(Calendar.HOUR, schedule_hour)

          val hourDiff = hourOfScheduleDay - hourOfCurrentDay
          println("hourDiff::" + hourDiff)

          val oneHourBeforeSchdule = if (dayDiff == 0) {
            println("same day appointment" + scheduleDay.getTime)
            if (hourDiff > 1) {
              sendOneHourBeforeSMS = true
              scheduleDay.add(Calendar.HOUR_OF_DAY, -1)
              dateFormat.format(scheduleDay.getTime)
            }
          } else {
            sendOneHourBeforeSMS = true
            scheduleDay.add(Calendar.HOUR_OF_DAY, -1)
            dateFormat.format(scheduleDay.getTime)
          }

          println("oneDayBeforeSchedule::" + oneDayBeforeSchedule)
          println("oneHourBeforeSchdule::" + oneHourBeforeSchdule)
          val currentSchedule = dateFormat.format(currentDate.getTime)

          val isSendSMS = isGreaterThanCurrentDateTime(scheduleDay, currentDate)
          println("isSendSMS::::::"+isSendSMS)
          if(!isSendSMS){
           println("Appointment date is Invalid!")
           return "Appointment date is Invalid!"
          }

          sendSMSandUpdateLogs(logObject, Some(currentSchedule.toString()))

          if (sendOneDayBeforeSMS && "B".equalsIgnoreCase(appointmentStatus)) {
            println("inside sendOneDayBeforeSMS")

            val message = "Hi " + patient.get.FirstName + ", your appointment at " +
              scanCentreName.get + ", " + scanCentrePostCode.get + " is tomorrow at " + formatedScheduleDate + ". Please call 01329552440 if you can't make it"

            val scheduleTime = oneDayBeforeSchedule.toString()

            val logObject = new AppointmentSMSMessageObj(obj.AppointmentID, appointmentStatus, patientID,
              hospitalID, message, "SMS", patientPhoneNumber, Some(scheduleTime), "ONEDAYBEFORE",
              obj.USRId, obj.USRType)

            sendSMSandUpdateLogs(logObject, Some(oneDayBeforeSchedule.toString()))
          }

          if (sendOneHourBeforeSMS && "B".equalsIgnoreCase(appointmentStatus)) {
            println("inside sendOneHourBeforeSMS")
            val message = "Hi " + patient.get.FirstName + ", your appointment at " +
              scanCentreName.get + ", " + scanCentrePostCode.get + " is in the next hour at " +
              formatedScheduleDate + ". Please call 01329552440 if you can't make it"

            val scheduleTime = oneHourBeforeSchdule.toString()

            val logObject = new AppointmentSMSMessageObj(obj.AppointmentID, appointmentStatus, patientID,
              hospitalID, message, "SMS", patientPhoneNumber, Some(scheduleTime), "ONEHRBEFORE",
              obj.USRId, obj.USRType)

            sendSMSandUpdateLogs(logObject, Some(oneHourBeforeSchdule.toString()))
          }
          return "Success"
        } else {
          log.info("No Phone Available")
          return "Failure"
        }
      } else {
        log.info("Appointment data not Available")
        return "Failure"
      }
    } else {
      log.info("Unable to send SMS. Appointment ID is empty")
      return "Failure"
    }
  }

  def deleteScheduledSMSandUpdateLogs(AppointmentID: Long, USRId: Long, USRType: String): Boolean = db withSession { implicit request =>
    val appmntmsgloglist = appointmentMsgLog.filter { x => x.AppointmentID === AppointmentID }.list
    var isMessageCancelled = false
    println("appmnt list length :: " + appmntmsgloglist.length)
    if (appmntmsgloglist.length > 0) {
      for (i <- 0 until appmntmsgloglist.length) {
        val externalMsgId = appmntmsgloglist(i).ExternalID
        val deliveryType = appmntmsgloglist(i).DeliveryType
        println("externalMsgId :: " + externalMsgId + " deliveryType :: " + deliveryType)
        if (deliveryType.equalsIgnoreCase("ONEHRBEFORE")) {
          val response = deleteSchduledSMS(externalMsgId)
          println("Inside ONEHRBEFORE :: " + response)
          if (response._1.equalsIgnoreCase("SUCCESS")) {
            isMessageCancelled = true
            appointmentMsgLog.filter { x =>
              x.AppointmentID === AppointmentID &&
                x.ExternalID === externalMsgId && x.DeliveryType === deliveryType
            }.map { x => (x.Status, x.APIResponse, x.UpdtUSR, x.UpdtUSRType) }.update { ("CANCELLED", response.toString(), Some(USRId), Some(USRType)) }
            println("ONEHRBEFORE - Table updated")
          }
        }
        if (deliveryType.equalsIgnoreCase("ONEDAYBEFORE")) {
          val response = deleteSchduledSMS(externalMsgId)
          println("Inside ONEDAYBEFORE :: " + response)
          if (response._1.equalsIgnoreCase("SUCCESS")) {
            isMessageCancelled = true
            appointmentMsgLog.filter { x =>
              x.AppointmentID === AppointmentID &&
                x.ExternalID === externalMsgId && x.DeliveryType === deliveryType
            }.map { x => (x.Status, x.APIResponse, x.UpdtUSR, x.UpdtUSRType) }.update { ("CANCELLED", response.toString(), Some(USRId), Some(USRType)) }
            println("ONEDAYBEFORE - Table updated")
          }
        }
      }
    }
    return isMessageCancelled
  }

  def isGreaterThanCurrentDateTime(cal1: Calendar, cal2: Calendar): Boolean = {
    val flag = cal1.compareTo(cal2)
    println("flag::::::"+flag)
   if(flag > 0)
   {
     return true
   }
   else
   {
    return false
   }
  }

  def sendSMSandUpdateLogs(appointmentobj: AppointmentSMSMessageObj, sendTime: Option[String]) = {

    val response = if (appointmentobj.DeliveryType.equalsIgnoreCase("IMMEDIATE")) {
      sendSMS(appointmentobj.Message, appointmentobj.Destination, None)
    } else {
      sendSMS(appointmentobj.Message, appointmentobj.Destination, sendTime)
    }

    println("response::" + response._1)
    val body = response._2.body
    val jsonUtils: C7JSONUtils = new C7JSONUtils()
    val aqlresponse: AQLResponse = jsonUtils.parseAQLMesssage(body)
    val aqlData: Array[AQLData] = aqlresponse.getData

    if (aqlData.length > 0) {
      messagingDAO.addAppointmentMsgLog(appointmentobj, aqlData(0).getId, response._1, response.toString())
      //messagingDAO.addMessagingLog(message, phoneNumber, response._1, aqlData(0).getId, USRId, USRType, sendTime)
    } 
    
  }

  def sendSMS(message: String, number: Long, sendTime: Option[String]): (String, HttpResponse[String]) = db withSession { implicit request =>
    val result =
      if (sendTime != None) {
        val post_data = "{\"destinations\":[\"" + number + "\"],\"message\":\"" + message + "\",\"send_time\":\"" + sendTime.get + "\"}"
        println("postdata::" + post_data)
        Http(url + "send").postData(post_data)
          .header("Content-Type", "application/json")
          .header("X-Auth-Token", accessToken)
          .option(HttpOptions.readTimeout(10000)).asString
      } else {
        val post_data = "{\"destinations\":[\"" + number + "\"],\"message\":\"" + message + "\"}"
        println("postdata::" + post_data)
        Http("https://api.aql.com/v2/sms/send").postData(post_data)
          .header("Content-Type", "application/json")
          .header("X-Auth-Token", accessToken)
          .option(HttpOptions.readTimeout(10000)).asString
      }
    println("result.statusLine::" + result.statusLine)
    println("result.code::" + result.code)

    val respone = if (200 == result.code) {
      println("sendSms - success")
      "SUCCESS"
    } else {
      println("sendSms - failed")
      "FAILED"
    }
    return (respone, result)
  }

  def deleteSchduledSMS(messageID: Long): (String, HttpResponse[String]) = db withSession { implicit request =>
    val result = Http(url + messageID)
      .method("DELETE")
      .header("Content-Type", "application/json")
      .header("X-Auth-Token", accessToken)
      .option(HttpOptions.readTimeout(10000)).asString
    println("result.statusLine::" + result.statusLine)
    println("result.code::" + result.code)

    val respone = if (204 == result.code) {
      println("sendSms - success")
      "SUCCESS"
    } else {
      println("sendSms - failed")
      "FAILED"
    }
    return (respone, result)
  }

}    
 