package models.com.hcue.doctors.traits.common

import org.postgresql.util.PSQLException

import models.com.hcue.doctors.Json.Response.buildErrorResponseObj
import models.com.hcue.doctors.traits.dbProperties.db

object CommonDao extends Common {

  def buildIllegalArgumentException(exceptionType: String, e: IllegalArgumentException): buildErrorResponseObj = db withSession { implicit session =>

    val error: buildErrorResponseObj = new buildErrorResponseObj(exceptionType, e.getMessage, e.fillInStackTrace().toString(), e.getLocalizedMessage,
      "", "", e.getStackTrace.toString(), e.getSuppressed.toString(), e.toString())

    return error
  }

  def buildIllegalStateException(exceptionType: String, e: IllegalStateException): buildErrorResponseObj = db withSession { implicit session =>

    val error: buildErrorResponseObj = new buildErrorResponseObj(exceptionType, e.getMessage, e.fillInStackTrace().toString(), e.getLocalizedMessage,
      "", "", e.getStackTrace.toString(), e.getSuppressed.toString(), e.toString())

    return error
  }

  def buildException(exceptionType: String, e: Exception): buildErrorResponseObj = db withSession { implicit session =>

    val error: buildErrorResponseObj = new buildErrorResponseObj(exceptionType, e.getMessage, e.fillInStackTrace().toString(), e.getLocalizedMessage,
      "", "", e.getStackTrace.toString(), e.getSuppressed.toString(), e.toString())

    return error
  }

  def buildPSQLException(exceptionType: String, e: PSQLException): buildErrorResponseObj = db withSession { implicit session =>

    val error: buildErrorResponseObj = new buildErrorResponseObj(exceptionType, e.getMessage, e.fillInStackTrace().toString(), e.getLocalizedMessage,
      e.getErrorCode.toString(), e.getSQLState, e.getStackTrace.toString(), e.getSuppressed.toString(), e.toString())

    return error
  }

  def buildRuntimeException(exceptionType: String, e: RuntimeException): buildErrorResponseObj = db withSession { implicit session =>

    val error: buildErrorResponseObj = new buildErrorResponseObj(exceptionType, e.getMessage, e.fillInStackTrace().toString(), e.getLocalizedMessage,
      "", e.getSuppressed.toString(), e.getStackTrace.toString(), e.getSuppressed.toString(), e.toString())

    return error
  }

}