package models.com.hcue.doctors.traits.login

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB
import org.postgresql.util.PSQLException
import play.Logger
import models.com.hcue.doctors.dataCasting.DataCasting
import play.api.libs.Crypto
import models.com.hcue.doctors.Json.Request._
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao
import models.com.hcue.doctors.Json.Request.doctorLoginDetailsObj
import models.com.hcue.doctors.Json.Response.getResponseObj
import models.com.hcue.doctors.Json.Response.buildErrorResponseObj
import models.com.hcue.doctors.Json.Response.forgotPasswordResponse
import models.com.hcue.doctors.Json.Request.forgotPasswordRequest
import models.com.hcue.doctors.slick.transactTable.doctor.login.TDoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.login.DoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorPhones
import models.com.hcue.doctors.Json.Response.ForgotPasswordRes
import org.joda.time.DateTime
import models.com.hcue.doctors.helper.CalenderDateConvertor


object AunthenticationDao  {

  //By default the Result will be in Sort By Patient ID  
  val doctorLogin = DoctorLogin.DoctorLogin
  val doctorPhone = DoctorPhones.DoctorsPhones
  val doctorEmail = DoctorEmail.DoctorEmails
  val doctor = Doctors.Doctors
  val doctorextn= DoctorsExtn.DoctorsExtn
  
  
    
  //Google Login
  def validateGoogleLogin(a: doctorLoginDetailsObj): getResponseObj = db withSession { implicit session =>

    val output: getResponseObj = null

    val doctorRecord: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorLoginID === a.DoctorLoginID.toLowerCase()).firstOption

    if (doctorRecord.nonEmpty) {
      ListDoctorDao.completeResponse(doctorRecord.get.DoctorID,true,None,false)
    } else
      output

  }
  
  /*getResponseObj*/
  def validateDoctorLogin(a: doctorLoginDetailsObj): (String, Long) = db withSession { implicit session =>

    val output: getResponseObj = null
    val currentDate: java.sql.Date = new java.sql.Date(CalenderDateConvertor.getISDDate().getTime())
    val inst: DataCasting = new DataCasting()
    val doctorLoginID: Option[String] = inst.convertToLower(Some(a.DoctorLoginID))
    //val doctorRecord: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorLoginID === a.DoctorLoginID.toLowerCase() && c.DoctorPassword === Crypto.encryptAES(inst.stringconvertion(a.DoctorPassword))).firstOption
    val doctorRecord: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorLoginID === a.DoctorLoginID.toLowerCase()).firstOption
    
    val  lastLogin: java.sql.Timestamp = new java.sql.Timestamp(DateTime.now().getMillis )   
    
    if (doctorRecord != None) {

      if (doctorRecord.get.DoctorPassword == Crypto.encryptAES(inst.stringconvertion(a.DoctorPassword))) {
        
        println("PasswordExpiryDt value "+doctorRecord.get.PasswordExpiryDt.get.toString())
        println("currentDate value "+currentDate)

        if (doctorRecord.get.Active.equals("N")) {
          return ("PASSWORD_EXPIRED", 0)
        } else if(doctorRecord.get.PasswordExpiryDt.get.toString() <= currentDate.toString()) {
          
          println("come to if else ")
          
          return ("RESET YOUR PASSWORD",0)
        } else {
        return ("VALID_CREDENTIAL", doctorRecord.get.DoctorID)
      }
      } else {
        return ("IN_VALID_PASSWORD", 0)
      }

    } else {
      return ("INVALID_USER_NAME", 0)
    }

  }

  /*
   * COMMENTED OUT THIS PIECE OF CODE BECAUSE OF NEW FLOW- Priya
  def validateforgotPassword(obj: forgotPasswordRequest): forgotPasswordResponse = db withSession { implicit session =>

    var doctorRecord: Option[TDoctorLogin] = None
    var phone: Option[TDoctorPhones] = None
    var email: Option[TDoctorEmail] = None
    var phNo: Option[Long] = None
    var eID: Option[String] = None
    var doctorID: Long = 0

    if (obj.EmailID != None) {

      doctorRecord = doctorLogin.filter(c => c.DoctorLoginID.toLowerCase === obj.EmailID.get.toLowerCase()).firstOption

      if (doctorRecord != None) {
        doctorID = doctorRecord.get.DoctorID
        phone = doctorPhone.filter { x => x.DoctorID === doctorRecord.get.DoctorID && x.PhType === "M" }.firstOption
        email = doctorEmail.filter { x => x.DoctorID === doctorRecord.get.DoctorID }.firstOption
      }

    } else if (obj.MobileNumber != None) {

      phone = doctorPhone.filter { x => x.PhNumber === obj.MobileNumber.get && x.PhType === "M" }.firstOption

      if (phone != None) {
        doctorID = phone.get.DoctorID
        email = doctorEmail.filter { x => x.DoctorID === phone.get.DoctorID }.firstOption
      }

    }

    if (doctorID != 0) {

      phNo = if (phone != None) {
        doctorID = phone.get.DoctorID
        Some(phone.get.PhNumber)
      } else { None }

      eID = if (email != None) {
        doctorID = phone.get.DoctorID
        Some(email.get.EmailID)
      } else {
        None
      }

      return new forgotPasswordResponse(doctorID, phNo, eID, "Success")

    } else {
      return new forgotPasswordResponse(doctorID, phNo, eID, "Invalid_Login_ID")
    }

  }  */

  def validateforgotPassword(obj: forgotPasswordRequest): ForgotPasswordRes = db withSession { implicit session =>

    var doctorID: Long = 0
    var doctorName: Option[String] = None
    var doctorLoginID: Option[String] = None
    var doctorImg: Option[String] = None
    var mobileNumber: Option[String] = None
    var emailID: Option[String] = None

    val doctorRecord = doctorLogin.filter(c => c.DoctorLoginID.toLowerCase === obj.DoctorLoginID.toLowerCase()).firstOption

    if (doctorRecord != None) {
      doctorID = doctorRecord.get.DoctorID
      doctorLoginID = Some(doctorRecord.get.DoctorLoginID)

      val phoneInfo = doctorPhone.filter { x => x.DoctorID === doctorID && x.PhType === "M" }.firstOption
      if (phoneInfo != None) {
        mobileNumber = Some(phoneInfo.get.PhNumber)
      }
      val emailInfo = doctorEmail.filter { x => x.DoctorID === doctorID && x.EmailIDType === "P" }.firstOption
      if (emailInfo != None) {
        emailID = Some(emailInfo.get.EmailID)
      }
      val doctorname = doctor.filter { x => x.DoctorID === doctorRecord.get.DoctorID }.firstOption
      if (doctorname != None) {
        doctorName = Some(doctorname.get.FullName)
      }
      val doctorExtnInfo = doctorextn.filter { x => x.DoctorID === doctorRecord.get.DoctorID }.firstOption
      if (doctorExtnInfo != None) {
        doctorImg = doctorExtnInfo.get.ProfilImage
      }
      
      val result = forgotPasswordResponse(doctorID, doctorName, doctorLoginID, doctorImg, mobileNumber, emailID)
      return new ForgotPasswordRes("Success",result)

    } else {

      val result = forgotPasswordResponse(doctorID, doctorName, doctorLoginID, doctorImg, mobileNumber, emailID)
      return new ForgotPasswordRes("Invalid-LoginID",result)

    }

  }
 
  
  def validateUpdatePassword(a: updateDoctorPasswordObj): String = db withSession { implicit session =>

    if (a.PinPassCode != None) {

      val pinPass: Option[String] = Some(a.DoctorCurrentPassword)
      val patientRecord: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorLoginID.toLowerCase === a.DoctorLoginID.toLowerCase() &&
        c.PinPassCode === pinPass).firstOption

      if (patientRecord.nonEmpty) {
        val updateRecord = doctorLogin.filter(c => c.DoctorLoginID.toLowerCase === a.DoctorLoginID.toLowerCase()).map(_x => (_x.DoctorPassword,_x.PinPassCode)).update(Crypto.encryptAES(a.DoctorNewPassword),None)
        return "Password_Updated"
      } else {
        return "Password_Not_Updated"
      }
    }

    val doctorRecord: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorLoginID.toLowerCase === a.DoctorLoginID.toLowerCase() && c.DoctorPassword === Crypto.encryptAES(a.DoctorCurrentPassword)).firstOption

    if (doctorRecord.nonEmpty) {
      val updateRecord = doctorLogin.filter(c => c.DoctorLoginID.toLowerCase === a.DoctorLoginID.toLowerCase()).map(_x => _x.DoctorPassword).update(Crypto.encryptAES(a.DoctorNewPassword))
      return "Password_Updated"
    } else {
      return "Password_Not_Updated"
    }

  } 

  def validateDoctorPinPass(a: validateDoctorPinPassObj): String = db withSession { implicit session =>

    val doctorRecord: Option[TDoctorLogin] = doctorLogin.filter(c => c.DoctorLoginID === a.DoctorLoginID.toLowerCase() && c.PinPassCode === a.PinPassCode).firstOption

    if (doctorRecord.nonEmpty) {
      "Success"
    } else {
      "Failure"
    }

  }

}
