package models.com.hcue.doctors.traits.delete.apppointment

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointments
import models.com.hcue.doctors.Json.Request._
import models.com.hcue.doctors.traits.list._
import models.com.hcue.doctors.traits.dbProperties.db

object DeleteDoctorAppointmentDao {

  val doctorAppointment = DoctorAppointments.DoctorAppointments

  def deleteDoctorAppointment(appointmentID: Long): String = db withSession { implicit session =>

    val deleteAppointment = doctorAppointment.filter(c => c.AppointmentID === appointmentID).delete
    return "Success"

  }
}