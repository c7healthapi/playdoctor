package models.com.hcue.doctors.traits.delete

import org.postgresql.ds.PGSimpleDataSource
import models.com.hcue.doctors.slick.MyPostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorSpecialityCD
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorSpecialityCD

import models.com.hcue.doctors.Json.Request._
import models.com.hcue.doctors.traits.list._
import models.com.hcue.doctors.traits.dbProperties.db

object DeleteDoctorDao extends DeleteRecords {

  val doctors = Doctors.Doctors
  val doctorPhones = DoctorPhones.DoctorsPhones
  val doctorEmails = DoctorEmail.DoctorEmails
  val doctorAddress = DoctorAddress.DoctorsAddress
  val doctorSpecialityCD = DoctorSpecialityCD.DoctorSpecialityCD

  // val doctorAppointment = DoctorAppointments.DoctorAppointments

  def deleteDoctor(id: Long): Boolean = db withSession { implicit session =>

    val deleteEmails = doctorEmails.filter(c => c.DoctorID === id).delete
  //  val deleteAddress = doctorAddress.filter(c => c.DoctorID === id).delete
    val deletePhones = doctorPhones.filter(c => c.DoctorID === id).delete
    //val deleteDoctor = doctors.filter(c => c.DoctorID === id).delete

    deletePhones > 0

  }
  
  def deleteDoctorPhoneDetails(id: Long): Boolean = db withSession { implicit session =>
    val result = doctorPhones.filter(c => c.DoctorID === id).delete

    result > 0
  }

 /* def deleteDoctorAddressDetails(id: Long): Boolean = db withSession { implicit session =>
    val result = doctorAddress.filter(c => c.DoctorID === id).delete

    result > 0
  }*/

  def deleteDoctorEmailDetails(id: Long): Boolean = db withSession { implicit session =>
    val result = doctorEmails.filter(c => c.DoctorID === id).delete

    result > 0
  }
  
   def deleteEnquirySpeciality(id: Long): Boolean = db withSession { implicit session =>
    val result = doctorEmails.filter(c => c.DoctorID === id).delete

    result > 0
  }
  
  def deletedoctorSpecialityCD(id: Long): Boolean = db withSession { implicit session =>
    val result = doctorSpecialityCD.filter(c => c.DoctorID === id).delete

    result > 0
  }
}