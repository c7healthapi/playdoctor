package models.com.hcue.doctors.traits.delete

import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

trait DeleteRecords {

  def deleteDoctor(id: Long): Boolean

  //def deleteDoctorAddressDetails(id: Long): Boolean

  def deleteDoctorPhoneDetails(id: Long): Boolean

  def deleteDoctorEmailDetails(id: Long): Boolean

}

