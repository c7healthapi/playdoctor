package models.com.hcue.doctors.traits

/*import scala.collection.mutable.ListBuffer

object hCueTextContent {

  var DAYCODE_STARTED = ListBuffer[String]()

}*/
/*
object MeSHCallObj {

  private val _hostName = "http://ec2-13-232-93-144.ap-south-1.compute.amazonaws.com:28080"
  private val _authService = "/MeSHAPICall/rest/authenticate/"
  private val _messageStatusService = "/MeSHAPICall/rest/checkmessagestatus/"
  private val _sendMessageService = "/MeSHAPICall/rest/sendmessage"

  def hostName = _hostName
  def authService = _authService
  def messageStatusService = _messageStatusService
  def sendMessageService = _sendMessageService

}*/

/*object dataBaseCredentials {

private val _URL = "jdbc:postgresql://db-appc7-rds-prod-2.cesyjcqqvl4m.eu-west-2.rds.amazonaws.com:5432/DB_AppC7_RDS_Prod_1";
  private val _USER = "DBU_AppC7_RDS_Prod_1";
  private val _PASSWORD = "AuPrbrQM8FXPSA9";
  private val _MINPOOLSIZE = 5
  private val _MAXCONNECTIONAGE = 86400
  private val _ACQUIREINCREMENT = 1
  private val _MAXPOOLSIZE = 30

  def URL = _URL
  def USER = _USER
  def PASSWORD = _PASSWORD
  def MINPOOLSIZE = _MINPOOLSIZE
  def MAXCONNECTIONAGE = _MAXCONNECTIONAGE
  def ACQUIREINCREMENT = _ACQUIREINCREMENT
  def MAXPOOLSIZE = _MAXPOOLSIZE

}

object hcueBuckets {

  private final val _C7HEALTH_AWS_S3_ACCESS_KEY: String = "AKIAISBIOJ2UQOWGOGTQ"
  private final val _C7HEALTH_AWS_S3_SECRET_KEY: String = "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"

  private final val _HCUE_SECURITY_UTIL_BUCKET: String = ""
  private final val _C7HEALTH_TEMPLATES: String = "c7health-amp-template"
  private final val _DIAGNOSTICS_REPORT_CLOUDFRONT_URL: String = "https://d3dtc5efknn6r6.cloudfront.net"
  private final val _C7HEALTH_TEMPLATES_CLOUDFRONT_URL: String ="https://d23hqy4zda1kvo.cloudfront.net"
  private final val _ANALYTIC_REPORT_CLOUDFRONT_URL: String = "https://d7fgq3tr9w6a6.cloudfront.net"
  
  private final val _DIAGNOSTICS_REPORTS_BUCKET: String = "fs-appc7-reports-prod-2"
  private final val _GENERAL_REPORTS_BUCKET: String = "fs-appc7-analytics-prod-1"

  def C7HEALTH_AWS_S3_ACCESS_KEY = _C7HEALTH_AWS_S3_ACCESS_KEY
  def C7HEALTH_AWS_S3_SECRET_KEY = _C7HEALTH_AWS_S3_SECRET_KEY

  def HCUE_SECURITY_UTIL_BUCKET = _HCUE_SECURITY_UTIL_BUCKET
  def C7HEALTH_TEMPLATES = _C7HEALTH_TEMPLATES
  def DIAGNOSTICS_REPORT_CLOUDFRONT_URL = _DIAGNOSTICS_REPORT_CLOUDFRONT_URL
  def C7HEALTH_TEMPLATES_CLOUDFRONT_URL = _C7HEALTH_TEMPLATES_CLOUDFRONT_URL

  def DIAGNOSTICS_REPORTS_BUCKET: String = _DIAGNOSTICS_REPORTS_BUCKET
  def GENERAL_REPORTS_BUCKET: String = _GENERAL_REPORTS_BUCKET
  def ANALYTIC_REPORT_CLOUDFRONT_URL : String = _ANALYTIC_REPORT_CLOUDFRONT_URL
  
  
  def SIGNED_URL_EXPIRY_MONTH: Int = 3
}
*/
object hcueEmailCredentials {

  private final val _USE_SERVER: String = "smtp2"
  private final val _SEND_EMAIL: Boolean = true
  private final val _ALWAYS_SEND: String = "N"

  def USE_SERVER = _USE_SERVER
  def SEND_EMAIL = _SEND_EMAIL
  def ALWAYS_SEND = _ALWAYS_SEND

}

/*object hcueSmtp2EmailCredentials {

  /*** Deve Credentials
  private final val _SMTPFROM: String = "C7Health" //Replace with your "From" address. This address must be verified.
  private final val _HCUE_EMAIL_SMTP_USERNAME: String = "testhcue@gmail.com"
  private final val _HCUE_EMAIL_SMTP_PASSWORD: String = "hcue123456"
  private final val _HCUE_EMAIL_SMTP_HOST: String = "smtp.gmail.com"
  private final val _HCUE_EMAIL_SMTP_PORT: String = "25"
  ***/
  
  private final val _SMTPFROM: String = "diagnosticworld.reports@nhs.net"
  private final val _HCUE_EMAIL_SMTP_USERNAME: String = "diagnosticworld.reports@nhs.net"
  private final val _HCUE_EMAIL_SMTP_PASSWORD: String = "MywtW7#wpY3cN*XF7Z6buG$NkVh&sjzz7jJ"
  private final val _HCUE_EMAIL_SMTP_HOST: String = "send.nhs.net"
  private final val _HCUE_EMAIL_SMTP_PORT: String = "587"
  private final val _HCUE_EMAIL_CC_EMAIL: String = "dhsl.usreports@nhs.net"//"krish@myhcue.com" 


  def SMTPFROM = _SMTPFROM
  def HCUE_EMAIL_SMTP_USERNAME = _HCUE_EMAIL_SMTP_USERNAME
  def HCUE_EMAIL_SMTP_PASSWORD = _HCUE_EMAIL_SMTP_PASSWORD
  def HCUE_EMAIL_SMTP_HOST = _HCUE_EMAIL_SMTP_HOST
  def HCUE_EMAIL_SMTP_PORT = _HCUE_EMAIL_SMTP_PORT
  def HCUE_EMAIL_CC_EMAIL = _HCUE_EMAIL_CC_EMAIL
}

object hcueGmailEmailCredentials {


  private final val _GMAIL_FROM: String = "C7Health - Production<no-reply@hcue.co>"
  private final val _HCUE_EMAIL_GMAIL_USERNAME: String = "hcue.health1@gmail.com"
  private final val _HCUE_EMAIL_GMAIL_PASSWORD: String = "mzPN3N3WGarh"
  private final val _HCUE_EMAIL_GMAIL_HOST: String = "mail.smtp2go.com"
  private final val _HCUE_EMAIL_GMAIL_PORT: String = "2525"
  //private final val _HCUE_EMAIL_GMAIL_PORT: String = "465"


  def FROM_GMAIL = _GMAIL_FROM
  def HCUE_EMAIL_GMAIL_USERNAME = _HCUE_EMAIL_GMAIL_USERNAME
  def HCUE_EMAIL_GMAIL_PASSWORD = _HCUE_EMAIL_GMAIL_PASSWORD
  def HCUE_EMAIL_GMAIL_HOST = _HCUE_EMAIL_GMAIL_HOST
  def HCUE_EMAIL_GMAIL_PORT = _HCUE_EMAIL_GMAIL_PORT

}

object hcueTemplateLocation {

  private final val _diagnosticsReportTemplate = "diagnosticreporttemplate.vm"
  private final val _newDoctorMailTrigger = "doctor/doctordetails.vm"

  def diagnosticsReportTemplate = _diagnosticsReportTemplate
  def newDoctorMailTrigger = _newDoctorMailTrigger

}

object errorLogNotify {

  private final val _Environment = "Production"
  private final val _projectName = "Play Doctor"
  private final val _errorNotification = true
  private final val _hostName = "https://d23hqy4zda1kvo.cloudfront.net"
  private final val _errorLogbadrequest = "/ErrorLog/reportHcueBadRequestMessage.vm"
  private final val _errorLoghcueerror = "/ErrorLog/reportHcueErrorMessage.vm"
  private final val _errorLogPharmaerror = ""


  private final val _to400ID = "sabarinathan@cardinality.ai"
  private final val _cc400ID = "krish@cardinality.ai"

  private final val _to500ID = "sabarinathan@cardinality.ai"
  private final val _cc500ID = "krish@cardinality.ai"

  def errorNotification = _errorNotification
  def hostName = _hostName
  def errorLogBadRequest = _errorLogbadrequest
  def errorLogHcueError = _errorLoghcueerror
  def errorLogPharmaerror = _errorLogPharmaerror

  def projectName = _projectName
  def Environment = _Environment

  def to400ID = _to400ID
  def cc400ID = _cc400ID

  def to500ID = _to500ID
  def cc500ID = _cc500ID

}
*/

object SMSCredentials {

  private final val _sms_api_url = "https://api.smsbroadcast.co.uk/api-adv.php?username=c7health&password=SMSC7H38lth&to="

  def sms_api_url = _sms_api_url

}
