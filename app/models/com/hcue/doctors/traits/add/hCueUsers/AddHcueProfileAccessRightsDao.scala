package models.com.hcue.doctors.traits.add.hCueUsers

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateProfileAccessRights.addUpdateProfileAccessRightsObj
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalGrpSetting

object AddHcueProfileAccessRightsDao {

  val hospitalGrpSetting = HospitalGrpSetting.HospitalGrpSetting

  def AddHcueProfileAccessRights(request: addUpdateProfileAccessRightsObj) = db withSession { implicit session =>

    val storedRecord = hospitalGrpSetting.filter { x => x.GroupCode === request.GroupCode }.firstOption

    if (storedRecord == None) {

      hospitalGrpSetting.map { x => (x.GroupCode, x.GroupName, x.HospitalID, x.AccountAccessID, x.Active, x.CrtUSR, x.CrtUSRType) } +=
        (request.GroupCode, request.GroupName, request.HospitalID, request.AccountAccessID, request.Active,request.USRId, request.USRType)

    } else {
      hospitalGrpSetting.filter(_x => _x.GroupCode === request.GroupCode).map { x => (x.GroupName,x.HospitalID, x.AccountAccessID,x.Active, x.UpdtUSR, x.UpdtUSRType) } +=
        (request.GroupName, request.HospitalID, request.AccountAccessID, request.Active, Some(request.USRId), Some(request.USRType))

    }

  }

}