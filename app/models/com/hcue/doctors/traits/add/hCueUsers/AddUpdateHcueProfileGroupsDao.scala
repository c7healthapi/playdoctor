package models.com.hcue.doctors.traits.add.hCueUsers

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker

import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateProfileAccessRights.addUpdateProfileAccessRightsObj
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalGrpSetting
import models.com.hcue.doctors.slick.transactTable.hCueUsers.THospitalGrpSetting
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.traits.dbProperties.db
import play.Logger

object AddUpdateHcueProfileGroupsDao {

  val logger = Logger.of("application")

  val hospitalGrpSetting = HospitalGrpSetting.HospitalGrpSetting
  val doctorsAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn
  val hospital = Hospital.Hospital

  def AddUpdateHcueProfileGroups(request: addUpdateProfileAccessRightsObj) : String = db withSession { implicit session =>

    val Parenthospitalid=hospital.filter { x => x.HospitalID === request.HospitalID}.map { x => x.ParentHospitalID.getOrElse(0L) }.firstOption
    
    val storedRecord = hospitalGrpSetting.filter { x => x.GroupCode === request.GroupCode }.firstOption

    if (storedRecord == None) {
      logger.info("Adding a profile group record")
      hospitalGrpSetting.map { x => (x.GroupCode, x.GroupName, x.HospitalID, x.RoleID, x.AccountAccessID, x.Active, x.ReadOnly,x.CrtUSR, x.CrtUSRType) } +=
        (0, request.GroupName, Parenthospitalid.get, Some(request.RoleID), request.AccountAccessID, request.Active,"N",request.USRId, request.USRType)
    } else {
      logger.info("Updating profile group record::" + request.GroupCode)
      
      val profileAccessGroup: THospitalGrpSetting = new THospitalGrpSetting(request.GroupCode, request.GroupName,Parenthospitalid.get, Some(request.RoleID), 
          request.AccountAccessID, request.Active, storedRecord.get.ReadOnly,storedRecord.get.CrtUSR, storedRecord.get.CrtUSRType, Some(request.USRId), Some(request.USRType))
      
      hospitalGrpSetting.filter(_x => _x.GroupCode === request.GroupCode).update(profileAccessGroup)
      
      
     if(request.GroupName != None) {
       val hospitalInfo =  hospital.filter { x => x.ParentHospitalID === storedRecord.get.HospitalID }.list
       if(request.Active == "N") {
         for(i <- 0 until hospitalInfo.length){
             val updateRecord = doctorsAddressExtn.filter(c => c.HospitalID === hospitalInfo(i).HospitalID && c.RoleID === storedRecord.get.RoleID && c.GroupID === storedRecord.get.GroupName).map(_x => (_x.RoleID,_x.GroupID)).update(null, null)
         }
       } else {
          if(storedRecord.get.GroupName != None && storedRecord.get.GroupName.get != request.GroupName.get) {
            for(i <- 0 until hospitalInfo.length){
               val updateRecord = doctorsAddressExtn.filter(c => c.HospitalID === hospitalInfo(i).HospitalID && c.GroupID === storedRecord.get.GroupName).map(_x => (_x.GroupID)).update(request.GroupName)
             }
          }
       }
       
     }
      
    }
 return "SUCCESS"
  }
  
   def addUpdateHospitalConsultantFlag(request: addUpdateProfileAccessRightsObj): String = db withSession { implicit session =>
  
        val hospitalid: Long =  request.HospitalID
        val grpCD: String = request.GroupName.get
        val roleCD: String = request.RoleID
        
     (sql"select * from update_hospital_consultaion_flag('#$hospitalid','#$grpCD','#$roleCD')".as[String]).firstOption.get
      
   
   "Success"
  }
  
  
  
  
  

}