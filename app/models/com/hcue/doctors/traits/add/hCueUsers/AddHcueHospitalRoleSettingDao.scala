package models.com.hcue.doctors.traits.add.hCueUsers

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalRoleSetting

import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueRoleLkup
import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHospitalRole.addDefaultHospitalRoleObj


object AddHcueHospitalRoleSettingDao {

  val hcueRoleLkup = HcueRoleLkup.HcueRoleLkup
  val hospitalRoleSetting = HospitalRoleSetting.HospitalRoleSetting
//  val hospitalGrpSetting = HospitalGrpSetting.HospitalGrpSetting

  def addHospitalRole(RoleInfo: addDefaultHospitalRoleObj): String = db withSession { implicit session =>

    val defaultRecord = hcueRoleLkup.filter { x => x.IsPrivate === "D" }.list

    val storedRecord = hospitalRoleSetting.filter { x => x.HospitalID === RoleInfo.HospitalID }.list

    for (i <- 0 until defaultRecord.length) {

      val record = storedRecord.find { x => x.RoleID == defaultRecord(i).RoleID }
      if (record == None) {
        //Add
        hospitalRoleSetting.map { x => (x.HospitalID, x.RoleID, x.ActiveIND, x.CrtUSR, x.CrtUSRType) } +=
          (RoleInfo.HospitalID, defaultRecord(i).RoleID, "Y", RoleInfo.USRId, RoleInfo.USRType)

      }

    }


  /*  val storedRecord = hospitalRoleSetting.filter { x => x.HospitalID === RoleInfo.HospitalID && x.RoleID === RoleInfo.RoleID }.list
    if(storedRecord.length > 0){
      
        hospitalRoleSetting.filter { x => x.HospitalID === RoleInfo.HospitalID && x.RoleID === RoleInfo.RoleID }.map { x => 
          (x.HospitalID, x.RoleID, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType)
          }.update(RoleInfo.HospitalID, RoleInfo.RoleID, RoleInfo.ActiveIND, Some(RoleInfo.USRId), Some(RoleInfo.USRType))
          
       if(RoleInfo.AccountAccessID != None) {
         
       val grpInfo = hospitalGrpSetting.filter { x => x.HospitalID === RoleInfo.HospitalID && x.RoleID === RoleInfo.RoleID }.list  
        
       val grpSettingObj: addUpdateProfileAccessRightsObj = new addUpdateProfileAccessRightsObj(grpInfo(0).GroupCode, None, 
             RoleInfo.HospitalID, RoleInfo.RoleID, RoleInfo.AccountAccessID.get, RoleInfo.ActiveIND, RoleInfo.USRId, RoleInfo.USRType) 
       
       val grpSettings = AddHcueProfileAccessRightsDao.AddHcueProfileAccessRights(grpSettingObj)
       }
    
    } else {
       
      hospitalRoleSetting.map { x => (x.HospitalID, x.RoleID, x.ActiveIND, x.CrtUSR, x.CrtUSRType) } +=
          (RoleInfo.HospitalID, RoleInfo.RoleID, RoleInfo.ActiveIND, RoleInfo.USRId, RoleInfo.USRType)
      
       if(RoleInfo.AccountAccessID != None) {

       val grpSettingObj: addUpdateProfileAccessRightsObj = new addUpdateProfileAccessRightsObj(0, None,RoleInfo.HospitalID, 
           RoleInfo.RoleID, RoleInfo.AccountAccessID.get, RoleInfo.ActiveIND, RoleInfo.USRId, RoleInfo.USRType) 
       
       val grpSettings = AddHcueProfileAccessRightsDao.AddHcueProfileAccessRights(grpSettingObj)
       
       }
    }
*/
    
    return "Success"
  }

}