package models.com.hcue.doctors.traits.add.hCueUsers

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import play.Logger

import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccessRights.addUpdateHcueAccessRightsObj
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccessLkup

object AddHcueAccessRightsDao {

  val logger = Logger.of("application")
  val hcueAccessLkup = HcueAccessLkup.HcueAccessLkup

  def addUpdateHcueAccessRights(request: addUpdateHcueAccessRightsObj) = db withSession { implicit session =>

    val storedRecord = hcueAccessLkup.filter { x => x.AccessID === request.AccessID }.firstOption

    if (storedRecord == None) {

      hcueAccessLkup.map { x => (x.AccessID, x.AccessDesc, x.ActiveIND, x.ParentAccessID, x.CrtUSR, x.CrtUSRType) } +=
        (request.AccessID, request.AccessDesc, request.ActiveIND, request.ParentAccessID, request.USRId, request.USRType)

    } else {
      hcueAccessLkup.filter(_x => _x.AccessID === request.AccessID).map { x => (x.AccessDesc, x.ActiveIND, x.ParentAccessID, x.UpdtUSR, x.UpdtUSRType) } +=
        (request.AccessDesc, request.ActiveIND, request.ParentAccessID, Some(request.USRId), Some(request.USRType))

    }

  }

}