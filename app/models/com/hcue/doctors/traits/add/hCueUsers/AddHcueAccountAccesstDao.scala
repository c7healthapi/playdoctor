package models.com.hcue.doctors.traits.add.hCueUsers

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccountAccess.addUpdateAccountAccessObj
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccountAccessLkup

object AddHcueAccountAccesstDao {

  val hcueAccountAccessLkup = HcueAccountAccessLkup.HcueAccountAccessLkup

  def addUpdateHcueAccountAccess(request: addUpdateAccountAccessObj) = db withSession { implicit session =>

    if (request.AcntAccessID == None) {

      hcueAccountAccessLkup.map { x => (x.AcntAccessID,x.AccessID,x.AccountID,x.ActiveIND, x.CrtUSR, x.CrtUSRType) } +=
        (request.AccessID, request.AccessID, request.AccountID,request.ActiveIND, request.USRId, request.USRType)

    } else {
      hcueAccountAccessLkup.filter(_x => _x.AcntAccessID === request.AcntAccessID).map { x => (x.AccessID,x.AccountID,x.ActiveIND, x.UpdtUSR, x.UpdtUSRType) } +=
        (request.AccessID, request.AccountID, request.ActiveIND, Some(request.USRId), Some(request.USRType))

    }

  }

}