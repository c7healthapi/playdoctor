package models.com.hcue.doctors.traits.add.hCueUsers

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccount.addUpdateHcueAccountObj
import models.com.hcue.doctors.slick.transactTable.hCueUsers.THcueAccountLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccountLkup

object AddHcueAccountDao {

  val hcueAccountLkup = HcueAccountLkup.HcueAccountLkup

  def addUpdateHcueAccount(request: addUpdateHcueAccountObj) = db withSession { implicit session =>

    val storedRecord = hcueAccountLkup.filter { x => x.AccountID === request.AccountID }.firstOption

    if (storedRecord == None) {

      hcueAccountLkup.map { x => (x.AccountID, x.AccountDesc, x.ActiveIND, x.CrtUSR, x.CrtUSRType) } +=
        (request.AccountID, request.AccountDesc, request.Active, request.USRId, request.USRType)

    } else {
      hcueAccountLkup.filter(_x => _x.AccountID === request.AccountID).map { x => (x.AccountDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType) } +=
        (request.AccountDesc, request.Active, Some(request.USRId), Some(request.USRType))

    }

  }

}