package models.com.hcue.doctors.traits.add.hCueUsers

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueRole.addUpdateRoleObj
import models.com.hcue.doctors.slick.transactTable.hCueUsers.THcueRoleLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueRoleLkup

object AddHcueRoleDao {

  val hcueRoleLkup = HcueRoleLkup.HcueRoleLkup

  def addUpdateRole(request: addUpdateRoleObj) = db withSession { implicit session =>

    val storedRecord = hcueRoleLkup.filter { x => x.RoleID === request.RoleID }.firstOption

    if (storedRecord == None) {

      hcueRoleLkup.map { x => (x.RoleID,x.RoleDesc,x.ActiveIND,x.IsPrivate,x.HospitalID, x.CrtUSR, x.CrtUSRType) } +=
        (request.RoleID,request.RoleDesc,request.ActiveIND,request.IsPrivate,request.HospitalID,request.USRId, request.USRType)

    } else {
      hcueRoleLkup.filter(_x => _x.RoleID === request.RoleID).map { x => (x.RoleDesc,x.ActiveIND,x.IsPrivate,x.HospitalID, x.UpdtUSR, x.UpdtUSRType) } +=
        (request.RoleDesc,request.ActiveIND,request.IsPrivate,request.HospitalID, Some(request.USRId), Some(request.USRType))

    }

  }

}