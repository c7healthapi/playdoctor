package models.com.hcue.doctors.traits.add.doctor

import play.Logger
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import java.io.InputStreamReader
import java.io.BufferedReader
import java.io.OutputStream
import java.io.BufferedOutputStream
import java.io.FileOutputStream
import java.io.FileNotFoundException
import com.amazonaws.services.s3.model.GetObjectRequest
import java.io.File
import java.io.FileWriter
import java.nio.file.Paths
import java.nio.file.Path
import models.com.hcue.doctors.Json.Request.doctorFileDownloadObj
//import models.com.hcue.doctors.traits.hcueProfileImgCloudFront
//import models.com.hcue.doctors.traits.hcueCookieHost
import models.com.hcue.doctors.traits.hcueEmailCredentials
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import play.api.Play.current


object FileDownload {
   val doctors = Doctors.Doctors

  val patients = Patients.patients
  
  val log = Logger.of("application")
  
   val s3AccessKey = current.configuration.getString("aws.s3.accessKey").get
  val s3SecretKey = current.configuration.getString("aws.s3.secretKey").get

    //val AWS_S3_CREDENTIAL2 = models.com.hcue.doctors.traits.hcueCookieHost
  

   //val AWS_DOCTOR_UPLOADED_FILES = AWS_S3_CREDENTIAL.DOCTOR_UPLOADED_FILES
   //val AWS_DOC_PROFILE_CLOUD_FRONT_DOMAIN = hcueCookieHost.COOKIE_ENABLED_DOCTOR_APP_CLOUD_FRONT_DOMAIN
  //val AWS_CLOUD_FRONT_PEM_CERT = hcueCookieHost.CLOUD_FRONT_PEM_CERT
  //val AWS_HCUE_DOMAIN_PROTOCOL = hcueCookieHost.CLOUD_FRONT_URL_PROTOCOL
  
   val yourAWSCredentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey)
   val amazonS3Client = new AmazonS3Client(yourAWSCredentials)
   //val profileBucket = amazonS3Client.createBucket(AWS_DOCTOR_UPLOADED_FILES)
   val profileBucket = amazonS3Client.createBucket("")
   
  def getFile(a:doctorFileDownloadObj): String = {
     
    try{
       val p:Path = Paths.get(a.fromPath);
       val file:String = p.getFileName().toString();
       log.error("a.frompath")
       val s3Object = amazonS3Client.getObject (new GetObjectRequest (profileBucket.getName(), a.fromPath), new File (a.toPath+file))
       return "fileDownloaded"
    }catch {
       case e: FileNotFoundException =>
               e.printStackTrace()
               return "File not found"
        case e: Exception =>
               return "file not downloading"
    }
  }
    def putFile(doctorID:String): String = db withSession { implicit session =>

      val docID:Long = doctorID.toLong
      val isDoc:Option[TDoctor] = doctors.filter { x => x.DoctorID === docID }.firstOption
      if(isDoc != None){  
      return("OK")
       }
       else{
         ("Not valid credentials")
       }
  }
}