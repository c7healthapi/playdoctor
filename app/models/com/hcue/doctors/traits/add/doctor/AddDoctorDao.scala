package models.com.hcue.doctors.traits.add.doctor

import java.text.SimpleDateFormat
import java.util.Calendar

import scala.slick.driver.PostgresDriver.simple._

import org.apache.velocity.VelocityContext

import play.api.libs.Crypto
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorAddressDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorAddressExtnObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorCommunicationObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorEmailDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.addDoctorDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.addDoctorObj
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorEducationObj
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorOtherDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorPublicationAchievementObj
import models.com.hcue.doctors.Json.Response.getResponseObj
import models.com.hcue.doctors.dataCasting.DataCasting
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.helper.HCueUtils
import models.com.hcue.doctors.helper.RequestResponseHelper
import models.com.hcue.doctors.helper.RequestResponseHelper.CombineHashMap
import models.com.hcue.doctors.helper.RequestResponseHelper.ValidateHashMap
import models.com.hcue.doctors.helper.RequestResponseHelper.validateString
import models.com.hcue.doctors.helper.hcueCommunication
import models.com.hcue.doctors.helper.hcueHelperClass
import models.com.hcue.doctors.helper.hcueHelperClass.setHstoreValue
import models.com.hcue.doctors.slick.lookupTable.CityLkup
import models.com.hcue.doctors.slick.lookupTable.Country
import models.com.hcue.doctors.slick.lookupTable.CurrencyLkup
import models.com.hcue.doctors.slick.lookupTable.DoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.LocationLkup
import models.com.hcue.doctors.slick.lookupTable.StateLkup
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddressPhone
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointments
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorSpecialityCD
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorSpecialityCD
import models.com.hcue.doctors.slick.transactTable.doctor.login.DoctorLogin
import models.com.hcue.doctors.slick.transactTable.doctor.login.TDoctorLogin
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao
import play.Logger
import play.api.db.slick.DB
import play.api.libs.Crypto
import models.com.hcue.doctors.slick.transactTable.hCueUsers
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalGrpSetting

import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.hospitals.THospital
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorDepartmentsObj
import models.com.hcue.doctor.slick.transactTable.Careteam.DoctorPracties
import models.com.hcue.doctors.slick.lookupTable.DoctorAccountDetails
import models.com.hcue.doctor.slick.transactTable.doctor.ExtnDoctor
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorConsultations
import models.com.hcue.doctors.slick.lookupTable.TDoctorAccountDetails
import models.com.hcue.doctors.Json.Request.AddUpdate.PractiesDetails
import models.com.hcue.doctor.traits.add.consultation.AddConsultationDao
import models.com.hcue.doctor.Json.Request.AddUpdateConsultation.addConsultationObj
import models.com.hcue.doctors.Json.Request.AddUpdate.SpecialityDetailsObj
import play.api.Play.current






object AddDoctorDao extends AddDoctor {

  val logger = Logger.of("application")

  logger.info("Created c3p0 connection pool")

  //By default the Result will be in Sort By Doctor ID  

  val doctors = Doctors.Doctors
  
  val extndoctor = ExtnDoctor.ExtnDoctor

  val hospital = Hospital.Hospital

  val doctorExtn = DoctorsExtn.DoctorsExtn

  val doctorPhones = DoctorPhones.DoctorsPhones

  val doctorCommunication = DoctorAddressPhone.DoctorAddressPhone

  val doctorEmails = DoctorEmail.DoctorEmails

  val doctorAddress = DoctorAddress.DoctorsAddress

  val doctorAppointment = DoctorAppointments.DoctorAppointments

  val doctorsAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

  val doctorLogin = DoctorLogin.DoctorLogin

  val doctorSpecialityCD = DoctorSpecialityCD.DoctorSpecialityCD

  val doctorConsultation = DoctorConsultations.DoctorConsultations

  val doctorSpecialityLkup = DoctorSpecialityLkup.DoctorSpecialityLkup

  val countryLkup = Country.Country

  val currencyLkup = CurrencyLkup.CurrencyLkup

  val stateLkup = StateLkup.StateLkup

  val locationLkup = LocationLkup.LocationLkup

  val cityLkup = CityLkup.CityLkup
  
  val doctorAccountDetails = DoctorAccountDetails.DoctorAccountDetails
  
  val doctorpractiesdetails= DoctorPracties.DoctorPracties

  def addDoctor(a: addDoctorObj): getResponseObj = db withSession { implicit session =>
    val inst: DataCasting = new DataCasting()

    val doctorRecords: addDoctorDetailsObj = a.doctorDetails

    val doctorLoginID: Option[String] = inst.convertToLower(a.doctorDetails.DoctorLoginID)
    val autoGenDoctorID = addDoctorDetails(doctorLoginID, doctorRecords, a, a.doctorOtherDetails)
    //logger.info(autoGenDoctorID)

    doctorRecords.SpecialityCD.foreach { x =>
      val value: Map[String, String] = x
      value.iterator.foreach { y =>
        var customSpeciality: Option[String] = None
        if (a.CustomSpecialityDesc != None) {
          val temp = a.CustomSpecialityDesc.get.get(y._2)
          if (temp != None) {
            customSpeciality = temp
          }
        }
        addSpecialityCode(autoGenDoctorID, y._2, customSpeciality)
      }
    }

    logger.info("Doctor Address : " + a.DoctorAddress)
    // Insert the Doctor Password
    if (doctorRecords.DoctorLoginID != None) {
      //logger.info("Crypto.encryptAES(inst.stringconvertion(doctorRecords.DoctorPassword)" + Crypto.encryptAES(inst.stringconvertion(doctorRecords.DoctorPassword)).length())
      val loginObj: TDoctorLogin = new TDoctorLogin(autoGenDoctorID, inst.stringconvertion(doctorLoginID), Crypto.encryptAES(doctorRecords.DoctorPassword.getOrElse(codeGeneration(7))), "Y", null, None, "N", None, None)

      doctorLogin += loginObj

    }

    var DoctorSetting: Map[String, String] = Map()

    var strNickname =""
    if(doctorRecords.FirstName.length()>1){
     strNickname = doctorRecords.FirstName.substring(0, 2).toUpperCase()
    }
    if (a.DoctorSettings != None) {
      var doctorSettings: Map[String, String] = a.DoctorSettings.get
      val key = "DoctorNickName"

      if (doctorSettings.contains(key)) {
        var DoctorNickName = doctorSettings.get(key)
        if (DoctorNickName == "") {
          val DoctorName = key -> strNickname
          doctorSettings = doctorSettings + DoctorName
        }
      }
      DoctorSetting = doctorSettings
    }

    //adding default currency code in preference setting based on Country ID
    if (HCueUtils.strSetDefaultCurrency.equalsIgnoreCase("Y")) {
      var doctorSettings: Map[String, String] = DoctorSetting
      val CurrencyKey = "CurrencyCode"
      if (a.DoctorAddress != None) {
        val addressDtl = a.DoctorAddress.get
        if (addressDtl(0).Country != None) {
          val Country = addressDtl(0).Country
          val countryId = countryLkup.filter { x => x.CountryID === Country }.map { x => x.CountryID }.firstOption.getOrElse("IN")
          val currency = currencyLkup.filter { x => x.CountryID === countryId }.map { x => x.CurrencyCode }.firstOption
          if (currency != None) {
            val currencySetting = CurrencyKey -> currency.get
            doctorSettings = doctorSettings + currencySetting
          }
        }
      }
      DoctorSetting = doctorSettings
    }    

    val doctorExtValue: TDoctorExtn = new TDoctorExtn(autoGenDoctorID, ValidateHashMap(doctorRecords.Qualification), ValidateHashMap(doctorRecords.About), doctorRecords.HcueScore, ValidateHashMap(doctorRecords.Awards),
      ValidateHashMap(doctorRecords.Membership), ValidateHashMap(doctorRecords.Services), ValidateHashMap(doctorRecords.MemberID), doctorRecords.Prospect, doctorRecords.ReferredBy, None,
      Some(CalenderDateConvertor.getISDDate()), None, None, None, ValidateHashMap(Some(DoctorSetting)), a.USRId, a.USRType, null, null)

    doctorExtn += doctorExtValue

    //Insert DoctorAddress
    a.DoctorAddress.foreach { x =>
      val doctorAddressDetails: Array[DoctorAddressDetailsObj] = x
      for (i <- 0 until doctorAddressDetails.length) {
        logger.info("Inside Add Doctor" + autoGenDoctorID)
        addAddress(doctorAddressDetails(i), a, autoGenDoctorID)
      }
    }
    
    if(a.SpecialityDetails != None){
          a.SpecialityDetails.foreach { x =>
      val doctorSpecialityDetails: Array[SpecialityDetailsObj] = x
      for (i <- 0 until doctorSpecialityDetails.length) {
        addSpeciality(doctorSpecialityDetails(i), a, autoGenDoctorID)
      }
    }  
    }
    
    //Insert Practiesdetails
    
    doctorpractiesdetails.filter { x => x.doctorid === autoGenDoctorID }.map { x => (x.active, x.updtusr, x.updtusrtype)}.
       update(Some(false),Some(a.USRId), Some(a.USRType))
   
       
    a.PractiesDetails.foreach {  x =>
      val practiesDetails: Array[PractiesDetails] = x
      for (i <- 0 until practiesDetails.length) {
        println("doctorid value 1 "+autoGenDoctorID)
        addPractiesdetails(a,practiesDetails(i),autoGenDoctorID)
      }
      }
    
    logger.info(" DoctorPhone : " + a.DoctorPhone)
    //Insert patientPhone
    a.DoctorPhone.foreach { x =>
      val doctorPhoneDetails: Array[DoctorPhoneDetailsObj] = x
      for (i <- 0 until doctorPhoneDetails.length) {
        addPhoneNumber(doctorPhoneDetails(i), a, autoGenDoctorID)
      }
    }

    //Insert patientEmail
    a.DoctorEmail.foreach { x =>
      val doctorEmailDetails: Array[DoctorEmailDetailsObj] = x
      for (i <- 0 until doctorEmailDetails.length) {
        addEmail(doctorEmailDetails(i), a, autoGenDoctorID)
      }
    }

    ListDoctorDao.completeResponse(autoGenDoctorID, true, None,false)

  }

 
 def addDoctorDetails(doctorLoginID: Option[String], doctorRecords: addDoctorDetailsObj, a: addDoctorObj, doctorOtherDtls: Option[doctorOtherDetailsObj]): Long = db withSession { implicit session =>
    val panCardId: Option[String] = if (doctorOtherDtls != None) { doctorOtherDtls.get.PanCardID } else { None } 
 
    var Searchable_Final: String = "N"
    if(a.Searchable != None)
    {
         Searchable_Final =a.Searchable.get
    }
  

    val mappedValue: TDoctor = new TDoctor(doctorRecords.FirstName, doctorRecords.LastName, 
      doctorRecords.FirstName + " " + doctorRecords.LastName.getOrElse(""), 0, doctorRecords.Gender, doctorRecords.DOB, doctorRecords.DoctorLoginID, 
      ValidateHashMap(setHstoreValue(doctorRecords.SpecialityCD)), Some(doctorRecords.Exp.getOrElse(0)),doctorRecords.TermsAccepted,
      None, Some(Searchable_Final), doctorRecords.RegistrationNo, a.USRId, a.USRType, null, null,doctorOtherDtls.get.faxno,doctorOtherDtls.get.gpcode,doctorOtherDtls.get.typevalue,doctorOtherDtls.get.notes,doctorRecords.Title)
    
    val autoGenDoctorID = (doctors returning doctors.map(_.DoctorID)) += mappedValue
    
    if(a.isPracticeDoctor !=None){
    extndoctor.filter { x => x.DoctorID === autoGenDoctorID }.map { x => x.PracticeDoctorActive}.update(a.isPracticeDoctor)
    } 
    
    if(a.AccountDetails != None)
    {
      var ExpiryDate : Option[java.sql.Date]  =  None
      if(a.AccountDetails.get.ExpiryDate != None)
      {
        ExpiryDate = a.AccountDetails.get.ExpiryDate
      }
        val AccDetailsValues:  TDoctorAccountDetails  = new TDoctorAccountDetails(autoGenDoctorID, a.AccountDetails.get.UserType,
        a.AccountDetails.get.Comments,ExpiryDate, a.USRId, a.USRType,None,None,a.HospitalDetails.get.HospitalCD,None)
    
    doctorAccountDetails += AccDetailsValues
    }
       
    return autoGenDoctorID

  }

  def addDoctorCode(autoGenDoctorID: Long, code: String) = db withSession { implicit session =>

    //  val mappedValue: TDoctorSpecialityCD = new TDoctorSpecialityCD(autoGenDoctorID, code, None)

    doctorSpecialityCD.map(c => (c.DoctorID, c.SpecialityCD)) +=
      (autoGenDoctorID, code)

  }

  def addSpecialityCode(autoGenDoctorID: Long, code: String, customSpeciality: Option[String]) = db withSession { implicit session =>

    doctorSpecialityCD.map(c => (c.DoctorID, c.SpecialityCD, c.CustomSpecialityDesc)) +=
      (autoGenDoctorID, code, customSpeciality)

  }
  
    def addSpeciality(b:SpecialityDetailsObj,a:addDoctorObj,autoGenDoctorID: Long) = db withSession { implicit session =>
      

      doctorSpecialityCD.map(c => (c.DoctorID,c.SpecialityCD,c.CustomSpecialityDesc,c.SubSpecialityID,c.ActiveIND)) +=
      (autoGenDoctorID,b.specialityID,b.specialityDesc,b.SubSpecialityID,b.ActiveIND) 
      
  }

  def addAddress(doctorAddressObj: DoctorAddressDetailsObj, b: addDoctorObj, autoGenDoctorID: Long) = db withSession { implicit session =>
    var initval: Option[Long] = None

    var ClinicSetting: Option[Map[String, String]] = None
    if (doctorAddressObj.ClinicSettings != None) {
      val Nickname = doctorAddressObj.ClinicName.get.substring(0, 2).toUpperCase()
      var ClinicSettings: Map[String, String] = doctorAddressObj.ClinicSettings.get
      val key = "ClinicNickName"
      var ClinicNickName = doctorAddressObj.ClinicSettings.get(key)
      if (ClinicNickName == "") {
        val ClinicName = key -> Nickname
        ClinicSettings = ClinicSettings + ClinicName
      }
      ClinicSetting = Some(ClinicSettings)
    }

    if (validateString(doctorAddressObj.Address2)) {

      val countryCode = doctorAddressObj.Country.get
      val country = ListDoctorDao.getCounrty(countryCode)

      val StateCode = doctorAddressObj.State
      val state = ListDoctorDao.getState(StateCode)

      val cityCode = doctorAddressObj.CityTown
      val city = ListDoctorDao.getCity(cityCode)

      val location = doctorAddressObj.Location.get
      val locationID = ListDoctorDao.getLocation(location)

      if (country.length == 0 && doctorAddressObj.Country != None) {
        countryLkup.map(x => (x.CountryID, x.CountryDesc, x.ActiveIND, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (doctorAddressObj.Country.get, doctorAddressObj.Country.get, "Y", b.USRId, b.USRType, Some("Y"))
      }

      if (state.length == 0 && doctorAddressObj.State != None) {

        stateLkup.map(x => (x.StateID, x.StateDesc, x.ActiveIND, x.CountryID, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (doctorAddressObj.State.get, doctorAddressObj.State.get, "Y", doctorAddressObj.Country.get, b.USRId, b.USRType, Some("Y"))
      }
      if (city.length == 0  && doctorAddressObj.CityTown != None) {
        cityLkup.map(x => (x.CityID, x.CityIDDesc, x.ActiveIND, x.StateID, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (doctorAddressObj.CityTown.get, doctorAddressObj.CityTown.get, "Y", doctorAddressObj.State.get, b.USRId, b.USRType, Some("Y"))
      }

      if (locationID.length == 0 && doctorAddressObj.Location != None) {

        locationLkup.map(x => (x.LocationID, x.LocationIDDesc, x.CityID, x.ActiveIND, x.CrtUSR, x.CrtUSRType, x.isPrivate)) += (doctorAddressObj.Location.get, doctorAddressObj.Location.get, doctorAddressObj.CityTown.get, "Y", b.USRId, b.USRType, Some("Y"))

      }
    }

    var autoGenAddressID: Long = 0
    // Need to Check and Confirm
    var mappedValue: TDoctorAddress = new TDoctorAddress(autoGenDoctorID, 0, doctorAddressObj.ClinicName, ValidateHashMap(ClinicSetting), doctorAddressObj.Address1, doctorAddressObj.Address2, doctorAddressObj.Street,
      doctorAddressObj.Location, doctorAddressObj.CityTown, doctorAddressObj.DistrictRegion, doctorAddressObj.State, doctorAddressObj.PostCode, doctorAddressObj.Country,
      doctorAddressObj.AddressType, doctorAddressObj.LandMark, "Y", b.USRId, b.USRType, null, null,doctorAddressObj.ShowTimings,doctorAddressObj.Address4)

    autoGenAddressID = (doctorAddress returning doctorAddress.map(_.AddressID)) += mappedValue

    logger.info("Inside Add Doctor" + autoGenAddressID)

    val extValue: Option[DoctorAddressExtnObj] = doctorAddressObj.ExtDetails
    logger.info("Ext Value " + extValue)
    if (extValue != None) {

      var hospitalCode: Option[String] = None
      var branchCode: Option[String] = None
      if (extValue.get.HospitalInfo != None) {
        hospitalCode = extValue.get.HospitalInfo.get.HospitalCode
        branchCode = extValue.get.HospitalInfo.get.BranchCode
      }
   if(doctorAddressObj.ExtDetails != None){
     
     val parentHospitalID = hospital.filter { x => x.HospitalID === extValue.get.HospitalID }.map { x => x.ParentHospitalID.getOrElse(0L) }.firstOption   
     
        logger.info("RoleID" + extValue.get.RoleID + ":::GroupID ::" + extValue.get.GroupID)
      doctorsAddressExtn.map(c => (c.DoctorID, c.HospitalID, c.ParentHospitalID, c.HospitalCode, c.AddressID, c.PrimaryIND, c.Latitude, c.Longitude, c.CrtUSR, c.CrtUSRType, c.UpdtUSR, c.UpdtUSRType, c.RoleID, c.GroupID,c.MinPerCase,c.RatePerHour,c.RateAboveTwenty,c.RateBelowTwenty)) +=
        (autoGenDoctorID, extValue.get.HospitalID, parentHospitalID, hospitalCode, autoGenAddressID, extValue.get.PrimaryIND,
          extValue.get.Latitude, extValue.get.Longitude, b.USRId, b.USRType, None, None, extValue.get.RoleID, extValue.get.GroupID,extValue.get.MinPerCase,
          extValue.get.RateperHour,extValue.get.RateAboveTwenty,extValue.get.RateBelowTwenty)
          }

      //Add Phone Number for communication
      logger.info(" DoctorCommunication : " + doctorAddressObj.ExtDetails.get.AddressCommunication)
      if (doctorAddressObj.ExtDetails.get.AddressCommunication != None) {
        doctorAddressObj.ExtDetails.get.AddressCommunication.foreach { x =>
          val doctorCommunication: Array[DoctorCommunicationObj] = x
          for (i <- 0 until doctorCommunication.length) {
            logger.info(" DoctorCommunication test1 :")
            addcommunication(autoGenDoctorID, autoGenAddressID, doctorCommunication(i), b.USRId, b.USRType)
          }
        }
      }

      //Handle Address Consultation
      val addressID = autoGenAddressID
      val doctorID = autoGenDoctorID
      val usrID = b.USRId
      val usrType = b.USRType
      val consultationDet = doctorAddressObj.ExtDetails.get.ConsultDetails
      if (consultationDet != None) {
        val validateRecords = AddConsultationDao.addValidateConsultation(doctorID, addressID, consultationDet.get)
        if (validateRecords._1) {

          val addedRecord = AddConsultationDao.addConsultation(new addConsultationObj(consultationDet.get, addressID, doctorID, usrID.toInt, usrType))
        }
      }

    } else {
      doctorsAddressExtn.map(c => (c.DoctorID, c.AddressID, c.PrimaryIND, c.CrtUSR, c.CrtUSRType)) += (autoGenDoctorID, autoGenAddressID, "Y", b.USRId, b.USRType)

    }
  }

  def addPhoneNumber(a: DoctorPhoneDetailsObj, b: addDoctorObj, autoGenPatientID: Long) = db withSession { implicit session =>

    doctorPhones.map(c => (c.DoctorID, c.PhCntryCD, c.PhStateCD, c.PhAreaCD, c.PhNumber, c.PhType, c.PrimaryIND, c.CrtUSR, c.CrtUSRType, c.UpdtUSR, c.UpdtUSRType, c.AuditTracker)) +=
      (autoGenPatientID, None, None, None, a.PhNumber, a.PhType, a.PrimaryIND, b.USRId, b.USRType, null, null, a.AuditTracker)
  }

  def addcommunication(doctorID: Long, addressID: Long, a: DoctorCommunicationObj, usrID: Long, usrType: String) = db withSession { implicit session =>

    doctorCommunication.map(c => (c.DoctorID, c.AddressID, c.PhCntryCD, c.PhStateCD, c.PhAreaCD, c.PhNumber, c.PhType, c.RowID, c.PrimaryIND, c.PublicDisplay, c.CrtUSR, c.CrtUSRType)) +=
      (doctorID, addressID, a.PhCntryCD, a.PhStateCD, a.PhAreaCD, a.PhNumber, a.PhType, a.RowID, a.PrimaryIND, a.PublicDisplay, usrID, usrType)
  }

  def addEmail(a: DoctorEmailDetailsObj, b: addDoctorObj, autoGenPatientID: Long) = db withSession { implicit session =>

    var emailValidity = "N"

    doctorEmails.map(c => (c.DoctorID, c.EmailID, c.EmailIDType, c.PrimaryIND, c.ValidIND, c.CrtUSR, c.CrtUSRType, c.UpdtUSR, c.UpdtUSRType, c.AuditTracker)) +=
      (autoGenPatientID, a.EmailID.toLowerCase().trim(), a.EmailIDType, a.PrimaryIND, emailValidity, b.USRId, b.USRType, null, null, a.AuditTracker)

  }

  def addPractiesdetails(a: addDoctorObj, b: PractiesDetails, autoGenDoctorID: Long) = db withSession { implicit session =>

    /*if (a != None) {*/ // If is not requierd

    println("doctorid value 2 " + autoGenDoctorID)

    var record = doctorpractiesdetails.filter { x => x.doctorid === autoGenDoctorID && x.practiceid === b.practiceid }.firstOption

    if (record != None) {
      println("come to if ")

      doctorpractiesdetails.filter { x => x.doctorid === autoGenDoctorID && x.practiceid === b.practiceid }.map { x => (x.practicename, x.practicecode, x.postcode, x.ccgname, x.telephone, x.active, x.updtusr, x.updtusrtype) }.
        update(b.practicename, b.practicecode, b.postcode, b.ccgname, b.contactnumber, Some(true), Some(a.USRId), Some(a.USRType))

    } else {

      println("come to else ")

      println("doctorid value 2 " + autoGenDoctorID)

      doctorpractiesdetails.map { x => (x.doctorid, x.practiceid, x.practicename, x.practicecode, x.postcode, x.ccgname, x.telephone, x.active, x.crtusr, x.crtusrtype) } += (autoGenDoctorID, b.practiceid, b.practicename, b.practicecode, b.postcode, b.ccgname, b.contactnumber, Some(true), a.USRId, a.USRType)

    }
    /*
    }*/
  }


  def addSEOName(DoctorID: Long): String = db withSession { implicit session =>

    val doctorInfo: Option[TDoctor] = doctors.filter { x => x.DoctorID === DoctorID }.firstOption

    if (doctorInfo.get.SEOName == None) {
      val doctorSpecialityCode: Option[TDoctorSpecialityCD] = doctorSpecialityCD.filter { x => x.DoctorID === DoctorID }.firstOption

      if (doctorSpecialityCode != None) {
        val doctorSpecialityDesc = doctorSpecialityLkup.list.find { _x => _x.DoctorSpecialityID == doctorSpecialityCode.get.SpecialityCD }

        var seoName: String = ""
        if (doctorInfo.get.LastName != None && doctorInfo.get.LastName != Some("") && doctorInfo.get.LastName != "") {
          seoName = doctorInfo.get.FirstName.trim().toLowerCase() + '-' + doctorInfo.get.LastName.get.trim().toLowerCase() + '-' + doctorSpecialityDesc.get.DoctorSpecialityDesc.trim().toLowerCase()

        } else {
          seoName = doctorInfo.get.FirstName.trim().toLowerCase() + '-' + doctorSpecialityDesc.get.DoctorSpecialityDesc.trim().toLowerCase()
        }
        seoName = seoName.replaceAll("[\\.][\\s]", "-").replaceAll("[ ]", "-").replaceAll("--", "-").replaceAll("[\\(]","").replaceAll("[\\)]","").replaceAll("[,]","").replaceAll("[.]","")
        val count = (for { doctor <- doctors if doctor.SEOName.toLowerCase like ("" + seoName + "%").toLowerCase() } yield (doctor.SEOName)).list

        if (count.length > 0) {
          seoName = seoName + '-' + count.length
        }
        doctors.filter { x => x.DoctorID === DoctorID }.map { x => (x.SEOName) }.update(Some(seoName))

        return "Success"
      } else {
        return "Failure"
      }
    } else {
      return "SEO Name Already exists"
    }
  }

  import scala.slick.jdbc.StaticQuery.interpolation

  def addUpdateConsultantFlag(addressDtls: Array[models.com.hcue.doctors.Json.Response.DoctorAddressDetailsObj]): String = db withSession { implicit session =>

    for (i <- 0 until addressDtls.length) {

      if (addressDtls(i).ExtDetails != None && addressDtls(i).ExtDetails.get.HospitalID != None && addressDtls(i).ExtDetails.get.GroupID != None) {

        val addressId: Long = addressDtls(i).AddressID.get
        val hospitalid: Long = addressDtls(i).ExtDetails.get.HospitalID.get
        val grpCD: String = addressDtls(i).ExtDetails.get.GroupID.get
        val roleCD: String = addressDtls(i).ExtDetails.get.RoleID.get

        (sql"select * from update_consultaion_flag(#$addressId,'#$hospitalid','#$grpCD','#$roleCD')".as[String]).firstOption.get
      }
    }
    "Success"
  }

  def codeGeneration(length: Int): String = {
    // val chars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9') 
    val chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%^&*!"
    var password = randomStringFromCharList(length, chars)
    return password
  }

  def randomStringFromCharList(length: Int, chars: Seq[Char]): String = {
    val sb = new StringBuilder("")
    for (i <- 1 to length) {
      val randomNum = util.Random.nextInt(chars.length)
      sb.append(chars(randomNum))
    }
    sb.toString
  }

  def validateDocLicense(a: addDoctorObj): String = db withSession {  implicit session =>

    var licValidationFlag = false
    var licValidationStatus="Success"

    if (a.DoctorAddress != None) {

      val addressDtl = a.DoctorAddress.get

      if (addressDtl(0).ExtDetails != None) {

        val hospitalId = addressDtl(0).ExtDetails.get.HospitalID
        

        val roleID = addressDtl(0).ExtDetails.get.RoleID.getOrElse("")

        if (hospitalId != None) {

          val hosInfo: Option[THospital] = hospital.filter { x => x.HospitalID === hospitalId }.firstOption

          if (hosInfo != None) {

            val parentHospitalInfo : Option[THospital] = hospital.filter { x => x.HospitalID === hosInfo.get.ParentHospitalID }.firstOption
       
            val hosinfoList = hospital.filter { x => x.ParentHospitalID === parentHospitalInfo.get.HospitalID }.map { x =>x.HospitalID }.list

             var queryForDoctorAddressInfo = for {
      (doctorAdd, doctorAddExtn) <- doctorAddress innerJoin doctorsAddressExtn on (_.DoctorID === _.DoctorID)
      if doctorAddExtn.ParentHospitalID === hosInfo.get.ParentHospitalID && doctorAdd.AddressID === doctorAddExtn.AddressID
    } yield (doctorAdd, doctorAddExtn)
    
   
             //var hospitalDoctorInfo: List[(TDoctorAddress, TDoctorAddressExtn)] = List()
             var hospitalDoctorInfo: List[Long] = List()
             var hospitalRecepInfo: List[(TDoctorAddress, TDoctorAddressExtn)] = List()
    
            
            hospitalDoctorInfo = queryForDoctorAddressInfo.filter{x=>    x._1.Active === "Y" && x._2.ParentHospitalID.getOrElse(0L) === parentHospitalInfo.get.HospitalID && (x._2.RoleID === "DOCTOR" || x._2.RoleID === "ADMIN")}.map{x=>x._1.DoctorID}.list.distinct
             
            hospitalRecepInfo=queryForDoctorAddressInfo.filter{x=>x._1.Active === "Y" && x._2.HospitalID === hosInfo.get.HospitalID && x._2.RoleID === "RECPTNIST" }.list
            
            var usrCount: Int = hospitalDoctorInfo.length + 1

            var recepCount: Int = hospitalRecepInfo.length
            
            val pref : Option[Map[String,String]]= parentHospitalInfo.get.Preference
  
            if (pref != None) {
            
              if (roleID == "DOCTOR") {
          
                if (pref.get.contains("MaxDoc")) {
                  var count = pref.get("MaxDoc")
                  if (count != null && usrCount <= count.toInt) {
                    licValidationFlag = true
                  }
                  else{
                    licValidationFlag = false
                    licValidationStatus="DoctorLimit"
                  }
                } else {
                  licValidationFlag = true
                }
              } else if (roleID == "RECPTNIST") {

                if (pref.get.contains("MaxRecep")) {
                  var count = pref.get("MaxRecep")
                  
                  if (count != null && recepCount < count.toInt) {
                    licValidationFlag = true
                  
                  }else{
                    
                    licValidationFlag = false  
                    licValidationStatus="RecpLimit"
                    
                  }
                } else {
                  licValidationFlag = true
                }
              } else  {
                licValidationFlag = true
              }
              
              if (!licValidationFlag && licValidationStatus=="DoctorLimit") {
                return HCueUtils.StrConstDoctorLimitExceed
              }else if(!licValidationFlag && licValidationStatus=="RecpLimit")
              {
                return HCueUtils.StrConstRecLimitExceed
              }else if(!licValidationFlag)
              {
                return HCueUtils.StrConstLicLimitExceed
              }
            } 
          }
        }
      }
    }
    return HCueUtils.StrConstSuccess
  }

  /*def newDoctorEmail(docDtls: Array[models.com.hcue.doctors.Json.Response.getResponseObj]): String = db withSession { implicit session =>*/

  def newDoctorEmail(docDtls: getResponseObj, Speciality: addDoctorObj): String = db withSession { implicit session =>

    var doctorSpecilization = ""
    var Final_Specialization = ""
    if(Speciality.doctorDetails.SpecialityCD != None)
    {
    val doctorSpecialityType = DoctorSpecialityLkup.DoctorSpecialityLkup
    
    val speciality = doctorSpecialityType.list
      
    var count = Speciality.doctorDetails.SpecialityCD.get.size
    val temp = Speciality.doctorDetails.SpecialityCD.get.foreach { x =>
      val sepcialityInfo = speciality.find { _x => _x.DoctorSpecialityID == x._2 }
      var sepcialityDesc = sepcialityInfo.get.DoctorSpecialityDesc
    if (count > 1) {
        doctorSpecilization = sepcialityDesc + "/" + doctorSpecilization
        Final_Specialization = doctorSpecilization.substring(0, doctorSpecilization.length-1)
      } else {
        Final_Specialization = sepcialityDesc
      }
    }
    }
    else
    {
      Final_Specialization =  "NA"
    }
    
    var vmFileName: String = current.configuration.getString("mailtrigger").get
    val doctorID = docDtls.doctor.apply(0).DoctorID
    val doctorFullName = docDtls.doctor.apply(0).FullName
    val version = "Default"
    val doctorLoginID = docDtls.doctor.apply(0).DoctorLoginID.getOrElse("")
    val doctorPassword = Crypto.decryptAES(doctorLogin.filter { x => x.DoctorID === doctorID }.map { x => x.DoctorPassword }.firstOption.getOrElse(""))
    val format = new SimpleDateFormat()
    val weeEndDate = format.format(Calendar.getInstance().getTime())
    
    import org.apache.velocity.VelocityContext
    import java.text.DateFormat

    val context = new VelocityContext();

    context.put("doctorID", doctorID)
    context.put("doctorFullName", doctorFullName)
    context.put("doctorSpecilization", Final_Specialization)
    context.put("weeEndDate", weeEndDate)
    context.put("version", version)
    context.put("doctorLoginID", doctorLoginID)
    context.put("doctorPassword", doctorPassword)
    val buckettemplates = current.configuration.getString("buckets.templates").get 
    val html = hcueCommunication.getHcueHTMLTempalte(context, buckettemplates, vmFileName)
    val contentSubject =  doctorFullName + " - Newly Added User";
    val returnValue = hcueCommunication.sendHcueEmail(html, doctorLoginID, Some("vignesh.k@myhcue.com"),None, contentSubject, "Y", "Y",None)
    HCueUtils.StrConstSuccess
  
  }

  /* def addAppointments(a: addDtAppointmentDetailsObj): Option[TDoctorAppointment] = db withSession { implicit session =>

    val consultationAddress: Option[TDoctorConsultation] = doctorConsultation.filter(c => c.AddressConsultID === a.AddressConsultID).firstOption

    logger.info("*******************************")
    logger.info("consultationAddress.get.StartTime: " + consultationAddress.get.StartTime)
    logger.info("consultationAddress.get.StartTime: " + a.StartTime)

    var consultaionStartTime: String = hcueHelperClass.setTimeFormats(consultationAddress.get.StartTime)
    var patientStartTime: String = hcueHelperClass.setTimeFormats(a.StartTime)
    var duration: Int = consultationAddress.get.MinPerCase.get

    logger.info("consultaionStartTime: " + consultaionStartTime)
    logger.info("patientStartTime: " + patientStartTime)

    val tokenNumber: Long = hcueHelperClass.geTokenNumber(consultaionStartTime, patientStartTime, duration)

    val mappedValue: TDoctorAppointment = new TDoctorAppointment(0, a.AddressConsultID, a.DayCD, a.ConsultationDt, a.StartTime, a.EndTime, a.PatientID, a.VisitUserTypeID,
      a.DoctorID, "N", a.DoctorVisitRsnID, a.OtherVisitRsn, a.ActualFees, a.AppointmentStatus, tokenNumber.toString(), 0, a.USRId, a.USRType, null, null)

    val autoGenDoctorID = (doctorAppointment returning doctorAppointment.map(_.AppointmentID)) += mappedValue

    val outPut: Option[TDoctorAppointment] = doctorAppointment.filter(c => c.AppointmentID === autoGenDoctorID).firstOption

    return outPut
  }*/

}