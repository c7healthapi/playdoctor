package models.com.hcue.doctors.traits.add.doctor

import org.postgresql.ds.PGSimpleDataSource
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.Json.Request.AddUpdate.addDoctorObj
import models.com.hcue.doctors.Json.Response.getResponseObj
import models.com.hcue.doctors.Json.Request.addDtAppointmentDetailsObj
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment

trait AddDoctor {
  
 def addDoctor(a: addDoctorObj): getResponseObj
 
// def addAppointments(a: addDtAppointmentDetailsObj): Option[TDoctorAppointment]
}

