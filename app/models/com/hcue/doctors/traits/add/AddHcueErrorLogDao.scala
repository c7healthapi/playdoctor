package models.com.hcue.doctors.traits.add

import models.com.hcue.doctors.Json.Response.AddErrorLogReq
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.jdbc.StaticQuery
import scala.slick.jdbc.StaticQuery.interpolation
import models.com.hcue.doctors.traits.dbProperties.db
import play.api.db.slick.DB

import org.slf4j.LoggerFactory
import models.com.hcue.doctors.slick.transactTable.THcueErrorLog
import models.com.hcue.doctors.slick.transactTable.HcueErrorLog


object AddHcueErrorLogDao {

  val hcueerrorlog = HcueErrorLog.HcueErrorLog

  def addHcueErrorLog(a: AddErrorLogReq): Long = db withSession { implicit session =>
 
    try {
    if (a.ErrorCode == 400) {
      
     val errorID = (hcueerrorlog.map { x => (x.ErrorCode, x.Environment, x.project, x.RemoteAddress, x.RequestURL, x.RequestMethod, x.RequestParameter, x.ResponseParameter,x.ResponseString,x.CrtUSR, x.CrtUSRType)
      } returning hcueerrorlog.map(_.ErrorID))  += (a.ErrorCode, a.Environment, a.project, a.RemoteAddress, a.RequestURL, a.RequestMethod, a.RequestParameter, a.ResponseParameter, a.ResponseError, a.USRId, a.USRType)
      
      return errorID
      
    } else {
      
     val errorID = (hcueerrorlog.map { x => (x.ErrorCode, x.Environment, x.project, x.RemoteAddress, x.RequestURL, x.RequestMethod, x.RequestParameter, 
          x.ResponseParameter, x.ResponseString, x.CrtUSR, x.CrtUSRType)
      } returning hcueerrorlog.map(_.ErrorID))  += (a.ErrorCode, a.Environment, a.project, a.RemoteAddress, a.RequestURL, a.RequestMethod,
          a.RequestParameter,a.ResponseParameter,a.ResponseError, a.USRId, a.USRType) 
      
       return errorID
         
    }
     }
     catch {
      case e: Exception =>
        e.printStackTrace()
       
    }
     0L
  }

}