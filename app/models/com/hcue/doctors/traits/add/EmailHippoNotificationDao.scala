package models.com.hcue.doctors.traits.add

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.hospitals.EmailHippoNotification



object EmailHippoNotificationDao {

  val emailHippo = EmailHippoNotification.EmailHippoNotification

  def addEmails( EmailID: String, ValidIND:String): String = db withSession { implicit session =>

    val emailID = emailHippo.filter { x => x.EmailID === EmailID.toLowerCase().trim() }.firstOption

    if (emailID == None) {

      emailHippo.map { x => (x.EmailID, x.ValidIND, x.CrtUSR, x.CrtUSRType) } += (EmailID.toLowerCase().trim(), ValidIND, 0, "ADMIN")

    }

    return "Success"

  }

} 