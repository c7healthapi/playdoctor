package models.com.hcue.doctors.traits.add.hospitalappointment

import java.io.BufferedInputStream
import java.io.File
import java.io.InputStream
import java.nio.file.Files
import java.sql.Date
import java.sql.Date
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker
import org.apache.velocity.VelocityContext
import org.joda.time.DateTime
import scala.util.matching.Regex
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.S3Object
import controllers.doctors.API.Doctor.MakeCSV
import models.com.hcue.doctor.slick.transactTable._
import models.com.hcue.doctor.slick.transactTable.Careteam.PracticeLkup
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.HospitalSchedule
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.HospitalScheduleExtn
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.THospitalSchedule
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.THospitalScheduleExtn
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.THospitalScheduleExtn
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.THospitalScheduleExtn
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.addScheduleAppointmentDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.appointmentEmailerObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.buildScheduleAppointmentList
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.getHospitalAppointmentDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.sonographerinfoObj
import models.com.hcue.doctors.Json.Request.AppointmentStatusUpdateObj
import models.com.hcue.doctors.Json.Request.appointmentresponseObj
import models.com.hcue.doctors.Json.Request.appointmentsCountResponseObj
import models.com.hcue.doctors.Json.Request.calenderObj
import models.com.hcue.doctors.Json.Request.consultationDtlObj
import models.com.hcue.doctors.Json.Request.doctorPersonalInfo
import models.com.hcue.doctors.Json.Request.listCalenderComponentRequestObj
import models.com.hcue.doctors.Json.Request.listCalenderComponentResponseObj
import models.com.hcue.doctors.Json.Request.patientPersonalInfo
import models.com.hcue.doctors.Json.Request.slotListObj
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.helper.RequestResponseHelper
import models.com.hcue.doctors.helper.hcueCommunication
import models.com.hcue.doctors.helper.hcueHelperClass
import models.com.hcue.doctor.slick.transactTable.Careteam.TSpecialityLkup
import models.com.hcue.doctors.slick.transactTable.PlatformPartners.Hospital.Hospital
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorConsultations
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorEmail
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorPhones
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.HospitalAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.HospitalAppointmentExtn
import models.com.hcue.doctors.slick.transactTable.doctor.PatientEnquiry
import models.com.hcue.doctors.slick.transactTable.doctor.PatientEnquiry
import models.com.hcue.doctors.slick.transactTable.doctor.PatientEnquiryDtlLkup
import models.com.hcue.doctors.slick.transactTable.doctor.ReferrerEnquiry
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.THospitalAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.THospitalAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.THospitalAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.THospitalAppointmentExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TPatientEnquiry
import models.com.hcue.doctors.slick.transactTable.doctor.TPatientEnquiry
import models.com.hcue.doctors.slick.transactTable.doctor.login.DoctorLogin
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalAddress
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalConsultation
import models.com.hcue.doctors.slick.transactTable.hospitals.HospitalExtn
import models.com.hcue.doctors.slick.transactTable.messaging.DiagnosticsRequest
import models.com.hcue.doctors.slick.transactTable.messaging.DiagnosticsResponse
import models.com.hcue.doctors.slick.transactTable.patient.PatientAddress
import models.com.hcue.doctors.slick.transactTable.patient.PatientEmail
import models.com.hcue.doctors.slick.transactTable.patient.PatientOtherDetails
import models.com.hcue.doctors.slick.transactTable.patient.PatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.PatientPhones
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.traits.dbProperties.db
import play.Logger
import models.com.hcue.doctors.slick.transactTable.patient.TEnquirySpeciality
import play.api.db.slick.DB
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import models.com.hcue.doctors.helper.hCueCalenderHelper
import models.com.hcue.doctors.slick.MyPostgresDriver._
import models.com.hcue.doctors.slick.transactTable.patient.EnquirySpeciality
import models.com.hcue.doctor.slick.transactTable.Careteam.SpecialityLkup
import play.api.Play.current

object AddHospitalAppointmentDao {

  val log = Logger.of("application")

  val hospital = Hospital.Hospital
  val hospitalAppointment = HospitalAppointment.HospitalAppointment
  val hospitalAppointmentExtn = HospitalAppointmentExtn.HospitalAppointmentExtn
  val hospitalAddress = HospitalAddress.HospitalAddress
  val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn
  val hospitalschedule = HospitalSchedule.HospitalSchedule
  val hospitalscheduleextn = HospitalScheduleExtn.HospitalScheduleExtn

  val hospitalConsultation = HospitalConsultation.HospitalConsultation
  val doctorConsultation = DoctorConsultations.DoctorConsultations
  val doctor = Doctors.Doctors
  val doctorExtn = DoctorsExtn.DoctorsExtn
  val doctorPhone = DoctorPhones.DoctorsPhones
  val doctorEmail = DoctorEmail.DoctorEmails
  val doctorAddress = DoctorAddress.DoctorsAddress
  val doctorlogin = DoctorLogin.DoctorLogin
  val patients = Patients.patients
  val patientEmail = PatientEmail.patientsEmail
  val patientAddress = PatientAddress.patientsAddress
  val patientOtherDetails = PatientOtherDetails.PatientOtherDetail
  val patientPhones = PatientPhones.patientsPhones
  val patientenquiry = PatientEnquiry.PatientEnquiry
  val patientenquirydtl = PatientEnquiryDtlLkup.patientenquirydtlLkup
  val referalEnquiry = ReferrerEnquiry.ReferrerEnquiry
  val diagnosticResponse = DiagnosticsResponse.DiagnosticsResponse
  val diagnosticRequest = DiagnosticsRequest.DiagnosticsRequest
  val practice = PracticeLkup.PracticeLkup
  val hospitalExtn = HospitalExtn.HospitalExtn
  val enquiry = EnquirySpeciality.EnquirySpeciality
  val lookup = SpecialityLkup.SpecialityLkup

  val s3AccessKey = current.configuration.getString("aws.s3.accessKey").get
  val s3SecretKey = current.configuration.getString("aws.s3.secretKey").get
  val yourAWSCredentials = new com.amazonaws.auth.BasicAWSCredentials(s3AccessKey, s3SecretKey)
  val amazonS3Client = new com.amazonaws.services.s3.AmazonS3Client(yourAWSCredentials)

  def addUpdateAppointments(a: addScheduleAppointmentDetailsObj, addressExtn: Option[TDoctorAddressExtn]): (String, Long, String) = db withSession { implicit session =>

    println("addupdate hospital method")
    var hospitalID: Option[Long] = None
    var hosCD: Option[String] = None
    var brCD: Option[String] = None
    var parentHospitalID: Option[Long] = None
    var patientenquiryid = a.Patientenquiryid

    val appointmentStartTime = (a.StartTime.replaceAll(":", "").toString()).toInt
    val appointmentEndTime = (a.EndTime.replaceAll(":", "").toString()).toInt

    if (patientenquiryid == 0 && a.EnquiryDetails != None) {
      val enquiryDetails = a.EnquiryDetails
      val value: TPatientEnquiry = TPatientEnquiry(0L, a.PatientID, enquiryDetails.get.referalcreated,
        enquiryDetails.get.referalreceived, enquiryDetails.get.referalprocessed, enquiryDetails.get.clinicalnotes, enquiryDetails.get.clinicaldetails,
        enquiryDetails.get.speciality, enquiryDetails.get.subspeciality, enquiryDetails.get.appointmenttypeid, enquiryDetails.get.enquirysourceid,
        None, enquiryDetails.get.crtusr, enquiryDetails.get.crtusrtype, None,
        enquiryDetails.get.isVoicemail, enquiryDetails.get.isLetter, enquiryDetails.get.isEmail, enquiryDetails.get.manageid, enquiryDetails.get.notes, enquiryDetails.get.enquirystatusid,
        enquiryDetails.get.regionid)

      patientenquiryid = (patientenquiry returning patientenquiry.map(_.patientenquiryid)) += value
    } else {

      patientenquiry.filter { x => x.patientenquiryid === a.Patientenquiryid }.map { x =>
        (x.notes)
      }.update(a.Notes)

    }

    if (a.HospitalDetails != None) {
      hospitalID = a.HospitalDetails.get.HospitalID
      hosCD = a.HospitalDetails.get.HospitalCD
      brCD = a.HospitalDetails.get.BranchCD
      parentHospitalID = a.HospitalDetails.get.ParentHospitalID
    }

    if (a.AppointmentStatus == "C" && a.AppointmentID != None) {
      hospitalAppointment.filter { x => x.AppointmentID === a.AppointmentID.get }.map { x => (x.AppointmentStatus, x.UpdtUSR, x.UpdtUSRType) }.update("C", Some(a.USRId), Some(a.USRType))

      hospitalAppointmentExtn.filter { x => x.AppointmentID === a.AppointmentID.get }.map { x => (x.Notes) }.update(a.Notes)

      return ("APPOINTMENT_CANCELLED_SUCCESSFULLY", a.AppointmentID.get, a.AppointmentStatus)
    } else {
      val record: Option[TDoctorAddressExtn] = if (addressExtn != None) { addressExtn } else {
        doctorAddressExtn.filter { x => x.AddressID === a.AddressID }.firstOption
      }
      if (record != None) {
        hospitalID = record.get.HospitalID
        hosCD = record.get.HospitalCode
        parentHospitalID = record.get.ParentHospitalID
      }

      /*      val consultationInfo: List[THospitalSchedule] = hospitalschedule.filter(c => c.AddressConsultID === a.AddressConsultID).list
      var addressConsultIds = consultationInfo.map(f => f.AddressConsultID).toList
      val consultationInfo1: List[THospitalScheduleExtn] = hospitalscheduleextn.filter(c => c.AddressConsultID inSet addressConsultIds).list
      val consultationval: THospitalSchedule = if (consultationInfo.length > 0) {
        var consultSlotval: THospitalSchedule = consultationInfo(0)
        consultSlotval
      } else {
        var consultSlotval: THospitalSchedule = consultationInfo(0)
        consultSlotval
      }

      val consultationAddress: THospitalScheduleExtn = if (consultationInfo1.length > 0) {
        var consultSlot: THospitalScheduleExtn = consultationInfo1(0)
        for (i <- 0 until consultationInfo1.length) {

          val startTime = (consultationInfo1(i).FromTime1.get.replaceAll(":", "").toString()).toInt

          val endTime = (consultationInfo1(i).ToTime1.get.replaceAll(":", "").toString()).toInt

          if (appointmentStartTime >= startTime && appointmentStartTime <= endTime) { consultSlot = consultationInfo1(i) }
        }
        consultSlot
      } else {
        val minPerCase = if (record != None) { record.get.MinPerCase.getOrElse(15) } else { 15 }
        var consultSlot: THospitalScheduleExtn = consultationInfo1(0)
        consultSlot
      }
*/

      val consultationInfo1: List[THospitalScheduleExtn] = if (a.AddressConsultID != None && a.AddressConsultID.get != 0L) {
        hospitalscheduleextn.filter(c => c.AddressConsultID === a.AddressConsultID.get).list
      } else {
        val addressconsultid = hospitalschedule.filter(c => c.SonographerAddressID === a.AddressID && c.DayCD === a.DayCD && c.ScheduleDate === a.ScheduleDate).map { x => x.AddressConsultID }.first
        println("addressconsultid val ::: " + addressconsultid);

        //hospitalscheduleextn.filter(c => c.SonographerAddressID === a.AddressID && c.DayCD === a.DayCD).list
        hospitalscheduleextn.filter(c => c.SonographerAddressID === a.AddressID && c.DayCD === a.DayCD && c.AddressConsultID === addressconsultid).list
      }

      if (consultationInfo1.length > 0) {
        var consultSlot: THospitalScheduleExtn = consultationInfo1(0)

        for (i <- 0 until consultationInfo1.length) {
          val startTime = (consultationInfo1(i).FromTime1.get.replaceAll(":", "").toString()).toInt
          val endTime = (consultationInfo1(i).ToTime1.get.replaceAll(":", "").toString()).toInt
          if (appointmentStartTime >= startTime && appointmentStartTime <= endTime) { consultSlot = consultationInfo1(i) }
        }

        val consultationAddress: THospitalScheduleExtn = consultSlot

        val consultaionStartTime: Calendar = CalenderDateConvertor.setDateTime(a.ScheduleDate, consultationAddress.FromTime1.get)
        var calStartTime: Calendar = CalenderDateConvertor.setDateTime(a.ScheduleDate, a.StartTime)
        val duration: Int = consultationAddress.MinPerCase.getOrElse(15);
        var tokenNumber = if (a.StartTime == a.EndTime) { 0 } else { hcueHelperClass.geTokenNumber(consultaionStartTime.getTime(), calStartTime.getTime(), duration) }

        if (a.AppointmentID != None) {

          var dnacount = 0
          var appointmentcount = 0

          println("AppointmentStatus " + a.AppointmentStatus)
          println("AppointmentID " + a.AppointmentID)

          if (a.AppointmentStatus == "DNA" && a.AppointmentID != None) {

            dnacount = patientenquirydtl.filter { x => (x.patientenquiryid === a.Patientenquiryid && x.enquirystatusid === "DNA") }.map { x => x.enquirystatusid }.list.length

            println("dnacount values " + dnacount)

            if (dnacount == 1) {
              val appointmentEmailerdtl: appointmentEmailerObj = new appointmentEmailerObj(
                a.AppointmentID.get, Some(a.Patientenquiryid), None, true, false, None, None, None, None, None)

              sendPatientDNAMail(appointmentEmailerdtl, Some(0L))

            }

          }
          
          appointmentcount = hospitalAppointment.filter { x => (x.AppointmentID === a.AppointmentID && x.PatientID === a.PatientID) }.map { x => x.AppointmentID }.list.length
          println("appointmentcount values " + appointmentcount)
          
          if(appointmentcount>0)
          {
          hospitalAppointment.filter { x => x.AppointmentID === a.AppointmentID }.map { x =>
            (x.AddressConsultID, x.DayCD, x.ScheduleDate, x.StartTime, x.EndTime, x.EmergencyStartTime, x.EmergencyEndTime, x.DoctorID, x.PatientID,
              x.AppointmentStatus, x.TokenNumber, x.HospitalID, x.HospitalCode, x.BranchCode, x.Patientenquiryid, x.UpdtUSR, x.UpdtUSRType)
          }.update(consultationAddress.AddressConsultID, a.DayCD, a.ScheduleDate, a.StartTime, a.EndTime, a.EmergencyStartTime, a.EmergencyEndTime, Some(a.DoctorID), Some(a.PatientID),
            a.AppointmentStatus, tokenNumber.toString(), hospitalID, hosCD, brCD, Some(patientenquiryid), Some(a.USRId), Some(a.USRType))

          hospitalAppointmentExtn.filter { x => x.AppointmentID === a.AppointmentID }.map { x =>
            (x.AddressConsultID, x.Notes, x.Startscantime, x.Completescantime, x.NoOfArea)
          }.update(consultationAddress.AddressConsultID, a.Notes, a.Startscantime, a.Completescantime, a.NoOfArea)

          if (a.AppointmentStatus == "SC" && a.AppointmentID != None) {
            val costreportaddval = Addcostreport(a.AppointmentID.get, a.AddressConsultID.get).first
          }

          return ("APPOINTMENT_UPDATED_SUCCESS", a.AppointmentID.get, a.AppointmentStatus)
          }else{
          return ("UNABLE TO UPDATE THIS APPOINTMENT", 0, null)  
          //return ("UNABLE TO UPDATE THIS APPOINTMENT", a.AppointmentID.get, a.AppointmentStatus)  
          }

        } else {
          val appointmentObj: addScheduleAppointmentDetailsObj = new addScheduleAppointmentDetailsObj(None, consultationAddress.SonographerAddressID, Some(consultationAddress.AddressConsultID), a.DayCD, a.ScheduleDate, RequestResponseHelper.properTime(a.StartTime), RequestResponseHelper.properTime(a.EndTime),
            RequestResponseHelper.properTime(a.EmergencyStartTime), RequestResponseHelper.properTime(a.EmergencyEndTime), a.DoctorID, a.PatientID, patientenquiryid,
            a.AppointmentStatus, a.HospitalDetails, None, None, a.Notes, a.USRId, a.USRType, a.EnquiryDetails, None)

          val currentBookedAppointmentList: Array[buildScheduleAppointmentList] = new Array[buildScheduleAppointmentList](1)
          currentBookedAppointmentList(0) = new buildScheduleAppointmentList(0, 0, consultationAddress.AddressConsultID, a.DayCD, a.ScheduleDate,
            RequestResponseHelper.properTime(a.StartTime), RequestResponseHelper.properTime(a.EndTime), RequestResponseHelper.properTime(a.EmergencyStartTime), RequestResponseHelper.properTime(a.EmergencyEndTime), Some(a.DoctorID), Some(a.PatientID), Some(patientenquiryid), a.AppointmentStatus, tokenNumber.toString(), a.Notes, a.Startscantime, a.Completescantime)

          var isMRI: String = "US";

          val outcome = enquiry.filter { x_ => ((x_.PatientEnquiryId === patientenquiryid) && (x_.SpecialityDescription.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption
          println(outcome + "AAAAAAAAAAAAAAAA")
          if (outcome != None) {

            val sub = lookup.filter { x => ((x.DoctorSpecialityID === (outcome.get.SubSpecialities.toList)(0)._2) && (x.DoctorSpecialityDesc.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption.get

            isMRI = "MRI"
          }

          /*        val diagnosisType = enquiry.filter { x_ =>  ((x_.PatientEnquiryId === input.patientenquiryid )&& (x_.SpecialityDescription like ("%" +"MRI" + "%")))}.firstOption  

 if(diagnosisType != None ){
  
  val outcome = lookup.filter { x => ((x.DoctorSpecialityID === (diagnosisType.get.SubSpecialities.toList)(0)._2)&&(x.DoctorSpecialityDesc like("%" +"MRI" + "%")))}.firstOption.get
   
         */

          /*val pattern = new Regex("MRI\\d+")
                  if(a.EnquiryDetails !=None){
                val str = (a.EnquiryDetails.get.speciality.getOrElse("")) 
                val outcome = ((pattern findAllIn str).mkString(","))
          
                if (outcome.equals(a.EnquiryDetails.get.speciality)){
                  
                 isMRI = "MRI"
                }}*/

          val appointmentId = insertAppointmentRecord(currentBookedAppointmentList, hospitalID, hosCD, brCD, parentHospitalID, Some(isMRI), a.USRId, a.USRType)

          return ("APPOINTMENT_ADDED_SUCCESS", if (appointmentId.length > 0) { appointmentId(0) } else { 0 }, a.AppointmentStatus)
        }

      } else {
        return ("NO_TIME_SLOT_FOR_THE_DOCTOR", 0, null)
      }
    }
  }

  def Addcostreport(AppointmnetID: Long, AddressConsultID: Long) =

    sql"SELECT * FROM addcostreport('#$AppointmnetID','#$AddressConsultID')".as[(Long)]

  def insertAppointmentRecord(bookingList: Array[buildScheduleAppointmentList], hospitalID: Option[Long], hospitalCD: Option[String], branchCD: Option[String], ParentHospitalID: Option[Long], isMRI: Option[String], USRId: Long, USRType: String): List[Long] = db withSession { implicit session =>
    println("ADDCOSTREPORT ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    var appointmentList: List[Long] = Nil

    var parentDoctorID: Long = 0

    for (i <- 0 until bookingList.length) {
      val autoGenDoctorID = (hospitalAppointment.map { x =>
        (x.AddressConsultID, x.DayCD, x.ScheduleDate, x.StartTime, x.EndTime, x.EmergencyStartTime, x.EmergencyEndTime, x.DoctorID, x.PatientID,
          x.AppointmentStatus, x.TokenNumber, x.ParentAppointmentID, x.HospitalID, x.HospitalCode, x.BranchCode, x.Patientenquiryid, x.CrtUSR, x.CrtUSRType, x.dienestype)
      } returning hospitalAppointment.map(_.AppointmentID)) +=
        (bookingList(i).AddressConsultID, bookingList(i).DayCD, bookingList(i).ScheduleDate,
          bookingList(i).StartTime, bookingList(i).EndTime, bookingList(i).EmergencyStartTime, bookingList(i).EmergencyEndTime, bookingList(i).DoctorID, bookingList(i).PatientID,
          bookingList(i).AppointmentStatus, bookingList(i).TokenNumber, parentDoctorID, hospitalID, hospitalCD, branchCD, bookingList(i).Patientenquiryid, USRId, USRType, isMRI)

      var result: THospitalAppointmentExtn = new THospitalAppointmentExtn(autoGenDoctorID, bookingList(i).AddressConsultID,
        bookingList(i).DayCD, bookingList(i).ScheduleDate,
        bookingList(i).AppointmentStatus, bookingList(i).TokenNumber, bookingList(i).Notes, bookingList(i).Startscantime, bookingList(i).Completescantime,
        ParentHospitalID, None, None, None)

      hospitalAppointmentExtn.filter(c => c.AppointmentID === autoGenDoctorID).update(result)

      if (bookingList(i).AppointmentStatus.equals("B")) {
        parentDoctorID = autoGenDoctorID
        appointmentList ::= parentDoctorID
      }

    }

    appointmentList
  }

  def updateAppointmentRecord(bookingList: buildScheduleAppointmentList, status: String, parentAppointmentID: Long, hospitalID: Option[Long], hospitalCD: Option[String], branchCD: Option[String], ParentHospitalID: Option[Long], crtUsr: Long, crtUsrType: String, updtUsr: Option[Long], updtUsrType: Option[String]): Long = db withSession { implicit session =>

    var appointmentId = bookingList.AppointmentID

    if (status == "B") {
      /*hospitalAppointment.filter { x => x.AppointmentID === bookingList.AppointmentID}.map { x =>
        (x.AddressConsultID, x.DayCD, x.ScheduleDate, x.StartTime, x.EndTime,x.EmergencyStartTime, x.EmergencyEndTime,x.DoctorID, x.PatientID,
          x.AppointmentStatus, x.TokenNumber, x.ParentAppointmentID, x.HospitalID, x.HospitalCode, x.BranchCode, x.CrtUSR, x.CrtUSRType, x.UpdtUSR, x.UpdtUSRType)
      }.update(bookingList.AddressConsultID, bookingList.DayCD, bookingList.ScheduleDate,
          bookingList.StartTime, bookingList.EndTime,bookingList.EmergencyStartTime, bookingList.EmergencyEndTime,bookingList.DoctorID, bookingList.PatientID, bookingList.VisitUserTypeID,
          bookingList.AppointmentStatus, bookingList.TokenNumber, parentAppointmentID, hospitalID, hospitalCD, branchCD, crtUsr, crtUsrType, updtUsr, updtUsrType)
     */

      hospitalAppointment.filter { x => x.AppointmentID === bookingList.AppointmentID }.map { x =>
        (x.AddressConsultID, x.DayCD, x.ScheduleDate, x.StartTime, x.EndTime, x.EmergencyStartTime, x.EmergencyEndTime, x.DoctorID, x.PatientID,
          x.AppointmentStatus, x.TokenNumber, x.ParentAppointmentID, x.HospitalID, x.HospitalCode, x.BranchCode, x.CrtUSR, x.CrtUSRType, x.UpdtUSR, x.UpdtUSRType)
      }.update(bookingList.AddressConsultID, bookingList.DayCD, bookingList.ScheduleDate, bookingList.StartTime, bookingList.EndTime, bookingList.EmergencyStartTime,
        bookingList.EmergencyEndTime, bookingList.DoctorID, bookingList.PatientID, bookingList.AppointmentStatus, bookingList.TokenNumber, parentAppointmentID, hospitalID, hospitalCD, branchCD, crtUsr, crtUsrType, updtUsr, updtUsrType)

      if (ParentHospitalID != None) {
        hospitalAppointmentExtn.filter(c => c.AppointmentID === bookingList.AppointmentID).map { x => x.ParentHospitalID }.update(ParentHospitalID)
      }
    } else if (status == "C") {
      hospitalAppointment.filter { x => x.AppointmentID === bookingList.AppointmentID }.map { x => x.AppointmentStatus }.update("C")
    } else {
      val autoGenerateID = (hospitalAppointment.map { x =>
        (x.AddressConsultID, x.DayCD, x.ScheduleDate, x.StartTime, x.EndTime, x.EmergencyStartTime, x.EmergencyEndTime, x.DoctorID, x.PatientID,
          x.AppointmentStatus, x.TokenNumber, x.ParentAppointmentID, x.HospitalID, x.HospitalCode, x.BranchCode, x.CrtUSR, x.CrtUSRType, x.UpdtUSR, x.UpdtUSRType)
      } returning hospitalAppointment.map(_.AppointmentID)) += (bookingList.AddressConsultID, bookingList.DayCD, bookingList.ScheduleDate, bookingList.StartTime, bookingList.EndTime, bookingList.EmergencyStartTime,
        bookingList.EmergencyEndTime, bookingList.DoctorID, bookingList.PatientID, bookingList.AppointmentStatus, bookingList.TokenNumber, parentAppointmentID, hospitalID, hospitalCD, branchCD, crtUsr, crtUsrType, updtUsr, updtUsrType)
      if (ParentHospitalID != None) {
        hospitalAppointmentExtn.filter(c => c.AppointmentID === autoGenerateID).map { x => x.ParentHospitalID }.update(ParentHospitalID)
      }
      appointmentId = autoGenerateID
    }
    appointmentId
  }

  def updateAppointments(a: Array[AppointmentStatusUpdateObj]): appointmentresponseObj = db withSession { implicit session =>

    val record: Array[AppointmentStatusUpdateObj] = a

    for (i <- 0 until record.length) {

      hospitalAppointmentExtn.filter(x => x.AppointmentID === record(i).AppointmentID).map { x => (x.AppointmentID, x.AppointmentStatus, x.Startscantime, x.Completescantime, x.Notes) }
        .update((record(i).AppointmentID, record(i).AppointmentStatus, record(i).ScanStartTime, record(i).ScanEndtime, record(i).Notes))

      if (record(i).AppointmentStatus == "SC" && record(i).AppointmentID != 0) {
        val costreportaddval = Addcostreport(record(i).AppointmentID, 0).first
      }

    }

    return new appointmentresponseObj("Success", 200)

  }

  def getHospitalAppointments(a: getHospitalAppointmentDetailsObj): JsValue = db withSession { implicit session =>

    Json.parse(getHospitalAppointments(a.StartDate, a.EndDate, a.HospitalCD.getOrElse(""), a.ParentHospitalID.getOrElse(0L), a.HospitalID.getOrElse(0L), a.DoctorID.getOrElse(0L), a.AddressID.getOrElse(0L)).firstOption.get)

  }

  def getHospitalAppointments(StartDate: Date, EndDate: Date, HospitalCD: String, ParentHospitalID: Long, HospitalID: Long, DoctorID: Long, AddressID: Long) =
    sql"SELECT * FROM gethospitalappointment_list('#$StartDate','#$EndDate','#$HospitalCD','#$ParentHospitalID','#$HospitalID','#$DoctorID','#$AddressID')".as[(String)]

  def listCalenderComponentDtls(a: listCalenderComponentRequestObj): listCalenderComponentResponseObj = db withSession { implicit session =>

    //var hospitalConsultationInfo: List[THospitalConsultation] = Nil
    var doctorConsultationInfo: List[THospitalScheduleExtn] = Nil
    var appointmentStartDate: Calendar = CalenderDateConvertor.getISDTimeZoneDate()
    appointmentStartDate.setTime(a.startDate)

    val difference = Math.abs(a.endDate.getTime() - a.startDate.getTime())
    val numberofDays = (Math.round(difference / (1000 * 60 * 60 * 24))) + 1
    var totalBookedAppointments: List[(THospitalAppointment, THospitalAppointmentExtn)] = Nil

    val totalBookedAppointmentQuery = if ((a.hospitalID != None && a.hospitalID.get != 0) && (a.addressID != None && a.addressID.get != 0)) {
      doctorConsultationInfo = hospitalscheduleextn.filter { x => x.SonographerAddressID === a.addressID }.list
      val addressConsultId = doctorConsultationInfo.map { x => x.AddressConsultID }
      for {
        (app, appExtn) <- hospitalAppointment innerJoin hospitalAppointmentExtn on (_.AppointmentID === _.AppointmentID)
        if (app.AddressConsultID inSet addressConsultId)
      } yield (app, appExtn)
    } else if (a.hospitalID != None && a.hospitalID.get != 0) {
      for {
        (app, appExtn) <- hospitalAppointment innerJoin hospitalAppointmentExtn on (_.AppointmentID === _.AppointmentID)
        if app.HospitalID === a.hospitalID.get
      } yield (app, appExtn)
    } else {
      doctorConsultationInfo = hospitalscheduleextn.filter { x => x.SonographerAddressID === a.addressID }.list
      val addressConsultId = doctorConsultationInfo.map { x => x.AddressConsultID }
      for {
        (app, appExtn) <- hospitalAppointment innerJoin hospitalAppointmentExtn on (_.AppointmentID === _.AppointmentID)
        if (app.AddressConsultID inSet addressConsultId)
      } yield (app, appExtn)
    }

    totalBookedAppointments = totalBookedAppointmentQuery.filter(f => f._1.ScheduleDate >= a.startDate && f._1.ScheduleDate <= a.endDate && (f._1.AppointmentStatus =!= "C" && f._1.AppointmentStatus =!= "LC")).sortBy { x => (x._1.ScheduleDate, x._1.StartTime) }.list

    println(totalBookedAppointments);

    if (a.hospitalID != None && a.hospitalID.get != 0) {
      //hospitalConsultationInfo = hospitalConsultation.filter { x => x.HospitalID === a.hospitalID.get }.list
      if (a.addressID == None || (a.addressID != None && a.addressID.get == 0)) {
        val addressConsultIds = totalBookedAppointments.filter(p => p._1.DoctorID != None).map(f => f._1.AddressConsultID).toList.distinct
        doctorConsultationInfo = hospitalscheduleextn.filter { x => x.AddressConsultID inSet addressConsultIds }.list
      }
    }

    var parentEventAppiontments: List[THospitalAppointment] = Nil
    var linkedEventApp = totalBookedAppointments.filter { x => x._1.AppointmentStatus == "EVENT" || x._1.AppointmentStatus == "IMPEVENT" || x._1.AppointmentStatus == "L" }.toList
    if (linkedEventApp.length > 0) {
      val eventAppointmentIds = linkedEventApp.map { x => x._1.ParentAppointmentID }.toList.distinct
      parentEventAppiontments = hospitalAppointment.filter { x => (x.ParentAppointmentID inSet eventAppointmentIds) && x.AppointmentStatus =!= "CLEVENT" }.list
    }

    var eventAppointmentList: List[(Long, Long, Long, java.sql.Date)] = Nil
    for (i <- 0 until parentEventAppiontments.length) {
      val seqNumber = ((parentEventAppiontments(i).ScheduleDate.toString().replaceAll("-", "").toString()).toInt + (parentEventAppiontments(i).StartTime.replaceAll(":", "").toString()).toInt).toLong
      eventAppointmentList = eventAppointmentList ::: List((seqNumber, parentEventAppiontments(i).AppointmentID, parentEventAppiontments(i).ParentAppointmentID, parentEventAppiontments(i).ScheduleDate))
    }

    var consultDtls: List[consultationDtlObj] = Nil
    for (k <- 0 until doctorConsultationInfo.length) {
      consultDtls = consultDtls ::: List(new consultationDtlObj(doctorConsultationInfo(k).AddressConsultID, Some(doctorConsultationInfo(k).SonographerAddressID), None))
    }
    /*   for (k <- 0 until hospitalConsultationInfo.length) {
      consultDtls = consultDtls ::: List(new consultationDtlObj(hospitalConsultationInfo(k).AddressConsultID, None, Some(hospitalConsultationInfo(k).HospitalID)))
    }*/

    val patientIDs = totalBookedAppointments.map { x => x._1.PatientID.getOrElse(0.toLong) }.toList
    val doctorIds = totalBookedAppointments.map { x => x._1.DoctorID.getOrElse(0.toLong) }.toList
    val patientInfoQuery = (for {
      (pat, patDtl) <- patients innerJoin patientOtherDetails on (_.PatientID === _.PatientID)
      if pat.PatientID inSet patientIDs
    } yield (pat, patDtl)).list

    val patientPhoneQuery = patientPhones.filter { x => x.PatientID inSet patientIDs }.list

    val doctorInfoQuery = (for {
      (doc, docExtn) <- doctor innerJoin doctorExtn on (_.DoctorID === _.DoctorID)
      if doc.DoctorID inSet doctorIds
    } yield (doc, docExtn)).list

    var hospitalId: Option[Long] = a.hospitalID
    val resultSet: Array[calenderObj] = new Array[calenderObj](numberofDays)
    var appointmentIds: List[Long] = Nil
    var eventsCount = 0
    for (i <- 0 until numberofDays) {

      val consultDt = new java.sql.Date(appointmentStartDate.getTimeInMillis())
      val appointmentList = totalBookedAppointments.filter { x => (x._1.ScheduleDate.toString() == consultDt.toString()) }.toList
      var slotList: List[slotListObj] = Nil

      for (j <- 0 until appointmentList.length) {
        var patientInfo: Option[patientPersonalInfo] = None
        var doctorInfo: Option[doctorPersonalInfo] = None
        if (appointmentList(j)._1.PatientID != None) {
          val patientDtl = patientInfoQuery.find { x => x._1.PatientID == appointmentList(j)._1.PatientID.get }
          val phNumber = patientPhoneQuery.find { x => x.PatientID == appointmentList(j)._1.PatientID.get && x.PhType == "M" }
          val landLine = patientPhoneQuery.find { x => x.PatientID == appointmentList(j)._1.PatientID.get && x.PhType == "L" }

          val paitentPhone = if (phNumber != None) {
            phNumber.get.PhNumber.toString()
          } else if (landLine != None) {
            landLine.get.PhStdCD.getOrElse("") + " " + landLine.get.PhNumber.toString()
          } else {
            ""
          }

          if (patientDtl != None) {
            var patDispID: Option[String] = None
            var FamilyDisplayID: Option[String] = None
            var patLastVstedStr: String = ""
            var patLastVsted: Option[java.sql.Date] = None
            if (patientDtl.get._2.PatientDisplayID != None) {
              if (a.hospitalCD != None) {
                patDispID = Some(patientDtl.get._2.PatientDisplayID.get.get(a.hospitalCD.getOrElse("")).getOrElse(""))
                patLastVstedStr = patientDtl.get._2.PatientDisplayID.get.get(a.hospitalCD.getOrElse("") + "_LASTVISITED").getOrElse("")
                if (patLastVstedStr != "") {
                  val format: SimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy")
                  val parsed = format.parse(patLastVstedStr)
                  patLastVsted = Some(new java.sql.Date(parsed.getTime()))
                }
              } else {
                patDispID = Some(patientDtl.get._2.PatientDisplayID.get.get("DOCTORID" + a.doctorID.toString).getOrElse(""))
                patLastVstedStr = patientDtl.get._2.PatientDisplayID.get.get("DOCTORID" + a.doctorID.toString + "_LASTVISITED").getOrElse("")
                if (patLastVstedStr != "") {
                  val format: SimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy")
                  val parsed = format.parse(patLastVstedStr)
                  patLastVsted = Some(new java.sql.Date(parsed.getTime()))
                }
              }
            }
            if (patientDtl.get._2.FamilyDisplayID != None) {
              val fullPatFamID = patientDtl.get._2.FamilyDisplayID.get.get(a.hospitalCD.getOrElse("")).getOrElse("")
              if (fullPatFamID != "") {
                val patFamIDArry = fullPatFamID.split("-")
                if (patFamIDArry.length == 3)
                  FamilyDisplayID = Some(patFamIDArry(1) + "-" + patFamIDArry(2))
              }
            }
            patientInfo = Some(new patientPersonalInfo(patientDtl.get._1.PatientID, Some(patientDtl.get._1.Title.getOrElse("")), patientDtl.get._1.FullName, Some(patientDtl.get._1.ProfileImage.getOrElse("")), patDispID, FamilyDisplayID, patLastVsted,
              patientDtl.get._1.Age, patientDtl.get._1.Gender, RequestResponseHelper.ageCalculation(patientDtl.get._1.DOB), Some(paitentPhone)))
          }
        }

        if (appointmentList(j)._1.DoctorID != None) {
          val doctorDtl = doctorInfoQuery.find { x => x._1.DoctorID == appointmentList(j)._1.DoctorID.get }
          if (doctorDtl != None) {
            doctorInfo = Some(new doctorPersonalInfo(doctorDtl.get._1.FullName, Some(doctorDtl.get._2.ProfilImage.getOrElse("")), doctorDtl.get._1.SpecialityCD))
          } else {
            doctorInfo = Some(new doctorPersonalInfo("", Some(""), None))
          }
        } else {
          doctorInfo = Some(new doctorPersonalInfo("", Some(""), None))
        }

        val seqNo = ((appointmentList(j)._1.ScheduleDate.toString().replaceAll("-", "").toString()).toInt + (appointmentList(j)._1.StartTime.replaceAll(":", "").toString()).toInt).toLong
        val consultationIdDtls = consultDtls.find { x => x.addressConsultId == appointmentList(j)._1.AddressConsultID }
        val addressId = if (consultationIdDtls != None) { consultationIdDtls.get.addressId } else { None }

        var startDate = appointmentList(j)._1.ScheduleDate
        var endDate = appointmentList(j)._1.ScheduleDate
        var allDayEvent: Option[String] = None
        if (appointmentList(j)._1.StartTime == "00:00" && appointmentList(j)._1.EndTime == "23:59") {
          allDayEvent = Some("Y")
          val events = eventAppointmentList.filter(p => p._3 == appointmentList(j)._1.ParentAppointmentID).sortBy(f => f._1)
          if (events.length > 0) {
            startDate = events(0)._4
            endDate = events(events.length - 1)._4
          }
        }

        var appointmentId = appointmentList(j)._1.AppointmentID
        var appStatus = appointmentList(j)._1.AppointmentStatus
        if (appointmentList(j)._1.AppointmentStatus == "L") {
          val parentAppointmentDtl = parentEventAppiontments.find { x => x.AppointmentID == appointmentList(j)._1.ParentAppointmentID }
          if (parentAppointmentDtl != None) {
            appointmentId = parentAppointmentDtl.get.AppointmentID
            appStatus = parentAppointmentDtl.get.AppointmentStatus
          }
        }

        val eventName: Option[String] = None

        if (!appointmentIds.contains(appointmentId)) {
          slotList = slotList ::: List(new slotListObj(
            addressId,
            appointmentList(j)._1.HospitalID,
            if (appointmentList(j)._1.DoctorID != None) { appointmentList(j)._1.DoctorID } else { Some(0L) },
            doctorInfo,
            seqNo,
            if (appointmentList(j)._1.EmergencyStartTime == "00:00") appointmentList(j)._1.StartTime else appointmentList(j)._1.EmergencyStartTime,
            if (appointmentList(j)._1.EmergencyEndTime == "00:00") appointmentList(j)._1.EndTime else appointmentList(j)._1.EmergencyEndTime,
            allDayEvent, appointmentList(j)._1.ScheduleDate,
            startDate, endDate, appointmentList(j)._1.DayCD, appointmentId, appStatus, eventName, None, None, None, "", appointmentList(j)._1.TokenNumber, patientInfo))
        }

        appointmentIds = appointmentIds ::: List(appointmentId)
      }
      val addressconsultid = hospitalschedule.filter(c => c.SonographerAddressID === a.addressID && c.ScheduleDate === a.startDate && c.HospitalID === a.hospitalID && c.SonographerDoctorID === a.doctorID).map { x => x.AddressConsultID }.first
      val emergencystarttime = hospitalscheduleextn.filter { x => x.AddressConsultID === addressconsultid }.map { x => x.FromTime2 }.first
      val emergencyendtime = hospitalscheduleextn.filter { x => x.AddressConsultID === addressconsultid }.map { x => x.ToTime2 }.first
      val minpercaseslot = hospitalscheduleextn.filter { x => x.AddressConsultID === addressconsultid }.map { x => x.MinPerCase }.first
      resultSet(i) = new calenderObj(consultDt, addressconsultid, emergencystarttime.getOrElse("00:00"), emergencyendtime.getOrElse("00:00"), minpercaseslot.getOrElse(0), slotList.toArray)
      appointmentStartDate.add(Calendar.DATE, 1)
    }

    // Appointment Counts
    val bookedAppointments = totalBookedAppointments.filter { x => x._1.AppointmentStatus == "B" }.length
    val checkedInAppointments = totalBookedAppointments.filter { x => x._1.AppointmentStatus == "I" }.length
    val startedAppointments = totalBookedAppointments.filter { x => x._1.AppointmentStatus == "S" }.length
    val completedAppointments = totalBookedAppointments.filter { x => x._1.AppointmentStatus == "E" }.length

    val appointmentCounts = new appointmentsCountResponseObj(bookedAppointments, checkedInAppointments, startedAppointments, completedAppointments, eventsCount)
    new listCalenderComponentResponseObj(None, hospitalId, Some(appointmentCounts), resultSet)
  }

  def getHTML(emailtype: String) = sql"select defaulthtml from emailtemplate where emailtype = '#$emailtype'".as[(String)]

  def sendAppointmentMail(input: appointmentEmailerObj): String = db withSession { implicit session =>
    println("SENTMAILINPUT################################")
    var starttimeval: Option[String] = None
    var doctorid: Option[Long] = None
    var shudledate: Option[java.sql.Date] = None
    var patientid: Option[Long] = None
    var patientenquiryid: Option[Long] = None
    var hospitalid: Option[Long] = None

    val obj = if (input.patientenquiryid.getOrElse(0L) > 0) {
      hospitalAppointment.filter { x => x.Patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first

    } else {
      hospitalAppointment.filter { x => x.AppointmentID === input.appointmentid }.first
    }

    if (input.appointmentid == 0L) {
      starttimeval = input.appointmenttime
      doctorid = input.doctorid
      shudledate = input.appointmentdate
      patientenquiryid = input.patientenquiryid
      hospitalid = input.appointmenthospitalid
    } else {
      starttimeval = if (obj.StartTime != "00:00") Some(obj.StartTime) else Some(obj.EmergencyStartTime)
      doctorid = obj.DoctorID
      shudledate = Some(obj.ScheduleDate)
      patientid = obj.PatientID
      patientenquiryid = obj.Patientenquiryid
      hospitalid = obj.HospitalID
    }

    val context = new VelocityContext();
    val dateFormat = CalenderDateConvertor.getDateFormatter(shudledate.get)
    var contentSubject: String = "Appointment Confirmation";
    var doctorName = ""
    var doctortitle = ""

    //val dateObj = new SimpleDateFormat("H:mm").parse(a.StartTime)  // sabari comment this line

    val dateObj = new SimpleDateFormat("H:mm").parse(starttimeval.get)
    // val TwelveHrFormat = new SimpleDateFormat("hh:mm a").format(dateObj)
    val TwentyHrFormat = new SimpleDateFormat("HH:mm a").format(dateObj)
    val consultDt = DateFormat.getDateInstance().format(shudledate.get).toString()
    val format: SimpleDateFormat = new SimpleDateFormat("dd MMM,yyyy");

    val diagnosisType = hospitalAppointment.filter { x => x.AppointmentID === input.appointmentid || x.Patientenquiryid === input.patientenquiryid }.map { x => x.dienestype }.first.getOrElse("")

    if (diagnosisType.equalsIgnoreCase("MRI")) {

      var html = getHTML("APPCOMFIRMATIONMRI").first

      val ConsultationDt1: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
      val currentDateFormat = CalenderDateConvertor.getDateFormatter(ConsultationDt1)
      if (obj.DoctorID != None && obj.DoctorID != Some(0L)) {
        doctorName = doctor.filter { x => x.DoctorID === obj.DoctorID }.map { x => x.FullName }.firstOption.getOrElse("")
        doctortitle = doctor.filter { x => x.DoctorID === obj.DoctorID }.map { x => x.Title.getOrElse("") }.first
      }
      val patientEnq = patientenquiry.filter { x => x.patientenquiryid === patientenquiryid }.map { x => (x.appointmenttypeid, x.clinicalnotes, x.patientid) }.firstOption
      patientid = Some(patientEnq.get._3)
      val patientDetails = patients.filter { x => x.PatientID === patientid.get }.map { x => (x.FirstName, x.LastName, x.Title) }.firstOption
      val patientAddres = patientAddress.filter { x => x.PatientID === patientid.get }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.PostCode) }.firstOption
      val hospAddress = hospitalAddress.filter { x => x.HospitalID === hospitalid.get }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.HospitalName, x.Latitude, x.Longitude, x.PostCode) }.firstOption
      val patEmail = patientEmail.filter { x => x.PatientID === patientid.get }.map { x => x.EmailID }.firstOption
      val abouthospital = hospital.filter { x => x.HospitalID === hospitalid.get }.map { x => x.About }.firstOption
      val AppointmentspecialityList = patientspecialityinfo(patientenquiryid.get).list

      val formatDate = new SimpleDateFormat("dd-MM-yyyy")
      val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())

      var emailer = html.replaceAllLiterally("{{created}}", createdDate)

      emailer = emailer.replaceAllLiterally("{{doctorName}}", doctorName)
      emailer = emailer.replaceAllLiterally("{{doctor_title}}", doctortitle)
      emailer = emailer.replaceAllLiterally("{{patient_forename}}", patientDetails.get._1)
      emailer = emailer.replaceAllLiterally("{{patient_surname}}", patientDetails.get._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{patient_title}}", patientDetails.get._3.getOrElse(""))
      var patientAddressdtl = "";
      if (patientAddres.get._1 != None) {
        patientAddressdtl += patientAddres.get._1.get;
      }
      if (patientAddres.get._2 != None) {
        patientAddressdtl += "<br>" + patientAddres.get._2.get;
      }
      if (patientAddres.get._3 != None) {
        patientAddressdtl += "<br>" + patientAddres.get._3.get;
      }
      /* Removed country - Krishnan
     * if(patientAddres.get._4 != None) {
      patientAddressdtl += "<br>" + patientAddres.get._4;
    }*/
      if (patientAddres.get._5 != None) {
        patientAddressdtl += "<br>" + patientAddres.get._5.get;
      }
      var aptcenteraddress = "";
      if (hospAddress.get._1 != None) {
        aptcenteraddress += hospAddress.get._1.get;
      }
      if (hospAddress.get._2 != None) {
        aptcenteraddress += "<br>" + hospAddress.get._2.get;
      }
      if (hospAddress.get._3 != None) {
        aptcenteraddress += "<br>" + hospAddress.get._3.get;
      }
      if (hospAddress.get._8 != None) {
        aptcenteraddress += "<br>" + hospAddress.get._8.get;
      }
      /* Removed country - Krishnan
     * if(hospAddress.get._4 != None) {
      aptcenteraddress += ", " + hospAddress.get._4.get;
    }*/
      emailer = emailer.replaceAllLiterally("{{patient_address}}", patientAddressdtl)
      emailer = emailer.replaceAllLiterally("{{patient_postcode}}", patientAddres.get._5.getOrElse(""))

      //    emailer = emailer.replaceAllLiterally("{{patient_address2}}", patientAddres.get._2.getOrElse(""))
      //    emailer = emailer.replaceAllLiterally("{{patient_address3}}", patientAddres.get._3.getOrElse(""))
      //    emailer = emailer.replaceAllLiterally("{{patient_address4}}", patientAddres.get._4)
      //    emailer = emailer.replaceAllLiterally("{{patient_address5}}", patientAddres.get._5.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{appointment_date}}", dateFormat.format(shudledate.get))
      emailer = emailer.replaceAllLiterally("{{appointment_start}}", TwentyHrFormat)
      emailer = emailer.replaceAllLiterally("{{appointment_centre_name}}", hospAddress.get._5.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{appointment_centre_address}}", aptcenteraddress)
      //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address2}}", hospAddress.get._2.getOrElse(""))
      //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address3}}", hospAddress.get._3.getOrElse(""))
      //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address4}}", hospAddress.get._4.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{appointment_type_name}}", patientEnq.get._1.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{speciality_notes}}", AppointmentspecialityList(0)._1)
      emailer = emailer.replaceAllLiterally("{{Latitude}}", hospAddress.get._6.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{Longitude}}", hospAddress.get._7.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{speciality_details}}", AppointmentspecialityList(0)._1)
      emailer = emailer.replaceAllLiterally("{{speciality}}", AppointmentspecialityList(0)._2)
      emailer = emailer.replaceAllLiterally("{{clinicDescription}}", abouthospital.get.getOrElse(""))

      val templateCloudfrontURL = current.configuration.getString("url.cloudfront.templates").get
      
      val fileLocationURL = templateCloudfrontURL+"/mri_scan_template/MRI_Safety_Questionnaire.pdf"

      var pdfFileName = (fileLocationURL.split("/"))((fileLocationURL.split("/")).length - 1)

      val futureString: Future[String] = scala.concurrent.Future {
        println("Patient Email : " + patEmail)
        if (patEmail != None && input.sendemail == true)
          // hcueCommunication.sendHcueEmail(emailer, patEmail.getOrElse(""), Some(hcueSmtp2EmailCredentials.HCUE_EMAIL_CC_EMAIL), None, contentSubject, "Y", "Y",Some(fileLocationURL))
          hcueCommunication.test(emailer, patEmail.getOrElse(""), None, None, contentSubject, fileLocationURL, None, None)

        "Success"
      }

      emailer

    } else {
      println("Inside Else APPCOMFIRMATION")
      var html = getHTML("APPCOMFIRMATION").first

      val ConsultationDt1: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
      val currentDateFormat = CalenderDateConvertor.getDateFormatter(ConsultationDt1)
      if (obj.DoctorID != None && obj.DoctorID != Some(0L)) {
        doctorName = doctor.filter { x => x.DoctorID === obj.DoctorID }.map { x => x.FullName }.firstOption.getOrElse("")
        doctortitle = doctor.filter { x => x.DoctorID === obj.DoctorID }.map { x => x.Title.getOrElse("") }.first
      }
      val patientEnq = patientenquiry.filter { x => x.patientenquiryid === patientenquiryid }.map { x => (x.appointmenttypeid, x.clinicalnotes, x.patientid) }.firstOption
      patientid = Some(patientEnq.get._3)
      val patientDetails = patients.filter { x => x.PatientID === patientid.get }.map { x => (x.FirstName, x.LastName, x.Title) }.firstOption
      val patientAddres = patientAddress.filter { x => x.PatientID === patientid.get }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.PostCode) }.firstOption
      val hospAddress = hospitalAddress.filter { x => x.HospitalID === hospitalid.get }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.HospitalName, x.Latitude, x.Longitude, x.PostCode) }.firstOption
      val patEmail = patientEmail.filter { x => x.PatientID === patientid.get }.map { x => x.EmailID }.firstOption
      val abouthospital = hospital.filter { x => x.HospitalID === hospitalid.get }.map { x => x.About }.firstOption
      val AppointmentspecialityList = patientspecialityinfo(patientenquiryid.get).list

      val formatDate = new SimpleDateFormat("dd-MM-yyyy")

      val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())

      println("Formatted Date " + createdDate)

      println("AppointmentspecialityList(0)._1" + AppointmentspecialityList(0)._1)

      println("AppointmentspecialityList(0)._1" + AppointmentspecialityList(0)._2)

      var emailer = html.replaceAllLiterally("{{created}}", createdDate)

      emailer = emailer.replaceAllLiterally("{{doctorName}}", doctorName)
      emailer = emailer.replaceAllLiterally("{{doctor_title}}", doctortitle)
      emailer = emailer.replaceAllLiterally("{{patient_forename}}", patientDetails.get._1)
      emailer = emailer.replaceAllLiterally("{{patient_surname}}", patientDetails.get._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{patient_title}}", patientDetails.get._3.getOrElse(""))
      var patientAddressdtl = "";
      if (patientAddres.get._1 != None) {
        patientAddressdtl += patientAddres.get._1.get;
      }
      if (patientAddres.get._2 != None) {
        patientAddressdtl += "<br>" + patientAddres.get._2.get;
      }
      if (patientAddres.get._3 != None) {
        patientAddressdtl += "<br>" + patientAddres.get._3.get;
      }
      /* Removed country - Krishnan
     * if(patientAddres.get._4 != None) {
      patientAddressdtl += "<br>" + patientAddres.get._4;
    }*/
      if (patientAddres.get._5 != None) {
        patientAddressdtl += "<br>" + patientAddres.get._5.get;
      }
      var aptcenteraddress = "";
      if (hospAddress.get._1 != None) {
        aptcenteraddress += hospAddress.get._1.get;
      }
      if (hospAddress.get._2 != None) {
        aptcenteraddress += "<br>" + hospAddress.get._2.get;
      }
      if (hospAddress.get._3 != None) {
        aptcenteraddress += "<br>" + hospAddress.get._3.get;
      }
      if (hospAddress.get._8 != None) {
        aptcenteraddress += "<br>" + hospAddress.get._8.get;
      }
      /* Removed country - Krishnan
     * if(hospAddress.get._4 != None) {
      aptcenteraddress += ", " + hospAddress.get._4.get;
    }*/
      emailer = emailer.replaceAllLiterally("{{patient_address}}", patientAddressdtl)
      emailer = emailer.replaceAllLiterally("{{patient_postcode}}", patientAddres.get._5.getOrElse(""))

      //    emailer = emailer.replaceAllLiterally("{{patient_address2}}", patientAddres.get._2.getOrElse(""))
      //    emailer = emailer.replaceAllLiterally("{{patient_address3}}", patientAddres.get._3.getOrElse(""))
      //    emailer = emailer.replaceAllLiterally("{{patient_address4}}", patientAddres.get._4)
      //    emailer = emailer.replaceAllLiterally("{{patient_address5}}", patientAddres.get._5.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{appointment_date}}", dateFormat.format(shudledate.get))
      emailer = emailer.replaceAllLiterally("{{appointment_start}}", TwentyHrFormat)
      emailer = emailer.replaceAllLiterally("{{appointment_centre_name}}", hospAddress.get._5.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{appointment_centre_address}}", aptcenteraddress)
      //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address2}}", hospAddress.get._2.getOrElse(""))
      //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address3}}", hospAddress.get._3.getOrElse(""))
      //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address4}}", hospAddress.get._4.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{appointment_type_name}}", patientEnq.get._1.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{speciality_notes}}", AppointmentspecialityList(0)._1)
      emailer = emailer.replaceAllLiterally("{{Latitude}}", hospAddress.get._6.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{Longitude}}", hospAddress.get._7.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{speciality_details}}", AppointmentspecialityList(0)._1)
      emailer = emailer.replaceAllLiterally("{{speciality}}", AppointmentspecialityList(0)._2)
      emailer = emailer.replaceAllLiterally("{{clinicDescription}}", abouthospital.get.getOrElse(""))

      val futureString: Future[String] = scala.concurrent.Future {
        println("Patient Email : " + patEmail)
        if (patEmail != None && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, patEmail.getOrElse(""), None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer
    }
  }

  def sendAppointmentMailold(input: appointmentEmailerObj): String = db withSession { implicit session =>
    println("SENTAPPOINTMENTILD@@@@@@@@@@@@@@@")
    val obj = if (input.patientenquiryid.getOrElse(0L) > 0) {
      hospitalAppointment.filter { x => x.Patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
    } else {
      hospitalAppointment.filter { x => x.AppointmentID === input.appointmentid }.first
    }

    val extn = hospitalAppointmentExtn.filter { x => x.AppointmentID === obj.AppointmentID }.first

    val a: addScheduleAppointmentDetailsObj = new addScheduleAppointmentDetailsObj(None, obj.DoctorID.getOrElse(0L), None, obj.DayCD, obj.ScheduleDate, RequestResponseHelper.properTime(obj.StartTime), RequestResponseHelper.properTime(obj.EndTime),
      RequestResponseHelper.properTime(obj.EmergencyStartTime), RequestResponseHelper.properTime(obj.EmergencyEndTime), obj.DoctorID.getOrElse(0L), obj.PatientID.getOrElse(0L), obj.Patientenquiryid.getOrElse(0L),
      obj.AppointmentStatus, None, None, None, None, obj.CrtUSR, obj.CrtUSRType, None, None)

    val context = new VelocityContext();
    val dateFormat = CalenderDateConvertor.getDateFormatter(a.ScheduleDate)
    var contentSubject: String = "Appointment Confirmation";
    var doctorName = ""
    var doctortitle = ""

    var starttime = if (a.StartTime != "00:00") a.StartTime else a.EmergencyStartTime //sabari add this line

    //val dateObj = new SimpleDateFormat("H:mm").parse(a.StartTime)  // sabari comment this line
    val dateObj = new SimpleDateFormat("H:mm").parse(starttime)
    val TwelveHrFormat = new SimpleDateFormat("hh:mm a").format(dateObj)
    val consultDt = DateFormat.getDateInstance().format(a.ScheduleDate).toString()
    val format: SimpleDateFormat = new SimpleDateFormat("dd MMM,yyyy");

    var html = getHTML("APPCOMFIRMATION").first

    val ConsultationDt1: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
    val currentDateFormat = CalenderDateConvertor.getDateFormatter(ConsultationDt1)
    if (obj.DoctorID != None && obj.DoctorID != Some(0L)) {
      doctorName = doctor.filter { x => x.DoctorID === obj.DoctorID }.map { x => x.FullName }.firstOption.getOrElse("")
      doctortitle = doctor.filter { x => x.DoctorID === obj.DoctorID }.map { x => x.Title.getOrElse("") }.first
    }
    val patientEnq = patientenquiry.filter { x => x.patientenquiryid === a.Patientenquiryid }.map { x => (x.appointmenttypeid, x.clinicalnotes) }.firstOption
    val patientDetails = patients.filter { x => x.PatientID === a.PatientID }.map { x => (x.FirstName, x.LastName, x.Title) }.firstOption
    val patientAddres = patientAddress.filter { x => x.PatientID === a.PatientID }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.PostCode) }.firstOption
    val hospAddress = hospitalAddress.filter { x => x.HospitalID === obj.HospitalID }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.HospitalName, x.Latitude, x.Longitude, x.PostCode) }.firstOption
    val patEmail = patientEmail.filter { x => x.PatientID === a.PatientID }.map { x => x.EmailID }.firstOption
    val abouthospital = hospital.filter { x => x.HospitalID === obj.HospitalID }.map { x => x.About }.firstOption
    val AppointmentspecialityList = patientspecialityinfo(a.Patientenquiryid).list

    val formatDate = new SimpleDateFormat("dd-MM-yyyy")

    val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())

    println("Formatted Date " + createdDate)

    println("AppointmentspecialityList(0)._1" + AppointmentspecialityList(0)._1)

    println("AppointmentspecialityList(0)._1" + AppointmentspecialityList(0)._2)

    var emailer = html.replaceAllLiterally("{{created}}", createdDate)

    emailer = emailer.replaceAllLiterally("{{doctorName}}", doctorName)
    emailer = emailer.replaceAllLiterally("{{doctor_title}}", doctortitle)
    emailer = emailer.replaceAllLiterally("{{patient_forename}}", patientDetails.get._1)
    emailer = emailer.replaceAllLiterally("{{patient_surname}}", patientDetails.get._2.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{patient_title}}", patientDetails.get._3.getOrElse(""))
    var patientAddressdtl = "";
    if (patientAddres.get._1 != None) {
      patientAddressdtl += patientAddres.get._1.get;
    }
    if (patientAddres.get._2 != None) {
      patientAddressdtl += "<br>" + patientAddres.get._2.get;
    }
    if (patientAddres.get._3 != None) {
      patientAddressdtl += "<br>" + patientAddres.get._3.get;
    }
    /* Removed country - Krishnan
     * if(patientAddres.get._4 != None) {
      patientAddressdtl += "<br>" + patientAddres.get._4;
    }*/
    if (patientAddres.get._5 != None) {
      patientAddressdtl += "<br>" + patientAddres.get._5.get;
    }
    var aptcenteraddress = "";
    if (hospAddress.get._1 != None) {
      aptcenteraddress += hospAddress.get._1.get;
    }
    if (hospAddress.get._2 != None) {
      aptcenteraddress += "<br>" + hospAddress.get._2.get;
    }
    if (hospAddress.get._3 != None) {
      aptcenteraddress += "<br>" + hospAddress.get._3.get;
    }
    if (hospAddress.get._8 != None) {
      aptcenteraddress += "<br>" + hospAddress.get._8.get;
    }
    /* Removed country - Krishnan
     * if(hospAddress.get._4 != None) {
      aptcenteraddress += ", " + hospAddress.get._4.get;
    }*/
    emailer = emailer.replaceAllLiterally("{{patient_address}}", patientAddressdtl)
    emailer = emailer.replaceAllLiterally("{{patient_postcode}}", patientAddres.get._5.getOrElse(""))

    //    emailer = emailer.replaceAllLiterally("{{patient_address2}}", patientAddres.get._2.getOrElse(""))
    //    emailer = emailer.replaceAllLiterally("{{patient_address3}}", patientAddres.get._3.getOrElse(""))
    //    emailer = emailer.replaceAllLiterally("{{patient_address4}}", patientAddres.get._4)
    //    emailer = emailer.replaceAllLiterally("{{patient_address5}}", patientAddres.get._5.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{appointment_date}}", dateFormat.format(a.ScheduleDate))
    emailer = emailer.replaceAllLiterally("{{appointment_start}}", TwelveHrFormat)
    emailer = emailer.replaceAllLiterally("{{appointment_centre_name}}", hospAddress.get._5.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{appointment_centre_address}}", aptcenteraddress)
    //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address2}}", hospAddress.get._2.getOrElse(""))
    //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address3}}", hospAddress.get._3.getOrElse(""))
    //    emailer = emailer.replaceAllLiterally("{{appointment_centre_address4}}", hospAddress.get._4.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{appointment_type_name}}", patientEnq.get._1.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{speciality_notes}}", AppointmentspecialityList(0)._1)
    emailer = emailer.replaceAllLiterally("{{Latitude}}", hospAddress.get._6.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{Longitude}}", hospAddress.get._7.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{speciality_details}}", AppointmentspecialityList(0)._1)
    emailer = emailer.replaceAllLiterally("{{speciality}}", AppointmentspecialityList(0)._2)
    emailer = emailer.replaceAllLiterally("{{clinicDescription}}", abouthospital.get.getOrElse(""))

    val futureString: Future[String] = scala.concurrent.Future {
      println("Patient Email : " + patEmail)
      if (patEmail != None && input.sendemail == true)
        hcueCommunication.sendHcueEmail(emailer, patEmail.getOrElse(""), None, None, contentSubject, "Y", "Y", None)
      "Success"
    }

    emailer
  }

  def patientspecialityinfo(Patientenquiryid: Long) =
    sql"SELECT * FROM getappointmentemaildata('#$Patientenquiryid')".as[(String, String, Long)]

 
  def sendPatientDelinedMail(input: appointmentEmailerObj, hospitalID: Option[Long]): String = db withSession { implicit session =>
    val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
    val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
    val patEmail = patientEmail.filter { x => x.PatientID === obj.patientid }.map { x => x.EmailID }.firstOption
    val refEnquiry = referalEnquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => (x.doctorid, x.doctorname, x.practiceid) }.firstOption.get
 
    val doctorMail = doctorEmail.filter { x => x.DoctorID === refEnquiry._1 }.map { x => x.EmailID }.firstOption.getOrElse("")
   
   val objPracticeRepEmail = practice.filter { x => x.practiceid === refEnquiry._3.getOrElse(0L) }.map { x => x.repadminemail }.firstOption
    println("objPracticeRepEmail" + objPracticeRepEmail.toString)
    val practiceRepEmail = if (objPracticeRepEmail != None) {
      objPracticeRepEmail.get
    } else {
      None
    }
    
    
    var contentSubject: String = "Appointment Declined";

    val diagnosisType = enquiry.filter { x_ => ((x_.PatientEnquiryId === input.patientenquiryid) && (x_.SpecialityDescription.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption

    if (diagnosisType != None) {

      val outcome = lookup.filter { x => ((x.DoctorSpecialityID === (diagnosisType.get.SubSpecialities.toList)(0)._2) && (x.DoctorSpecialityDesc.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption.get

      var html = getHTML("PATIENTDECLINEDMRI").first

      val formatDate = new SimpleDateFormat("dd-MM-yyyy") //.format(obj.referalprocessed)
      val context = new VelocityContext();

      val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())

      var emailer = html.replaceAllLiterally("{{doctorName}}", refEnquiry._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{patient_name}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{scheduled_date}}", formatDate.format(obj.referalprocessed.get))
      emailer = emailer.replaceAllLiterally("{{reason}}", obj.notes.getOrElse(""));
      emailer = emailer.replaceAllLiterally("{{currentDate}}", createdDate)

      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patient_dob}}", formatDate.format(patientDetails.get._4.get))
      }

      val futureString: Future[String] = scala.concurrent.Future {

        if (practiceRepEmail.get != "" && input.sendemail == true) {
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.get , None, None, contentSubject, "Y", "Y",None)
        }
        "Success"
      }
      emailer

    } else {
      println("3333333333333333333333333333333333333333333333333333")
      var html = getHTML("PATIENTDECLINED").first

      val formatDate = new SimpleDateFormat("dd-MM-yyyy") //.format(obj.referalprocessed)
      val context = new VelocityContext();

      val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())

      var emailer = html.replaceAllLiterally("{{doctorName}}", refEnquiry._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{patient_name}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{scheduled_date}}", formatDate.format(obj.referalprocessed.get))
      emailer = emailer.replaceAllLiterally("{{reason}}", obj.notes.getOrElse(""));
      emailer = emailer.replaceAllLiterally("{{currentDate}}", createdDate)

      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patient_dob}}", formatDate.format(patientDetails.get._4.get))
      }

      val futureString: Future[String] = scala.concurrent.Future {
        println("doctor Email :"+ practiceRepEmail.get)
        if (practiceRepEmail.get != "" && input.sendemail == true) {
          
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.get, None, None, contentSubject, "Y", "Y",None)
        }
        "Success"
      }
      emailer
    }
  }
  def sendPatientDNAMail(input: appointmentEmailerObj, hospitalID: Option[Long]): String = db withSession { implicit session =>
    // PAtientEnquiry - tappointment

    val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
    val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
    val patEmail = patientEmail.filter { x => x.PatientID === obj.patientid }.map { x => x.EmailID }.firstOption
    val hosAppointmentDate = hospitalAppointment.filter { x => x.Patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => x.ScheduleDate }.firstOption.getOrElse("")
    val contentSubject: String = "Patient Did not attend";
    val formatDate = new SimpleDateFormat("dd-MM-yyyy")
    val refEnquiry = referalEnquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => (x.doctorid, x.doctorname, x.practiceid) }.firstOption.get
    val doctorMail = doctorEmail.filter { x => x.DoctorID === refEnquiry._1 }.map { x => x.EmailID }.firstOption.getOrElse("")
    val doctorName = doctor.filter { x => x.DoctorID === refEnquiry._1 }.map { x => (x.FirstName, x.LastName) }.firstOption

    val diagnosisType = enquiry.filter { x_ => ((x_.PatientEnquiryId === input.patientenquiryid) && (x_.SpecialityDescription.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption

     val objPracticeRepEmail = practice.filter { x => x.practiceid === refEnquiry._3.getOrElse(0L) }.map { x => x.repadminemail }.firstOption
    println("objPracticeRepEmail" + objPracticeRepEmail.toString)
    val practiceRepEmail = if (objPracticeRepEmail != None) {
      objPracticeRepEmail.get
    } else {
      None
    }
    
    if (diagnosisType != None) {

      val outcome = lookup.filter { x => ((x.DoctorSpecialityID === (diagnosisType.get.SubSpecialities.toList)(0)._2) && (x.DoctorSpecialityDesc.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption.get

      var html = getHTML("DNATOGPMRI").first

      val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())

      var emailer = html.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))

      emailer = emailer.replaceAllLiterally("{{currentDate}}", createdDate)

      if (doctorName != None) {
        emailer = emailer.replaceAllLiterally("{{doctorName}}", doctorName.get._1 + " " + doctorName.get._2.getOrElse(""))
      }

      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", formatDate.format(patientDetails.get._4.get))
      } else {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", "-")
      }
      if (obj.referalcreated != None) {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(obj.referalcreated.get))
      } else {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", "-")
      }

      val futureString: Future[String] = scala.concurrent.Future {
        if (practiceRepEmail.get != "" && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.get, None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer
    } else {
      val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
      val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
      val patEmail = patientEmail.filter { x => x.PatientID === obj.patientid }.map { x => x.EmailID }.firstOption
      val hosAppointmentDate = hospitalAppointment.filter { x => x.Patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => x.ScheduleDate }.firstOption.getOrElse("")
      val contentSubject: String = "Patient Did not attend";
      val formatDate = new SimpleDateFormat("dd-MM-yyyy")
      val refEnquiry = referalEnquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => (x.doctorid, x.doctorname) }.firstOption.get
      val doctorMail = doctorEmail.filter { x => x.DoctorID === refEnquiry._1 }.map { x => x.EmailID }.firstOption.getOrElse("")
      val doctorName = doctor.filter { x => x.DoctorID === refEnquiry._1 }.map { x => (x.FirstName, x.LastName) }.firstOption
      var html = getHTML("DNATOGP").first

      val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())

      var emailer = html.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))

      emailer = emailer.replaceAllLiterally("{{currentDate}}", createdDate)

      if (doctorName != None) {
        emailer = emailer.replaceAllLiterally("{{doctorName}}", doctorName.get._1 + " " + doctorName.get._2.getOrElse(""))
      }

      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", formatDate.format(patientDetails.get._4.get))
      } else {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", "-")
      }
      if (obj.referalcreated != None) {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(obj.referalcreated.get))
      } else {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", "-")
      }

      val futureString: Future[String] = scala.concurrent.Future {

        if (practiceRepEmail.get != "" && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.get, None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer
    }
  }
  def sendPatientWithdrawalMail(input: appointmentEmailerObj, hospitalID: Option[Long]): String = db withSession { implicit session =>

    val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
    val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
    val refEnquiry = referalEnquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => (x.doctorid, x.doctorname, x.practiceid) }.firstOption.get
    val doctorMail = doctorEmail.filter { x => x.DoctorID === refEnquiry._1 }.map { x => x.EmailID }.firstOption
    val hosAppointmentDate: Option[java.sql.Date] = hospitalAppointment.filter { x => x.Patientenquiryid === input.patientenquiryid.getOrElse(0L) }.firstOption.map { x => x.ScheduleDate }
    println(hosAppointmentDate + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    
     val objPracticeRepEmail = practice.filter { x => x.practiceid === refEnquiry._3.getOrElse(0L) }.map { x => x.repadminemail }.firstOption
    println("objPracticeRepEmail" + objPracticeRepEmail.toString)
    val practiceRepEmail = if (objPracticeRepEmail != None) {
      objPracticeRepEmail.get
    } else {
      None
    }
  
    
    
    val contentSubject: String = "Patient Withdrawal";

    val diagnosisType = enquiry.filter { x_ => ((x_.PatientEnquiryId === input.patientenquiryid) && (x_.SpecialityDescription.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption

    if (diagnosisType != None) {

      val outcome = lookup.filter { x => ((x.DoctorSpecialityID === (diagnosisType.get.SubSpecialities.toList)(0)._2) && (x.DoctorSpecialityDesc.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption.get

      var html = getHTML("WITHDRAWTOGPMRI").first
      val formatDate = new SimpleDateFormat("dd-MM-yyyy")
      val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())

      var emailer = html.replaceAllLiterally("{{doctorName}}", refEnquiry._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
      /*if (hosAppointmentDate != None) {
        emailer = emailer.replaceAllLiterally ("{{scanDate}}", formatDate.format(hosAppointmentDate.get))
         println(hosAppointmentDate.get +"****************************************************")
        // ("{{scanDate}}", formatDate.format(CalenderDateConvertor.getCalenderInstance(hosAppointmentDate.get).getTime))
      }*/

      if (obj.referalcreated != None) {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(obj.referalcreated.get))
      } else {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", "-")
      }

      emailer = emailer.replaceAllLiterally("{{currentDate}}", createdDate)
      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", formatDate.format(patientDetails.get._4.get))
      }

      val futureString: Future[String] = scala.concurrent.Future {
        if (practiceRepEmail.getOrElse("") != None && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.getOrElse(""), None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer
    } else {

      val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
      val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
      val refEnquiry = referalEnquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => (x.doctorid, x.doctorname) }.firstOption.get
      val doctorMail = doctorEmail.filter { x => x.DoctorID === refEnquiry._1 }.map { x => x.EmailID }.firstOption

      val hosAppointmentDate: Option[java.sql.Date] = hospitalAppointment.filter { x => x.Patientenquiryid === input.patientenquiryid.getOrElse(0L) }.firstOption.map { x => x.ScheduleDate }
      println(hosAppointmentDate + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
      val contentSubject: String = "Patient Withdrawal";
      val formatDate = new SimpleDateFormat("dd-MM-yyyy")
      val createdDate = formatDate.format(CalenderDateConvertor.getISDDate())
      var html = getHTML("WITHDRAWTOGP").first

      var emailer = html.replaceAllLiterally("{{doctorName}}", refEnquiry._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
      /* if (hosAppointmentDate != None) {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(hosAppointmentDate.get))
        println(hosAppointmentDate.get +"****************************************************")
      }
      */

      if (obj.referalcreated != None) {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(obj.referalcreated.get))
      } else {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", "-")
      }

      emailer = emailer.replaceAllLiterally("{{currentDate}}", createdDate)
      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", formatDate.format(patientDetails.get._4.get))
      }

      val futureString: Future[String] = scala.concurrent.Future {
        if (practiceRepEmail.getOrElse("") != None && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.getOrElse(""), None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer

    }

  }

  def sendDwExclusionMail(input: appointmentEmailerObj, hospitalID: Option[Long]): String = db withSession { implicit session =>
    println("***********Sending Exclusion Email - Begins*************")
    //patient Enquiry ID 
    val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
    val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
    val patEmail = patientEmail.filter { x => x.PatientID === obj.patientid }.map { x => x.EmailID }.firstOption
    val refEnquiry = referalEnquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => (x.doctorid, x.doctorname, x.practiceid) }.firstOption.get
    val doctorMail = doctorEmail.filter { x => x.DoctorID === refEnquiry._1 }.map { x => x.EmailID }.firstOption.getOrElse("")

    val objPracticeRepEmail = practice.filter { x => x.practiceid === refEnquiry._3 }.map { x => x.repadminemail }.firstOption
    println("objPracticeRepEmail" + objPracticeRepEmail.toString)
    val practiceRepEmail = if (objPracticeRepEmail != None) {
      objPracticeRepEmail.get
    } else {
      None
    }

    val hosAppointmentDate = hospitalAppointment.filter { x => x.Patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => x.ScheduleDate }.firstOption.getOrElse("")
    val diagnosisType = enquiry.filter { x_ => ((x_.PatientEnquiryId === input.patientenquiryid) && (x_.SpecialityDescription.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption

    if (diagnosisType != None) {

      println("diagnosisType::" + diagnosisType)
      val contentSubject: String = "Exclusion Mail";
      val formatDate = new SimpleDateFormat("dd-MM-yyyy")
      var html = getHTML("EXCLUSIONTOGPMRI").first

      var emailer = html.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{createdDate}}", formatDate.format(obj.referalprocessed.get))
      emailer = emailer.replaceAllLiterally("{{doctorName}}", refEnquiry._2.getOrElse(""))
      if (hosAppointmentDate != None && hosAppointmentDate != "") {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(hosAppointmentDate))
      } else {
        emailer = emailer.replaceAllLiterally("(dated {{scanDate}})", "")
      }
      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", formatDate.format(patientDetails.get._4.get))
      }

      val futureString: Future[String] = scala.concurrent.Future {
        println("Practie Rep mail::" + practiceRepEmail)
        val isEmailSent = if (practiceRepEmail != None && practiceRepEmail != "" && input.sendemail == true) {
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.get, None, None, contentSubject, "Y", "Y", None)
        } else {
          println("Exclusion email not sent")
          "Exclusion email not sent"
        }
        println("Exclusion Email Sent? ::" + isEmailSent)
        isEmailSent
      }
      println("Exclusion Email Sent? ::" + futureString.toString())
      println("***********Sending Exclusion Email - Ends*************")
      emailer
    } else {
      println("diagnosisType::" + diagnosisType)
      val contentSubject: String = "Exclusion Mail";
      val formatDate = new SimpleDateFormat("dd-MM-yyyy")
      var html = getHTML("EXCLUSIONTOGP").first

      var emailer = html.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{createdDate}}", formatDate.format(obj.referalprocessed.get))
      emailer = emailer.replaceAllLiterally("{{doctorName}}", refEnquiry._2.getOrElse(""))
      if (hosAppointmentDate != None && hosAppointmentDate != "") {
        emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(hosAppointmentDate))
      } else {
        emailer = emailer.replaceAllLiterally("(dated {{scanDate}})", "")
      }
      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", formatDate.format(patientDetails.get._4.get))
      }

      val futureString: Future[String] = scala.concurrent.Future {
        println("Practie Rep mail::" + practiceRepEmail)
        val isEmailSent = if (practiceRepEmail != None && practiceRepEmail != "" && input.sendemail == true) {
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.get, None, None, contentSubject, "Y", "Y", None)
        } else {
          println("Exclusion email not sent")
          "Exclusion email not sent"
        }
        println("Exclusion Email Sent? ::" + isEmailSent)
        isEmailSent
      }
      println("***********Sending Exclusion Email - Ends*************")
      emailer
    }

  }

  def sendNoContactToGP(input: appointmentEmailerObj, hospitalID: Option[Long]): String = db withSession { implicit session =>

    val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
    val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
    val patEmail = patientEmail.filter { x => x.PatientID === obj.patientid }.map { x => x.EmailID }.firstOption
    val refEnquiry = referalEnquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => (x.doctorid, x.doctorname, x.practiceid) }.firstOption.get
    var doctorMail = doctorEmail.filter { x => x.DoctorID === refEnquiry._1 }.map { x => x.EmailID }.firstOption.getOrElse("")
    val hosAppointmentDate = hospitalAppointment.filter { x => x.Patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => x.ScheduleDate }.firstOption.getOrElse("")
    val doctorName = doctor.filter { x => x.DoctorID === refEnquiry._1 }.map { x => (x.FirstName, x.LastName) }.firstOption
    
     val objPracticeRepEmail = practice.filter { x => x.practiceid === refEnquiry._3 }.map { x => x.repadminemail }.firstOption
    println("objPracticeRepEmail" + objPracticeRepEmail.toString)
    val practiceRepEmail = if (objPracticeRepEmail != None) {
      objPracticeRepEmail.get
    } else {
      None
    }

    
    
    val contentSubject: String = "Unable to Contact";
    val formatDate = new SimpleDateFormat("dd-MM-yyyy")

    val diagnosisType = enquiry.filter { x_ => ((x_.PatientEnquiryId === input.patientenquiryid) && (x_.SpecialityDescription.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption

    if (diagnosisType != None) {

      val outcome = lookup.filter { x => ((x.DoctorSpecialityID === (diagnosisType.get.SubSpecialities.toList)(0)._2) && (x.DoctorSpecialityDesc.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption.get

      var html = if (input.emailtype.getOrElse("") == "URGENT") {
        getHTML("URGENTNOCONTACTTOGPMRI").first
      } else {
        getHTML("NOCONTACTMRI").first
      }

      var emailer = html //.replaceAllLiterally("{{doctorName}}", refEnquiry._2.getOrElse(""))

      if (doctorName != None) {
        emailer = emailer.replaceAllLiterally("{{doctorName}}", doctorName.get._1 + " " + doctorName.get._2.getOrElse(""))
      }
      emailer = emailer.replaceAllLiterally("{{date}}", formatDate.format(obj.referalprocessed.get))
      emailer = emailer.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + " " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(obj.referalcreated.get))
      if (patientDetails.get._4 != None) {
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", formatDate.format(patientDetails.get._4.get))
      }

      val futureString: Future[String] = scala.concurrent.Future {
        if (practiceRepEmail.get != "" && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.get, None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer
    } else {

      var html = if (input.emailtype.getOrElse("") == "URGENT") {
        getHTML("URGENTNOCONTACTTOGP").first
      } else {
        getHTML("NOCONTACT").first
      }

      var emailer = html //.replaceAllLiterally("{{doctorName}}", refEnquiry._2.getOrElse(""))

      if (doctorName != None) {
        emailer = emailer.replaceAllLiterally("{{doctorName}}", doctorName.get._1 + " " + doctorName.get._2.getOrElse(""))
      }
      emailer = emailer.replaceAllLiterally("{{date}}", formatDate.format(obj.referalprocessed.get))
      emailer = emailer.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + " " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{scanDate}}", formatDate.format(obj.referalcreated.get))
      if (patientDetails.get._4 != None) {
        println(" patient  Details   :" + patientDetails)
        emailer = emailer.replaceAllLiterally("{{patientDOB}}", formatDate.format(patientDetails.get._4.get))
      }

      val futureString: Future[String] = scala.concurrent.Future {
        if (practiceRepEmail.get != "" && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, practiceRepEmail.get, None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer
    }

  }

  def sendContactUs(input: appointmentEmailerObj, hospitalID: Option[Long]): String = db withSession { implicit session =>

    val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
    val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
    val patAddress = patientAddress.filter { x => x.PatientID === obj.patientid }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.PostCode) }.firstOption
    val patEmail = patientEmail.filter { x => x.PatientID === obj.patientid }.map { x => x.EmailID }.firstOption
    val formatDate = new SimpleDateFormat("dd-MM-yyyy")

    val diagnosisType = enquiry.filter { x_ => ((x_.PatientEnquiryId === input.patientenquiryid) && (x_.SpecialityDescription.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption

    if (diagnosisType != None) {
      println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      val outcome = lookup.filter { x => ((x.DoctorSpecialityID === (diagnosisType.get.SubSpecialities.toList)(0)._2) && (x.DoctorSpecialityDesc.toLowerCase like ("%" + "MRI".toLowerCase + "%"))) }.firstOption.get

      val currentDate: java.sql.Date = new java.sql.Date(hCueCalenderHelper.getUKDate().getTime())
      val contentSubject: String = "Contact Us";

      var html = getHTML("DWCONTACTMRI").first

      var emailer = html.replaceAllLiterally("{{patient_title}}", patientDetails.get._3.get)
      emailer = emailer.replaceAllLiterally("{{patient_forename}}", patientDetails.get._1)
      emailer = emailer.replaceAllLiterally("{{patient_surname}}", patientDetails.get._2.getOrElse(""))

      var arrPatientAddress = ArrayBuffer[String]()

      patAddress.get._1.isEmpty match {
        case false => arrPatientAddress += patAddress.get._1.get.trim()
        case true  => None
      }

      patAddress.get._2.isEmpty match {
        case false => arrPatientAddress += patAddress.get._2.get.trim()
        case true  => None
      }

      patAddress.get._3.isEmpty match {
        case false => arrPatientAddress += patAddress.get._3.get.trim()
        case true  => None
      }

      patAddress.get._5.isEmpty match {
        case false => arrPatientAddress += patAddress.get._5.get.trim()
        case true  => None
      }

      emailer = emailer.replaceAllLiterally("{{patient_address}}", arrPatientAddress.mkString("<br/>"))
      emailer = emailer.replaceAllLiterally("{{created}}", formatDate.format(currentDate))

      val futureString: Future[String] = scala.concurrent.Future {
        if (patEmail != None && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, patEmail.getOrElse(""), None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer
    } else {

      println("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB")

      val currentDate: java.sql.Date = new java.sql.Date(hCueCalenderHelper.getUKDate().getTime())
      val contentSubject: String = "Contact Us";

      var html = getHTML("DWCONTACT").first

      var emailer = html.replaceAllLiterally("{{patient_title}}", patientDetails.get._3.get)
      emailer = emailer.replaceAllLiterally("{{patient_forename}}", patientDetails.get._1)
      emailer = emailer.replaceAllLiterally("{{patient_surname}}", patientDetails.get._2.getOrElse(""))

      var arrPatientAddress = ArrayBuffer[String]()

      patAddress.get._1.isEmpty match {
        case false => arrPatientAddress += patAddress.get._1.get.trim()
        case true  => None
      }

      patAddress.get._2.isEmpty match {
        case false => arrPatientAddress += patAddress.get._2.get.trim()
        case true  => None
      }

      patAddress.get._3.isEmpty match {
        case false => arrPatientAddress += patAddress.get._3.get.trim()
        case true  => None
      }

      patAddress.get._5.isEmpty match {
        case false => arrPatientAddress += patAddress.get._5.get.trim()
        case true  => None
      }

      emailer = emailer.replaceAllLiterally("{{patient_address}}", arrPatientAddress.mkString("<br/>"))
      emailer = emailer.replaceAllLiterally("{{created}}", formatDate.format(currentDate))

      val futureString: Future[String] = scala.concurrent.Future {
        if (patEmail != None && input.sendemail == true)
          hcueCommunication.sendHcueEmail(emailer, patEmail.getOrElse(""), None, None, contentSubject, "Y", "Y", None)
        "Success"
      }

      emailer
    }
  }

  def sendUrgentNoContactToGP(input: appointmentEmailerObj, hospitalID: Option[Long]): String = db withSession { implicit session =>

    val obj = patientenquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.first
    val patientDetails = patients.filter { x => x.PatientID === obj.patientid }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
    val refEnquiry = referalEnquiry.filter { x => x.patientenquiryid === input.patientenquiryid.getOrElse(0L) }.map { x => (x.doctorid, x.doctorname) }.firstOption.get
    val scheduleDateFormatted = new SimpleDateFormat("dd-MM-yyyy").format(obj.referalprocessed)
    val patEmail = patientEmail.filter { x => x.PatientID === obj.patientid }.map { x => x.EmailID }.firstOption
    val formatDate = new SimpleDateFormat("dd-MM-yyyy")
    val contentSubject: String = "Contact Us";
    val doctorMail = doctorEmail.filter { x => x.DoctorID === refEnquiry._1 }.map { x => x.EmailID }.firstOption.getOrElse("")

    var html = getHTML("URGENTNOCONTACTTOGP").first

    var emailer = html.replaceAllLiterally("{{doctorName}}", refEnquiry._2.get)
    emailer = emailer.replaceAllLiterally("{{createdDate}}", scheduleDateFormatted)
    emailer = emailer.replaceAllLiterally("{{patientName}}", patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse(""))
    if (patientDetails.get._4 != None) {
      emailer = emailer.replaceAllLiterally("{{patientDOB}}", "DOB: " + formatDate.format(patientDetails.get._4.get))
    }

    val futureString: Future[String] = scala.concurrent.Future {
      if (doctorMail != "" && input.sendemail == true)
        hcueCommunication.sendHcueEmail(emailer, doctorMail, None, None, contentSubject, "Y", "Y", None)
      "Success"
    }

    emailer
  }

  def sendAttachment(responseId: Long, PracticeID: Long): String = db withSession { implicit session =>

    val fileLocationURL = diagnosticResponse.filter { x => x.responseid === responseId }.map(x => x.ReportURL).firstOption.get

    val reportadminmailid = practice.filter { x => x.practiceid === PracticeID }.map { x => x.repadminemail }.firstOption.get

    val referralid = diagnosticResponse.filter { x => x.responseid === responseId }.map { x => x.referralid }.firstOption.get

    val patandAppID = diagnosticRequest.filter { x => x.ReferralId === referralid && x.ActiveIND === "Y" && x.AppointmentStatus === "B" }.map { x => (x.patientid, x.appointmentid) }.firstOption.get

    val patName = patients.filter { x => x.PatientID === patandAppID._1 }.map { x => x.FullName }.firstOption.get

    val docandPatEnqId = hospitalAppointment.filter { x => x.AppointmentID === patandAppID._2 }.map { x => (x.DoctorID, x.Patientenquiryid) }.firstOption.get

    //val docName = doctor.filter { x => x.DoctorID === docandPatEnqId._1 }.map { x => x.FullName }.firstOption.get

    val notes = patientenquiry.filter { x => x.patientenquiryid === docandPatEnqId._2 }.map { x => x.notes }.firstOption.get

    //new code add hear get from referral doctor

    val doctoridval = referalEnquiry.filter { x => x.patientenquiryid === docandPatEnqId._2 }.map { x => x.doctorid }.firstOption.get

    val docName = doctor.filter { x => x.DoctorID === doctoridval }.map { x => x.FullName }.firstOption.get

    println("reportadminmailid  values " + reportadminmailid)

    //val bucketName = "c7fileattachment"
    val bucketName = "c7health-amp-reports"

    var pdfFileName = (fileLocationURL.split("/"))((fileLocationURL.split("/")).length - 1)

    println("*******************************************")
    println("PDF FILE NAME   " + pdfFileName)
    println("*******************************************")

    //downloadFile(bucketName, pdfFileName, "Default123.pdf")

    //var vmFileName: String = hcueTemplateLocation.sendFileAttachment
    //val contentSubject: String = "File Attachment";
    val contentSubject: String = "UltraSound Report - " + patName;

    val futureString: Future[String] = scala.concurrent.Future {
    	
    val Emailcc = current.configuration.getString("email.cc").get

      hcueCommunication.sendAttachmentMail(reportadminmailid.get, Some(Emailcc), None, contentSubject, fileLocationURL, docName, notes)

    }

    /*   var localFile: File = new File("Default123.pdf");

    if (localFile.delete()) {
      System.out.println("------------------------------File deleted------------------------------")
    }*/
    "Success"

  }

  def sendappointmentdetailstoscancenter(input: Integer): String = db withSession { implicit session =>

    val formatDate = new SimpleDateFormat("dd-MM-yyyy")
    var dt = new java.util.Date();
    val c = Calendar.getInstance();
    c.setTime(dt);
    c.add(Calendar.DATE, input);

    val sqlDate = new java.sql.Date(c.getTimeInMillis());
    
   /* val formatDate = new SimpleDateFormat("dd-MM-yyyy")
    val currentDate: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
    val dtOrg: DateTime = new DateTime(currentDate)
    val sqlDate: java.sql.Date = new Date(dtOrg.toLocalDateTime().toDateTime().getMillis)
    println("scheduledate val " + sqlDate) */
    
    //val hospitalAppDetail = hospitalAppointment.filter { x => x.ScheduleDate === sqlDate && x.AppointmentStatus === "B" }.list
    val hospitalAppDetail = hospitalAppointment.filter { x => x.ScheduleDate === sqlDate }.list.distinct
    val hospitlIds = hospitalAppDetail.map { x => x.HospitalID.getOrElse(0L) }.toList
    val patientIds = hospitalAppDetail.map { x => x.PatientID.getOrElse(0L) }.toList
    val hospitaldetail = hospital.filter { x => x.HospitalID inSet hospitlIds }.list
    val hospitalExtndetail = hospitalExtn.filter { x => x.HospitalID inSet hospitlIds }.list
    val patientDtl = patients.filter { x => x.PatientID inSet patientIds }.list

    val htmlQuery = getHTML("APPOINTMENTREMAINDER").first

    for (i <- 0 until hospitaldetail.length) {
      val hospAddress = hospitalAddress.filter { x => x.HospitalID === hospitaldetail(i).HospitalID }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.HospitalName, x.Latitude, x.Longitude) }.firstOption
      var html = htmlQuery;
      var emailer = html.replaceAllLiterally("{{scancentre_name}}", hospitaldetail(i).HospitalName.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{scancentre_address}}", hospAddress.get._1.getOrElse("") + ", " + hospAddress.get._2.getOrElse("") + ", " + hospAddress.get._3.getOrElse("") + ", " + hospAddress.get._4.getOrElse(""))

      var hospitalAppList = hospitalAppDetail.filter { x => x.HospitalID.get == hospitaldetail(i).HospitalID }
        .map { x => (x, x.StartTime.replace(":", "").toLong + x.EmergencyStartTime.replace(":", "").toLong) }.sortBy(f => f._2).distinct

      var appLoop = ""
      for (j <- 0 until hospitalAppList.length) {

        val dateObj: java.util.Date = hospitalAppList(j)._1.StartTime value match {
          case "00:00" => new SimpleDateFormat("H:mm").parse(hospitalAppList(j)._1.EmergencyStartTime)
          case _       => new SimpleDateFormat("H:mm").parse(hospitalAppList(j)._1.StartTime)
        }

        //val dateObj = new SimpleDateFormat("H:mm").parse(hospitalAppList(j).StartTime)
        val TwentyFourHrFormat = new SimpleDateFormat("HH:mm").format(dateObj)
        //val TwelveHrFormat = new SimpleDateFormat("hh:mm a").format(dateObj)
        val patientDetails = patients.filter { x => x.PatientID === hospitalAppList(j)._1.PatientID }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
        appLoop += "<tr><td>" + formatDate.format(hospitalAppList(j)._1.ScheduleDate) + "</td>" + TwentyFourHrFormat + "</td> <td> " + patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse("") + "</td> <td> " + formatDate.format(patientDetails.get._4.getOrElse("")) + "</td> <td>" + hospitalAppList(j)._1.AppointmentStatus + "</td></tr>"
      }

      emailer = emailer.replaceAllLiterally("{{appointment_dtls}}", appLoop)

      val futureString: Future[String] = scala.concurrent.Future {
        var emailid = hospitalExtndetail.find { x => x.HospitalID == hospitaldetail(i).HospitalID }.map { x => x.AppListEmail.getOrElse("") }
        if (emailid != None && emailid.get != "")
          hcueCommunication.sendHcueEmail(emailer, emailid.getOrElse(""), None, None, "Tomorrow's Appointment List", "Y", "Y", None)
        "Success"
      }
    }
    "success"
  }

  def appointmentdetailstoscancenter(hospitalid: Long, sonographerid: Long, addressconsultid: Long, scheduledate: java.sql.Date): String = db withSession { implicit session =>

    //val hospitalAppDetail = hospitalAppointment.filter { x => x.ScheduleDate === scheduledate && x.AppointmentStatus === "B" && x.DoctorID === sonographerid && x.AddressConsultID === addressconsultid && x.HospitalID === hospitalid }.list
    val hospitalAppDetail = hospitalAppointment.filter { x => x.ScheduleDate === scheduledate && x.DoctorID === sonographerid && x.AddressConsultID === addressconsultid && x.HospitalID === hospitalid }.list.distinct
    val patientIds = hospitalAppDetail.map { x => x.PatientID.getOrElse(0L) }.toList
    val hospitaldetail = hospital.filter { x => x.HospitalID === hospitalid }.list
    val hospitalExtndetail = hospitalExtn.filter { x => x.HospitalID === hospitalid }.list
    val patientDtl = patients.filter { x => x.PatientID inSet patientIds }.list

    val htmlQuery = getHTML("APPOINTMENTREMAINDER").first

    val formatDate = new SimpleDateFormat("dd-MM-yyyy")

    for (i <- 0 until hospitaldetail.length) {

      val hospAddress = hospitalAddress.filter { x => x.HospitalID === hospitaldetail(i).HospitalID }.map { x => (x.Address1, x.Address2, x.CityTown, x.Country, x.HospitalName, x.Latitude, x.Longitude) }.firstOption
      var html = htmlQuery;
      var emailer = html.replaceAllLiterally("{{scancentre_name}}", hospitaldetail(i).HospitalName.getOrElse(""))
      emailer = emailer.replaceAllLiterally("{{scancentre_address}}", hospAddress.get._1.getOrElse("") + ", " + hospAddress.get._2.getOrElse("") + ", " + hospAddress.get._3.getOrElse("") + ", " + hospAddress.get._4.getOrElse(""))

      var hospitalAppList = hospitalAppDetail.filter { x => x.HospitalID.get == hospitaldetail(i).HospitalID }
        .map { x => (x, x.StartTime.replace(":", "").toLong + x.EmergencyStartTime.replace(":", "").toLong) }.sortBy(f => f._2).distinct

      var appLoop = ""
      for (j <- 0 until hospitalAppList.length) {

        val dateObj: java.util.Date = hospitalAppList(j)._1.StartTime value match {
          case "00:00" => new SimpleDateFormat("H:mm").parse(hospitalAppList(j)._1.EmergencyStartTime)
          case _       => new SimpleDateFormat("H:mm").parse(hospitalAppList(j)._1.StartTime)
        }

        val TwentyFourHrFormat = new SimpleDateFormat("HH:mm").format(dateObj)
        //val TwelveHrFormat = new SimpleDateFormat("hh:mm a").format(dateObj)
        val patientDetails = patients.filter { x => x.PatientID === hospitalAppList(j)._1.PatientID }.map { x => (x.FirstName, x.LastName, x.Title, x.DOB) }.firstOption
        appLoop += "<tr><td>" + formatDate.format(scheduledate) + "</td>" + TwentyFourHrFormat + "</td> <td> " + patientDetails.get._3.getOrElse("") + ". " + patientDetails.get._1 + " " + patientDetails.get._2.getOrElse("") + "</td> <td> " + formatDate.format(patientDetails.get._4.getOrElse("")) + "</td> <td>" + hospitalAppList(j)._1.AppointmentStatus + "</td></tr>"
      }

      emailer = emailer.replaceAllLiterally("{{appointment_dtls}}", appLoop)

      val futureString: Future[String] = scala.concurrent.Future {
        var emailid = hospitalExtndetail.find { x => x.HospitalID == hospitaldetail(i).HospitalID }.map { x => x.AppListEmail.getOrElse("") }

        if (emailid != None && emailid.get != "")
          hcueCommunication.sendHcueEmail(emailer, emailid.getOrElse(""), None, None, "Appointment Details", "Y", "Y", None)
        "Success"
      }
    }
    "success"
  }

  def downloadFile(bucketName: String, tempFilePath: String, fileName: String) = {

    try {
      System.out.println("Start Download");
      println("-------tempFilePath-------" + tempFilePath)

      var fetchFile: S3Object = amazonS3Client.getObject(new GetObjectRequest(bucketName, tempFilePath));

      var i: BufferedInputStream = new BufferedInputStream(fetchFile.getObjectContent());
      var objectData: InputStream = fetchFile.getObjectContent();
      Files.copy(objectData, new File(fileName).toPath()); //location to local path
      objectData.close();

      System.out.println("End Download");
    } catch {
      case e: Exception =>
        e.printStackTrace()

    }

  }

  def sendsonographerMail(input: sonographerinfoObj): String = db withSession { implicit session =>

    val appointmentlist = getsonographerinfo(input.scheduledate, input.doctorid).list

    val doctorMail = doctorlogin.filter { x => x.DoctorID === input.doctorid }.map { x => x.DoctorLoginID }.firstOption.getOrElse("")

    //val timevalues = hospitalscheduleextn.filter { x => x.AddressConsultID === input.addressconsultid }.map { x => (x.FromTime1.getOrElse("00:00"), x.ToTime1.getOrElse("00:00")) }.firstOption

    val hospitalscheduledetails = hospitalschedule.filter { x => x.AddressConsultID === input.addressconsultid }.map { x => (x.SonographerDoctorID, x.SupportworkerDoctorID) }.firstOption

    val sonographername = doctor.filter { x => x.DoctorID === hospitalscheduledetails.get._1 }.map { x => x.FullName }.firstOption
    val cswname = doctor.filter { x => x.DoctorID === hospitalscheduledetails.get._2 }.map { x => x.FullName }.firstOption

    //val doctorMail="sabarinathan@cardinality.ai"

    val htmlQuery = getHTML("SONOGRAPHERINF0").first

    var html = htmlQuery;
    var emailer = html.replaceAllLiterally("{{scancentre_name}}", input.hospitalname.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{sonographername}}", sonographername.getOrElse(""))
    emailer = emailer.replaceAllLiterally("{{cswname}}", cswname.getOrElse(""))

    var appLoop = ""
    for (j <- 0 until appointmentlist.length) {

      appLoop += "<tr><td>" + appointmentlist(j)._5 + "</td><td>" + appointmentlist(j)._7 + "</td></tr>"
    }

    emailer = emailer.replaceAllLiterally("{{appointment_dtls}}", appLoop)

    val futureString: Future[String] = scala.concurrent.Future {
      var emailid = doctorMail

      if (emailid != None && emailid != "")
        hcueCommunication.sendHcueEmail(emailer, emailid, None, None, "Sonographer-Appointmentdetails", "Y", "Y", None)
      "Success"
    }

    /*    if (appointmentlist.length > 0) {
      MakeCSV.generateCSV(appointmentlist, doctorMail, input.doctorid, input.hospitalname.get, timevalues.get._1, timevalues.get._2,
        input.sonographername.get, input.cswname.get)
    }*/
    "Success"
  }

  def sendsupportworkerMail(input: sonographerinfoObj): String = db withSession { implicit session =>

    val appointmentlist = getsupporworkerinfo(input.scheduledate, input.doctorid).list

    val doctorMail = doctorlogin.filter { x => x.DoctorID === input.doctorid }.map { x => x.DoctorLoginID }.firstOption.getOrElse("")

    val timevalues = hospitalscheduleextn.filter { x => x.AddressConsultID === input.addressconsultid }.map { x => (x.FromTime1.getOrElse("00:00"), x.ToTime1.getOrElse("00:00")) }.firstOption

    //val doctorMail="sabarinathan@cardinality.ai" 

    if (appointmentlist.length > 0) {
      /* MakeCSV.generatecswCSVnew(appointmentlist, doctorMail, input.doctorid, input.hospitalname.get, timevalues.get._1, timevalues.get._2,
        input.sonographername.get, input.cswname.get) */
        
      MakeCSV.generatecswexcel(appointmentlist, doctorMail, input.doctorid, input.hospitalname.get, timevalues.get._1, timevalues.get._2,
        input.sonographername.get, input.cswname.get)
        
    }

    "Success"
  }

  def sendsonographerInfo(): String = db withSession { implicit session =>

    val currentDate: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())

    val dtOrg: DateTime = new DateTime(currentDate)
    println("current date value ::: "+dtOrg)
    //val adddate = dtOrg.plusDays(1)
    val scheduledate: java.sql.Date = new Date(dtOrg.toLocalDateTime().toDateTime().getMillis)
    println("adddate val " + scheduledate)

    val doctorsRecord = (for {
      (appointmentval, scheduleval) <- hospitalAppointment innerJoin hospitalschedule on (_.ScheduleDate === _.ScheduleDate)
      if (appointmentval.ScheduleDate === scheduledate && scheduleval.ScheduleDate === scheduledate)
    } yield (appointmentval, scheduleval)).list

    val sonographerid = doctorsRecord.map { x => x._2.SonographerDoctorID }.distinct

    var status = ""
    for (i <- 0 until sonographerid.length) {

      val appointmentlist = getsonographerinfo(scheduledate, sonographerid(i)).list

      val doctorMail = doctorlogin.filter { x => x.DoctorID === sonographerid(i) }.map { x => x.DoctorLoginID }.firstOption.getOrElse("")

      if (appointmentlist.length > 0) {
        MakeCSV.generateCSV(appointmentlist, doctorMail, sonographerid(i), "", "", "", "", "")

        status = "Success"
      }

    }
    status
  }

  def sendsupportworkerInfo(): String = db withSession { implicit session =>

    val currentDate: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())

    val dtOrg: DateTime = new DateTime(currentDate).plusDays(1)
    //val adddate = dtOrg.plusDays(1)
    val scheduledate: java.sql.Date = new Date(dtOrg.toLocalDateTime().toDateTime().getMillis)
    println("adddate val " + scheduledate)

    val doctorsRecord = (for {
      (appointmentval, scheduleval) <- hospitalAppointment innerJoin hospitalschedule on (_.ScheduleDate === _.ScheduleDate)
      if (appointmentval.ScheduleDate === scheduledate && scheduleval.ScheduleDate === scheduledate)
    } yield (appointmentval, scheduleval)).list

    val supportworkerid = doctorsRecord.map { x => x._2.SupportworkerDoctorID }.distinct

    var status = ""
    for (i <- 0 until supportworkerid.length) {

      val appointmentlist = getsupporworkerinfo(scheduledate, supportworkerid(i)).list

      val doctorMail = doctorlogin.filter { x => x.DoctorID === supportworkerid(i) }.map { x => x.DoctorLoginID }.firstOption.getOrElse("")

      if (appointmentlist.length > 0) {
        MakeCSV.generatecswCSV(appointmentlist, doctorMail, supportworkerid(i), "", "", "", "", "")

        status = "Success"
      }

    }
    status
  }

  def appointmentbookingvalidation(a: addScheduleAppointmentDetailsObj): Boolean = db withSession { implicit session =>

    var status = true
    if (a.AddressConsultID != None) {

      val validationcount = appointmentvalidationinfo(a.DoctorID, a.HospitalDetails.get.HospitalID.get,
        a.AddressConsultID.get, a.ScheduleDate, a.StartTime, a.EndTime, a.EmergencyStartTime, a.EmergencyEndTime, a.PatientID, a.Patientenquiryid).list

      if (validationcount(0) > 0) {
        status = false
      }
    }
    return status
  }

  def appointmentremindersmssend(appointmentid: Long): String = db withSession { implicit session =>

    var resultstr = "Error"

    val appointmentdtl = (for {
      ((hosapp, patient), patientph) <- hospitalAppointment leftJoin patients on (_.PatientID === _.PatientID) leftJoin patientPhones on (_._2.PatientID === _.PatientID)
      if (hosapp.AppointmentID === appointmentid)
    } yield (hosapp, patient, patientph)).list

    //val appointmentdtls = hospitalAppointment.filter { x => x.AppointmentID ===  appointmentid}.map { x => (x.PatientID,x.StartTime,x.EndTime,x.EmergencyStartTime,x.EmergencyEndTime,x.ScheduleDate) }.firstOption
    if (appointmentdtl.length > 0) {

      val patientid = appointmentdtl.map(f => f._1.PatientID).distinct
      val patientname = appointmentdtl.map(f => f._2.FirstName).distinct
      val starttime = appointmentdtl.map(f => f._1.StartTime).distinct
      val endtime = appointmentdtl.map(f => f._1.EndTime).distinct
      val emergencystarttime = appointmentdtl.map(f => f._1.EmergencyStartTime).distinct
      val emergencyendtime = appointmentdtl.map(f => f._1.EmergencyEndTime).distinct
      val scheduledate = appointmentdtl.map(f => f._1.ScheduleDate).distinct
      val phonenumber = appointmentdtl.map(f => f._3.PhNumber).distinct

      println("patientid  " + patientid(0).get)
      println("patientname  " + patientname(0))
      println("starttime  " + starttime(0))
      println("endtime  " + endtime(0))
      println("emergencystarttime  " + emergencystarttime(0))
      println("emergencyendtime  " + emergencyendtime(0))
      println("scheduledate  " + scheduledate(0))
      println("phonenumber  " + phonenumber(0))

      val URL = "https://api.smsbroadcast.co.uk/api-adv.php?username=c7health&password=SMSC7H38lth&to=" + phonenumber + "&from=c7healthamp&message=Test%20message%20from%20development%20server&ref=112233&maxsplit=5&delay=15" //phil
      println("URL value " + URL)

      val result = scala.io.Source.fromURL(URL).mkString.replace("\\", "")

      val resultarr = result.split(':')

      println("result value " + resultarr(0))

      resultstr = resultarr(0)
    }
    resultstr

  }

  def getHospitalAppmntById(appmntId: Long): THospitalAppointment = db withSession { implicit session =>
    hospitalAppointment.filter { x => x.AppointmentID === appmntId }.first
  }

  def updateAppointmentStatus(appmntId: Long): Int = db withSession { implicit session =>
    hospitalAppointment.filter { x => x.AppointmentID === appmntId }.map { x => x.AppointmentStatus }.update("SC")
  }

  def updateAppointmentTimeandStatus(appmntId: Long, scanStartTime: Option[String], scanEndTime: Option[String]): Int = db withSession { implicit session =>
    val updateval = hospitalAppointmentExtn.filter { x => x.AppointmentID === appmntId }.map { x => (x.AppointmentStatus, x.Startscantime, x.Completescantime, x.UpdtUSR, x.UpdtUSRType) }.
      update("SC", scanStartTime, scanEndTime, Some(1), Some("WSO2"))
      
      val costreportaddval = Addcostreport(appmntId, 0L).first
      
      return updateval;
  }

  def getsonographerinfo(date: java.sql.Date, doctorid: Long) =
    sql"SELECT * FROM getsonographercsvfile('#$date','#$doctorid')".as[(Long, String, String, String, String, String, String, String, String)]

  def getsupporworkerinfo(date: java.sql.Date, doctorid: Long) =
    sql"SELECT * FROM getsupportworkercsvfile('#$date','#$doctorid')".as[(Long, String, Long, String, String, String, String, String, String, String, String, String)]

  def appointmentvalidationinfo(doctorid: Long, hospitalid: Long, addressconsultid: Long, scheduledate: java.sql.Date, starttime: String, endtime: String, emergencystarttime: String, emergencyendtime: String, patientidval: Long, patientenquiryidval: Long) =
    sql"SELECT * FROM appointmentvalidation('#$doctorid','#$hospitalid','#$addressconsultid','#$scheduledate','#$starttime','#$endtime','#$emergencystarttime','#$emergencyendtime','#$patientidval','#$patientenquiryidval')".as[(Int)]

}