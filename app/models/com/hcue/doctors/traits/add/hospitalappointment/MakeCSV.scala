package controllers.doctors.API.Doctor

import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import scala.util.Random
import java.io.BufferedWriter
import java.io.FileWriter
import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB
import play.Logger
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models.com.hcue.doctors.traits.dbProperties.db
import au.com.bytecode.opencsv.CSVWriter
import models.com.hcue.doctors.helper.hcueCommunication
import scala.concurrent.Future
import org.postgresql.ds.PGSimpleDataSource
import java.text.SimpleDateFormat
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.StaticQuery
import scala.slick.jdbc.Invoker
import scala.slick.jdbc.StaticQuery.interpolation
import java.util.Calendar
import play.Logger
import play.api.db.slick.DB
import play.api.libs.json.JsValue
import play.api.libs.json
import play.api.libs.json.Json
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker
import java.io.File
import scala.concurrent.Future
import play.api.Play.current

//imports for excel file generate

import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.util.IOUtils;
import java.io.FileOutputStream
import java.io.File
import org.apache.poi.ss.util.CellRangeAddress
import java.io.InputStream
import java.io.FileInputStream
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.CreationHelper
import org.apache.poi.ss.usermodel.Drawing
import org.apache.poi.ss.usermodel.ClientAnchor
import org.apache.poi.ss.usermodel.Picture
import java.lang.Short
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.util.CellRangeAddressList
import org.apache.poi.hssf.usermodel.DVConstraint
import org.apache.poi.ss.usermodel.DataValidation
import org.apache.poi.hssf.usermodel.HSSFDataValidation
import org.apache.poi.ss.usermodel.DataValidationHelper
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper
import org.apache.poi.ss.usermodel.DataValidationConstraint
import org.apache.poi.xssf.usermodel.XSSFColor
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.hssf.util.HSSFColor.WHITE
import org.apache.poi.xssf.usermodel.XSSFFont
import org.apache.poi.ss.usermodel.BorderStyle
import java.net.URI
import java.net.URL
import java.awt.Image
import javax.imageio.ImageIO
import java.io.ByteArrayOutputStream
import org.apache.poi.util.Units
import org.apache.poi.ss.usermodel.Comment
import org.apache.poi.ss.usermodel.RichTextString
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint
import org.apache.poi.ss.usermodel.DataValidationConstraint.OperatorType
import org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType



object MakeCSV {
  
  
  var path = current.configuration.getString("temp_folder").get

  /* def generateCSV(appointmentidlist:List[Long],patientnamelist:List[String],statuslist:List[String],doctornamelist:List[String],
      appointmenttimelist:List[String],specialitylist:List[String],appointmentdate:List[java.sql.Date]) = {*/

  def generateCSV(resultset: List[(Long, String, String, String, String, String, String, String, String)], tomail: String, doctorid: Long, hospitalname: String, schedulestarttime: String, scheduleendtime: String,
                  sonographername: String, cswname: String) = {

    val appointmentidlist = resultset.map(f => f._1)
    val patientnamelist = resultset.map(f => f._2)
    val statuslist = resultset.map(f => f._3)
    val doctornamelist = resultset.map(f => f._4)
    val appointmenttimelist = resultset.map(f => f._5)
    val specialitylist = resultset.map(f => f._6)
    val subspecialitylist = resultset.map(f => f._7)
    val appointmentdate = resultset.map(f => f._8)
    val clinicnamelist = resultset.map(f => f._9)

    val fileName = "sonographer" + doctorid.toString();
    val CSVFile = fileName + ".csv";

    println("path file --------- " + path)
    val outputFile = new BufferedWriter(new FileWriter(path + "/" + CSVFile)) //replace the path with the desired path and filename with the desired filename
    val csvWriter = new CSVWriter(outputFile)

    val emptyfiled = Array("", "")
    val hospitalnamefield = Array("ClinicName", hospitalname)
    val datefield = Array("ScheduleDate", appointmentdate(0))
    val sonographerfield = Array("SonographerName", sonographername)
    val cswfield = Array("SupprotWorkerName", cswname)
    val starttimefield = Array("StartTime", schedulestarttime)
    val endtimefield = Array("EndTime", scheduleendtime)

    val csvFields = Array("Srlno", "Appointment Date", "Appointment Time", "Sonographer", "Patient Name", "Speciality", "Subspeciality", "Appointment Status","ClinicName")
    val random = new Random()
    var listOfRecords = new ListBuffer[Array[String]]()

    if (hospitalname != "") {
      listOfRecords += hospitalnamefield
      listOfRecords += datefield
      listOfRecords += sonographerfield
      listOfRecords += cswfield
      listOfRecords += starttimefield
      listOfRecords += endtimefield
      listOfRecords += emptyfiled
    }
    listOfRecords += csvFields

    for (i <- 0 until appointmentidlist.length) {
      listOfRecords += Array(
        (i + 1).toString, appointmentdate(i).toString(),
        appointmenttimelist(i),
        doctornamelist(i),
        patientnamelist(i),
        specialitylist(i),
        subspecialitylist(i),
        statuslist(i),
        clinicnamelist(i))
    }

    csvWriter.writeAll(listOfRecords.toList)
    outputFile.close()
    sendAttachment(0L, 0L, tomail, CSVFile)

  }
  
  
    
  def generatecswCSV(resultset:List[(Long, String, Long, String, String, String, String,String,String,String,String,String)],tomail: String, doctorid: Long,hospitalname: String,schedulestarttime: String,scheduleendtime: String,
      sonographername: String,cswname: String) = {
   
    val appointmentidlist = resultset.map(f => f._1)
             val patientnamelist = resultset.map(f => f._2)
             val patientidlist = resultset.map(f => f._3)
             val statuslist = resultset.map(f => f._4)
             val doctornamelist = resultset.map(f => f._5)
             val appointmenttimelist = resultset.map(f => f._6)
             val specialitylist = resultset.map(f => f._7)
             val subspecialitylist = resultset.map(f => f._8)
             val appointmentdate = resultset.map(f => f._9)
             val scanstarttime = resultset.map(f => f._10)
             val scanendtime = resultset.map(f => f._11)
             val clinicnamelist = resultset.map(f => f._12)
             
             val fileName="supportworker"+doctorid.toString();
                val CSVFile = fileName + ".csv";

    val outputFile = new BufferedWriter(new FileWriter(path+"/"+CSVFile))         
   //val outputFile = new BufferedWriter(new FileWriter("F:/sonographer.csv")) //replace the path with the desired path and filename with the desired filename
    val csvWriter = new CSVWriter(outputFile)
    
    val emptyfiled = Array("","")
    val hospitalnamefield = Array("ClinicName",hospitalname)
    val datefield = Array("ScheduleDate",appointmentdate(0))
    val sonographerfield = Array("SonographerName",sonographername)
    val cswfield = Array("SupprotWorkerName",cswname)
    val starttimefield = Array("StartTime",schedulestarttime)
    val endtimefield = Array("EndTime",scheduleendtime)
    
    //val csvFields = Array("Srlno","Appointment Date", "Appointment Time","Support Worker","Patient Name", "Speciality","Subspeciality","Appointment Status","Key","No of Areas","Patient Collected","Ready for Next Patient","ClinicName","Survey Completed")
    val csvFields = Array("Srlno", "Appointment Time","Patient Name", "Speciality","Key","No of Areas","Patient Collected","Ready for Next Patient","Survey Completed")
    val random = new Random()
    var listOfRecords = new ListBuffer[Array[String]]()
    
     if(hospitalname !="")
    {
    listOfRecords+= hospitalnamefield
    listOfRecords+= datefield
    listOfRecords+= sonographerfield
    listOfRecords+= cswfield
    listOfRecords+= starttimefield
    listOfRecords+= endtimefield 
    listOfRecords += emptyfiled
    }
    
    listOfRecords += csvFields

    for (i <- 0 until appointmentidlist.length) {
      listOfRecords += Array(
        (i+1).toString, 
        //appointmentdate(i).toString(),
        appointmenttimelist(i),
        //doctornamelist(i),
        patientnamelist(i),
        specialitylist(i),
        //subspecialitylist(i),
        //statuslist(i),
        "",
        "",
        "",
        "",
        //clinicnamelist(i),
        ""
        )
    }

    csvWriter.writeAll(listOfRecords.toList)
    outputFile.close()
    sendAttachmentcsw(0L, 0L,tomail,CSVFile)

  }

  
  
  def generatecswCSVnew(resultset:List[(Long, String, String, String, String, String,String,String,String,String,String)],tomail: String, doctorid: Long,hospitalname: String,schedulestarttime: String,scheduleendtime: String,
      sonographername: String,cswname: String) = {
   
      val appointmentidlist = resultset.map(f => f._1)
             val patientnamelist = resultset.map(f => f._2)
             val statuslist = resultset.map(f => f._3)
             val doctornamelist = resultset.map(f => f._4)
             val appointmenttimelist = resultset.map(f => f._5)
             val specialitylist = resultset.map(f => f._6)
             val subspecialitylist = resultset.map(f => f._7)
             val appointmentdate = resultset.map(f => f._8)
             val scanstarttime = resultset.map(f => f._9)
             val scanendtime = resultset.map(f => f._10)
             val clinicnamelist = resultset.map(f => f._11)
             
             val fileName="supportworker"+doctorid.toString();
                val CSVFile = fileName + ".csv";

    val outputFile = new BufferedWriter(new FileWriter(path+"/"+CSVFile))         
   //val outputFile = new BufferedWriter(new FileWriter("F:/sonographer.csv")) //replace the path with the desired path and filename with the desired filename
    val csvWriter = new CSVWriter(outputFile)
    
    val emptyfiled = Array("","")
    val hospitalnamefield = Array("Clinic Name",hospitalname)
    val datefield = Array("Schedule Date",appointmentdate(0))
    val sonographerfield = Array("Sonographer Name",sonographername)
    val cswfield = Array("Supprot Worker Name",cswname)
    val starttimefield = Array("Start Time",schedulestarttime)
    val endtimefield = Array("End Time",scheduleendtime)
    
    //val csvFields = Array("Srlno","Appointment Time","Patient Initials", "Subspeciality","Key","No of Areas","Patient Collected","Ready for next patient","Survey Completed")
    //val csvFields = Array("Srlno","Appointment Date", "Appointment Time","Support Worker","Patient Name", "Speciality","Subspeciality","Appointment Status","Key","No of Areas","Patient Collected","Ready for Next Patient","ClinicName","Survey Completed")
    
    val csvFields = Array("Srlno","Appointment Time","Patient Name", "Sub-Specialities","Key","No of Areas","Patient Collected","Ready for Next Patient","Survey Completed")
    
    
    val random = new Random()
    var listOfRecords = new ListBuffer[Array[String]]()
    
     if(hospitalname !="")
    {
    listOfRecords+= hospitalnamefield
    listOfRecords+= datefield
    listOfRecords+= sonographerfield
    listOfRecords+= cswfield
    listOfRecords+= starttimefield
    listOfRecords+= endtimefield 
    listOfRecords += emptyfiled
    }
    
    listOfRecords += csvFields

    for (i <- 0 until appointmentidlist.length) {
      listOfRecords += Array(
        (i+1).toString, 
        //appointmentdate(i).toString(),
        appointmenttimelist(i),
        //doctornamelist(i),
        patientnamelist(i),
        //specialitylist(i),
        subspecialitylist(i),
        //statuslist(i),
        "",
        "",
        "",
        "",
        //clinicnamelist(i),
        ""
        )
    }

    csvWriter.writeAll(listOfRecords.toList)
    outputFile.close()
    
    sendAttachmentcsw(0L, 0L,tomail,CSVFile)

  }
  
  
  
  def generatecswexcel(resultset:List[(Long, String, Long,String, String, String, String,String,String,String,String,String)],tomail: String, doctorid: Long,hospitalname: String,schedulestarttime: String,scheduleendtime: String,
      sonographername: String,cswname: String) = {
   
      val appointmentidlist = resultset.map(f => f._1)
             val patientnamelist = resultset.map(f => f._2)
             val patientidlist = resultset.map(f => f._3)
             val statuslist = resultset.map(f => f._4)
             val doctornamelist = resultset.map(f => f._5)
             val appointmenttimelist = resultset.map(f => f._6)
             val specialitylist = resultset.map(f => f._7)
             val subspecialitylist = resultset.map(f => f._8)
             val appointmentdate = resultset.map(f => f._9)
             val scanstarttime = resultset.map(f => f._10)
             val scanendtime = resultset.map(f => f._11)
             val clinicnamelist = resultset.map(f => f._12)
             
             val fileName="supportworker"+doctorid.toString();
             val filename = fileName + ".xlsx";
             
     var patientcapacity = patientnamelist.length;       
             
     var keyval :Array[String] = Array("V", "E", "D" )
     var TVval :Array[String] = Array(" ", "Y")
     var DVTval :Array[String] = Array(" ", "Y")
     var servayval :Array[String] = Array("Y", "N")        
    
    
     val workbook: XSSFWorkbook = new XSSFWorkbook();
     val sheet: XSSFSheet = workbook.createSheet("Sheet1");
     var style:CellStyle = workbook.createCellStyle();
     var wraptextstyle:CellStyle = workbook.createCellStyle();
     
     var dateformatstyle :CellStyle = workbook.createCellStyle();
     var hrsminutesstyle :CellStyle = workbook.createCellStyle();
     var hmcolorstyle :CellStyle = workbook.createCellStyle();
     var normalstyle:CellStyle = workbook.createCellStyle();
     
     sheet.setColumnWidth(0, 3000);
     sheet.setColumnWidth(1, 3500);
     sheet.setColumnWidth(2, 3500);
     sheet.setColumnWidth(3, 8000);
     sheet.setColumnWidth(9, 5000);
     sheet.setColumnWidth(10, 4700);
     sheet.setColumnWidth(11, 8000);
     
     style.setBorderBottom(BorderStyle.MEDIUM);
		 style.setBorderLeft(BorderStyle.MEDIUM);
		 style.setBorderRight(BorderStyle.MEDIUM);
		 style.setBorderTop(BorderStyle.MEDIUM);
		 
		 wraptextstyle.setBorderBottom(BorderStyle.MEDIUM);
		 wraptextstyle.setBorderLeft(BorderStyle.MEDIUM);
		 wraptextstyle.setBorderRight(BorderStyle.MEDIUM);
		 wraptextstyle.setBorderTop(BorderStyle.MEDIUM);
		  
		 dateformatstyle.setBorderBottom(BorderStyle.MEDIUM);
		 dateformatstyle.setBorderLeft(BorderStyle.MEDIUM);
		 dateformatstyle.setBorderRight(BorderStyle.MEDIUM);
		 dateformatstyle.setBorderTop(BorderStyle.MEDIUM);
		 
		 hrsminutesstyle.setBorderBottom(BorderStyle.MEDIUM);
		 hrsminutesstyle.setBorderLeft(BorderStyle.MEDIUM);
		 hrsminutesstyle.setBorderRight(BorderStyle.MEDIUM);
		 hrsminutesstyle.setBorderTop(BorderStyle.MEDIUM);
		 
		 hmcolorstyle.setBorderBottom(BorderStyle.MEDIUM);
		 hmcolorstyle.setBorderLeft(BorderStyle.MEDIUM);
		 hmcolorstyle.setBorderRight(BorderStyle.MEDIUM);
		 hmcolorstyle.setBorderTop(BorderStyle.MEDIUM);
		 
		 normalstyle.setBorderBottom(BorderStyle.MEDIUM);
		 normalstyle.setBorderLeft(BorderStyle.MEDIUM);
		 normalstyle.setBorderRight(BorderStyle.MEDIUM);
		 normalstyle.setBorderTop(BorderStyle.MEDIUM);
     var font: Font= workbook.createFont();//Create font
     var whitefont: Font= workbook.createFont();//Create white font
     
     font.setBold(true);//Make font bold
     style.setFont(font);//set it to bold
     dateformatstyle.setFont(font);//set it to bold
     hrsminutesstyle.setFont(font);//set it to bold
     

     whitefont.setColor(WHITE.index)
     
     var row0: Row = sheet.createRow(0);
     var ycell: Cell = row0.createCell(14);
     var ncell: Cell = row0.createCell(15);
     
     
     ycell.setCellValue("Y");
     ncell.setCellValue("N");
     
     var row1: Row = sheet.createRow(1);
     var row2: Row = sheet.createRow(2);
     var officeuselablecell: Cell = row1.createCell(15);
     officeuselablecell.setCellValue("OFFICE USE ONLY");
     
     var redcolorStyle:CellStyle = workbook.createCellStyle();
     redcolorStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
     redcolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     
     
     var yellowcolorStyle:CellStyle = workbook.createCellStyle();
     yellowcolorStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
     yellowcolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     
     var lightbluecolorStyle:CellStyle = workbook.createCellStyle();
     
     lightbluecolorStyle.setBorderBottom(BorderStyle.MEDIUM);
		 lightbluecolorStyle.setBorderLeft(BorderStyle.MEDIUM);
		 lightbluecolorStyle.setBorderRight(BorderStyle.MEDIUM);
		 lightbluecolorStyle.setBorderTop(BorderStyle.MEDIUM);
		 
     //bluecolorStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
     lightbluecolorStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
     lightbluecolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     lightbluecolorStyle.setFont(whitefont)
     
     var bluecolorStyle:CellStyle = workbook.createCellStyle();
     
     bluecolorStyle.setBorderBottom(BorderStyle.MEDIUM);
		 bluecolorStyle.setBorderLeft(BorderStyle.MEDIUM);
		 bluecolorStyle.setBorderRight(BorderStyle.MEDIUM);
		 bluecolorStyle.setBorderTop(BorderStyle.MEDIUM);
		 
     //bluecolorStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
     bluecolorStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
     bluecolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     bluecolorStyle.setFont(whitefont)
     
     var OrangecolorStyle:CellStyle = workbook.createCellStyle();
     //bluecolorStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
     
     OrangecolorStyle.setBorderBottom(BorderStyle.MEDIUM);
		 OrangecolorStyle.setBorderLeft(BorderStyle.MEDIUM);
		 OrangecolorStyle.setBorderRight(BorderStyle.MEDIUM);
		 OrangecolorStyle.setBorderTop(BorderStyle.MEDIUM);
			
     /*OrangecolorStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
     OrangecolorStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
     OrangecolorStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
     OrangecolorStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex()); */
     OrangecolorStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
     OrangecolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     
     hmcolorstyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
     hmcolorstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     
     wraptextstyle.setWrapText(true);
     
     
     
     var createHelper : CreationHelper = workbook.getCreationHelper();
     hrsminutesstyle.setDataFormat(createHelper.createDataFormat().getFormat("HH:MM"));
     hmcolorstyle.setDataFormat(createHelper.createDataFormat().getFormat("hh:mm"));
     dateformatstyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mm-yyyy"));

     
     
     var counter = 1
     
     sheet.addMergedRegion(new CellRangeAddress(3,3,5,10));
     
     
     var row4: Row = sheet.createRow(3);
     var descriptioncell1: Cell = row4.createCell(5);
     //descriptioncell1.setCellStyle(normalstyle);

     descriptioncell1.setCellValue("Below to be completed by CSW / Clinician at BEGINNING and END of clinic ");
     
     var row5: Row = sheet.createRow(4);
     var datelablecell: Cell = row5.createCell(9);
     var datevaluecell: Cell = row5.createCell(10);
     
     datelablecell.setCellValue("Session Date:  ");
     datevaluecell.setCellValue(appointmentdate(0));
     
     datelablecell.setCellStyle(style);
     datevaluecell.setCellStyle(style);
     
     var commonvalidationHelper : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var commonconstraint : DataValidationConstraint  = commonvalidationHelper.createExplicitListConstraint(keyval);
     var dvConstraint : DataValidationConstraint = commonvalidationHelper.createDateConstraint(OperatorType.GREATER_THAN, "2019-01-01", "=DATE(2119,12,31)", "yyyyy-mm-dd");
     var timeConstraint : DataValidationConstraint = commonvalidationHelper.createTimeConstraint(OperatorType.BETWEEN, "=TIME(0,0,0)", "=TIME(23,59,59)");
     
     var dateList : CellRangeAddressList  = new CellRangeAddressList(4, 4, 10, 10);
     var datedataValidation : DataValidation  = commonvalidationHelper.createValidation(dvConstraint, dateList);
     datedataValidation.createPromptBox("Input Date Clinic Held", "Input the Date the clinic is being held at the start of clinic - must be in format DD-MM-YYYY");
     datedataValidation.setShowPromptBox(true)
     datedataValidation.setSuppressDropDownArrow(false); 
     datedataValidation.setShowErrorBox(true);
     datedataValidation.setErrorStyle(0);
     datedataValidation.createErrorBox("Input Date Clinic Held", "Input the Date the clinic is being held at the start of clinic - must be in format DD-MM-YYYY");
     sheet.addValidationData(datedataValidation);
    
     
     var row6: Row = sheet.createRow(5);
     var sonographerlablecell: Cell = row6.createCell(0);
     var sonographervaluecell: Cell = row6.createCell(1);
     
     sonographerlablecell.setCellValue("Sonographer / Radiologist Name: ");
     sonographervaluecell.setCellValue(sonographername);
     

     sonographerlablecell.setCellStyle(style);
     sonographervaluecell.setCellStyle(hrsminutesstyle);
     
     var SonographerList : CellRangeAddressList  = new CellRangeAddressList(5, 5, 1, 1);
     var SononamedataValidation : DataValidation  = commonvalidationHelper.createValidation(commonconstraint, SonographerList);
     SononamedataValidation.createPromptBox("Lead Clinician Name", "Input the name of the lead clinician (Sonographer, Radiographer etc) in this cell at the start of clinic");
     SononamedataValidation.setShowPromptBox(true)
     SononamedataValidation.setSuppressDropDownArrow(false); 
     sheet.addValidationData(SononamedataValidation); 
     
     var starttimelablecell: Cell = row6.createCell(9);
     var starttimevaluecell: Cell = row6.createCell(10);
     
     starttimelablecell.setCellValue("Session Start Time:");
     starttimevaluecell.setCellValue(schedulestarttime);
     
     starttimelablecell.setCellStyle(style);
     starttimevaluecell.setCellStyle(hrsminutesstyle);
     
     var starttimeList : CellRangeAddressList  = new CellRangeAddressList(5, 5, 10, 10);
     var starttimeValidation : DataValidation  = commonvalidationHelper.createValidation(timeConstraint, starttimeList);
     starttimeValidation.createPromptBox("Clinic Start and End Times", "Input the time the clinic begins (First patient arrives) and the time the clinic ends (Last patient leaves) in time format (e.g. HH:MM)");
     starttimeValidation.setShowPromptBox(true)
     starttimeValidation.setSuppressDropDownArrow(false); 
     starttimeValidation.setShowErrorBox(true);
     starttimeValidation.setErrorStyle(0);
     starttimeValidation.createErrorBox("Clinic Start and End Times", "Input the time the clinic begins (First patient arrives) and the time the clinic ends (Last patient leaves) in time format (e.g. HH:MM)");
     sheet.addValidationData(starttimeValidation); 
     
     
     var clinicdurcell: Cell = row6.createCell(15);
     clinicdurcell.setCellValue("Clinic Duration");
     
     var row7: Row = sheet.createRow(6);
     
     var cswnamelablecell: Cell = row7.createCell(0);
     var cswnamevaluecell: Cell = row7.createCell(1);
     
     cswnamelablecell.setCellValue("Clinical Support Staff Name:");
     cswnamevaluecell.setCellValue(cswname);
     
     cswnamelablecell.setCellStyle(style);
     cswnamevaluecell.setCellStyle(style);
     
     var cswnameList : CellRangeAddressList  = new CellRangeAddressList(6, 6, 1, 1);
     var cswnameValidation : DataValidation  = commonvalidationHelper.createValidation(commonconstraint, cswnameList);
     cswnameValidation.createPromptBox("Supporting Clinician Name", "The Name of the Supporting Clinician if applicable must be input ot this cell at the start of clinic");
     cswnameValidation.setShowPromptBox(true)
     cswnameValidation.setSuppressDropDownArrow(false); 
     sheet.addValidationData(cswnameValidation);
     
     
     var endtimelablecell: Cell = row7.createCell(9);
     var endtimevaluecell: Cell = row7.createCell(10);
     
     endtimelablecell.setCellValue("Session End Time:");
     endtimevaluecell.setCellValue(scheduleendtime);
     
     endtimelablecell.setCellStyle(style);
     endtimevaluecell.setCellStyle(hrsminutesstyle);
     
     var endtimeList : CellRangeAddressList  = new CellRangeAddressList(6, 6, 10, 10);
     var endtimeValidation : DataValidation  = commonvalidationHelper.createValidation(timeConstraint, endtimeList);
     endtimeValidation.createPromptBox("Clinic Start and End Times ", "Input the time the clinic begins (First patient arrives) and the time the clinic ends (Last patient leaves) in time format (e.g. HH:MM)");
     endtimeValidation.setShowPromptBox(true)
     endtimeValidation.setSuppressDropDownArrow(false); 
     endtimeValidation.setShowErrorBox(true);
     endtimeValidation.setErrorStyle(0);
     endtimeValidation.createErrorBox("Clinic Start and End Times", "Input the time the clinic begins (First patient arrives) and the time the clinic ends (Last patient leaves) in time format (e.g. HH:MM)");
     
     sheet.addValidationData(endtimeValidation);
     
     var durationcell: Cell = row7.createCell(14);
     durationcell.setCellValue("Duration");
     var numericalmincell: Cell = row7.createCell(15);
     numericalmincell.setCellValue("Numerical Mins");
     var numericalhrscell: Cell = row7.createCell(16);
     numericalhrscell.setCellValue("Numerical Hours");
     
     var row8: Row = sheet.createRow(7);
     
     var locationlablecell: Cell = row8.createCell(0);
     var locationvaluecell: Cell = row8.createCell(1);
     
     locationlablecell.setCellValue("Clinic Location / Practice Name:");
     locationvaluecell.setCellValue(hospitalname);
     
     locationlablecell.setCellStyle(style);
     locationvaluecell.setCellStyle(style);
     
     var locationList : CellRangeAddressList  = new CellRangeAddressList(7, 7, 1, 1);
     var locationValidation : DataValidation  = commonvalidationHelper.createValidation(commonconstraint, locationList);
     locationValidation.createPromptBox("Location Clinic is being Held ", "Please input the location of the clinic (Practice Name) in this cell at the start of clinic ");
     locationValidation.setShowPromptBox(true)
     locationValidation.setSuppressDropDownArrow(false); 
     sheet.addValidationData(locationValidation);
     
     var capacitylablecell: Cell = row8.createCell(9);
     var capacityvaluecell: Cell = row8.createCell(10);
     
     capacitylablecell.setCellValue("Patient Capacity:");
     capacityvaluecell.setCellValue(patientcapacity);
     
     capacitylablecell.setCellStyle(style);
     capacityvaluecell.setCellStyle(style);
     
     var capacityList : CellRangeAddressList  = new CellRangeAddressList(7, 7, 10, 10);
     var capacityconstraint : DataValidationConstraint  = commonvalidationHelper.createNumericConstraint(ValidationType.INTEGER, OperatorType.BETWEEN, "0", "60")
     var capacityValidation : DataValidation  = commonvalidationHelper.createValidation(capacityconstraint, capacityList);
     capacityValidation.createPromptBox("Total Capacity of Clinic", "The total capacity of the clinic can be calculated by adding the total amount of empty appointment slots (if any) to the total number of patients booked (please see cell U13 for patients booked figure)- Must be a full number ");
     capacityValidation.setShowPromptBox(true)
     capacityValidation.setSuppressDropDownArrow(false);
     capacityValidation.setShowErrorBox(true);
     capacityValidation.setErrorStyle(0);
     capacityValidation.createErrorBox("Total Capacity of Clinic", "The total capacity of the clinic can be calculated by adding the total amount of empty appointment slots (if any) to the total number of patients booked (please see cell U13 for patients booked figure)- Must be a full number ");   
     sheet.addValidationData(capacityValidation);
     
     var durationvalcell: Cell = row8.createCell(14);
     durationvalcell.setCellFormula("IF(K7-K6<0,\"Error\",IF(AND(K6<>\"\",K7<>\"\"),K7-K6,\"\"))")
     var numericalminvalcell: Cell = row8.createCell(15);
     numericalminvalcell.setCellFormula("IF(K6<0,\"\",ROUND((O8*1440),0))")
     var numericalhrsvalcell: Cell = row8.createCell(16);
     numericalhrsvalcell.setCellFormula("IFERROR(P8/60,\"\")");
     
      
     var row9: Row = sheet.createRow(8);
     var row10: Row = sheet.createRow(9);
     
     var descriptioncell2: Cell = row9.createCell(0);
     var descriptioncell3: Cell = row10.createCell(0);
     sheet.addMergedRegion(new CellRangeAddress(8,8,0,10));
     //sheet.addMergedRegion(new CellRangeAddress(9,9,4,11));
     sheet.addMergedRegion(new CellRangeAddress(9,9,0,10));
     
     
     descriptioncell2.setCellValue("All Cells MUST be completed - Records MUST be kept DURING clinic - Worksheets MUST be submitted at the End of Clinic - System Sync MUST be done at the End of Clinic -");
     descriptioncell3.setCellValue("***  THESE ARE MANDATORY ACTIONS ***");
     //redcolorStyle.setAlignment(CellStyle.ALIGN_CENTER);
     redcolorStyle.setAlignment(HorizontalAlignment.CENTER);
     descriptioncell2.setCellStyle(redcolorStyle);
     descriptioncell3.setCellStyle(redcolorStyle);
     
     var row11: Row = sheet.createRow(10);
     sheet.addMergedRegion(new CellRangeAddress(10,10,4,7));
     var descriptioncell4: Cell = row11.createCell(4);
     descriptioncell4.setCellValue("DHSL.worksheets@nhs.net");
     yellowcolorStyle.setAlignment(HorizontalAlignment.CENTER);
     descriptioncell4.setCellStyle(yellowcolorStyle)
     
     
     var row12: Row = sheet.createRow(11);
     sheet.addMergedRegion(new CellRangeAddress(11,11,0,3));
     var descriptioncell5: Cell = row12.createCell(0);
     //descriptioncell5.setCellStyle(normalstyle);
     //descriptioncell5.setCellValue("Below to be completed by Head Office - Copy and Paste Value from AMP worksheet download");
     
     sheet.addMergedRegion(new CellRangeAddress(11,11,4,10));
     var descriptioncell6: Cell = row12.createCell(4);
     descriptioncell6.setCellValue("Below to be completed by CSW / Clinician DURING Clinic ");
     //descriptioncell6.setCellStyle(normalstyle);
     
     var headerlist = List("Appt Time","DW Reference","Patient Initials","Examination Type","Key","TV","DVT","No of Areas","Patient Collected","Ready for next patient","Survey Completed","Notes") 
     var headerrow: Row = sheet.createRow(12);
     for (m1 <- 0 to headerlist.length - 1) {
     var headercell: Cell = headerrow.createCell(m1);
     headercell.setCellValue(headerlist(m1));
     //headercell.setCellStyle(style);
     headercell.setCellStyle(lightbluecolorStyle);
      }
     
     var scandurationcell: Cell = headerrow.createCell(15);
     scandurationcell.setCellValue("Scan Durations");
     
          
     var detaillength = appointmentidlist.length;
     var looplength = 50;
     if(detaillength>looplength)
     {
       looplength = detaillength;
     }
     
     
     for (n1 <- 0 to looplength-1) {
       
     //for (n1 <- 0 to 3) {
     var rowint = 13 +n1;
     var rowintformula = rowint + 1;
     
     var detailrow: Row = sheet.createRow(rowint);
     var apptimecell: Cell = detailrow.createCell(0);
     var dwreferencecell: Cell = detailrow.createCell(1);
     var patientintcell: Cell = detailrow.createCell(2);
     var examtypecell: Cell = detailrow.createCell(3);
     //apptimecell.setCellValue(apptimelist(n1));
     if(n1<detaillength)
     {
     apptimecell.setCellValue(appointmenttimelist(n1));
     }
     
     var apptimeceList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 0, 0);
     var apptimeValidation : DataValidation  = commonvalidationHelper.createValidation(commonconstraint, apptimeceList);
     apptimeValidation.createPromptBox("To be completed by Head office", "This Section of the worksheet should be completed by Head office and sent to the CSW or clinician before the clinic starts");
     apptimeValidation.setShowPromptBox(true)
     apptimeValidation.setSuppressDropDownArrow(false); 
     sheet.addValidationData(apptimeValidation);
     
     //patientintcell.setCellValue("TS");
     if(n1<detaillength)
     {
     patientintcell.setCellValue(patientnamelist(n1));
     }
     apptimecell.setCellStyle(normalstyle);
     patientintcell.setCellStyle(normalstyle);
     
     var patientintList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 2, 2);
     var patientintValidation : DataValidation  = commonvalidationHelper.createValidation(commonconstraint, patientintList);
     patientintValidation.createPromptBox("To be completed by Head office", "This Section of the worksheet should be completed by Head office and sent to the CSW or clinician before the clinic starts");
     patientintValidation.setShowPromptBox(true)
     patientintValidation.setSuppressDropDownArrow(false); 
     sheet.addValidationData(patientintValidation);
     
     if(n1<detaillength)
     {
     dwreferencecell.setCellValue(patientidlist(n1));
     }
     dwreferencecell.setCellStyle(wraptextstyle);
     
     var dwreferenceList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 1, 1);
     var dwreferenceValidation : DataValidation  = commonvalidationHelper.createValidation(commonconstraint, dwreferenceList);
     dwreferenceValidation.createPromptBox("To be completed by Head office", "This Section of the worksheet should be completed by Head office and sent to the CSW or clinician before the clinic starts");
     dwreferenceValidation.setShowPromptBox(true)
     dwreferenceValidation.setSuppressDropDownArrow(false); 
     sheet.addValidationData(dwreferenceValidation);
     
      if(n1<detaillength)
     {
     examtypecell.setCellValue(subspecialitylist(n1));
     }
     examtypecell.setCellStyle(wraptextstyle);
     
     var examtypeList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 3, 3);
     var examtypeValidation : DataValidation  = commonvalidationHelper.createValidation(commonconstraint, examtypeList);
     examtypeValidation.createPromptBox("To be completed by Head office", "This Section of the worksheet should be completed by Head office and sent to the CSW or clinician before the clinic starts");
     examtypeValidation.setShowPromptBox(true)
     examtypeValidation.setSuppressDropDownArrow(false); 
     sheet.addValidationData(examtypeValidation);
     
     
     //drop down list assign in coulm header key
     var keycell: Cell = detailrow.createCell(4);
     keycell.setCellStyle(OrangecolorStyle);
     var keyList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 4, 4);
     var validationHelper : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var constraint : DataValidationConstraint  = validationHelper.createExplicitListConstraint(keyval);
     var dataValidation : DataValidation  = validationHelper.createValidation(constraint, keyList);
     dataValidation.setSuppressDropDownArrow(true); 
     dataValidation.createErrorBox("Bad Value", "Please select value from drop down list!");
     dataValidation.setShowErrorBox(true);
     dataValidation.createPromptBox("Appointment Indicatior", "Input from list below or select from drop down menu \n V = Verified / Patient Seen   \n E = Escalations (High priority / Urgent) \n D = Patient Did Not Attend");
     dataValidation.setShowPromptBox(true)
     sheet.addValidationData(dataValidation); 

    
     
     var TVcell: Cell = detailrow.createCell(5);
     TVcell.setCellStyle(OrangecolorStyle);
     var TVList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 5, 5);
     var validationHelper1 : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var constraint1 : DataValidationConstraint  = validationHelper1.createExplicitListConstraint(TVval);
     var dataValidation1 : DataValidation  = validationHelper1.createValidation(constraint1, TVList);
     dataValidation1.setSuppressDropDownArrow(true); 
     dataValidation1.createErrorBox("Bad Value", "Please select value from drop down list!");
     dataValidation1.setShowErrorBox(true);
     dataValidation1.createPromptBox("Input Y or leave Blank ONLY", "Select Y if the \"specialist\" scan type in this column header has been completed during the appointment Leave Blank if the the \"specialist\" scan type was NOT completed");
     dataValidation1.setShowPromptBox(true)
     sheet.addValidationData(dataValidation1); 
     
     var DVTcell: Cell = detailrow.createCell(6);
     DVTcell.setCellStyle(OrangecolorStyle);
     var DVTList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 6, 6);
     var validationHelper2 : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var constraint2 : DataValidationConstraint  = validationHelper2.createExplicitListConstraint(DVTval);
     var dataValidation2 : DataValidation  = validationHelper2.createValidation(constraint2, DVTList);
     dataValidation2.setSuppressDropDownArrow(true); 
     dataValidation2.createErrorBox("Bad Value", "Please select value from drop down list!");
     dataValidation2.setShowErrorBox(true);
     dataValidation2.createPromptBox("Input Y or leave Blank ONLY", "Select Y if the \"specialist\" scan type in this column header has been completed during the appointment Leave Blank if the the \"specialist\" scan type was NOT completed");
     dataValidation2.setShowPromptBox(true)
     sheet.addValidationData(dataValidation2); 
     
     var areacell: Cell = detailrow.createCell(7);
     areacell.setCellStyle(OrangecolorStyle);
     
     var areaList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 7, 7);
     var areaHelper2 : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     
     var areaconstraint : DataValidationConstraint  = areaHelper2.createNumericConstraint(ValidationType.INTEGER, OperatorType.BETWEEN, "0", "5")
     var areaValidation : DataValidation  = commonvalidationHelper.createValidation(areaconstraint, areaList);
     
     areaValidation.createPromptBox("Number of Areas / Body Parts", "Input the number of areas / body parts scanned during the appointment in numerical format only");
     areaValidation.setShowPromptBox(true)
     areaValidation.setSuppressDropDownArrow(false);
     areaValidation.setShowErrorBox(true);
     areaValidation.setErrorStyle(0);
     areaValidation.createErrorBox("Number of Areas / Body Parts", "Input the number of areas / body parts scanned during the appointment in numerical format only");   
     sheet.addValidationData(areaValidation);
     
     
     
     var collectedcell: Cell = detailrow.createCell(8); 
     //collectedcell.setCellStyle(OrangecolorStyle);
     collectedcell.setCellStyle(hmcolorstyle);
     
     
     var collectedList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 8, 8);
     var collectedValidation : DataValidation  = commonvalidationHelper.createValidation(timeConstraint, collectedList);
     collectedValidation.createPromptBox("Time Format Only ", "Please use time format HH:MM for the Worksheet to accept Start and Finish times");
     collectedValidation.setShowPromptBox(true)
     collectedValidation.setSuppressDropDownArrow(false); 
     collectedValidation.setShowErrorBox(true);
     collectedValidation.setErrorStyle(0);
     collectedValidation.createErrorBox("Time Format Only ", "Please use time format HH:MM for the Worksheet to accept Start and Finish times");   
     sheet.addValidationData(collectedValidation);
     
     
     var nextpatientcell: Cell = detailrow.createCell(9);
     nextpatientcell.setCellStyle(hmcolorstyle);
     
     var nextpatientList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 9, 9);
     var nextpatientValidation : DataValidation  = commonvalidationHelper.createValidation(timeConstraint, nextpatientList);
     nextpatientValidation.createPromptBox("Time Format Only ", "Please use time format HH:MM for the Worksheet to accept Start and Finish times");
     nextpatientValidation.setShowPromptBox(true)
     nextpatientValidation.setSuppressDropDownArrow(false);
     nextpatientValidation.setShowErrorBox(true);
     nextpatientValidation.setErrorStyle(0);
     nextpatientValidation.createErrorBox("Time Format Only ", "Please use time format HH:MM for the Worksheet to accept Start and Finish times");   
     sheet.addValidationData(nextpatientValidation);
     
     var servaycell: Cell = detailrow.createCell(10);
     servaycell.setCellStyle(OrangecolorStyle);
     var servayList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 10, 10);
     
     var validationHelper3 : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var constraint3 : DataValidationConstraint  = validationHelper3.createExplicitListConstraint(servayval);
     var dataValidation3 : DataValidation  = validationHelper3.createValidation(constraint3, servayList);
     dataValidation3.setSuppressDropDownArrow(true); 
     dataValidation3.createErrorBox("Bad Value", "Please select value from drop down list!");
     dataValidation3.setShowErrorBox(true);
     dataValidation3.createPromptBox("Input Y or N ONLY ", "You can type Y or N into these cells to indicate whether a Survey has been completed - May also select option from drop down menu");
     dataValidation3.setShowPromptBox(true)
     sheet.addValidationData(dataValidation3); 
     
     var notescell: Cell = detailrow.createCell(11);
     
     notescell.setCellStyle(wraptextstyle);
     
     var notesList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 11, 11);
     var notesValidation : DataValidation  = commonvalidationHelper.createValidation(commonconstraint, notesList);
     notesValidation.createPromptBox("Additional Appointment Notes", "This Area of the worksheet is available for any additional notes that may need to be made against appointments such as; Patient did not prepare for appointment, patient refused scan etc ");
     notesValidation.setShowPromptBox(true)
     notesValidation.setSuppressDropDownArrow(false); 
     sheet.addValidationData(notesValidation);
     
     
     //scan duration cell with formulas
   
     var scanformula1 ="IF(E"+rowintformula+"=\"D\",\"DNA\",IF(J"+rowintformula+"-I"+rowintformula+"<0,\"Error\",IF(AND(I"+rowintformula+"<>\"\",J"+rowintformula+"<>\"\"),J"+rowintformula+"-I"+rowintformula+",\"\")))"
     var scanformula2 = "IFERROR(ROUND((O"+rowintformula+"*1440),0),\"\")"
     var scanformula3 = "IFERROR(P"+rowintformula+"/60,0)"
     
     var scancell1: Cell = detailrow.createCell(14);
     scancell1.setCellFormula(scanformula1);
     var scancell2: Cell = detailrow.createCell(15);
     scancell2.setCellFormula(scanformula2);
     var scancell3: Cell = detailrow.createCell(16);
     scancell3.setCellFormula(scanformula3);
      }
     
     
     
     sheet.addMergedRegion(new CellRangeAddress(2,2,22,26));
     var descriptioncell7: Cell = row2.createCell(22);
     descriptioncell7.setCellValue("Summary");
     
     var detailsendval = 13+looplength
     println("detailsendval value "+detailsendval);
     

     var summarylist = List("Clinic Duration","Budgeted Cap","Actual Capacity","Patients Booked","DNA","Scan Rate 1","Scan Rate 2","Scan Rate 3","BP Scanned") 
     for (l1 <- 18 to 26) {
     var summarycell: Cell = row4.createCell(l1);
     summarycell.setCellValue(summarylist(l1-18));
     summarycell.setCellStyle(bluecolorStyle);
      }
     
     var clinicdurationcell: Cell = row5.createCell(18);
     clinicdurationcell.setCellFormula("Q8");
     clinicdurationcell.setCellStyle(normalstyle);
     
     var budgetedcapcell: Cell = row5.createCell(19);
     budgetedcapcell.setCellFormula("K8");
     budgetedcapcell.setCellStyle(normalstyle);
     
     var actualcapcell: Cell = row5.createCell(20);
     actualcapcell.setCellFormula("K8");
     actualcapcell.setCellStyle(normalstyle);
     
     
     var patientbookedcell: Cell = row5.createCell(21);
     patientbookedcell.setCellFormula("COUNTA(C14:C"+detailsendval+")");
     patientbookedcell.setCellStyle(normalstyle);
     var dnacell: Cell = row5.createCell(22);
     dnacell.setCellFormula("COUNTIF(E14:E"+detailsendval+",\"D\")");
     dnacell.setCellStyle(normalstyle);
     var scanrate1cell: Cell = row5.createCell(23);
     scanrate1cell.setCellFormula("COUNTIFS(F14:F"+detailsendval+",\"\",G14:G"+detailsendval+",\"\",P14:P"+detailsendval+",\"<20\")");
     scanrate1cell.setCellStyle(normalstyle);
     var scanrate2cell: Cell = row5.createCell(24);
     scanrate2cell.setCellFormula("COUNTIFS(F14:F"+detailsendval+",\"\",G14:G"+detailsendval+",\"\",P14:P"+detailsendval+",\">19\")");
     scanrate2cell.setCellStyle(normalstyle);
     var scanrate3cell: Cell = row5.createCell(25);
     scanrate3cell.setCellFormula("COUNTIF(G14:G"+detailsendval+",\"Y\")+COUNTIF(F14:F"+detailsendval+",\"Y\")");
     scanrate3cell.setCellStyle(normalstyle);
     var bpscanedcell: Cell = row5.createCell(26);
     bpscanedcell.setCellFormula("SUM(H14:H"+detailsendval+")"); 
     bpscanedcell.setCellStyle(normalstyle);
     
    
     var summarylist2 = List("Key","Description ","Count") 
     for (l1 <- 18 to 20) {
     var summarycell2: Cell = row7.createCell(l1);
     summarycell2.setCellValue(summarylist2(l1-18));
     summarycell2.setCellStyle(bluecolorStyle);
      }
     
     var keylist = List("V","E","D")
     var descriptionlist = List("Patient Seen / verified","Patient Escalations (Critical / High Priority)","Patient Did Not Attend Appointment")
     
     var keycell1: Cell = row8.createCell(18);
     keycell1.setCellValue(keylist(0));
     keycell1.setCellStyle(normalstyle);
     var keycell2: Cell = row9.createCell(18);
     keycell2.setCellValue(keylist(1));
     keycell2.setCellStyle(normalstyle);
     var keycell3: Cell = row10.createCell(18);
     keycell3.setCellValue(keylist(2));
     keycell3.setCellStyle(normalstyle);
     
     
     var descell1: Cell = row8.createCell(19);
     descell1.setCellValue(descriptionlist(0));
     descell1.setCellStyle(normalstyle);
     var descell2: Cell = row9.createCell(19);
     descell2.setCellValue(descriptionlist(1));
     descell2.setCellStyle(normalstyle);
     var descell3: Cell = row10.createCell(19);
     descell3.setCellValue(descriptionlist(2));
     descell3.setCellStyle(normalstyle);
     
     var countcell1: Cell = row8.createCell(20);
     countcell1.setCellFormula("COUNTIF($E$14:$E$"+detailsendval+",$S8)");
     countcell1.setCellStyle(normalstyle);
     var countcell2: Cell = row9.createCell(20);
     countcell2.setCellFormula("COUNTIF($E$14:$E$"+detailsendval+",$S9)");
     countcell2.setCellStyle(normalstyle);
     var countcell3: Cell = row10.createCell(20);
     countcell3.setCellFormula("COUNTIF($E$14:$E$"+detailsendval+",$S10)");
     countcell3.setCellStyle(normalstyle);
     
     
     //var inputStream : InputStream = new FileInputStream("C:/Users/Cardinality/Desktop/c7healthlogo/dw-logo-high.JPG");
    /* var inputStream : InputStream = new FileInputStream("https://d332o5h2loq9iy.cloudfront.net/images/dw-logo-high.jpg");
     
     var bytes : Array[Byte] = IOUtils.toByteArray(inputStream);
     var pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
     inputStream.close(); */
     
     
     var url : URL  = new URL("https://d332o5h2loq9iy.cloudfront.net/images/dw-logo-high.jpg");
     var inputStream : InputStream = url.openStream();
     
     var bytes : Array[Byte] = IOUtils.toByteArray(inputStream);
     var pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
     inputStream.close();
     
     
     var helper : CreationHelper = workbook.getCreationHelper();
     var drawing : Drawing  = sheet.createDrawingPatriarch();
     
     var anchor : ClientAnchor  = helper.createClientAnchor();
     
     
     anchor.setCol1(0); //Column B
     anchor.setRow1(0); //Row 3
     anchor.setCol2(2); //Column C
     anchor.setRow2(5); //Row 4
   
     var pict : Picture  = drawing.createPicture(anchor, pictureIdx);
     pict.resize(2, 1)
     
     
    var tooltipcell: Cell = row0.createCell(100);
    tooltipcell.setCellValue("A1");
     
     var anchor1 : ClientAnchor  = helper.createClientAnchor();
     anchor1.setCol1(tooltipcell.getColumnIndex()+1)
     anchor1.setDx1(10*Units.EMU_PER_PIXEL)
     anchor1.setCol2(tooltipcell.getColumnIndex()+2)
     anchor1.setDx2(10*Units.EMU_PER_PIXEL)
     anchor1.setRow1(row0.getRowNum())
     anchor1.setDy1(10*Units.EMU_PER_PIXEL)
     anchor1.setRow2(row0.getRowNum()+3)
     anchor1.setDy2(10*Units.EMU_PER_PIXEL)
     
       // Create the comment and set the text+author
    var comment: Comment  = drawing.createCellComment(anchor);
    var strval : RichTextString  = helper.createRichTextString("Hello, World!");
    comment.setString(strval);
    comment.setAuthor("Apache POI")
    
    tooltipcell.setCellComment(comment);
    
     
    val tempfolder1 = current.configuration.getString("temp_folder").get
    //val fileName = ExcelFile.toString()
    println("files to be printed" + current.configuration.getString("temp_folder").get + "/" + filename)
    
    var fos: FileOutputStream = null
    var outputFile: File = null
    var path: String = null
    outputFile = new File(current.configuration.getString("temp_folder").get + "/" + filename);
    fos = new FileOutputStream(outputFile);

    //  var out: FileOutputStream = new FileOutputStream(new File(current.configuration.getString("temp_folder").get + "/" + fileName));

    workbook.write(fos);
    fos.close()
     println("to email mailID  "+tomail);
     sendAttachmentcsw(0L, 0L,tomail,filename)
    //sendAttachmentcsw(0L, 0L,"sabarinathan@cardinality.ai",filename)

  }

  
  
  
  

  def sendAttachment(responseId: Long, PracticeID: Long, tomail: String, filename: String): String = db withSession { implicit session =>

    //tomail = "sabarinathan@cardinality.ai"
    println("tomail  values " + tomail)
    val contentSubject: String = "File Attachment";

    val futureString: Future[String] = scala.concurrent.Future {
      if (tomail != None && tomail != "")
        hcueCommunication.sendAttachmentMailcsv(tomail, "Appointmentdetails-Sonographer", path + "/" + filename)

      var localFile: File = new File(path + "/" + filename);

      if (localFile.delete()) {
        println("------------------------------File deleted------------------------------")
      }
      "Success"
    }
    "Success"
  }
  
  def sendAttachmentcsw(responseId: Long, PracticeID: Long, tomail: String,filename: String): String = db withSession { implicit session =>

    //val tomail = "sabarinathan@cardinality.ai"

    println("tomail  values " + tomail)

    val contentSubject: String = "File Attachment";

    val futureString: Future[String] = scala.concurrent.Future {
      if (tomail != None && tomail != "" )
        hcueCommunication.sendAttachmentMailcsv(tomail, "Appointmentdetails-CSW",  path+"/"+filename)
        
      var localFile: File = new File(path+"/"+filename);

    if (localFile.delete()) {
      System.out.println("------------------------------File deleted------------------------------")
    }
        
      "Success"
    }

    "Success"

  }


  //def getsonographerinfo(date: java.sql.Date, doctorid: Long) =
    //sql"SELECT * FROM getsonographercsvfile('#$date','#$doctorid')".as[(Long, String, String, String, String, String, java.sql.Date)]

  //def getsupporworkerinfo(date: java.sql.Date, doctorid: Long) =
    //sql"SELECT * FROM getsupportworkercsvfile('#$date','#$doctorid')".as[(Long, String, String, String, String, String, java.sql.Date)]

}