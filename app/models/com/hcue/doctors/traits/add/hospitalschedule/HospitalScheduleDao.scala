package models.com.hcue.doctors.traits.add.hospitalschedule

import java.sql.Date
import java.util.Calendar
import scala.slick.driver.PostgresDriver.simple.booleanColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.booleanOptionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.dateColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.productQueryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.queryToInsertInvoker
import scala.slick.driver.PostgresDriver.simple.queryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn
import scala.slick.jdbc.StaticQuery.interpolation
import scala.util.control.Breaks.break
import scala.util.control.Breaks.breakable
import org.joda.time.DateTime
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.HospitalSchedule
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.HospitalScheduleExtn
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.THospitalSchedule
import models.com.hcue.doctor.slick.transactTable.doctor.schedule.THospitalScheduleExtn
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.addScheduleObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.addScheduleRecord
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.getCalendarObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.getScheduleObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.getavailappointmentObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.listAllApppointmentResObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.listAllCalendarObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.listAllScheduleObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.otherdetailObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.updateScheduleObj
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.updateScheduleRecord
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.validateConsultationRecord
import models.com.hcue.doctors.traits.dbProperties.db
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import models.com.hcue.doctors.Json.Request.AddUpdateSchedule.delobj
import models.com.hcue.doctors.slick.transactTable.doctor.THospitalAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.HospitalAppointment
object HospitalScheduleDao {

  val hospitalschedule = HospitalSchedule.HospitalSchedule

  val hospitalscheduleextn = HospitalScheduleExtn.HospitalScheduleExtn
  
  val hospitalapp = HospitalAppointment.HospitalAppointment
  

  def addHospitalSchedule(addscheduleObj: addScheduleObj) = db withTransaction { implicit session =>

    addscheduleObj.scheduleRecord.foreach { x =>

      val hospitalScheduleDetails: addScheduleRecord = x

      val otherdetails: otherdetailObj = hospitalScheduleDetails.otherdetails

      var minpercase = hospitalScheduleDetails.MinPerCase

      println("HospitalID " + addscheduleObj.HospitalID)
      println("SonoDocId " + hospitalScheduleDetails.SonographerDoctorID.get)
      println("SupportWorkedDOcID " + hospitalScheduleDetails.SupportworkerDoctorID.get)
      println("SonographerAddressID " + hospitalScheduleDetails.SonographerAddressID.get)
      println("SupportWorkerAddressId " + hospitalScheduleDetails.SupportworkerAddressID.get)
      println("Schedule Date " + hospitalScheduleDetails.ScheduleDate.get)

      val mappedValue: THospitalSchedule = new THospitalSchedule(
        0, addscheduleObj.HospitalID, hospitalScheduleDetails.SonographerDoctorID.get, hospitalScheduleDetails.SupportworkerDoctorID.get,
        hospitalScheduleDetails.SonographerAddressID.get, hospitalScheduleDetails.SupportworkerAddressID.get, hospitalScheduleDetails.DayCD,
        hospitalScheduleDetails.ScheduleDate.get, minpercase, Some(otherdetails.Active), Some(otherdetails.BookFlag), otherdetails.Slot, otherdetails.RoomCostStatus, addscheduleObj.USRId,
        addscheduleObj.USRType, null, null)

      val addressConsultID = (hospitalschedule returning hospitalschedule.map(_.AddressConsultID)) += mappedValue

      hospitalscheduleextn.filter { x => x.AddressConsultID === addressConsultID }.map { x => (x.FromTime1, x.ToTime1, x.FromBreakTime1, x.ToBreakTime1, x.FromTime2, x.ToTime2, x.FromBreakTime2, x.ToBreakTime2, x.FromTime3, x.ToTime3, x.FromBreakTime3, x.ToBreakTime3) }.update(
        hospitalScheduleDetails.FromTime1, hospitalScheduleDetails.ToTime1, hospitalScheduleDetails.FromBreakTime1, hospitalScheduleDetails.ToBreakTime1,
        hospitalScheduleDetails.FromTime2, hospitalScheduleDetails.ToTime2, hospitalScheduleDetails.FromBreakTime2, hospitalScheduleDetails.ToBreakTime2,
        hospitalScheduleDetails.FromTime3, hospitalScheduleDetails.ToTime3, hospitalScheduleDetails.FromBreakTime3, hospitalScheduleDetails.ToBreakTime3)

    }

  }

  def addHospitalSchedule1withdays(addscheduleObj: addScheduleObj) = db withTransaction { implicit session =>

    val scheduledays = addscheduleObj.Scheduledays
    addscheduleObj.scheduleRecord.foreach { x =>

      val hospitalScheduleDetails: addScheduleRecord = x

      val otherdetails: otherdetailObj = hospitalScheduleDetails.otherdetails

      var minpercase = hospitalScheduleDetails.MinPerCase

      for (i <- 0 until scheduledays) {

        val dtOrg: DateTime = new DateTime(hospitalScheduleDetails.ScheduleDate.get)
        val adddate = dtOrg.plusDays(i)
        val scheduledate: java.sql.Date = new Date(adddate.toLocalDateTime().toDateTime().getMillis)
        println("adddate val " + scheduledate)
        val dayOfWeek = scheduledate.getDay
        println("dayOfWeek val " + dayOfWeek)
        val dayarr: Array[String] = Array("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT")
        val daycd = dayarr(dayOfWeek)

        val mappedValue: THospitalSchedule = new THospitalSchedule(
          0, addscheduleObj.HospitalID, hospitalScheduleDetails.SonographerDoctorID.get, hospitalScheduleDetails.SupportworkerDoctorID.get,
          hospitalScheduleDetails.SonographerAddressID.get, hospitalScheduleDetails.SupportworkerAddressID.get, daycd,
          scheduledate, minpercase, Some(otherdetails.Active), Some(otherdetails.BookFlag), otherdetails.Slot, otherdetails.RoomCostStatus, addscheduleObj.USRId,
          addscheduleObj.USRType, null, null)

        val addressConsultID = (hospitalschedule returning hospitalschedule.map(_.AddressConsultID)) += mappedValue

        hospitalscheduleextn.filter { x => x.AddressConsultID === addressConsultID }.map { x => (x.FromTime1, x.ToTime1, x.FromBreakTime1, x.ToBreakTime1, x.FromTime2, x.ToTime2, x.FromBreakTime2, x.ToBreakTime2, x.FromTime3, x.ToTime3, x.FromBreakTime3, x.ToBreakTime3) }.update(
          hospitalScheduleDetails.FromTime1, hospitalScheduleDetails.ToTime1, hospitalScheduleDetails.FromBreakTime1, hospitalScheduleDetails.ToBreakTime1,
          hospitalScheduleDetails.FromTime2, hospitalScheduleDetails.ToTime2, hospitalScheduleDetails.FromBreakTime2, hospitalScheduleDetails.ToBreakTime2,
          hospitalScheduleDetails.FromTime3, hospitalScheduleDetails.ToTime3, hospitalScheduleDetails.FromBreakTime3, hospitalScheduleDetails.ToBreakTime3)

      }

    }

  }

  def addValidateConsultation(consultationRecord: Array[addScheduleRecord]): (Boolean, String) = db withSession { implicit session =>

    val scheduleRecord = consultationRecord
    var status = true
    var dtCd = ""

    for (i <- 0 until scheduleRecord.length) {

      //val stData =  hospitalschedule.filter{x => x.SonographerDoctorID === scheduleRecord(i).SonographerDoctorID && x.ScheduleDate === scheduleRecord(i).ScheduleDate}.firstOption

      val stData = hospitalschedule.filter { x => (x.SonographerDoctorID === scheduleRecord(i).SonographerDoctorID || x.SupportworkerDoctorID === scheduleRecord(i).SupportworkerDoctorID) && x.ScheduleDate === scheduleRecord(i).ScheduleDate }.list
      if (stData != "" && stData != None) {
        //val storedData: List[THospitalScheduleExtn] = hospitalscheduleextn.filter(c => c.SonographerDoctorID === scheduleRecord(i).SonographerDoctorID && c.AddressConsultID === stData.get.AddressConsultID).list

        val storedData: List[THospitalScheduleExtn] = hospitalscheduleextn.filter(c => c.AddressConsultID inSet stData.map { x => x.AddressConsultID }.toList).list

        var storedLst: Array[validateConsultationRecord] = new Array[validateConsultationRecord](storedData.length)

        if (storedData.length > 0) {

          for (i <- 0 until storedData.length) {
            val startTime = (storedData(i).FromTime1.getOrElse("00:00").replaceAll(":", "").toString()).toInt
            val endTime = (storedData(i).ToTime1.getOrElse("00:00").replaceAll(":", "").toString()).toInt
            storedLst(i) = new validateConsultationRecord(storedData(i).DayCD, startTime, endTime, storedData(i).Active)
          }

        }

        var tempLst: Array[validateConsultationRecord] = new Array[validateConsultationRecord](consultationRecord.length)

        if (consultationRecord.length > 0) {

          for (i <- 0 until consultationRecord.length) {
            val startTime = (consultationRecord(i).FromTime1.getOrElse("00:00").replaceAll(":", "").toString()).toInt
            val endTime = (consultationRecord(i).ToTime1.getOrElse("00:00").replaceAll(":", "").toString()).toInt
            tempLst(i) = new validateConsultationRecord(consultationRecord(i).DayCD, startTime, endTime, Some("Y"))
          }

        }

        var resultset = storedLst.toList ::: tempLst.toList

        resultset = resultset.filter { x => x.Active == Some("Y") }.sortBy { x => x.StartTime }

        breakable {

          val dayCode = resultset.map { x => x.DayCD }.distinct.toList

          for (j <- 0 until dayCode.length) {

            val dayCodeList = resultset.filter { x => x.DayCD == dayCode(j) }.toList
            if (dayCodeList.length > 0) {

              for (i <- 1 until dayCodeList.length) {

                if (dayCodeList(i).StartTime.toInt < dayCodeList(i - 1).EndTime.toInt) {

                  status = false
                  dtCd = dayCode(j)
                  break
                }

              }

            }

          }
        }
      }
    }
    return (status, dtCd)

  }

  def updateValidateConsultation(consultationRecord: Array[updateScheduleRecord], hospitalid: Long): (Boolean, String) = db withSession { implicit session =>

    val scheduleRecord = consultationRecord
    var status = true
    var dtCd = ""

    for (i <- 0 until scheduleRecord.length) {

      val stData = hospitalschedule.filter { x => (x.SonographerDoctorID === scheduleRecord(i).SonographerDoctorID || x.SupportworkerDoctorID === scheduleRecord(i).SupportworkerDoctorID) && x.ScheduleDate === scheduleRecord(i).ScheduleDate && x.HospitalID =!= hospitalid }.list

      val storedData: List[THospitalScheduleExtn] = hospitalscheduleextn.filter(c => c.AddressConsultID inSet stData.map { x => x.AddressConsultID }.toList).list

      var storedLst: Array[validateConsultationRecord] = new Array[validateConsultationRecord](storedData.length)

      if (storedData.length > 0) {

        for (i <- 0 until storedData.length) {
          val startTime = (storedData(i).FromTime1.getOrElse("00:00").replaceAll(":", "").toString()).toInt
          val endTime = (storedData(i).ToTime1.getOrElse("00:00").replaceAll(":", "").toString()).toInt
          storedLst(i) = new validateConsultationRecord(storedData(i).DayCD, startTime, endTime, storedData(i).Active)
        }

      }

      var tempLst: Array[validateConsultationRecord] = new Array[validateConsultationRecord](consultationRecord.length)

      if (consultationRecord.length > 0) {

        for (i <- 0 until consultationRecord.length) {
          val startTime = (consultationRecord(i).FromTime1.getOrElse("00:00").replaceAll(":", "").toString()).toInt
          val endTime = (consultationRecord(i).ToTime1.getOrElse("00:00").replaceAll(":", "").toString()).toInt
          tempLst(i) = new validateConsultationRecord(consultationRecord(i).DayCD, startTime, endTime, Some("Y"))
        }

      }

      var resultset = storedLst.toList ::: tempLst.toList

      resultset = resultset.filter { x => x.Active == Some("Y") }.sortBy { x => x.StartTime }

      breakable {

        val dayCode = resultset.map { x => x.DayCD }.distinct.toList

        for (j <- 0 until dayCode.length) {

          val dayCodeList = resultset.filter { x => x.DayCD == dayCode(j) }.toList
          if (dayCodeList.length > 0) {

            for (i <- 1 until dayCodeList.length) {

              if (dayCodeList(i).StartTime.toInt < dayCodeList(i - 1).EndTime.toInt) {

                status = false
                dtCd = dayCode(j)
                break
              }

            }

          }

        }
      }
    }
    return (status, dtCd)

  }
  
  
    def scheduleslotValidation(consultationRecord: Array[updateScheduleRecord], hospitalid: Long): (Int) = db withSession { implicit session =>
        
      var res = 0
      
       val scheduleRecord = consultationRecord
       for (i <- 0 until scheduleRecord.length) {
         var addressconsultid=scheduleRecord(i).AddressConsultID
         var minpercase = scheduleRecord(i).MinPerCase
         var starttime = scheduleRecord(i).FromTime1
         var endtime = scheduleRecord(i).ToTime1
         var doctorid = scheduleRecord(i).SonographerDoctorID
         
         res = getschedulevalidationQuery(addressconsultid.get,minpercase.get,starttime.get,endtime.get,doctorid.get).first
         
       }
    
        println("scheduleslotValidation val ::: "+res);
        return res

  }
  
  
  

  def checkAvailablity(addscheduleObj: addScheduleObj): String = db withTransaction { implicit session =>

    val scheduleRecord = addscheduleObj.scheduleRecord

    for (i <- 0 until scheduleRecord.length) {

      val sonographerDoctorID = scheduleRecord(i).SonographerDoctorID.get
      val supportworkerDoctorID = scheduleRecord(i).SupportworkerDoctorID.get
      val appointmentDate = scheduleRecord(i).ScheduleDate.get
      val fromTime = scheduleRecord(i).FromTime1.get
      val toTime = scheduleRecord(i).ToTime1.get

      val hospSchedule = hospitalschedule.filter { x => x.SonographerDoctorID === sonographerDoctorID && x.ScheduleDate === appointmentDate }.firstOption

      val addressConsultID = hospSchedule.get.AddressConsultID

      val hospScheduleExtn = hospitalscheduleextn.filter { x => x.AddressConsultID === addressConsultID }.map { x => (x.FromTime1, x.ToTime1) }.firstOption

    }

    ""
  }

  def updateHospitalSchedule1withdays(updatescheduleObj: updateScheduleObj) = db withTransaction { implicit session =>

    val scheduledays = updatescheduleObj.Scheduledays

    val storedData: List[THospitalSchedule] = hospitalschedule.filter(c => c.HospitalID === updatescheduleObj.HospitalID).list

    updatescheduleObj.scheduleRecord.foreach { x =>

      val hospitalScheduleDetails: updateScheduleRecord = x

      val otherdetails: otherdetailObj = hospitalScheduleDetails.otherdetails

      val ScheduleDetails = storedData.find(c => c.AddressConsultID == hospitalScheduleDetails.AddressConsultID.get &&
        c.HospitalID == updatescheduleObj.HospitalID)

      for (i <- 0 until scheduledays) {

        val dtOrg: DateTime = new DateTime(hospitalScheduleDetails.ScheduleDate.get)
        val adddate = dtOrg.plusDays(i)
        val scheduledate: java.sql.Date = new Date(adddate.toLocalDateTime().toDateTime().getMillis)
        println("adddate val " + scheduledate)
        val cal: Calendar = Calendar.getInstance();
        // set to the starting time
        cal.setTime(scheduledate);
        val dayOfWeek = cal.get(Calendar.DAY_OF_YEAR)
        println("dayOfWeek val " + dayOfWeek)
        val dayarr: Array[String] = Array("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT")
        val daycd = dayarr(dayOfWeek)

        val ScheduleDetailsval = storedData.find(c => c.SonographerDoctorID == ScheduleDetails.get.SonographerDoctorID && c.SonographerAddressID == ScheduleDetails.get.SonographerAddressID &&
          c.SupportworkerDoctorID == ScheduleDetails.get.SupportworkerDoctorID && c.SupportworkerAddressID == ScheduleDetails.get.SupportworkerAddressID &&
          c.ScheduleDate == scheduledate && c.DayCD == daycd && c.Slot == ScheduleDetails.get.Slot &&
          c.HospitalID == updatescheduleObj.HospitalID)

        if (ScheduleDetailsval != None) {

          val mappedValue: THospitalSchedule = new THospitalSchedule(
            ScheduleDetailsval.get.AddressConsultID, updatescheduleObj.HospitalID, hospitalScheduleDetails.SonographerDoctorID.get, hospitalScheduleDetails.SupportworkerDoctorID.get,
            hospitalScheduleDetails.SonographerAddressID.get, hospitalScheduleDetails.SupportworkerAddressID.get, daycd,
            scheduledate, hospitalScheduleDetails.MinPerCase, Some(otherdetails.Active), Some(otherdetails.BookFlag), otherdetails.Slot, otherdetails.RoomCostStatus, updatescheduleObj.USRId,
            updatescheduleObj.USRType, null, null)

          hospitalschedule.filter(c => c.AddressConsultID === ScheduleDetailsval.get.AddressConsultID).update(mappedValue)

          hospitalscheduleextn.filter { x => x.AddressConsultID === ScheduleDetailsval.get.AddressConsultID }.map { x => (x.FromTime1, x.ToTime1, x.FromBreakTime1, x.ToBreakTime1, x.FromTime2, x.ToTime2, x.FromBreakTime2, x.ToBreakTime2, x.FromTime3, x.ToTime3, x.FromBreakTime3, x.ToBreakTime3) }.update(
            hospitalScheduleDetails.FromTime1, hospitalScheduleDetails.ToTime1, hospitalScheduleDetails.FromBreakTime1, hospitalScheduleDetails.ToBreakTime1,
            hospitalScheduleDetails.FromTime2, hospitalScheduleDetails.ToTime2, hospitalScheduleDetails.FromBreakTime2, hospitalScheduleDetails.ToBreakTime2,
            hospitalScheduleDetails.FromTime3, hospitalScheduleDetails.ToTime3, hospitalScheduleDetails.FromBreakTime3, hospitalScheduleDetails.ToBreakTime3)

        } else {

          val mappedValue: THospitalSchedule = new THospitalSchedule(
            0, updatescheduleObj.HospitalID, hospitalScheduleDetails.SonographerDoctorID.get, hospitalScheduleDetails.SupportworkerDoctorID.get,
            hospitalScheduleDetails.SonographerAddressID.get, hospitalScheduleDetails.SupportworkerAddressID.get, daycd,
            scheduledate, hospitalScheduleDetails.MinPerCase, Some(otherdetails.Active), Some(otherdetails.BookFlag), otherdetails.Slot, otherdetails.RoomCostStatus, updatescheduleObj.USRId,
            updatescheduleObj.USRType, null, null)

          val addressConsultID = (hospitalschedule returning hospitalschedule.map(_.AddressConsultID)) += mappedValue

          hospitalscheduleextn.filter { x => x.AddressConsultID === addressConsultID }.map { x => (x.FromTime1, x.ToTime1, x.FromBreakTime1, x.ToBreakTime1, x.FromTime2, x.ToTime2, x.FromBreakTime2, x.ToBreakTime2, x.FromTime3, x.ToTime3, x.FromBreakTime3, x.ToBreakTime3) }.update(
            hospitalScheduleDetails.FromTime1, hospitalScheduleDetails.ToTime1, hospitalScheduleDetails.FromBreakTime1, hospitalScheduleDetails.ToBreakTime1,
            hospitalScheduleDetails.FromTime2, hospitalScheduleDetails.ToTime2, hospitalScheduleDetails.FromBreakTime2, hospitalScheduleDetails.ToBreakTime2,
            hospitalScheduleDetails.FromTime3, hospitalScheduleDetails.ToTime3, hospitalScheduleDetails.FromBreakTime3, hospitalScheduleDetails.ToBreakTime3)

        }
      }
    }

  }

  def updateHospitalSchedule(updatescheduleObj: updateScheduleObj) = db withTransaction { implicit session =>

    //Check for record in tPatientPharma

    val storedData: List[THospitalSchedule] = hospitalschedule.filter(c => c.HospitalID === updatescheduleObj.HospitalID).list

    updatescheduleObj.scheduleRecord.foreach { x =>

      val hospitalScheduleDetails: updateScheduleRecord = x

      val otherdetails: otherdetailObj = hospitalScheduleDetails.otherdetails

      val ScheduleDetails = storedData.find(c => c.AddressConsultID == hospitalScheduleDetails.AddressConsultID.get &&
        c.HospitalID == updatescheduleObj.HospitalID)

      if (ScheduleDetails != None) {

        val mappedValue: THospitalSchedule = new THospitalSchedule(
          hospitalScheduleDetails.AddressConsultID.get, updatescheduleObj.HospitalID, hospitalScheduleDetails.SonographerDoctorID.get, hospitalScheduleDetails.SupportworkerDoctorID.get,
          hospitalScheduleDetails.SonographerAddressID.get, hospitalScheduleDetails.SupportworkerAddressID.get, hospitalScheduleDetails.DayCD,
          hospitalScheduleDetails.ScheduleDate.get, hospitalScheduleDetails.MinPerCase, Some(otherdetails.Active), Some(otherdetails.BookFlag), otherdetails.Slot, otherdetails.RoomCostStatus, updatescheduleObj.USRId,
          updatescheduleObj.USRType, null, null)

        hospitalschedule.filter(c => c.AddressConsultID === hospitalScheduleDetails.AddressConsultID).update(mappedValue)

        hospitalscheduleextn.filter { x => x.AddressConsultID === hospitalScheduleDetails.AddressConsultID.get }.map { x => (x.FromTime1, x.ToTime1, x.FromBreakTime1, x.ToBreakTime1, x.FromTime2, x.ToTime2, x.FromBreakTime2, x.ToBreakTime2, x.FromTime3, x.ToTime3, x.FromBreakTime3, x.ToBreakTime3) }.update(
          hospitalScheduleDetails.FromTime1, hospitalScheduleDetails.ToTime1, hospitalScheduleDetails.FromBreakTime1, hospitalScheduleDetails.ToBreakTime1,
          hospitalScheduleDetails.FromTime2, hospitalScheduleDetails.ToTime2, hospitalScheduleDetails.FromBreakTime2, hospitalScheduleDetails.ToBreakTime2,
          hospitalScheduleDetails.FromTime3, hospitalScheduleDetails.ToTime3, hospitalScheduleDetails.FromBreakTime3, hospitalScheduleDetails.ToBreakTime3)
          
        hospitalapp.filter { x => x.AddressConsultID === hospitalScheduleDetails.AddressConsultID.get}.map { x =>
        (x.DoctorID)
      }.update(hospitalScheduleDetails.SonographerDoctorID)  
          
          

      } else {

        val mappedValue: THospitalSchedule = new THospitalSchedule(
          0, updatescheduleObj.HospitalID, hospitalScheduleDetails.SonographerDoctorID.get, hospitalScheduleDetails.SupportworkerDoctorID.get,
          hospitalScheduleDetails.SonographerAddressID.get, hospitalScheduleDetails.SupportworkerAddressID.get, hospitalScheduleDetails.DayCD,
          hospitalScheduleDetails.ScheduleDate.get, hospitalScheduleDetails.MinPerCase, Some(otherdetails.Active), Some(otherdetails.BookFlag), otherdetails.Slot, otherdetails.RoomCostStatus, updatescheduleObj.USRId,
          updatescheduleObj.USRType, null, null)

        val addressConsultID = (hospitalschedule returning hospitalschedule.map(_.AddressConsultID)) += mappedValue

        hospitalscheduleextn.filter { x => x.AddressConsultID === addressConsultID }.map { x => (x.FromTime1, x.ToTime1, x.FromBreakTime1, x.ToBreakTime1, x.FromTime2, x.ToTime2, x.FromBreakTime2, x.ToBreakTime2, x.FromTime3, x.ToTime3, x.FromBreakTime3, x.ToBreakTime3) }.update(
          hospitalScheduleDetails.FromTime1, hospitalScheduleDetails.ToTime1, hospitalScheduleDetails.FromBreakTime1, hospitalScheduleDetails.ToBreakTime1,
          hospitalScheduleDetails.FromTime2, hospitalScheduleDetails.ToTime2, hospitalScheduleDetails.FromBreakTime2, hospitalScheduleDetails.ToBreakTime2,
          hospitalScheduleDetails.FromTime3, hospitalScheduleDetails.ToTime3, hospitalScheduleDetails.FromBreakTime3, hospitalScheduleDetails.ToBreakTime3)

      }

    }

  }

  def allscheduleQuery(hospitalid: Long, scheduledate: java.sql.Date, usrid: Long) = sql"select * from getallscheduleval('#$hospitalid','#$scheduledate', '#$usrid')".as[(Long, Long, String, String, Long, String, java.sql.Date, String, String, Long, String)]

  def listAllSchedule(a: getScheduleObj): Array[listAllScheduleObj] = db withSession { implicit session =>

    val allScheduleList = allscheduleQuery(a.HospitalID, a.ScheduleDate, 1).list

    val resultSet: Array[listAllScheduleObj] = new Array[listAllScheduleObj](allScheduleList.length)

    for (i <- 0 until allScheduleList.length) {

      var json1: Option[JsValue] = None

      if (allScheduleList(i)._8 != None) {
        json1 = Some(Json.parse(allScheduleList(i)._8))
      }

      var json2: Option[JsValue] = None

      if (allScheduleList(i)._9 != None) {
        json2 = Some(Json.parse(allScheduleList(i)._9))
      }

      resultSet(i) = new listAllScheduleObj(Some(allScheduleList(i)._1), Some(allScheduleList(i)._2), Some(allScheduleList(i)._3), Some(allScheduleList(i)._4), Some(allScheduleList(i)._5), Some(allScheduleList(i)._6), Some(allScheduleList(i)._7), json1, json2, Some(allScheduleList(i)._10), Some(allScheduleList(i)._11))

    }

    return resultSet
  }
  
  def getschedulevalidationQuery(addressconsultid: Long,minpercase: Int,starttime: String, endtime: String,doctorid: Long) = sql"select * from getscheduleslotvalidation('#$addressconsultid','#$minpercase','#$starttime','#$endtime','#$doctorid')".as[(Int)]
  
  def getappintmentvalQuery(addressconsultid: Long) = sql"select * from getappintmentval('#$addressconsultid')".as[(Long)]

  def update(a: delobj): String = db withSession { implicit session =>

    var res = ""
    
    println("AddressConsultID latest "+a.AddressConsultID.get)
    
    val Listcount = getappintmentvalQuery(a.AddressConsultID.get).list
    
    //val list = hospitalapp.filter { f => (f.AddressConsultID === a.AddressConsultID.get  && f.AppointmentStatus != "C")}.list
    println("leangth value "+Listcount.length)
    if (Listcount(0) > 0) { res = "Appointment Exists for this schedule" }
    else {

      val delete = hospitalschedule.filter { x => x.AddressConsultID === a.AddressConsultID.get }.map { x =>
        (x.Active)
      }.update(Some("N"))

      res = "Updated Successfully"

    }

    res
  }

  /*def allcalendarQuery(hospitalid: Long, sonographerid: Long,fromdate: java.sql.Date,todate: java.sql.Date) = sql"select * from getallcalendarval('#$hospitalid', '#$sonographerid','#$fromdate','#$todate')".as[(Long,String,Long,Long,Int,String,String,String,Long,String,java.sql.Date)]

    def listAllCalendar(a:getCalendarObj): Array[listAllCalendarObj] = db withSession { implicit session =>


     val allCalendarList = allcalendarQuery(a.HospitalID,a.SonographersID.get,a.FromDate.get,a.ToDate.get).list


    val resultSet: Array[listAllCalendarObj] = new Array[listAllCalendarObj](allCalendarList.length)


    for (i <- 0 until allCalendarList.length) {

      var json1: Option[JsValue] = None
      var json2: Option[JsValue] = None
      var json3: Option[JsValue] = None
      var json4: Option[JsValue] = None

      if(allCalendarList(i)._6 != None)
      {
        json1 = Some(Json.parse(allCalendarList(i)._6))
      }
           if(allCalendarList(i)._7 != None)
      {
        json2 = Some(Json.parse(allCalendarList(i)._7))
      }
           if(allCalendarList(i)._8 != None)
      {
        json3 = Some(Json.parse(allCalendarList(i)._8))
      }
      if(allCalendarList(i)._10 != None)
      {
        json4 = Some(Json.parse(allCalendarList(i)._10))
      }

      resultSet(i) = new listAllCalendarObj(Some(allCalendarList(i)._1),Some(allCalendarList(i)._2),Some(allCalendarList(i)._3),Some(allCalendarList(i)._4),Some(allCalendarList(i)._5),json1,json2,json3,Some(allCalendarList(i)._9),json4,Some(allCalendarList(i)._11))

    }

    return resultSet
  }*/

  def allcalendarQuerytest(hospitalid: Long, sonographerid: Long, fromdate: java.sql.Date, todate: java.sql.Date) = sql"select * from getallcalendarval('#$hospitalid', '#$sonographerid','#$fromdate','#$todate')".as[(Long, String, Long, Long, String, String, Int, String, Int, String, Int, String, java.sql.Date, String, String, String)]

  def listAllCalendartest(a: getCalendarObj): Array[listAllCalendarObj] = db withSession { implicit session =>

    val allCalendarList = allcalendarQuerytest(a.HospitalID, a.SonographersID.get, a.FromDate.get, a.ToDate.get).list

    val resultSet: Array[listAllCalendarObj] = new Array[listAllCalendarObj](allCalendarList.length)

    for (i <- 0 until allCalendarList.length) {

      var availabletimeslotval = allCalendarList(i)._8
      val patientdetailarr = allCalendarList(i)._12.split("~")

      for (i <- 0 until patientdetailarr.length) {

        var patientdetailjson: Option[JsValue] = None
        patientdetailjson = Some(Json.parse(patientdetailarr(i)))
        val booktimeslot = patientdetailjson.map { x => x \ "Times" }.get.toString()
        val slotflag = patientdetailjson.map { x => x \ "Timeflag" }.get.toString()

        if (booktimeslot.nonEmpty && booktimeslot != "[]" && booktimeslot != None) {
          val booktimeslotval1 = booktimeslot.replace("[", "")
          val booktimeslotval = booktimeslotval1.replace("]", "")

          val booktimeslotarr = booktimeslotval.split(",")
          for (j <- 0 until booktimeslotarr.length) {

            var timebook = booktimeslotarr(j).slice(0, 6)
            var timebookwithflag = timebook.concat("~Y\"")
            println("slotflag values 1234 " + slotflag)
            if (slotflag == "\"E\"") {
              println("come to E slot values 123 " + "E")
              timebookwithflag = timebook.concat("~EY\"")
            }
            availabletimeslotval = availabletimeslotval.replace(booktimeslotarr(j), timebookwithflag)

          }
        }
      }

      //new code- for emergency slot
      var emergencytimeslotval = allCalendarList(i)._10
      if (emergencytimeslotval.nonEmpty && emergencytimeslotval != "[]" && emergencytimeslotval != None) {

        val emetimeslotval1 = emergencytimeslotval.replace("[", "")
        val emetimeslotval = emetimeslotval1.replace("]", "")

        val emetimeslotarr = emetimeslotval.split(",")
        for (j <- 0 until emetimeslotarr.length) {

          var timeEme = emetimeslotarr(j).slice(0, 6)
          var timeEmewithflag = timeEme.concat("~E\"")

          availabletimeslotval = availabletimeslotval.replace(emetimeslotarr(j), timeEmewithflag)
        }
      }

      var availabletimeslotjson: Option[JsValue] = None
      availabletimeslotjson = Some(Json.parse(availabletimeslotval))

      var totslot = allCalendarList(i)._9

      var Emergencytimeslotjson: Option[JsValue] = None

      if (allCalendarList(i)._10 != None) {
        Emergencytimeslotjson = Some(Json.parse(allCalendarList(i)._10))
      }

      var Breaktime1: Option[JsValue] = None

      if (allCalendarList(i)._14 != None) {
        Breaktime1 = Some(Json.parse(allCalendarList(i)._14))
      }

      var Breaktime2: Option[JsValue] = None
      if (allCalendarList(i)._15 != None) {
        Breaktime2 = Some(Json.parse(allCalendarList(i)._15))
      }

      var Breaktime3: Option[JsValue] = None
      if (allCalendarList(i)._16 != None) {
        Breaktime3 = Some(Json.parse(allCalendarList(i)._16))
      }

      resultSet(i) = new listAllCalendarObj(Some(allCalendarList(i)._1), Some(allCalendarList(i)._2), Some(allCalendarList(i)._3), Some(allCalendarList(i)._4), Some(allCalendarList(i)._5), Some(allCalendarList(i)._6), Some(allCalendarList(i)._7),
        availabletimeslotjson, Emergencytimeslotjson, Some(totslot), Some(allCalendarList(i)._13), Breaktime1, Breaktime2, Breaktime3)

    }

    return resultSet
  }

  def allallbooking(hospitalid: String, fromDate: java.sql.Date, toDate: java.sql.Date, toTriageDate: java.sql.Date, appointmenttype: String,specialitycd: String) = sql"select * from getallbookingslot('#$hospitalid', '#$fromDate','#$toDate','#$toTriageDate','#$appointmenttype','#$specialitycd')".as[(java.sql.Date, String)]

  def listAllAppointment(a: getavailappointmentObj): Array[listAllApppointmentResObj] = db withSession { implicit session =>

    val hospitalidval = a.HospitalID.mkString(", ")
    val subspecialityidval = a.SpecialityCD.get.mkString(", ")

    var allAppointmentList = allallbooking(hospitalidval, a.fromDate.get, a.toDate.get, a.traigeDate.get, a.appointmenttype.get,subspecialityidval).list

    val dtOrg: DateTime = new DateTime(a.toDate.get)
    val adddate = dtOrg.plusDays(60)
    val endtodate: java.sql.Date = new Date(adddate.toLocalDateTime().toDateTime().getMillis)
    println("adddate val " + endtodate)

    if (allAppointmentList.length <= 0) {
      allAppointmentList = allallbooking(hospitalidval, a.toDate.get, endtodate, a.traigeDate.get, a.appointmenttype.get,subspecialityidval).list.drop(0).take(4)
    }

    val resultSet: Array[listAllApppointmentResObj] = new Array[listAllApppointmentResObj](allAppointmentList.length)

    for (i <- 0 until allAppointmentList.length) {

      var json1: Option[JsValue] = None

      if (allAppointmentList(i)._2 != None) {
        json1 = Some(Json.parse(allAppointmentList(i)._2))
      }

      resultSet(i) = new listAllApppointmentResObj(Some(allAppointmentList(i)._1), json1)

    }

    return resultSet
  }

  def getappdtl(patientenquiryid: Long) = sql"select * from getappointmentdtl('#$patientenquiryid')".as[(String)]

  def getAppointmentInfo(patientenquiryid: Long) = db withSession { implicit session => Json.parse(getappdtl(patientenquiryid).first) }

}