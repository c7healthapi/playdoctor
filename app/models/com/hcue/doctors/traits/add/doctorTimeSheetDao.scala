package models.com.hcue.doctors.traits.add

import models.com.hcue.doctors.Json.Request.addTimeSlotobj
import models.com.hcue.doctors.Json.Request.listAvailableTimeSheets
import models.com.hcue.doctors.Json.Request.addTimeSheetobj
import models.com.hcue.doctors.Json.Request.listAvailableTimeSlots

import play.Logger
import play.api.libs.json._
import java.text.SimpleDateFormat
import models.com.hcue.doctors.traits.dbProperties.db
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker
import scala.slick.driver.PostgresDriver.simple._
import models.com.hcue.doctors.slick.lookupTable.TTimeSheets
import models.com.hcue.doctors.slick.lookupTable.TTimeSlots
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor

import models.com.hcue.doctors.slick.lookupTable.TimeSheets
import models.com.hcue.doctors.slick.lookupTable.TimeSlots
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors


object doctorTimeSheetDao {
  
  val log = Logger.of("application")
  
  val timeSheetTable = TimeSheets.TimeSheets
  val timeSlotTable = TimeSlots.TimeSlots
  val doctors = Doctors.Doctors
  
   
   def listTimeSheets(a:listAvailableTimeSheets):(Array[addTimeSheetobj],Int) = db withSession { implicit session =>
   
   log.info("Inside listings")
   val offset = (a.PageNumber-1)  * a.PageSize
    
   var twinJoin: List[(TTimeSlots, TTimeSheets)] = if(a.HospitalID != None && a.HospitalID.get > 0) {
       if(a.FromDate != None && a.ToDate != None){
       (for {
        (lukup, set) <- timeSlotTable innerJoin timeSheetTable on (_.TimeSlotID === _.TimeSlotID )
        if (set.HospitalID === a.HospitalID.get && set.SlotDate >= a.FromDate.get && set.SlotDate <= a.ToDate.get)
      } yield (lukup, set)).list.sortBy(f => (f._2.TimeSlotID )).reverse 
       } else {
          (for {
        (lukup, set) <- timeSlotTable innerJoin timeSheetTable on (_.TimeSlotID === _.TimeSlotID)
        if set.HospitalID === a.HospitalID.get
      } yield (lukup, set)).list.sortBy(f => (f._2.TimeSlotID )).reverse 
       } // fromdate
     } //hospital ID
    else {
      if(a.FromDate != None && a.ToDate != None){
       (for {
       (lukup, set) <- timeSlotTable innerJoin timeSheetTable on (_.TimeSlotID === _.TimeSlotID)
        if (set.DoctorID === a.DoctorID  && set.SlotDate >= a.FromDate.get && set.SlotDate <= a.ToDate.get)
      } yield (lukup, set)).list.sortBy(f => (f._2.TimeSlotID )).reverse 
      } else{
        (for {
       (lukup, set) <- timeSlotTable innerJoin timeSheetTable on (_.TimeSlotID === _.TimeSlotID)
        if set.DoctorID === a.DoctorID
      } yield (lukup, set)).list.sortBy(f => (f._2.TimeSlotID )).reverse 
      } // from date
     } // hospital ID
   
    if(a.forType != None && a.forType != ""){
      twinJoin = twinJoin.filter(p => p._2.DutyType.equals(a.forType)).toList
      
    }
    var joindateList = twinJoin;
   /* if(a.FromDate != None && a.ToDate != None){
      
      joindateList = twinJoin.filter(p => parseDate(p._2.SlotDate.toString()).get.after(a.FromDate.get)
         ).toList.filter(q =>  parseDate(q._2.SlotDate.toString()).get.before(a.ToDate.get)).toList
    }
    else if(a.FromDate != None &&  a.ToDate == None){
      
       
        joindateList = twinJoin.filter(p => parseDate(p._2.SlotDate.toString()).equals(a.FromDate)).toList
       
      // joindateList = twinJoin.filter(p => parseDate(p._2.SlotDate.toString()).equals(a.FromDate.get) 
         //  && parseDate(p._2.SlotDate.toString()).get.after(a.FromDate.get) )
      }*/
    
    if(a.forUser != None && a.forUser.get > 0){
      
      joindateList = twinJoin.filter(p => p._2.UserID.equals(a.forUser.get)).toList
    }
    
   
      var uName = "";
    val queryBuild = joindateList.drop(offset).take(a.PageSize)
     var timeList: Array[addTimeSheetobj] = new Array[addTimeSheetobj](queryBuild.length) 
     for(i <- 0 until queryBuild.length) {
      
      val doclist = doctors.filter { x => x.DoctorID === queryBuild(i)._2.UserID }.firstOption
      if(doclist != None){
        
        uName = doclist.get.FullName
      }
      
      log.info("Date format" + queryBuild(i)._2.SlotDate.toString())
      timeList(i) = new addTimeSheetobj(Some(queryBuild(i)._2.TimeSheetID),queryBuild(i)._2.TimeSlotID,queryBuild(i)._1.DoctorID.get,
                          queryBuild(i)._1.HospitalID,queryBuild(i)._1.HospitalCD,queryBuild(i)._2.ActiveIND,
                         queryBuild(i)._2.CrtUSR, queryBuild(i)._2.CrtUSRType,queryBuild(i)._2.UserID, queryBuild(i)._2.SlotDate,
                         queryBuild(i)._2.SlotType,queryBuild(i)._2.DutyType, queryBuild(i)._1.TimeFrom,
                         queryBuild(i)._1.TimeTo,Some(uName),queryBuild(i)._2.Remarks)
                         
                       
     }
     (timeList, joindateList.length)
   
   
   }
  
  def parseDate(value: String) = {
  try {
    Some(new SimpleDateFormat("yyyy-MM-dd").parse(value))
  } catch {
    case e: Exception => None
  }
} 
  
  def addUpdateTimeSlots(a: addTimeSlotobj) = db withSession { implicit session =>
    
     if (a.TimeSlotID != None && a.TimeSlotID.get > 0) {
    
        
       val tmeSlottbl = timeSlotTable.filter {x => x.TimeSlotID === a.TimeSlotID}.firstOption
       if(tmeSlottbl != None){
            timeSlotTable.filter{x=> x.TimeSlotID === a.TimeSlotID.get}.map { x=>
             (x.DoctorID,x.HospitalID,x.HospitalCD,x.ActiveIND,x.TimeFrom,x.TimeTo,x.Remarks, x.UpdtUSR,x.UpdtUSRType
                 )
              }.update(Some(a.DoctorID),a.HospitalID,a.HospitalCD,a.ActiveIND,Some(a.TimeFrom),Some(a.TimeTo),
                  a.Remarks,Some(a.CrtUSR),Some(a.CrtUSRType)  )          
         }
     }
     else
     {
         timeSlotTable.map { x => 
          (x.DoctorID,x.HospitalID,x.HospitalCD,x.ActiveIND,x.TimeFrom,x.TimeTo,x.Remarks,x.CrtUSR,x.CrtUSRType
           
              )
          
       }+= (Some(a.DoctorID),a.HospitalID,a.HospitalCD,a.ActiveIND,Some(a.TimeFrom),Some(a.TimeTo),
           a.Remarks,a.CrtUSR,a.CrtUSRType
           )
     
     } //a.TimeSlotID
  
   }
  
   def addUpdateTimeSheets(a: addTimeSheetobj) = db withSession { implicit session =>
    
     if (a.TimeSheetID != None && a.TimeSheetID.get > 0) {
    
        
       val tmesheettbl = timeSheetTable.filter {x => x.TimeSheetID === a.TimeSheetID.get}.firstOption
       if(tmesheettbl != None){
            timeSheetTable.filter{x=> x.TimeSheetID === a.TimeSheetID.get}.map { x=>
             (x.TimeSlotID,x.DoctorID,x.HospitalID,x.HospitalCD,x.ActiveIND,x.Remarks,x.SlotDate,x.SlotType,x.DutyType,
               x.CrtUSR,x.CrtUSRType,x.UserID  
                 )
              }.update(a.TimeSlotID,Some(a.DoctorID),a.HospitalID,a.HospitalCD,a.ActiveIND,a.Remarks,a.SlotDate,a.SlotType,a.DutyType,
                 a.CrtUSR,a.CrtUSRType,a.UserID 
              )
         }
     }
     else
     {
         timeSheetTable.map { x => 
          (x.TimeSlotID,x.DoctorID,x.HospitalID,x.HospitalCD,x.ActiveIND,x.UserID,x.Remarks,x.SlotDate,x.SlotType,x.DutyType,
              x.CrtUSR,x.CrtUSRType
           
              )
          
       }+= (a.TimeSlotID,Some(a.DoctorID),a.HospitalID,a.HospitalCD,a.ActiveIND,a.UserID,a.Remarks,a.SlotDate,a.SlotType,a.DutyType,a.CrtUSR,
           a.CrtUSRType
           )
     
     } //a.TimeSheetID
  
   }
   
   def listTimeSlots(a:listAvailableTimeSlots):(Array[addTimeSlotobj],Int) = db withSession { implicit session =>
     
   val offset = (a.PageNumber-1)  * a.PageSize
  
   var filterList = timeSlotTable.list 
      
   if(a.DoctorID != None && a.DoctorID.get > 0){
     
      filterList = timeSlotTable.filter { x => x.DoctorID === a.DoctorID.get }.list
         
   }
   
  if(a.HospitalID != None && a.HospitalID.get > 0){
     
      filterList = timeSlotTable.filter { x => x.HospitalID === a.HospitalID.get }.list
         
   }
   
  if(a.HospitalCD != None && a.HospitalCD.get != ""){
     
      filterList = timeSlotTable.filter { x => x.HospitalCD === a.HospitalCD.get }.list
         
   }
      var uName = "";
    val queryBuild = filterList.drop(offset).take(a.PageSize)
     var timeList: Array[addTimeSlotobj] = new Array[addTimeSlotobj](queryBuild.length) 
     for(i <- 0 until queryBuild.length) {
      
     
     timeList(i) = new addTimeSlotobj(Some(queryBuild(i).TimeSlotID),queryBuild(i).DoctorID.get,
                                      queryBuild(i).HospitalID,queryBuild(i).HospitalCD,
                                       queryBuild(i).ActiveIND,  queryBuild(i).CrtUSR,
                                        queryBuild(i).CrtUSRType,  queryBuild(i).TimeFrom.get,
                                         queryBuild(i).TimeTo.get, queryBuild(i).Remarks)
         
              
     }
    (timeList, filterList.length)
   
   
   }

}