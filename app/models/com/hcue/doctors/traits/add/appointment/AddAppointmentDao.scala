package models.com.hcue.doctors.traits.add.appointment

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import play.Logger

import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointments

import models.com.hcue.doctors.traits.list.Doctor.ListDoctorDao

import models.com.hcue.doctors.Json.Request.addDtAppointmentDetailsObj
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.buildDoctorAppointmentList
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.helper.hcueHelperClass

import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAppointmentExtn
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAppointmentExtn
import java.util.Calendar

object AddAppointmentDao {

  val log = Logger.of("application")

  val doctorAppointment = DoctorAppointments.DoctorAppointments
  val doctorAppointmentExtn = DoctorAppointmentExtn.DoctorAppointmentExtn
  
  def addAppointments(a: addDtAppointmentDetailsObj): (String, Long) = db withSession { implicit session =>

    var uniqueBooking = false
    
    var hospitalID: Option[Long] = None
    
    var hosCD : Option[String]= None
          
    var brCD : Option[String]= None  

    var parentHospitalID : Option[Long]=  None
    
    if(a.HospitalDetails != None) {
    
      hospitalID = a.HospitalDetails.get.HospitalID
    
      hosCD = a.HospitalDetails.get.HospitalCD 
          
      brCD = a.HospitalDetails.get.BranchCD 

      parentHospitalID =  a.HospitalDetails.get.ParentHospitalID  
    
    }else {
      
      hosCD = a.HospitalCD 
      brCD = a.BranchCD
      
    }
    
    val alreadyBookedAppointment: List[TDoctorAppointment] = doctorAppointment.filter(c => c.DoctorID === a.DoctorID &&
      c.ConsultationDt === a.ConsultationDt && c.AddressConsultID === a.AddressConsultID && c.AppointmentStatus =!= "C").sortBy { x => x.StartTime.asc }.list

    val bookedAppointmentList: Array[buildDoctorAppointmentList] = new Array[buildDoctorAppointmentList](alreadyBookedAppointment.length)
   
   // log.info("Booked Appointment List : "+alreadyBookedAppointment)
     for (i <- 0 until alreadyBookedAppointment.length) {

      val seqNo = (alreadyBookedAppointment(i).StartTime.replaceAll(":", "").toString()).toInt

      //log.info("Sequence : "+seqNo)
      bookedAppointmentList(i) = new buildDoctorAppointmentList(seqNo, alreadyBookedAppointment(i).AppointmentID, alreadyBookedAppointment(i).AddressConsultID: Long,
        alreadyBookedAppointment(i).DayCD, alreadyBookedAppointment(i).ConsultationDt, alreadyBookedAppointment(i).StartTime,
        alreadyBookedAppointment(i).EndTime, alreadyBookedAppointment(i).PatientID, alreadyBookedAppointment(i).VisitUserTypeID,
        alreadyBookedAppointment(i).DoctorID, alreadyBookedAppointment(i).FirstTimeVisit, alreadyBookedAppointment(i).DoctorVisitRsnID,
        alreadyBookedAppointment(i).OtherVisitRsn, alreadyBookedAppointment(i).AppointmentStatus,
        alreadyBookedAppointment(i).TokenNumber, None, None, None, None)

    }
    


    //Slot Start Time
    import java.util.Calendar
    var calStartTime: Calendar = CalenderDateConvertor.setDateTime(a.ConsultationDt, a.StartTime)
    var calEndTime: Calendar = CalenderDateConvertor.setDateTime(a.ConsultationDt, a.EndTime)
    val duration: Int = 15;
    var slotCount = ((CalenderDateConvertor.hoursDifference(calStartTime, calEndTime) / duration)).toInt

    log.info("Slot Count "+slotCount)
    //(endTime.replaceAll(":", "").toString()).toInt
    //Check for start time booking
    val startTime = (a.StartTime.replaceAll(":", "").toString()).toInt
    log.info("Start Time"+startTime)
    if (bookedAppointmentList.find(x => x.SeqNo == startTime) != None) {
      return ("Slot_Already_Booked", 0)
    }

    

    //Check for Single Date or Multiple Date
    if (slotCount <= 0) {
      slotCount = 1
    }

    if (slotCount == 1) {

      var calStartTime: Calendar = CalenderDateConvertor.setDateTime(a.ConsultationDt, a.StartTime)
      var calEndTime: Calendar = CalenderDateConvertor.setDateTime(a.ConsultationDt, a.EndTime)

      var startTime = a.StartTime
      var endTime = a.EndTime
      // var tokenNumber = 0

      if (a.StartTime.length == 8) {
        startTime = a.StartTime.substring(0, 5)
      }

      if (a.EndTime.length == 8) {
        endTime = a.EndTime.substring(0, 5)
      }


      
      val currentBookedAppointmentList: Array[buildDoctorAppointmentList] = new Array[buildDoctorAppointmentList](1)

      currentBookedAppointmentList(0) = new buildDoctorAppointmentList(0, 0, a.AddressConsultID, a.DayCD, a.ConsultationDt,
        startTime, endTime, Some(a.PatientID), a.VisitUserTypeID, Some(a.DoctorID), "N", a.DoctorVisitRsnID, a.OtherVisitRsn, "B", "0", a.VisitRsnID, a.PreliminaryNotes, a.ReferralSourceDtls, None)

   
      val appointmentList: List[Long] = insertAppointmentRecord(currentBookedAppointmentList,hospitalID,hosCD,brCD,parentHospitalID,a.USRId, a.USRType)

 
      var appointmentID: Long = 0

      if (appointmentList.length > 0) {
        appointmentID = appointmentList(0)
      }

      if (appointmentID == 0)
        return ("APPOINTMENT_FAILED", appointmentID)
      else
        return ("APPOINTMENT_SUCCESS", appointmentID)
    } else {

      val currentBookedAppointmentList: Array[buildDoctorAppointmentList] = new Array[buildDoctorAppointmentList](slotCount)

      var startTime = a.StartTime

      if (a.StartTime.length == 8) {
        startTime = a.StartTime.substring(0, 5)
      }

      var appointmentList: List[Long] = Nil


     
      appointmentList = insertAppointmentRecord(currentBookedAppointmentList,a.HospitalDetails.get.HospitalID,hosCD,brCD,parentHospitalID,a.USRId, a.USRType)
      
      
      if (appointmentList.length == 0)
        return ("APPOINTMENT_FAILED", 0)
      else
        return ("APPOINTMENT_SUCCESS", appointmentList(0))

    }

  }

  def insertAppointmentRecord(bookingList: Array[buildDoctorAppointmentList],hospitalID:Option[Long],hospitalCD:Option[String],branchCD:Option[String],ParentHospitalID:Option[Long],USRId: Long, USRType: String): List[Long] = db withSession { implicit session =>
 
    var appointmentList: List[Long] = Nil

    var parentDoctorID: Long = 0

    for (i <- 0 until bookingList.length) {
 
       val autoGenDoctorID = (doctorAppointment.map { x =>
        (x.AddressConsultID, x.DayCD, x.ConsultationDt, x.StartTime, x.EndTime, x.PatientID,
          x.VisitUserTypeID, x.DoctorID,x.FirstTimeVisit,  x.DoctorVisitRsnID, x.OtherVisitRsn,
          x.AppointmentStatus, x.TokenNumber, x.ParentAppointmentID, x.HospitalID, x.HospitalCode, x.BranchCode, x.CrtUSR, x.CrtUSRType)
      } returning doctorAppointment.map(_.AppointmentID)) +=
        (bookingList(i).AddressConsultID, bookingList(i).DayCD, bookingList(i).ConsultationDt,
          bookingList(i).StartTime, bookingList(i).EndTime, bookingList(i).PatientID, bookingList(i).VisitUserTypeID,
          bookingList(i).DoctorID, "N" , bookingList(i).DoctorVisitRsnID, bookingList(i).OtherVisitRsn,
          bookingList(i).AppointmentStatus, bookingList(i).TokenNumber, parentDoctorID, hospitalID, hospitalCD, branchCD, USRId, USRType)
      
          
        var refferalId: Option[Long] = None  
        var subRefferalId: Option[Long] = None  
        if(bookingList(i).ReferralSourceDtls != None) {
          refferalId = Some(bookingList(i).ReferralSourceDtls.get.ReferralID)
          subRefferalId = bookingList(i).ReferralSourceDtls.get.SubReferralID
        }
       
        var result: TDoctorAppointmentExtn = new TDoctorAppointmentExtn(autoGenDoctorID, bookingList(i).AddressConsultID,
          bookingList(i).DayCD, bookingList(i).ConsultationDt,
          bookingList(i).VisitUserTypeID,
          bookingList(i).DoctorVisitRsnID, bookingList(i).AppointmentStatus, bookingList(i).TokenNumber,
          ParentHospitalID,None,None,None,bookingList(i).VisitRsnID,bookingList(i).PreliminaryNotes,refferalId,subRefferalId, None)

        doctorAppointmentExtn.filter(c => c.AppointmentID === autoGenDoctorID).update(result)

      if (bookingList(i).AppointmentStatus.equals("B")) {
        parentDoctorID = autoGenDoctorID
        appointmentList ::= parentDoctorID
      }

    }

    appointmentList
  }
  
    def updateAppointmentRecord(bookingList: buildDoctorAppointmentList,status: String, parentAppointmentID: Long, hospitalID:Option[Long],hospitalCD:Option[String],branchCD:Option[String],ParentHospitalID:Option[Long],crtUsr: Long, crtUsrType:String, updtUsr: Option[Long], updtUsrType: Option[String]): Long = db withSession { implicit session =>
 
    var appointmentId = bookingList.AppointmentID  
    
    if(status == "B") {
     doctorAppointment.filter { x => x.AppointmentID === bookingList.AppointmentID}.map { x =>
        (x.AddressConsultID, x.DayCD, x.ConsultationDt, x.StartTime, x.EndTime, x.PatientID,
          x.VisitUserTypeID, x.DoctorID, x.FirstTimeVisit, x.DoctorVisitRsnID, x.OtherVisitRsn,
          x.AppointmentStatus, x.TokenNumber, x.ParentAppointmentID, x.HospitalID, x.HospitalCode, x.BranchCode, x.CrtUSR, x.CrtUSRType, x.UpdtUSR, x.UpdtUSRType)
      }.update(bookingList.AddressConsultID, bookingList.DayCD, bookingList.ConsultationDt,
          bookingList.StartTime, bookingList.EndTime, bookingList.PatientID, bookingList.VisitUserTypeID,
          bookingList.DoctorID, "N", bookingList.DoctorVisitRsnID, bookingList.OtherVisitRsn,
          bookingList.AppointmentStatus, bookingList.TokenNumber, parentAppointmentID, hospitalID, hospitalCD, branchCD, crtUsr, crtUsrType, updtUsr, updtUsrType)
            
     
      
        if (ParentHospitalID != None) {
             doctorAppointmentExtn.filter(c => c.AppointmentID === bookingList.AppointmentID).map{ x => x.ParentHospitalID}.update(ParentHospitalID)
        }
     } else if(status ==  "C") {
       doctorAppointment.filter { x => x.AppointmentID === bookingList.AppointmentID}.map { x => x.AppointmentStatus }.update("C")
     } else {
        val autoGenerateID = (doctorAppointment.map { x =>
          (x.AddressConsultID, x.DayCD, x.ConsultationDt, x.StartTime, x.EndTime, x.PatientID,
           x.VisitUserTypeID, x.DoctorID, x.FirstTimeVisit, x.DoctorVisitRsnID, x.OtherVisitRsn,
           x.AppointmentStatus, x.TokenNumber, x.ParentAppointmentID, x.HospitalID, x.HospitalCode, x.BranchCode, x.CrtUSR, x.CrtUSRType, x.UpdtUSR, x.UpdtUSRType)
        } returning doctorAppointment.map(_.AppointmentID)) += (bookingList.AddressConsultID, bookingList.DayCD, bookingList.ConsultationDt,
                 bookingList.StartTime, bookingList.EndTime, bookingList.PatientID, bookingList.VisitUserTypeID,
                 bookingList.DoctorID, "N", bookingList.DoctorVisitRsnID, bookingList.OtherVisitRsn,
                 bookingList.AppointmentStatus, bookingList.TokenNumber, parentAppointmentID, hospitalID, hospitalCD, branchCD, crtUsr, crtUsrType, updtUsr, updtUsrType)
        if (ParentHospitalID != None) {
             doctorAppointmentExtn.filter(c => c.AppointmentID === autoGenerateID).map{ x => x.ParentHospitalID}.update(ParentHospitalID)
        }
        appointmentId = autoGenerateID
      }
      appointmentId
    }

}