package models.com.hcue.doctors.traits.lookup

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.api.db.DB
import play.api.db.slick.DB

import models.com.hcue.doctors.Json.Response.getCommonLookupObj

trait Lookups {

  def loadLookupTables(): getCommonLookupObj

}

