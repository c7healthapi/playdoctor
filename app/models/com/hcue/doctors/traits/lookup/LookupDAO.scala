package models.com.hcue.doctors.traits.lookup

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.StaticQuery
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.interpolation
import models.com.hcue.doctors.Json.Request.getDoctorVisitRsnLkupObj
import models.com.hcue.doctors.Json.Request.allergensInfoObj
import models.com.hcue.doctors.Json.Request.getLkupObj
import models.com.hcue.doctors.Json.Response._
import models.com.hcue.doctors.Json.Response.getCommonLookupObj
import models.com.hcue.doctors.Json.Response.allergiesListObj
import models.com.hcue.doctors.dataCasting.DataCasting
import models.com.hcue.doctors.slick.lookupTable.AddressTypes
import models.com.hcue.doctors.slick.lookupTable.AppointmentStatus
import models.com.hcue.doctors.slick.lookupTable.CityLkup
import models.com.hcue.doctors.slick.lookupTable.CityLkup
import models.com.hcue.doctors.slick.lookupTable.ContactTypeLkup
import models.com.hcue.doctors.slick.lookupTable.Country
import models.com.hcue.doctors.slick.lookupTable.DayLkup
import models.com.hcue.doctors.slick.lookupTable.DoctorQualfLkup
import models.com.hcue.doctors.slick.lookupTable.DoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.DoctorVisitRsnLkyp
import models.com.hcue.doctors.slick.lookupTable.FamilyRltnLkup
import models.com.hcue.doctors.slick.lookupTable.LocationLkup
import models.com.hcue.doctors.slick.lookupTable.OtherIDTypeLkup
import models.com.hcue.doctors.slick.lookupTable.PhoneTypeLkup
import models.com.hcue.doctors.slick.lookupTable.StateLkup
import models.com.hcue.doctors.slick.lookupTable.TAddressTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TAppointmentStatusLkup
import models.com.hcue.doctors.slick.lookupTable.TCityLkup
import models.com.hcue.doctors.slick.lookupTable.TContactTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TCountryLkup
import models.com.hcue.doctors.slick.lookupTable.TDayLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorQualfLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorVisitRsnLkyp
import models.com.hcue.doctors.slick.lookupTable.TFamilyRltnLkup
import models.com.hcue.doctors.slick.lookupTable.TLocationLkup

import models.com.hcue.doctors.slick.lookupTable.TOtherIDTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TPhoneTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TStateLkup
import models.com.hcue.doctors.slick.lookupTable.TUSRTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TVisitorTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TCurrencyLkup
import models.com.hcue.doctors.slick.lookupTable.CurrencyLkup
import models.com.hcue.doctors.slick.lookupTable.USRTypeLkup
import models.com.hcue.doctors.slick.lookupTable.VisitorTypeLkup
import models.com.hcue.doctors.slick.transactTable.doctor.PatientRegistration
import models.com.hcue.doctors.slick.transactTable.doctor.TPatientRegistration
import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccessLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccountAccessLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueAccountLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HcueRoleLkup
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalGrpSetting
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalRoleSetting
import models.com.hcue.doctors.traits.dbProperties.db
import play.Logger
import play.api.db.slick.DB
import play.api.libs.json.JsValue
import models.com.hcue.doctors.Json.Response.CurrencyLkupObj
import models.com.hcue.doctors.slick.lookupTable.TLocationLkup
import models.com.hcue.doctors.slick.lookupTable.TLocationLkup
import play.libs.Json
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsArray
import play.api.libs.json.JsNumber
import play.api.libs.json.JsNull
import models.com.hcue.doctors.slick.lookupTable.TCityLkup
import models.com.hcue.doctors.helper.HCueUtils.stringconcatinate
import models.com.hcue.doctors.helper.HCueUtils
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.hospitals.THospital

object LookupDAO extends Lookups {

  val log = Logger.of("application")


  val addressType = AddressTypes.AddressTypes

  val contactType = ContactTypeLkup.ContactTypeLkup

  val countryLookup = Country.Country

  val cityTown = CityLkup.CityLkup

  val dayType = DayLkup.DayLkup

  val doctorQualfType = DoctorQualfLkup.DoctorQualfLkup

  val doctorSpecialityType = DoctorSpecialityLkup.DoctorSpecialityLkup

  val doctorVisitRsnType = DoctorVisitRsnLkyp.DoctorVisitRsnLkyp.sortBy(_.SequenceRow.asc.nullsFirst)

  val familyRltnType = FamilyRltnLkup.FamilyRltnLkup

  val OtherIDType = OtherIDTypeLkup.OtherIDTypeLkup

  val phoneType = PhoneTypeLkup.PhoneTypeLkup

  val stateType = StateLkup.StateLkup

  val currencyType = CurrencyLkup.CurrencyLkup

  val usrType = USRTypeLkup.USRTypeLkup

  val appointmentStatus = AppointmentStatus.AppointmentStatus

  val visitorTypeLkup = VisitorTypeLkup.VisitorTypeLkup

  val cityLkup = CityLkup.CityLkup

  val locationLookup = LocationLkup.LocationLkup

  val hospital = Hospital.Hospital.sortBy(_.HospitalID.asc.nullsFirst)

  val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn

  val doctors = Doctors.Doctors

  val patientRegistration = PatientRegistration.PatientReg

  //def specialityDesc(specialityDesc: String) =
  /*
  def query(value: String) = sql"""SELECT * FROM tDoctorSpecialityLkup WHERE SpecialityDesc_tsvector @@ plainto_tsquery('english', '#$value')""".as[(String,String,Option[String],String)]

  def specialityDesc(value: String):Array[DoctorSpecialityTypeObj]  = db withSession { implicit session =>

    val doctorSpecialityList = query(value).list
    
    val doctorSpecialityDetailsList: Array[DoctorSpecialityTypeObj] = new Array[DoctorSpecialityTypeObj](doctorSpecialityList.length)

    for (i <- 0 until doctorSpecialityList.length) {
      doctorSpecialityDetailsList(i) = new DoctorSpecialityTypeObj(doctorSpecialityList(i)._1,doctorSpecialityList(i)._2, doctorSpecialityList(i)._3,doctorSpecialityList(i)._4)
    }

    return doctorSpecialityDetailsList
    
  }*/

  def specialQuery(value: String) = sql"""SELECT * FROM tdoctorspecialitylkup WHERE specialitydesc_tsvector @@ plainto_tsquery('english', '#$value')""".as[(String, String, Option[String], String)]

  def specialityDesc(value: String): List[String] = db withSession { implicit session =>

    val doctorSpecialityList = specialQuery(value).list

    var doctorSpecialityDetailsList: List[String] = Nil

    for (i <- 0 until doctorSpecialityList.length) {
      val values: String = doctorSpecialityList(i)._1
      doctorSpecialityDetailsList ::= values
    }

    return doctorSpecialityDetailsList

  }

  def doctorSearchQuery(firstName: String, LastName: String) = sql"{call searchdoctor($firstName, $LastName)}".as[Long]

  def doctorSearch(firstName: String, LastName: String): List[Long] = db withSession { implicit session =>

    val doctorSearchList: List[Long] = doctorSearchQuery(firstName, LastName).list

    return doctorSearchList

  }

  def loadDoctorSpecialityTypeTables(active: String): Array[DoctorSpecialityTypeObj] = db withSession { implicit session =>

    var doctorSpecialityList: List[TDoctorSpecialityLkup] = null

    if (active.equalsIgnoreCase("")) {
      doctorSpecialityList = doctorSpecialityType.sortBy { x => x.DoctorSpecialityDesc.asc }.list
    } else {
      doctorSpecialityList = doctorSpecialityType.filter(c => c.ActiveIND === active).sortBy { x => x.DoctorSpecialityDesc.asc }.list
    }

    val doctorSpecialityDetailsList: Array[DoctorSpecialityTypeObj] = new Array[DoctorSpecialityTypeObj](doctorSpecialityList.length)

    for (i <- 0 until doctorSpecialityList.length) {

      var doctorOtherDesc = doctorSpecialityList(i).DoctorSpecialityDesc

      if (doctorOtherDesc != None) {
        doctorOtherDesc = stringconcatinate(doctorOtherDesc)     

        doctorSpecialityDetailsList(i) = new DoctorSpecialityTypeObj(doctorSpecialityList(i).DoctorSpecialityID,
          doctorSpecialityList(i).DoctorSpecialityDesc, doctorSpecialityList(i).SpecialityDesc, Some(doctorOtherDesc),
          doctorSpecialityList(i).ActiveIND)
      }
    }
    return doctorSpecialityDetailsList

  }

  def searchDoctorSpeciality(searchText: String): Array[DoctorSpecialityTypeObj] = db withSession { implicit session =>
    
    val doctorSpecialityList = doctorSpecialityType.filter { x => (x.DoctorSpecialityDesc.toLowerCase like ("%" + searchText.toLowerCase() + "%")) && x.ActiveIND === "Y" }.list

    val doctorSpecialityDetailsList: Array[DoctorSpecialityTypeObj] = new Array[DoctorSpecialityTypeObj](doctorSpecialityList.length)

    for (i <- 0 until doctorSpecialityList.length) {

      var doctorOtherDesc = doctorSpecialityList(i).DoctorSpecialityDesc

      if (doctorOtherDesc != None) {
        doctorOtherDesc = stringconcatinate(doctorOtherDesc)

        doctorSpecialityDetailsList(i) = new DoctorSpecialityTypeObj(doctorSpecialityList(i).DoctorSpecialityID,
          doctorSpecialityList(i).DoctorSpecialityDesc, doctorSpecialityList(i).SpecialityDesc, Some(doctorOtherDesc),
          doctorSpecialityList(i).ActiveIND)
      }
    }
    return doctorSpecialityDetailsList
  }

  //State Lookup

  def loadStateLookup(active: String): Array[StateLkupObj] = db withSession { implicit session =>

    var StateLookupList: List[TStateLkup] = null

    if (active.equalsIgnoreCase("")) {
      StateLookupList = stateType.sortBy { x => x.StateDesc.asc }.list
    } else {
      StateLookupList = stateType.sortBy { x => x.StateDesc.asc }.filter(c => c.ActiveIND === active && c.isPrivate === "N").list
    }

    val stateLookupDetailsList: Array[StateLkupObj] = new Array[StateLkupObj](StateLookupList.length)

    for (i <- 0 until StateLookupList.length) {
      var stateSEODesc = StateLookupList(i).StateDesc
      if (stateSEODesc != None) {
        stateSEODesc = stringconcatinate(stateSEODesc)

        stateLookupDetailsList(i) = new StateLkupObj(StateLookupList(i).StateID,
          StateLookupList(i).StateDesc, Some(stateSEODesc),
          StateLookupList(i).ActiveIND, StateLookupList(i).CountryID, StateLookupList(i).isPrivate)
      }
    }
    return stateLookupDetailsList
  }

  def loadStateLookups(a: getLkupObj): Array[StateLkupObj] = db withSession { implicit session =>

    var StateLookupList: List[TStateLkup] = null

    if (a.ActiveIND == "Y" && a.DoctorID != None) {
      StateLookupList = stateType.filter { x => x.ActiveIND === a.ActiveIND && (x.isPrivate === "N" || (x.CrtUSR === a.DoctorID && x.isPrivate === "Y")) }.sortBy { x => x.StateDesc.asc }.list
    } else if (a.ActiveIND == "Y") {
      StateLookupList = stateType.filter(c => c.ActiveIND === a.ActiveIND && c.isPrivate === "N").sortBy { x => x.StateDesc.asc }.list
    } else if (a.ActiveIND == "N") {
      StateLookupList = stateType.filter(c => c.ActiveIND === a.ActiveIND && c.isPrivate === "N").sortBy { x => x.StateDesc.asc }.list
    }

    val stateLookupDetailsList: Array[StateLkupObj] = new Array[StateLkupObj](StateLookupList.length)

    for (i <- 0 until StateLookupList.length) {
      var stateSEODesc = StateLookupList(i).StateDesc
      if (stateSEODesc != None) {
        
         stateSEODesc = stringconcatinate(stateSEODesc)
        stateLookupDetailsList(i) = new StateLkupObj(StateLookupList(i).StateID,
          StateLookupList(i).StateDesc, Some(stateSEODesc),
          StateLookupList(i).ActiveIND, StateLookupList(i).CountryID, StateLookupList(i).isPrivate)
      }
    }
    return stateLookupDetailsList
  }

  //change - added seo desc - 
  def loadCityLookup(active: String): Array[CityLkupObj] = db withSession { implicit session =>

    var cityLookupList: List[TCityLkup] = null

    if (active.equalsIgnoreCase("")) {
      cityLookupList = cityLkup.sortBy { x => x.CityIDDesc.asc }.list
    } else {
      cityLookupList = cityLkup.sortBy { x => x.CityIDDesc.asc }.filter(c => c.ActiveIND === active && c.isPrivate === "N").list
    }

    val cityLookupDetailsList: Array[CityLkupObj] = new Array[CityLkupObj](cityLookupList.length)

    for (i <- 0 until cityLookupList.length) {
      var cityOtherDesc = cityLookupList(i).CityIDDesc

      if (cityOtherDesc != None) {
        cityOtherDesc = stringconcatinate(cityOtherDesc)

        cityLookupDetailsList(i) = new CityLkupObj(cityLookupList(i).CityID,
          cityLookupList(i).CityIDDesc, Some(cityOtherDesc),
          cityLookupList(i).ActiveIND, cityLookupList(i).StateID, cityLookupList(i).isPrivate)
      }
    }
    return cityLookupDetailsList

  }

  def loadCityLookups(a: getLkupObj): Array[CityLkupObj] = db withSession { implicit session =>

    var cityLookupList: List[TCityLkup] = null

    if (a.ActiveIND == "Y" && a.DoctorID != None) {
      cityLookupList = cityLkup.filter { x => x.ActiveIND === a.ActiveIND && (x.isPrivate === "N" || (x.CrtUSR === a.DoctorID && x.isPrivate === "Y")) }.sortBy { x => x.CityIDDesc.asc }.list
    } else if (a.ActiveIND == "Y") {
      cityLookupList = cityLkup.filter(c => c.ActiveIND === a.ActiveIND && c.isPrivate === "N").sortBy { x => x.CityIDDesc.asc }.list
    } else if (a.ActiveIND == "N") {
      cityLookupList = cityLkup.filter(c => c.ActiveIND === a.ActiveIND && c.isPrivate === "N").sortBy { x => x.CityIDDesc.asc }.list
    }

    val cityLookupDetailsList: Array[CityLkupObj] = new Array[CityLkupObj](cityLookupList.length)

    for (i <- 0 until cityLookupList.length) {
      var cityOtherDesc = cityLookupList(i).CityIDDesc

      if (cityOtherDesc != None) {
        cityOtherDesc = stringconcatinate(cityOtherDesc)

        cityLookupDetailsList(i) = new CityLkupObj(cityLookupList(i).CityID,
          cityLookupList(i).CityIDDesc, Some(cityOtherDesc),
          cityLookupList(i).ActiveIND, cityLookupList(i).StateID, cityLookupList(i).isPrivate)
      }
    }
    return cityLookupDetailsList

  }
  def loadCountryLookup(active: String): Array[CountryLkupObj] = db withSession { implicit session =>

    var countryLookupList: List[TCountryLkup] = null

    if (active.equalsIgnoreCase("")) {
      countryLookupList = countryLookup.sortBy { x => x.CountryDesc.asc }.list
    } else {
      countryLookupList = countryLookup.sortBy { x => x.CountryDesc.asc }.filter(c => c.ActiveIND === active && c.isPrivate === "N").list
    }

    val countryLookupDetailsList: Array[CountryLkupObj] = new Array[CountryLkupObj](countryLookupList.length)

    for (i <- 0 until countryLookupList.length) {
      var countryOtherDesc = countryLookupList(i).CountryDesc

      if (countryOtherDesc != None) {
        countryOtherDesc = stringconcatinate(countryOtherDesc)
        countryLookupDetailsList(i) = new CountryLkupObj(countryLookupList(i).CountryID,
          countryLookupList(i).CountryDesc, countryOtherDesc,
          countryLookupList(i).ActiveIND, countryLookupList(i).CountryCode, countryLookupList(i).isPrivate)
      }
    }
    return countryLookupDetailsList
  }

  def loadCountryLookups(a: getLkupObj): Array[CountryLkupObj] = db withSession { implicit session =>

    var countryLookupList: List[TCountryLkup] = null

    if (a.ActiveIND == "Y" && a.DoctorID != None) {
      countryLookupList = countryLookup.filter { x => x.ActiveIND === a.ActiveIND && (x.isPrivate === "N" || (x.CrtUSR === a.DoctorID && x.isPrivate === "Y")) }.sortBy { x => x.CountryDesc.asc }.list
    } else if (a.ActiveIND == "Y") {
      countryLookupList = countryLookup.filter(c => c.ActiveIND === a.ActiveIND && c.isPrivate === "N").sortBy { x => x.CountryDesc.asc }.list
    } else if (a.ActiveIND == "N") {
      countryLookupList = countryLookup.filter(c => c.ActiveIND === a.ActiveIND && c.isPrivate === "N").sortBy { x => x.CountryDesc.asc }.list
    }

    val countryLookupDetailsList: Array[CountryLkupObj] = new Array[CountryLkupObj](countryLookupList.length)

    for (i <- 0 until countryLookupList.length) {
      var countryOtherDesc = countryLookupList(i).CountryDesc

      if (countryOtherDesc != None) {
        countryOtherDesc = stringconcatinate(countryOtherDesc)
        
        countryLookupDetailsList(i) = new CountryLkupObj(countryLookupList(i).CountryID,
          countryLookupList(i).CountryDesc, countryOtherDesc,
          countryLookupList(i).ActiveIND, countryLookupList(i).CountryCode, countryLookupList(i).isPrivate)
      }
    }
    return countryLookupDetailsList
  }
  def loadCurrencyLookup(active: String): Array[CurrencyLkupObj] = db withSession { implicit session =>

    var currencyLookupList: List[TCurrencyLkup] = null

    if (active.equalsIgnoreCase("")) {
      currencyLookupList = currencyType.sortBy { x => x.CurrencyDesc.asc }.list
    } else {
      currencyLookupList = currencyType.sortBy { x => x.CurrencyDesc.asc }.filter(c => c.ActiveIND === active).list
    }

    val currencyLookupDetailsList: Array[CurrencyLkupObj] = new Array[CurrencyLkupObj](currencyLookupList.length)

    for (i <- 0 until currencyLookupList.length) {
      var currencyOtherDesc = currencyLookupList(i).CurrencyDesc

      if (currencyOtherDesc != None) {
        currencyOtherDesc = stringconcatinate(currencyOtherDesc)
        currencyLookupDetailsList(i) = new CurrencyLkupObj(currencyLookupList(i).CurrencyCode,
          currencyLookupList(i).CurrencyDesc, currencyLookupList(i).ActiveIND, currencyLookupList(i).CountryID)
      }
    }
    return currencyLookupDetailsList
  }

  def loadLocationLookup(active: String): Array[LocationLkupObj] = db withSession { implicit session =>

    var LocationLookupList: List[TLocationLkup] = null

    if (active.equalsIgnoreCase("")) {
      LocationLookupList = locationLookup.sortBy { x => x.LocationIDDesc.asc }.list
    } else {
      LocationLookupList = locationLookup.filter(c => c.ActiveIND === active && c.isPrivate === "N").sortBy { x => x.LocationIDDesc.asc }.list
    }

    val LocationLookupDetailsList: Array[LocationLkupObj] = new Array[LocationLkupObj](LocationLookupList.length)

    for (i <- 0 until LocationLookupList.length) {

      var locationOtherDesc = LocationLookupList(i).LocationIDDesc

      if (locationOtherDesc != None) {
        
        locationOtherDesc = stringconcatinate(locationOtherDesc)

        LocationLookupDetailsList(i) = new LocationLkupObj(LocationLookupList(i).LocationID, LocationLookupList(i).LocationIDDesc, LocationLookupList(i).CityID, Some(locationOtherDesc), LocationLookupList(i).ActiveIND, LocationLookupList(i).isPrivate)
      }
    }

    return LocationLookupDetailsList

  }

  def loadLocationLookups(a: getLkupObj): Array[LocationLkupObj] = db withSession { implicit session =>

    var LocationLookupList: List[TLocationLkup] = null

    if (a.ActiveIND == "Y" && a.DoctorID != None) {
      LocationLookupList = locationLookup.filter { x => x.ActiveIND === a.ActiveIND && (x.isPrivate === "N" || (x.CrtUSR === a.DoctorID && x.isPrivate === "Y")) }.sortBy { x => x.LocationIDDesc.asc }.list
    } else if (a.ActiveIND == "Y") {
      LocationLookupList = locationLookup.filter(c => c.ActiveIND === a.ActiveIND && c.isPrivate === "N").sortBy { x => x.LocationIDDesc.asc }.list
    } else if (a.ActiveIND == "N") {
      LocationLookupList = locationLookup.filter(c => c.ActiveIND === a.ActiveIND && c.isPrivate === "N").sortBy { x => x.LocationIDDesc.asc }.list
    }

    val LocationLookupDetailsList: Array[LocationLkupObj] = new Array[LocationLkupObj](LocationLookupList.length)

    for (i <- 0 until LocationLookupList.length) {

      var locationOtherDesc = LocationLookupList(i).LocationIDDesc

      if (locationOtherDesc != None) {
        
        locationOtherDesc = stringconcatinate(locationOtherDesc)

        LocationLookupDetailsList(i) = new LocationLkupObj(LocationLookupList(i).LocationID, LocationLookupList(i).LocationIDDesc, LocationLookupList(i).CityID, Some(locationOtherDesc), LocationLookupList(i).ActiveIND, LocationLookupList(i).isPrivate)
      }
    }

    return LocationLookupDetailsList

  }

  def loadVisitorTypeLkupTables(active: String, visitorType: String): Array[VisitorTypeLkupObj] = db withSession { implicit session =>

    var visitorTypeList: List[TVisitorTypeLkup] = null

    if (active.equalsIgnoreCase("")) {

      if (visitorType.equalsIgnoreCase("all")) {
        visitorTypeList = visitorTypeLkup.sortBy { x => x.VisitUserTypeDesc.asc }.list
      } else {
        visitorTypeList = visitorTypeLkup.filter(c => c.VisitUserTypeID === visitorType).sortBy { x => x.VisitUserTypeDesc.asc }.list
      }

    } else {

      if (visitorType.equalsIgnoreCase("all")) {
        visitorTypeList = visitorTypeLkup.filter(c => c.ActiveIND === active).sortBy { x => x.VisitUserTypeDesc.asc }.list
      } else {
        visitorTypeList = visitorTypeLkup.filter(c => c.ActiveIND === active && c.VisitUserTypeID === visitorType).sortBy { x => x.VisitUserTypeDesc.asc }.list
      }

    }

    val visitorTypeDetailsList: Array[VisitorTypeLkupObj] = new Array[VisitorTypeLkupObj](visitorTypeList.length)

    for (i <- 0 until visitorTypeList.length) {
      visitorTypeDetailsList(i) = new VisitorTypeLkupObj(visitorTypeList(i).VisitUserTypeID, visitorTypeList(i).VisitUserTypeDesc, visitorTypeList(i).ActiveIND)
    }

    return visitorTypeDetailsList

  }

  def loadDoctorVisitRsnTypeTables(vt: getDoctorVisitRsnLkupObj): Array[DoctorVisitRsnTypeObj] = db withSession { implicit session =>

    var DoctorVisitRsnLkypList: List[TDoctorVisitRsnLkyp] = null
    var active: String = "Y"
    var specfication: List[String] = List("ALL")

    val inst: DataCasting = new DataCasting()

    if (vt.ActiveIND != None) {
      active = inst.stringconvertion(vt.ActiveIND)
    }

    if (vt.SpecificationID != None) {
      specfication = vt.SpecificationID.get.toList
    }

    if (vt.SpecificationID != None) {
      val buildQuery = doctorVisitRsnType.filter(c => c.DoctorSpecialityID inSet specfication)
      DoctorVisitRsnLkypList = buildQuery.filter(c => c.ActiveIND === active).sortBy { x => x.DoctorVisitRsnDesc.asc }.list
    } else {
      val buildQuery = doctorVisitRsnType
      DoctorVisitRsnLkypList = buildQuery.filter(c => c.ActiveIND === active).sortBy { x => x.DoctorVisitRsnDesc.asc }.list
    }

    val visitorTypeDetailsList: Array[DoctorVisitRsnTypeObj] = new Array[DoctorVisitRsnTypeObj](DoctorVisitRsnLkypList.length)

    for (i <- 0 until DoctorVisitRsnLkypList.length) {
      visitorTypeDetailsList(i) = new DoctorVisitRsnTypeObj(DoctorVisitRsnLkypList(i).DoctorVisitRsnID, DoctorVisitRsnLkypList(i).DoctorSpecialityID,
        DoctorVisitRsnLkypList(i).DoctorVisitRsnDesc, DoctorVisitRsnLkypList(i).VisitUserTypeID, DoctorVisitRsnLkypList(i).SequenceRow,
        DoctorVisitRsnLkypList(i).ActiveIND)
    }

    return visitorTypeDetailsList

  }

  def loadLookupTables(): getCommonLookupObj = db withSession { implicit session =>

    val addressTypeList: List[TAddressTypeLkup] = addressType.sortBy { x => x.AddressTypeDesc.asc }.list
    val addressTypeDetailsList: Array[AddressTypeObj] = new Array[AddressTypeObj](addressTypeList.length)

    for (i <- 0 until addressTypeList.length) {
      addressTypeDetailsList(i) = new AddressTypeObj(addressTypeList(i).AddressTypeID, addressTypeList(i).AddressTypeDesc, addressTypeList(i).ActiveIND)
    }

    val contactTypeList: List[TContactTypeLkup] = contactType.sortBy { x => x.ContactTypeDesc.asc }.list
    val contactTypeDetailsList: Array[ContactTypeObj] = new Array[ContactTypeObj](contactTypeList.length)

    for (i <- 0 until contactTypeList.length) {
      contactTypeDetailsList(i) = new ContactTypeObj(contactTypeList(i).ContactTypeID, contactTypeList(i).ContactTypeDesc, contactTypeList(i).ActiveIND)
    }

     //Country Lookup
    val countryTypeList: List[TCountryLkup] = countryLookup.sortBy { x => x.CountryDesc.asc }.list
    val countryTypeDetailsList: Array[CountryLkupObj] = new Array[CountryLkupObj](countryTypeList.length)

    for (i <- 0 until countryTypeList.length) {
      var countryOtherDesc = countryTypeList(i).CountryDesc.replace('.', '-');
      countryOtherDesc = countryOtherDesc.replace(' ', '-').toLowerCase();
      countryTypeDetailsList(i) = new CountryLkupObj(countryTypeList(i).CountryID, countryTypeList(i).CountryDesc, countryOtherDesc, countryTypeList(i).ActiveIND,countryTypeList(i).CountryCode,countryTypeList(i).isPrivate)
    }
    
    //City Lookup
    
    val cityTypeList: List[TCityLkup] = cityTown.sortBy { x => x.CityIDDesc.asc }.list
    val cityTypeDetailsList: Array[CityLkupObj] = new Array[CityLkupObj](cityTypeList.length)
    
    for(i <- 0 until cityTypeList.length)
    {
      var cityOtherDesc = cityTypeList(i).CityIDDesc.replace('.','-');
      cityOtherDesc = cityOtherDesc.replace(' ', '-').toLowerCase();
      cityTypeDetailsList(i) = new CityLkupObj(cityTypeList(i).CityID,
          cityTypeList(i).CityIDDesc, Some(cityOtherDesc),cityTypeList(i).ActiveIND, cityTypeList(i).StateID,cityTypeList(i).isPrivate)
    }

    // Location Lookup
    
    val locationTypeList: List[TLocationLkup] = locationLookup.sortBy { x => x.LocationIDDesc.asc }.list
    val locationTypeDetailsList: Array[LocationLkupObj] = new Array[LocationLkupObj](locationTypeList.length)
    
    for(i <- 0 until locationTypeList.length)
    {
      var locationOtherDesc = locationTypeList(i).LocationIDDesc.replace('.','-');
      locationOtherDesc = locationOtherDesc.replace(' ', '-').toLowerCase();
      locationTypeDetailsList(i) = new LocationLkupObj(locationTypeList(i).LocationID,
          locationTypeList(i).LocationIDDesc, locationTypeList(i).CityID,Some(locationOtherDesc), locationTypeList(i).ActiveIND, locationTypeList(i).isPrivate)
    }
   
    //Currency Lookup
    val currencyTypeList: List[TCurrencyLkup] = currencyType.sortBy { x => x.CurrencyDesc.asc }.list
    val currencyTypeDetailsList: Array[CurrencyLkupObj] = new Array[CurrencyLkupObj](currencyTypeList.length)

    for (i <- 0 until currencyTypeList.length) {
      currencyTypeDetailsList(i) = new CurrencyLkupObj(currencyTypeList(i).CurrencyCode, currencyTypeList(i).CurrencyDesc, currencyTypeList(i).ActiveIND, currencyTypeList(i).CountryID)
    }

    val dayTypeList: List[TDayLkup] = dayType.sortBy { x => x.DayDesc.asc }.list
    val dayTypeDetailsList: Array[DayTypeObj] = new Array[DayTypeObj](dayTypeList.length)

    for (i <- 0 until dayTypeList.length) {
      dayTypeDetailsList(i) = new DayTypeObj(dayTypeList(i).DayID, dayTypeList(i).DayDesc, dayTypeList(i).ActiveIND)
    }

    /*val doctorQualfList: List[TDoctorQualfLkup] = doctorQualfType.sortBy { x => x.DoctorQualfDesc.asc }.list
    val doctorQualfDetailsList: Array[DoctorQualfTypeObj] = new Array[DoctorQualfTypeObj](doctorQualfList.length)

    for (i <- 0 until doctorQualfList.length) {
      doctorQualfDetailsList(i) = new DoctorQualfTypeObj(doctorQualfList(i).DoctorQualfID, doctorQualfList(i).DoctorQualfDesc, doctorQualfList(i).ActiveIND)
    }*/

    val doctorSpecialityList: List[TDoctorSpecialityLkup] = doctorSpecialityType.sortBy { x => x.DoctorSpecialityDesc.asc }.list
    val doctorSpecialityDetailsList: Array[DoctorSpecialityTypeObj] = new Array[DoctorSpecialityTypeObj](doctorSpecialityList.length)

    for (i <- 0 until doctorSpecialityList.length) {

      var doctorOtherDesc = doctorSpecialityList(i).DoctorSpecialityDesc

      if (doctorOtherDesc != None) {
        doctorOtherDesc = stringconcatinate(doctorOtherDesc)

        doctorSpecialityDetailsList(i) = new DoctorSpecialityTypeObj(doctorSpecialityList(i).DoctorSpecialityID,
          doctorSpecialityList(i).DoctorSpecialityDesc, doctorSpecialityList(i).SpecialityDesc, Some(doctorOtherDesc),
          doctorSpecialityList(i).ActiveIND)
      }
    }

/*    val doctorVisitRsnList: List[TDoctorVisitRsnLkyp] = doctorVisitRsnType.sortBy { x => x.DoctorVisitRsnDesc.asc }.list
    val doctorVisitRsnDetailsList: Array[DoctorVisitRsnTypeObj] = new Array[DoctorVisitRsnTypeObj](doctorVisitRsnList.length)

    for (i <- 0 until doctorVisitRsnList.length) {
      doctorVisitRsnDetailsList(i) = new DoctorVisitRsnTypeObj(doctorVisitRsnList(i).DoctorVisitRsnID, doctorVisitRsnList(i).DoctorSpecialityID,
        doctorVisitRsnList(i).DoctorVisitRsnDesc, doctorVisitRsnList(i).VisitUserTypeID, doctorVisitRsnList(i).SequenceRow, doctorVisitRsnList(i).ActiveIND)
    }*/

   /* val familyRltnList: List[TFamilyRltnLkup] = familyRltnType.sortBy { x => x.FamilyRltnDesc.asc }.list
    val familyRltnDetailsList: Array[FamilyRltnTypeObj] = new Array[FamilyRltnTypeObj](familyRltnList.length)

    for (i <- 0 until familyRltnList.length) {
      familyRltnDetailsList(i) = new FamilyRltnTypeObj(familyRltnList(i).FamilyRltnID, familyRltnList(i).FamilyRltnDesc, familyRltnList(i).ActiveIND)
    }*/

    val otherIDTypeList: List[TOtherIDTypeLkup] = OtherIDType.sortBy { x => x.OtherIDTypeDesc.asc }.list
    val otherIDTypeDetailsList: Array[OtherIDTypeObj] = new Array[OtherIDTypeObj](otherIDTypeList.length)

    for (i <- 0 until otherIDTypeList.length) {
      otherIDTypeDetailsList(i) = new OtherIDTypeObj(otherIDTypeList(i).OtherIDTypeID, otherIDTypeList(i).OtherIDTypeDesc, otherIDTypeList(i).ActiveIND)
    }

    val phoneTypeList: List[TPhoneTypeLkup] = phoneType.sortBy { x => x.PhoneTypeDesc.asc }.list
    val phoneTypeDetailsList: Array[PhoneTypeObj] = new Array[PhoneTypeObj](phoneTypeList.length)

    for (i <- 0 until phoneTypeList.length) {
      phoneTypeDetailsList(i) = new PhoneTypeObj(phoneTypeList(i).PhoneTypeID, phoneTypeList(i).PhoneTypeDesc, phoneTypeList(i).ActiveIND)
    }

   val stateTypeList: List[TStateLkup] = stateType.sortBy { x => x.StateDesc.asc}.list
   val stateTypeDetailsList: Array[StateLkupObj] = new Array[StateLkupObj](stateTypeList.length)

   for (i <- 0 until stateTypeList.length) {
       var stateSEODesc =stateTypeList(i).StateDesc
      
      stateTypeDetailsList(i) = new StateLkupObj(stateTypeList(i).StateID, stateTypeList(i).StateDesc,Some(stateSEODesc), stateTypeList(i).ActiveIND,stateTypeList(i).CountryID,stateTypeList(i).isPrivate)
    }

    val usrTypeList: List[TUSRTypeLkup] = usrType.sortBy { x => x.USRTypeDesc.asc }.list
    val usrTypeDetailsList: Array[UsrTypeObj] = new Array[UsrTypeObj](usrTypeList.length)

    for (i <- 0 until usrTypeList.length) {
      usrTypeDetailsList(i) = new UsrTypeObj(usrTypeList(i).USRTypeID, usrTypeList(i).USRTypeDesc, usrTypeList(i).ActiveIND)
    }
    /****/

    val appointmentStatusList: List[TAppointmentStatusLkup] = appointmentStatus.sortBy { x => x.AppointmentStatusDesc.asc }.list
    val appointmentStatusDetailsList: Array[AppointmentStatusObj] = new Array[AppointmentStatusObj](appointmentStatusList.length)

    for (i <- 0 until appointmentStatusList.length) {
      appointmentStatusDetailsList(i) = new AppointmentStatusObj(appointmentStatusList(i).AppointmentStatusID, appointmentStatusList(i).AppointmentStatusDesc, appointmentStatusList(i).ActiveIND)
    }

    return new getCommonLookupObj(addressTypeDetailsList, contactTypeDetailsList, countryTypeDetailsList,cityTypeDetailsList,locationTypeDetailsList,
      dayTypeDetailsList, 
      //doctorQualfDetailsList, 
      doctorSpecialityDetailsList,
      //doctorVisitRsnDetailsList, 
      //familyRltnDetailsList, 
      otherIDTypeDetailsList,
      phoneTypeDetailsList, stateTypeDetailsList, usrTypeDetailsList, appointmentStatusDetailsList,currencyTypeDetailsList)

  }

  def getCountryDesc(code: String): Option[String] = db withSession { implicit session =>
    countryLookup.filter { x => x.CountryID === code }.map { x => x.CountryDesc }.firstOption
  }

  def getStateDesc(code: String): Option[String] = db withSession { implicit session =>
    stateType.filter { x => x.StateID === code }.map { x => x.StateDesc }.firstOption
  }

  def getCityTown(code: String): Option[String] = db withSession { implicit session =>
    cityTown.filter { x => x.CityID === code }.map { x => x.CityIDDesc }.firstOption
  }

  val hcueAccessLkup = HcueAccessLkup.HcueAccessLkup
  val hcueRoleLkup = HcueRoleLkup.HcueRoleLkup
  val hCueAccountLkup = HcueAccountLkup.HcueAccountLkup
  val hcueAccountAccessLkup = HcueAccountAccessLkup.HcueAccountAccessLkup
  val hospitalGrpSetting = HospitalGrpSetting.HospitalGrpSetting
  val hospitalRoleSetting = HospitalRoleSetting.HospitalRoleSetting

  def loadAdminLookup(HospitalId: Long): AdminLookup = db withSession { implicit session =>
    //Access Lookup

    val templistHcueAccessLkup = hcueAccessLkup.sortBy { x => x.AccessID }.list
    val listHcueAccessLkup: Array[HcueAccessLookup] = new Array[HcueAccessLookup](templistHcueAccessLkup.length)

    for (i <- 0 until templistHcueAccessLkup.length) {

      listHcueAccessLkup(i) = new HcueAccessLookup(templistHcueAccessLkup(i).AccessID, templistHcueAccessLkup(i).AccessDesc, templistHcueAccessLkup(i).ActiveIND, templistHcueAccessLkup(i).ParentAccessID)
    }

    // Incentive Mapping
 /*   val samp = hospital.filter { x => x.HospitalID === HospitalId }.first

    val ParentHospitalID = hospital.filter { x => x.HospitalID === HospitalId }.map { x => x.ParentHospitalID.get }.first
    val hospitalIncentiveRecords = hospitalIncentiveMapping.filter { x => x.ParentHospitalID === ParentHospitalID }.list
    val grpIncentiveRecords = hospitalIncentiveRecords.groupBy { x => x.TreatmentStatus }.toList
    val IncentiveMappingInfo = new Array[IncentiveMappingInfo](grpIncentiveRecords.length)
    for (i <- 0 until grpIncentiveRecords.length) {

      val treatmentType = grpIncentiveRecords(i)._1
      val incentiveRecords = grpIncentiveRecords(i)._2.map { x => (x.IncentiveID, x.IncentiveType) }
      val incentiveRecordinfo = new Array[IncentiveRecordInfo](incentiveRecords.length)
      for (j <- 0 until incentiveRecords.length) {

        val incentiveID = incentiveRecords(j)._1
        val incentiveType = incentiveRecords(j)._2
        incentiveRecordinfo(j) = new IncentiveRecordInfo(incentiveID, incentiveType)

      }
      IncentiveMappingInfo(i) = new IncentiveMappingInfo(treatmentType, incentiveRecordinfo)

    }*/

    //Role Lookup
    val templistHcueRoleLkup = hcueRoleLkup.sortBy { x => x.RoleID }.list
    val listHcueRoleLkup: Array[HcueRoleLookup] = new Array[HcueRoleLookup](templistHcueRoleLkup.length)

    for (i <- 0 until templistHcueRoleLkup.length) {

      listHcueRoleLkup(i) = new HcueRoleLookup(templistHcueRoleLkup(i).RoleID, templistHcueRoleLkup(i).RoleDesc, templistHcueRoleLkup(i).ActiveIND,
        templistHcueRoleLkup(i).IsPrivate, templistHcueRoleLkup(i).HospitalID)
    }

    //Account Lookup
    val templistHCueAccountLkup = hCueAccountLkup.sortBy { x => x.AccountID }.list
    val listHCueAccountLkup: Array[HCueAccountLookup] = new Array[HCueAccountLookup](templistHCueAccountLkup.length)

    for (i <- 0 until templistHCueAccountLkup.length) {

      listHCueAccountLkup(i) = new HCueAccountLookup(templistHCueAccountLkup(i).AccountID, templistHCueAccountLkup(i).AccountDesc, templistHCueAccountLkup(i).ActiveIND)
    }

    //Account Access Lookup
    val templistHcueAccountAccessLkup = hcueAccountAccessLkup.sortBy { x => x.AccessID }.list
    val listHcueAccountAccessLkup: Array[HcueAccountAccessLookup] = new Array[HcueAccountAccessLookup](templistHcueAccountAccessLkup.length)

    for (i <- 0 until templistHcueAccountAccessLkup.length) {

      listHcueAccountAccessLkup(i) = new HcueAccountAccessLookup(templistHcueAccountAccessLkup(i).AcntAccessID, templistHcueAccountAccessLkup(i).AccessID,
        templistHcueAccountAccessLkup(i).AccountID, templistHcueAccountAccessLkup(i).ActiveIND)
    }

    val templistHospitalGrpSettings = hospitalGrpSetting.filter { x => x.HospitalID === HospitalId }.list
    val listHospitalGrpSettings: Array[HcueHospitalGrpSettings] = new Array[HcueHospitalGrpSettings](templistHospitalGrpSettings.length)

    for (i <- 0 until templistHospitalGrpSettings.length) {

      listHospitalGrpSettings(i) = new HcueHospitalGrpSettings(templistHospitalGrpSettings(i).GroupCode, templistHospitalGrpSettings(i).GroupName,
        templistHospitalGrpSettings(i).HospitalID, templistHospitalGrpSettings(i).AccountAccessID, templistHospitalGrpSettings(i).Active)
    }

    val templistHospitalRoleSetting = hospitalRoleSetting.filter { x => x.HospitalID === HospitalId }.list
    val listHospitalRoleSetting: Array[HcueHospitalRoleSettings] = new Array[HcueHospitalRoleSettings](templistHospitalRoleSetting.length)

    for (i <- 0 until templistHospitalRoleSetting.length) {

      listHospitalRoleSetting(i) = new HcueHospitalRoleSettings(templistHospitalRoleSetting(i).HospitalID, templistHospitalRoleSetting(i).RoleID,
        templistHospitalRoleSetting(i).ActiveIND)
    }

    val hospitalInfo: List[THospital] = hospital.filter { x => x.ParentHospitalID === HospitalId }.list
    val listHcueHospitalInfo: Array[HcueHospitalInfo] = new Array[HcueHospitalInfo](hospitalInfo.length)

    for (i <- 0 until hospitalInfo.length) {
      var hospitalId = hospitalInfo(i).HospitalID
      val addressExtnInfo: List[TDoctorAddressExtn] = doctorAddressExtn.filter { x => x.HospitalID === hospitalId }.list
      val listHcueHospitalDoctorInfo: Array[HcueHospitalDoctorInfo] = new Array[HcueHospitalDoctorInfo](addressExtnInfo.length)
      for (i <- 0 until addressExtnInfo.length) {
        var DoctorID = addressExtnInfo(i).DoctorID;
        val DoctorName = doctors.filter { x => x.DoctorID === DoctorID }.firstOption
        val RoleID = addressExtnInfo(i).RoleID
        val GroupID = addressExtnInfo(i).GroupID
        listHcueHospitalDoctorInfo(i) = new HcueHospitalDoctorInfo(DoctorID, DoctorName.get.FullName, RoleID, GroupID)
      }
      var parentHospitalFlag: Option[String] = None
      if (hospitalInfo(i).HospitalID == hospitalInfo(i).ParentHospitalID.get) {
        parentHospitalFlag = Some("Y")
      }
      listHcueHospitalInfo(i) = new HcueHospitalInfo(hospitalId, hospitalInfo(i).HospitalName.get, hospitalInfo(i).ActiveIND, parentHospitalFlag, listHcueHospitalDoctorInfo, hospitalInfo(i).Preference)
    }

    var patientReg: Option[JsValue] = None
    val patRegInfo = patientRegistration.filter { x => x.ID === HospitalId }.firstOption
    if (patRegInfo != None) {
      patientReg = Some(patRegInfo.get.RegSettings)
    }

    // Admin Lookup
    val adminLookup: AdminLookup = new AdminLookup(listHcueAccessLkup, listHcueRoleLkup, listHCueAccountLkup, listHcueAccountAccessLkup,
      listHospitalGrpSettings, listHospitalRoleSetting, listHcueHospitalInfo, None, patientReg)

    return adminLookup
  }


}