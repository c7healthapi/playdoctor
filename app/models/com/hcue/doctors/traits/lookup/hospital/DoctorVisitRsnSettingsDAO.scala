package models.com.hcue.doctors.traits.lookup.hospital

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.Logger
import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.Json.Request.addUpdateVisitRsnSettingsObj
import models.com.hcue.doctors.Json.Request.listAvailableVisitRsnObj
import models.com.hcue.doctors.Json.Request.availableVisitRsnResponseObj
import models.com.hcue.doctors.slick.lookupTable.hospital.TDoctorVisitRsnSettings
import models.com.hcue.doctors.slick.lookupTable.hospital.DoctorVisitRsnSettings

object DoctorVisitRsnSettingsDAO {
  
   val visitRsnSettings = DoctorVisitRsnSettings.DoctorVisitRsnSetting
  
    def loadDoctorVisitRsnSetting(a: listAvailableVisitRsnObj): (Array[availableVisitRsnResponseObj],Int) = db withSession { implicit session =>
 
    val offset = (a.PageNumber-1)  * a.PageSize
    val startsWith = a.StartsWith
    
    var buildQuery = if(a.HospitalCD != None) { 
         for { getvisitRsn <- visitRsnSettings.filter(c => c.HospitalCD === a.HospitalCD.get)
            } yield (getvisitRsn)
     } else {
         for { getvisitRsn <- visitRsnSettings.filter(c => c.DoctorID === a.DoctorID)
            } yield (getvisitRsn)
     }
 
    var querySet = buildQuery
    
    if(startsWith != None) {
        querySet = buildQuery.filter { x => x.VisitRsnTitle.toLowerCase.startsWith(startsWith.get.toLowerCase()) }
    }
    
    var query = querySet
    if(a.ActiveIND != None) {
      query = querySet.filter { x => x.ActiveIND === a.ActiveIND.get }
    }
  
    val queryResult = query.sortBy { x => x.VisitRsnTitle.asc }.drop(offset).take(a.PageSize).list.distinct
    
    
    val returnResult: Array[availableVisitRsnResponseObj] = new Array[availableVisitRsnResponseObj](queryResult.length)

    for (i <- 0 until queryResult.length) {
        returnResult(i)= new availableVisitRsnResponseObj(queryResult(i).VisitRsnID, queryResult(i).VisitRsnTitle,
                                     queryResult(i).VisitRsnDesc, queryResult(i).ActiveIND)
      }
    
    return (returnResult,query.run.length)
  }
   
 
   def addUpdateDoctorVisitRsn(a: addUpdateVisitRsnSettingsObj) = db withSession { implicit session =>
    
     if (a.VisitRsnID != None) {
      val referralSetting: Option[TDoctorVisitRsnSettings] = if (a.HospitalCD != None) {
        visitRsnSettings.filter { x => x.HospitalCD === a.HospitalCD.get && x.VisitRsnID === a.VisitRsnID }.firstOption
      } else {
         visitRsnSettings.filter { x => x.DoctorID === a.DoctorID && x.VisitRsnID === a.VisitRsnID }.firstOption
      }
      
      if (referralSetting != None) {
        if(a.HospitalCD != None) {
            visitRsnSettings.filter { x => x.HospitalCD === a.HospitalCD.get && x.VisitRsnID === a.VisitRsnID.get }.map { x =>
            (x.DoctorID, x.HospitalCD, x.VisitRsnID, x.VisitRsnTitle, x.VisitRsnDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType)
          }.update(a.DoctorID, a.HospitalCD, a.VisitRsnID.get, a.VisitRsnTitle, a.VisitRsnDesc, a.ActiveIND, Some(a.USRId), Some(a.USRType))
        } else {
            visitRsnSettings.filter { x => x.DoctorID === a.DoctorID && x.VisitRsnID === a.VisitRsnID.get }.map { x =>
            (x.DoctorID, x.HospitalCD, x.VisitRsnID, x.VisitRsnTitle, x.VisitRsnDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType)
          }.update(a.DoctorID, a.HospitalCD, a.VisitRsnID.get, a.VisitRsnTitle, a.VisitRsnDesc, a.ActiveIND, Some(a.USRId), Some(a.USRType))
        }
       }
      }
     else {
      visitRsnSettings.map { x =>
         (x.DoctorID, x.HospitalCD, x.VisitRsnID, x.VisitRsnTitle, x.VisitRsnDesc, x.ActiveIND, x.CrtUSR, x.CrtUSRType)
      } +=
        (a.DoctorID, a.HospitalCD, 0, a.VisitRsnTitle, a.VisitRsnDesc, a.ActiveIND, a.USRId, a.USRType)
     }
    }



}