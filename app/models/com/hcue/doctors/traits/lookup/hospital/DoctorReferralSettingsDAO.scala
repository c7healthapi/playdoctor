package models.com.hcue.doctors.traits.lookup.hospital

import org.postgresql.ds.PGSimpleDataSource
import scala.slick.driver.PostgresDriver.simple._
import play.Logger
import models.com.hcue.doctors.traits.dbProperties.db

import models.com.hcue.doctors.Json.Request.addUpdateReferralSettingsObj
import models.com.hcue.doctors.Json.Request.listAvailableReferralObj
import models.com.hcue.doctors.Json.Request.availableReferralResponseObj
import models.com.hcue.doctors.slick.lookupTable.hospital.TDoctorReferralSettings
import models.com.hcue.doctors.slick.lookupTable.hospital.DoctorReferralSettings
import models.com.hcue.doctors.slick.transactTable.doctor.catagory.DoctorSubCatagorySetting

object DoctorReferralSettingsDAO {
  
  val referralSettings = DoctorReferralSettings.DoctorReferralSetting
  val doctorSubCatagorySetting = DoctorSubCatagorySetting.DoctorSubCatagorySetting
  
    def loadDoctorReferralSetting(a: listAvailableReferralObj): (Array[availableReferralResponseObj],Int) = db withSession { implicit session =>
 
    val offset = (a.PageNumber-1)  * a.PageSize
 
     val startsWith = a.StartsWith
     
    var buildQuery = if(a.HospitalCD != None) {
        if(a.ActiveIND != None) {
          for {
            getReferral <- referralSettings.filter(c => c.HospitalCD === a.HospitalCD.get && c.ActiveIND === a.ActiveIND.get)
            } yield (getReferral)
        } else {
              for {
            getReferral <- referralSettings.filter(c => c.HospitalCD === a.HospitalCD.get)
            } yield (getReferral)
        }  

    } else {
        if(a.ActiveIND != None) {
          for {
            getReferral <- referralSettings.filter(c => c.DoctorID === a.DoctorID && c.ActiveIND === a.ActiveIND.get)
           } yield (getReferral)
        } else {
            for {
             getReferral <- referralSettings.filter(c => c.DoctorID === a.DoctorID)
           } yield (getReferral)
        }
     }
    
    var querySet = buildQuery
    
    if(startsWith != None){
        querySet = buildQuery.filter { x => x.ReferralTitle.toLowerCase.startsWith(startsWith.get.toLowerCase()) }
    }
  
    val queryResult = querySet.sortBy { x => x.ReferralTitle.asc }.drop(offset).take(a.PageSize).list.distinct
 
    val returnResult: Array[availableReferralResponseObj] = new Array[availableReferralResponseObj](queryResult.length)

    for (i <- 0 until queryResult.length) {
        returnResult(i)= new availableReferralResponseObj(queryResult(i).ReferralID, queryResult(i).ReferralTitle,
                                     queryResult(i).ReferralDesc, queryResult(i).ActiveIND)
      }
    
    return (returnResult,querySet.run.length)
  }


  def addUpdateDoctorReferral(a: addUpdateReferralSettingsObj) = db withSession { implicit session =>
    
     if (a.ReferralID != None) {
    
    val referralSetting = if(a.HospitalCD != None) {
      referralSettings.filter { x => x.HospitalCD === a.HospitalCD.get && x.ReferralID === a.ReferralID }.firstOption
    } else {
      referralSettings.filter { x => x.DoctorID === a.DoctorID && x.ReferralID === a.ReferralID }.firstOption
    }
     if (referralSetting != None) {
       
        if(a.HospitalCD != None) {
             referralSettings.filter { x => x.HospitalCD === a.HospitalCD.get && x.ReferralID === a.ReferralID.get }.map { x =>
          (x.DoctorID, x.HospitalCD, x.ReferralID, x.ReferralTitle, x.ReferralDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType)
         }.update(a.DoctorID, a.HospitalCD, a.ReferralID.get, a.ReferralTitle, a.ReferralDesc, a.ActiveIND, Some(a.USRId), Some(a.USRType))
        } else {
             referralSettings.filter { x => x.DoctorID === a.DoctorID && x.ReferralID === a.ReferralID.get }.map { x =>
          (x.DoctorID, x.HospitalCD, x.ReferralID, x.ReferralTitle, x.ReferralDesc, x.ActiveIND, x.UpdtUSR, x.UpdtUSRType)
         }.update(a.DoctorID, a.HospitalCD, a.ReferralID.get, a.ReferralTitle, a.ReferralDesc, a.ActiveIND, Some(a.USRId), Some(a.USRType))
   
        }
        
        if(a.ActiveIND == "N") {
          doctorSubCatagorySetting.filter { x => x.CatagoryID === a.ReferralID }.map { x => x.ActiveIND }.update("N") 
        }
      }
     
     }
     else {

      referralSettings.map { x =>
         (x.DoctorID, x.HospitalCD, x.ReferralID, x.ReferralTitle, x.ReferralDesc, x.ActiveIND, x.CrtUSR, x.CrtUSRType)
      } +=
        (a.DoctorID, a.HospitalCD, 0, a.ReferralTitle, a.ReferralDesc, a.ActiveIND, a.USRId, a.USRType)
    }

     
    }
  }