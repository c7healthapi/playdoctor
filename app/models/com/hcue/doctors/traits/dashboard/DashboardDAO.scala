package models.com.hcue.doctors.traits.dashboard

import java.sql.Date

import scala.collection.mutable.ListBuffer
import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.booleanOptionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.optionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker

import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.DashboardInfoTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.EachRecordObj
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.dctrAptmtDtls
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.NewDashboardInfoTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.patientVisitsDtlsObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.AgeDtls
import models.com.hcue.doctors.Json.Request.doctorCommStatusObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.totalAmountObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.outstandingObjDtls
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.revenueTargetObj

import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.GeneralStat.Report
import models.com.hcue.doctors.helper.hcueDocuments

import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.RevenueObjDtls

import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.Play.current
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import java.util.Calendar
import java.text.SimpleDateFormat
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.EnquiryDashboardInfoInputTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.ListEnquiryDashboardInfo
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getDailyReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getDailyReportListInfo
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getDailySpecialityReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listDailySpecialityReportListInfo
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getCostReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listCostReportListInfo
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getReferralReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listReferralReportListInfo
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listAppointmentpracticeReport
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listPatientinSystemInfo
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listClinicauditcriteriaInfo
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listCostanalyticsreportInfo
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listAppointmentpracticeindicationsReport
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getCostReportwithregionInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getReferralwithoutIndicationObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getclinicauditReportInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getAppointmentpracticeInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getCalenderlistviewInfoObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getAgentstatsInfoObj
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.util.IOUtils;
import java.io.FileOutputStream
import java.io.File
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.listForecastReportListInfo
import org.json.JSONArray
import org.json.JSONObject
import play.api.db.slick.DB
import scala.slick.driver.PostgresDriver.simple.queryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.queryToInsertInvoker
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getReferralReportInfoObjv1
import org.apache.poi.ss.util.CellRangeAddress
import java.io.InputStream
import java.io.FileInputStream
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.CreationHelper
import org.apache.poi.ss.usermodel.Drawing
import org.apache.poi.ss.usermodel.ClientAnchor
import org.apache.poi.ss.usermodel.Picture
import java.lang.Short
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.util.CellRangeAddressList
import org.apache.poi.hssf.usermodel.DVConstraint
import org.apache.poi.ss.usermodel.DataValidation
import org.apache.poi.hssf.usermodel.HSSFDataValidation
import org.apache.poi.ss.usermodel.DataValidationHelper
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper
import org.apache.poi.ss.usermodel.DataValidationConstraint
import org.apache.poi.xssf.usermodel.XSSFColor
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.hssf.util.HSSFColor.WHITE
import org.apache.poi.xssf.usermodel.XSSFFont
import org.apache.poi.ss.usermodel.BorderStyle
import models.com.hcue.doctors.slick.transactTable.patient.EnquirySpeciality
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.specialitydetailsObj

object DashboardDAO {
  
  val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn
  val hospital =  Hospital.Hospital
  val generalreport = Report.Report
  val enquirySpeciality = EnquirySpeciality.EnquirySpeciality
  
  def getDashboardInfo (hospitalId: Long, doctorid : Long, addressid : Long, startDate: Date, endDate: Date, includeBranch: String) : DashboardInfoTypeObj  = db withSession { implicit session =>
    var AddressID: Long = addressid
    if (hospitalId > 0L && doctorid > 0L) {
      AddressID = doctorAddressExtn.filter { x => x.HospitalID === hospitalId && x.DoctorID === doctorid }.map { x => x.AddressID }.firstOption.getOrElse(0L)
    }
    val dashboardInfo = getDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch).first

           var topTrendDoctors = ListBuffer[EachRecordObj]()
           var topTrendCosultants = ListBuffer[EachRecordObj]()
           var topTrendBranches = ListBuffer[EachRecordObj]()
           var topTrendTreatments = ListBuffer[EachRecordObj]()
           var topRatedDoctors = ListBuffer[EachRecordObj]()
           
           
           var doctorAppointmentDtls  = new  dctrAptmtDtls(0,0,0,0)
    
    
            if(dashboardInfo._10 != null) {
             
            val topTrendDoctorsrecords = dashboardInfo._10.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendDoctorsrecords(0).isEmpty()){
            for(j <- 0 until topTrendDoctorsrecords.length){
              val topTrendDoctorsrec = topTrendDoctorsrecords(j).split(":")
              topTrendDoctors += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }
            if(dashboardInfo._11 != null) {
             
            val topTrendCosultantsrecords = dashboardInfo._11.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendCosultantsrecords(0).isEmpty()){
            for(j <- 0 until topTrendCosultantsrecords.length){
              val topTrendCosultantsrec = topTrendCosultantsrecords(j).split(":")
              topTrendCosultants += new EachRecordObj(topTrendCosultantsrec(0),topTrendCosultantsrec(1))  
            }
           }
          }
           
           
            
            if(dashboardInfo._12 != null) {
             
            val topTrendBranchesrecords = dashboardInfo._12.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendBranchesrecords(0).isEmpty()){
            for(j <- 0 until topTrendBranchesrecords.length){
              val topTrendBranchesrec = topTrendBranchesrecords(j).split(":")
              topTrendBranches += new EachRecordObj(topTrendBranchesrec(0),topTrendBranchesrec(1))  
            }
           }
          }
            if(dashboardInfo._13 != null) {
             
            val topTrendTreatmentsrecords = dashboardInfo._13.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendTreatmentsrecords(0).isEmpty()){
            for(j <- 0 until topTrendTreatmentsrecords.length){
              val topTrendTreatmentsrec = topTrendTreatmentsrecords(j).split(":")
              topTrendTreatments += new EachRecordObj(topTrendTreatmentsrec(0),topTrendTreatmentsrec(1))  
            }
           }
          }
           var avgTime = "0 Min"
             if(dashboardInfo._16 != null) {
             
            val topRatedDoctorrecords = dashboardInfo._16.toString.replaceAll("\"","").toString.split("~")
             if(!topRatedDoctorrecords(0).isEmpty()){
            for(j <- 0 until topRatedDoctorrecords.length){
              val topRatedDoctorsrec = topRatedDoctorrecords(j).split(":")
              topRatedDoctors += new EachRecordObj(topRatedDoctorsrec(0),topRatedDoctorsrec(1))  
            }
           }
          }
           
           if(dashboardInfo._18 != null) {             
            val topTrendDoctorsrecords = dashboardInfo._18.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendDoctorsrecords(0).isEmpty()){
            for(j <- 0 until topTrendDoctorsrecords.length){
              val topTrendDoctorsrec = topTrendDoctorsrecords(j).split("")
              doctorAppointmentDtls  = new dctrAptmtDtls(topTrendDoctorsrecords(0).toInt,topTrendDoctorsrecords(1).toInt,topTrendDoctorsrecords(2).toInt,topTrendDoctorsrecords(3).toInt)  
            }
           }
          }
            
           try {
           
             if(dashboardInfo._14 != null && dashboardInfo._14.length() > 0) {
            
             val timeAray = dashboardInfo._14.split(":")
             val hh = timeAray(0)
             val min = timeAray(1)
             
             avgTime = ((timeAray(0).toInt * 60) + min.toInt).toString + " Mins"
              }
           
           } catch {
            case e: Exception =>
              avgTime = "1 Day"
              e.printStackTrace()
          }
                   
           new DashboardInfoTypeObj(dashboardInfo._1,dashboardInfo._2,dashboardInfo._3,dashboardInfo._4,
                   dashboardInfo._5,dashboardInfo._6,dashboardInfo._7,dashboardInfo._8,dashboardInfo._9,
                   topTrendDoctors.toArray,
                   topTrendCosultants.toArray,
                   topTrendBranches.toArray,
                   topTrendTreatments.toArray,
                   avgTime ,Some(dashboardInfo._15 ),topRatedDoctors.toArray,dashboardInfo._17,Some(doctorAppointmentDtls))
                
    }

  def getDashboardInfoQuery(hospitalId: Long,doctorid: Long,addressid: Long, startDate: Date, endDate: Date, includeBranch: String)  = 
      sql"SELECT * FROM getdashboardinfo('#$hospitalId','#$doctorid','#$addressid','#$startDate','#$endDate','#$includeBranch')".as[(Double,Double,Int,Double,Int,Int,Int,Int,Int,String,String,String,String,String,Double,String,Int,String)]
  
 
  
  def getNewDashBoardInfo (hospitalId: Long, doctorid : Long, addressid : Long, startDate: Date, endDate: Date, includeBranch: String, dashboardtype: String,
                           inventoryType : String,referralId: Option[Long], subreferralId: Option[Long],appointmentVisit:Option[String]) : (NewDashboardInfoTypeObj,Option[JsValue])  = db withSession { implicit session =>
    var AddressID: Long = addressid
    if (hospitalId > 0L && doctorid > 0L) {
      AddressID = doctorAddressExtn.filter { x => x.HospitalID === hospitalId && x.DoctorID === doctorid }.map { x => x.AddressID }.firstOption.getOrElse(0L)
    }
           var topTrendDoctors = ListBuffer[EachRecordObj]()
           var topTrendCosultants = ListBuffer[EachRecordObj]()
           var topTrendBranches = ListBuffer[EachRecordObj]()
           var topTrendTreatments = ListBuffer[EachRecordObj]()
           var topRatedDoctors = ListBuffer[EachRecordObj]()
           
           var outstandingDoctorWise = ListBuffer[EachRecordObj]()
           var outstandingTreatmentWise = ListBuffer[EachRecordObj]()
           var outstandingBranchWise = ListBuffer[EachRecordObj]()
           
           var pharmaleadscount = ListBuffer[EachRecordObj]()
           var expensesCost = ListBuffer[EachRecordObj]()
            var feedback = ListBuffer[EachRecordObj]()
           
           
           
           var newPatientCount = 0 
           var oldPatientCount = 0
           var cancelPatientCount = 0
           
           var malePatientCount = 0 
           var femalePatientCount = 0
           
           var firstage = 0
           var secondage = 0
           var thirdage = 0
           var fourthage = 0
           var fifthage = 0
           
           var booked = 0
           var completed = 0
           var cancelled = 0
           var total = 0
           
           var totalSms = 0
           var deliveredSms = 0
           var pendingSms = 0
           var failedSMS = 0
           
          
           
if(dashboardtype.equalsIgnoreCase("PATIENTDTLS"))
      {
             
            val dashboardInfo=  getNewDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch,dashboardtype).first
            
            if(dashboardInfo._11 != null)
            {
              var decodeData = dashboardInfo._11.toString().replaceAll("\"","").toString.split("~")
              if(!decodeData(0).isEmpty)
              {
                   newPatientCount  = decodeData(0).toInt
                   oldPatientCount  = decodeData(1).toInt
                   cancelPatientCount  = decodeData(2).toInt
              }
            }
            
            if(dashboardInfo._12 != null)
            {
              var decodeData = dashboardInfo._12.toString().replaceAll("\"","").toString.split("~")
              if(!decodeData(0).isEmpty)
              {
                   firstage  = decodeData(0).toInt
                   secondage  = decodeData(1).toInt
                   thirdage  = decodeData(2).toInt
                   fourthage  = decodeData(3).toInt
                   fifthage  = decodeData(4).toInt
                   malePatientCount  = decodeData(5).toInt
                   femalePatientCount  = decodeData(6).toInt
                   
              }
            }
            
            if(dashboardInfo._13 != null)
            {
              var decodeData = dashboardInfo._13.toString().replaceAll("\"","").toString.split("~")
              if(!decodeData(0).isEmpty)
              {
                   totalSms  = decodeData(0).toInt
                   deliveredSms  = decodeData(1).toInt
                   pendingSms  = decodeData(2).toInt
                   failedSMS  = decodeData(3).toInt
                   
              }
            }
            
            var agegroubobj = new AgeDtls(Some(firstage),Some(secondage),Some(thirdage),Some(fourthage),Some(fifthage))
            
            var patientVistisobj =  new patientVisitsDtlsObj(Some(newPatientCount),Some(oldPatientCount),Some(cancelPatientCount),
            Some(malePatientCount),Some(femalePatientCount),None,None,Some(agegroubobj))
            
            var smsDetailsObj = new doctorCommStatusObj(deliveredSms.toLong,failedSMS.toLong,pendingSms.toLong,totalSms.toLong)
            
            (new NewDashboardInfoTypeObj(Some(patientVistisobj),Some(smsDetailsObj),None,None,None,None,None,None,None,None,None,None,None,None,None,None,None),None)
            
             
      }
else if(dashboardtype.equalsIgnoreCase("APPOINTMENTDTLS"))
      {
  
          val dashboardInfo=  getNewDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch,dashboardtype).first
          
          (new NewDashboardInfoTypeObj(None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None),None)
      }
else if(dashboardtype.equalsIgnoreCase("TRENDING"))
{
  val dashboardInfo = getNewDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch,dashboardtype).first
    
            if(dashboardInfo._3 != null) {
             
            val topTrendDoctorsrecords = dashboardInfo._3.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendDoctorsrecords(0).isEmpty()){
            for(j <- 0 until topTrendDoctorsrecords.length){
              val topTrendDoctorsrec = topTrendDoctorsrecords(j).split(":")
              topTrendDoctors += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }
            if(dashboardInfo._4 != null) {
             
            val topTrendCosultantsrecords = dashboardInfo._4.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendCosultantsrecords(0).isEmpty()){
            for(j <- 0 until topTrendCosultantsrecords.length){
              val topTrendCosultantsrec = topTrendCosultantsrecords(j).split(":")
              topTrendCosultants += new EachRecordObj(topTrendCosultantsrec(0),topTrendCosultantsrec(1))  
            }
           }
          }
           
           
            
            if(dashboardInfo._5 != null) {
             
            val topTrendBranchesrecords = dashboardInfo._5.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendBranchesrecords(0).isEmpty()){
            for(j <- 0 until topTrendBranchesrecords.length){
              val topTrendBranchesrec = topTrendBranchesrecords(j).split(":")
              topTrendBranches += new EachRecordObj(topTrendBranchesrec(0),topTrendBranchesrec(1))  
            }
           }
          }
            if(dashboardInfo._6 != null) {
             
            val topTrendTreatmentsrecords = dashboardInfo._6.toString.replaceAll("\"","").toString.split("~")
             if(!topTrendTreatmentsrecords(0).isEmpty()){
            for(j <- 0 until topTrendTreatmentsrecords.length){
              val topTrendTreatmentsrec = topTrendTreatmentsrecords(j).split(":")
              topTrendTreatments += new EachRecordObj(topTrendTreatmentsrec(0),topTrendTreatmentsrec(1))  
            }
           }
          }
            
            var avgTime = "0 Min"
            try {
           
             if(dashboardInfo._7 != null && dashboardInfo._9.length() > 0) {
            
             val timeAray = dashboardInfo._12.split(":")
             val hh = timeAray(0)
             val min = timeAray(1)
             
             avgTime = ((timeAray(0).toInt * 60) + min.toInt).toString + " Mins"
              }
           
           } catch {
            case e: Exception =>
              avgTime = "1 Day"
              e.printStackTrace()
          }
           
           
       
             if(dashboardInfo._9 != null) {             
            val topRatedDoctorrecords = dashboardInfo._9.toString.replaceAll("\"","").toString.split("~")
             if(!topRatedDoctorrecords(0).isEmpty()){
            for(j <- 0 until topRatedDoctorrecords.length){
              val topRatedDoctorsrec = topRatedDoctorrecords(j).split(":")
              topRatedDoctors += new EachRecordObj(topRatedDoctorsrec(0),topRatedDoctorsrec(1))  
            }
           }
          }        
          
          
                   
           (new NewDashboardInfoTypeObj(None,None,None,Some(topTrendDoctors.toArray),Some(topTrendCosultants.toArray),Some(topTrendBranches.toArray),
               Some(topTrendTreatments.toArray),Some(avgTime),Some(topRatedDoctors.toArray),None,None,None,None,None,None,None,None),None)
}     
else if(dashboardtype.equalsIgnoreCase("OUTSTANDING"))
{
  val dashboardInfo = getNewDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch,dashboardtype).first
  
  if(dashboardInfo._14 != null) {
             
            val topTreatmentOutstanding = dashboardInfo._14.toString.replaceAll("\"","").toString.split("~")
             if(!topTreatmentOutstanding(0).isEmpty()){
            for(j <- 0 until topTreatmentOutstanding.length){
              val topTrendDoctorsrec = topTreatmentOutstanding(j).split(":")
              outstandingTreatmentWise += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }
  if(dashboardInfo._15 != null) {
             
            val topDoctorOutstanding = dashboardInfo._15.toString.replaceAll("\"","").toString.split("~")
             if(!topDoctorOutstanding(0).isEmpty()){
            for(j <- 0 until topDoctorOutstanding.length){
              val topTrendDoctorsrec = topDoctorOutstanding(j).split(":")
              outstandingDoctorWise += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }
  if(dashboardInfo._16 != null) {
             
            val topBranchOutstanding = dashboardInfo._16.toString.replaceAll("\"","").toString.split("~")
             if(!topBranchOutstanding(0).isEmpty()){
            for(j <- 0 until topBranchOutstanding.length){
              val topTrendDoctorsrec = topBranchOutstanding(j).split(":")
              outstandingBranchWise += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }  
  
  var outstandingObjDtls   = new  outstandingObjDtls(Some(outstandingDoctorWise.toArray),Some(outstandingTreatmentWise.toArray),Some(outstandingBranchWise.toArray))      
  
  (new NewDashboardInfoTypeObj(None,None,None,None,None,None, None,None,None,None,Some(outstandingObjDtls),None,None,None,None,None,None),None)
             
}
else if(dashboardtype.equalsIgnoreCase("PHARMALEADS"))
{
  
  val dashboardInfo = getNewDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch,dashboardtype).first
  
  if(dashboardInfo._17 != null) {
             
            val pharmaleadscountobj = dashboardInfo._17.toString.replaceAll("\"","").toString.split("~")
             if(!pharmaleadscountobj(0).isEmpty()){
            for(j <- 0 until pharmaleadscountobj.length){
              val topTrendDoctorsrec = pharmaleadscountobj(j).split(":")
              pharmaleadscount += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }
  
   
  (new NewDashboardInfoTypeObj(None,None,None,None,None,None, None,None,None,None,None,Some(pharmaleadscount.toArray),None,None,None,None,None),None)
}
else if(dashboardtype.equalsIgnoreCase("EXPENSES"))
{
  
  val dashboardInfo = getNewDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch,dashboardtype).first
  
  if(dashboardInfo._18 != null) {
             
            val expenseCostobj = dashboardInfo._18.toString.replaceAll("\"","").toString.split("~")
             if(!expenseCostobj(0).isEmpty()){
            for(j <- 0 until expenseCostobj.length){
              val topTrendDoctorsrec = expenseCostobj(j).split(":")
              expensesCost += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }
  
   
  (new NewDashboardInfoTypeObj(None,None,None,None,None,None, None,None,None,None,None,None,Some(expensesCost.toArray),None,None,None,None),None)
}
else if(dashboardtype.equalsIgnoreCase("INVENTORY"))
{
   
  (new NewDashboardInfoTypeObj(None,None,None,None,None,None, None,None,None,None,None,None,None,None,None,None,None),Some(Json.parse(getDoctorMedQuery(doctorid,hospitalId,inventoryType).firstOption.get)))
}
else if(dashboardtype.equalsIgnoreCase("TARGET"))
{

      var targetAmount: Long = 0
      var totalRevenue = 0.0
      var targetType: Option[String] = None
      var tempUpcomingDailyTarget: Double = 0
      
      val now: Calendar = CalenderDateConvertor.getISDTimeZoneDate() 

      val todayDate: Int = now.get(Calendar.DAY_OF_MONTH);

      val todayMonth: Int = now.get(Calendar.MONTH) + 1;

      val todayYear: Int = now.get(Calendar.YEAR);

      var remainingDays = 30 - todayDate

  var targetSet = hospital.filter { x => x.HospitalID === hospitalId}.map{x => x.Preference.getOrElse(Map.empty[String,String])}.firstOption
  
  var targetExists = targetSet.get.contains("RevenueTarget")

  if(targetExists)
  {   
   
   val format = new SimpleDateFormat("yyyy-MM-dd") 
   
   var tempRevenueStartDate = format.parse(todayYear+"-"+todayMonth+"-"+"01")     
   var RevenueStartDate = new java.sql.Date(tempRevenueStartDate.getTime()); 
   
   var tempRevenueEndDate = format.parse(todayYear+"-"+todayMonth+"-"+"31")    
   var RevenueEndDate = new java.sql.Date(tempRevenueEndDate.getTime()); 
   
    totalRevenue = Math.round(gettargetbasedRevenue(hospitalId, RevenueStartDate, RevenueEndDate).first)
    
    targetAmount = targetSet.get("RevenueTarget").toLong   
    
    targetType = Some(targetSet.get("TargetType").toString)
    
    var tempRevenuePerDay = totalRevenue / todayDate
    
    var projectedRevenue = Math.round(totalRevenue + (tempRevenuePerDay * remainingDays))
    
    var ActualTargetToBeAchievedByToday : Double= (targetAmount /30) * todayDate
    
    var targetAchievedPercenttillDate = Math.round((totalRevenue / targetAmount) * 100)
     
    if(targetAmount > totalRevenue)
    {
       tempUpcomingDailyTarget = Math.round(targetAmount.toDouble - totalRevenue)
    }
   
   
   var UpcomingDailyTargettoBeAchieved = Math.round(tempUpcomingDailyTarget /remainingDays)
     
     var revenueTargetObj = new revenueTargetObj(hospitalId,Some(totalRevenue),Some(projectedRevenue),Some(ActualTargetToBeAchievedByToday),Some(targetAmount),Some(targetAchievedPercenttillDate),Some(UpcomingDailyTargettoBeAchieved),targetType)
     
     (new NewDashboardInfoTypeObj(None,None,None,None,None,None, None,None,None,None,None,None,None,Some(revenueTargetObj),None,None,None),None)
    
  }
 else
 {
   
   val format = new SimpleDateFormat("yyyy-MM-dd") 
   
   var tempRevenueStartDate = format.parse(todayYear+"-"+todayMonth+"-"+"01")     
   var RevenueStartDate = new java.sql.Date(tempRevenueStartDate.getTime()); 
   
   var tempRevenueEndDate = format.parse(todayYear+"-"+todayMonth+"-"+"31")    
   var RevenueEndDate = new java.sql.Date(tempRevenueEndDate.getTime()); 
   
   totalRevenue = Math.round(gettargetbasedRevenue(hospitalId, RevenueStartDate, RevenueEndDate).first)
   
   var tempRevenuePerDay = totalRevenue / todayDate
    
   var projectedRevenue = Math.round(totalRevenue + (tempRevenuePerDay * remainingDays)) 
   
   var revenueTargetObj = new revenueTargetObj(hospitalId,Some(totalRevenue),Some(projectedRevenue),Some(0),Some(0),Some(0),Some(0),None)
   
     (new NewDashboardInfoTypeObj(None,None,None,None,None,None, None,None,None,None,None,None,None,Some(revenueTargetObj),None,None,None),None)
 }
  
}
           
else if(dashboardtype.equalsIgnoreCase("CAMPAIGN"))
{
    
       
  
  
  (new NewDashboardInfoTypeObj(None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None),None)

}
                     
else if(dashboardtype.equalsIgnoreCase("REVENUERECIEVED"))
{
  val dashboardInfo = getNewDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch,dashboardtype).first
  
  if(dashboardInfo._19 != null) {
             
            val topTreatmentOutstanding = dashboardInfo._19.toString.replaceAll("\"","").toString.split("~")
             if(!topTreatmentOutstanding(0).isEmpty()){
            for(j <- 0 until topTreatmentOutstanding.length){
              val topTrendDoctorsrec = topTreatmentOutstanding(j).split(":")
              outstandingTreatmentWise += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }
  if(dashboardInfo._20 != null) {
             
            val topDoctorOutstanding = dashboardInfo._20.toString.replaceAll("\"","").toString.split("~")
             if(!topDoctorOutstanding(0).isEmpty()){
            for(j <- 0 until topDoctorOutstanding.length){
              val topTrendDoctorsrec = topDoctorOutstanding(j).split(":")
              outstandingDoctorWise += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }
  if(dashboardInfo._21 != null) {
             
            val topBranchOutstanding = dashboardInfo._21.toString.replaceAll("\"","").toString.split("~")
             if(!topBranchOutstanding(0).isEmpty()){
            for(j <- 0 until topBranchOutstanding.length){
              val topTrendDoctorsrec = topBranchOutstanding(j).split(":")
              outstandingBranchWise += (new EachRecordObj(topTrendDoctorsrec(0),topTrendDoctorsrec(1)))  
            }
           }
          }  
  
  var revenueRecievedDetails   = new  RevenueObjDtls(Some(outstandingDoctorWise.toArray),Some(outstandingTreatmentWise.toArray),Some(outstandingBranchWise.toArray))      
  
  (new NewDashboardInfoTypeObj(None,None,None,None,None,None, None,None,None,None,None,None,None,None,Some(revenueRecievedDetails),None,None),None)
             
}
else if(dashboardtype.equalsIgnoreCase("TREATMENTCNT"))
{
  var totTreatmentCount = getTotalTreatmentCount(startDate, endDate,hospitalId, doctorid, AddressID,includeBranch).first
  
  (new NewDashboardInfoTypeObj(None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,Some(totTreatmentCount),None),None)
  
  
}
else if(dashboardtype.equalsIgnoreCase("FEEDBACK"))
{
    val dashboardInfo = getNewDashboardInfoQuery(hospitalId, doctorid, AddressID, startDate, endDate, includeBranch,dashboardtype).first

     if(dashboardInfo._22 != null) {
             
            val feedbackreport = dashboardInfo._22.toString.replaceAll("\"","").toString.split("~")
             if(!feedbackreport(0).isEmpty()){
            for(j <- 0 until feedbackreport.length){
              val feedbackreprtDtls = feedbackreport(j).split(":")
              feedback += (new EachRecordObj(feedbackreprtDtls(0),feedbackreprtDtls(1)))  
            }
           }
          }

    (new NewDashboardInfoTypeObj(None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,Some(feedback.toArray)),None)
}
else
{
   (new NewDashboardInfoTypeObj(None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None),None)
}
                  
}
  
  
  
  def getEnquiryNewDashBoardInfo(a: EnquiryDashboardInfoInputTypeObj): ListEnquiryDashboardInfo = db withSession { implicit session =>
  

        val enquirydashboradlist = getdashborddetailsval(a.fromdate,a.todate,a.Hospitalcode).list
        
       val resultSet: ListEnquiryDashboardInfo= new ListEnquiryDashboardInfo(enquirydashboradlist(0)._1,enquirydashboradlist(0)._2,enquirydashboradlist(0)._3,enquirydashboradlist(0)._4,enquirydashboradlist(0)._5,
           enquirydashboradlist(0)._6,enquirydashboradlist(0)._7,enquirydashboradlist(0)._8,enquirydashboradlist(0)._9,enquirydashboradlist(0)._10,enquirydashboradlist(0)._11,
           enquirydashboradlist(0)._12,enquirydashboradlist(0)._13,enquirydashboradlist(0)._14,enquirydashboradlist(0)._15,enquirydashboradlist(0)._16)

    return resultSet
     
  }

    def getDailyReportInfo(a: getDailyReportInfoObj): Array[getDailyReportListInfo] = db withSession { implicit session =>
  
    

        val dailyReportList = getdailyreportinfo(a.date,a.hospitalID,a.Hospitalcode).list
        
        val result: Array[getDailyReportListInfo] = new Array[getDailyReportListInfo](dailyReportList.length)
        
      for (i <- 0 until dailyReportList.length) {
         
        result(i) = new getDailyReportListInfo(dailyReportList(i)._1,Some(dailyReportList(i)._2),Some(dailyReportList(i)._3),Some(dailyReportList(i)._4),
                           Some(dailyReportList(i)._5),Some(dailyReportList(i)._6),Some(dailyReportList(i)._7),Some(dailyReportList(i)._8))
        
      }

    return result
     
  }
    
        def getDailySpecialityReportInfo(a: getDailySpecialityReportInfoObj): Array[listDailySpecialityReportListInfo] = db withSession { implicit session =>
  

        val dailySpecialityReportList = getdailyspecialityreportinfo(a.date,a.hospitalID,a.Hospitalcode).list
        
        val result: Array[listDailySpecialityReportListInfo] = new Array[listDailySpecialityReportListInfo](dailySpecialityReportList.length)
        
      for (i <- 0 until dailySpecialityReportList.length) {
         
        result(i) = new listDailySpecialityReportListInfo(dailySpecialityReportList(i)._1,dailySpecialityReportList(i)._2,dailySpecialityReportList(i)._3,dailySpecialityReportList(i)._4,Some(dailySpecialityReportList(i)._5),Some(dailySpecialityReportList(i)._6),Some(dailySpecialityReportList(i)._7))
        
      }

    return result
     
  }
        
       def getCostReportInfo(a: getCostReportInfoObj): Array[listCostReportListInfo] = db withSession { implicit session =>
  

        val costReportList = getcostreportinfo(a.fromdate,a.todate,a.Regionid).list
        
        val result: Array[listCostReportListInfo] = new Array[listCostReportListInfo](costReportList.length)
        
      for (i <- 0 until costReportList.length) {
         
        result(i) = new listCostReportListInfo(Some(costReportList(i)._1),Some(costReportList(i)._2),Some(costReportList(i)._3),Some(costReportList(i)._4),Some(costReportList(i)._5),Some(costReportList(i)._6)
                                               ,Some(costReportList(i)._7),Some(costReportList(i)._8),Some(costReportList(i)._9),Some(costReportList(i)._10),Some(costReportList(i)._11),Some(costReportList(i)._12)
                                               ,Some(costReportList(i)._13),Some(costReportList(i)._14),Some(costReportList(i)._15))
        
      }

    return result
     
  }
       
       
       
              def getForecastCostReportInfo(a: getCostReportInfoObj): Array[listForecastReportListInfo] = db withSession { implicit session =>
  

        val costReportList = getforecastreportinfo(a.fromdate,a.todate,a.Regionid).list
        
        val result: Array[listForecastReportListInfo] = new Array[listForecastReportListInfo](costReportList.length)
        
      for (i <- 0 until costReportList.length) {
         
        result(i) = new listForecastReportListInfo(Some(costReportList(i)._1),Some(costReportList(i)._2),Some(costReportList(i)._3),Some(costReportList(i)._4),Some(costReportList(i)._5),Some(costReportList(i)._6)
                                               ,Some(costReportList(i)._7),Some(costReportList(i)._8),Some(costReportList(i)._9),Some(costReportList(i)._10),Some(costReportList(i)._11),Some(costReportList(i)._12)
                                               ,Some(costReportList(i)._13),Some(costReportList(i)._14),Some(costReportList(i)._15),Some(costReportList(i)._16),Some(costReportList(i)._17))
        
      }
        
        
              // Blank workbook 
      /* val workbook: XSSFWorkbook  = new XSSFWorkbook();
        // Create a blank sheet 
       val sheet: XSSFSheet  = workbook.createSheet("student Details"); 
       var row: Row  = sheet.createRow(1);
       var cell: Cell = row.createCell(1);
       cell.setCellValue("Name");
       var out: FileOutputStream = new FileOutputStream(new File("D:/Sabari/testexcel.xlsx")); 
       workbook.write(out); 
       out.close(); */
  
        

    return result
     
  }
       
   def getCostReportwithregion(a: getCostReportwithregionInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getCostReportwithregion(a.fromdate,a.todate,a.Hospitalcode).firstOption.get)
     
  }
   
   def getReferral(a: getReferralwithoutIndicationObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getReferralwithoutindication(a.fromdate,a.todate,a.HospitalID,a.HospitalCode).firstOption.get)
     
  }
   
    def getGeneralreport(a: getReferralwithoutIndicationObj): JsValue = db withSession { implicit session =>
  
    val reportdata=Json.parse(getGeneralreportsmethod(a.fromdate,a.todate,a.HospitalID,a.HospitalCode).firstOption.get)
         
    return reportdata
     
  }
  
  def excelfilegeneratewithformula(a: getReferralwithoutIndicationObj) : String = db withSession { implicit session =>
     val workbook: XSSFWorkbook = new XSSFWorkbook();
     val sheet: XSSFSheet = workbook.createSheet("Sheet1");
     var style:CellStyle = workbook.createCellStyle();
     var normalstyle:CellStyle = workbook.createCellStyle();
     style.setBorderBottom(BorderStyle.MEDIUM);
		 style.setBorderLeft(BorderStyle.MEDIUM);
		 style.setBorderRight(BorderStyle.MEDIUM);
		 style.setBorderTop(BorderStyle.MEDIUM);
		 normalstyle.setBorderBottom(BorderStyle.MEDIUM);
		 normalstyle.setBorderLeft(BorderStyle.MEDIUM);
		 normalstyle.setBorderRight(BorderStyle.MEDIUM);
		 normalstyle.setBorderTop(BorderStyle.MEDIUM);
     var font: Font= workbook.createFont();//Create font
     var whitefont: Font= workbook.createFont();//Create white font
     
     font.setBold(true);//Make font bold
     style.setFont(font);//set it to bold
     
     whitefont.setColor(WHITE.index)
     
     var row0: Row = sheet.createRow(0);
     var ycell: Cell = row0.createCell(14);
     var ncell: Cell = row0.createCell(15);
     
     ycell.setCellValue("Y");
     ncell.setCellValue("N");
     
     var row1: Row = sheet.createRow(1);
     var row2: Row = sheet.createRow(2);
     var officeuselablecell: Cell = row1.createCell(15);
     officeuselablecell.setCellValue("OFFICE USE ONLY");
     
     var redcolorStyle:CellStyle = workbook.createCellStyle();
     redcolorStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
     redcolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     
     var yellowcolorStyle:CellStyle = workbook.createCellStyle();
     yellowcolorStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
     yellowcolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     
     var lightbluecolorStyle:CellStyle = workbook.createCellStyle();
     
     lightbluecolorStyle.setBorderBottom(BorderStyle.MEDIUM);
		 lightbluecolorStyle.setBorderLeft(BorderStyle.MEDIUM);
		 lightbluecolorStyle.setBorderRight(BorderStyle.MEDIUM);
		 lightbluecolorStyle.setBorderTop(BorderStyle.MEDIUM);
		 
     //bluecolorStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
     lightbluecolorStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
     lightbluecolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     lightbluecolorStyle.setFont(whitefont)
     
     var bluecolorStyle:CellStyle = workbook.createCellStyle();
     
     bluecolorStyle.setBorderBottom(BorderStyle.MEDIUM);
		 bluecolorStyle.setBorderLeft(BorderStyle.MEDIUM);
		 bluecolorStyle.setBorderRight(BorderStyle.MEDIUM);
		 bluecolorStyle.setBorderTop(BorderStyle.MEDIUM);
		 
     //bluecolorStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
     bluecolorStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
     bluecolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     bluecolorStyle.setFont(whitefont)
     
     var OrangecolorStyle:CellStyle = workbook.createCellStyle();
     //bluecolorStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
     
     OrangecolorStyle.setBorderBottom(BorderStyle.MEDIUM);
		 OrangecolorStyle.setBorderLeft(BorderStyle.MEDIUM);
		 OrangecolorStyle.setBorderRight(BorderStyle.MEDIUM);
		 OrangecolorStyle.setBorderTop(BorderStyle.MEDIUM);
			
     /*OrangecolorStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
     OrangecolorStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
     OrangecolorStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
     OrangecolorStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex()); */
     OrangecolorStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
     OrangecolorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
     

     
     
     var counter = 1
     
     sheet.addMergedRegion(new CellRangeAddress(3,3,6,11));
     
     
     var row4: Row = sheet.createRow(3);
     var descriptioncell1: Cell = row4.createCell(6);
     //descriptioncell1.setCellStyle(normalstyle);

     descriptioncell1.setCellValue("Below to be completed by CSW / Clinician at BEGINNING and END of clinic ");
     
     var row5: Row = sheet.createRow(4);
     var datelablecell: Cell = row5.createCell(10);
     var datevaluecell: Cell = row5.createCell(11);
     
     datelablecell.setCellValue("Session Date:  ");
     datevaluecell.setCellValue("16-01-2019");
     
     datelablecell.setCellStyle(style);
     datevaluecell.setCellStyle(style);
     
     var row6: Row = sheet.createRow(5);
     var sonographerlablecell: Cell = row6.createCell(0);
     var sonographervaluecell: Cell = row6.createCell(1);
     
     sonographerlablecell.setCellValue("Sonographer / Radiologist Name: ");
     sonographervaluecell.setCellValue("Shahzad Akram");
     
     sonographerlablecell.setCellStyle(style);
     sonographervaluecell.setCellStyle(style);
     
     var starttimelablecell: Cell = row6.createCell(10);
     var starttimevaluecell: Cell = row6.createCell(11);
     
     starttimelablecell.setCellValue("Session Start Time:");
     starttimevaluecell.setCellValue("00:05:00");
     
     starttimelablecell.setCellStyle(style);
     starttimevaluecell.setCellStyle(style);     
     
     var clinicdurcell: Cell = row6.createCell(15);
     clinicdurcell.setCellValue("Clinic Duration");
     
     var row7: Row = sheet.createRow(6);
     
     var cswnamelablecell: Cell = row7.createCell(0);
     var cswnamevaluecell: Cell = row7.createCell(1);
     
     cswnamelablecell.setCellValue("Clinical Support Staff Name:");
     cswnamevaluecell.setCellValue("Diane Low");
     
     cswnamelablecell.setCellStyle(style);
     cswnamevaluecell.setCellStyle(style);
     
     var endtimelablecell: Cell = row7.createCell(10);
     var endtimevaluecell: Cell = row7.createCell(11);
     
     endtimelablecell.setCellValue("Session End Time:");
     endtimevaluecell.setCellValue("23:55:00");
     
     endtimelablecell.setCellStyle(style);
     endtimevaluecell.setCellStyle(style);
     
     var durationcell: Cell = row7.createCell(14);
     durationcell.setCellValue("Duration");
     var numericalmincell: Cell = row7.createCell(15);
     numericalmincell.setCellValue("Numerical Mins");
     var numericalhrscell: Cell = row7.createCell(16);
     numericalhrscell.setCellValue("Numerical Hours");
     
     var row8: Row = sheet.createRow(7);
     
     var locationlablecell: Cell = row8.createCell(0);
     var locationvaluecell: Cell = row8.createCell(1);
     
     locationlablecell.setCellValue("Clinic Location / Practice Name:");
     locationvaluecell.setCellValue("Alrewas Surgery");
     
     locationlablecell.setCellStyle(style);
     locationvaluecell.setCellStyle(style);
     
     var capacitylablecell: Cell = row8.createCell(10);
     var capacityvaluecell: Cell = row8.createCell(11);
     
     capacitylablecell.setCellValue("Patient Capacity:");
     capacityvaluecell.setCellValue(5);
     
     capacitylablecell.setCellStyle(style);
     capacityvaluecell.setCellStyle(style);
     
     var durationvalcell: Cell = row8.createCell(14);
     durationvalcell.setCellFormula("IF(L7-L6<0,\"Error\",IF(AND(L6<>\"\",L7<>\"\"),L7-L6,\"\"))")
     var numericalminvalcell: Cell = row8.createCell(15);
     numericalminvalcell.setCellFormula("IF(L6<0,\"\",ROUND((O8*1440),0))")
     var numericalhrsvalcell: Cell = row8.createCell(16);
     numericalhrsvalcell.setCellFormula("IFERROR(P8/60,\"\")");
     
      
     var row9: Row = sheet.createRow(8);
     var row10: Row = sheet.createRow(9);
     
     var descriptioncell2: Cell = row9.createCell(0);
     var descriptioncell3: Cell = row10.createCell(0);
     sheet.addMergedRegion(new CellRangeAddress(8,8,0,11));
     //sheet.addMergedRegion(new CellRangeAddress(9,9,4,11));
     sheet.addMergedRegion(new CellRangeAddress(9,9,0,11));
     
     
     descriptioncell2.setCellValue("All Cells MUST be completed - Records MUST be kept DURING clinic - Worksheets MUST be submitted at the End of Clinic - System Sync MUST be done at the End of Clinic -");
     descriptioncell3.setCellValue("***  THESE ARE MANDATORY ACTIONS ***");
     //redcolorStyle.setAlignment(CellStyle.ALIGN_CENTER);
     redcolorStyle.setAlignment(HorizontalAlignment.CENTER);
     descriptioncell2.setCellStyle(redcolorStyle);
     descriptioncell3.setCellStyle(redcolorStyle);
     
     var row11: Row = sheet.createRow(10);
     sheet.addMergedRegion(new CellRangeAddress(10,10,4,7));
     var descriptioncell4: Cell = row11.createCell(4);
     descriptioncell4.setCellValue("DHSL.worksheets@nhs.net");
     yellowcolorStyle.setAlignment(HorizontalAlignment.CENTER);
     descriptioncell4.setCellStyle(yellowcolorStyle)
     
     
     var row12: Row = sheet.createRow(11);
     sheet.addMergedRegion(new CellRangeAddress(11,11,0,3));
     var descriptioncell5: Cell = row12.createCell(0);
     //descriptioncell5.setCellStyle(normalstyle);
     //descriptioncell5.setCellValue("Below to be completed by Head Office - Copy and Paste Value from AMP worksheet download");
     
     sheet.addMergedRegion(new CellRangeAddress(11,11,5,11));
     var descriptioncell6: Cell = row12.createCell(5);
     descriptioncell6.setCellValue("Below to be completed by CSW / Clinician DURING Clinic ");
     //descriptioncell6.setCellStyle(normalstyle);
     
     var headerlist = List("Appt Time","DW Reference","Patient Initials","Examination Type","Key","TV","DVT","No of Areas","Patient Collected","Ready for next patient","Survey Completed","Notes") 
     var headerrow: Row = sheet.createRow(12);
     for (m1 <- 0 to headerlist.length - 1) {
     var headercell: Cell = headerrow.createCell(m1);
     headercell.setCellValue(headerlist(m1));
     //headercell.setCellStyle(style);
     headercell.setCellStyle(lightbluecolorStyle);
      }
     
     var scandurationcell: Cell = headerrow.createCell(15);
     scandurationcell.setCellValue("Scan Durations");
     
     
     var apptimelist = List("10:45:00","11:05:00","11:25:00","11:45:00");
     for (n1 <- 0 to 3) {
     var rowint = 13 +n1;  
     var detailrow: Row = sheet.createRow(rowint);
     var apptimecell: Cell = detailrow.createCell(0);
     var dwreferencecell: Cell = detailrow.createCell(1);
     var patientintcell: Cell = detailrow.createCell(2);
     var examtypecell: Cell = detailrow.createCell(3);
     apptimecell.setCellValue(apptimelist(n1));
     patientintcell.setCellValue("TS");
     apptimecell.setCellStyle(normalstyle);
     patientintcell.setCellStyle(normalstyle);
     dwreferencecell.setCellStyle(normalstyle);
     examtypecell.setCellStyle(normalstyle);
     
     //drop down list assign in coulm header key
     var keycell: Cell = detailrow.createCell(4);
     keycell.setCellStyle(OrangecolorStyle);
     var keyList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 4, 4);
     var keyval :Array[String] = Array("V", "E", "D" )
     var validationHelper : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var constraint : DataValidationConstraint  = validationHelper.createExplicitListConstraint(keyval);
     var dataValidation : DataValidation  = validationHelper.createValidation(constraint, keyList);
     dataValidation.setSuppressDropDownArrow(true); 
     dataValidation.createErrorBox("Bad Value", "Please select value from drop down list!");
     dataValidation.setShowErrorBox(true);
     sheet.addValidationData(dataValidation); 
     
     var TVcell: Cell = detailrow.createCell(5);
     TVcell.setCellStyle(OrangecolorStyle);
     var TVList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 5, 5);
     var TVval :Array[String] = Array(" ", "Y")
     var validationHelper1 : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var constraint1 : DataValidationConstraint  = validationHelper1.createExplicitListConstraint(TVval);
     var dataValidation1 : DataValidation  = validationHelper1.createValidation(constraint1, TVList);
     dataValidation1.setSuppressDropDownArrow(true); 
     dataValidation1.createErrorBox("Bad Value", "Please select value from drop down list!");
     dataValidation1.setShowErrorBox(true);
     sheet.addValidationData(dataValidation1); 
     
     var DVTcell: Cell = detailrow.createCell(6);
     DVTcell.setCellStyle(OrangecolorStyle);
     var DVTList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 6, 6);
     var DVTval :Array[String] = Array(" ", "Y")
     var validationHelper2 : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var constraint2 : DataValidationConstraint  = validationHelper2.createExplicitListConstraint(DVTval);
     var dataValidation2 : DataValidation  = validationHelper2.createValidation(constraint2, DVTList);
     dataValidation2.setSuppressDropDownArrow(true); 
     dataValidation2.createErrorBox("Bad Value", "Please select value from drop down list!");
     dataValidation2.setShowErrorBox(true);
     sheet.addValidationData(dataValidation2); 
     
     var areacell: Cell = detailrow.createCell(7);
     areacell.setCellStyle(OrangecolorStyle);
     
     var collectedcell: Cell = detailrow.createCell(8);
     collectedcell.setCellStyle(OrangecolorStyle);
     
     var nextpatientcell: Cell = detailrow.createCell(9);
     nextpatientcell.setCellStyle(OrangecolorStyle);
     
     var servaycell: Cell = detailrow.createCell(10);
     servaycell.setCellStyle(OrangecolorStyle);
     var servayList : CellRangeAddressList  = new CellRangeAddressList(rowint, rowint, 10, 10);
     var servayval :Array[String] = Array("Y", "N")
     var validationHelper3 : DataValidationHelper  = new XSSFDataValidationHelper(sheet);
     var constraint3 : DataValidationConstraint  = validationHelper3.createExplicitListConstraint(servayval);
     var dataValidation3 : DataValidation  = validationHelper3.createValidation(constraint3, servayList);
     dataValidation3.setSuppressDropDownArrow(true); 
     dataValidation3.createErrorBox("Bad Value", "Please select value from drop down list!");
     dataValidation3.setShowErrorBox(true);
     sheet.addValidationData(dataValidation3); 
     
     var notescell: Cell = detailrow.createCell(11);
     notescell.setCellStyle(normalstyle);
     
     
     //scan duration cell with formulas
   
     
     var scancell1: Cell = detailrow.createCell(14);
     scancell1.setCellFormula("IF(E14=\"D\",\"DNA\",IF(J14-I14<0,\"Error\",IF(AND(I14<>\"\",J14<>\"\"),J14-I14,\"\")))")
     var scancell2: Cell = detailrow.createCell(15);
     scancell2.setCellFormula("IFERROR(ROUND((O14*1440),0),\"\")");
     var scancell3: Cell = detailrow.createCell(16);
     scancell3.setCellFormula("IFERROR(P14/60,0)");
     
     

      }
     
     
     
     sheet.addMergedRegion(new CellRangeAddress(2,2,22,26));
     var descriptioncell7: Cell = row2.createCell(22);
     descriptioncell7.setCellValue("Summary");
     

     var summarylist = List("Clinic Duration","Budgeted Cap","Actual Capacity","Patients Booked","DNA","Scan Rate 1","Scan Rate 2","Scan Rate 3","BP Scanned") 
     for (l1 <- 18 to 26) {
     var summarycell: Cell = row4.createCell(l1);
     summarycell.setCellValue(summarylist(l1-18));
     summarycell.setCellStyle(bluecolorStyle);
      }
     
     var clinicdurationcell: Cell = row5.createCell(18);
     clinicdurationcell.setCellFormula("Q8");
     clinicdurationcell.setCellStyle(normalstyle);
     
     var budgetedcapcell: Cell = row5.createCell(19);
     budgetedcapcell.setCellFormula("L8");
     budgetedcapcell.setCellStyle(normalstyle);
     
     var actualcapcell: Cell = row5.createCell(20);
     actualcapcell.setCellFormula("L8");
     actualcapcell.setCellStyle(normalstyle);
     
     
     var patientbookedcell: Cell = row5.createCell(21);
     patientbookedcell.setCellFormula("COUNTA(C14:C17)");
     patientbookedcell.setCellStyle(normalstyle);
     var dnacell: Cell = row5.createCell(22);
     dnacell.setCellFormula("COUNTIF(E14:E17,\"D\")");
     dnacell.setCellStyle(normalstyle);
     var scanrate1cell: Cell = row5.createCell(23);
     scanrate1cell.setCellFormula("COUNTIFS(F14:F17,\"\",G14:G17,\"\",P14:P17,\"<20\")");
     scanrate1cell.setCellStyle(normalstyle);
     var scanrate2cell: Cell = row5.createCell(24);
     scanrate2cell.setCellFormula("COUNTIFS(F14:F17,\"\",G14:G17,\"\",P14:P17,\">19\")");
     scanrate2cell.setCellStyle(normalstyle);
     var scanrate3cell: Cell = row5.createCell(25);
     scanrate3cell.setCellFormula("COUNTIF(G14:G17,\"Y\")+COUNTIF(F14:F17,\"Y\")");
     scanrate3cell.setCellStyle(normalstyle);
     var bpscanedcell: Cell = row5.createCell(26);
     bpscanedcell.setCellFormula("SUM(H14:H17)"); 
     bpscanedcell.setCellStyle(normalstyle);
     
     
     var summarylist2 = List("Key","Description ","Count") 
     for (l1 <- 18 to 20) {
     var summarycell2: Cell = row7.createCell(l1);
     summarycell2.setCellValue(summarylist2(l1-18));
     summarycell2.setCellStyle(bluecolorStyle);
      }
     
     var keylist = List("V","E","D")
     var descriptionlist = List("Patient Seen / verified","Patient Escalations (Critical / High Priority)","Patient Did Not Attend Appointment")
     
     var keycell1: Cell = row8.createCell(18);
     keycell1.setCellValue(keylist(0));
     keycell1.setCellStyle(normalstyle);
     var keycell2: Cell = row9.createCell(18);
     keycell2.setCellValue(keylist(1));
     keycell2.setCellStyle(normalstyle);
     var keycell3: Cell = row10.createCell(18);
     keycell3.setCellValue(keylist(2));
     keycell3.setCellStyle(normalstyle);
     
     
     var descell1: Cell = row8.createCell(19);
     descell1.setCellValue(descriptionlist(0));
     descell1.setCellStyle(normalstyle);
     var descell2: Cell = row9.createCell(19);
     descell2.setCellValue(descriptionlist(1));
     descell2.setCellStyle(normalstyle);
     var descell3: Cell = row10.createCell(19);
     descell3.setCellValue(descriptionlist(2));
     descell3.setCellStyle(normalstyle);
     
     var countcell1: Cell = row8.createCell(20);
     countcell1.setCellFormula("COUNTIF($E$14:$E$17,$S8)");
     countcell1.setCellStyle(normalstyle);
     var countcell2: Cell = row9.createCell(20);
     countcell2.setCellFormula("COUNTIF($E$14:$E$17,$S9)");
     countcell2.setCellStyle(normalstyle);
     var countcell3: Cell = row10.createCell(20);
     countcell3.setCellFormula("COUNTIF($E$14:$E$17,$S10)");
     countcell3.setCellStyle(normalstyle);
     
     
     var inputStream : InputStream = new FileInputStream("C:/Users/Cardinality/Desktop/c7healthlogo/dw-logo-high.JPG");
     
     var bytes : Array[Byte] = IOUtils.toByteArray(inputStream);
     var pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
     inputStream.close();
     
     var helper : CreationHelper = workbook.getCreationHelper();
     var drawing : Drawing  = sheet.createDrawingPatriarch();
     
     var anchor : ClientAnchor  = helper.createClientAnchor();
     
     anchor.setCol1(0); //Column B
     anchor.setRow1(0); //Row 3
     anchor.setCol2(2); //Column C
     anchor.setRow2(5); //Row 4
   
     var pict : Picture  = drawing.createPicture(anchor, pictureIdx);
     pict.resize(2, 1)
     
    
    val tempfolder1 = current.configuration.getString("temp_folder").get
    val fileName = "formulatestfile.xlsx".toString()
    println("files to be printed" + current.configuration.getString("temp_folder").get + "/" + fileName)
    
    var fos: FileOutputStream = null
    var outputFile: File = null
    var path: String = null
    outputFile = new File(current.configuration.getString("temp_folder").get + "/" + fileName);
    fos = new FileOutputStream(outputFile);

    //  var out: FileOutputStream = new FileOutputStream(new File(current.configuration.getString("temp_folder").get + "/" + fileName));

    workbook.write(fos);
    fos.close()
       
       
  return "request added succesfully"
  }

  def getGeneralreportfile(a: getReferralwithoutIndicationObj): String = db withSession { implicit session =>

    val reportdata = Json.parse(getGeneralreportsmethod(a.fromdate, a.todate, a.HospitalID, a.HospitalCode).firstOption.get)

    // Blank workbook
    val workbook: XSSFWorkbook = new XSSFWorkbook();
    // Create a blank sheet
    val sheet: XSSFSheet = workbook.createSheet("Stat Details");
    val jsonArray = new JSONArray(reportdata.toString())

    if (jsonArray.length() > 0) {

      var count = 0;
      var headerjson: JSONObject = jsonArray.getJSONObject(0);
      val iter = headerjson.keys()
      var row: Row = sheet.createRow(0);
      while (iter.hasNext()) {
        val key = iter.next().toString()
        val value = headerjson.get(key).toString();
        //println(key + "    "+value);

        var cell: Cell = row.createCell(count);
        cell.setCellValue(key);
        //rowhead.createCell((short) count).setCellValue("value");
        //rowhead.createCell((short) 1);
        count = count + 1
      }

    }
    var counter = 1
    for (m1 <- 0 to jsonArray.length() - 1) {
      val jsonobject = jsonArray.getJSONObject(m1)
      val iter = jsonobject.keys()

      var count = 0
      var row: Row = sheet.createRow(counter);
      while (iter.hasNext()) {
        val key = iter.next().toString()
        val value = jsonobject.get(key).toString();

        var cell: Cell = row.createCell(count);
        cell.setCellValue(value);

        count = count + 1;
      }
      counter = counter + 1
    }

    val tempfolder1 = current.configuration.getString("temp_folder").get
    val uuid = java.util.UUID.randomUUID.toString
    val fileName = uuid + "_report.xlsx".toString()
    println("files to be printed" + current.configuration.getString("temp_folder").get + "/" + fileName)

    var fos: FileOutputStream = null
    var outputFile: File = null
    var path: String = null
    outputFile = new File(current.configuration.getString("temp_folder").get + "/" + fileName);
    fos = new FileOutputStream(outputFile);

    //  var out: FileOutputStream = new FileOutputStream(new File(current.configuration.getString("temp_folder").get + "/" + fileName));

    workbook.write(fos);
    fos.close()
    var filepath = outputFile.getAbsolutePath
    val s3FilePath = fileName
    val generalReportBucket = current.configuration.getString("general.report.bucket").get

    val result = hcueDocuments.S3UploadHelperService(generalReportBucket, filepath, s3FilePath)

    val localexcel = tempfolder1 + "/" + fileName
    val localExcelFile = new File(localexcel)
    if (localExcelFile.exists()) {
      localExcelFile.delete()
      println(localexcel + " - file deleted")
    }

    generalreport.map { x => (x.reportname, x.startdate, x.enddate, x.CrtUSR, x.downloadurl) } += ("GeneralStats", Some(a.fromdate), Some(a.todate), a.UserID, Some(result._2))

    //val reportulrdata = Json.parse(allreportstats("GeneralStats", true).firstOption.get) //json response called through file report name,flagcheck

    return "request added succesfully" //json response name 

  }

    def getGeneralreportfileList(date :String): JsValue = db withSession { implicit session =>
      val stats = Json.parse(allreportstats1("GeneralStats", true,date).firstOption.get)
      return stats
    }
     
      def allreportstats1(Reportname: String,Flagcheck: Boolean,currentdate: String) =
      sql"select * from getallreportstatsval2('#$Reportname','#$Flagcheck','#$currentdate')".as[(String)]
    
   /* def allreportstats(Reportname: String,Flagcheck: Boolean) =
      sql"select * from getallreportstatsval('#$Reportname','#$Flagcheck')".as[(String)]
   */
   def getGeneralreportsmethod(Fromdate: Date, Todate : Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getgeneralreports('#$Fromdate','#$Todate','#$HospitalID','#$HospitalCode')".as[(String)]
  
     def getReferralwithoutindication(Fromdate: Date, Todate : Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getReferralwithoutindication('#$Fromdate','#$Todate','#$HospitalID','#$HospitalCode')".as[(String)]
  
   def getCostReportwithregion(Fromdate: Date, Todate : Date,Hospitalcode: String) =
    sql"SELECT * FROM getcostreportbyregion('#$Fromdate','#$Todate','#$Hospitalcode')".as[(String)]

       
       
      def getReferralReportInfo(a: getReferralReportInfoObj): Array[listReferralReportListInfo] = db withSession { implicit session =>
  

        val referralReportList = getreferralreportinfo(a.fromdate,a.todate,a.hospitalid,a.Hospitalcode).list
        
        val result: Array[listReferralReportListInfo] = new Array[listReferralReportListInfo](referralReportList.length)
        
      for (i <- 0 until referralReportList.length) {
        
        val patientEnquiryID: Option[Long] = Some(referralReportList(i)._11)
        
        val specialityMasterDetails =  enquirySpeciality.filter { x => x.PatientEnquiryId === patientEnquiryID.get }.list
        
        val specialityDetails =  specialityMasterDetails.filter { x => x.PatientEnquiryId ==  patientEnquiryID.get  }.toList
        
        val specialityArr: Array[specialitydetailsObj] = new Array[specialitydetailsObj](specialityDetails.length)

        for(i <- 0 until specialityDetails.length){
      
        specialityArr(i) = new specialitydetailsObj(Some(specialityDetails(i).Id), specialityDetails(i).SpecialityCode,specialityDetails(i).SpecialityDescription,specialityDetails(i).ActiveInd,specialityDetails(i).SubSpecialities)
      
        }
         
        result(i) = new listReferralReportListInfo(Some(referralReportList(i)._1),Some(referralReportList(i)._2),Some(referralReportList(i)._3),Some(referralReportList(i)._4),Some(referralReportList(i)._5),Some(referralReportList(i)._6)
                                               ,Some(referralReportList(i)._7),Some(referralReportList(i)._8),Some(referralReportList(i)._9),Some(referralReportList(i)._10),Some(referralReportList(i)._11),specialityArr)
        
      }

    return result
     
  }
      
            def getAppointmentPracticereportInfo(a: getAppointmentpracticeInfoObj): Array[listAppointmentpracticeReport] = db withSession { implicit session =>
  

        val appointmentpracticeReportList = getappointmentpracticereportinfo(a.fromdate,a.todate,a.hospitalid,a.Hospitalcode,a.practiceid).list
        
        val result: Array[listAppointmentpracticeReport] = new Array[listAppointmentpracticeReport](appointmentpracticeReportList.length)
        
      for (i <- 0 until appointmentpracticeReportList.length) {
         
        result(i) = new listAppointmentpracticeReport(Some(appointmentpracticeReportList(i)._1),Some(appointmentpracticeReportList(i)._2),Some(appointmentpracticeReportList(i)._3),Some(appointmentpracticeReportList(i)._4),Some(appointmentpracticeReportList(i)._5),Some(appointmentpracticeReportList(i)._6),Some(appointmentpracticeReportList(i)._7)
                                               ,Some(appointmentpracticeReportList(i)._8),Some(appointmentpracticeReportList(i)._9),Some(appointmentpracticeReportList(i)._10),Some(appointmentpracticeReportList(i)._11)
                                               ,Some(appointmentpracticeReportList(i)._12),Some(appointmentpracticeReportList(i)._13),Some(appointmentpracticeReportList(i)._14),Some(appointmentpracticeReportList(i)._15)
                                               ,Some(appointmentpracticeReportList(i)._16),Some(appointmentpracticeReportList(i)._17),Some(appointmentpracticeReportList(i)._18),Some(appointmentpracticeReportList(i)._19)
                                               ,Some(appointmentpracticeReportList(i)._20),Some(appointmentpracticeReportList(i)._21))
        
      }

    return result
     
  }
            
            
            
          def getAppointmentPracticeindicationreportInfo(a: getReferralReportInfoObj): Array[listAppointmentpracticeindicationsReport] = db withSession { implicit session =>
  

        val appointmentpracticeindicationReportList = getappointmentpracticeindicationreportinfo(a.fromdate,a.todate,a.hospitalid,a.Hospitalcode).list
        
        val result: Array[listAppointmentpracticeindicationsReport] = new Array[listAppointmentpracticeindicationsReport](appointmentpracticeindicationReportList.length)
        
      for (i <- 0 until appointmentpracticeindicationReportList.length) {
         
        result(i) = new listAppointmentpracticeindicationsReport(Some(appointmentpracticeindicationReportList(i)._1),Some(appointmentpracticeindicationReportList(i)._2),Some(appointmentpracticeindicationReportList(i)._3),Some(appointmentpracticeindicationReportList(i)._4),Some(appointmentpracticeindicationReportList(i)._5),Some(appointmentpracticeindicationReportList(i)._6),Some(appointmentpracticeindicationReportList(i)._7)
                                               ,Some(appointmentpracticeindicationReportList(i)._8),Some(appointmentpracticeindicationReportList(i)._9),Some(appointmentpracticeindicationReportList(i)._10),Some(appointmentpracticeindicationReportList(i)._11)
                                               ,Some(appointmentpracticeindicationReportList(i)._12),Some(appointmentpracticeindicationReportList(i)._13),Some(appointmentpracticeindicationReportList(i)._14),Some(appointmentpracticeindicationReportList(i)._15)
                                               ,Some(appointmentpracticeindicationReportList(i)._16),Some(appointmentpracticeindicationReportList(i)._17),Some(appointmentpracticeindicationReportList(i)._18),Some(appointmentpracticeindicationReportList(i)._19))
        
      }

    return result
     
  }
    
                   
      def getPatientinSystemReportInfo(a: getReferralReportInfoObj): Array[listPatientinSystemInfo] = db withSession { implicit session =>
  

        val  patientinsystemReportList = patientinsystemreportinfo(a.fromdate,a.todate,a.hospitalid,a.Hospitalcode).list
        
        val result: Array[listPatientinSystemInfo] = new Array[listPatientinSystemInfo](patientinsystemReportList.length)
        
      for (i <- 0 until patientinsystemReportList.length) {
         
        result(i) = new listPatientinSystemInfo(Some(patientinsystemReportList(i)._1),Some(patientinsystemReportList(i)._2),Some(patientinsystemReportList(i)._3),Some(patientinsystemReportList(i)._4),Some(patientinsystemReportList(i)._5),Some(patientinsystemReportList(i)._6)
                                               ,Some(patientinsystemReportList(i)._7))
        
      }

    return result
     
  }
      
      
         def getPatientinSystemReportInfonew(a: getReferralReportInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getPatientinSystemReportnew(a.fromdate,a.todate,a.hospitalid,a.Hospitalcode).firstOption.get)
     
  }
         
           def getPatientinSystemReportInfonewv1(a: getReferralReportInfoObjv1): JsValue = db withSession { implicit session =>
  
    Json.parse(getPatientinSystemReportnewv1(a.CategoryID,a.fromdate,a.todate,a.hospitalid,a.Hospitalcode).firstOption.get)
     
  }
         
         
              def getPatientinSystemReportnew(Fromdate: Date, Todate : Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getpatientinsystemdetailsjson('#$Fromdate','#$Todate','#$HospitalID','#$HospitalCode')".as[(String)]
         
              
               def getPatientinSystemReportnewv1(categoryid: Long,Fromdate: Date, Todate : Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getpatientinsystemdetailsjson11('#$categoryid','#$Fromdate','#$Todate','#$HospitalID','#$HospitalCode')".as[(String)]
         
              
              
              def getPatientinSystemReporttotal(a: getReferralReportInfoObjv1): JsValue = db withSession { implicit session =>
  
    Json.parse(getPatientinsystemreporttotal(a.CategoryID,a.fromdate,a.todate,a.hospitalid,a.Hospitalcode).firstOption.get)
     
  }
         
              def getPatientinsystemreporttotal(categoryid: Long,Fromdate: Date, Todate : Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getpatientinsystemdetailsjsontotal11('#$categoryid','#$Fromdate','#$Todate','#$HospitalID','#$HospitalCode')".as[(String)]
                
              
              def getCalendarlistview(a: getCalenderlistviewInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getCalendarlistviewmethod(a.date,a.Hospitalcode,0,a.hospitalID).firstOption.get)
     
  }
         
              def getCalendarlistviewmethod(CurrentDate: Date, HospitalCode : String,ParentHospitalID: Long,HospitalID: Long) =
    sql"SELECT * FROM getcalendar_listview('#$CurrentDate','#$HospitalCode','#$ParentHospitalID','#$HospitalID')".as[(String)]
  
           
              def getAgentstatsreport(a: getAgentstatsInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getAgentstatsreportmethod(a.date,a.Hospitalcode).firstOption.get)
     
  }
         
              def getAgentstatsreportmethod(CurrentDate: Date, HospitalCode : String) =
    sql"SELECT * FROM getagentstatsreport('#$CurrentDate','#$HospitalCode')".as[(String)]
              
                  def getAgentstatsreporttotal(a: getAgentstatsInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getAgentstatsreporttotalmethod(a.date,a.Hospitalcode).firstOption.get)
     
  }
              
              
              def getExceptionnotesreportmethod(FromDate: Date,EndDate: Date, HospitalCode : String) =
    sql"SELECT * FROM getexceptionnotesreport('#$FromDate','#$EndDate','#$HospitalCode')".as[(String)]
              
                  def getExceptionnotesreport(a: getCostReportwithregionInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getExceptionnotesreportmethod(a.fromdate,a.todate,a.Hospitalcode).firstOption.get)
     
  }
              
              
              
                       
              def getAgentstatsreporttotalmethod(CurrentDate: Date, HospitalCode : String) =
    sql"SELECT * FROM getagentstatsreporttotal('#$CurrentDate','#$HospitalCode')".as[(String)]
  
              
                def getallPracticereport(a: getAgentstatsInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getallpracticereportmethod(a.date,a.Hospitalcode).firstOption.get)
     
  }
                            
                def getallpracticereportmethod(CurrentDate: Date, HospitalCode : String) =
    sql"SELECT * FROM getallpracticereport('#$CurrentDate','#$HospitalCode')".as[(String)]

      
    def getClinicauditcriteriaReportInfo(a: getclinicauditReportInfoObj): Array[listClinicauditcriteriaInfo] = db withSession { implicit session =>
  
 
        val  clinicauditcriteriaReportList = clinicauditcriteriareportinfo(a.fromdate,a.todate,a.hospitalid,a.Hospitalcode,a.regionid,a.ccgid).list
        
        val result: Array[listClinicauditcriteriaInfo] = new Array[listClinicauditcriteriaInfo](clinicauditcriteriaReportList.length)
        
      for (i <- 0 until clinicauditcriteriaReportList.length) {
         
        result(i) = new listClinicauditcriteriaInfo(Some(clinicauditcriteriaReportList(i)._1),Some(clinicauditcriteriaReportList(i)._2),Some(clinicauditcriteriaReportList(i)._3),Some(clinicauditcriteriaReportList(i)._4),Some(clinicauditcriteriaReportList(i)._5),Some(clinicauditcriteriaReportList(i)._6),Some(clinicauditcriteriaReportList(i)._7),Some(clinicauditcriteriaReportList(i)._8))
   
      }

    return result
     
  }
    
        
        def getCostanalyticsreportInfo(a: getCostReportInfoObj): Array[listCostanalyticsreportInfo] = db withSession { implicit session =>
  

        val  CostanalyticsReportList = costanalyticsreportinfo(a.fromdate,a.todate,a.Regionid).list
        
        val result: Array[listCostanalyticsreportInfo] = new Array[listCostanalyticsreportInfo](CostanalyticsReportList.length)
        
      for (i <- 0 until CostanalyticsReportList.length) {
         
        result(i) = new listCostanalyticsreportInfo(Some(CostanalyticsReportList(i)._1),Some(CostanalyticsReportList(i)._2),Some(CostanalyticsReportList(i)._3),Some(CostanalyticsReportList(i)._4),Some(CostanalyticsReportList(i)._5))
        
      }

    return result
     
  }
        
        def costanalyticsreportinfo(FromDate: Date,ToDate: Date,RegionID: Long) =
    sql"SELECT * FROM getcostanalyticsreport('#$FromDate','#$ToDate','#$RegionID')".as[(Long,Long,Long,Long,Long)]
           
     
             def patientinsystemreportinfo(FromDate: Date,ToDate: Date,HospitalID: Long,Hospitalcode: String) =
    sql"SELECT * FROM getpatientinsystemdetails('#$FromDate','#$ToDate','#$HospitalID','#$Hospitalcode')".as[(Long,Long,Long,Long,Long,Long,java.sql.Date)]


                        def clinicauditcriteriareportinfo(FromDate: Date,ToDate: Date,HospitalID: Long,HospitalCode: String,RegionID: Long,CcgID: Long) =
    sql"SELECT * FROM getclinicauditcriteria('#$FromDate','#$ToDate','#$HospitalID','#$HospitalCode','#$RegionID','#$CcgID')".as[(Long,String,java.sql.Date,String,String,String,String,String)]

            
      
                   def getappointmentpracticereportinfo(FromDate: Date,ToDate: Date,HospitalID: Long,HospitalCode: String,PracticeID: Long) =
    sql"SELECT * FROM getappointmentpracticereport('#$FromDate','#$ToDate','#$HospitalID','#$HospitalCode','#$PracticeID')".as[(Long,Long,String,String,String,String,String,String,String,java.sql.Date,String,java.sql.Date,String,String,String,String,String,String,String,Long,String)]
                   
                    
                   def getappointmentpracticeindicationreportinfo(FromDate: Date,ToDate: Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getappointmentpracticereportwithindication('#$FromDate','#$ToDate','#$HospitalID','#$HospitalCode')".as[(Long,Long,String,String,String,String,String,String,String,java.sql.Date,String,java.sql.Date,String,String,String,String,String,String,Long)]

      
      
             def getreferralreportinfo(FromDate: Date,ToDate: Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getreferralreport('#$FromDate','#$ToDate','#$HospitalID','#$HospitalCode')".as[(Long,java.sql.Date,java.sql.Date,java.sql.Date,String,String,String,String,String,String,Long)]


       def getcostreportinfo(FromDate: Date,ToDate: Date,RegionID: Long) =
    sql"SELECT * FROM getcostreport('#$FromDate','#$ToDate','#$RegionID')".as[(Long,java.sql.Date,String,Long,Long,Long,Long,Long,Long,Long,Long,Double,Double,Double,Double)]
   
        
               def getforecastreportinfo(FromDate: Date,ToDate: Date,RegionID: Long) =
    sql"SELECT * FROM getforecastreport('#$FromDate','#$ToDate','#$RegionID')".as[(Long,java.sql.Date,String,Long,Long,String,String,Long,Long,Long,Long,Long,Long,Double,Double,Double,Double)]
   

  
   def getdashborddetailsval(FromDate: Date,ToDate: Date,HospitalCode: String) =
    sql"SELECT * FROM getdashborddetails('#$FromDate','#$ToDate','#$HospitalCode')".as[(Long,Long,Long,Long,Long,Long,Long,Long,Long,Long,Long,Long,Long,Long,Long,Long)]
   
      def getdailyreportinfo(date: Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getdailyreport('#$date','#$HospitalID','#$HospitalCode')".as[(Long,String,Long,Long,Long,Long,Long,Long)]
   
         def getdailyspecialityreportinfo(date: Date,HospitalID: Long,HospitalCode: String) =
    sql"SELECT * FROM getdailspecialityyreport('#$date','#$HospitalID','#$HospitalCode')".as[(Long,java.sql.Date,Long,Long,String,String,String)]

   
  def getNewDashboardInfoQuery(hospitalId: Long,doctorid: Long,addressid: Long, startDate: Date, endDate: Date, includeBranch: String,dashboardtype : String)  = 
      sql"SELECT * FROM getnewdashboardinfoquery('#$hospitalId','#$doctorid','#$addressid','#$startDate','#$endDate','#$includeBranch','#$dashboardtype')".as[
        (Double,Double,String,String,String,String,String,Double,String,String,String,String,String,String,String,String,String,String,String,String,String,String)]
  
  def getDoctorMedQuery(DoctorID: Long, HospitalID: Long, ItemType:String) =
    sql"SELECT * FROM get_medicine_newdashboard('#$DoctorID','#$HospitalID','#$ItemType' )".as[(String)]
  
   def gettargetbasedRevenue(HospitalID: Long, startDate: Date, endDate: Date) =
    sql"SELECT * FROM gettargetbasedRevenue('#$HospitalID','#$startDate','#$endDate' )".as[(Double)]
  
  def getTotalTreatmentCount(startDate: Date, endDate: Date,hospitalId: Long,doctorid: Long,addressid: Long, includeBranch: String) =
    sql"SELECT * FROM rpttotaltreatment_cnt('#$startDate','#$endDate','#$hospitalId','#$doctorid','#$addressid','#$includeBranch')".as[(Long)]
  
}