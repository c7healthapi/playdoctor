package models.com.hcue.doctors.traits.dashboard

import java.sql.Date

import scala.collection.mutable.ListBuffer
import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.booleanOptionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.optionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker

import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.DashboardInfoTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.EachRecordObj
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.dctrAptmtDtls
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.NewDashboardInfoTypeObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.patientVisitsDtlsObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.AgeDtls
import models.com.hcue.doctors.Json.Request.doctorCommStatusObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.totalAmountObj
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.outstandingObjDtls
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.revenueTargetObj

import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital

import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.RevenueObjDtls

import play.api.libs.json.JsValue
import play.api.libs.json.Json

import play.api.libs.json.Json.toJsFieldJsValueWrapper
import java.util.Calendar
import java.text.SimpleDateFormat
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.Json.Request.dashBoard.DashboardReqResValidator.getCostReportwithregionInfoObj


object CustomreportDAO {
  
  def getCustombookingreportview(a: getCostReportwithregionInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getCustombookingreportmethod(a.fromdate,a.todate,a.Hospitalcode).firstOption.get)
     
  }
         
              def getCustombookingreportmethod(FromDate: Date, ToDate: Date,HospitalCode : String) =
    sql"SELECT * FROM getcustombookingreport('#$FromDate','#$ToDate','#$HospitalCode')".as[(String)]
  
              
     def getCustomadminreportview(a: getCostReportwithregionInfoObj): JsValue = db withSession { implicit session =>
  
    Json.parse(getCustomadminreportmethod(a.fromdate,a.todate,a.Hospitalcode).firstOption.get)
     
  }
         
              def getCustomadminreportmethod(FromDate: Date, ToDate: Date,HospitalCode : String) =
    sql"SELECT * FROM getcustomadminreport('#$FromDate','#$ToDate','#$HospitalCode')".as[(String)]           
           
               
}