/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccessRights

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime

case class addUpdateHcueAccessRightsObj(AccessID: String, AccessDesc: String, ActiveIND: String, ParentAccessID: Option[String], AccessCategoryID: Option[Long], USRId: Long, USRType: String)

object addUpdateHcueAccessRightsObj {

  val addUpdateHcueAccessRightsObjReads = (
    (__ \ "AccessID").read[String] and
    (__ \ "AccessDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "ParentAccessID").readNullable[String] and
    (__ \ "AccessCategoryID").readNullable[Long] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateHcueAccessRightsObj.apply _)

  val addUpdateHcueAccessRightsObjWrites = (
    (__ \ "AccessID").write[String] and
    (__ \ "AccessDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "ParentAccessID").writeNullable[String] and
    (__ \ "AccessCategoryID").writeNullable[Long] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateHcueAccessRightsObj.unapply))
  implicit val addUpdateProcedureCatalogFormat: Format[addUpdateHcueAccessRightsObj] = Format(addUpdateHcueAccessRightsObjReads, addUpdateHcueAccessRightsObjWrites)

}

case class HcueAccessRightsReqObj(USRId: Long, USRType: String, hospitalId: Option[Long])

object HcueAccessRightsReqObj {

  val HcueAccessRightsReqObjReads = (
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String] and
    (__ \ "hospitalId").readNullable[Long])(HcueAccessRightsReqObj.apply _)

  val HcueAccessRightsReqObjWrites = (

    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String] and
    (__ \ "hospitalId").writeNullable[Long])(unlift(HcueAccessRightsReqObj.unapply))
  implicit val HcueAccessRightsReqObjFormat: Format[HcueAccessRightsReqObj] = Format(HcueAccessRightsReqObjReads, HcueAccessRightsReqObjWrites)

}

case class listHcueAccessRightsObj(AccessID: String, AccessDesc: String, ActiveIND: String, ParentAccessID: Option[String])

object listHcueAccessRightsObj {

  val listHcueAccessRightsObjReads = (
    (__ \ "AccessID").read[String] and
    (__ \ "AccessDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "ParentAccessID").readNullable[String])(listHcueAccessRightsObj.apply _)

  val listHcueAccessRightsObjWrites = (
    (__ \ "AccessID").write[String] and
    (__ \ "AccessDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "ParentAccessID").writeNullable[String])(unlift(listHcueAccessRightsObj.unapply))
  implicit val listProcedureCatalogFormat: Format[listHcueAccessRightsObj] = Format(listHcueAccessRightsObjReads, listHcueAccessRightsObjWrites)

}

case class HcueAccessAdminRigthsObj(AccessCategoryID: Option[Long], AccessCategoryDesc: Option[String], AccessRights: Array[listHcueAccessAdminRightsObj])

object HcueAccessAdminRigthsObj {

  val HcueAccessAdminRigthsObjReads = (
    (__ \ "AccessCategoryID").readNullable[Long] and
    (__ \ "AccessCategoryDesc").readNullable[String] and
    (__ \ "AccessRights").read[Array[listHcueAccessAdminRightsObj]])(HcueAccessAdminRigthsObj.apply _)

  val HcueAccessAdminRigthsObjWrites = (
    (__ \ "AccessCategoryID").writeNullable[Long] and
    (__ \ "AccessCategoryDesc").writeNullable[String] and
    (__ \ "AccessRights").write[Array[listHcueAccessAdminRightsObj]])(unlift(HcueAccessAdminRigthsObj.unapply))
  implicit val HcueAccessAdminRigthsObjFormat: Format[HcueAccessAdminRigthsObj] = Format(HcueAccessAdminRigthsObjReads, HcueAccessAdminRigthsObjWrites)

}

case class listHcueAccessAdminRightsObj(AccessID: String, AccessDesc: String, ActiveIND: String, ParentAccessID: Option[String], AccessCategoryID: Option[Long], Order: Option[Int])

object listHcueAccessAdminRightsObj {

  val listHcueAccessAdminRightsObjReads = (
    (__ \ "AccessID").read[String] and
    (__ \ "AccessDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "ParentAccessID").readNullable[String] and
    (__ \ "AccessCategoryID").readNullable[Long] and
    (__ \ "Order").readNullable[Int])(listHcueAccessAdminRightsObj.apply _)

  val listHcueAccessAdminRightsObjWrites = (
    (__ \ "AccessID").write[String] and
    (__ \ "AccessDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "ParentAccessID").writeNullable[String] and
    (__ \ "AccessCategoryID").writeNullable[Long] and
    (__ \ "Order").writeNullable[Int])(unlift(listHcueAccessAdminRightsObj.unapply))
  implicit val listHcueAccessAdminRightsObjFormat: Format[listHcueAccessAdminRightsObj] = Format(listHcueAccessAdminRightsObjReads, listHcueAccessAdminRightsObjWrites)

}