/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccount

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime

case class addUpdateHcueAccountObj(AccountID: String, AccountDesc: String, Active: String, USRId: Long, USRType: String)

object addUpdateHcueAccountObj {

  val addUpdateHcueAccountObjReads = (
    (__ \ "AccountID").read[String] and
    (__ \ "AccountDesc").read[String] and
    (__ \ "Active").read[String] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateHcueAccountObj.apply _)

  val addUpdateHcueAccountObjWrites = (
    (__ \ "AccountID").write[String] and
    (__ \ "AccountDesc").write[String] and
    (__ \ "Active").write[String] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateHcueAccountObj.unapply))
  implicit val addUpdateProcedureCatalogFormat: Format[addUpdateHcueAccountObj] = Format(addUpdateHcueAccountObjReads, addUpdateHcueAccountObjWrites)

}

case class listHcueAccountObj(AccountID: String, AccountDesc: String, Active: String)

object listHcueAccountObj {

  val listHcueAccountObjReads = (
    (__ \ "AccountID").read[String] and
    (__ \ "AccountDesc").read[String] and
    (__ \ "Active").read[String])(listHcueAccountObj.apply _)

  val listHcueAccountObjWrites = (
    (__ \ "AccountID").write[String] and
    (__ \ "AccountDesc").write[String] and
    (__ \ "Active").write[String])(unlift(listHcueAccountObj.unapply))
  implicit val listProcedureCatalogFormat: Format[listHcueAccountObj] = Format(listHcueAccountObjReads, listHcueAccountObjWrites)

}
