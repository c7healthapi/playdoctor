/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHospitalRole

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime
import myUtils.hcueFormats._

case class addDefaultHospitalRoleObj(HospitalID: Long, USRId: Long, USRType: String)
  
object addDefaultHospitalRoleObj {

  val addDefaultHospitalRoleObjReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addDefaultHospitalRoleObj.apply _)

  val addDefaultHospitalRoleObjWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addDefaultHospitalRoleObj.unapply))
  implicit val addUpdateProcedureCatalogFormat: Format[addDefaultHospitalRoleObj] = Format(addDefaultHospitalRoleObjReads, addDefaultHospitalRoleObjWrites)

}

case class listHospitalRoleSettingObj(HospitalID: Long, Roles:Array[listRoleArrayObj] )
  
object listHospitalRoleSettingObj {

  val listHospitalRoleSettingObjReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "Roles").read[Array[listRoleArrayObj]])(listHospitalRoleSettingObj.apply _)

  val listHospitalRoleSettingObjWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "Roles").write[Array[listRoleArrayObj]])(unlift(listHospitalRoleSettingObj.unapply))
  implicit val listHospitalRoleSettingFormat: Format[listHospitalRoleSettingObj] = Format(listHospitalRoleSettingObjReads, listHospitalRoleSettingObjWrites)

}

case class listRoleArrayObj(RoleID: String,ActiveIND: String)
  
object listRoleArrayObj {

  val listRoleArrayObjReads = (
    (__ \ "RoleID").read[String] and
    (__ \ "ActiveIND").read[String])(listRoleArrayObj.apply _)

  val listRoleArrayObjWrites = (
    (__ \ "RoleID").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(listRoleArrayObj.unapply))
  implicit val listRoleArrayObjFormat: Format[listRoleArrayObj] = Format(listRoleArrayObjReads, listRoleArrayObjWrites)

}

