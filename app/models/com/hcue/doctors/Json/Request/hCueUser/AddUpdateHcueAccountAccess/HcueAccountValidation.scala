/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueAccountAccess

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.functional.syntax.unlift
import play.api.libs.json.Format
import play.api.libs.json.__

case class addUpdateAccountAccessObj(AcntAccessID: Option[String], AccessID: String, ActiveIND: String, AccountID: Map[String, String], USRId: Long, USRType: String)

object addUpdateAccountAccessObj {

  val addUpdateAccountAccessObjReads = (
    (__ \ "AcntAccessID").readNullable[String] and
    (__ \ "AccessID").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "AccountID").read[Map[String, String]] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateAccountAccessObj.apply _)

  val addUpdateAccountAccessObjWrites = (
    (__ \ "AcntAccessID").writeNullable[String] and
    (__ \ "AccessID").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "AccountID").write[Map[String, String]] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateAccountAccessObj.unapply))
  implicit val addUpdateAccountObjFormat: Format[addUpdateAccountAccessObj] = Format(addUpdateAccountAccessObjReads, addUpdateAccountAccessObjWrites)

}

case class listHcueAccountObj(AccountID: String, AccountDesc: String, Active: String)

object listHcueAccountObj {

  val listHcueAccountObjReads = (
    (__ \ "AccountID").read[String] and
    (__ \ "AccountDesc").read[String] and
    (__ \ "Active").read[String])(listHcueAccountObj.apply _)

  val listHcueAccountObjWrites = (
    (__ \ "AccountID").write[String] and
    (__ \ "AccountDesc").write[String] and
    (__ \ "Active").write[String])(unlift(listHcueAccountObj.unapply))
  implicit val listProcedureCatalogFormat: Format[listHcueAccountObj] = Format(listHcueAccountObjReads, listHcueAccountObjWrites)

}

case class listAccountAccessObj(AcntAccessID: String, AccessID: String, ActiveIND: String, AccountID: Map[String, String])

object listAccountAccessObj {

  val listAccountAccessObjReads = (
    (__ \ "AcntAccessID").read[String] and
    (__ \ "AccessID").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "AccountID").read[Map[String, String]])(listAccountAccessObj.apply _)

  val listAccountAccessObjWrites = (
    (__ \ "AcntAccessID").write[String] and
    (__ \ "AccessID").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "AccountID").write[Map[String, String]])(unlift(listAccountAccessObj.unapply))
  implicit val listAccountObjFormat: Format[listAccountAccessObj] = Format(listAccountAccessObjReads, listAccountAccessObjWrites)

}


