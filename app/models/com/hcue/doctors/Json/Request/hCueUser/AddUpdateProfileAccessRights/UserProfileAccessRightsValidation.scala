/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateProfileAccessRights

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import play.api.data.validation._
import play.api.libs.functional.syntax._


case class addUpdateProfileAccessRightsObj(GroupCode: Long, GroupName: Option[String],
                                           HospitalID: Long, RoleID: String,ReadOnly : String ,AccountAccessID: Map[String, String], 
                                           Active: String, USRId: Long, USRType: String)

object addUpdateProfileAccessRightsObj {

  val addUpdateProfileAccessRightsObjReads = (
    (__ \ "GroupCode").read[Long] and
    (__ \ "GroupName").readNullable[String] and
    (__ \ "HospitalID").read[Long] and
    (__ \ "RoleID").read[String] and
    (__ \ "ReadOnly").read[String] and
    (__ \ "AccountAccessID").read[Map[String, String]] and
    (__ \ "Active").read[String] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String] )(addUpdateProfileAccessRightsObj.apply _)

  val addUpdateProfileAccessRightsObjWrites = (
    (__ \ "GroupCode").write[Long] and
    (__ \ "GroupName").writeNullable[String] and
    (__ \ "HospitalID").write[Long] and
    (__ \ "RoleID").write[String] and
    (__ \ "ReadOnly").write[String] and
    (__ \ "AccountAccessID").write[Map[String, String]] and
    (__ \ "Active").write[String] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateProfileAccessRightsObj.unapply))
  implicit val addUpdateProcedureCatalogFormat: Format[addUpdateProfileAccessRightsObj] = Format(addUpdateProfileAccessRightsObjReads, addUpdateProfileAccessRightsObjWrites)

}


case class listHospitalProfileAccessRightsObj(Hospital:Long,Roles:Array[listProfileAccessRightsObj])

object listHospitalProfileAccessRightsObj {

  val listHospitalProfileAccessRightsObjReads = (
    (__ \ "Hospital").read[Long] and
    (__ \ "Roles").read[Array[listProfileAccessRightsObj]])(listHospitalProfileAccessRightsObj.apply _)

  val listHospitalProfileAccessRightsObjWrites = (
    (__ \ "Hospital").write[Long] and
    (__ \ "Roles").write[Array[listProfileAccessRightsObj]])(unlift(listHospitalProfileAccessRightsObj.unapply))
  implicit val listProcedureCatalogFormat: Format[listHospitalProfileAccessRightsObj] = Format(listHospitalProfileAccessRightsObjReads, listHospitalProfileAccessRightsObjWrites)

}


case class listProfileAccessRightsObj(GroupCode: Long, GroupName: Option[String], AccountAccessID: Map[String, String], RoleID: Option[String],RoleDesc: Option[String], Active: String,ReadOnly:String)

object listProfileAccessRightsObj {

  val listProfileAccessRightsObjReads = (
    (__ \ "GroupCode").read[Long] and
    (__ \ "GroupName").readNullable[String] and
    (__ \ "AccountAccessID").read[Map[String, String]] and
    (__ \ "RoleID").readNullable[String] and
    (__ \ "RoleDesc").readNullable[String] and
    (__ \ "Active").read[String] and
    (__ \ "ReadOnly").read[String])(listProfileAccessRightsObj.apply _)

  val listProfileAccessRightsObjWrites = (
    (__ \ "GroupCode").write[Long] and
    (__ \ "GroupName").writeNullable[String] and
    (__ \ "AccountAccessID").write[Map[String, String]] and
    (__ \ "RoleID").writeNullable[String] and
    (__ \ "RoleDesc").writeNullable[String] and
    (__ \ "Active").write[String] and
    (__ \ "ReadOnly").write[String])(unlift(listProfileAccessRightsObj.unapply))
  implicit val listProcedureCatalogFormat: Format[listProfileAccessRightsObj] = Format(listProfileAccessRightsObjReads, listProfileAccessRightsObjWrites)

}
