/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.hCueUser.AddUpdateHcueRole

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.functional.syntax.unlift
import play.api.libs.json.Format
import play.api.libs.json.__

case class addUpdateRoleObj(RoleID: String, RoleDesc: String, ActiveIND: String, IsPrivate: String, HospitalID: Option[Long], USRId: Long, USRType: String)

  
object addUpdateRoleObj {

  val addUpdateRoleObjReads = (
    (__ \ "RoleID").read[String] and
    (__ \ "RoleDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "IsPrivate").read[String] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateRoleObj.apply _)

  val addUpdateRoleObjWrites = (
    (__ \ "RoleID").write[String] and
    (__ \ "RoleDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "IsPrivate").write[String] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateRoleObj.unapply))
  implicit val addUpdateRoleObjFormat: Format[addUpdateRoleObj] = Format(addUpdateRoleObjReads, addUpdateRoleObjWrites)

}


case class listRoleObj(RoleID: String, RoleDesc: String, ActiveIND: String, IsPrivate: String, HospitalID: Option[Long])

  
object listRoleObj {

  val listRoleObjReads = (
    (__ \ "RoleID").read[String] and
    (__ \ "RoleDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "IsPrivate").read[String] and
    (__ \ "HospitalID").readNullable[Long])(listRoleObj.apply _)

  val listRoleObjWrites = (
    (__ \ "RoleID").write[String] and
    (__ \ "RoleDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "IsPrivate").write[String] and
    (__ \ "HospitalID").writeNullable[Long])(unlift(listRoleObj.unapply))
  implicit val addUpdateRoleObjFormat: Format[listRoleObj] = Format(listRoleObjReads, listRoleObjWrites)

}

case class listHospitalRoleObj(ParentHospitalID: Long, ActiveIND: Option[String], USRId: Long, USRType: String)

  
object listHospitalRoleObj {

  val listHospitalRoleObjReads = (
    (__ \ "ParentHospitalID").read[Long] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "USRId").read[Long] and 
    (__ \ "USRType").read[String])(listHospitalRoleObj.apply _)

  val listHospitalRoleObjWrites = (
    (__ \ "ParentHospitalID").write[Long] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "USRId").write[Long] and 
    (__ \ "USRType").write[String])(unlift(listHospitalRoleObj.unapply))
  implicit val listHospitalRoleObjFormat: Format[listHospitalRoleObj] = Format(listHospitalRoleObjReads, listHospitalRoleObjWrites)

}

