/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date

import play.api.libs.functional.syntax._
import models.com.hcue.doctors.slick.lookupTable.TAddressTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TContactTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TCountryLkup
import models.com.hcue.doctors.slick.lookupTable.TDayLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorQualfLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorVisitRsnLkyp
import models.com.hcue.doctors.slick.lookupTable.TFamilyRltnLkup
import models.com.hcue.doctors.slick.lookupTable.TOtherIDTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TPhoneTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TStateLkup
import models.com.hcue.doctors.slick.lookupTable.TUSRTypeLkup

case class getDoctorVisitRsnLkupObj(SpecificationID: Option[List[String]], ActiveIND: Option[String])

object getDoctorVisitRsnLkupObj {
  val getDoctorVisitRsnLkupObjReads = (
    (__ \ "SpecificationID").readNullable[List[String]] and
    (__ \ "ActiveIND").readNullable[String])(getDoctorVisitRsnLkupObj.apply _)

  val getDoctorVisitRsnLkupObjWrites = (
    (__ \ "SpecificationID").writeNullable[List[String]] and
    (__ \ "ActiveIND").writeNullable[String])(unlift(getDoctorVisitRsnLkupObj.unapply))
  implicit val getVisitorTypeLkupFormat: Format[getDoctorVisitRsnLkupObj] = Format(getDoctorVisitRsnLkupObjReads, getDoctorVisitRsnLkupObjWrites)
}

case class getLkupObj(ActiveIND: String,DoctorID : Option[Long])

object getLkupObj {
  val getLkupObjReads = (
    (__ \ "ActiveIND").read[String] and
    (__ \ "DoctorID").readNullable[Long])(getLkupObj.apply _)

  val getLkupObjWrites = (
    (__ \ "ActiveIND").write[String] and
    (__ \ "DoctorID").writeNullable[Long])(unlift(getLkupObj.unapply))
  implicit val getLkupFormat: Format[getLkupObj] = Format(getLkupObjReads, getLkupObjWrites)
}

case class listDoctorAvailableVitalObj(VitalID: String,VitalDesc: Option[String],ActiveIND: String,DefaultOrder:Int)

object listDoctorAvailableVitalObj {
  val listDoctorAvailableVitalObjReads = (
    (__ \ "VitalID").read[String] and
    (__ \ "VitalDesc").readNullable[String] and
    (__ \ "ActiveIND").read[String] and 
    (__ \ "DefaultOrder").read[Int])(listDoctorAvailableVitalObj.apply _)

  val listDoctorAvailableVitalObjWrites = (
     (__ \ "VitalID").write[String] and
    (__ \ "VitalDesc").writeNullable[String] and
    (__ \ "ActiveIND").write[String] and 
    (__ \ "DefaultOrder").write[Int])(unlift(listDoctorAvailableVitalObj.unapply))
  implicit val listDoctorAvailableVitalFormat: Format[listDoctorAvailableVitalObj] = Format(listDoctorAvailableVitalObjReads, listDoctorAvailableVitalObjWrites)
}


case class getDoctorAvailableVitalObj(DoctorID: Long, HospitalCD: Option[String], VitalInfo: Array[getDoctorVitalInfoObj], PageNumber: Option[Int], PageSize: Option[Int], USRId: Int, USRType: String)

object getDoctorAvailableVitalObj {
  val getDoctorAvailableVitalObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "VitalInfo").read[Array[getDoctorVitalInfoObj]] and
    (__ \ "PageNumber").readNullable[Int] and
    (__ \ "PageSize").readNullable[Int] and
    (__ \ "USRId").read[Int] and 
    (__ \ "USRType").read[String])(getDoctorAvailableVitalObj.apply _)

  val getDoctorAvailableVitalObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "VitalInfo").write[Array[getDoctorVitalInfoObj]] and
    (__ \ "PageNumber").writeNullable[Int] and
    (__ \ "PageSize").writeNullable[Int] and
    (__ \ "USRId").write[Int] and 
    (__ \ "USRType").write[String])(unlift(getDoctorAvailableVitalObj.unapply))
  implicit val getVisitorTypeLkupFormat: Format[getDoctorAvailableVitalObj] = Format(getDoctorAvailableVitalObjReads, getDoctorAvailableVitalObjWrites)
}


case class getDoctorVitalInfoObj(VitalID: String, ActiveIND: String, DefaultOrder:Int)

object getDoctorVitalInfoObj {
  val getDoctorVitalInfoObjReads = (
    (__ \ "VitalID").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "DefaultOrder").read[Int])(getDoctorVitalInfoObj.apply _)

  val getDoctorVitalInfoObjWrites = (
    (__ \ "VitalID").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "DefaultOrder").write[Int])(unlift(getDoctorVitalInfoObj.unapply))
  implicit val getDoctorVitalInfoFormat: Format[getDoctorVitalInfoObj] = Format(getDoctorVitalInfoObjReads, getDoctorVitalInfoObjWrites)
}

case class listDoctorVitalsObj(DoctorID: Long, HospitalCD: Option[String],  ActiveIND: Option[String], PageNumber: Int, PageSize: Int,USRId: Int, USRType: String)

object listDoctorVitalsObj {
  val listDoctorVitalsObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "USRId").read[Int] and 
    (__ \ "USRType").read[String])(listDoctorVitalsObj.apply _)

  val listDoctorVitalsObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "USRId").write[Int] and 
    (__ \ "USRType").write[String])(unlift(listDoctorVitalsObj.unapply))
  implicit val listDoctorVitalsObjFormat: Format[listDoctorVitalsObj] = Format(listDoctorVitalsObjReads, listDoctorVitalsObjWrites)
}


case class getDoctorAvailableHistoryObj(DoctorID: Long, HospitalCD: Option[String], SpecialityCD: String,HistoryInfo: Array[getDoctorHistoryInfoObj],StartsWith:Option[String], PageNumber: Int, PageSize: Int,USRId: Int, USRType: String)

object getDoctorAvailableHistoryObj {
  val getDoctorAvailableHistoryObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "SpecialityCD").read[String] and
    (__ \ "HistoryInfo").read[Array[getDoctorHistoryInfoObj]] and
    (__ \ "StartsWith").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "USRId").read[Int] and 
    (__ \ "USRType").read[String])(getDoctorAvailableHistoryObj.apply _)

  val getDoctorAvailableHistoryObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "SpecialityCD").write[String] and
    (__ \ "HistoryInfo").write[Array[getDoctorHistoryInfoObj]] and
    (__ \ "StartsWith").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "USRId").write[Int] and 
    (__ \ "USRType").write[String])(unlift(getDoctorAvailableHistoryObj.unapply))
  implicit val getVisitorTypeLkupFormat: Format[getDoctorAvailableHistoryObj] = Format(getDoctorAvailableHistoryObjReads, getDoctorAvailableHistoryObjWrites)
}

case class doctorAvailableHistoryObj(DoctorID: Long, HospitalCD: Option[String], SpecialityCD: String,StartsWith:Option[String], PageNumber: Int, PageSize: Int,USRId: Int, USRType: String)

object doctorAvailableHistoryObj {
  val doctorAvailableHistoryObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "SpecialityCD").read[String] and
    (__ \ "StartsWith").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "USRId").read[Int] and 
    (__ \ "USRType").read[String])(doctorAvailableHistoryObj.apply _)

  val doctorAvailableHistoryObjWrites = (
    (__ \ "doctorID").write[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "specialityCD").write[String] and
    (__ \ "StartsWith").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "USRId").write[Int] and 
    (__ \ "USRType").write[String])(unlift(doctorAvailableHistoryObj.unapply))
  implicit val doctorAvailableHistoryFormat: Format[doctorAvailableHistoryObj] = Format(doctorAvailableHistoryObjReads, doctorAvailableHistoryObjWrites)
}

case class availableHistoryResponseObj(DoctorID: Long, HospitalCD: Option[String], HistoryID: Long, Description:String, SpecialityCD:String,DefaultOrder:Long, ActiveIND: String, IsPrivate:String)

object availableHistoryResponseObj {
  val availableHistoryResponseObjReads = (
    (__ \ "doctorID").read[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "HistoryID").read[Long] and
    (__ \ "Description").read[String] and
    (__ \ "SpecialityCD").read[String] and
    (__ \ "DefaultOrder").read[Long] and 
    (__ \ "ActiveIND").read[String] and
    (__ \ "IsPrivate").read[String])(availableHistoryResponseObj.apply _)

  val availableHistoryResponseObjWrites = (
    (__ \ "doctorID").write[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "HistoryID").write[Long] and
    (__ \ "Description").write[String] and
    (__ \ "SpecialityCD").write[String] and
    (__ \ "DefaultOrder").write[Long] and 
    (__ \ "ActiveIND").write[String] and
    (__ \ "IsPrivate").write[String])(unlift(availableHistoryResponseObj.unapply))
  implicit val availableHistoryResponseFormat: Format[availableHistoryResponseObj] = Format(availableHistoryResponseObjReads, availableHistoryResponseObjWrites)
}

case class getDoctorHistoryInfoObj(HistoryID: Long, Description:String,ActiveIND: String, DefaultOrder:Int,IsPrivate: String)

object getDoctorHistoryInfoObj {
  val getDoctorHistoryInfoObjReads = (
    (__ \ "HistoryID").read[Long] and
    (__ \ "Description").read[String] and 
    (__ \ "ActiveIND").read[String] and
    (__ \ "DefaultOrder").read[Int] and
    (__ \ "IsPrivate").read[String])(getDoctorHistoryInfoObj.apply _)

  val getDoctorHistoryInfoObjWrites = (
    (__ \ "HistoryID").write[Long] and
    (__ \ "Description").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "DefaultOrder").write[Int] and
    (__ \ "IsPrivate").write[String])(unlift(getDoctorHistoryInfoObj.unapply))
  implicit val getDoctorHistoryInfoFormat: Format[getDoctorHistoryInfoObj] = Format(getDoctorHistoryInfoObjReads, getDoctorHistoryInfoObjWrites)
}


case class allergensInfoObj(AllergensDesc:String, DoctorID: Option[Long], HospitalID: Option[Long], USRType: String, USRId: Long)

object allergensInfoObj {
  val allergensInfoObjReads = (
    (__ \ "AllergensDesc").read[String] and 
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "USRType").read[String] and
    (__ \ "USRId").read[Long])(allergensInfoObj.apply _)

  val allergensInfoObjWrites = (
    (__ \ "AllergensDesc").write[String] and
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "USRType").write[String] and
    (__ \ "USRId").write[Long])(unlift(allergensInfoObj.unapply))
  implicit val allergensInfoObjFormat: Format[allergensInfoObj] = Format(allergensInfoObjReads, allergensInfoObjWrites)
}

case class patientAppointmentListObj(PatientID:Long,DoctorID:Long,ConsultationDate:java.sql.Date, ConsultationTime: String, DoctorFullName:String,DoctorQualification:String,DoctorSpecialityCode:String, DoctorClinicName:Option[String],DoctorClinicAddress:Option[String],
    DoctorClinicPhoneNumber:Option[Long],DoctorClinicEmailID:Option[String],DoctorClinicWebSite:Option[String],PatientFullName:String,
    PatientPhoneNumber:Option[Long],PatientEmailID:Option[String],PatientAddress:Option[String])
    
object patientAppointmentListObj {
  val patientAppointmentListObjReads = (
    (__ \ "PatientID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "ConsultationDate").read[java.sql.Date] and 
    (__ \ "ConsultationTime").read[String] and 
    (__ \ "DoctorFullName").read[String] and 
    (__ \ "DoctorQualification").read[String] and 
    (__ \ "DoctorSpecialityCode").read[String] and 
    (__ \ "DoctorClinicName").readNullable[String] and 
    (__ \ "DoctorClinicAddress").readNullable[String] and
    (__ \ "DoctorClinicPhoneNumber").readNullable[Long] and
    (__ \ "DoctorClinicEmailID").readNullable[String] and 
    (__ \ "DoctorClinicWebSite").readNullable[String] and
    (__ \ "PatientFullName").read[String] and 
    (__ \ "PatientPhoneNumber").readNullable[Long] and
    (__ \ "PatientEmailID").readNullable[String] and
    (__ \ "PatientAddress").readNullable[String])(patientAppointmentListObj.apply _)

  val patientAppointmentListObjWrites = (
    (__ \ "PatientID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "ConsultationDate").write[java.sql.Date] and 
    (__ \ "ConsultationTime").write[String] and 
    (__ \ "DoctorFullName").write[String] and 
    (__ \ "DoctorQualification").write[String] and 
    (__ \ "DoctorSpecialityCode").write[String] and 
    (__ \ "DoctorClinicName").writeNullable[String] and 
    (__ \ "DoctorClinicAddress").writeNullable[String] and
    (__ \ "DoctorClinicPhoneNumber").writeNullable[Long] and
    (__ \ "DoctorClinicEmailID").writeNullable[String] and 
    (__ \ "DoctorClinicWebSite").writeNullable[String] and
    (__ \ "PatientFullName").write[String] and 
    (__ \ "PatientPhoneNumber").writeNullable[Long] and
    (__ \ "PatientEmailID").writeNullable[String] and
    (__ \ "PatientAddress").writeNullable[String])(unlift(patientAppointmentListObj.unapply))
  
  implicit val patientAppointmentListObjFormat: Format[patientAppointmentListObj] = Format(patientAppointmentListObjReads, patientAppointmentListObjWrites)
}

case class getLabTestLukUpObj(SearchText:String, LabType: Option[String], ActiveIND: Option[String], PageNumber: Int, PageSize: Int, Count: Option[Int], USRType: String, USRId: Long)

object getLabTestLukUpObj {
  val getLabTestLukUpObjReads = (
    (__ \ "SearchText").read[String] and 
    (__ \ "LabType").readNullable[String] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "Count").readNullable[Int] and
    (__ \ "USRType").read[String] and
    (__ \ "USRId").read[Long])(getLabTestLukUpObj.apply _)

  val getLabTestLukUpObjWrites = (
    (__ \ "SearchText").write[String] and
    (__ \ "LabType").writeNullable[String] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "Count").writeNullable[Int] and
    (__ \ "USRType").write[String] and
    (__ \ "USRId").write[Long])(unlift(getLabTestLukUpObj.unapply))
  implicit val getLabTestLukUpObjFormat: Format[getLabTestLukUpObj] = Format(getLabTestLukUpObjReads, getLabTestLukUpObjWrites)
}

case class listLabTestLukUpObj(LabTestID:Long, LabTest: Option[String], LabTestDescription: Option[String], LabType: Option[String], LabTestUsage: Option[String], 
                               LabTestGroup: Option[String], SpecialityCD: Option[String], ActiveIND: String)

object listLabTestLukUpObj {
  val listLabTestLukUpObjReads = (
    (__ \ "LabTestID").read[Long] and 
    (__ \ "LabTest").readNullable[String] and
    (__ \ "LabTestDescription").readNullable[String] and
    (__ \ "LabType").readNullable[String] and
    (__ \ "LabTestUsage").readNullable[String] and
    (__ \ "LabTestGroup").readNullable[String] and
    (__ \ "SpecialityCD").readNullable[String] and
    (__ \ "ActiveIND").read[String])(listLabTestLukUpObj.apply _)

  val listLabTestLukUpObjWrites = (
    (__ \ "LabTestID").write[Long] and
    (__ \ "LabTest").writeNullable[String] and
    (__ \ "LabTestDescription").writeNullable[String] and
    (__ \ "LabType").writeNullable[String] and
    (__ \ "LabTestUsage").writeNullable[String] and
    (__ \ "LabTestGroup").writeNullable[String] and
    (__ \ "SpecialityCD").writeNullable[String] and
    (__ \ "ActiveIND").write[String])(unlift(listLabTestLukUpObj.unapply))
  implicit val listLabTestLukUpObjFormat: Format[listLabTestLukUpObj] = Format(listLabTestLukUpObjReads, listLabTestLukUpObjWrites)
}


case class specialitylkupreq(SpecialityCD: Option[String], ActiveIND: String, SearchText : Option[String])

object specialitylkupreq {
  val specialitylkupreqReads = (
    (__ \ "SpecialityCD").readNullable[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "SearchText").readNullable[String])(specialitylkupreq.apply _)

  val specialitylkupreqWrites = (
    (__ \ "SpecialityCD").writeNullable[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "SearchText").writeNullable[String])(unlift(specialitylkupreq.unapply))
  implicit val specialitylkupreqFormat: Format[specialitylkupreq] = Format(specialitylkupreqReads, specialitylkupreqWrites)
}



case class readlkupreq(ActiveIND: String, SearchText : Option[String])

object readlkupreq {
  val readlkupreqReads = (
    (__ \ "ActiveIND").read[String] and
    (__ \ "SearchText").readNullable[String])(readlkupreq.apply _)

  val readlkupreqWrites = (
    (__ \ "ActiveIND").write[String] and
    (__ \ "SearchText").writeNullable[String])(unlift(readlkupreq.unapply))
  implicit val readlkupreqFormat: Format[readlkupreq] = Format(readlkupreqReads, readlkupreqWrites)
}
