/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.AddUpdateSchedule

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime

case class hospitalAppointmentResp(AppointmentID: Long, AppointmentStatus: String)

object hospitalAppointmentResp {
  val hospitalAppointmentRespReads = (
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AppointmentStatus").read[String])(hospitalAppointmentResp.apply _)

  val hospitalAppointmentRespWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AppointmentStatus").write[String])(unlift(hospitalAppointmentResp.unapply))
  implicit val hospitalAppointmentRespFormat: Format[hospitalAppointmentResp] = Format(hospitalAppointmentRespReads, hospitalAppointmentRespWrites)

}
case class addScheduleAppointmentDetailsObj(AppointmentID: Option[Long], AddressID: Long, AddressConsultID: Option[Long],
                                            DayCD: String, ScheduleDate: java.sql.Date, StartTime: String, EndTime: String,
                                            EmergencyStartTime: String, EmergencyEndTime: String,
                                            DoctorID: Long, PatientID: Long, Patientenquiryid: Long, AppointmentStatus: String,
                                            HospitalDetails: Option[hospitalDetailsObj], Startscantime: Option[String], 
                                            Completescantime: Option[String], Notes: Option[String], 
                                            USRId: Long, USRType: String, EnquiryDetails: Option[enquiryDetailsObj],
                                            NoOfArea: Option[Long])

object addScheduleAppointmentDetailsObj {

  val sqlDateWrite = Writes.sqlDateWrites("yyyy-MM-dd HH:mm:ss")

  val addScheduleAppointmentDetailsObjReads = (
    (__ \ "AppointmentID").readNullable[Long] and
    (__ \ "AddressID").read[Long] and
    (__ \ "AddressConsultID").readNullable[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ScheduleDate").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "EmergencyStartTime").read[String] and
    (__ \ "EmergencyEndTime").read[String] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "PatientID").read[Long] and
    (__ \ "Patientenquiryid").read[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "HospitalDetails").readNullable[hospitalDetailsObj] and
    (__ \ "Startscantime").readNullable[String] and
    (__ \ "Completescantime").readNullable[String] and
    (__ \ "Notes").readNullable[String] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String] and
    (__ \ "EnquiryDetails").readNullable[enquiryDetailsObj] and 
    (__ \ "NoOfArea").readNullable[Long])(addScheduleAppointmentDetailsObj.apply _)

  val addScheduleAppointmentDetailsObjWrites = (
    (__ \ "AppointmentID").writeNullable[Long] and
    (__ \ "AddressID").write[Long] and
    (__ \ "AddressConsultID").writeNullable[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ScheduleDate").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "EmergencyStartTime").write[String] and
    (__ \ "EmergencyEndTime").write[String] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "PatientID").write[Long] and
    (__ \ "Patientenquiryid").write[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "HospitalDetails").writeNullable[hospitalDetailsObj] and
    (__ \ "Startscantime").writeNullable[String] and
    (__ \ "Completescantime").writeNullable[String] and
    (__ \ "Notes").writeNullable[String] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String] and
    (__ \ "EnquiryDetails").writeNullable[enquiryDetailsObj] and 
    (__ \ "NoOfArea").writeNullable[Long])(unlift(addScheduleAppointmentDetailsObj.unapply))
  implicit val addScheduleAppointmentDetailsObjFormat: Format[addScheduleAppointmentDetailsObj] = Format(addScheduleAppointmentDetailsObjReads, addScheduleAppointmentDetailsObjWrites)

}

case class appointmentEmailerObj(appointmentid: Long, patientenquiryid: Option[Long], patientid: Option[Long], sendemail: Boolean, sendletter: Boolean, 
    emailtype: Option[String],doctorid: Option[Long],appointmenthospitalid:Option[Long],appointmentdate:Option[java.sql.Date],appointmenttime:Option[String])

object appointmentEmailerObj {

  val appointmentEmailerObjReads = (
    (__ \ "appointmentid").read[Long] and
    (__ \ "patientenquiryid").readNullable[Long] and
    (__ \ "patientid").readNullable[Long] and
    (__ \ "sendemail").read[Boolean] and
    (__ \ "sendletter").read[Boolean] and
    (__ \ "emailtype").readNullable[String] and 
    (__ \ "doctorid").readNullable[Long] and
    (__ \ "appointmenthospitalid").readNullable[Long] and
    (__ \ "appointmentdate").readNullable[java.sql.Date] and
    (__ \ "appointmenttime").readNullable[String])(appointmentEmailerObj.apply _)

  val appointmentEmailerObjWrites = (
    (__ \ "appointmentid").write[Long] and
    (__ \ "patientenquiryid").writeNullable[Long] and
    (__ \ "patientid").writeNullable[Long] and
    (__ \ "sendemail").write[Boolean] and
    (__ \ "sendletter").write[Boolean] and
    (__ \ "emailtype").writeNullable[String] and 
    (__ \ "doctorid").writeNullable[Long] and
    (__ \ "appointmenthospitalid").writeNullable[Long] and
    (__ \ "appointmentdate").writeNullable[java.sql.Date] and
    (__ \ "appointmenttime").writeNullable[String])(unlift(appointmentEmailerObj.unapply))
  implicit val addScheduleAppointmentDetailsObjFormat: Format[appointmentEmailerObj] = Format(appointmentEmailerObjReads, appointmentEmailerObjWrites)

}



case class sonographerinfoObj(scheduledate: java.sql.Date, doctorid: Long,hospitalid: Option[Long],hospitalname: Option[String],
    sonographername: Option[String],cswname: Option[String],addressconsultid: Option[Long],sendemail: Boolean)

object sonographerinfoObj {

  val sonographerinfoObjReads = (
    (__ \ "scheduledate").read[java.sql.Date] and
    (__ \ "doctorid").read[Long] and
    (__ \ "hospitalid").readNullable[Long] and
    (__ \ "hospitalname").readNullable[String] and
    (__ \ "sonographername").readNullable[String] and
    (__ \ "cswname").readNullable[String] and
    (__ \ "addressconsultid").readNullable[Long] and
    (__ \ "sendemail").read[Boolean])(sonographerinfoObj.apply _)

  val sonographerinfoObjWrites = (
    (__ \ "scheduledate").write[java.sql.Date] and
    (__ \ "doctorid").write[Long] and
    (__ \ "hospitalid").writeNullable[Long] and
    (__ \ "hospitalname").writeNullable[String] and
    (__ \ "sonographername").writeNullable[String] and
    (__ \ "cswname").writeNullable[String] and
    (__ \ "addressconsultid").writeNullable[Long] and
    (__ \ "sendemail").write[Boolean])(unlift(sonographerinfoObj.unapply))
  implicit val addScheduleAppointmentDetailsObjFormat: Format[sonographerinfoObj] = Format(sonographerinfoObjReads, sonographerinfoObjWrites)

}


case class sendFileAttachmentObj(responseId: Long, reportURL: Option[String],PracticeID: Option[Long])

object sendFileAttachmentObj {

  val sendFileAttachmentObjReads = (

    (__ \ "responseId").read[Long] and
    (__ \ "reportURL").readNullable[String] and
    (__ \ "PracticeID").readNullable[Long])(sendFileAttachmentObj.apply _)

  val sendFileAttachmentObjWrites = (
    (__ \ "responseId").write[Long] and
    (__ \ "reportURL").writeNullable[String] and
    (__ \ "PracticeID").writeNullable[Long])(unlift(sendFileAttachmentObj.unapply))
  implicit val sendFileAttachmentObjFormat: Format[sendFileAttachmentObj] = Format(sendFileAttachmentObjReads, sendFileAttachmentObjWrites)

}


case class sendAppointmentmailObj(hospitalid: Long, sonographerid: Long,addressconsultid: Long,scheduledate: java.sql.Date)

object sendAppointmentmailObj {

  val sendAppointmentmailObjReads = (
    (__ \ "hospitalid").read[Long] and
    (__ \ "sonographerid").read[Long] and
    (__ \ "addressconsultid").read[Long] and
    (__ \ "scheduledate").read[java.sql.Date])(sendAppointmentmailObj.apply _)

  val sendAppointmentmailObjWrites = (
    (__ \ "hospitalid").write[Long] and
    (__ \ "sonographerid").write[Long] and
    (__ \ "addressconsultid").write[Long] and
    (__ \ "scheduledate").write[java.sql.Date])(unlift(sendAppointmentmailObj.unapply))
  implicit val sendAppointmentmailObjFormat: Format[sendAppointmentmailObj] = Format(sendAppointmentmailObjReads, sendAppointmentmailObjWrites)

}

case class enquiryDetailsObj(patientid: Option[Long], referalcreated: Option[java.sql.Date], referalreceived: Option[java.sql.Date],
                             referalprocessed: Option[java.sql.Date], clinicalnotes: Option[String], clinicaldetails: Option[String],
                             speciality: Option[String], subspeciality: Option[String], appointmenttypeid: Option[String],
                             enquirysourceid: Option[String], enquiryfile: Option[Map[String, String]], crtusr: Long, crtusrtype: String,
                             patientenquiryid: Option[Long], isVoicemail: Option[Boolean], isEmail: Option[Boolean],
                             isLetter: Option[Boolean], manageid: Option[Long], notes: Option[String], enquirystatusid: Option[String],
                             regionid: Option[Long])

object enquiryDetailsObj {
  val enquiryDetailsObjReads = (
    (__ \ "patientid").readNullable[Long] and
    (__ \ "referalcreated").readNullable[java.sql.Date] and
    (__ \ "referalreceived").readNullable[java.sql.Date] and
    (__ \ "referalprocessed").readNullable[java.sql.Date] and
    (__ \ "clinicalnotes").readNullable[String] and
    (__ \ "clinicaldetails").readNullable[String] and
    (__ \ "speciality").readNullable[String] and
    (__ \ "subspeciality").readNullable[String] and
    (__ \ "appointmenttypeid").readNullable[String] and
    (__ \ "enquirysourceid").readNullable[String] and
    (__ \ "enquiryfile").readNullable[Map[String, String]] and
    (__ \ "crtusr").read[Long] and
    (__ \ "crtusrtype").read[String] and
    (__ \ "patientenquiryid").readNullable[Long] and
    (__ \ "isVoicemail").readNullable[Boolean] and
    (__ \ "isEmail").readNullable[Boolean] and
    (__ \ "isLetter").readNullable[Boolean] and
    (__ \ "manageid").readNullable[Long] and
    (__ \ "notes").readNullable[String] and
    (__ \ "enquirystatusid").readNullable[String] and
    (__ \ "regionid").readNullable[Long])(enquiryDetailsObj.apply _)

  val enquiryDetailsObjWrites = (
    (__ \ "patientid").writeNullable[Long] and
    (__ \ "referalcreated").writeNullable[java.sql.Date] and
    (__ \ "referalreceived").writeNullable[java.sql.Date] and
    (__ \ "referalprocessed").writeNullable[java.sql.Date] and
    (__ \ "clinicalnotes").writeNullable[String] and
    (__ \ "clinicaldetails").writeNullable[String] and
    (__ \ "speciality").writeNullable[String] and
    (__ \ "subspeciality").writeNullable[String] and
    (__ \ "appointmenttypeid").writeNullable[String] and
    (__ \ "enquirysourceid").writeNullable[String] and
    (__ \ "enquiryfile").writeNullable[Map[String, String]] and
    (__ \ "crtusr").write[Long] and
    (__ \ "crtusrtype").write[String] and
    (__ \ "patientenquiryid").writeNullable[Long] and
    (__ \ "isVoicemail").writeNullable[Boolean] and
    (__ \ "isEmail").writeNullable[Boolean] and
    (__ \ "isLetter").writeNullable[Boolean] and
    (__ \ "manageid").writeNullable[Long] and
    (__ \ "notes").writeNullable[String] and
    (__ \ "enquirystatusid").writeNullable[String] and
    (__ \ "regionid").writeNullable[Long])(unlift(enquiryDetailsObj.unapply))
  implicit val enquiryDetailsObjFormat: Format[enquiryDetailsObj] = Format(enquiryDetailsObjReads, enquiryDetailsObjWrites)
}

case class hospitalDetailsObj(HospitalID: Option[Long], HospitalCD: Option[String], BranchCD: Option[String], ParentHospitalID: Option[Long])

object hospitalDetailsObj {
  val hospitalDetailsObjReads = (
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "BranchCD").readNullable[String] and
    (__ \ "ParentHospitalID").readNullable[Long])(hospitalDetailsObj.apply _)

  val hospitalDetailsObjWrites = (
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "BranchCD").writeNullable[String] and
    (__ \ "ParentHospitalID").writeNullable[Long])(unlift(hospitalDetailsObj.unapply))
  implicit val hospitalDetailsormat: Format[hospitalDetailsObj] = Format(hospitalDetailsObjReads, hospitalDetailsObjWrites)

}

case class buildScheduleAppointmentList(SeqNo: Int, AppointmentID: Long, AddressConsultID: Long, DayCD: String, ScheduleDate: java.sql.Date,
                                        StartTime: String, EndTime: String, EmergencyStartTime: String, EmergencyEndTime: String,
                                        DoctorID: Option[Long], PatientID: Option[Long], Patientenquiryid: Option[Long],
                                        AppointmentStatus: String, TokenNumber: String, Notes: Option[String], Startscantime: Option[String], Completescantime: Option[String])

object buildScheduleAppointmentList {
  val buildScheduleAppointmentListReads = (
    (__ \ "SeqNo").read[Int] and
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ScheduleDate").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "EmergencyStartTime").read[String] and
    (__ \ "EmergencyEndTime").read[String] and
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "PatientID").readNullable[Long] and
    (__ \ "Patientenquiryid").readNullable[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "TokenNumber").read[String] and
    (__ \ "Notes").readNullable[String] and
    (__ \ "Startscantime").readNullable[String] and
    (__ \ "Completescantime").readNullable[String])(buildScheduleAppointmentList.apply _)

  val buildScheduleAppointmentListWrites = (
    (__ \ "SeqNo").write[Int] and
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ScheduleDate").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "EmergencyStartTime").write[String] and
    (__ \ "EmergencyEndTime").write[String] and
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "PatientID").writeNullable[Long] and
    (__ \ "Patientenquiryid").writeNullable[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "TokenNumber").write[String] and
    (__ \ "Notes").writeNullable[String] and
    (__ \ "Startscantime").writeNullable[String] and
    (__ \ "Completescantime").writeNullable[String])(unlift(buildScheduleAppointmentList.unapply))
  implicit val getResponseFormat: Format[buildScheduleAppointmentList] = Format(buildScheduleAppointmentListReads, buildScheduleAppointmentListWrites)
}

case class getHospitalAppointmentDetailsObj(HospitalCD: Option[String], AddressConsultID: Option[List[Long]], AddressID: Option[Long],DoctorID: Option[Long],
                                            HospitalID: Option[Long], StartDate: java.sql.Date,EndDate: java.sql.Date, Status: Option[String], ParentHospitalID: Option[Long])

object getHospitalAppointmentDetailsObj {

  val getHospitalAppointmentDetailsObjReads = (
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "AddressConsultID").readNullable[List[Long]] and
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "StartDate").read[java.sql.Date] and
    (__ \ "EndDate").read[java.sql.Date] and
    (__ \ "Status").readNullable[String] and
    (__ \ "ParentHospitalID").readNullable[Long])(getHospitalAppointmentDetailsObj.apply _)

  val getHospitalAppointmentDetailsObjWrites = (
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "AddressConsultID").writeNullable[List[Long]] and
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "StartDate").write[java.sql.Date] and
    (__ \ "EndDate").write[java.sql.Date] and
    (__ \ "Status").writeNullable[String] and
    (__ \ "ParentHospitalID").writeNullable[Long])(unlift(getHospitalAppointmentDetailsObj.unapply))
  implicit val getHospitalAppointmentDetailsObjFormat: Format[getHospitalAppointmentDetailsObj] = Format(getHospitalAppointmentDetailsObjReads, getHospitalAppointmentDetailsObjWrites)

}

case class AppointmentStatusUpdateObj(AppointmentID: Long, AppointmentStatus: String, ScanStartTime: Option[String], ScanEndtime: Option[String], Notes: Option[String])

object AppointmentStatusUpdateObj {

  val AppointmentStatusUpdateObjReads = (
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "ScanStartTime").readNullable[String] and
    (__ \ "ScanEndtime").readNullable[String] and
    (__ \ "Notes").readNullable[String])(AppointmentStatusUpdateObj.apply _)

  val AppointmentStatusUpdateObjWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "ScanStartTime").writeNullable[String] and
    (__ \ "ScanEndtime").writeNullable[String] and
    (__ \ "Notes").writeNullable[String])(unlift(AppointmentStatusUpdateObj.unapply))
  implicit val AppointmentStatusUpdateObjFormat: Format[AppointmentStatusUpdateObj] = Format(AppointmentStatusUpdateObjReads, AppointmentStatusUpdateObjWrites)

}

case class appointmentresponseObj(message: String, status: Long)

object appointmentresponseObj {
  val observationressponseObjReads = (
    (__ \ "message").read[String] and
    (__ \ "status").read[Long])(appointmentresponseObj.apply _)

  val observationressponseObjWrites = (
    (__ \ "message").write[String] and
    (__ \ "status").write[Long])(unlift(appointmentresponseObj.unapply))
  implicit val observationressponseObjFormat: Format[appointmentresponseObj] = Format(observationressponseObjReads, observationressponseObjWrites)
}







