/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.AddUpdateSchedule

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime



case class validateConsultationRecord(DayCD: String, StartTime: Int, EndTime: Int,
                                Active: Option[String])

object validateConsultationRecord {
 val validateConsultationRecordReads = (
   (__ \ "DayCD").read[String] and
   (__ \ "StartTime").read[Int] and
   (__ \ "EndTime").read[Int] and
   (__ \ "Active").readNullable[String])(validateConsultationRecord.apply _)

 val validateConsultationRecordWrites = (
   (__ \ "DayCD").write[String] and
   (__ \ "StartTime").write[Int] and
   (__ \ "EndTime").write[Int] and
   (__ \ "Active").writeNullable[String])(unlift(validateConsultationRecord.unapply))
 implicit val validateConsultationFormat: Format[validateConsultationRecord] = Format(validateConsultationRecordReads, validateConsultationRecordWrites)

}

case class addScheduleObj(scheduleRecord: Array[addScheduleRecord], HospitalID: Long,Scheduledateflag: String,Scheduledays: Int,USRId: Int, USRType: String)

object addScheduleObj {
  val addScheduleObjReads = (
    (__ \ "scheduleRecord").read[Array[addScheduleRecord]] and
    (__ \ "HospitalID").read[Long] and
    (__ \ "Scheduledateflag").read[String] and
    (__ \ "Scheduledays").read[Int] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(addScheduleObj.apply _)

  val addScheduleObjWrites = (
    (__ \ "scheduleRecord").write[Array[addScheduleRecord]] and
    (__ \ "HospitalID").write[Long] and
    (__ \ "Scheduledateflag").write[String] and
    (__ \ "Scheduledays").write[Int] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(addScheduleObj.unapply))
  implicit val addConsultationObjFormat: Format[addScheduleObj] = Format(addScheduleObjReads, addScheduleObjWrites)

}

case class validateScheduleRecord(DayCD: String, StartTime: Int, EndTime: Int,
                                 MinPerCase: Option[Int], Fees: Option[Int], Active: Option[String])

object validateScheduleRecord {
  val validateScheduleRecordReads = (
    (__ \ "DayCD").read[String] and
    (__ \ "StartTime").read[Int] and
    (__ \ "EndTime").read[Int] and
    (__ \ "MinPerCase").readNullable[Int] and
    (__ \ "Fees").readNullable[Int] and
    (__ \ "Active").readNullable[String])(validateScheduleRecord.apply _)

  val validateScheduleRecordWrites = (
    (__ \ "DayCD").write[String] and
    (__ \ "StartTime").write[Int] and
    (__ \ "EndTime").write[Int] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "Fees").writeNullable[Int] and
    (__ \ "Active").writeNullable[String])(unlift(validateScheduleRecord.unapply))
  implicit val validateScheduleFormat: Format[validateScheduleRecord] = Format(validateScheduleRecordReads, validateScheduleRecordWrites)

}


case class addScheduleRecord(AddressConsultID: Option[Long],SonographerDoctorID: Option[Long],
                             SupportworkerDoctorID: Option[Long],SonographerAddressID: Option[Long],
                             SupportworkerAddressID: Option[Long],DayCD: String,ScheduleDate: Option[java.sql.Date],
                             MinPerCase: Option[Int],FromTime1: Option[String], ToTime1: Option[String],FromBreakTime1: Option[String],ToBreakTime1: Option[String],FromTime2: Option[String],ToTime2: Option[String], FromBreakTime2: Option[String],ToBreakTime2: Option[String],
                             FromTime3: Option[String],ToTime3: Option[String],FromBreakTime3: Option[String],ToBreakTime3: Option[String],otherdetails:otherdetailObj)

object addScheduleRecord {
  val scheduleRecordReads = (
    (__ \ "AddressConsultID").readNullable[Long] and  
    (__ \ "SonographerDoctorID").readNullable[Long] and
    (__ \ "SupportworkerDoctorID").readNullable[Long] and
    (__ \ "SonographerAddressID").readNullable[Long] and
    (__ \ "SupportworkerAddressID").readNullable[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ScheduleDate").readNullable[java.sql.Date] and
    (__ \ "MinPerCase").readNullable(Reads.min[Int](5)) and
    (__ \ "FromTime1").readNullable[String] and
    (__ \ "ToTime1").readNullable[String] and
    (__ \ "FromBreakTime1").readNullable[String] and
    (__ \ "ToBreakTime1").readNullable[String] and
    (__ \ "FromTime2").readNullable[String] and
    (__ \ "ToTime2").readNullable[String] and
    (__ \ "FromBreakTime2").readNullable[String] and
    (__ \ "ToBreakTime2").readNullable[String] and
    (__ \ "FromTime3").readNullable[String] and
    (__ \ "ToTime3").readNullable[String] and
    (__ \ "FromBreakTime3").readNullable[String] and
    (__ \ "ToBreakTime3").readNullable[String] and
    (__ \ "otherdetails").read[otherdetailObj] )(addScheduleRecord.apply _)

  val scheduleRecordWrites = (
    (__ \ "AddressConsultID").writeNullable[Long] and  
    (__ \ "SonographerDoctorID").writeNullable[Long] and
    (__ \ "SupportworkerDoctorID").writeNullable[Long] and
    (__ \ "SonographerAddressID").writeNullable[Long] and
    (__ \ "SupportworkerAddressID").writeNullable[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ScheduleDate").writeNullable[java.sql.Date] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "FromTime1").writeNullable[String] and
    (__ \ "ToTime1").writeNullable[String] and
    (__ \ "FromBreakTime1").writeNullable[String] and
    (__ \ "ToBreakTime1").writeNullable[String] and
    (__ \ "FromTime2").writeNullable[String] and
    (__ \ "ToTime2").writeNullable[String] and
    (__ \ "FromBreakTime2").writeNullable[String] and
    (__ \ "ToBreakTime2").writeNullable[String] and
    (__ \ "FromTime3").writeNullable[String] and
    (__ \ "ToTime3").writeNullable[String] and
    (__ \ "FromBreakTime3").writeNullable[String] and
    (__ \ "ToBreakTime3").writeNullable[String] and
    (__ \ "otherdetails").write[otherdetailObj])(unlift(addScheduleRecord.unapply))
  implicit val scheduleRecordFormat: Format[addScheduleRecord] = Format(scheduleRecordReads, scheduleRecordWrites)

}


case class delobj(AddressConsultID: Option[Long],Active: Option[String])

object delobj {
  val delobjReads = (
    (__ \ "AddressConsultID").readNullable[Long] and 
    (__ \ "Active").readNullable[String])(delobj.apply _)

  val delobjWrites = ((__ \ "AddressConsultID").writeNullable[Long] and 
    (__ \ "Active").writeNullable[String])(unlift(delobj.unapply))
  implicit val delObjFormat: Format[delobj] = Format(delobjReads, delobjWrites)

}





















case class updateScheduleObj(scheduleRecord: Array[updateScheduleRecord], HospitalID: Long,Scheduledateflag: String,Scheduledays: Int,USRId: Int, USRType: String)

object updateScheduleObj {
  val addScheduleObjReads = (
    (__ \ "scheduleRecord").read[Array[updateScheduleRecord]] and
    (__ \ "HospitalID").read[Long] and
    (__ \ "Scheduledateflag").read[String] and
    (__ \ "Scheduledays").read[Int] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updateScheduleObj.apply _)

  val addScheduleObjWrites = (
    (__ \ "scheduleRecord").write[Array[updateScheduleRecord]] and
    (__ \ "HospitalID").write[Long] and
    (__ \ "Scheduledateflag").write[String] and
    (__ \ "Scheduledays").write[Int] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(updateScheduleObj.unapply))
  implicit val addScheduleObjFormat: Format[updateScheduleObj] = Format(addScheduleObjReads, addScheduleObjWrites)

}

case class getScheduleObj(HospitalID: Long,ScheduleDate: java.sql.Date, USRId: Long, USRType: String)

object getScheduleObj {
  val getScheduleObjReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "ScheduleDate").read[java.sql.Date] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(getScheduleObj.apply _)

  val getScheduleObjWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "ScheduleDate").write[java.sql.Date] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(getScheduleObj.unapply))
  implicit val getScheduleObjFormat: Format[getScheduleObj] = Format(getScheduleObjReads, getScheduleObjWrites)

}



case class getCalendarObj(HospitalID: Long,HospitalName:Option[String],SonographersID:Option[Long],SonographersName:Option[String],FromDate:Option[java.sql.Date],ToDate:Option[java.sql.Date],USRId: Long, USRType: String)

object getCalendarObj {
  val getCalendarObjReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "HospitalName").readNullable[String] and
    (__ \ "SonographersID").readNullable[Long] and
    (__ \ "SonographersName").readNullable[String] and
    (__ \ "FromDate").readNullable[java.sql.Date] and
    (__ \ "ToDate").readNullable[java.sql.Date] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(getCalendarObj.apply _)

  val getCalendarObjWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "HospitalName").writeNullable[String] and
    (__ \ "SonographersID").writeNullable[Long] and
    (__ \ "SonographersName").writeNullable[String] and
    (__ \ "FromDate").writeNullable[java.sql.Date] and
    (__ \ "ToDate").writeNullable[java.sql.Date] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(getCalendarObj.unapply))
  implicit val getCalendarObjFormat: Format[getCalendarObj] = Format(getCalendarObjReads, getCalendarObjWrites)

}


case class getavailappointmentObj(HospitalID: Array[Long],fromDate:Option[java.sql.Date],toDate:Option[java.sql.Date],traigeDate:Option[java.sql.Date],appointmenttype: Option[String],SpecialityCD:Option[Array[String]])

object getavailappointmentObj {
  val getavailappointmentObjReads = (
    (__ \ "HospitalID").read[Array[Long]] and
    (__ \ "fromDate").readNullable[java.sql.Date] and
    (__ \ "toDate").readNullable[java.sql.Date] and
    (__ \ "traigeDate").readNullable[java.sql.Date] and
    (__ \ "appointmenttype").readNullable[String] and
    (__ \ "SpecialityCD").readNullable[Array[String]])(getavailappointmentObj.apply _)

  val getavailappointmentObjWrites = (
    (__ \ "HospitalID").write[Array[Long]] and
    (__ \ "fromDate").writeNullable[java.sql.Date] and
    (__ \ "toDate").writeNullable[java.sql.Date] and
    (__ \ "traigeDate").writeNullable[java.sql.Date] and
    (__ \ "appointmenttype").writeNullable[String] and
    (__ \ "SpecialityCD").writeNullable[Array[String]])(unlift(getavailappointmentObj.unapply))
  implicit val getCalendarObjFormat: Format[getavailappointmentObj] = Format(getavailappointmentObjReads, getavailappointmentObjWrites)

}


case class updateScheduleRecord(AddressConsultID: Option[Long],SonographerDoctorID: Option[Long],
                             SupportworkerDoctorID: Option[Long],SonographerAddressID: Option[Long],
                             SupportworkerAddressID: Option[Long],DayCD: String,ScheduleDate: Option[java.sql.Date],
                             MinPerCase: Option[Int],FromTime1: Option[String], ToTime1: Option[String],FromBreakTime1: Option[String],ToBreakTime1: Option[String],FromTime2: Option[String],ToTime2: Option[String], FromBreakTime2: Option[String],ToBreakTime2: Option[String],
                             FromTime3: Option[String],ToTime3: Option[String],FromBreakTime3: Option[String],ToBreakTime3: Option[String],otherdetails:otherdetailObj)

object updateScheduleRecord {
  val updateScheduleObjReads = (
    (__ \ "AddressConsultID").readNullable[Long] and  
    (__ \ "SonographerDoctorID").readNullable[Long] and
    (__ \ "SupportworkerDoctorID").readNullable[Long] and
    (__ \ "SonographerAddressID").readNullable[Long] and
    (__ \ "SupportworkerAddressID").readNullable[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ScheduleDate").readNullable[java.sql.Date] and
    (__ \ "MinPerCase").readNullable(Reads.min[Int](5)) and
    (__ \ "FromTime1").readNullable[String] and
    (__ \ "ToTime1").readNullable[String] and
    (__ \ "FromBreakTime1").readNullable[String] and
    (__ \ "ToBreakTime1").readNullable[String] and
    (__ \ "FromTime2").readNullable[String] and
    (__ \ "ToTime2").readNullable[String] and
    (__ \ "FromBreakTime2").readNullable[String] and
    (__ \ "ToBreakTime2").readNullable[String] and
    (__ \ "FromTime3").readNullable[String] and
    (__ \ "ToTime3").readNullable[String] and
    (__ \ "FromBreakTime3").readNullable[String] and
    (__ \ "ToBreakTime3").readNullable[String] and
    (__ \ "otherdetails").read[otherdetailObj]  )(updateScheduleRecord.apply _)

  val updateScheduleObjWrites = (
    (__ \ "AddressConsultID").writeNullable[Long] and  
    (__ \ "SonographerDoctorID").writeNullable[Long] and
    (__ \ "SupportworkerDoctorID").writeNullable[Long] and
    (__ \ "SonographerAddressID").writeNullable[Long] and
    (__ \ "SupportworkerAddressID").writeNullable[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ScheduleDate").writeNullable[java.sql.Date] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "FromTime1").writeNullable[String] and
    (__ \ "ToTime1").writeNullable[String] and
    (__ \ "FromBreakTime1").writeNullable[String] and
    (__ \ "ToBreakTime1").writeNullable[String] and
    (__ \ "FromTime2").writeNullable[String] and
    (__ \ "ToTime2").writeNullable[String] and
    (__ \ "FromBreakTime2").writeNullable[String] and
    (__ \ "ToBreakTime2").writeNullable[String] and
    (__ \ "FromTime3").writeNullable[String] and
    (__ \ "ToTime3").writeNullable[String] and
    (__ \ "FromBreakTime3").writeNullable[String] and
    (__ \ "ToBreakTime3").writeNullable[String] and
    (__ \ "otherdetails").write[otherdetailObj])(unlift(updateScheduleRecord.unapply))
  implicit val updateScheduleObjFormat: Format[updateScheduleRecord] = Format(updateScheduleObjReads, updateScheduleObjWrites)

}

case class otherdetailObj(Slot: String,RoomCostStatus: String,BookFlag: String,Active: String)

object otherdetailObj {
  val otherdetailObjReads = (
    (__ \ "Slot").read[String] and
    (__ \ "RoomCostStatus").read[String] and
    (__ \ "BookFlag").read[String] and
    (__ \ "Active").read[String])(otherdetailObj.apply _)

  val otherdetailObjWrites = (
    (__ \ "Slot").write[String] and
    (__ \ "RoomCostStatus").write[String] and
    (__ \ "BookFlag").write[String] and
    (__ \ "Active").write[String])(unlift(otherdetailObj.unapply))
  implicit val getScheduleObjFormat: Format[otherdetailObj] = Format(otherdetailObjReads, otherdetailObjWrites)

}

case class listAllScheduleObj(sonographerid:Option[Long],supportworkerid: Option[Long],sonographername: Option[String],supportworkername:Option[String],
    hospitalid: Option[Long],hospitalname:Option[String],scheduledate: Option[java.sql.Date],scheduledetail:Option[JsValue],doctordetails:Option[JsValue],districtregion:Option[Long], regionname:Option[String])

object listAllScheduleObj {
  val listAllScheduleObjReads = (
    (__ \ "sonographerid").readNullable[Long] and
    (__ \ "supportworkerid").readNullable[Long] and
    (__ \ "sonographername").readNullable[String] and
    (__ \ "supportworkername").readNullable[String] and
    (__ \ "hospitalid").readNullable[Long] and
    (__ \ "hospitalname").readNullable[String] and
    (__ \ "scheduledate").readNullable[java.sql.Date] and
    (__ \ "scheduledetail").readNullable[JsValue] and
    (__ \ "doctordetails").readNullable[JsValue] and 
    (__ \ "districtregion").readNullable[Long] and 
    (__ \ "regionname").readNullable[String])(listAllScheduleObj.apply _)

  val listAllScheduleObjWrites = (
    (__ \ "sonographerid").writeNullable[Long] and
    (__ \ "supportworkerid").writeNullable[Long] and
    (__ \ "sonographername").writeNullable[String] and
    (__ \ "supportworkername").writeNullable[String] and
    (__ \ "hospitalid").writeNullable[Long] and
    (__ \ "hospitalname").writeNullable[String] and
    (__ \ "scheduledate").writeNullable[java.sql.Date] and
    (__ \ "scheduledetail").writeNullable[JsValue] and
    (__ \ "doctordetails").writeNullable[JsValue] and 
    (__ \ "districtregion").writeNullable[Long]and 
    (__ \ "regionname").writeNullable[String])(unlift(listAllScheduleObj.unapply))
  implicit val listAllScheduleObjFormat: Format[listAllScheduleObj] = Format(listAllScheduleObjReads, listAllScheduleObjWrites)

}


case class listAllApppointmentResObj(scheduledate:Option[java.sql.Date],availableslotslist: Option[JsValue])

object listAllApppointmentResObj {
  val listAllApppointmentResObjReads = (
    (__ \ "scheduledate").readNullable[java.sql.Date] and
    (__ \ "availableslotslist").readNullable[JsValue])(listAllApppointmentResObj.apply _)

  val listAllApppointmentResObjWrites = (
    (__ \ "scheduledate").writeNullable[java.sql.Date] and
    (__ \ "availableslotslist").writeNullable[JsValue])(unlift(listAllApppointmentResObj.unapply))
  implicit val listAllApppointmentResObjFormat: Format[listAllApppointmentResObj] = Format(listAllApppointmentResObjReads, listAllApppointmentResObjWrites)

}

case class listAllCalendarObj(addressconsultid:Option[Long],hospitalname: Option[String],sonographerid: Option[Long],
    supportworkerid: Option[Long],sonographername: Option[String],supportworkername: Option[String], minpercaseval: Option[Int],time1: Option[JsValue],time2: Option[JsValue],
    totalslot: Option[Long],ScheduledDate:Option[java.sql.Date],BreakTime1: Option[JsValue],BreakTime2: Option[JsValue],BreakTime3: Option[JsValue])

object listAllCalendarObj {
  val listAllCalendarObjReads = (
    (__ \ "addressconsultid").readNullable[Long] and
    (__ \ "hospitalname").readNullable[String] and
    (__ \ "sonographerid").readNullable[Long] and
    (__ \ "supportworkerid").readNullable[Long] and
    (__ \ "sonographername").readNullable[String] and
    (__ \ "supportworkername").readNullable[String] and
    (__ \ "minpercaseval").readNullable[Int] and
    (__ \ "time1").readNullable[JsValue] and
    (__ \ "time2").readNullable[JsValue] and
    (__ \ "totalslot").readNullable[Long] and
    (__ \ "ScheduledDate").readNullable[java.sql.Date] and
    (__ \ "BreakTime1").readNullable[JsValue] and
    (__ \ "BreakTime2").readNullable[JsValue] and
    (__ \ "BreakTime3").readNullable[JsValue])(listAllCalendarObj.apply _)

  val listAllCalendarObjWrites = (
    (__ \ "addressconsultid").writeNullable[Long] and
    (__ \ "hospitalname").writeNullable[String] and
    (__ \ "sonographerid").writeNullable[Long] and
    (__ \ "supportworkerid").writeNullable[Long] and
    (__ \ "sonographername").writeNullable[String] and
    (__ \ "supportworkername").writeNullable[String] and
    (__ \ "minpercaseval").writeNullable[Int] and
    (__ \ "time1").writeNullable[JsValue] and
    (__ \ "time2").writeNullable[JsValue] and
    (__ \ "totalslot").writeNullable[Long] and
    (__ \ "ScheduledDate").writeNullable[java.sql.Date] and
    (__ \ "BreakTime1").writeNullable[JsValue] and
    (__ \ "BreakTime2").writeNullable[JsValue] and
    (__ \ "BreakTime3").writeNullable[JsValue])(unlift(listAllCalendarObj.unapply))
  implicit val listAllCalendarObjFormat: Format[listAllCalendarObj] = Format(listAllCalendarObjReads, listAllCalendarObjWrites)

}



