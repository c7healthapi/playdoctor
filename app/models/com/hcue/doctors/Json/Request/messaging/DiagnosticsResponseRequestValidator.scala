package models.com.hcue.doctors.Json.Request.messaging

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation._

case class diagnosticsrequestObj(responseid: Long, requestid: Long, patientid: Long,
                      appointmentid: Long, nhsnumber: Long, referralid: Long, findings: String,
                      history: String, impressions: String, reportor: String, reportstatus: String,
                      examdate: String, reportdate: String,exam:String)

object diagnosticsrequestObj {
  val diagnosticsrequestObjReads = (
    (__ \ "responseid").read[Long] and
    (__ \ "requestid").read[Long] and
    (__ \ "patientid").read[Long] and
    (__ \ "appointmentid").read[Long] and
    (__ \ "nhsnumber").read[Long] and
    (__ \ "referralid").read[Long] and
    (__ \ "findings").read[String] and
    (__ \ "history").read[String] and
    (__ \ "impressions").read[String] and
    (__ \ "reportor").read[String] and
    (__ \ "reportstatus").read[String] and
    (__ \ "examdate").read[String] and
    (__ \ "reportdate").read[String] and
    (__ \ "exam").read[String])(diagnosticsrequestObj.apply _)

  val diagnosticsrequestObjWrites = (
    (__ \ "responseid").write[Long] and
    (__ \ "requestid").write[Long] and
    (__ \ "patientid").write[Long] and
    (__ \ "appointmentid").write[Long] and
    (__ \ "nhsnumber").write[Long] and
    (__ \ "referralid").write[Long] and
    (__ \ "findings").write[String] and
    (__ \ "history").write[String] and
    (__ \ "impressions").write[String] and
    (__ \ "reportor").write[String] and
    (__ \ "reportstatus").write[String] and
    (__ \ "examdate").write[String] and
    (__ \ "reportdate").write[String] and
    (__ \ "exam").write[String]
)(unlift(diagnosticsrequestObj.unapply))
  implicit val observationrequestObjFormat: Format[diagnosticsrequestObj] = Format(diagnosticsrequestObjReads, diagnosticsrequestObjWrites)
}


case class messagelogobject(messageid: Long, deliverycode : String, drlmessage: String)

object messagelogobject {
  val messagelogobjectReads = (
    (__ \ "messageid").read[Long] and
    (__ \ "deliverycode").read[String] and
    (__ \ "drlmessage").read[String])(messagelogobject.apply _)

  val messagelogobjectWrites = (
    (__ \ "messageid").write[Long] and
    (__ \ "deliverycode").write[String] and
    (__ \ "drlmessage").write[String])(unlift(messagelogobject.unapply))
  implicit val messagelogobjectFormat: Format[messagelogobject] = Format(messagelogobjectReads, messagelogobjectWrites)
}

case class AppointmentSMSMessageObj(AppointmentID: Long, AppointmentStatus: String, PatientID: Long,
                                    HospitalID: Long, Message: String, MessageType: String,
                                    Destination: Long, DateTime: Option[String], 
                                    DeliveryType: String,CrtUSR: Long, CrtUSRType: String)

object AppointmentSMSMessageObj {
  val AppointmentSMSMessageObjReads = (
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "PatientID").read[Long] and
    (__ \ "HospitalID").read[Long] and
    (__ \ "Message").read[String] and
    (__ \ "MessageType").read[String] and
    (__ \ "Destination").read[Long] and
    (__ \ "DateTime").readNullable[String] and
    (__ \ "DeliveryType").read[String] and
    (__ \ "CrtUSR").read[Long] and
    (__ \ "CrtUSRType").read[String])(AppointmentSMSMessageObj.apply _)

  val AppointmentSMSMessageObjWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "PatientID").write[Long] and
    (__ \ "HospitalID").write[Long] and
    (__ \ "Message").write[String] and
    (__ \ "MessageType").write[String] and
    (__ \ "Destination").write[Long] and
    (__ \ "DateTime").writeNullable[String] and
    (__ \ "DeliveryType").write[String] and
    (__ \ "CrtUSR").write[Long] and
    (__ \ "CrtUSRType").write[String])(unlift(AppointmentSMSMessageObj.unapply))
  implicit val AppointmentSMSMessageObjFormat: Format[AppointmentSMSMessageObj] = Format(AppointmentSMSMessageObjReads, AppointmentSMSMessageObjWrites)
}