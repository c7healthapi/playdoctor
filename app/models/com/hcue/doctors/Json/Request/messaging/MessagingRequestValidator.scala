package models.com.hcue.doctors.Json.Request.messaging

import java.sql.Date

import models.com.hcue.doctors.slick.MyPostgresDriver.simple._
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class observationrequestObj(patientid: String, obrsetid: String, obrplacerordernumber: String,
                                 obrfillerordernumber: String, obrunivserviceidentifier: String, obrrequesteddatetime: String, obrobservationdatetime: String,
                                 obrorderingprovider: String, obrresultstatuschgdatetime: String, obrdiagservicesecretid: String, obrresultstatus: String,
                                 obrresultinterpreter: String, obrtranscriptionist: String, observationresult: Option[Array[observationresultObj]])

object observationrequestObj {
  val observationrequestObjReads = (
    (__ \ "patientid").read[String] and
    (__ \ "obrsetid").read[String] and
    (__ \ "obrplacerordernumber").read[String] and
    (__ \ "obrfillerordernumber").read[String] and
    (__ \ "obrunivserviceidentifier").read[String] and
    (__ \ "obrrequesteddatetime").read[String] and
    (__ \ "obrobservationdatetime").read[String] and
    (__ \ "obrorderingprovider").read[String] and
    (__ \ "obrresultstatuschgdatetime").read[String] and
    (__ \ "obrdiagservicesecretid").read[String] and
    (__ \ "obrresultstatus").read[String] and
    (__ \ "obrresultinterpreter").read[String] and
    (__ \ "obrtranscriptionist").read[String] and
    (__ \ "observationresult").readNullable[Array[observationresultObj]])(observationrequestObj.apply _)

  val observationrequestObjWrites = (
    (__ \ "patientid").write[String] and
    (__ \ "obrsetid").write[String] and
    (__ \ "obrplacerordernumber").write[String] and
    (__ \ "obrfillerordernumber").write[String] and
    (__ \ "obrunivserviceidentifier").write[String] and
    (__ \ "obrrequesteddatetime").write[String] and
    (__ \ "obrobservationdatetime").write[String] and
    (__ \ "obrorderingprovider").write[String] and
    (__ \ "obrresultstatuschgdatetime").write[String] and
    (__ \ "obrdiagservicesecretid").write[String] and
    (__ \ "obrresultstatus").write[String] and
    (__ \ "obrresultinterpreter").write[String] and
    (__ \ "obrtranscriptionist").write[String] and
    (__ \ "observationresult").writeNullable[Array[observationresultObj]])(unlift(observationrequestObj.unapply))
  implicit val observationrequestObjFormat: Format[observationrequestObj] = Format(observationrequestObjReads, observationrequestObjWrites)
}

case class observationresultObj(PatientID: String, OBXSetID: String, OBXValueType: String, OBXIdentifier: String,
                                OBXSubId: String, OBXValue: String, OBXResultStatus: String, OBXEffectiveDateRefRange: String,
                                OBXUserDefAccessCheck: String, OBXObservationDateTime: String, OBXProducerId: String, OBXResObserver: String)

object observationresultObj {
  val observationresultObjReads = (
    (__ \ "PatientID").read[String] and
    (__ \ "OBXSetID").read[String] and
    (__ \ "OBXValueType").read[String] and
    (__ \ "OBXIdentifier").read[String] and
    (__ \ "OBXSubId").read[String] and
    (__ \ "OBXValue").read[String] and
    (__ \ "OBXResultStatus").read[String] and
    (__ \ "OBXEffectiveDateRefRange").read[String] and
    (__ \ "OBXUserDefAccessCheck").read[String] and
    (__ \ "OBXObservationDateTime").read[String] and
    (__ \ "OBXProducerId").read[String] and
    (__ \ "OBXResObserver").read[String])(observationresultObj.apply _)

  val observationresultObjWrites = (
    (__ \ "PatientID").write[String] and
    (__ \ "OBXSetID").write[String] and
    (__ \ "OBXValueType").write[String] and
    (__ \ "OBXIdentifier").write[String] and
    (__ \ "OBXSubId").write[String] and
    (__ \ "OBXValue").write[String] and
    (__ \ "OBXResultStatus").write[String] and
    (__ \ "OBXEffectiveDateRefRange").write[String] and
    (__ \ "OBXUserDefAccessCheck").write[String] and
    (__ \ "OBXObservationDateTime").write[String] and
    (__ \ "OBXProducerId").write[String] and
    (__ \ "OBXResObserver").write[String])(unlift(observationresultObj.unapply))
  implicit val observationresultObjFormat: Format[observationresultObj] = Format(observationresultObjReads, observationresultObjWrites)
}

case class observationressponseObj(status: String, observationid: Long)

object observationressponseObj {
  val observationressponseObjReads = (
    (__ \ "status").read[String] and
    (__ \ "observationid").read[Long])(observationressponseObj.apply _)

  val observationressponseObjWrites = (
    (__ \ "message").write[String] and
    (__ \ "observationid").write[Long])(unlift(observationressponseObj.unapply))
  implicit val observationressponseObjFormat: Format[observationressponseObj] = Format(observationressponseObjReads, observationressponseObjWrites)
}

case class postMeshMessageObj(hl7ReferenceID: Long, toMailBoxID: String, usrid: Long, usrtype: String)

object postMeshMessageObj {
  val postMeshMessageObjReads = (
    (__ \ "hl7ReferenceID").read[Long] and
    (__ \ "toMailBoxID").read[String] and
    (__ \ "usrid").read[Long] and
    (__ \ "usrtype").read[String])(postMeshMessageObj.apply _)

  val postMeshMessageObjWrites = (
    (__ \ "hl7ReferenceID").write[Long] and
    (__ \ "toMailBoxID").write[String] and
    (__ \ "usrid").write[Long] and
    (__ \ "usrtype").write[String])(unlift(postMeshMessageObj.unapply))
  implicit val postMeshMessageObjFormat: Format[postMeshMessageObj] = Format(postMeshMessageObjReads, postMeshMessageObjWrites)
}

case class listObjHL7Message(AppointmentID : Long, AppointmentDate: Date, PatientName: String, PatientID: Long, NHSNumber: String,
                             ScanCentre: String, Practice: String, CCG: String, MessageType : String , Status: String, Time : String)

object listObjHL7Message {
  val listObjHL7MessageReads = (
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AppointmentDate").read[Date] and
    (__ \ "PatientName").read[String] and
    (__ \ "PatientID").read[Long] and
    (__ \ "NHSNumber").read[String] and
    (__ \ "ScanCentre").read[String] and
    (__ \ "Practice").read[String] and
    (__ \ "CCG").read[String] and
    (__ \ "MessageType").read[String] and
    (__ \ "Status").read[String] and
    (__ \ "Time").read[String])(listObjHL7Message.apply _)

  val listObjHL7MessageWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AppointmentDate").write[Date] and
    (__ \ "PatientName").write[String] and
    (__ \ "PatientID").write[Long] and
    (__ \ "NHSNumber").write[String] and
    (__ \ "ScanCentre").write[String] and
    (__ \ "Practice").write[String] and
    (__ \ "CCG").write[String] and
    (__ \ "MessageType").write[String] and
    (__ \ "Status").write[String] and
    (__ \ "Time").write[String])(unlift(listObjHL7Message.unapply))
  implicit val listObjHL7MessageFormat: Format[listObjHL7Message] = Format(listObjHL7MessageReads, listObjHL7MessageWrites)
}

case class getHL7Obj(HospitalID: Option[Long], Date: String, pageNumber: Option[Int], pageSize: Option[Int])

object getHL7Obj {
  val getHL7ObjReads = (
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "Date").read[String] and
    (__ \ "pageNumber").readNullable[Int] and
    (__ \ "pageSize").readNullable[Int])(getHL7Obj.apply _)

  val getHL7ObjWrites = (
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "Date").write[String]and
    (__ \ "pageNumber").writeNullable[Int] and
    (__ \ "pageSize").writeNullable[Int])(unlift(getHL7Obj.unapply))
  implicit val getHL7ObjFormat: Format[getHL7Obj] = Format(getHL7ObjReads, getHL7ObjWrites)

}

case class listHL7Response(arrListObjHL7Message: Array[listObjHL7Message], Count: Option[Long])

object listHL7Response {
  val listHL7ResponseReads = (
    (__ \ "arrListObjHL7Message").read[Array[listObjHL7Message]] and
    (__ \ "Count").readNullable[Long])(listHL7Response.apply _)

  val listHL7ResponseWrites = (
    (__ \ "arrListObjHL7Message").write[Array[listObjHL7Message]] and
    (__ \ "Count").writeNullable[Long])(unlift(listHL7Response.unapply))
  implicit val listHL7ResponseWritesFormat: Format[listHL7Response] = Format(listHL7ResponseReads, listHL7ResponseWrites)

}
