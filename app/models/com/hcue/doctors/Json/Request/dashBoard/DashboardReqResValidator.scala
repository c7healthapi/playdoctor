package models.com.hcue.doctors.Json.Request.dashBoard

import play.api.libs.functional.syntax._
import play.api.libs.json._
import java.sql.Date
import models.com.hcue.doctors.Json.Request.doctorCommStatusObj

object DashboardReqResValidator {

  case class DashboardInfoTypeObj(totalOutstanding: Double, totalCashReceived: Double, totalPotential: Int, potentialWorth: Double,
                                  newPatientVisit: Int, followUpPatientVisit: Int, raisedlabOrders: Int, deliveredlabOrders: Int, pendinglabOrders: Int,
                                  topTrendDoctors: Array[EachRecordObj], topTrendCosultants: Array[EachRecordObj], topTrendBranches: Array[EachRecordObj], topTrendTreatments: Array[EachRecordObj], averageConsultationTime: String,
                                  totalExpenses: Option[Double], topRatedDoctors: Array[EachRecordObj], cancelledPatients: Int, doctorAppointmentDetails: Option[dctrAptmtDtls])

  object DashboardInfoTypeObj {
    val DashboardInfoTypeObjReads = (
      (__ \ "totalOutstanding").read[Double] and
      (__ \ "totalCashReceived").read[Double] and
      (__ \ "totalPotential").read[Int] and
      (__ \ "potentialWorth").read[Double] and
      (__ \ "newPatientVisit").read[Int] and
      (__ \ "followUpPatientVisit").read[Int] and
      (__ \ "raisedlabOrders").read[Int] and
      (__ \ "deliveredlabOrders").read[Int] and
      (__ \ "pendinglabOrders").read[Int] and
      (__ \ "topTrendDoctors").read[Array[EachRecordObj]] and
      (__ \ "topTrendCosultants").read[Array[EachRecordObj]] and
      (__ \ "topTrendBranches").read[Array[EachRecordObj]] and
      (__ \ "topTrendTreatments").read[Array[EachRecordObj]] and
      (__ \ "averageConsultationTime").read[String] and
      (__ \ "totalExpenses").readNullable[Double] and
      (__ \ "topRatedDoctors").read[Array[EachRecordObj]] and
      (__ \ "cancelledPatients").read[Int] and
      (__ \ "doctorAppointmentDetails").readNullable[dctrAptmtDtls])(DashboardInfoTypeObj.apply _)

    val DashboardInfoTypeObjWrites = (
      (__ \ "totalOutstanding").write[Double] and
      (__ \ "totalCashReceived").write[Double] and
      (__ \ "totalPotential").write[Int] and
      (__ \ "potentialWorth").write[Double] and
      (__ \ "newPatientVisit").write[Int] and
      (__ \ "followUpPatientVisit").write[Int] and
      (__ \ "raisedlabOrders").write[Int] and
      (__ \ "deliveredlabOrders").write[Int] and
      (__ \ "pendinglabOrders").write[Int] and
      (__ \ "topTrendDoctors").write[Array[EachRecordObj]] and
      (__ \ "topTrendCosultants").write[Array[EachRecordObj]] and
      (__ \ "topTrendBranches").write[Array[EachRecordObj]] and
      (__ \ "topTrendTreatments").write[Array[EachRecordObj]] and
      (__ \ "averageConsultationTime").write[String] and
      (__ \ "totalExpenses").writeNullable[Double] and
      (__ \ "topRatedDoctors").write[Array[EachRecordObj]] and
      (__ \ "cancelledPatients").write[Int] and
      (__ \ "doctorAppointmentDetails").writeNullable[dctrAptmtDtls])(unlift(DashboardInfoTypeObj.unapply))
    implicit val DashboardInfoTypeObjFormat: Format[DashboardInfoTypeObj] = Format(DashboardInfoTypeObjReads, DashboardInfoTypeObjWrites)
  }

  case class NewDashboardInfoTypeObj(patientVisits: Option[patientVisitsDtlsObj], smsDetails: Option[doctorCommStatusObj], totalAmountObj: Option[totalAmountObj],
                                     topTrenDoctors: Option[Array[EachRecordObj]], topTrendCosultants: Option[Array[EachRecordObj]], topTrendBranches: Option[Array[EachRecordObj]],
                                     topTrendTreatments: Option[Array[EachRecordObj]], averageConsultationTime: Option[String],
                                     topRatedDoctors: Option[Array[EachRecordObj]], doctorAppointmentDetails: Option[dctrAptmtDtls], outstandingDetails: Option[outstandingObjDtls],
                                     pharmaLeadsCount: Option[Array[EachRecordObj]], expensesCost: Option[Array[EachRecordObj]], revenueTarget: Option[revenueTargetObj],
                                     revenueRecievedDetails: Option[RevenueObjDtls], totalTreatmentCount: Option[Long],
                                     feedbackDtls: Option[Array[EachRecordObj]])

  object NewDashboardInfoTypeObj {
    val NewDashboardInfoTypeObjReads = (
      (__ \ "patientVisits").readNullable[patientVisitsDtlsObj] and
      (__ \ "smsDetails").readNullable[doctorCommStatusObj] and
      (__ \ "totalAmountObj").readNullable[totalAmountObj] and
      (__ \ "topTrenDoctors").readNullable[Array[EachRecordObj]] and
      (__ \ "topTrendCosultants").readNullable[Array[EachRecordObj]] and
      (__ \ "topTrendBranches").readNullable[Array[EachRecordObj]] and
      (__ \ "topTrendTreatments").readNullable[Array[EachRecordObj]] and
      (__ \ "averageConsultationTime").readNullable[String] and
      (__ \ "topRatedDoctors").readNullable[Array[EachRecordObj]] and
      (__ \ "doctorAppointmentDetails").readNullable[dctrAptmtDtls] and
      (__ \ "outstandingDetails").readNullable[outstandingObjDtls] and
      (__ \ "pharmaLeadsCount").readNullable[Array[EachRecordObj]] and
      (__ \ "expensesCost").readNullable[Array[EachRecordObj]] and
      (__ \ "revenueTarget").readNullable[revenueTargetObj] and
      (__ \ "revenueRecievedDetails").readNullable[RevenueObjDtls] and
      (__ \ "totalTreatmentCount").readNullable[Long] and
      (__ \ "feedbackDetails").readNullable[Array[EachRecordObj]])(NewDashboardInfoTypeObj.apply _)

    val NewDashboardInfoTypeObjWrites = (
      (__ \ "patientVisits").writeNullable[patientVisitsDtlsObj] and
      (__ \ "smsDetails").writeNullable[doctorCommStatusObj] and
      (__ \ "totalAmountObj").writeNullable[totalAmountObj] and
      (__ \ "topTrenDoctors").writeNullable[Array[EachRecordObj]] and
      (__ \ "topTrendCosultants").writeNullable[Array[EachRecordObj]] and
      (__ \ "topTrendBranches").writeNullable[Array[EachRecordObj]] and
      (__ \ "topTrendTreatments").writeNullable[Array[EachRecordObj]] and
      (__ \ "averageConsultationTime").writeNullable[String] and
      (__ \ "topRatedDoctors").writeNullable[Array[EachRecordObj]] and
      (__ \ "doctorAppointmentDetails").writeNullable[dctrAptmtDtls] and
      (__ \ "outstandingDetails").writeNullable[outstandingObjDtls] and
      (__ \ "pharmaLeadsCount").writeNullable[Array[EachRecordObj]] and
      (__ \ "expensesCost").writeNullable[Array[EachRecordObj]] and
      (__ \ "revenueTarget").writeNullable[revenueTargetObj] and
      (__ \ "revenueRecievedDetails").writeNullable[RevenueObjDtls] and
      (__ \ "totalTreatmentCount").writeNullable[Long] and
      (__ \ "feedbackDetails").writeNullable[Array[EachRecordObj]])(unlift(NewDashboardInfoTypeObj.unapply))
    implicit val NewDashboardInfoTypeObjFormat: Format[NewDashboardInfoTypeObj] = Format(NewDashboardInfoTypeObjReads, NewDashboardInfoTypeObjWrites)
  }

  case class revenueTargetObj(hospitalID: Long, totalRevenue: Option[Double], totalRevenueAmt: Option[Double], totalProjectedRevenueAmt: Option[Double],
                              totalProjectedRevenueAmountTillDate: Option[Double], targetAchievedPercent: Option[Double], UpcomingDailyTargettoBeAchieved: Option[Double],
                              targetType: Option[String])

  object revenueTargetObj {
    val revenueTargetObjReads = (
      (__ \ "hospitalID").read[Long] and
      (__ \ "totalRevenueAmt").readNullable[Double] and
      (__ \ "totalProjectedRevenueAmt").readNullable[Double] and
      (__ \ "totalProjectedRevenueAmountTillDate").readNullable[Double] and
      (__ \ "targetRevenueAmt").readNullable[Double] and
      (__ \ "targetAchievedPercent").readNullable[Double] and
      (__ \ "UpcomingDailyTargettoBeAchieved").readNullable[Double] and
      (__ \ "targetType").readNullable[String])(revenueTargetObj.apply _)

    val revenueTargetObjWrites = (
      (__ \ "hospitalID").write[Long] and
      (__ \ "totalRevenueAmt").writeNullable[Double] and
      (__ \ "totalProjectedRevenueAmt").writeNullable[Double] and
      (__ \ "totalProjectedRevenueAmountTillDate").writeNullable[Double] and
      (__ \ "targetRevenue").writeNullable[Double] and
      (__ \ "targetAchievedPercent").writeNullable[Double] and
      (__ \ "UpcomingDailyTargettoBeAchieved").writeNullable[Double] and
      (__ \ "targetType").writeNullable[String])(unlift(revenueTargetObj.unapply))
    implicit val EachRecordObjFormat: Format[revenueTargetObj] = Format(revenueTargetObjReads, revenueTargetObjWrites)
  }

  case class outstandingObjDtls(doctorWise: Option[Array[EachRecordObj]], treatmentWise: Option[Array[EachRecordObj]], branchWise: Option[Array[EachRecordObj]])

  object outstandingObjDtls {
    val outstandingObjDtlsReads = (
      (__ \ "doctorWise").readNullable[Array[EachRecordObj]] and
      (__ \ "treatmentWise").readNullable[Array[EachRecordObj]] and
      (__ \ "branchWise").readNullable[Array[EachRecordObj]])(outstandingObjDtls.apply _)

    val outstandingObjDtlsWrites = (
      (__ \ "doctorWise").writeNullable[Array[EachRecordObj]] and
      (__ \ "treatmentWise").writeNullable[Array[EachRecordObj]] and
      (__ \ "branchWise").writeNullable[Array[EachRecordObj]])(unlift(outstandingObjDtls.unapply))
    implicit val EachRecordObjFormat: Format[outstandingObjDtls] = Format(outstandingObjDtlsReads, outstandingObjDtlsWrites)
  }

  case class RevenueObjDtls(doctorWise: Option[Array[EachRecordObj]], treatmentWise: Option[Array[EachRecordObj]], branchWise: Option[Array[EachRecordObj]])

  object RevenueObjDtls {
    val RevenueObjDtlsReads = (
      (__ \ "doctorWise").readNullable[Array[EachRecordObj]] and
      (__ \ "treatmentWise").readNullable[Array[EachRecordObj]] and
      (__ \ "branchWise").readNullable[Array[EachRecordObj]])(RevenueObjDtls.apply _)

    val RevenueObjDtlsWrites = (
      (__ \ "doctorWise").writeNullable[Array[EachRecordObj]] and
      (__ \ "treatmentWise").writeNullable[Array[EachRecordObj]] and
      (__ \ "branchWise").writeNullable[Array[EachRecordObj]])(unlift(RevenueObjDtls.unapply))
    implicit val EachRecordObjFormat: Format[RevenueObjDtls] = Format(RevenueObjDtlsReads, RevenueObjDtlsWrites)
  }

  case class totalAmountObj(totalOutstanding: Option[Double], totalCashReceived: Option[Double], totalDocReferralCount: Option[Int], totalExpenses: Option[Double])

  object totalAmountObj {
    val totalAmountObjReads = (
      (__ \ "totalOutstanding").readNullable[Double] and
      (__ \ "totalCashReceived").readNullable[Double] and
      (__ \ "totalDocReferralCount").readNullable[Int] and
      (__ \ "totalExpenses").readNullable[Double])(totalAmountObj.apply _)

    val totalAmountObjWrites = (
      (__ \ "totalOutstanding").writeNullable[Double] and
      (__ \ "totalCashReceived").writeNullable[Double] and
      (__ \ "totalDocReferralCount").writeNullable[Int] and
      (__ \ "totalExpenses").writeNullable[Double])(unlift(totalAmountObj.unapply))
    implicit val EachRecordObjFormat: Format[totalAmountObj] = Format(totalAmountObjReads, totalAmountObjWrites)
  }

  case class EachRecordObj(key: String, value: String)

  object EachRecordObj {
    val EachRecordObjReads = (
      (__ \ "key").read[String] and
      (__ \ "value").read[String])(EachRecordObj.apply _)

    val EachRecordObjWrites = (
      (__ \ "key").write[String] and
      (__ \ "value").write[String])(unlift(EachRecordObj.unapply))
    implicit val EachRecordObjFormat: Format[EachRecordObj] = Format(EachRecordObjReads, EachRecordObjWrites)
  }

  case class dctrAptmtDtls(booked: Int, completed: Int, cancelled: Int, totalappointment: Int)

  object dctrAptmtDtls {
    val dctrAptmtDtlsReads = (
      (__ \ "booked").read[Int] and
      (__ \ "completed").read[Int] and
      (__ \ "cancelled").read[Int] and
      (__ \ "totalappointment").read[Int])(dctrAptmtDtls.apply _)

    val dctrAptmtDtlsWrites = (
      (__ \ "booked").write[Int] and
      (__ \ "completed").write[Int] and
      (__ \ "cancelled").write[Int] and
      (__ \ "totalappointment").write[Int])(unlift(dctrAptmtDtls.unapply))
    implicit val EachRecordObjFormat: Format[dctrAptmtDtls] = Format(dctrAptmtDtlsReads, dctrAptmtDtlsWrites)
  }
  case class DashboardInfoInputTypeObj(hospitalId: Long, doctorid: Long, addressid: Long, startDate: Date, endDate: Date, includeBranch: String)

  object DashboardInfoInputTypeObj {
    val DashboardInfoInputTypeObjReads = (
      (__ \ "hospitalId").read[Long] and
      (__ \ "doctorid").read[Long] and
      (__ \ "addressid").read[Long] and
      (__ \ "startDate").read[Date] and
      (__ \ "endDate").read[Date] and
      (__ \ "includeBranch").read[String])(DashboardInfoInputTypeObj.apply _)

    val DashboardInfoInputTypeObjWrites = (
      (__ \ "hospitalId").write[Long] and
      (__ \ "doctorid").write[Long] and
      (__ \ "addressid").write[Long] and
      (__ \ "startDate").write[Date] and
      (__ \ "endDate").write[Date] and
      (__ \ "includeBranch").write[String])(unlift(DashboardInfoInputTypeObj.unapply))
    implicit val dashboardInfoInputTypeObjFormat: Format[DashboardInfoInputTypeObj] = Format(DashboardInfoInputTypeObjReads, DashboardInfoInputTypeObjWrites)
  }
  /*Added for New Dashboard Change- ZENOTI Type- Goutham Sai*/

  case class NewDashboardInfoInputTypeObj(hospitalId: Long, doctorid: Long, addressid: Long, startDate: Date, endDate: Date, includeBranch: String, dashBoardType: String,
                                          InventoryType: Option[String], ReferralID: Option[Long], SubReferralID: Option[Long], AppointmentVisit: Option[String])

  object NewDashboardInfoInputTypeObj {
    val NewDashboardInfoInputTypeObjReads = (
      (__ \ "hospitalId").read[Long] and
      (__ \ "doctorid").read[Long] and
      (__ \ "addressid").read[Long] and
      (__ \ "startDate").read[Date] and
      (__ \ "endDate").read[Date] and
      (__ \ "includeBranch").read[String] and
      (__ \ "dashBoardType").read[String] and
      (__ \ "InventoryType").readNullable[String] and
      (__ \ "ReferralID").readNullable[Long] and
      (__ \ "SubReferralID").readNullable[Long] and
      (__ \ "AppointmentVisit").readNullable[String])(NewDashboardInfoInputTypeObj.apply _)

    val NewDashboardInfoInputTypeObjWrites = (
      (__ \ "hospitalId").write[Long] and
      (__ \ "doctorid").write[Long] and
      (__ \ "addressid").write[Long] and
      (__ \ "startDate").write[Date] and
      (__ \ "endDate").write[Date] and
      (__ \ "includeBranch").write[String] and
      (__ \ "dashBoardType").write[String] and
      (__ \ "InventoryType").writeNullable[String] and
      (__ \ "ReferralID").writeNullable[Long] and
      (__ \ "SubReferralID").writeNullable[Long] and
      (__ \ "AppointmentVisit").writeNullable[String])(unlift(NewDashboardInfoInputTypeObj.unapply))
    implicit val dashboardInfoInputTypeObjFormat: Format[NewDashboardInfoInputTypeObj] = Format(NewDashboardInfoInputTypeObjReads, NewDashboardInfoInputTypeObjWrites)
  }

  case class EnquiryDashboardInfoInputTypeObj(Hospitalcode:String,hospitalId: Long, fromdate: java.sql.Date, todate: java.sql.Date)

  object EnquiryDashboardInfoInputTypeObj {
    val EnquiryDashboardInfoInputTypeObjReads = (
      (__ \ "Hospitalcode").read[String] and
      (__ \ "hospitalId").read[Long] and
      (__ \ "fromdate").read[java.sql.Date] and
      (__ \ "todate").read[java.sql.Date])(EnquiryDashboardInfoInputTypeObj.apply _)

    val EnquiryDashboardInfoInputTypeObjWrites = (
      (__ \ "Hospitalcode").write[String] and
      (__ \ "hospitalId").write[Long] and
      (__ \ "fromdate").write[java.sql.Date] and
      (__ \ "todate").write[java.sql.Date])(unlift(EnquiryDashboardInfoInputTypeObj.unapply))
    implicit val EnquiryDashboardInfoInputTypeObjFormat: Format[EnquiryDashboardInfoInputTypeObj] = Format(EnquiryDashboardInfoInputTypeObjReads, EnquiryDashboardInfoInputTypeObjWrites)
  }

  case class getDailyReportInfoObj(Hospitalcode: String,hospitalID: Long, date: java.sql.Date)

  object getDailyReportInfoObj {
    val getDailyReportInfoObjReads = (
      (__ \ "Hospitalcode").read[String] and   
      (__ \ "hospitalID").read[Long] and
      (__ \ "date").read[java.sql.Date])(getDailyReportInfoObj.apply _)

    val getDailyReportInfoObjWrites = (
      (__ \ "Hospitalcode").write[String] and   
      (__ \ "hospitalID").write[Long] and
      (__ \ "date").write[java.sql.Date])(unlift(getDailyReportInfoObj.unapply))
    implicit val getDailyReportInfoObjFormat: Format[getDailyReportInfoObj] = Format(getDailyReportInfoObjReads, getDailyReportInfoObjWrites)
  }

  case class getDailySpecialityReportInfoObj(Hospitalcode: String,hospitalID: Long, date: java.sql.Date)

  object getDailySpecialityReportInfoObj {
    val getDailySpecialityReportInfoObjReads = (
      (__ \ "Hospitalcode").read[String] and   
      (__ \ "hospitalID").read[Long] and
      (__ \ "date").read[java.sql.Date])(getDailySpecialityReportInfoObj.apply _)

    val getDailySpecialityReportInfoObjWrites = (
      (__ \ "Hospitalcode").write[String] and   
      (__ \ "hospitalID").write[Long] and
      (__ \ "date").write[java.sql.Date])(unlift(getDailySpecialityReportInfoObj.unapply))
    implicit val getDailyReportInfoObjFormat: Format[getDailySpecialityReportInfoObj] = Format(getDailySpecialityReportInfoObjReads, getDailySpecialityReportInfoObjWrites)
  }
  
  
   case class getCalenderlistviewInfoObj(Hospitalcode: String,hospitalID: Long, date: java.sql.Date)

  object getCalenderlistviewInfoObj {
    val getCalenderlistviewInfoObjReads = (
      (__ \ "Hospitalcode").read[String] and   
      (__ \ "hospitalID").read[Long] and
      (__ \ "date").read[java.sql.Date])(getCalenderlistviewInfoObj.apply _)

    val getCalenderlistviewInfoObjWrites = (
      (__ \ "Hospitalcode").write[String] and   
      (__ \ "hospitalID").write[Long] and
      (__ \ "date").write[java.sql.Date])(unlift(getCalenderlistviewInfoObj.unapply))
    implicit val getCalenderlistviewInfoObjFormat: Format[getCalenderlistviewInfoObj] = Format(getCalenderlistviewInfoObjReads, getCalenderlistviewInfoObjWrites)
  }
   
   
      case class getAgentstatsInfoObj(Hospitalcode: String,date: java.sql.Date)
  object getAgentstatsInfoObj {
    val getAgentstatsInfoObjReads = (
      (__ \ "Hospitalcode").read[String] and   
      (__ \ "date").read[java.sql.Date])(getAgentstatsInfoObj.apply _)

    val getAgentstatsInfoObjWrites = (
      (__ \ "Hospitalcode").write[String] and   
      (__ \ "date").write[java.sql.Date])(unlift(getAgentstatsInfoObj.unapply))
    implicit val getAgentstatsInfoObjFormat: Format[getAgentstatsInfoObj] = Format(getAgentstatsInfoObjReads, getAgentstatsInfoObjWrites)
  }
      
      

  case class getCostReportInfoObj(Regionid: Long, fromdate: java.sql.Date, todate: java.sql.Date)

  object getCostReportInfoObj {
    val getCostReportInfoObjReads = (
      (__ \ "Regionid").read[Long] and
      (__ \ "fromdate").read[java.sql.Date] and
      (__ \ "todate").read[java.sql.Date])(getCostReportInfoObj.apply _)

    val getCostReportInfoObjWrites = (
      (__ \ "Regionid").write[Long] and
      (__ \ "fromdate").write[java.sql.Date] and
      (__ \ "todate").write[java.sql.Date])(unlift(getCostReportInfoObj.unapply))
    implicit val getDailyReportInfoObjFormat: Format[getCostReportInfoObj] = Format(getCostReportInfoObjReads, getCostReportInfoObjWrites)
  }
  
  
    case class getCostReportwithregionInfoObj(Hospitalcode: String, fromdate: java.sql.Date, todate: java.sql.Date)

  object getCostReportwithregionInfoObj {
    val getCostReportwithregionInfoObjReads = (
      (__ \ "Hospitalcode").read[String] and
      (__ \ "fromdate").read[java.sql.Date] and
      (__ \ "todate").read[java.sql.Date])(getCostReportwithregionInfoObj.apply _)

    val getCostReportwithregionInfoObjWrites = (
      (__ \ "Hospitalcode").write[String] and
      (__ \ "fromdate").write[java.sql.Date] and
      (__ \ "todate").write[java.sql.Date])(unlift(getCostReportwithregionInfoObj.unapply))
    implicit val getCostReportwithregionInfoObjFormat: Format[getCostReportwithregionInfoObj] = Format(getCostReportwithregionInfoObjReads, getCostReportwithregionInfoObjWrites)
  }
    
    
   case class getReferralwithoutIndicationObj(HospitalCode: String,HospitalID: Long, fromdate: java.sql.Date, todate: java.sql.Date, UserID: Long)

  object getReferralwithoutIndicationObj {
    val getReferralwithoutIndicationObjReads = (
      (__ \ "HospitalCode").read[String] and
      (__ \ "HospitalID").read[Long] and
      (__ \ "fromdate").read[java.sql.Date] and
      (__ \ "todate").read[java.sql.Date] and 
	  (__ \ "UserID").read[Long])(getReferralwithoutIndicationObj.apply _)

    val getReferralwithoutIndicationObjWrites = (
      (__ \ "HospitalCode").write[String] and
      (__ \ "HospitalID").write[Long] and
      (__ \ "fromdate").write[java.sql.Date] and
      (__ \ "todate").write[java.sql.Date] and
	  (__ \ "UserID").write[Long])(unlift(getReferralwithoutIndicationObj.unapply))
    implicit val getReferralwithoutIndicationObjFormat: Format[getReferralwithoutIndicationObj] = Format(getReferralwithoutIndicationObjReads, getReferralwithoutIndicationObjWrites)
  }


  case class getReferralReportInfoObj(Hospitalcode: String,hospitalid: Long, fromdate: java.sql.Date, todate: java.sql.Date)

  object getReferralReportInfoObj {
    val getReferralReportInfoObjReads = (
      (__ \ "Hospitalcode").read[String] and
      (__ \ "hospitalid").read[Long] and
      (__ \ "fromdate").read[java.sql.Date] and
      (__ \ "todate").read[java.sql.Date])(getReferralReportInfoObj.apply _)

    val getReferralReportInfoObjWrites = (
      (__ \ "Hospitalcode").write[String] and
      (__ \ "hospitalid").write[Long] and
      (__ \ "fromdate").write[java.sql.Date] and
      (__ \ "todate").write[java.sql.Date])(unlift(getReferralReportInfoObj.unapply))
    implicit val getReferralReportInfoObjFormat: Format[getReferralReportInfoObj] = Format(getReferralReportInfoObjReads, getReferralReportInfoObjWrites)
  }
  
  case class getReferralReportInfoObjv1(CategoryID: Long,Hospitalcode: String,hospitalid: Long, fromdate: java.sql.Date, todate: java.sql.Date)

  object getReferralReportInfoObjv1 {
    val getReferralReportInfoObjReads = (
      (__ \ "CategoryID").read[Long] and 
      (__ \ "Hospitalcode").read[String] and
      (__ \ "hospitalid").read[Long] and
      (__ \ "fromdate").read[java.sql.Date] and
      (__ \ "todate").read[java.sql.Date])(getReferralReportInfoObjv1.apply _)

    val getReferralReportInfoObjWrites = (
      (__ \ "CategoryID").write[Long] and 
      (__ \ "Hospitalcode").write[String] and
      (__ \ "hospitalid").write[Long] and
      (__ \ "fromdate").write[java.sql.Date] and
      (__ \ "todate").write[java.sql.Date])(unlift(getReferralReportInfoObjv1.unapply))
    implicit val getReferralReportInfoObjFormat: Format[getReferralReportInfoObjv1] = Format(getReferralReportInfoObjReads, getReferralReportInfoObjWrites)
  }
  
    case class getAppointmentpracticeInfoObj(Hospitalcode: String,hospitalid: Long, fromdate: java.sql.Date, todate: java.sql.Date,practiceid: Long)

  object getAppointmentpracticeInfoObj {
    val getAppointmentpracticeInfoObjReads = (
      (__ \ "Hospitalcode").read[String] and
      (__ \ "hospitalid").read[Long] and
      (__ \ "fromdate").read[java.sql.Date] and
      (__ \ "todate").read[java.sql.Date] and
      (__ \ "practiceid").read[Long])(getAppointmentpracticeInfoObj.apply _)

    val getAppointmentpracticeInfoObjWrites = (
      (__ \ "Hospitalcode").write[String] and
      (__ \ "hospitalid").write[Long] and
      (__ \ "fromdate").write[java.sql.Date] and
      (__ \ "todate").write[java.sql.Date] and
      (__ \ "practiceid").write[Long])(unlift(getAppointmentpracticeInfoObj.unapply))
    implicit val getAppointmentpracticeInfoObjFormat: Format[getAppointmentpracticeInfoObj] = Format(getAppointmentpracticeInfoObjReads, getAppointmentpracticeInfoObjWrites)
  }
  
  
    case class getclinicauditReportInfoObj(Hospitalcode: String,hospitalid: Long,regionid: Long,ccgid: Long, fromdate: java.sql.Date, todate: java.sql.Date)

  object getclinicauditReportInfoObj {
    val getclinicauditReportInfoObjReads = (
      (__ \ "Hospitalcode").read[String] and
      (__ \ "hospitalid").read[Long] and
      (__ \ "regionid").read[Long] and
      (__ \ "ccgid").read[Long] and
      (__ \ "fromdate").read[java.sql.Date] and
      (__ \ "todate").read[java.sql.Date])(getclinicauditReportInfoObj.apply _)

    val getclinicauditReportInfoObjWrites = (
      (__ \ "Hospitalcode").write[String] and
      (__ \ "hospitalid").write[Long] and
      (__ \ "regionid").write[Long] and
      (__ \ "ccgid").write[Long] and
      (__ \ "fromdate").write[java.sql.Date] and
      (__ \ "todate").write[java.sql.Date])(unlift(getclinicauditReportInfoObj.unapply))
    implicit val getclinicauditReportInfoObjFormat: Format[getclinicauditReportInfoObj] = Format(getclinicauditReportInfoObjReads, getclinicauditReportInfoObjWrites)
  }
    
    

  case class ListEnquiryDashboardInfo(Tottotriage: Long, Totawaitingbooking: Long, Totbooked: Long,
                                      Totroutine: Long, Totcomplete: Long, Totawaitingforcall: Long,
                                      Toturgent: Long, Totissues: Long, Totreject: Long, Totpublished: Long,
                                      Totdna: Long, Totdvt: Long, Totcost: Long, Totgrossprofit: Long,
                                      Totgrossprofitpercent: Long, Totcapacitypercent: Long)
  object ListEnquiryDashboardInfo {
    val ListEnquiryDashboardInfoReads = (
      (__ \ "Tottotriage").read[Long] and
      (__ \ "Totawaitingbooking").read[Long] and
      (__ \ "Totbooked").read[Long] and
      (__ \ "Totroutine").read[Long] and
      (__ \ "Totcomplete").read[Long] and
      (__ \ "Totawaitingforcall").read[Long] and
      (__ \ "Toturgent").read[Long] and
      (__ \ "Totissues").read[Long] and
      (__ \ "Totreject").read[Long] and
      (__ \ "Totpublished").read[Long] and
      (__ \ "Totdna").read[Long] and
      (__ \ "Totdvt").read[Long] and
      (__ \ "Totcost").read[Long] and
      (__ \ "Totgrossprofit").read[Long] and
      (__ \ "Totgrossprofitpercent").read[Long] and
      (__ \ "Totcapacitypercent").read[Long]
      )(ListEnquiryDashboardInfo.apply _)

    val ListEnquiryDashboardInfoWrites = (
      (__ \ "Tottotriage").write[Long] and
      (__ \ "Totawaitingbooking").write[Long] and
      (__ \ "Totbooked").write[Long] and
      (__ \ "Totroutine").write[Long] and
      (__ \ "Totcomplete").write[Long] and
      (__ \ "Totawaitingforcall").write[Long] and
      (__ \ "Toturgent").write[Long] and
      (__ \ "Totissues").write[Long] and
      (__ \ "Totreject").write[Long] and
      (__ \ "Totpublished").write[Long] and
      (__ \ "Totdna").write[Long] and
      (__ \ "Totdvt").write[Long] and
      (__ \ "Totcost").write[Long] and
      (__ \ "Totgrossprofit").write[Long] and
      (__ \ "Totgrossprofitpercent").write[Long] and
      (__ \ "Totcapacitypercent").write[Long])(unlift(ListEnquiryDashboardInfo.unapply))
    implicit val ListEnquiryDashboardInfoFormat: Format[ListEnquiryDashboardInfo] = Format(ListEnquiryDashboardInfoReads, ListEnquiryDashboardInfoWrites)
  }

  case class getDailyReportListInfo(regionid: Long, regionname: Option[String], newtriagebook: Option[Long],
                                    awaiting: Option[Long], followup: Option[Long], nocontact: Option[Long], issue: Option[Long],
                                    totalregion: Option[Long])
  object getDailyReportListInfo {
    val ListEnquiryDashboardInfoReads = (
      (__ \ "regionid").read[Long] and
      (__ \ "regionname").readNullable[String] and
      (__ \ "newtriagebook").readNullable[Long] and
      (__ \ "awaiting").readNullable[Long] and
      (__ \ "followup").readNullable[Long] and
      (__ \ "nocontact").readNullable[Long] and
      (__ \ "issue").readNullable[Long] and
      (__ \ "totalregion").readNullable[Long])(getDailyReportListInfo.apply _)

    val ListEnquiryDashboardInfoWrites = (
      (__ \ "regionid").write[Long] and
      (__ \ "regionname").writeNullable[String] and
      (__ \ "newtriagebook").writeNullable[Long] and
      (__ \ "awaiting").writeNullable[Long] and
      (__ \ "followup").writeNullable[Long] and
      (__ \ "nocontact").writeNullable[Long] and
      (__ \ "issue").writeNullable[Long] and
      (__ \ "totalregion").writeNullable[Long])(unlift(getDailyReportListInfo.unapply))
    implicit val ListEnquiryDashboardInfoFormat: Format[getDailyReportListInfo] = Format(ListEnquiryDashboardInfoReads, ListEnquiryDashboardInfoWrites)
  }

  case class listDailySpecialityReportListInfo(patientid: Long,referalreceived: java.sql.Date,patientenquiryid: Long,regionid: Long, regionname: Option[String], speciality: Option[String],
                                               reason: Option[String])
  object listDailySpecialityReportListInfo {
    val listDailySpecialityReportListInfoReads = (
      (__ \ "patientid").read[Long] and
      (__ \ "referalreceived").read[java.sql.Date] and
      (__ \ "patientenquiryid").read[Long] and
      (__ \ "regionid").read[Long] and
      (__ \ "regionname").readNullable[String] and
      (__ \ "speciality").readNullable[String] and
      (__ \ "reason").readNullable[String])(listDailySpecialityReportListInfo.apply _)

    val listDailySpecialityReportListInfoWrites = (
      (__ \ "patientid").write[Long] and
      (__ \ "referalreceived").write[java.sql.Date] and
      (__ \ "patientenquiryid").write[Long] and  
      (__ \ "regionid").write[Long] and
      (__ \ "regionname").writeNullable[String] and
      (__ \ "speciality").writeNullable[String] and
      (__ \ "reason").writeNullable[String])(unlift(listDailySpecialityReportListInfo.unapply))
    implicit val listDailySpecialityReportListInfoFormat: Format[listDailySpecialityReportListInfo] = Format(listDailySpecialityReportListInfoReads, listDailySpecialityReportListInfoWrites)
  }

  case class listCostReportListInfo(AddressConsultID: Option[Long], Date: Option[java.sql.Date], HospitalName: Option[String],
                                    PTsscaned: Option[Long], Fullpercent: Option[Long], Revenue: Option[Long],
                                    Base: Option[Long], Triage: Option[Long], BaseInc: Option[Long], Expense: Option[Long], RoomCost: Option[Long],
                                    Consumables: Option[Double], Totalcost: Option[Double], Grossprofit: Option[Double], Grossprofitpercent: Option[Double])
  object listCostReportListInfo {
    val listCostReportListInfoReads = (
      (__ \ "AddressConsultID").readNullable[Long] and
      (__ \ "Date").readNullable[java.sql.Date] and
      (__ \ "HospitalName").readNullable[String] and
      (__ \ "PTsscaned").readNullable[Long] and
      (__ \ "Fullpercent").readNullable[Long] and
      (__ \ "Revenue").readNullable[Long] and
      (__ \ "Base").readNullable[Long] and
      (__ \ "Triage").readNullable[Long] and
      (__ \ "BaseInc").readNullable[Long] and
      (__ \ "Expense").readNullable[Long] and
      (__ \ "RoomCost").readNullable[Long] and
      (__ \ "Consumables").readNullable[Double] and
      (__ \ "Totalcost").readNullable[Double] and
      (__ \ "Grossprofit").readNullable[Double] and
      (__ \ "Grossprofitpercent").readNullable[Double])(listCostReportListInfo.apply _)

    val listCostReportListInfoWrites = (
      (__ \ "AddressConsultID").writeNullable[Long] and
      (__ \ "Date").writeNullable[java.sql.Date] and
      (__ \ "HospitalName").writeNullable[String] and
      (__ \ "PTsscaned").writeNullable[Long] and
      (__ \ "Fullpercent").writeNullable[Long] and
      (__ \ "Revenue").writeNullable[Long] and
      (__ \ "Base").writeNullable[Long] and
      (__ \ "Triage").writeNullable[Long] and
      (__ \ "BaseInc").writeNullable[Long] and
      (__ \ "Expense").writeNullable[Long] and
      (__ \ "RoomCost").writeNullable[Long] and
      (__ \ "Consumables").writeNullable[Double] and
      (__ \ "Totalcost").writeNullable[Double] and
      (__ \ "Grossprofit").writeNullable[Double] and
      (__ \ "Grossprofitpercent").writeNullable[Double])(unlift(listCostReportListInfo.unapply))
    implicit val listCostReportListInfoFormat: Format[listCostReportListInfo] = Format(listCostReportListInfoReads, listCostReportListInfoWrites)
  }
  
  
  
    case class listForecastReportListInfo(AddressConsultID: Option[Long], Date: Option[java.sql.Date], HospitalName: Option[String],
                                    PTsscaned: Option[Long], Fullpercent: Option[Long],dnastatus: Option[String],lcstatus: Option[String], Revenue: Option[Long],
                                    Base: Option[Long], Triage: Option[Long], BaseInc: Option[Long], Expense: Option[Long], RoomCost: Option[Long],
                                    Consumables: Option[Double], Totalcost: Option[Double], Grossprofit: Option[Double], Grossprofitpercent: Option[Double])
  object listForecastReportListInfo {
    val listForecastReportListInfoReads = (
      (__ \ "AddressConsultID").readNullable[Long] and
      (__ \ "Date").readNullable[java.sql.Date] and
      (__ \ "HospitalName").readNullable[String] and
      (__ \ "PTsscaned").readNullable[Long] and
      (__ \ "Fullpercent").readNullable[Long] and
      (__ \ "dnastatus").readNullable[String] and
      (__ \ "lcstatus").readNullable[String] and
      (__ \ "Revenue").readNullable[Long] and
      (__ \ "Base").readNullable[Long] and
      (__ \ "Triage").readNullable[Long] and
      (__ \ "BaseInc").readNullable[Long] and
      (__ \ "Expense").readNullable[Long] and
      (__ \ "RoomCost").readNullable[Long] and
      (__ \ "Consumables").readNullable[Double] and
      (__ \ "Totalcost").readNullable[Double] and
      (__ \ "Grossprofit").readNullable[Double] and
      (__ \ "Grossprofitpercent").readNullable[Double])(listForecastReportListInfo.apply _)

    val listForecastReportListInfoWrites = (
      (__ \ "AddressConsultID").writeNullable[Long] and
      (__ \ "Date").writeNullable[java.sql.Date] and
      (__ \ "HospitalName").writeNullable[String] and
      (__ \ "PTsscaned").writeNullable[Long] and
      (__ \ "Fullpercent").writeNullable[Long] and
      (__ \ "dnastatus").writeNullable[String] and
      (__ \ "lcstatus").writeNullable[String] and
      (__ \ "Revenue").writeNullable[Long] and
      (__ \ "Base").writeNullable[Long] and
      (__ \ "Triage").writeNullable[Long] and
      (__ \ "BaseInc").writeNullable[Long] and
      (__ \ "Expense").writeNullable[Long] and
      (__ \ "RoomCost").writeNullable[Long] and
      (__ \ "Consumables").writeNullable[Double] and
      (__ \ "Totalcost").writeNullable[Double] and
      (__ \ "Grossprofit").writeNullable[Double] and
      (__ \ "Grossprofitpercent").writeNullable[Double])(unlift(listForecastReportListInfo.unapply))
    implicit val listForecastReportListInfoFormat: Format[listForecastReportListInfo] = Format(listForecastReportListInfoReads, listForecastReportListInfoWrites)
  }


  case class listReferralReportListInfo(DWID: Option[Long], ReferralCreated: Option[java.sql.Date], ReferralProcessed: Option[java.sql.Date], ReferralRecevied: Option[java.sql.Date],
                                        EnquirySource: Option[String], ReferralDoctor: Option[String], PracticeName: Option[String],
                                        CCGName: Option[String], RegionName: Option[String], AppointmentType: Option[String],Patientenquiryid: Option[Long],specialitydetails:Array[specialitydetailsObj])
  object listReferralReportListInfo {
    val listReferralReportListInfoReads = (
      (__ \ "DWID").readNullable[Long] and
      (__ \ "ReferralCreated").readNullable[java.sql.Date] and
      (__ \ "ReferralProcessed").readNullable[java.sql.Date] and
      (__ \ "ReferralRecevied").readNullable[java.sql.Date] and
      (__ \ "EnquirySource").readNullable[String] and
      (__ \ "ReferralDoctor").readNullable[String] and
      (__ \ "PracticeName").readNullable[String] and
      (__ \ "CCGName").readNullable[String] and
      (__ \ "RegionName").readNullable[String] and
      (__ \ "AppointmentType").readNullable[String] and
      (__ \ "Patientenquiryid").readNullable[Long] and 
      (__ \ "specialitydetails").read[Array[specialitydetailsObj]])(listReferralReportListInfo.apply _)

    val listReferralReportListInfoWrites = (
      (__ \ "DWID").writeNullable[Long] and
      (__ \ "ReferralCreated").writeNullable[java.sql.Date] and
      (__ \ "ReferralProcessed").writeNullable[java.sql.Date] and
      (__ \ "ReferralRecevied").writeNullable[java.sql.Date] and
      (__ \ "EnquirySource").writeNullable[String] and
      (__ \ "ReferralDoctor").writeNullable[String] and
      (__ \ "PracticeName").writeNullable[String] and
      (__ \ "CCGName").writeNullable[String] and
      (__ \ "RegionName").writeNullable[String] and
      (__ \ "AppointmentType").writeNullable[String] and
      (__ \ "Patientenquiryid").writeNullable[Long] and
      (__ \ "specialitydetails").write[Array[specialitydetailsObj]])(unlift(listReferralReportListInfo.unapply))
    implicit val listReferralReportListInfoFormat: Format[listReferralReportListInfo] = Format(listReferralReportListInfoReads, listReferralReportListInfoWrites)
  }
  
  case class specialitydetailsObj(id : Option[Long], specialityid:String, speciality:String, activeInd: String, subSpecialities : Map[String, String])

object specialitydetailsObj {
  val specialitydetailsObjReads = (
      (__ \ "id").readNullable[Long] and      
      (__ \ "specialityid").read[String] and 
      (__ \ "speciality").read[String] and 
      (__ \ "activeInd").read[String] and 
      (__ \ "subSpecialities").read[Map[String,String]]) (specialitydetailsObj.apply _)
      
  val specialitydetailsObjWrites = (
      (__ \ "id").writeNullable[Long] and 
      (__ \ "specialityid").write[String] and 
      (__ \ "speciality").write[String] and 
      (__ \ "activeInd").write[String] and 
      (__ \ "subSpecialities").write[Map[String,String]]) (unlift(specialitydetailsObj.unapply))
    
    implicit val addPatientLoginFormat: Format[specialitydetailsObj] = Format(specialitydetailsObjReads, specialitydetailsObjWrites)
    
}


  case class listPatientinSystemInfo(Triagetot: Option[Long], Followupstot: Option[Long], NCtot: Option[Long], Issuestot: Option[Long],
                                     Awaitingtot: Option[Long], Regiontot: Option[Long], ReceivedDate: Option[java.sql.Date])
  object listPatientinSystemInfo {
    val listPatientinSystemInfoReads = (
      (__ \ "Triagetot").readNullable[Long] and
      (__ \ "Followupstot").readNullable[Long] and
      (__ \ "NCtot").readNullable[Long] and
      (__ \ "Issuestot").readNullable[Long] and
      (__ \ "Awaitingtot").readNullable[Long] and
      (__ \ "Regiontot").readNullable[Long] and
      (__ \ "ReceivedDate").readNullable[java.sql.Date])(listPatientinSystemInfo.apply _)

    val listPatientinSystemInfoWrites = (
      (__ \ "Triagetot").writeNullable[Long] and
      (__ \ "Followupstot").writeNullable[Long] and
      (__ \ "NCtot").writeNullable[Long] and
      (__ \ "Issuestot").writeNullable[Long] and
      (__ \ "Awaitingtot").writeNullable[Long] and
      (__ \ "Regiontot").writeNullable[Long] and
      (__ \ "ReceivedDate").writeNullable[java.sql.Date])(unlift(listPatientinSystemInfo.unapply))
    implicit val listPatientinSystemInfoFormat: Format[listPatientinSystemInfo] = Format(listPatientinSystemInfoReads, listPatientinSystemInfoWrites)
  }

  case class listClinicauditcriteriaInfo(DWID: Option[Long], Patientname: Option[String], Scandate: Option[java.sql.Date], Speciality: Option[String],
                                         SonographerName: Option[String], RegionName: Option[String],CcgName: Option[String],AppointmentStatus: Option[String])
  object listClinicauditcriteriaInfo {
    val listClinicauditcriteriaInfoReads = (
      (__ \ "DWID").readNullable[Long] and
      (__ \ "Patientname").readNullable[String] and
      (__ \ "Scandate").readNullable[java.sql.Date] and
      (__ \ "Speciality").readNullable[String] and
      (__ \ "SonographerName").readNullable[String] and
      (__ \ "RegionName").readNullable[String] and
      (__ \ "CcgName").readNullable[String] and 
      (__ \ "AppointmentStatus").readNullable[String])(listClinicauditcriteriaInfo.apply _)

    val listClinicauditcriteriaInfoWrites = (
      (__ \ "DWID").writeNullable[Long] and
      (__ \ "Patientname").writeNullable[String] and
      (__ \ "Scandate").writeNullable[java.sql.Date] and
      (__ \ "Speciality").writeNullable[String] and
      (__ \ "SonographerName").writeNullable[String] and
      (__ \ "RegionName").writeNullable[String] and
      (__ \ "CcgName").writeNullable[String] and 
       (__ \ "AppointmentStatus").writeNullable[String])(unlift(listClinicauditcriteriaInfo.unapply))
    implicit val listClinicauditcriteriaInfoFormat: Format[listClinicauditcriteriaInfo] = Format(listClinicauditcriteriaInfoReads, listClinicauditcriteriaInfoWrites)
  }
  
  
  case class listCostanalyticsreportInfo(GPvalue: Option[Long], GPpercentvalue: Option[Long], Costvalue: Option[Long], Revenuevalue: Option[Long],
                                         Capacitypercent: Option[Long])
  object listCostanalyticsreportInfo {
    val listCostanalyticsreportInfoReads = (
      (__ \ "GPvalue").readNullable[Long] and
      (__ \ "GPpercentvalue").readNullable[Long] and
      (__ \ "Costvalue").readNullable[Long] and
      (__ \ "Revenuevalue").readNullable[Long] and
      (__ \ "Capacitypercent").readNullable[Long])(listCostanalyticsreportInfo.apply _)

    val listCostanalyticsreportInfoWrites = (
      (__ \ "GPvalue").writeNullable[Long] and
      (__ \ "GPpercentvalue").writeNullable[Long] and
      (__ \ "Costvalue").writeNullable[Long] and
      (__ \ "Revenuevalue").writeNullable[Long] and
      (__ \ "Capacitypercent").writeNullable[Long])(unlift(listCostanalyticsreportInfo.unapply))
    implicit val listCostanalyticsreportInfoFormat: Format[listCostanalyticsreportInfo] = Format(listCostanalyticsreportInfoReads, listCostanalyticsreportInfoWrites)
  }

  case class listAppointmentpracticeReport(AppointmentID: Option[Long], DWID: Option[Long], Title: Option[String],
                                           PatientForename: Option[String], PatientSurname: Option[String], PatientPostcode: Option[String],
                                           NHSNumber: Option[String], PracticeCode: Option[String], PracticeName: Option[String],
                                           Dateoftreatment: Option[java.sql.Date], SonographerName: Option[String],
                                           ReferralRecevied: Option[java.sql.Date], Speciality: Option[String],
                                           SubSpeciality: Option[String], ScanCentrename: Option[String],
                                           DNAStatus: Option[String], CCGName: Option[String], AppointmentStartTime: Option[String],
                                           AppointmentEndime: Option[String], Duration: Option[Long], LastCancellationReason: Option[String])
  object listAppointmentpracticeReport {
    val listAppointmentpracticeReportReads = (
      (__ \ "AppointmentID").readNullable[Long] and
      (__ \ "DWID").readNullable[Long] and
      (__ \ "Title").readNullable[String] and
      (__ \ "PatientForename").readNullable[String] and
      (__ \ "PatientSurname").readNullable[String] and
      (__ \ "PatientPostcode").readNullable[String] and
      (__ \ "NHSNumber").readNullable[String] and
      (__ \ "PracticeCode").readNullable[String] and
      (__ \ "PracticeName").readNullable[String] and
      (__ \ "Dateoftreatment").readNullable[java.sql.Date] and
      (__ \ "SonographerName").readNullable[String] and
      (__ \ "ReferralRecevied").readNullable[java.sql.Date] and
      (__ \ "Speciality").readNullable[String] and
      (__ \ "SubSpeciality").readNullable[String] and
      (__ \ "ScanCentrename").readNullable[String] and
      (__ \ "DNAStatus").readNullable[String] and
      (__ \ "CCGName").readNullable[String] and
      (__ \ "AppointmentStartTime").readNullable[String] and
      (__ \ "AppointmentEndime").readNullable[String] and
      (__ \ "Duration").readNullable[Long] and
      (__ \ "LastCancellationReason").readNullable[String])(listAppointmentpracticeReport.apply _)

    val listAppointmentpracticeReportWrites = (
      (__ \ "AppointmentID").writeNullable[Long] and
      (__ \ "DWID").writeNullable[Long] and
      (__ \ "Title").writeNullable[String] and
      (__ \ "PatientForename").writeNullable[String] and
      (__ \ "PatientSurname").writeNullable[String] and
      (__ \ "PatientPostcode").writeNullable[String] and
      (__ \ "NHSNumber").writeNullable[String] and
      (__ \ "PracticeCode").writeNullable[String] and
      (__ \ "PracticeName").writeNullable[String] and
      (__ \ "Dateoftreatment").writeNullable[java.sql.Date] and
      (__ \ "SonographerName").writeNullable[String] and
      (__ \ "ReferralRecevied").writeNullable[java.sql.Date] and
      (__ \ "Speciality").writeNullable[String] and
      (__ \ "SubSpeciality").writeNullable[String] and
      (__ \ "ScanCentrename").writeNullable[String] and
      (__ \ "DNAStatus").writeNullable[String] and
      (__ \ "CCGName").writeNullable[String] and
      (__ \ "AppointmentStartTime").writeNullable[String] and
      (__ \ "AppointmentEndime").writeNullable[String] and
      (__ \ "Duration").writeNullable[Long] and
      (__ \ "LastCancellationReason").writeNullable[String])(unlift(listAppointmentpracticeReport.unapply))
    implicit val listAppointmentpracticeReportFormat: Format[listAppointmentpracticeReport] = Format(listAppointmentpracticeReportReads, listAppointmentpracticeReportWrites)
  }
  
  
  case class listAppointmentpracticeindicationsReport(AppointmentID: Option[Long], DWID: Option[Long], Title: Option[String],
                                           PatientForename: Option[String], PatientSurname: Option[String], PatientPostcode: Option[String],
                                           NHSNumber: Option[String], PracticeCode: Option[String], PracticeName: Option[String],
                                           Dateoftreatment: Option[java.sql.Date], SonographerName: Option[String],
                                           ReferralRecevied: Option[java.sql.Date], Appointmenttype: Option[String],
                                           ScanCentrename: Option[String],
                                           DNAStatus: Option[String], CCGName: Option[String], AppointmentStartTime: Option[String],
                                           AppointmentEndime: Option[String], Duration: Option[Long])
  object listAppointmentpracticeindicationsReport {
    val listAppointmentpracticeindicationsReportReads = (
      (__ \ "AppointmentID").readNullable[Long] and
      (__ \ "DWID").readNullable[Long] and
      (__ \ "Title").readNullable[String] and
      (__ \ "PatientForename").readNullable[String] and
      (__ \ "PatientSurname").readNullable[String] and
      (__ \ "PatientPostcode").readNullable[String] and
      (__ \ "NHSNumber").readNullable[String] and
      (__ \ "PracticeCode").readNullable[String] and
      (__ \ "PracticeName").readNullable[String] and
      (__ \ "Dateoftreatment").readNullable[java.sql.Date] and
      (__ \ "SonographerName").readNullable[String] and
      (__ \ "ReferralRecevied").readNullable[java.sql.Date] and
      (__ \ "Appointmenttype").readNullable[String] and
      (__ \ "ScanCentrename").readNullable[String] and
      (__ \ "DNAStatus").readNullable[String] and
      (__ \ "CCGName").readNullable[String] and
      (__ \ "AppointmentStartTime").readNullable[String] and
      (__ \ "AppointmentEndime").readNullable[String] and
      (__ \ "Duration").readNullable[Long])(listAppointmentpracticeindicationsReport.apply _)

    val listAppointmentpracticeindicationsReportWrites = (
      (__ \ "AppointmentID").writeNullable[Long] and
      (__ \ "DWID").writeNullable[Long] and
      (__ \ "Title").writeNullable[String] and
      (__ \ "PatientForename").writeNullable[String] and
      (__ \ "PatientSurname").writeNullable[String] and
      (__ \ "PatientPostcode").writeNullable[String] and
      (__ \ "NHSNumber").writeNullable[String] and
      (__ \ "PracticeCode").writeNullable[String] and
      (__ \ "PracticeName").writeNullable[String] and
      (__ \ "Dateoftreatment").writeNullable[java.sql.Date] and
      (__ \ "SonographerName").writeNullable[String] and
      (__ \ "ReferralRecevied").writeNullable[java.sql.Date] and
      (__ \ "Appointmenttype").writeNullable[String] and
      (__ \ "ScanCentrename").writeNullable[String] and
      (__ \ "DNAStatus").writeNullable[String] and
      (__ \ "CCGName").writeNullable[String] and
      (__ \ "AppointmentStartTime").writeNullable[String] and
      (__ \ "AppointmentEndime").writeNullable[String] and
      (__ \ "Duration").writeNullable[Long])(unlift(listAppointmentpracticeindicationsReport.unapply))
    implicit val listAppointmentpracticeindicationsReportFormat: Format[listAppointmentpracticeindicationsReport] = Format(listAppointmentpracticeindicationsReportReads, listAppointmentpracticeindicationsReportWrites)
  }

  

  case class DashboardNewInfoTypeResObj(patientVisits: Option[patientVisitsDtlsObj], smsDetails: Option[doctorCommStatusObj], revenue: Option[Long], doctorAppointmentDetails: Option[dctrAptmtDtls])

  object DashboardNewInfoTypeResObj {
    val DashboardNewInfoTypeResObjReads = (
      (__ \ "patientVisits").readNullable[patientVisitsDtlsObj] and
      (__ \ "smsDetails").readNullable[doctorCommStatusObj] and
      (__ \ "revenue").readNullable[Long] and
      (__ \ "doctorAppointmentDetails").readNullable[dctrAptmtDtls])(DashboardNewInfoTypeResObj.apply _)

    val DashboardNewInfoTypeResObjWrites = (
      (__ \ "patientVisits").writeNullable[patientVisitsDtlsObj] and
      (__ \ "smsDetails").writeNullable[doctorCommStatusObj] and
      (__ \ "revenue").writeNullable[Long] and
      (__ \ "doctorAppointmentDetails").writeNullable[dctrAptmtDtls])(unlift(DashboardNewInfoTypeResObj.unapply))
    implicit val dashboardInfoInputTypeObjFormat: Format[DashboardNewInfoTypeResObj] = Format(DashboardNewInfoTypeResObjReads, DashboardNewInfoTypeResObjWrites)
  }

  case class patientVisitsDtlsObj(NewPatient: Option[Int], OldPatient: Option[Int], CancelledPatient: Option[Int], Male: Option[Int], Female: Option[Int], ReferralSource: Option[Int],
                                  Sub_ReferralSource: Option[Int], Age: Option[AgeDtls])

  object patientVisitsDtlsObj {
    val patientVisitsDtlsObjReads = (
      (__ \ "NewPatient").readNullable[Int] and
      (__ \ "OldPatient").readNullable[Int] and
      (__ \ "CancelledPatient").readNullable[Int] and
      (__ \ "Male").readNullable[Int] and
      (__ \ "Female").readNullable[Int] and
      (__ \ "ReferralSource").readNullable[Int] and
      (__ \ "Sub_ReferralSource").readNullable[Int] and
      (__ \ "Age").readNullable[AgeDtls])(patientVisitsDtlsObj.apply _)

    val patientVisitsDtlsObjWrites = (
      (__ \ "NewPatient").writeNullable[Int] and
      (__ \ "OldPatient").writeNullable[Int] and
      (__ \ "CancelledPatient").writeNullable[Int] and
      (__ \ "Male").writeNullable[Int] and
      (__ \ "Female").writeNullable[Int] and
      (__ \ "ReferralSource").writeNullable[Int] and
      (__ \ "Sub_ReferralSource").writeNullable[Int] and
      (__ \ "Age").writeNullable[AgeDtls])(unlift(patientVisitsDtlsObj.unapply))
    implicit val dashboardInfoInputTypeObjFormat: Format[patientVisitsDtlsObj] = Format(patientVisitsDtlsObjReads, patientVisitsDtlsObjWrites)
  }

  case class AgeDtls(Age_0_10: Option[Long], Age_10_25: Option[Long], Age_25_50: Option[Long], Age_50_75: Option[Long], Age_75_100: Option[Long])

  object AgeDtls {
    val patientVisitsDtlsObjReads = (
      (__ \ "Age_0_10").readNullable[Long] and
      (__ \ "Age_10_25").readNullable[Long] and
      (__ \ "Age_25_50").readNullable[Long] and
      (__ \ "Age_50_75").readNullable[Long] and
      (__ \ "Age_75_100").readNullable[Long])(AgeDtls.apply _)

    val patientVisitsDtlsObjWrites = (
      (__ \ "Age_0_10").writeNullable[Long] and
      (__ \ "Age_10_25").writeNullable[Long] and
      (__ \ "Age_25_50").writeNullable[Long] and
      (__ \ "Age_50_75").writeNullable[Long] and
      (__ \ "Age_75_100").writeNullable[Long])(unlift(AgeDtls.unapply))
    implicit val dashboardInfoInputTypeObjFormat: Format[AgeDtls] = Format(patientVisitsDtlsObjReads, patientVisitsDtlsObjWrites)
  }
  /*Changes End for New Dashboard Change- ZENOTI Type- Goutham Sai*/
}