package models.com.hcue.doctors.Json.Request

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import play.api.data.validation._
import play.api.libs.functional.syntax._
import java.sql.Date

case class DashboardManagementRequestObj(DoctorID:Long,DoctorName: String, USRId: Int, USRType: String)

object DashboardManagementRequestObj {
  val DashboardManagementRequestObjReads = (
      
      (__ \ "DoctorID").read[Long]and
      (__ \ "DoctorName").read[String]and
      (__ \ "USRId").read[Int] and 
      (__ \ "USRType").read[String])(DashboardManagementRequestObj.apply _)

  val DashboardManagementRequestObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "DoctorName").write[String] and 
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(DashboardManagementRequestObj.unapply))
  implicit val DashboardManagementRequestObjFormat: Format[DashboardManagementRequestObj] = Format(DashboardManagementRequestObjReads, DashboardManagementRequestObjWrites)
}

case class AdminDashboardInfoRequestObj(HospitalID:Long, USRId: Int, USRType: String)

object AdminDashboardInfoRequestObj {
  val AdminDashboardInfoRequestObjReads = (
      
      (__ \ "HospitalID").read[Long]and
      (__ \ "USRId").read[Int] and 
      (__ \ "USRType").read[String])(AdminDashboardInfoRequestObj.apply _)

  val AdminDashboardInfoRequestObjWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(AdminDashboardInfoRequestObj.unapply))
  implicit val AdminDashboardInfoRequestObjFormat: Format[AdminDashboardInfoRequestObj] = Format(AdminDashboardInfoRequestObjReads, AdminDashboardInfoRequestObjWrites)
}

case class DoctorAccuntDetailsRequestObj(DoctorID:Long,HospitalCD:String,UserTypeFlag:Option[String],Comments :Option[String],ExpiryDate:Option[java.sql.Date] ,
       ActiveIND:Option[String],USRId: Int, USRType: String)

object DoctorAccuntDetailsRequestObj{
  val DoctorAccuntDetailsRequestObjReads = (
      (__ \ "DoctorID").read[Long] and 
      (__ \ "HospitalCD").read[String]and
      (__ \ "UserTypeFlag").readNullable[String] and
      (__ \ "Comments").readNullable[String] and
      (__ \ "ExpiryDate").readNullable[java.sql.Date] and
      (__ \ "ActiveIND").readNullable[String] and
      (__ \ "USRId").read[Int] and 
      (__ \ "USRType").read[String])(DoctorAccuntDetailsRequestObj.apply _)

  val DoctorAccuntDetailsRequestObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalCD").write[String] and
    (__ \ "UserTypeFlag").writeNullable[String] and
    (__ \ "Comments").writeNullable[String] and
    (__ \ "ExpiryDate").writeNullable[java.sql.Date] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(DoctorAccuntDetailsRequestObj.unapply))
  implicit val DoctorAccuntDetailsRequestObjFormat: Format[DoctorAccuntDetailsRequestObj] = Format(DoctorAccuntDetailsRequestObjReads, DoctorAccuntDetailsRequestObjWrites)
}


case class AdminDashboardInfoResponseObj(BranchesCreated:Long,ReceptionistAdded:Long,
LeadAgentAdded:Long,SonographerAdded:Long, AgentAdded:Long,ClinicalSupportWorkerAdded:Long,TotalUserCount:Long,Profilecount:Long,Profilelist:List[Option[String]])

object AdminDashboardInfoResponseObj {
  val AdminDashboardInfoResponseObjReads = (
      (__ \ "BranchesCreated").read[Long] and
      (__ \ "ReceptionistAdded").read[Long] and
      (__ \ "LeadAgentAdded").read[Long] and
      (__ \ "SonographerAdded").read[Long] and
      (__ \ "AgentAdded").read[Long] and
      (__ \ "ClinicalSupportWorkerAdded").read[Long] and
      (__ \ "TotalUserCount").read[Long] and
      (__ \ "Profilecount").read[Long] and
      (__ \ "Profilelist").read[List[Option[String]]])(AdminDashboardInfoResponseObj.apply _)

  val AdminDashboardInfoResponseObjWrites = (
      (__ \ "BranchesCreated").write[Long] and
      (__ \ "ReceptionistAdded").write[Long] and
      (__ \ "LeadAgentAdded").write[Long] and
      (__ \ "SonographerAdded").write[Long] and
      (__ \ "AgentAdded").write[Long] and
      (__ \ "ClinicalSupportWorkerAdded").write[Long] and
      (__ \ "TotalUserCount").write[Long] and
      (__ \ "Profilecount").write[Long] and
      (__ \ "Profilelist").write[List[Option[String]]])(unlift(AdminDashboardInfoResponseObj.unapply))
  implicit val AdminDashboardInfoResponseObjFormat: Format[AdminDashboardInfoResponseObj] = Format(AdminDashboardInfoResponseObjReads, AdminDashboardInfoResponseObjWrites)
}
