/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.AddUpdateHospital

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime

case class addDocHospitalObj(hospitalRecord: addDocHospitalRecord, DoctorID: Long, USRId: Int, USRType: String)

object addDocHospitalObj {
  val addDocHospitalObjReads = (
    (__ \ "hospitalRecord").read[addDocHospitalRecord] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(addDocHospitalObj.apply _)

  val addDocHospitalObjWrites = (
    (__ \ "hospitalRecord").write[addDocHospitalRecord] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(addDocHospitalObj.unapply))
  implicit val addDocHospitalObjFormat: Format[addDocHospitalObj] = Format(addDocHospitalObjReads, addDocHospitalObjWrites)

}



case class listHospitalDoctorsReqObj(HospitalID: Option[Long],HospitalCD: Option[String],ParentHospitalID: Option[Long],DoctorID: Long, USRId: Int, USRType: String)

object listHospitalDoctorsReqObj {
  val listHospitalDoctorsReqObjReads = (
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(listHospitalDoctorsReqObj.apply _)

  val listHospitalDoctorsReqObjWrites = (
   (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "ParentHospitalID").writeNullable[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(listHospitalDoctorsReqObj.unapply))
  implicit val listHospitalDoctorsReqObjFormat: Format[listHospitalDoctorsReqObj] = Format(listHospitalDoctorsReqObjReads, listHospitalDoctorsReqObjWrites)

}

case class addDocHospitalRecord(HospitalID: Option[Long], ParentHospitalID: Option[Long], HospitalCD: Option[String], MyNetworkDoctorID: Long, PrimaryIND: Option[String],ActiveIND: String)

object addDocHospitalRecord {
  val DocHospitalRecordReads = (
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "MyNetworkDoctorID").read[Long] and
    (__ \ "PrimaryIND").readNullable[String] and
    (__ \ "ActiveIND").read[String])(addDocHospitalRecord.apply _)

  val DocHospitalRecordWrites = (
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "ParentHospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "MyNetworkDoctorID").write[Long] and
    (__ \ "PrimaryIND").writeNullable[String] and
    (__ \ "ActiveIND").write[String])(unlift(addDocHospitalRecord.unapply))
  implicit val DocHospitalRecordFormat: Format[addDocHospitalRecord] = Format(DocHospitalRecordReads, DocHospitalRecordWrites)

}

case class updateDocHospitalObj(hospitalRecord: Array[updateDocHospitalRecord], DoctorID: Long, USRId: Int, USRType: String)

object updateDocHospitalObj {
  val updateDocHospitalObjReads = (
    (__ \ "hospitalRecord").read[Array[updateDocHospitalRecord]] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updateDocHospitalObj.apply _)

  val updateDocHospitalObjWrites = (
    (__ \ "hospitalRecord").write[Array[updateDocHospitalRecord]] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(updateDocHospitalObj.unapply))
  implicit val updateDocHospitalObjFormat: Format[updateDocHospitalObj] = Format(updateDocHospitalObjReads, updateDocHospitalObjWrites)

}

case class updateDocHospitalRecord(DoctorHospitalID: Long, HospitalID: Option[Long], MyNetworkDoctorID: Long, PrimaryIND: Option[String],ActiveIND: String)

object updateDocHospitalRecord {
  val DocHospitalRecordReads = (
    (__ \ "DoctorHospitalID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "MyNetworkDoctorID").read[Long] and
    (__ \ "PrimaryIND").readNullable[String] and
    (__ \ "ActiveIND").read[String])(updateDocHospitalRecord.apply _)

  val DocHospitalRecordWrites = (
    (__ \ "DoctorHospitalID").write[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "MyNetworkDoctorID").write[Long] and
    (__ \ "PrimaryIND").writeNullable[String] and 
    (__ \ "ActiveIND").write[String])(unlift(updateDocHospitalRecord.unapply))
  implicit val DocHospitalRecordFormat: Format[updateDocHospitalRecord] = Format(DocHospitalRecordReads, DocHospitalRecordWrites)

}
