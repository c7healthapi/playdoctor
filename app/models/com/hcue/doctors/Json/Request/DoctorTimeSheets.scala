package models.com.hcue.doctors.Json.Request

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
//import java.util.Date
import java.sql.Date

import play.api.libs.functional.syntax._


case class listAvailableTimeSheets(DoctorID: Option[Long], HospitalID: Option[Long], HospitalCD: Option[String],  
    PageNumber: Int, PageSize: Int, FromDate: Option[Date],  ToDate: Option[Date],forUser:Option[Long], forType:Option[String])

object listAvailableTimeSheets {
  val listAvailableTimeSheetsReads = (
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "FromDate").readNullable[Date] and 
     (__ \ "ToDate").readNullable[Date] and 
    (__ \ "forUser").readNullable[Long] and 
     (__ \ "forType").readNullable[String]  
    )(listAvailableTimeSheets.apply _)

  val listAvailableTimeSheetsWrites = (
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "FromDate").writeNullable[Date] and 
    (__ \ "ToDate").writeNullable[Date] and 
    (__ \ "forUser").writeNullable[Long] and
    (__ \ "forType").writeNullable[String]  
    )(unlift(listAvailableTimeSheets.unapply))
  implicit val listAvailableTimeSheetsFormat: Format[listAvailableTimeSheets] = Format(listAvailableTimeSheetsReads, listAvailableTimeSheetsWrites)
}

  
 case class addTimeSlotobj ( TimeSlotID: Option[Long],DoctorID: Long, HospitalID: Option[Long],HospitalCD: Option[String],
                             ActiveIND:String,CrtUSR:Long,CrtUSRType:String, TimeFrom:String,TimeTo:String,Remarks: Option[String])
                           
                           
  
object addTimeSlotobj {
  val addTimeSlotobjReads = (
     (__ \ "TimeSlotID").readNullable[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "CrtUSR").read[Long] and 
    (__ \ "CrtUSRType").read[String] and 
    (__ \ "TimeFrom").read[String] and 
    (__ \ "TimeTo").read[String] and 
    (__ \ "Remarks").readNullable[String])(addTimeSlotobj.apply _)

  val addTimeSlotobjWrites = (
      (__ \ "TimeSlotID").writeNullable[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "CrtUSR").write[Long] and 
    (__ \ "CrtUSRType").write[String] and 
    (__ \ "TimeFrom").write[String] and 
    (__ \ "TimeTo").write[String] and 
    (__ \ "Remarks").writeNullable[String])(unlift(addTimeSlotobj.unapply))
  implicit val addTimeSlotobjFormat: Format[addTimeSlotobj] = Format(addTimeSlotobjReads, addTimeSlotobjWrites)
}
 
 
 case class addTimeSheetobj (TimeSheetID:Option[Long], TimeSlotID: Long,DoctorID: Long, HospitalID: Option[Long],HospitalCD: Option[String],
                             ActiveIND:String,CrtUSR:Long,CrtUSRType:String, UserID:Long,SlotDate:Date,SlotType:String,DutyType:Option[String],
                            FromTime:Option[String],ToTime:Option[String],UserName:Option[String],Remarks: Option[String])
                            
                           
                           
  
object addTimeSheetobj {
  val addTimeSheetobjReads = (
    (__ \ "TimeSheetID").readNullable[Long] and
    (__ \ "TimeSlotID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "CrtUSR").read[Long] and 
    (__ \ "CrtUSRType").read[String] and 
    (__ \ "UserID").read[Long] and 
    (__ \ "SlotDate").read[Date] and 
    (__ \ "SlotType").read[String] and 
    (__ \ "DutyType").readNullable[String] and
    (__ \ "FromTime").readNullable[String] and
    (__ \ "ToTime").readNullable[String] and
    (__ \ "UserName").readNullable[String] and
    (__ \ "Remarks").readNullable[String])(addTimeSheetobj.apply _)

  val addTimeSheetobjWrites = (
    (__ \ "TimeSheetID").writeNullable[Long] and  
    (__ \ "TimeSlotID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "CrtUSR").write[Long] and 
    (__ \ "CrtUSRType").write[String] and 
    (__ \ "UserID").write[Long] and 
     (__ \ "SlotDate").write[Date] and 
    (__ \ "SlotType").write[String] and 
    (__ \ "DutyType").writeNullable[String] and
    (__ \ "FromTime").writeNullable[String] and
    (__ \ "ToTime").writeNullable[String] and
    (__ \ "UserName").writeNullable[String] and
    (__ \ "Remarks").writeNullable[String])(unlift(addTimeSheetobj.unapply))
  implicit val addTimeSheetobjFormat: Format[addTimeSheetobj] = Format(addTimeSheetobjReads, addTimeSheetobjWrites)
}

 
 case class listAvailableTimeSlots(DoctorID: Option[Long], HospitalID: Option[Long], HospitalCD: Option[String],  
    PageNumber: Int, PageSize: Int)

object listAvailableTimeSlots {
  val listAvailableTimeSlotsReads = (
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] 
       )(listAvailableTimeSlots.apply _)

  val listAvailableTimeSlotsWrites = (
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] 
    
    )(unlift(listAvailableTimeSlots.unapply))
  implicit val listAvailableTimeSheetsFormat: Format[listAvailableTimeSlots] = Format(listAvailableTimeSlotsReads, listAvailableTimeSlotsWrites)
}