package models.com.hcue.doctors.Json.Request

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime
import myUtils.hcueFormats._

case class addDoctorCommObj(DoctorID: Long, GroupIDLst: Option[List[Long]], HospitalID: Option[Long], HospitalCD: Option[String], ParentHospitalID: Option[Long], MessageBody: String, Subject: Option[String], 
                              MessageType: String, IncludeAll: Option[String], PatientIDs: Option[List[Long]],FiltersApplied: Option[GroupFiltersAppliedObj] , USRId: Long, USRType: String)
  
object addDoctorCommObj {

  val addDoctorCommObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "GroupIDLst").readNullable[List[Long]] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "MessageBody").read[String] and
    (__ \ "Subject").readNullable[String] and
    (__ \ "MessageType").read[String] and
    (__ \ "IncludeAll").readNullable[String] and
    (__ \ "PatientIDs").readNullable[List[Long]] and
     (__ \ "FiltersApplied").readNullable[GroupFiltersAppliedObj] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addDoctorCommObj.apply _)
  
  val addDoctorCommObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "GroupIDLst").writeNullable[List[Long]] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "ParentHospitalID").writeNullable[Long] and
    (__ \ "MessageBody").write[String] and
    (__ \ "Subject").writeNullable[String] and
    (__ \ "MessageType").write[String] and
    (__ \ "IncludeAll").writeNullable[String] and
    (__ \ "PatientIDs").writeNullable[List[Long]] and
    (__ \ "FiltersApplied").writeNullable[GroupFiltersAppliedObj] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addDoctorCommObj.unapply))
  implicit val addDoctorCommObjFormat: Format[addDoctorCommObj] = Format(addDoctorCommObjReads, addDoctorCommObjWrites)

}

case class PatientCommInfo(PatientID: Long, DestinationID: String, TokenID: Option[String], Status: Option[String])
  
object PatientCommInfo {

  val PatientCommInfoReads = (
    (__ \ "PatientID").read[Long] and
    (__ \ "DestinationID").read[String] and
    (__ \ "TokenID").readNullable[String] and
    (__ \ "Status").readNullable[String])(PatientCommInfo.apply _)
  
  val PatientCommInfoWrites = (
    (__ \ "PatientID").write[Long] and
    (__ \ "DestinationID").write[String] and
    (__ \ "TokenID").writeNullable[String] and
    (__ \ "Status").writeNullable[String])(unlift(PatientCommInfo.unapply))
  implicit val addPatientCommInfoFormat: Format[PatientCommInfo] = Format(PatientCommInfoReads, PatientCommInfoWrites)

}



case class doctorCommInfoCountReq(DoctorID: Long,DoctorCommID:Option[Long], HospitalID: Option[Long], HospitalCD: Option[String],MessageType:String, StartDate: java.sql.Date,
                                  EndDate: java.sql.Date,PageNumber: Int,PageSize: Int,USRId: Long, USRType: String)
  
object doctorCommInfoCountReq {

  val doctorCommInfoCountReqReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "DoctorCommID").readNullable[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "MessageType").read[String] and
    (__ \ "StartDate").read[java.sql.Date] and
    (__ \ "EndDate").read[java.sql.Date] and 
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(doctorCommInfoCountReq.apply _)
  
  val doctorCommInfoCountReqWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "DoctorCommID").writeNullable[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "MessageType").write[String] and
    (__ \ "StartDate").write[java.sql.Date] and
    (__ \ "EndDate").write[java.sql.Date] and 
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(doctorCommInfoCountReq.unapply))
  implicit val doctorCommInfoCountReqFormat: Format[doctorCommInfoCountReq] = Format(doctorCommInfoCountReqReads, doctorCommInfoCountReqWrites)

}

case class doctorCommStatusObj(Total_Delivered: Long, Total_Failed: Long, Total_Pending: Long,Total_Submitted:Long)

object doctorCommStatusObj {

  val doctorCommStatusObjReads = (
    (__ \ "Total_Delivered").read[Long] and
    (__ \ "Total_Failed").read[Long] and
    (__ \ "Total_Pending").read[Long] and
    (__ \ "Total_Submitted").read[Long])(doctorCommStatusObj.apply _)

  val doctorCommStatusObjWrites = (
    (__ \ "Total_Delivered").write[Long] and
    (__ \ "Total_Failed").write[Long] and
    (__ \ "Total_Pending").write[Long] and
    (__ \ "Total_Submitted").write[Long])(unlift(doctorCommStatusObj.unapply))
  implicit val doctorCommStatusObjFormat: Format[doctorCommStatusObj] = Format(doctorCommStatusObjReads, doctorCommStatusObjWrites)

}

case class listDoctorCommObj(DoctorCommID: Long, DoctorID: Long, HospitalID: Option[Long], MessageBody: String, Subject: String, 
                              MessageType: String, PatientInfo: Option[Array[ListPatientInfo]], USRId: Long, USRType: String)
  
object listDoctorCommObj {

  val listDoctorCommObjReads = (
    (__ \ "DoctorCommID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "MessageBody").read[String] and
    (__ \ "Subject").read[String] and
    (__ \ "MessageType").read[String] and
    (__ \ "PatientInfo").readNullable[Array[ListPatientInfo]] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(listDoctorCommObj.apply _)
  
  val listDoctorCommObjWrites = (
    (__ \ "DoctorCommID").write[Long] and  
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "MessageBody").write[String] and
    (__ \ "Subject").write[String] and
    (__ \ "MessageType").write[String] and
    (__ \ "PatientInfo").writeNullable[Array[ListPatientInfo]] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(listDoctorCommObj.unapply))
  implicit val listDoctorCommObjFormat: Format[listDoctorCommObj] = Format(listDoctorCommObjReads, listDoctorCommObjWrites)

}

case class listDoctorCommDtlsObj(DoctorID: Long,DoctorCommID :Option[Long],Message:Option[String],Status:doctorCommStatusObj,RequestDate:Option[java.sql.Date])

object listDoctorCommDtlsObj {

  val listDoctorCommDtlsObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "DoctorCommID").readNullable[Long] and
    (__ \ "Message").readNullable[String] and
    (__ \ "Status").read[doctorCommStatusObj] and
    (__ \ "RequestDate").readNullable[java.sql.Date])(listDoctorCommDtlsObj.apply _)

  val listDoctorCommDtlsObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "DoctorCommID").writeNullable[Long] and
    (__ \ "Message").writeNullable[String] and
    (__ \ "Status").write[doctorCommStatusObj] and
    (__ \ "RequestDate").writeNullable[java.sql.Date])(unlift(listDoctorCommDtlsObj.unapply))
  implicit val listDoctorCommDtlsObjFormat: Format[listDoctorCommDtlsObj] = Format(listDoctorCommDtlsObjReads, listDoctorCommDtlsObjWrites)

}

case class listDoctorSMSDtlsObj(Message: Option[String],Details :Option[Array[ListSMSInfo]])

object listDoctorSMSDtlsObj {

  val listDoctorSMSDtlsObjReads = (
    (__ \ "Message").readNullable[String] and
    (__ \ "Details").readNullable[Array[ListSMSInfo]])(listDoctorSMSDtlsObj.apply _)

  val listDoctorSMSDtlsObjWrites = (
    (__ \ "Message").writeNullable[String] and
    (__ \ "Details").writeNullable[Array[ListSMSInfo]])(unlift(listDoctorSMSDtlsObj.unapply))
  implicit val listDoctorCommDtlsObjFormat: Format[listDoctorSMSDtlsObj] = Format(listDoctorSMSDtlsObjReads, listDoctorSMSDtlsObjWrites)

}

case class ListSMSInfo(PatientName: String, PatientID: Long, Status: String, PhoneNumber: String,MessageBody : String)
  
object ListSMSInfo {

  val ListPatientInfoReads = (
    (__ \ "PatientName").read[String] and  
    (__ \ "PatientID").read[Long] and
    (__ \ "Status").read[String] and
    (__ \ "PhoneNumber").read[String] and
    (__ \ "MessageBody").read[String])(ListSMSInfo.apply _)
  
  val ListPatientInfoWrites = (
    (__ \ "PatientName").write[String] and  
    (__ \ "PatientID").write[Long] and
    (__ \ "Status").write[String] and
    (__ \ "PhoneNumber").write[String] and
    (__ \ "MessageBody").write[String])(unlift(ListSMSInfo.unapply))
  implicit val ListPatientInfoFormat: Format[ListSMSInfo] = Format(ListPatientInfoReads, ListPatientInfoWrites)

}

case class ListPatientInfo(DoctorCommID: Long, MessageType:String,PatientID: Long, DestinationID: String, TokenID: Option[String], Status: Option[String])
  
object ListPatientInfo {

  val ListPatientInfoReads = (
    (__ \ "DoctorCommID").read[Long] and  
     (__ \ "MessageType").read[String] and
    (__ \ "PatientID").read[Long] and
    (__ \ "DestinationID").read[String] and
    (__ \ "TokenID").readNullable[String] and
    (__ \ "Status").readNullable[String])(ListPatientInfo.apply _)
  
  val ListPatientInfoWrites = (
    (__ \ "DoctorCommID").write[Long] and 
    (__ \ "MessageType").write[String] and
    (__ \ "PatientID").write[Long] and
    (__ \ "DestinationID").write[String] and
    (__ \ "TokenID").writeNullable[String] and
    (__ \ "Status").writeNullable[String])(unlift(ListPatientInfo.unapply))
  implicit val ListPatientInfoFormat: Format[ListPatientInfo] = Format(ListPatientInfoReads, ListPatientInfoWrites)

}
 
case class addPatientsToGroupReqObj(GroupID: Long, ExistGroupIDs: Option[List[Long]],  GroupDesc: Option[String],  AddPatientIDs:Option[List[Long]],
                   RemovePatientIDs: Option[List[Long]], FiltersApplied: Option[GroupFiltersAppliedObj],
                   DoctorID: Option[Long], HospitalID: Long, HospitalCD: String, ParentHospitalID: Long, MessageType: String,
                   ActiveIND: Option[String], RemovePatientsFlag: Option[String], AddPatientsFlag: Option[String], Count:Option[Long],
                   IncludeAll: Option[String], USRId: Long, USRType: String)

object addPatientsToGroupReqObj {

  val addPatientsToGroupReqObjReads = (
    (__ \ "GroupID").read[Long] and
    (__ \ "ExistGroupIDs").readNullable[List[Long]] and
    (__ \ "GroupDesc").readNullable[String] and
    (__ \ "AddPatientIDs").readNullable[List[Long]] and
    (__ \ "RemovePatientIDs").readNullable[List[Long]] and
    (__ \ "FiltersApplied").readNullable[GroupFiltersAppliedObj] and
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "HospitalID").read[Long] and
    (__ \ "HospitalCD").read[String] and
    (__ \ "ParentHospitalID").read[Long] and
    (__ \ "MessageType").read[String] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "RemovePatientsFlag").readNullable[String] and
    (__ \ "AddPatientsFlag").readNullable[String] and
    (__ \ "Count").readNullable[Long] and
    (__ \ "IncludeAll").readNullable[String] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addPatientsToGroupReqObj.apply _)

  val addPatientsToGroupReqObjWrites = (
    (__ \ "GroupID").write[Long] and
    (__ \ "ExistGroupIDs").writeNullable[List[Long]] and
    (__ \ "GroupDesc").writeNullable[String] and
    (__ \ "AddPatientIDs").writeNullable[List[Long]] and
    (__ \ "RemovePatientIDs").writeNullable[List[Long]] and
    (__ \ "FiltersApplied").writeNullable[GroupFiltersAppliedObj] and
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "HospitalID").write[Long] and
    (__ \ "HospitalCD").write[String] and
    (__ \ "ParentHospitalID").write[Long] and
     (__ \ "MessageType").write[String] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "RemovePatientsFlag").writeNullable[String] and
    (__ \ "AddPatientsFlag").writeNullable[String] and
    (__ \ "Count").writeNullable[Long] and
    (__ \ "IncludeAll").writeNullable[String] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addPatientsToGroupReqObj.unapply))
  implicit val addPatientsToGroupReqObjFormat: Format[addPatientsToGroupReqObj] = Format(addPatientsToGroupReqObjReads, addPatientsToGroupReqObjWrites)

}



case class GroupFiltersAppliedObj(StartAge: Option[Int],EndAge: Option[Int], ReferralID: Option[Long], SubReferralID: Option[Long], Gender: Option[String], StartDate: Option[String],
            EndDate: Option[String],SearchText: Option[String], HospitalIDLst: Option[List[Long]],
            SearchCountry: Option[String], SearchState: Option[String], SearchCityTown: Option[String],
            SearchLocation: Option[List[String]], PatientType: Option[String])
 
object GroupFiltersAppliedObj {

  val GroupFiltersAppliedObjReads = (
    (__ \ "StartAge").readNullable[Int] and
    (__ \ "EndAge").readNullable[Int] and
     (__ \ "ReferralID").readNullable[Long] and
    (__ \ "SubReferralID").readNullable[Long] and
    (__ \ "Gender").readNullable[String] and
    (__ \ "StartDate").readNullable[String] and
    (__ \ "EndDate").readNullable[String] and
    (__ \ "SearchText").readNullable[String] and
    
    (__ \ "HospitalIDLst").readNullable[List[Long]] and
    (__ \ "SearchCountry").readNullable[String] and
    (__ \ "SearchState").readNullable[String] and
    (__ \ "SearchCityTown").readNullable[String] and
    (__ \ "SearchLocation").readNullable[List[String]] and
    (__ \ "PatientType").readNullable[String] )(GroupFiltersAppliedObj.apply _)

  val GroupFiltersAppliedObjWrites = (
    (__ \ "StartAge").writeNullable[Int] and
    (__ \ "EndAge").writeNullable[Int] and
     (__ \ "ReferralID").writeNullable[Long] and
    (__ \ "SubReferralID").writeNullable[Long] and
    (__ \ "Gender").writeNullable[String] and
    (__ \ "StartDate").writeNullable[String] and
    (__ \ "EndDate").writeNullable[String] and
    (__ \ "SearchText").writeNullable[String] and
   
    (__ \ "HospitalIDLst").writeNullable[List[Long]] and
     (__ \ "SearchCountry").writeNullable[String] and
    (__ \ "SearchState").writeNullable[String] and
    (__ \ "SearchCityTown").writeNullable[String] and
    (__ \ "SearchLocation").writeNullable[List[String]] and
    (__ \ "PatientType").writeNullable[String] )(unlift(GroupFiltersAppliedObj.unapply))
  implicit val GroupFiltersAppliedObjFormat: Format[GroupFiltersAppliedObj] = Format(GroupFiltersAppliedObjReads, GroupFiltersAppliedObjWrites)

}


case class listDoctorCommGroupDtls(GroupID: Option[Long],GroupDesc: Option[String],DoctorID: Option[Long], HospitalID: Option[Long],
                                          HospitalCD: Option[String], ParentHospitalID: Option[Long], USRId: Long, USRType: String)

object listDoctorCommGroupDtls {

  val listDoctorCommGroupDtlsReads = (
    (__ \ "GroupID").readNullable[Long] and
    (__ \ "GroupDesc").readNullable[String] and
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String] 
   )(listDoctorCommGroupDtls.apply _)

  val listDoctorCommGroupDtlsWrites = (
    (__ \ "GroupID").writeNullable[Long] and 
    (__ \ "GroupDesc").writeNullable[String] and 
    (__ \ "DoctorID").writeNullable[Long] and 
    (__ \ "HospitalID").writeNullable[Long] and 
    (__ \ "HospitalCD").writeNullable[String] and 
    (__ \ "ParentHospitalID").writeNullable[Long] and 
    (__ \ "USRId").write[Long] and 
    (__ \ "USRType").write[String] 
  )(unlift(listDoctorCommGroupDtls.unapply))
  implicit val listDoctorCommGroupDtlsFormat: Format[listDoctorCommGroupDtls] = Format(listDoctorCommGroupDtlsReads, listDoctorCommGroupDtlsWrites)

}

