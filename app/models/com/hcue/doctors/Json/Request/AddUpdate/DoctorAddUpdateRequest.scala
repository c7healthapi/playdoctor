/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.AddUpdate

import java.sql.Date
import java.util.Date
import java.util.Date

import scala.slick.driver.PostgresDriver.simple._

import myUtils.hcueFormats._
import play.api.libs.functional.syntax._
import play.api.libs.json._
import models.com.hcue.doctor.Json.Request.AddUpdateConsultation.addConsultationRecord

case class addDoctorDetailsObj(FirstName: String, LastName: Option[String], FullName: String, Gender: Option[String], DOB: Option[java.sql.Date],
                               SpecialityCD: Option[Map[String, String]], MemberID: Option[Map[String, String]], Qualification: Option[Map[String, String]], Exp: Option[Int],
                               DoctorLoginID: Option[String], DoctorPassword: Option[String], About: Option[Map[String, String]],
                               HcueScore: Option[Float], Awards: Option[Map[String, String]],RegistrationNo: Option[String],
                               Membership: Option[Map[String, String]], Services: Option[Map[String, String]],
                               Prospect: Option[String], ReferredBy: Option[Long], TermsAccepted: String, Title:Option[String] )
object addDoctorDetailsObj {
  val addDoctorDetailsObjReads = ((__ \ "FirstName").read(Reads.maxLength[String](50)) and 
    (__ \ "LastName").readNullable(Reads.maxLength[String](50)) and
    (__ \ "FullName").read(Reads.maxLength[String](150)) and
    (__ \ "Gender").readNullable(Reads.maxLength[String](1)) and 
    (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "SpecialityCD").readNullable[Map[String, String]] and 
    (__ \ "MemberID").readNullable[Map[String, String]] and 
    (__ \ "Qualification").readNullable[Map[String, String]] and
    (__ \ "Exp").readNullable[Int] and 
    (__ \ "DoctorLoginID").readNullable(Reads.maxLength[String](100)) and
    (__ \ "DoctorPassword").readNullable(Reads.maxLength[String](20)) and
    (__ \ "About").readNullable[Map[String, String]] and
    (__ \ "HcueScore").readNullable[Float] and
    (__ \ "Awards").readNullable[Map[String, String]] and
    (__ \ "RegistrationNo").readNullable[String] and
    (__ \ "Membership").readNullable[Map[String, String]] and
    (__ \ "Services").readNullable[Map[String, String]] and
    (__ \ "Prospect").readNullable[String] and
    (__ \ "ReferredBy").readNullable[Long] and
    (__ \ "TermsAccepted").read(Reads.maxLength[String](1)) and 
    (__ \ "Title").readNullable[String] )(addDoctorDetailsObj.apply _)

  val addDoctorDetailsObjWrites = ((__ \ "FirstName").write[String] and 
    (__ \ "LastName").writeNullable[String] and 
    (__ \ "FullName").write[String] and
    (__ \ "Gender").writeNullable[String] and 
    (__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "SpecialityCD").writeNullable[Map[String, String]] and 
    (__ \ "MemberID").writeNullable[Map[String, String]] and 
    (__ \ "Qualification").writeNullable[Map[String, String]] and
    (__ \ "Exp").writeNullable[Int] and 
    (__ \ "DoctorLoginID").writeNullable[String] and
    (__ \ "DoctorPassword").writeNullable[String] and
    (__ \ "About").writeNullable[Map[String, String]] and
    (__ \ "HcueScore").writeNullable[Float] and
    (__ \ "Awards").writeNullable[Map[String, String]] and
    (__ \ "RegistrationNo").writeNullable[String] and
    (__ \ "Membership").writeNullable[Map[String, String]] and
    (__ \ "Services").writeNullable[Map[String, String]] and
    (__ \ "Prospect").writeNullable[String] and
    (__ \ "ReferredBy").writeNullable[Long] and
    (__ \ "TermsAccepted").write[String] and 
    (__ \ "Title").writeNullable[String])(unlift(addDoctorDetailsObj.unapply))
  implicit val addDoctorDetailsFormat: Format[addDoctorDetailsObj] = Format(addDoctorDetailsObjReads, addDoctorDetailsObjWrites)
}


case class updateDoctorLoginIDObj(DoctorID: Long, DoctorLoginID: String, DoctorPassword: String, USRId: Int, USRType: String)

object updateDoctorLoginIDObj {
  val updateDoctorLoginIDObjReads = (
    (__ \ "DoctorID").read[Long] and
      (__ \ "DoctorLoginID").read[String] and
      (__ \ "DoctorPassword").read[String] and
      (__ \ "USRId").read[Int] and
      (__ \ "USRType").read[String])(updateDoctorLoginIDObj.apply _)
  val updateDoctorLoginIDObjWrites = (
    (__ \ "DoctorID").write[Long] and
      (__ \ "DoctorLoginID").write[String] and
      (__ \ "DoctorPassword").write[String] and
      (__ \ "USRId").write[Int] and
      (__ \ "USRType").write[String])(unlift(updateDoctorLoginIDObj.unapply))
  implicit val updateDoctorLoginID: Format[updateDoctorLoginIDObj] = Format(updateDoctorLoginIDObjReads, updateDoctorLoginIDObjWrites)
}

case class updateDoctorLastLoginObj(DoctorID: Long, USRId: Int, USRType: String)

object updateDoctorLastLoginObj {
  val updateDoctorLastLoginObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updateDoctorLastLoginObj.apply _)
  val updateDoctorLastLoginObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(updateDoctorLastLoginObj.unapply))
  implicit val updateDoctorLoginID: Format[updateDoctorLastLoginObj] = Format(updateDoctorLastLoginObjReads, updateDoctorLastLoginObjWrites)
}



case class HospitalDetails(HospitalCD: Option[String] ,HospitalID: Option[Long],ParentHospitalID: Option[Long])

object HospitalDetails {
 val HospitalDetailsReads = ((__ \ "HospitalCD").readNullable[String] and
     (__ \ "HospitalID").readNullable[Long] and
     (__ \ "ParentHospitalID").readNullable[Long])(HospitalDetails.apply _)
 
 val HospitalDetailsWrites = (
 (__ \ "HospitalCD").writeNullable[String] and
 (__ \ "HospitalID").writeNullable[Long] and
 (__ \ "ParentHospitalID").writeNullable[Long])(unlift(HospitalDetails.unapply))
 
 implicit val HospitalDetailsFormat: Format[HospitalDetails] = Format(HospitalDetailsReads, HospitalDetailsWrites)
}


case class addDoctorObj(doctorDetails: addDoctorDetailsObj, doctorOtherDetails: Option[doctorOtherDetailsObj], USRId: Long, USRType: String, DoctorAddress: Option[Array[DoctorAddressDetailsObj]],DoctorSettings:Option[Map[String, String]],
                        DoctorPhone: Option[Array[DoctorPhoneDetailsObj]], DoctorEmail: Option[Array[DoctorEmailDetailsObj]],
                        DoctorEducation:Option[Array[doctorEducationObj]],DoctorPublishing:Option[Array[doctorPublicationAchievementObj]],
                        DoctorAchievements:Option[Array[doctorPublicationAchievementObj]], DoctorDepartments:Option[Array[doctorDepartmentsObj]], 
                        Searchable: Option[String], CustomSpecialityDesc: Option[Map[String, String]], 
                        HospitalDetails : Option[HospitalDetails], SendCredential:Option[String],AccountDetails : Option[AccountDetails],
                        PractiesDetails : Option[Array[PractiesDetails]],SpecialityDetails:Option[Array[SpecialityDetailsObj]],isPracticeDoctor:Option[Boolean])

object addDoctorObj {
  val addDoctorObjReads = (
    (__ \ "doctorDetails").read[addDoctorDetailsObj] and
    (__ \ "doctorOtherDetails").readNullable[doctorOtherDetailsObj] and
    (__ \ "USRId").read[Long] and 
    (__ \ "USRType").read(Reads.maxLength[String](150)) and
    (__ \ "DoctorAddress").readNullable[Array[DoctorAddressDetailsObj]] and
    (__ \ "DoctorSettings").readNullable[Map[String, String]] and
    (__ \ "DoctorPhone").readNullable[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").readNullable[Array[DoctorEmailDetailsObj]] and
    (__ \ "DoctorEducation").readNullable[Array[doctorEducationObj]] and
    (__ \ "DoctorPublishing").readNullable[Array[doctorPublicationAchievementObj]] and
    (__ \ "DoctorAchievements").readNullable[Array[doctorPublicationAchievementObj]] and
    (__ \ "DoctorDepartments").readNullable[Array[doctorDepartmentsObj]] and
    (__ \ "Searchable").readNullable[String] and
    (__ \ "CustomSpecialityDesc").readNullable[Map[String, String]] and
    (__ \ "HospitalDetails").readNullable[HospitalDetails] and
    (__ \ "SendCredential").readNullable[String] and
    (__ \ "AccountDetails").readNullable[AccountDetails] and 
    (__ \ "PractiesDetails").readNullable[Array[PractiesDetails]] and
    (__ \ "SpecialityDetails").readNullable[Array[SpecialityDetailsObj]] and
    (__ \ "isPracticeDoctor").readNullable[Boolean])(addDoctorObj.apply _)

  val addDoctorObjWrites = (
    (__ \ "doctorDetails").write[addDoctorDetailsObj] and
    (__ \ "doctorOtherDetails").writeNullable[doctorOtherDetailsObj] and
    (__ \ "CrtUSR").write[Long] and 
    (__ \ "CrtUSRType").write[String] and
    (__ \ "DoctorAddress").writeNullable[Array[DoctorAddressDetailsObj]] and
    (__ \ "DoctorSettings").writeNullable[Map[String, String]] and
    (__ \ "DoctorPhone").writeNullable[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").writeNullable[Array[DoctorEmailDetailsObj]] and
    (__ \ "DoctorEducation").writeNullable[Array[doctorEducationObj]] and
    (__ \ "DoctorPubliAchieve").writeNullable[Array[doctorPublicationAchievementObj]] and
    (__ \ "DoctorAchievements").writeNullable[Array[doctorPublicationAchievementObj]] and
    (__ \ "DoctorDepartments").writeNullable[Array[doctorDepartmentsObj]] and
    (__ \ "Searchable").writeNullable[String] and 
    (__ \ "CustomSpecialityDesc").writeNullable[Map[String, String]] and
    (__ \ "HospitalDetails").writeNullable[HospitalDetails] and
    (__ \ "SendCredential").writeNullable[String] and
    (__ \ "AccountDetails").writeNullable[AccountDetails] and 
    (__ \ "PractiesDetails").writeNullable[Array[PractiesDetails]] and
    (__ \ "SpecialityDetails").writeNullable[Array[SpecialityDetailsObj]] and
    (__ \ "isPracticeDoctor").writeNullable[Boolean])(unlift(addDoctorObj.unapply))
  implicit val addDoctorFormat: Format[addDoctorObj] = Format(addDoctorObjReads, addDoctorObjWrites)
}

case class PractiesDetails(practiceid: Long ,practicename: Option[String], practicecode: Option[String],
                           postcode: Option[String],ccgname: Option[String],contactnumber: Option[String],active: Boolean)

object PractiesDetails {
 val PractiesDetailsReads = (
     (__ \ "practiceid").read[Long] and
     (__ \ "practicename").readNullable[String] and
     (__ \ "practicecode").readNullable[String] and
     (__ \ "postcode").readNullable[String] and
     (__ \ "ccgname").readNullable[String] and
     (__ \ "contactnumber").readNullable[String] and
     (__ \ "active").read[Boolean])(PractiesDetails.apply _)
 
 val PractiesDetailsWrites = (
     (__ \ "practiceid").write[Long] and
     (__ \ "practicename").writeNullable[String] and
     (__ \ "practicecode").writeNullable[String] and
     (__ \ "postcode").writeNullable[String] and
     (__ \ "ccgname").writeNullable[String] and
     (__ \ "contactnumber").writeNullable[String] and
     (__ \ "active").write[Boolean])(unlift(PractiesDetails.unapply))
 
 implicit val PractiesDetailsFormat: Format[PractiesDetails] = Format(PractiesDetailsReads, PractiesDetailsWrites)
}

case class SpecialityDetailsObj(specialityID:String,specialityDesc:Option[String],ActiveIND:Option[String],SubSpecialityID:Option[Map[String,String]])

object SpecialityDetailsObj {
  val SpecialityDetailsObjReads = (
    (__ \ "specialityID").read[String] and
    (__ \ "specialityDesc").readNullable[String] and
    (__ \ "ActiveIND").readNullable[String] and 
    (__ \ "SubSpecialityID").readNullable[Map[String,String]])(SpecialityDetailsObj.apply _)

  val SpecialityDetailsObjWrites = (
    (__ \ "specialityID").write[String] and
    (__ \ "specialityDesc").writeNullable[String] and
    (__ \ "ActiveIND").writeNullable[String] and 
    (__ \ "SubSpecialityID").writeNullable[Map[String,String]])(unlift(SpecialityDetailsObj.unapply))
  implicit val SpecialityDetailsObjWritesFormat: Format[SpecialityDetailsObj] = Format(SpecialityDetailsObjReads, SpecialityDetailsObjWrites)

}

case class doctorOtherDetailsObj(PanCardID: Option[String], ActiveIND: Option[String],
     faxno : Option[String],gpcode: Option[String],typevalue: Option[String],notes:Option[String],isDeleted:Option[String])

object doctorOtherDetailsObj {
  val doctorOtherDetailsObjReads = (
     
    (__ \ "PanCardID").readNullable[String] and 
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "faxno").readNullable[String] and
    (__ \ "gpcode").readNullable[String] and
    (__ \ "typevalue").readNullable[String] and
    (__ \ "notes").readNullable[String] and
    (__ \ "isDeleted").readNullable[String])(doctorOtherDetailsObj.apply _)

  val doctorOtherDetailsObjWrites = (
   
    (__ \ "PanCardID").writeNullable[String] and
    (__ \ "ActiveIND").writeNullable[String] and 
    (__ \ "faxno").writeNullable[String] and
    (__ \ "gpcode").writeNullable[String] and
    (__ \ "typevalue").writeNullable[String] and
    (__ \ "notes").writeNullable[String] and
    (__ \ "isDeleted").writeNullable[String])(unlift(doctorOtherDetailsObj.unapply))
  implicit val doctorOtherDetailsObjFormat: Format[doctorOtherDetailsObj] = Format(doctorOtherDetailsObjReads, doctorOtherDetailsObjWrites)
}

case class doctorEducationObj(RowID:Int,EducationName:String,InstituteName:String,YearOfPassedOut:String)

object doctorEducationObj {
  val doctorEducationObjReads = (
    (__ \ "RowID").read[Int] and
    (__ \ "EducationName").read[String] and
    (__ \ "InstituteName").read[String] and
    (__ \ "YearOfPassedOut").read[String])(doctorEducationObj.apply _)

  val doctorEducationObjWrites = (
    (__ \ "RowID").write[Int] and
    (__ \ "EducationName").write[String] and
    (__ \ "InstituteName").write[String] and
    (__ \ "YearOfPassedOut").write[String])(unlift(doctorEducationObj.unapply))
  implicit val updateDoctorDetailsFormat: Format[doctorEducationObj] = Format(doctorEducationObjReads, doctorEducationObjWrites)
}

case class doctorPublicationAchievementObj(RowID:Long,Name:String,Details:Option[Map[String, String]],Year:String,Indicator:String)

object doctorPublicationAchievementObj {
  val doctorPubAchObjReads = (
    (__ \ "RowID").read[Long] and
    (__ \ "Name").read[String] and
    (__ \ "Details").readNullable[Map[String, String]] and
    (__ \ "Year").read[String] and
    (__ \ "Indicator").read[String])(doctorPublicationAchievementObj.apply _)

  val doctorPubAchObjWrites = (
    (__ \ "RowID").write[Long] and
    (__ \ "Name").write[String] and
    (__ \ "Details").writeNullable[Map[String, String]] and
    (__ \ "Year").write[String] and
    (__ \ "Indicator").write[String])(unlift(doctorPublicationAchievementObj.unapply))
  implicit val doctorPubAchObjFormat: Format[doctorPublicationAchievementObj] = Format(doctorPubAchObjReads, doctorPubAchObjWrites)
}

case class doctorDepartmentsObj(DoctorDepartmentID: Option[Long], DepartmentID:Long, SubDepartmentID:Option[Long], DoctorConsultationFee: Option[Int], BillingStructure: Option[JsValue], ActiveIND: String)

object doctorDepartmentsObj {
  val doctorDepartmentsObjReads = (
    (__ \ "DoctorDepartmentID").readNullable[Long] and
    (__ \ "DepartmentID").read[Long] and
    (__ \ "SubDepartmentID").readNullable[Long] and
    (__ \ "DoctorConsultationFee").readNullable[Int] and
    (__ \ "BillingStructure").readNullable[JsValue] and
    (__ \ "ActiveIND").read[String])(doctorDepartmentsObj.apply _)

  val doctorDepartmentsObjWrites = (
    (__ \ "DoctorDepartmentID").writeNullable[Long] and
    (__ \ "DepartmentID").write[Long] and
    (__ \ "SubDepartmentID").writeNullable[Long] and
    (__ \ "DoctorConsultationFee").writeNullable[Int] and
    (__ \ "BillingStructure").writeNullable[JsValue] and
    (__ \ "ActiveIND").write[String])(unlift(doctorDepartmentsObj.unapply))
  implicit val doctorPubAchObjFormat: Format[doctorDepartmentsObj] = Format(doctorDepartmentsObjReads, doctorDepartmentsObjWrites)
}

case class updateDoctorDetailsObj(FirstName: String, LastName: Option[String], FullName: Option[String], DoctorID: Long, Gender: Option[String],
                                  DOB: Option[java.sql.Date], SpecialityCD: Option[Map[String, String]], MemberID: Option[Map[String, String]], Qualification: Option[Map[String, String]],
                                  Exp: Option[Int], DoctorLoginID: Option[String], DoctorPassword: Option[String], About: Option[Map[String, String]],
                                  HcueScore: Option[Float], Awards: Option[Map[String, String]],
                                  RegistrationNo: Option[String], Services: Option[Map[String, String]], Prospect: Option[String], ReferredBy: Option[Long],TermsAccepted: String,Title: Option[String])

object updateDoctorDetailsObj {
  val updateDoctorDetailsObjReads = ((__ \ "FirstName").read(Reads.maxLength[String](50)) and
    (__ \ "LastName").readNullable(Reads.maxLength[String](50)) and
    (__ \ "FullName").readNullable(Reads.maxLength[String](50)) and
    (__ \ "DoctorID").read(Reads.min[Long](1)) and
    (__ \ "Gender").readNullable(Reads.maxLength[String](50)) and
    (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "SpecialityCD").readNullable[Map[String, String]] and
    (__ \ "MemberID").readNullable[Map[String, String]] and
    (__ \ "Qualification").readNullable[Map[String, String]] and
    (__ \ "Exp").readNullable[Int] and
    (__ \ "DoctorLoginID").readNullable(Reads.maxLength[String](100)) and
    (__ \ "DoctorPassword").readNullable(Reads.maxLength[String](20)) and
    (__ \ "About").readNullable[Map[String, String]] and
    (__ \ "HcueScore").readNullable[Float] and
    (__ \ "Awards").readNullable[Map[String, String]] and
    (__ \ "RegistrationNo").readNullable[String] and
    (__ \ "Services").readNullable[Map[String, String]] and
    (__ \ "Prospect").readNullable[String] and
    (__ \ "ReferredBy").readNullable[Long] and
    (__ \ "TermsAccepted").read(Reads.maxLength[String](1)) and
    (__ \ "Title").readNullable[String])(updateDoctorDetailsObj.apply _)

  val updateDoctorDetailsObjWrites = ((__ \ "FirstName").write[String] and
    (__ \ "LastName").writeNullable[String] and
    (__ \ "FullName").writeNullable[String] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "Gender").writeNullable[String] and
    (__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "SpecialityCD").writeNullable[Map[String, String]] and
    (__ \ "MemberID").writeNullable[Map[String, String]] and
    (__ \ "Qualification").writeNullable[Map[String, String]] and
    (__ \ "Exp").writeNullable[Int] and
    (__ \ "DoctorLoginID").writeNullable[String] and
    (__ \ "DoctorPassword").writeNullable[String] and
    (__ \ "About").writeNullable[Map[String, String]] and
    (__ \ "HcueScore").writeNullable[Float] and
    (__ \ "Awards").writeNullable[Map[String, String]] and
    (__ \ "Title").writeNullable[String] and
    (__ \ "Services").writeNullable[Map[String, String]] and
    (__ \ "Prospect").writeNullable[String] and
    (__ \ "ReferredBy").writeNullable[Long] and
    (__ \ "TermsAccepted").write[String] and
    (__ \ "Title").writeNullable[String])(unlift(updateDoctorDetailsObj.unapply))
  implicit val updateDoctorDetailsFormat: Format[updateDoctorDetailsObj] = Format(updateDoctorDetailsObjReads, updateDoctorDetailsObjWrites)
}

case class linkDoctorAddressDetailsObj(HospitalID:Long, ParentHospitalID: Option[Long], HospitalCode: Option[String],BranchCode: Option[String],Preference:Option[Map[String, String]])

object linkDoctorAddressDetailsObj {
  val linkDoctorAddressDetailsObjReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "HospitalCode").readNullable[String] and
    (__ \ "BranchCode").readNullable[String] and
     (__ \ "Preference").readNullable[Map[String, String]])(linkDoctorAddressDetailsObj.apply _)

  val linkDoctorAddressDetailsObjWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "ParentHospitalID").writeNullable[Long] and
    (__ \ "HospitalCode").writeNullable[String] and
    (__ \ "BranchCode").writeNullable[String] and
    (__ \ "Preference").writeNullable[Map[String, String]])(unlift(linkDoctorAddressDetailsObj.unapply))
  implicit val hospitalInfoFormat: Format[linkDoctorAddressDetailsObj] = Format(linkDoctorAddressDetailsObjReads, linkDoctorAddressDetailsObjWrites)
}

case class linkDoctorObj(doctorDetails: updateDoctorDetailsObj, doctorOtherDetails: Option[doctorOtherDetailsObj], CareerImages:Option[Map[String,String]],USRId: Int, USRType: String,
                           DoctorAddress: Option[Array[linkDoctorAddressDetailsObj]],DoctorSettings:Option[Map[String,String]],
                           DoctorPhone: Option[Array[DoctorPhoneDetailsObj]], DoctorEmail: Option[Array[DoctorEmailDetailsObj]],
                           DoctorEducation:Option[Array[doctorEducationObj]],DoctorPublishing:Option[Array[doctorPublicationAchievementObj]],
                           DoctorAchievements:Option[Array[doctorPublicationAchievementObj]],DoctorDepartments:Option[Array[doctorDepartmentsObj]],
                           Searchable : Option[String],CustomSpecialityDesc: Option[Map[String, String]],
                           AccountDetails : Option[AccountDetails],DoctorRequestObj : Option[DoctorRequestObj])

object linkDoctorObj {
  val linkDoctorObjReads = (
    (__ \ "doctorDetails").read[updateDoctorDetailsObj] and
    (__ \ "doctorOtherDetails").readNullable[doctorOtherDetailsObj] and
    (__ \ "CareerImages").readNullable[Map[String,String]] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read(Reads.maxLength[String](50)) and
    (__ \ "DoctorAddress").readNullable[Array[linkDoctorAddressDetailsObj]] and
    (__ \ "DoctorSettings").readNullable[Map[String,String]] and
    (__ \ "DoctorPhone").readNullable[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").readNullable[Array[DoctorEmailDetailsObj]] and
    (__ \ "DoctorEducation").readNullable[Array[doctorEducationObj]] and 
    (__ \ "DoctorPublishing").readNullable[Array[doctorPublicationAchievementObj]] and 
    (__ \ "DoctorAchievements").readNullable[Array[doctorPublicationAchievementObj]] and 
    (__ \ "DoctorDepartments").readNullable[Array[doctorDepartmentsObj]] and 
    (__ \ "Searchable").readNullable[String] and 
    (__ \ "CustomSpecialityDesc").readNullable[Map[String, String]] and
    (__ \ "AccountDetails").readNullable[AccountDetails] and
    (__ \ "DoctorRequestObj").readNullable[DoctorRequestObj])(linkDoctorObj.apply _) // read[String](minLength(0)) yields compiler error

  val linkDoctorObjWrites = (
    (__ \ "doctorDetails").write[updateDoctorDetailsObj] and
    (__ \ "doctorOtherDetails").writeNullable[doctorOtherDetailsObj] and
    (__ \ "CareerImages").writeNullable[Map[String,String]] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String] and
    (__ \ "DoctorAddress").writeNullable[Array[linkDoctorAddressDetailsObj]] and
    (__ \ "DoctorSettings").writeNullable[Map[String,String]] and
    (__ \ "DoctorPhone").writeNullable[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").writeNullable[Array[DoctorEmailDetailsObj]] and
    (__ \ "DoctorEducation").writeNullable[Array[doctorEducationObj]] and 
    (__ \ "DoctorPublishing").writeNullable[Array[doctorPublicationAchievementObj]] and 
    (__ \ "DoctorAchievements").writeNullable[Array[doctorPublicationAchievementObj]] and
    (__ \ "DoctorDepartments").writeNullable[Array[doctorDepartmentsObj]] and 
    (__ \ "Searchable").writeNullable[String] and
    (__ \ "CustomSpecialityDesc").writeNullable[Map[String, String]] and
    (__ \ "AccountDetails").writeNullable[AccountDetails] and
    (__ \ "DoctorRequestObj").writeNullable[DoctorRequestObj])(unlift(linkDoctorObj.unapply))
  implicit val updateDoctorFormat: Format[linkDoctorObj] = Format(linkDoctorObjReads, linkDoctorObjWrites)
}

case class updateDoctorObj(doctorDetails: updateDoctorDetailsObj, doctorOtherDetails: Option[doctorOtherDetailsObj], CareerImages:Option[Map[String,String]],USRId: Int, USRType: String,
                           DoctorAddress: Option[Array[DoctorAddressDetailsObj]],DoctorSettings:Option[Map[String,String]],
                           DoctorPhone: Option[Array[DoctorPhoneDetailsObj]], DoctorEmail: Option[Array[DoctorEmailDetailsObj]],
                           DoctorEducation:Option[Array[doctorEducationObj]],DoctorPublishing:Option[Array[doctorPublicationAchievementObj]],
                           DoctorAchievements:Option[Array[doctorPublicationAchievementObj]],DoctorDepartments:Option[Array[doctorDepartmentsObj]],
                           Searchable : Option[String],CustomSpecialityDesc: Option[Map[String, String]],
                           AccountDetails : Option[AccountDetails],PractiesDetails : Option[Array[PractiesDetails]],SpecialityDetails:Option[Array[SpecialityDetailsObj]],isPracticeDoctor:Option[Boolean])
                           
                           object updateDoctorObj {
  val updateDoctorObjReads = (
    (__ \ "doctorDetails").read[updateDoctorDetailsObj] and
    (__ \ "doctorOtherDetails").readNullable[doctorOtherDetailsObj] and
    (__ \ "CareerImages").readNullable[Map[String,String]] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read(Reads.maxLength[String](50)) and
    (__ \ "DoctorAddress").readNullable[Array[DoctorAddressDetailsObj]] and
    (__ \ "DoctorSettings").readNullable[Map[String,String]] and
    (__ \ "DoctorPhone").readNullable[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").readNullable[Array[DoctorEmailDetailsObj]] and
    (__ \ "DoctorEducation").readNullable[Array[doctorEducationObj]] and 
    (__ \ "DoctorPublishing").readNullable[Array[doctorPublicationAchievementObj]] and 
    (__ \ "DoctorAchievements").readNullable[Array[doctorPublicationAchievementObj]] and 
    (__ \ "DoctorDepartments").readNullable[Array[doctorDepartmentsObj]] and 
    (__ \ "Searchable").readNullable[String] and 
    (__ \ "CustomSpecialityDesc").readNullable[Map[String, String]] and
    (__ \ "AccountDetails").readNullable[AccountDetails] and
    (__ \ "PractiesDetails").readNullable[Array[PractiesDetails]] and
    (__ \ "SpecialityDetails").readNullable[Array[SpecialityDetailsObj]] and
    (__ \ "isPracticeDoctor").readNullable[Boolean])(updateDoctorObj.apply _) // read[String](minLength(0)) yields compiler error

  val updateDoctorObjWrites = (
    (__ \ "doctorDetails").write[updateDoctorDetailsObj] and
    (__ \ "doctorOtherDetails").writeNullable[doctorOtherDetailsObj] and
    (__ \ "CareerImages").writeNullable[Map[String,String]] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String] and
    (__ \ "DoctorAddress").writeNullable[Array[DoctorAddressDetailsObj]] and
    (__ \ "DoctorSettings").writeNullable[Map[String,String]] and
    (__ \ "DoctorPhone").writeNullable[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").writeNullable[Array[DoctorEmailDetailsObj]] and
    (__ \ "DoctorEducation").writeNullable[Array[doctorEducationObj]] and 
    (__ \ "DoctorPublishing").writeNullable[Array[doctorPublicationAchievementObj]] and 
    (__ \ "DoctorAchievements").writeNullable[Array[doctorPublicationAchievementObj]] and
    (__ \ "DoctorDepartments").writeNullable[Array[doctorDepartmentsObj]] and 
    (__ \ "Searchable").writeNullable[String] and
    (__ \ "CustomSpecialityDesc").writeNullable[Map[String, String]] and
    (__ \ "AccountDetails").writeNullable[AccountDetails] and
    (__ \ "PractiesDetails").writeNullable[Array[PractiesDetails]] and
    (__ \ "SpecialityDetails").writeNullable[Array[SpecialityDetailsObj]] and
    (__ \ "isPracticeDoctor").writeNullable[Boolean])(unlift(updateDoctorObj.unapply))
  implicit val updateDoctorFormat: Format[updateDoctorObj] = Format(updateDoctorObjReads, updateDoctorObjWrites)
}


case class getDoctorInfoObj(DoctorID: Long, AddressID:Option[List[Long]],USRId: Long, USRType: String, profileImage: Option[Boolean])

object getDoctorInfoObj {
  val getDoctorInfoObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "AddressID").readNullable[List[Long]] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String] and
    (__ \ "profileImage").readNullable[Boolean])(getDoctorInfoObj.apply _)

  val getDoctorInfoObjWrites = (
    (__ \ "DoctorID").write[Long] and (__ \ "AddressID").writeNullable[List[Long]] and
    (__ \ "USRId").write[Long] and (__ \ "USRType").write[String]  and
    (__ \ "profileImage").writeNullable[Boolean])(unlift(getDoctorInfoObj.unapply))
  implicit val getDoctorInfoObjFormat: Format[getDoctorInfoObj] = Format(getDoctorInfoObjReads, getDoctorInfoObjWrites)
}

case class getHospitalInfoObj(HospitalID: Long, USRId: Long, USRType: String)

object getHospitalInfoObj {
  val getHospitalInfoObjReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(getHospitalInfoObj.apply _)

  val getHospitalInfoObjWrites = (
    (__ \ "HospitalID").write[Long] and 
    (__ \ "USRId").write[Long] and (__ \ "USRType").write[String])(unlift(getHospitalInfoObj.unapply))
  implicit val getHospitalInfoObjFormat: Format[getHospitalInfoObj] = Format(getHospitalInfoObjReads, getHospitalInfoObjWrites)
}


case class DoctorPhoneDetailsObj(PhCntryCD: Option[Int], PhStateCD: Option[Int], PhAreaCD: Option[Int], PhNumber: String, PhType: String, PrimaryIND: String, AuditTracker:Option[Map[String, String]])

object DoctorPhoneDetailsObj {
  val DoctorPhoneDetailsObjReads = (
    (__ \ "PhCntryCD").readNullable(Reads.min[Int](1)) and
    (__ \ "PhStateCD").readNullable(Reads.min[Int](1)) and
    (__ \ "PhAreaCD").readNullable(Reads.min[Int](1)) and
    (__ \ "PhNumber").read[String] and
    (__ \ "PhType").read(Reads.maxLength[String](10)) and
    (__ \ "PrimaryIND").read[String] and
     (__ \ "AuditTracker").readNullable[Map[String, String]])(DoctorPhoneDetailsObj.apply _) // read[String](minLength(0)) yields compiler error

  val DoctorPhoneDetailsObjWrites = (
    (__ \ "PhCntryCD").writeNullable[Int] and
    (__ \ "PhStateCD").writeNullable[Int] and
    (__ \ "PhAreaCD").writeNullable[Int] and
    (__ \ "PhNumber").write[String] and
    (__ \ "PhType").write[String] and
    (__ \ "PrimaryIND").write[String] and
    (__ \ "AuditTracker").writeNullable[Map[String, String]])(unlift(DoctorPhoneDetailsObj.unapply))
  implicit val DoctorPhoneDetailsFormat: Format[DoctorPhoneDetailsObj] = Format(DoctorPhoneDetailsObjReads, DoctorPhoneDetailsObjWrites)
}


case class DoctorAddressDetailsObj(AddressID: Option[Long], ClinicName: Option[String],ClinicSettings:Option[Map[String,String]], Address1: Option[String], 
                                   Address2: Option[String], Street: Option[String], Location: Option[String], CityTown: Option[String], DistrictRegion: Option[Long],
                                   State: Option[String], PinCode: Option[Int], Country: Option[String], AddressType: String,LandMark:Option[String],Active:Option[String],
                                   ExtDetails: Option[DoctorAddressExtnObj],ShowTimings : Option[String],PostCode:Option[String],Address4: Option[String])

object DoctorAddressDetailsObj {
  val DoctorAddressDetailsObjReads = (
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "ClinicName").readNullable[String] and
    (__ \ "ClinicSettings").readNullable[Map[String,String]] and
    (__ \ "Address1").readNullable(Reads.maxLength[String](250)) and
    (__ \ "Address2").readNullable[String] and
    (__ \ "Street").readNullable(Reads.maxLength[String](250)) and
    (__ \ "Location").readNullable[String] and
    (__ \ "CityTown").readNullable(Reads.maxLength[String](250)) and
    (__ \ "DistrictRegion").readNullable[Long] and
    (__ \ "State").readNullable[String] and
    (__ \ "PinCode").readNullable[Int] and
    (__ \ "Country").readNullable(Reads.maxLength[String](100)) and
    (__ \ "AddressType").read(Reads.maxLength[String](10)) and
    (__ \ "LandMark").readNullable[String] and
    (__ \ "Active").readNullable[String] and
    (__ \ "ExtDetails").readNullable[DoctorAddressExtnObj] and
    (__ \ "ShowTimings").readNullable[String] and 
    (__ \ "PostCode").readNullable[String] and
    (__ \ "Address4").readNullable[String])(DoctorAddressDetailsObj.apply _) // read[String](minLength(0)) yields compiler error

  val DoctorAddressDetailsObjWrites = (
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "ClinicName").writeNullable[String] and
    (__ \ "ClinicSettings").writeNullable[Map[String,String]] and
    (__ \ "Address1").writeNullable[String] and
    (__ \ "Address2").writeNullable[String] and
    (__ \ "Street").writeNullable[String] and
    (__ \ "Location").writeNullable[String] and
    (__ \ "CityTown").writeNullable[String] and
    (__ \ "DistrictRegion").writeNullable[Long] and
    (__ \ "State").writeNullable[String] and
    (__ \ "PinCode").writeNullable[Int] and
    (__ \ "Country").writeNullable[String] and
    (__ \ "AddressType").write[String] and
    (__ \ "LandMark").writeNullable[String] and
    (__ \ "Active").writeNullable[String] and
    (__ \ "ExtDetails").writeNullable[DoctorAddressExtnObj] and
    (__ \ "ShowTimings").writeNullable[String] and 
    (__ \ "PostCode").writeNullable[String] and
    (__ \ "Address4").writeNullable[String])(unlift(DoctorAddressDetailsObj.unapply))
  implicit val DoctorAddressDetailsFormat: Format[DoctorAddressDetailsObj] = Format(DoctorAddressDetailsObjReads, DoctorAddressDetailsObjWrites)
}


case class DoctordeactiveObj(AddressID: Option[Long],  
                              Active: Option[String], HospitalID: Option[Long], DoctorID: Option[Long],RoleID: Option[String])

object DoctordeactiveObj {
  val DoctordeactiveObjReads = (
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "Active").readNullable[String] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "RoleID").readNullable[String])(DoctordeactiveObj.apply _) // read[String](minLength(0)) yields compiler error

  val DoctordeactiveObjWrites = (
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "Active").writeNullable[String] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "RoleID").writeNullable[String])(unlift(DoctordeactiveObj.unapply))
  implicit val DoctorAddressDetailsFormat: Format[DoctordeactiveObj] = Format(DoctordeactiveObjReads, DoctordeactiveObjWrites)
}





case class DoctorAddressExtnObj(HospitalID: Option[Long], ParentHospitalID: Option[Long], PrimaryIND: String,
                                Latitude: Option[String], Longitude: Option[String],isSearchable: Option[String],
                                AddressCommunication : Option[Array[DoctorCommunicationObj]],ClinicImages:Option[Map[String,String]], 
                                HospitalInfo: Option[hospitalInfoObj], RoleID:Option[String],GroupID:Option[String], 
                                MinPerCase: Option[Int], RateperHour: Option[Float],ConsultDetails: Option[Array[addConsultationRecord]],
                                RateBelowTwenty: Option[BigDecimal],RateAboveTwenty: Option[BigDecimal])

object DoctorAddressExtnObj {
  val DoctorAddressExtnObjReads = (
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "PrimaryIND").read[String] and
    (__ \ "Latitude").readNullable[String] and
    (__ \ "Longitude").readNullable[String] and
    (__ \ "isSearchable").readNullable[String] and
    (__ \ "AddressCommunication").readNullable[Array[DoctorCommunicationObj]] and
    (__ \ "ClinicImages").readNullable[Map[String,String]] and
    (__ \ "HospitalInfo").readNullable[hospitalInfoObj] and
    (__ \ "RoleID").readNullable[String] and
    (__ \ "GroupID").readNullable[String] and
    (__ \ "MinPerCase").readNullable(Reads.min[Int](5)) and
    (__ \ "RateperHour").readNullable[Float] and
    (__ \ "ConsultDetails").readNullable[Array[addConsultationRecord]] and
    (__ \ "RateBelowTwenty").readNullable[BigDecimal] and
    (__ \ "RateAboveTwenty").readNullable[BigDecimal])(DoctorAddressExtnObj.apply _)

  val DoctorAddressExtnObjWrites = (
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "ParentHospitalID").writeNullable[Long] and
    (__ \ "PrimaryIND").write[String] and
    (__ \ "Latitude").writeNullable[String] and
    (__ \ "Longitude").writeNullable[String] and
    (__ \ "isSearchable").writeNullable[String] and
    (__ \ "AddressCommunication").writeNullable[Array[DoctorCommunicationObj]] and
    (__ \ "ClinicImages").writeNullable[Map[String,String]] and
    (__ \ "HospitalInfo").writeNullable[hospitalInfoObj] and
    (__ \ "RoleID").writeNullable[String] and
    (__ \ "GroupID").writeNullable[String] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "RateperHour").writeNullable[Float] and
    (__ \ "ConsultDetails").writeNullable[Array[addConsultationRecord]] and
    (__ \ "RateBelowTwenty").writeNullable[BigDecimal] and
    (__ \ "RateAboveTwenty").writeNullable[BigDecimal])(unlift(DoctorAddressExtnObj.unapply))
  implicit val DoctorAddressExtnObjFormat: Format[DoctorAddressExtnObj] = Format(DoctorAddressExtnObjReads, DoctorAddressExtnObjWrites)
}


case class AccessRightsInfo(groupCode:String,readOnly:String)
object AccessRightsInfo{
  val accessRightsInfoReads = (
      (__ \ "groupCode").read[String] and 
      (__ \ "readOnly").read[String])(AccessRightsInfo.apply _)

  val accessRightsInfoWrites = (
      (__ \ "groupCode").write[String] and 
      (__ \ "readOnly").write[String])(unlift(AccessRightsInfo.unapply))
  implicit val accessRightsInfoFormat: Format[AccessRightsInfo] = Format(accessRightsInfoReads, accessRightsInfoWrites)
}


case class hospitalInfoObj(HospitalID:Long, ParentHospitalID: Option[Long], HospitalCode: Option[String],BranchCode: Option[String],Preference:Option[Map[String, String]], CountryCodes:Option[Map[String, String]], HospitalDocuments: Option[JsValue])

object hospitalInfoObj {
  val hospitalInfoObjReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "HospitalCode").readNullable[String] and
    (__ \ "BranchCode").readNullable[String] and
    (__ \ "Preference").readNullable[Map[String, String]] and
    (__ \ "CountryCodes").readNullable[Map[String, String]] and
    (__ \ "HospitalDocuments").readNullable[JsValue])(hospitalInfoObj.apply _)

  val hospitalInfoObjWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "ParentHospitalID").writeNullable[Long] and
    (__ \ "HospitalCode").writeNullable[String] and
    (__ \ "BranchCode").writeNullable[String] and
    (__ \ "Preference").writeNullable[Map[String, String]] and
    (__ \ "CountryCodes").writeNullable[Map[String, String]] and
    (__ \ "HospitalDocuments").writeNullable[JsValue])(unlift(hospitalInfoObj.unapply))
  implicit val hospitalInfoFormat: Format[hospitalInfoObj] = Format(hospitalInfoObjReads, hospitalInfoObjWrites)
}



case class DoctorEmailDetailsObj(EmailID: String, EmailIDType: String, PrimaryIND: String, CrtUSR: Option[Long],
                                 CrtUSRType: Option[String], UpdtUSR: Option[Long], UpdtUSRType: Option[String], AuditTracker:Option[Map[String, String]] )

object DoctorEmailDetailsObj {
  val DoctorEmailDetailsObjReads = (
    (__ \ "EmailID").read(Reads.maxLength[String](100)) and
    (__ \ "EmailIDType").read(Reads.maxLength[String](10)) and
    (__ \ "PrimaryIND").read(Reads.maxLength[String](1)) and
    (__ \ "CrtUSR").readNullable[Long] and
    (__ \ "CrtUSRType").readNullable[String] and
    (__ \ "UpdtUSR").readNullable[Long] and
    (__ \ "UpdtUSRType").readNullable[String] and 
    (__ \ "AuditTracker").readNullable[Map[String, String]] )(DoctorEmailDetailsObj.apply _)

  val DoctorEmailDetailsObjWrites = (
    (__ \ "EmailID").write[String] and
    (__ \ "EmailIDType").write[String] and
    (__ \ "PrimaryIND").write[String] and
    (__ \ "CrtUSR").writeNullable[Long] and
    (__ \ "CrtUSRType").writeNullable[String] and
    (__ \ "UpdtUSR").writeNullable[Long] and
    (__ \ "UpdtUSRType").writeNullable[String] and
    (__ \ "AuditTracker").writeNullable[Map[String, String]] )(unlift(DoctorEmailDetailsObj.unapply))
  implicit val DoctorAddressDetailsFormat: Format[DoctorEmailDetailsObj] = Format(DoctorEmailDetailsObjReads, DoctorEmailDetailsObjWrites)
}
case class DoctorCommunicationObj(PhCntryCD: String, PhStateCD: String, PhAreaCD: String, PhNumber: String, PhType: String,RowID :Int, PrimaryIND: String,PublicDisplay : String)

object DoctorCommunicationObj {
  val DoctorCommunicationObjReads = (
    (__ \ "PhCntryCD").read[String] and
    (__ \ "PhStateCD").read[String] and
    (__ \ "PhAreaCD").read[String] and
    (__ \ "PhNumber").read[String] and
    (__ \ "PhType").read[String] and
    (__ \ "RowID").read[Int] and
    (__ \ "PrimaryIND").read[String] and
    (__ \ "PublicDisplay").read[String])(DoctorCommunicationObj.apply _) // read[String](minLength(0)) yields compiler error

  val DoctorCommunicationObjWrites = (
    (__ \ "PhCntryCD").write[String] and
    (__ \ "PhStateCD").write[String] and
    (__ \ "PhAreaCD").write[String] and
    (__ \ "PhNumber").write[String] and
    (__ \ "PhType").write[String] and
    (__ \ "RowID").write[Int] and
    (__ \ "PrimaryIND").write[String] and
    (__ \ "PublicDisplay").write[String])(unlift(DoctorCommunicationObj.unapply))
  implicit val DoctorCommunicationFormat: Format[DoctorCommunicationObj] = Format(DoctorCommunicationObjReads, DoctorCommunicationObjWrites)
}


case class AccountDetails(UserType: String ,Comments: Option[String], ExpiryDate: Option[java.sql.Date])

object AccountDetails {
 val AccountDetailsReads = (
     (__ \ "UserType").read[String] and
     (__ \ "Comments").readNullable[String] and
     (__ \ "ExpiryDate").readNullable[java.sql.Date])(AccountDetails.apply _)
 
 val AccountDetailsWrites = (
 (__ \ "UserType").write[String] and
 (__ \ "Comments").writeNullable[String] and 
 (__ \ "ExpiryDate").writeNullable[java.sql.Date])(unlift(AccountDetails.unapply))
 
 implicit val AccountDetailsFormat: Format[AccountDetails] = Format(AccountDetailsReads, AccountDetailsWrites)
}



case class DoctorRequestObj(RequestType: Option[String] ,Comments: Option[String])

object DoctorRequestObj {
 val DoctorRequestObjReads = (
     (__ \ "RequestType").readNullable[String] and
     (__ \ "Comments").readNullable[String])(DoctorRequestObj.apply _)
 
 val DoctorRequestObjWrites = (
 (__ \ "RequestType").writeNullable[String] and
 (__ \ "Comments").writeNullable[String])(unlift(DoctorRequestObj.unapply))
 
 implicit val AccountDetailsFormat: Format[DoctorRequestObj] = Format(DoctorRequestObjReads, DoctorRequestObjWrites)
}