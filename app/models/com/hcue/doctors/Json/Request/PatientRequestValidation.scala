/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date

import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime

case class addPatientObj(FirstName: String,
                         LastName: Option[String], FullName: String, AadhaarID: Option[Int], Gender: String,
                         DOB: Option[java.sql.Date], Age: Option[Int], BloodGroup: Option[String],
                         FamilyHdIND: Option[String], FamilyHdID: Option[Int], FamilyRltnType: String,
                         PatientLoginID: String, PatientPassword: String, PinPassCode: Option[Int],
                         TermsAccepted: String, USRId: Int, USRType: String,
                         patientPhone: Option[Array[patientPhoneDetailsObj]],
                         patientAddress: Option[Array[patientAddressDetailsObj]],
                         patientEmail: Option[Array[patientEmailDetailsObj]],
                         patientOtherID: Option[Array[patientOtherIdDetailsObj]])

object addPatientObj {
  val addPatientObjReads = (
    (__ \ "FirstName").read(Reads.minLength[String](1)) and (__ \ "LastName").readNullable[String] and
    (__ \ "FullName").read(Reads.minLength[String](1)) and (__ \ "AadhaarID").readNullable[Int] and
    (__ \ "Gender").read[String] and (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "Age").readNullable[Int] and (__ \ "BloodGroup").readNullable[String] and
    (__ \ "FamilyHdIND").readNullable[String] and (__ \ "FamilyHdID").readNullable[Int] and
    (__ \ "FamilyRltnType").read(Reads.minLength[String](1)) and (__ \ "PatientLoginID").read(Reads.minLength[String](1)) and
    (__ \ "PatientPassword").read[String] and (__ \ "PinPassCode").readNullable[Int] and
    (__ \ "TermsAccepted").read[String] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String] and
    (__ \ "patientPhone").readNullable[Array[patientPhoneDetailsObj]] and
    (__ \ "patientAddress").readNullable[Array[patientAddressDetailsObj]] and
    (__ \ "patientEmail").readNullable[Array[patientEmailDetailsObj]] and
    (__ \ "patientOtherID").readNullable[Array[patientOtherIdDetailsObj]])(addPatientObj.apply _)

  val addPatientObjWrites = (
    (__ \ "FirstName").write[String] and (__ \ "LastName").writeNullable[String] and
    (__ \ "FullName").write[String] and (__ \ "AadhaarID").writeNullable[Int] and
    (__ \ "Gender").write[String] and (__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "Age").writeNullable[Int] and (__ \ "BloodGroup").writeNullable[String] and
    (__ \ "FamilyHdIND").writeNullable[String] and (__ \ "FamilyHdID").writeNullable[Int] and
    (__ \ "FamilyRltnType").write[String] and (__ \ "PatientLoginID").write[String] and
    (__ \ "PatientPassword").write[String] and (__ \ "PinPassCode").writeNullable[Int] and
    (__ \ "TermsAccepted").write[String] and
    (__ \ "USRId").write[Int] and (__ \ "USRType").write[String] and
    (__ \ "patientPhone").writeNullable[Array[patientPhoneDetailsObj]] and
    (__ \ "patientAddress").writeNullable[Array[patientAddressDetailsObj]] and
    (__ \ "patientEmail").writeNullable[Array[patientEmailDetailsObj]] and
    (__ \ "patientOtherID").writeNullable[Array[patientOtherIdDetailsObj]])(unlift(addPatientObj.unapply))
  implicit val addPatientFormat: Format[addPatientObj] = Format(addPatientObjReads, addPatientObjWrites)
}

case class updatePatientObj(FirstName: Option[String],
                            LastName: Option[String], FullName: Option[String], PatientID: Long, AadhaarID: Option[Int],
                            Gender: Option[String], DOB: Option[java.sql.Date], Age: Option[Int], BloodGroup: Option[String],
                            FamilyHdIND: Option[String], FamilyHdID: Option[Int], FamilyRltnType: Option[String],
                            PatientPassword: Option[String], PinPassCode: Option[Int],
                            TermsAccepted: String, USRId: Int, USRType: String,
                            patientPhone: Option[Array[patientPhoneDetailsObj]],
                            patientAddress: Option[Array[patientAddressDetailsObj]],
                            patientEmail: Option[Array[patientEmailDetailsObj]],
                            patientOtherID: Option[Array[patientOtherIdDetailsObj]])

object updatePatientObj {
  val updatePatientObjReads = (
    (__ \ "FirstName").readNullable(Reads.minLength[String](1)) and (__ \ "LastName").readNullable[String] and
    (__ \ "FullName").readNullable(Reads.minLength[String](1)) and
    (__ \ "PatientID").read[Long] and
    (__ \ "AadhaarID").readNullable[Int] and
    (__ \ "Gender").readNullable[String] and (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "Age").readNullable[Int] and (__ \ "BloodGroup").readNullable[String] and
    (__ \ "FamilyHdIND").readNullable[String] and (__ \ "FamilyHdID").readNullable[Int] and
    (__ \ "FamilyRltnType").readNullable(Reads.minLength[String](1)) and
    (__ \ "PatientPassword").readNullable[String] and (__ \ "PinPassCode").readNullable[Int] and
    (__ \ "TermsAccepted").read[String] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String] and
    (__ \ "patientPhone").readNullable[Array[patientPhoneDetailsObj]] and
    (__ \ "patientAddress").readNullable[Array[patientAddressDetailsObj]] and
    (__ \ "patientEmail").readNullable[Array[patientEmailDetailsObj]] and
    (__ \ "patientOtherID").readNullable[Array[patientOtherIdDetailsObj]])(updatePatientObj.apply _)

  val updatePatientObjWrites = (
    (__ \ "FirstName").writeNullable[String] and (__ \ "LastName").writeNullable[String] and (__ \ "FullName").writeNullable[String] and
    (__ \ "PatientID").write[Long] and (__ \ "AadhaarID").writeNullable[Int] and (__ \ "Gender").writeNullable[String] and
    (__ \ "DOB").writeNullable[java.sql.Date] and (__ \ "Age").writeNullable[Int] and (__ \ "BloodGroup").writeNullable[String] and
    (__ \ "FamilyHdIND").writeNullable[String] and (__ \ "FamilyHdID").writeNullable[Int] and
    (__ \ "FamilyRltnType").writeNullable[String] and
    (__ \ "PatientPassword").writeNullable[String] and (__ \ "PinPassCode").writeNullable[Int] and
    (__ \ "TermsAccepted").write[String] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String] and
    (__ \ "patientPhone").writeNullable[Array[patientPhoneDetailsObj]] and
    (__ \ "patientAddress").writeNullable[Array[patientAddressDetailsObj]] and
    (__ \ "patientEmail").writeNullable[Array[patientEmailDetailsObj]] and
    (__ \ "patientOtherID").writeNullable[Array[patientOtherIdDetailsObj]])(unlift(updatePatientObj.unapply))

  implicit val updatePatientFormat: Format[updatePatientObj] = Format(updatePatientObjReads, updatePatientObjWrites)
}
case class patientPhoneDetailsObj(PhCntryCD: Int, PhStateCD: Int, PhAreaCD: Int, PhNumber: Long, PhType: String, PrimaryIND: String, PhStdCD: Option[String])

object patientPhoneDetailsObj {
  val patientPhoneDetailsObjReads = ((__ \ "PhCntryCD").read(Reads.min[Int](1)) and (__ \ "PhStateCD").read(Reads.min[Int](1)) and
    (__ \ "PhAreaCD").read(Reads.min[Int](1)) and 
    (__ \ "PhNumber").read(Reads.min[Long](1)) and (__ \ "PhType").read[String] and
    (__ \ "PrimaryIND").read[String] and
    (__ \ "PhStdCD").readNullable[String])(patientPhoneDetailsObj.apply _)

  val patientPhoneDetailsObjWrites = ((__ \ "PhCntryCD").write[Int] and
    (__ \ "PhStateCD").write[Int] and (__ \ "PhAreaCD").write[Int] and
     (__ \ "PhNumber").write[Long] and
    (__ \ "PhType").write[String] and (__ \ "PrimaryIND").write[String] and
    (__ \ "PhStdCD").writeNullable[String])(unlift(patientPhoneDetailsObj.unapply))
  implicit val patientPhoneDetailsFormat: Format[patientPhoneDetailsObj] = Format(patientPhoneDetailsObjReads, patientPhoneDetailsObjWrites)
}


case class patientAddressDetailsObj(Address1: String, Address2: String, Street: String, Location: Option[String], CityTown: String, DistrictRegion: String,
                                    State: String, PinCode: Int, Country: String, AddressType: String, PrimaryIND: String, LandMark: String,
                                    Latitude: String, Longitude: String)

object patientAddressDetailsObj {
  val patientAddressDetailsObjReads = ((__ \ "Address1").read[String] and (__ \ "Address2").read[String] and (__ \ "Street").read[String] and
      (__ \ "Location").readNullable[String] and (__ \ "CityTown").read[String] and (__ \ "DistrictRegion").read[String] and (__ \ "State").read[String] and
    (__ \ "PinCode").read[Int] and (__ \ "Country").read[String] and (__ \ "AddressType").read[String] and (__ \ "LandMark").read[String] and
    (__ \ "PrimaryIND").read[String] and (__ \ "Latitude").read[String] and (__ \ "Longitude").read[String])(patientAddressDetailsObj.apply _)

  val patientAddressDetailsObjWrites = ((__ \ "Address1").write[String] and (__ \ "Address2").write[String] and
    (__ \ "Street").write[String] and (__ \ "Location").writeNullable[String] and (__ \ "CityTown").write[String] and (__ \ "DistrictRegion").write[String] and
    (__ \ "State").write[String] and (__ \ "PinCode").write[Int] and (__ \ "Country").write[String] and
    (__ \ "AddressType").write[String] and (__ \ "PrimaryIND").write[String] and (__ \ "LandMark").write[String] and (__ \ "Latitude").write[String] and
    (__ \ "Longitude").write[String])(unlift(patientAddressDetailsObj.unapply))
  implicit val patientAddressDetailsFormat: Format[patientAddressDetailsObj] = Format(patientAddressDetailsObjReads, patientAddressDetailsObjWrites)
}

case class patientEmailDetailsObj(EmailID: String, EmailIDType: String, PrimaryIND: String)

object patientEmailDetailsObj {
  val patientEmailDetailsObjReads = ((__ \ "EmailID").read[String] and (__ \ "EmailIDType").read[String] and (__ \ "PrimaryIND").read[String])(patientEmailDetailsObj.apply _)

  val patientEmailDetailsObjWrites = ((__ \ "EmailID").write[String] and (__ \ "EmailIDType").write[String] and
    (__ \ "PrimaryIND").write[String])(unlift(patientEmailDetailsObj.unapply))
  implicit val patientAddressDetailsFormat: Format[patientEmailDetailsObj] = Format(patientEmailDetailsObjReads, patientEmailDetailsObjWrites)
}

case class patientOtherIdDetailsObj(PatientOtherID: String, PatientOtherIDType: String)

object patientOtherIdDetailsObj {
  val patientOtherIdDetailsObjReads = ((__ \ "PatientOtherID").read[String] and (__ \ "PatientOtherIDType").read[String])(patientOtherIdDetailsObj.apply _)

  val patientOtherIdDetailsObjWrites = (
    (__ \ "PatientOtherID").write[String] and (__ \ "PatientOtherIDType").write[String])(unlift(patientOtherIdDetailsObj.unapply))
  implicit val patientOtherIdDetailsFormat: Format[patientOtherIdDetailsObj] = Format(patientOtherIdDetailsObjReads, patientOtherIdDetailsObjWrites)

}

case class patientLoginDetailsObj(PatientLoginID: String, PatientPassword: String)

object patientLoginDetailsObj {
  val patientLoginDetailsObjReads = (
    (__ \ "PatientLoginID").read[String] and
    (__ \ "PatientPassword").read[String])(patientLoginDetailsObj.apply _)

  val patientLoginDetailsObjWrites = (
    (__ \ "PatientLoginID").write[String] and
    (__ \ "PatientPassword").write[String])(unlift(patientLoginDetailsObj.unapply))
  implicit val patientLoginDetailsFormat: Format[patientLoginDetailsObj] = Format(patientLoginDetailsObjReads, patientLoginDetailsObjWrites)

}

case class validatePatientPinPassDetailsObj(PatientLoginID: String, PinPassCode: Int)

object validatePatientPinPassDetailsObj {
  val validatePatientPinPassDetailsObjReads = (
    (__ \ "PatientLoginID").read[String] and
    (__ \ "PinPassCode").read[Int])(validatePatientPinPassDetailsObj.apply _)

  val validatePatientPinPassDetailsObjWrites = (
    (__ \ "PatientLoginID").write[String] and
    (__ \ "PinPassCode").write[Int])(unlift(validatePatientPinPassDetailsObj.unapply))
  implicit val validatePatientPinPasDetailsFormat: Format[validatePatientPinPassDetailsObj] = Format(validatePatientPinPassDetailsObjReads, validatePatientPinPassDetailsObjWrites)

}



case class GroupResponseObj(GroupID: Long, GroupDesc: String, MessageType: String, ActiveIND: String)

object GroupResponseObj {
  val GroupResponseObjReads = (
    (__ \ "GroupID").read[Long] and
    (__ \ "GroupDesc").read[String] and
    (__ \ "MessageType").read[String] and
    (__ \ "ActiveIND").read[String])(GroupResponseObj.apply _)

  val GroupResponseObjWrites = (
    (__ \ "GroupID").write[Long] and
    (__ \ "GroupDesc").write[String] and
    (__ \ "MessageType").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(GroupResponseObj.unapply))
  implicit val GroupResponseObjFormat: Format[GroupResponseObj] = Format(GroupResponseObjReads, GroupResponseObjWrites)

}

case class updatePatientPinPassObj(PatientLoginID: String, PinPassCode: Int, TermsAccepted: String, USRId: Int, USRType: String)

object updatePatientPinPassObj {
  val updatePatientPinPassObjReads = (
    (__ \ "PatientLoginID").read[String] and
    (__ \ "PinPassCode").read[Int] and
    (__ \ "TermsAccepted").read[String] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updatePatientPinPassObj.apply _)

  val updatePatientPinPassObjWrites = (
    (__ \ "PatientLoginID").write[String] and
    (__ \ "PinPassCode").write[Int] and
    (__ \ "TermsAccepted").write[String] and
    (__ \ "USRId").write[Int] and (__ \ "USRType").write[String])(unlift(updatePatientPinPassObj.unapply))
  implicit val validatePatientPinPasDetailsFormat: Format[updatePatientPinPassObj] = Format(updatePatientPinPassObjReads, updatePatientPinPassObjWrites)

}


case class doctorPatientSearchDetailsExtnObj( 
        PatientID: Option[Long], GroupID: Option[List[Long]], ReferralID: Option[Long], SubReferralID: Option[Long],  Gender: Option[String], 
        SearchCountry: Option[String], SearchState: Option[String], SearchCityTown: Option[String], 
        SearchLocation: Option[List[String]],startDOB: Option[String], endDOB: Option[String], PatientType: Option[String], listdocpatients: Option[String])

object doctorPatientSearchDetailsExtnObj {
  val doctorPatientSearchDetailsExtnObjReads = (
    (__ \ "PatientID").readNullable[Long] and
    (__ \ "GroupID").readNullable[List[Long]] and
    (__ \ "ReferralID").readNullable[Long] and
    (__ \ "SubReferralID").readNullable[Long] and
    (__ \ "Gender").readNullable[String] and
    (__ \ "SearchCountry").readNullable[String] and
    (__ \ "SearchState").readNullable[String] and
    (__ \ "SearchCityTown").readNullable[String] and
    (__ \ "SearchLocation").readNullable[List[String]] and
    (__ \ "startDOB").readNullable[String] and
    (__ \ "endDOB").readNullable[String] and
    (__ \ "PatientType").readNullable[String] and
    (__ \ "listdocpatients").readNullable[String]
   )(doctorPatientSearchDetailsExtnObj .apply _)

  val doctorPatientSearchDetailsExtnObjWrites = (
    (__ \ "PatientID").writeNullable[Long] and
    (__ \ "GroupID").writeNullable[List[Long]] and
    (__ \ "ReferralID").writeNullable[Long] and
    (__ \ "SubReferralID").writeNullable[Long] and
    (__ \ "Gender").writeNullable[String] and
    (__ \ "SearchCountry").writeNullable[String] and
    (__ \ "SearchState").writeNullable[String] and
    (__ \ "SearchCityTown").writeNullable[String] and
    (__ \ "SearchLocation").writeNullable[List[String]] and
    (__ \ "startDOB").writeNullable[String] and 
    (__ \ "endDOB").writeNullable[String] and 
    (__ \ "PatientType").writeNullable[String] and 
    (__ \ "listdocpatients").writeNullable[String])(unlift(doctorPatientSearchDetailsExtnObj.unapply))
  implicit val doctorPatientSearchDetailsExtnObjFormat: Format[doctorPatientSearchDetailsExtnObj] = Format(doctorPatientSearchDetailsExtnObjReads, doctorPatientSearchDetailsExtnObjWrites)

}



case class doctorPatientSearchDetailsObj(PageNumber: Int, PageSize: Int, Sort: Option[String], 
    SearchDetails: Option[doctorPatientSearchDetailsExtnObj], PhoneNumber: Option[String], MessageType: Option[String], EmailID: Option[String], 
    FirstName: Option[String],DOB: Option[java.sql.Date],StartAge:Option[Int], EndAge:Option[Int],StartDate: Option[String],
    EndDate: Option[String],IncludeCommDetails: Option[String],DoctorID:Long, HospitalID: Option[Long], HospitalIDLst: Option[List[Long]], 
    HospitalCD:Option[String], SearchText: Option[String], LocalSearchFlag: Option[String],Count:Option[Int])

object doctorPatientSearchDetailsObj {
  val doctorPatientSearchDetailsObjReads = (
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "Sort").readNullable[String] and
    (__ \ "SearchDetails").readNullable[doctorPatientSearchDetailsExtnObj] and
    (__ \ "PhoneNumber").readNullable[String] and
    (__ \ "MessageType").readNullable[String] and
    (__ \ "EmailID").readNullable[String] and
    (__ \ "FirstName").readNullable[String] and
    (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "StartAge").readNullable[Int] and
    (__ \ "EndAge").readNullable[Int] and
    (__ \ "StartDate").readNullable[String] and
    (__ \ "EndDate").readNullable[String] and
    (__ \ "IncludeCommDetails").readNullable[String] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalIDLst").readNullable[List[Long]] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "SearchText").readNullable[String] and
    (__ \ "LocalSearchFlag").readNullable[String] and
    (__ \ "Count").readNullable[Int] )(doctorPatientSearchDetailsObj .apply _)

  val doctorPatientSearchDetailsObjWrites = (
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "Sort").writeNullable[String] and
    (__ \ "SearchDetails").writeNullable[doctorPatientSearchDetailsExtnObj] and
    (__ \ "PhoneNumber").writeNullable[String] and
    (__ \ "MessageType").writeNullable[String] and
    (__ \ "EmailID").writeNullable[String] and
    (__ \ "FirstName").writeNullable[String] and
    (__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "StartAge").writeNullable[Int] and
    (__ \ "EndAge").writeNullable[Int] and
    (__ \ "StartDate").writeNullable[String] and
    (__ \ "EndDate").writeNullable[String] and
    (__ \ "IncludeCommDetails").writeNullable[String] and
    (__ \ "DoctorID").write[Long] and
     (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalIDLst").writeNullable[List[Long]] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "SearchText").writeNullable[String] and
    (__ \ "LocalSearchFlag").writeNullable[String] and
    (__ \ "Count").writeNullable[Int] )(unlift(doctorPatientSearchDetailsObj.unapply))
  implicit val patientOtherIdDetailsFormat: Format[doctorPatientSearchDetailsObj] = Format(doctorPatientSearchDetailsObjReads, doctorPatientSearchDetailsObjWrites)

}

case class patientCaseBillingAmtInfo(ProcedureCost: BigDecimal, BilledCost: BigDecimal, DiscountCost: BigDecimal, totalBalanceCost: BigDecimal)

object patientCaseBillingAmtInfo {
  val patientCaseBillingAmtReads = (
    (__ \ "ProcedureCost").read[BigDecimal] and
    (__ \ "BilledCost").read[BigDecimal] and
    (__ \ "DiscountCost").read[BigDecimal] and
    (__ \ "totalBalanceCost").read[BigDecimal])(patientCaseBillingAmtInfo.apply _)

  val patientCaseBillingAmtWrites = (
    (__ \ "ProcedureCost").write[BigDecimal] and
    (__ \ "BilledCost").write[BigDecimal] and
    (__ \ "DiscountCost").write[BigDecimal] and
    (__ \ "totalBalanceCost").write[BigDecimal])(unlift(patientCaseBillingAmtInfo.unapply))
  implicit val patientCaseBillingAmtFormat: Format[patientCaseBillingAmtInfo] = Format(patientCaseBillingAmtReads, patientCaseBillingAmtWrites)

}

