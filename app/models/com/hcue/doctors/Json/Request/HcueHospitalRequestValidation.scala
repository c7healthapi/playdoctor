/* Contains Json constructor and implicit Read write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request

import java.sql.Date
import java.sql.Timestamp

import scala.slick.driver.PostgresDriver.simple._

import play.api.libs.functional.syntax._
import play.api.libs.functional.syntax._
import play.api.libs.json._
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.TimeZone

case class HospitalReportsStatusObj(status:Boolean,record:Option[HcueHospitalReportsJsonObj])

object HospitalReportsStatusObj {

  val HospitalReportsStatusObjReads = (
    (__ \ "status").read[Boolean] and
    (__ \ "record").readNullable[HcueHospitalReportsJsonObj])(HospitalReportsStatusObj.apply _)
  
  val HospitalReportsStatusObjWrites = (
    (__ \ "status").write[Boolean] and
    (__ \ "record").writeNullable[HcueHospitalReportsJsonObj])(unlift(HospitalReportsStatusObj.unapply))
  implicit val HospitalReportsStatusFormat: Format[HospitalReportsStatusObj] = Format(HospitalReportsStatusObjReads, HospitalReportsStatusObjWrites)

}
//HospitalID: Option[Long], HospitalCode :  Option[String], StartDate :  java.sql.Date ,EndDate
case class getHcueHospitalReportsJsonObj(StartDate: java.sql.Date, EndDate: java.sql.Date, HospitalID: Option[Long], HospitalCode: Option[String],
                                         PageNumber: Int, PageSize: Int)

object getHcueHospitalReportsJsonObj {

  val getHcueHospitalReportsJsonObjReads = (
    (__ \ "StartDate").read[java.sql.Date] and
    (__ \ "EndDate").read[java.sql.Date] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCode").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int])(getHcueHospitalReportsJsonObj.apply _)

  val getHcueHospitalReportsJsonObjwrites = (
    (__ \ "StartDate").write[java.sql.Date] and
    (__ \ "EndDate").write[java.sql.Date] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCode").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int])(unlift(getHcueHospitalReportsJsonObj.unapply))

  implicit val HcueHospitalReportsFormat: Format[getHcueHospitalReportsJsonObj] = Format(getHcueHospitalReportsJsonObjReads, getHcueHospitalReportsJsonObjwrites)

}

case class HcueHospitalReportsJsonObj(ReportID: Option[Long], ReportType: Option[String], ReportDescription: Option[String], StartDate: java.sql.Date,
                                      EndDate: java.sql.Date, RequestDateTime: Option[java.sql.Date], CompletedDateTime: Option[java.sql.Date], Status: Option[String],
                                      HospitalID: Option[Long], HospitalCode: Option[String], ParentHospitalID: Option[Long], USRId: Long,
                                      USRIdType: String)

object HcueHospitalReportsJsonObj {

  val HcueHospitalReportsJsonObjReads = (
    (__ \ "ReportID").readNullable[Long] and
    (__ \ "ReportType").readNullable[String] and
    (__ \ "ReportDescription").readNullable[String] and
    (__ \ "StartDate").read[java.sql.Date] and
    (__ \ "EndDate").read[java.sql.Date] and
    (__ \ "RequestDateTime").readNullable[java.sql.Date] and
    (__ \ "CompletedDateTime").readNullable[java.sql.Date] and
    (__ \ "Status").readNullable[String] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCode").readNullable[String] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRIdType").read[String])(HcueHospitalReportsJsonObj.apply _)

  val HcueHospitalReportsJsonObjwrites = (
    (__ \ "ReportID").writeNullable[Long] and
    (__ \ "ReportType").writeNullable[String] and
    (__ \ "ReportDescription").writeNullable[String] and
    (__ \ "StartDate").write[java.sql.Date] and
    (__ \ "EndDate").write[java.sql.Date] and
    (__ \ "RequestDateTime").writeNullable[java.sql.Date] and
    (__ \ "CompletedDateTime").writeNullable[java.sql.Date] and
    (__ \ "Status").writeNullable[String] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCode").writeNullable[String] and
    (__ \ "ParentHospitalID").writeNullable[Long] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRIdType").write[String])(unlift(HcueHospitalReportsJsonObj.unapply))

  implicit val HcueHospitalReportsFormat: Format[HcueHospitalReportsJsonObj] = Format(HcueHospitalReportsJsonObjReads, HcueHospitalReportsJsonObjwrites)

}

case class ListHospitalReportsJsonObj(ArrayReportObj : Array[ListHospitalReportsResponse], Count : Int)


object ListHospitalReportsJsonObj {

  val ListHospitalReportsJsonObjReads = (
    (__ \ "ArrayReportObj").read[Array[ListHospitalReportsResponse]] and
    (__ \ "Count").read[Int])(ListHospitalReportsJsonObj.apply _)

  val ListHospitalReportsJsonObjwrites = (
    (__ \ "ArrayReportObj").write[Array[ListHospitalReportsResponse]] and
    (__ \ "Count").write[Int])(unlift(ListHospitalReportsJsonObj.unapply))

  implicit val ListHospitalReportsJsonObjFormat: Format[ListHospitalReportsJsonObj] = Format(ListHospitalReportsJsonObjReads, ListHospitalReportsJsonObjwrites)
}



case class ListHospitalReportsResponse(ReportID: Long, ReportType: Option[String], ReportDescription: Option[String], StartDate: java.sql.Date, EndDate: java.sql.Date,
                                          RequestDateTime: java.sql.Date, CompletedDateTime: Option[java.sql.Date], Status: Option[String],Location:Option[String])

object ListHospitalReportsResponse {

  val ListHospitalReportsResponseReads = (
    (__ \ "ReportID").read[Long] and
    (__ \ "ReportType").readNullable[String] and
    (__ \ "ReportDescription").readNullable[String] and
    (__ \ "StartDate").read[java.sql.Date] and
    (__ \ "EndDate").read[java.sql.Date] and
    (__ \ "RequestDateTime").read[java.sql.Date] and
    (__ \ "CompletedDateTime").readNullable[java.sql.Date] and
    (__ \ "Status").readNullable[String] and 
    (__ \ "Location").readNullable[String])(ListHospitalReportsResponse.apply _)

  val ListHospitalReportsResponsewrites = (
    (__ \ "ReportID").write[Long] and
    (__ \ "ReportType").writeNullable[String] and
    (__ \ "ReportDescription").writeNullable[String] and
    (__ \ "StartDate").write[java.sql.Date] and
    (__ \ "EndDate").write[java.sql.Date] and
    (__ \ "RequestDateTime").write[java.sql.Date] and
    (__ \ "CompletedDateTime").writeNullable[java.sql.Date] and
    (__ \ "Status").writeNullable[String] and 
    (__ \ "Location").writeNullable[String])(unlift(ListHospitalReportsResponse.unapply))

  implicit val ListHospitalReportsResponseFormat: Format[ListHospitalReportsResponse] = Format(ListHospitalReportsResponseReads, ListHospitalReportsResponsewrites)

}