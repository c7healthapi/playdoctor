package models.com.hcue.doctors.Json.Request
/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime
import models.com.hcue.doctors.Json.Request.AddUpdate.AccountDetails
import models.com.hcue.doctors.Json.Response.currentAgeObj
 
case class doctorSearchDetailsObj(PageNumber: Int, PageSize: Int, Sort: Option[String], FirstName:Option[String], FullName: Option[String], 
                                  SpecialityID: Option[String], Location: Option[String],ActiveDoctors:Option[String],
                                  Sex:Option[String],DayCode:Option[String],CityTown: Option[String],MyCareID: Option[Long], DoctorID: Option[List[Long]],AddressID: Option[List[Long]],  
                                  HospitalID: Option[Long], HospitalCD:Option[String], ClinicName: Option[String],
                                  FullTextSearch:Option[String],DoctorLoginID:Option[String],filterByDate: Option[java.sql.Date],filterByEndDate:Option[java.sql.Date])
//List of Doctor 
object doctorSearchDetailsObj {
  val doctorSearchDetailsObjReads = (
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "Sort").readNullable[String] and
     (__ \ "FirstName").readNullable[String] and
    (__ \ "FullName").readNullable[String] and
    (__ \ "SpecialityID").readNullable[String] and
    (__ \ "Location").readNullable[String] and
    (__ \ "ActiveDoctors").readNullable[String] and
    (__ \ "Sex").readNullable[String] and
    (__ \ "DayCode").readNullable[String] and
    (__ \ "CityTown").readNullable[String] and
    (__ \ "MyCareID").readNullable[Long] and
    (__ \ "DoctorID").readNullable[List[Long]] and
    (__ \ "AddressID").readNullable[List[Long]] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "ClinicName").readNullable[String] and
    (__ \ "FullTextSearch").readNullable[String] and
    (__ \ "DoctorLoginID").readNullable[String] and
    (__ \ "filterByDate").readNullable[java.sql.Date] and
    (__ \ "filterByEndDate").readNullable[java.sql.Date])(doctorSearchDetailsObj.apply _)

  val doctorSearchDetailsObjWrites = (
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "Sort").writeNullable[String] and
     (__ \ "FirstName").writeNullable[String] and
    (__ \ "FullName").writeNullable[String] and
    (__ \ "SpecialityID").writeNullable[String] and
    (__ \ "Location").writeNullable[String] and
    (__ \ "ActiveDoctors").writeNullable[String] and
    (__ \ "Sex").writeNullable[String] and
    (__ \ "DayCode").writeNullable[String] and
    (__ \ "CityTown").writeNullable[String] and
    (__ \ "MyCareID").writeNullable[Long] and
    (__ \ "DoctorID").writeNullable[List[Long]] and
    (__ \ "AddressID").writeNullable[List[Long]] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "ClinicName").writeNullable[String] and
    (__ \ "FullTextSearch").writeNullable[String] and
    (__ \ "DoctorLoginID").writeNullable[String] and
    (__ \ "filterByDate").writeNullable[java.sql.Date] and
    (__ \ "filterByEndDate").writeNullable[java.sql.Date])(unlift(doctorSearchDetailsObj.unapply))
  implicit val commonSearchDetailsFormat: Format[doctorSearchDetailsObj] = Format(doctorSearchDetailsObjReads, doctorSearchDetailsObjWrites)

}

case class doctorLocationInfo(IP:String,MacAddress:String,UUID:String,AccessKey:String,ClientType:Option[String])

object doctorLocationInfo {
  val doctorLocationInfoObjReads = (
    (__ \ "IP").read[String] and
    (__ \ "MacAddress").read[String] and
    (__ \ "UUID").read[String] and 
    (__ \ "AccessKey").read[String] and
    (__ \ "ClientType").readNullable[String])(doctorLocationInfo.apply _)

  val doctorLocationInfoObjWrites = (
    (__ \ "IP").write[String] and
    (__ \ "MacAddress").write[String] and
    (__ \ "UUID").write[String] and 
    (__ \ "AccessKey").write[String] and
    (__ \ "ClientType").writeNullable[String])(unlift(doctorLocationInfo.unapply))
  implicit val doctorLocationInfoFormat: Format[doctorLocationInfo] = Format(doctorLocationInfoObjReads, doctorLocationInfoObjWrites)

}

case class doctorLoginDetailsObj(DoctorLoginID: String, DoctorPassword: Option[String],googleLogin : Option[String],LocationInfo:Option[doctorLocationInfo],action:Option[String], LoggedIn:Option[String],AccountDetails : Option[AccountDetails])

object doctorLoginDetailsObj {
  val DoctorLoginDetailsObjReads = (
    (__ \ "DoctorLoginID").read(Reads.maxLength[String](100)) and
    (__ \ "DoctorPassword").readNullable(Reads.maxLength[String](20)) and
    (__ \ "googleLogin").readNullable[String] and
    (__ \ "LocationInfo").readNullable[doctorLocationInfo] and 
    (__ \ "action").readNullable[String] and
    (__ \ "LoggedIn").readNullable[String] and
    (__ \ "AccountDetails").readNullable[AccountDetails])(doctorLoginDetailsObj.apply _)

  val DoctortLoginDetailsObjWrites = (
    (__ \ "DoctorPassword").write[String] and
    (__ \ "PinPassCode").writeNullable[String] and
    (__ \ "googleLogin").writeNullable[String] and
    (__ \ "LocationInfo").writeNullable[doctorLocationInfo] and
    (__ \ "action").writeNullable[String] and
    (__ \ "LoggedIn").writeNullable[String] and
    (__ \ "AccountDetails").writeNullable[AccountDetails])(unlift(doctorLoginDetailsObj.unapply))
  implicit val DoctorLoginDetailsFormat: Format[doctorLoginDetailsObj] = Format(DoctorLoginDetailsObjReads, DoctortLoginDetailsObjWrites)

}


case class updatepasswordObj(doctorid: Option[Long],doctorloginid:  Option[String],password: Option[String],
  newpassword:  Option[String],lastlogin: Option[java.sql.Date],active: Boolean)

object updatepasswordObj {

  val updatepasswordObjreads = (
    (__ \ "doctorid").readNullable[Long] and
    (__ \ "doctorloginid").readNullable[String] and
    (__ \ "password").readNullable[String] and
    (__ \ "newpassword").readNullable[String] and
    (__ \ "lastlogin").readNullable[java.sql.Date] and
    (__ \ "active").read[Boolean])(updatepasswordObj.apply _)

  val updatepasswordObjWrites = (
    (__ \ "doctorid").writeNullable[Long] and
    (__ \ "doctorloginid").writeNullable[String] and
    (__ \ "password").writeNullable[String] and
    (__ \ "newpassword").writeNullable[String] and
    (__ \ "lastlogin").writeNullable[java.sql.Date] and
    (__ \ "active").write[Boolean])(unlift(updatepasswordObj.unapply _))

  implicit val updatepasswordObjFormat: Format[updatepasswordObj] = Format(updatepasswordObjreads, updatepasswordObjWrites)

}
case class updateDoctorPasswordObj(DoctorLoginID: String, DoctorCurrentPassword: String, DoctorNewPassword: String,PinPassCode: Option[String], USRId: Int, USRType: String)

object updateDoctorPasswordObj {
  val updateDoctorPasswordObjReads = (
    (__ \ "DoctorLoginID").read[String] and
    (__ \ "DoctorCurrentPassword").read[String] and
    (__ \ "DoctorNewPassword").read[String] and
    (__ \ "PinPassCode").readNullable[String] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updateDoctorPasswordObj.apply _)

  val updateDoctorPasswordObjWrites = (
    (__ \ "DoctorLoginID").write[String] and
    (__ \ "DoctorCurrentPassword").write[String] and
    (__ \ "DoctorNewPassword").write[String] and
    (__ \ "PinPassCode").writeNullable[String] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(updateDoctorPasswordObj.unapply))
  implicit val updateDoctorPasswordFormat: Format[updateDoctorPasswordObj] = Format(updateDoctorPasswordObjReads, updateDoctorPasswordObjWrites)

}


case class forgotPasswordRequest(DoctorLoginID:String,MobileNumber: Option[Long],EmailID:Option[String],SendMail:Option[String],PreferedEmailID:Option[String])

object forgotPasswordRequest {
  val forgotPasswordRequestReads = (
    (__ \ "DoctorLoginID").read[String] and
    (__ \ "MobileNumber").readNullable[Long] and
    (__ \ "EmailID").readNullable[String] and 
    (__ \ "SendMail").readNullable[String] and 
    (__ \ "PreferedEmailID").readNullable[String])(forgotPasswordRequest.apply _)

  val forgotPasswordRequestWrites = (
       (__ \ "DoctorLoginID").write[String] and
    (__ \ "MobileNumber").writeNullable[Long] and
    (__ \ "EmailID").writeNullable[String] and 
    (__ \ "SendMail").writeNullable[String] and 
    (__ \ "PreferedEmailID").writeNullable[String])(unlift(forgotPasswordRequest.unapply))
    
  implicit val forgotPasswordResponseFormat: Format[forgotPasswordRequest] = Format(forgotPasswordRequestReads, forgotPasswordRequestWrites)
} 

case class hospitalDetailsObj(HospitalID:Option[Long],HospitalCD:Option[String],BranchCD:Option[String],ParentHospitalID:Option[Long])

object hospitalDetailsObj
{
  val hospitalDetailsObjReads = (
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "BranchCD").readNullable[String] and
    (__ \ "ParentHospitalID").readNullable[Long])(hospitalDetailsObj.apply _)

 val hospitalDetailsObjWrites = ( (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "BranchCD").writeNullable[String] and
    (__ \ "ParentHospitalID").writeNullable[Long])(unlift(hospitalDetailsObj.unapply))
  implicit val hospitalDetailsormat: Format[hospitalDetailsObj] = Format(hospitalDetailsObjReads, hospitalDetailsObjWrites)

}

case class smsSettingObj(doctorPhone:Long,smsLanguage:String, sendbirthdaySms: Option[String], walkInConsultation: smsInputSettingObj, appointmentBook:smsInputSettingObj, appointmentCancel:smsInputSettingObj, appointmentReschedule:smsInputSettingObj,
    appointmentReminder:smsInputSettingObj, followUpReminder:smsInputSettingObj)

object smsSettingObj
{
  val smsSettingObjReads = (
    (__ \ "doctorPhone").read[Long] and
    (__ \ "smsLanguage").read[String] and
    (__ \ "sendbirthdaySms").readNullable[String] and
    (__ \ "walkInConsultation").read[smsInputSettingObj] and
    (__ \ "appointmentBook").read[smsInputSettingObj] and
    (__ \ "appointmentCancel").read[smsInputSettingObj] and
    (__ \ "appointmentReschedule").read[smsInputSettingObj] and
    (__ \ "appointmentReminder").read[smsInputSettingObj] and
    (__ \ "followUpReminder").read[smsInputSettingObj])(smsSettingObj.apply _)

 val smsSettingObjWrites = ( 
    (__ \ "doctorPhone").write[Long] and
    (__ \ "smsLanguage").write[String] and
    (__ \ "sendbirthdaySms").writeNullable[String] and
    (__ \ "walkInConsultation").write[smsInputSettingObj] and
    (__ \ "appointmentBook").write[smsInputSettingObj] and
    (__ \ "appointmentCancel").write[smsInputSettingObj] and
    (__ \ "appointmentReschedule").write[smsInputSettingObj] and
    (__ \ "appointmentReminder").write[smsInputSettingObj] and
    (__ \ "followUpReminder").write[smsInputSettingObj])(unlift(smsSettingObj.unapply))
  implicit val smsSettingObjFormat: Format[smsSettingObj] = Format(smsSettingObjReads, smsSettingObjWrites)

}

case class smsInputSettingObj(activeIND:String,patientName: String, doctorName: String, clinicName: String, smsStructure:String, setReminderTime: Option[String])

object smsInputSettingObj
{
  val smsInputSettingObjReads = (
    (__ \ "activeIND").read[String] and
    (__ \ "patientName").read[String] and
    (__ \ "doctorName").read[String] and
    (__ \ "clinicName").read[String] and
    (__ \ "smsStructure").read[String] and
    (__ \ "setReminderTime").readNullable[String])(smsInputSettingObj.apply _)

 val smsInputSettingObjWrites = ( 
    (__ \ "activeIND").write[String] and
    (__ \ "patientName").write[String] and
    (__ \ "doctorName").write[String] and
    (__ \ "clinicName").write[String] and
    (__ \ "smsStructure").write[String] and
    (__ \ "setReminderTime").writeNullable[String])(unlift(smsInputSettingObj.unapply))
  implicit val smsInputSettingObjFormat: Format[smsInputSettingObj] = Format(smsInputSettingObjReads, smsInputSettingObjWrites)

}


case class sourceDetailsObj(ReferralID:Long,SubReferralID:Option[Long])

object sourceDetailsObj
{
  val sourceDetailsObjReads = (
    (__ \ "ReferralID").read[Long] and
    (__ \ "SubReferralID").readNullable[Long])(sourceDetailsObj.apply _)

 val sourceDetailsObjWrites = ( 
    (__ \ "ReferralID").write[Long] and
    (__ \ "SubReferralID").writeNullable[Long])(unlift(sourceDetailsObj.unapply))
  implicit val sourceDetailsObjFormat: Format[sourceDetailsObj] = Format(sourceDetailsObjReads, sourceDetailsObjWrites)

}

case class addDtAppointmentDetailsObj(AddressConsultID: Long, DayCD: String, ConsultationDt: java.sql.Date, StartTime: String, EndTime: String,
                                      PatientID: Long,DoctorID: Long, VisitUserTypeID: String, DoctorVisitRsnID: String, OtherVisitRsn: Option[String], 
                                      ActualFees: Option[Long],AppointmentStatus: String, SendSMS:Option[String],HospitalDetails:Option[hospitalDetailsObj],
                                      VisitRsnID: Option[Long], PreliminaryNotes: Option[String], ReferralSourceDtls: Option[sourceDetailsObj], HospitalCD:Option[String],BranchCD:Option[String],USRId: Long, USRType: String)

object addDtAppointmentDetailsObj {

  val sqlDateWrite = Writes.sqlDateWrites("yyyy-MM-dd HH:mm:ss")

  val addDtAppointmentDetailsObjReads = (
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ConsultationDt").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "PatientID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "OtherVisitRsn").readNullable[String] and
    (__ \ "ActualFees").readNullable[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "SendSMS").readNullable[String] and
    (__ \ "HospitalDetails").readNullable[hospitalDetailsObj] and
    (__ \ "VisitRsnID").readNullable[Long] and
    (__ \ "PreliminaryNotes").readNullable[String] and
    (__ \ "ReferralSourceDtls").readNullable[sourceDetailsObj] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "BranchCD").readNullable[String] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addDtAppointmentDetailsObj.apply _)

  val addDtAppointmentDetailsObjWrites = (
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ConsultationDt").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "PatientID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "DoctorVisitRsnID").write[String] and
    (__ \ "OtherVisitRsn").writeNullable[String] and
    (__ \ "ActualFees").writeNullable[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "SendSMS").writeNullable[String] and
    (__ \ "HospitalDetails").writeNullable[hospitalDetailsObj] and
    (__ \ "VisitRsnID").writeNullable[Long] and
    (__ \ "PreliminaryNotes").writeNullable[String] and
    (__ \ "ReferralSourceDtls").writeNullable[sourceDetailsObj] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "BranchCD").writeNullable[String] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addDtAppointmentDetailsObj.unapply))
  implicit val addDtAppointmentDetailsFormat: Format[addDtAppointmentDetailsObj] = Format(addDtAppointmentDetailsObjReads, addDtAppointmentDetailsObjWrites)

}


case class addHospAppointmentDetailsObj(AddressConsultID: Long, DayCD: String, ConsultationDt: java.sql.Date, StartTime: String, EndTime: String,
                                      PatientID: Option[Long], VisitUserTypeID: String, DoctorVisitRsnID: String, 
                                      AppointmentStatus: String, SendSMS:Option[String],
  HospitalCD:Option[String],
  HospitalID:Option[Long],
  BranchCode:Option[Long],
  KioskID:Option[Long],
  SpecialityCD:Option[String],
  USRId: Int, 
  USRType: String)

object addHospAppointmentDetailsObj { 

  val sqlDateWrite = Writes.sqlDateWrites("yyyy-MM-dd HH:mm:ss")

  val addHospAppointmentDetailsObjReads = (
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ConsultationDt").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "PatientID").readNullable[Long] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "SendSMS").readNullable[String] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "BranchCode").readNullable[Long] and
    (__ \ "KioskID").readNullable[Long] and
    (__ \ "SpecialityCD").readNullable[String] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(addHospAppointmentDetailsObj.apply _)

  val addHospAppointmentDetailsObjWrites = (
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ConsultationDt").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "PatientID").writeNullable[Long] and
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "DoctorVisitRsnID").write[String] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "SendSMS").writeNullable[String] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "BranchCode").writeNullable[Long] and
    (__ \ "KioskID").writeNullable[Long] and
    (__ \ "SpecialityCD").writeNullable[String] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(addHospAppointmentDetailsObj.unapply))
  implicit val addHospAppointmentDetailsObjFormat: Format[addHospAppointmentDetailsObj] = Format(addHospAppointmentDetailsObjReads, addHospAppointmentDetailsObjWrites)

}

case class sendAppointmentSMSReq(AppointmentID: Long, USRId: Long, USRType: String)

object sendAppointmentSMSReq {
  val sendAppointmentSMSReqReads = (
    (__ \ "AppointmentID").read[Long] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(sendAppointmentSMSReq.apply _)

  val sendAppointmentSMSReqWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(sendAppointmentSMSReq.unapply))
  implicit val sendAppointmentSMSReqFormat: Format[sendAppointmentSMSReq] = Format(sendAppointmentSMSReqReads, sendAppointmentSMSReqWrites)
}

case class updateDtAppointmentDetailsObj(AppointmentID: Long, AddressConsultID: Long, DayCD: String, ConsultationDt: java.sql.Date, StartTime: String,
                                         EndTime: String, PatientID: Long, DoctorID: Long, VisitUserTypeID: String, DoctorVisitRsnID: String,
                                         OtherVisitRsn: Option[String],ActualFees: Option[Long], AppointmentStatus: String,Reason : Option[String],HospitalDetails:Option[hospitalDetailsObj],USRId: Long, USRType: String,schedulerType: Option[String])
                                       

object updateDtAppointmentDetailsObj {
  val updateDtAppointmentDetailsObjReads = (
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ConsultationDt").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "PatientID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "OtherVisitRsn").readNullable[String] and
    (__ \ "ActualFees").readNullable[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "Reason").readNullable[String] and
    (__ \ "HospitalDetails").readNullable[hospitalDetailsObj] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String] and
    (__ \ "schedulerType").readNullable[String])(updateDtAppointmentDetailsObj.apply _)

  val updateDtAppointmentDetailsObjWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ConsultationDt").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "PatientID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "DoctorVisitRsnID").write[String] and
    (__ \ "OtherVisitRsn").writeNullable[String] and
    (__ \ "ActualFees").writeNullable[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "Reason").writeNullable[String] and
    (__ \ "HospitalDetails").writeNullable[hospitalDetailsObj] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String] and 
    (__ \ "schedulerType").writeNullable[String])(unlift(updateDtAppointmentDetailsObj.unapply))
  implicit val updateDtAppointmentDetailsFormat: Format[updateDtAppointmentDetailsObj] = Format(updateDtAppointmentDetailsObjReads, updateDtAppointmentDetailsObjWrites)

}

case class updateDtAppointmentStatusObj(AppointmentID: Long, AppointmentStatus: String, Reason: Option[String], VisitRsnID: Option[Long], 
                        OtherVisitRsn: Option[String], PreliminaryNotes: Option[String], ReferralSourceDtls: Option[sourceDetailsObj], USRId: Long, USRType: String)
                                       

object updateDtAppointmentStatusObj {
  val updateDtAppointmentStatusObjReads = (
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "Reason").readNullable[String] and
    (__ \ "VisitRsnID").readNullable[Long] and
    (__ \ "OtherVisitRsn").readNullable[String] and
    (__ \ "PreliminaryNotes").readNullable[String] and
    (__ \ "ReferralSourceDtls").readNullable[sourceDetailsObj] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(updateDtAppointmentStatusObj.apply _)

  val updateDtAppointmentStatusObjWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "Reason").writeNullable[String] and
    (__ \ "VisitRsnID").writeNullable[Long] and
    (__ \ "OtherVisitRsn").writeNullable[String] and
    (__ \ "PreliminaryNotes").writeNullable[String] and
    (__ \ "ReferralSourceDtls").writeNullable[sourceDetailsObj] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(updateDtAppointmentStatusObj.unapply))
  implicit val updateDtAppointmentDetailsFormat: Format[updateDtAppointmentStatusObj] = Format(updateDtAppointmentStatusObjReads, updateDtAppointmentStatusObjWrites)

}


case class getDtAppointmentDetailsObj(HospitalCD:Option[String],AddressConsultID: Option[List[Long]],AddressID:Option[Long], HospitalID:Option[Long],ConsultationDt: java.sql.Date, Status:Option[String],ShowDueAmount: Option[String], IncludePatientDetails:Option[String],Version: Option[String])

object getDtAppointmentDetailsObj {

  val getDtAppointmentDetailsObjReads = ( 
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "AddressConsultID").readNullable[List[Long]] and
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "ConsultationDt").read[java.sql.Date] and
    (__ \ "Status").readNullable[String] and 
    (__ \ "ShowDueAmount").readNullable[String] and 
    (__ \ "IncludePatientDetails").readNullable[String] and
    (__ \ "Version").readNullable[String])(getDtAppointmentDetailsObj.apply _)

  val getDtAppointmentDetailsObjWrites = (
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "AddressConsultID").writeNullable[List[Long]] and
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "ConsultationDt").write[java.sql.Date] and
    (__ \ "Status").writeNullable[String] and
    (__ \ "ShowDueAmount").writeNullable[String] and
    (__ \ "IncludePatientDetails").writeNullable[String] and
    (__ \ "Version").writeNullable[String])(unlift(getDtAppointmentDetailsObj.unapply))
  implicit val getDtAppointmentDetailsFormat: Format[getDtAppointmentDetailsObj] = Format(getDtAppointmentDetailsObjReads, getDtAppointmentDetailsObjWrites)

}

case class getTopAppointmentDetailsObj(AddressConsultID: Long, ConsultationDt: java.sql.Date, Status:Option[String],EndTime:String)

object getTopAppointmentDetailsObj {

  val getTopAppointmentDetailsObjReads = ( 
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "ConsultationDt").read[java.sql.Date] and
    (__ \ "Status").readNullable[String] and
    (__ \ "EndTime").read[String])(getTopAppointmentDetailsObj.apply _)

  val getTopAppointmentDetailsObjWrites = (
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "ConsultationDt").write[java.sql.Date] and
    (__ \ "Status").writeNullable[String] and
    (__ \ "EndTime").write[String])(unlift(getTopAppointmentDetailsObj.unapply))
  implicit val getDtAppointmentDetailsFormat: Format[getTopAppointmentDetailsObj] = Format(getTopAppointmentDetailsObjReads, getTopAppointmentDetailsObjWrites)

}

case class validateDoctorPinPassObj(DoctorLoginID: String, PinPassCode: String)

object validateDoctorPinPassObj {
  val validateDoctorPinPassObjReads = (
    (__ \ "DoctorLoginID").read[String] and
    (__ \ "PinPassCode").read[String])(validateDoctorPinPassObj.apply _)

  val validateDoctorPinPassObjWrites = (
    (__ \ "DoctorLoginID").write[String] and
    (__ \ "PinPassCode").write[String])(unlift(validateDoctorPinPassObj.unapply))
  implicit val validateDoctorPinPasFormat: Format[validateDoctorPinPassObj] = Format(validateDoctorPinPassObjReads, validateDoctorPinPassObjWrites)

}

case class PatientAppDoctorSearchDetailsObj(Latitude:String, Longitude: String, LocalityName: Option[String], SpecialityID: Option[String], 
    DoctorName: Option[String], Gender:Option[String],ExpStartRange:Option[Int],ExpEndRange:Option[Int],
    CityName: String,FeeStartRange: Option[Int],FeeEndRange:Option[Int],AvailabilityInfo: Option[AvailabilityInfoObj],
    PageNumber: Int, PageSize: Int, Radius: Option[Int], Count: Int, OrderBy: Option[String],Sort:String,DoctorIDs : Option[String])

object PatientAppDoctorSearchDetailsObj {
  val PatientAppDoctorSearchDetailsObjReads = (
    (__ \ "Latitude").read[String] and
    (__ \ "Longitude").read[String] and
    (__ \ "LocalityName").readNullable[String] and
    (__ \ "SpecialityID").readNullable[String] and
    (__ \ "DoctorName").readNullable[String] and
    (__ \ "Gender").readNullable[String] and
    (__ \ "ExpStartRange").readNullable[Int] and
    (__ \ "ExpEndRange").readNullable[Int] and
   // (__ \ "CurrentTime").readNullable[String] and
    (__ \ "CityName").read[String] and
    (__ \ "FeeStartRange").readNullable[Int] and
    (__ \ "FeeEndRange").readNullable[Int] and
    (__ \ "AvailabilityInfo").readNullable[AvailabilityInfoObj] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "Radius").readNullable[Int] and
    (__ \ "Count").read[Int] and
    (__ \ "OrderBy").readNullable[String] and 
    (__ \ "Sort").read[String] and
    (__ \ "DoctorIDs").readNullable[String])(PatientAppDoctorSearchDetailsObj.apply _)

  val PatientAppDoctorSearchDetailsObjWrites = (
    (__ \ "Latitude").write[String] and
    (__ \ "Longitude").write[String] and
    (__ \ "LocalityName").writeNullable[String] and
    (__ \ "SpecialityID").writeNullable[String] and
    (__ \ "DoctorName").writeNullable[String] and
    (__ \ "Gender").writeNullable[String] and
    (__ \ "ExpStartRange").writeNullable[Int] and
    (__ \ "ExpEndRange").writeNullable[Int] and
   // (__ \ "CurrentTime").writeNullable[String] and
    (__ \ "CityName").write[String] and
    (__ \ "FeeStartRange").writeNullable[Int] and
    (__ \ "FeeEndRange").writeNullable[Int] and
    (__ \ "AvailabilityInfo").writeNullable[AvailabilityInfoObj] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "Radius").writeNullable[Int] and
    (__ \ "Count").write[Int] and
    (__ \ "OrderBy").writeNullable[String]and
    (__ \ "Sort").write[String] and
    (__ \ "DoctorIDs").writeNullable[String])(unlift(PatientAppDoctorSearchDetailsObj.unapply))
  implicit val PatientAppDoctorSearchDetailsObjFormat: Format[PatientAppDoctorSearchDetailsObj] = Format(PatientAppDoctorSearchDetailsObjReads, PatientAppDoctorSearchDetailsObjWrites)
}

case class AvailabilityInfoObj(FilterDay: Option[String], StartTime:Option[String],EndTime: Option[String],DisStartrange:Option[Double],DisEndRange:Option[Double],
                              Book:Option[String],Available:Option[String])

object AvailabilityInfoObj {
  val AvailabilityInfoObjReads = ( 
    (__ \ "FilterDay").readNullable[String] and
    (__ \ "StartTime").readNullable[String] and
    (__ \ "EndTime").readNullable[String] and
    (__ \ "DisStartRange").readNullable[Double] and
    (__ \ "DisEndRange").readNullable[Double]and
    (__ \ "Book").readNullable[String]and
    (__ \ "Available").readNullable[String])(AvailabilityInfoObj.apply _)

  val AvailabilityInfoObjWrites = (
    (__ \ "FilterDay").writeNullable[String] and
    (__ \ "StartTime").writeNullable[String] and
    (__ \ "EndTime").writeNullable[String] and
    (__ \ "DisStartRange").writeNullable[Double] and
    (__ \ "DisEndRange").writeNullable[Double] and
    (__ \ "Book").writeNullable[String]and
    (__ \ "Available").writeNullable[String])(unlift(AvailabilityInfoObj.unapply))
  implicit val AvailabilityInfoObjFormat: Format[AvailabilityInfoObj] = Format(AvailabilityInfoObjReads, AvailabilityInfoObjWrites)

}
case class updateDoctorPinPassObj(DoctorLoginID: String, PinPassCode: String, TermsAccepted: String, USRId: Int, USRType: String)

object updateDoctorPinPassObj {
  val updateDoctorPinPassObjReads = (
    (__ \ "DoctorLoginID").read[String] and
    (__ \ "PinPassCode").read[String] and
    (__ \ "TermsAccepted").read[String] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(updateDoctorPinPassObj.apply _)

  val updateDoctorPinPassObjWrites = (
    (__ \ "DoctorLoginID").write[String] and
    (__ \ "PinPassCode").write[String] and
    (__ \ "TermsAccepted").write[String] and
    (__ \ "USRId").write[Int] and (__ \ "USRType").write[String])(unlift(updateDoctorPinPassObj.unapply))
  implicit val validateDoctorPinPasDetailsFormat: Format[updateDoctorPinPassObj] = Format(updateDoctorPinPassObjReads, updateDoctorPinPassObjWrites)

}

case class addUpdateAppointmentDetailsObj(AppointmentID: Option[Long], AddressID: Long, DayCD: String, ScheduleDate: java.sql.Date, StartTime: String,
                                         EndTime: String, PatientID: Long, DoctorID: Long, VisitUserTypeID: String, DoctorVisitRsnID: String,
                                         OtherVisitRsn: Option[String], AppointmentStatus: String,Reason : Option[String],HospitalDetails:Option[hospitalDetailsObj],SendNotification: Option[sendNotificationObj], 
                                         VisitRsnID: Option[Long], PreliminaryNotes: Option[String], ReferralSourceDtls: Option[sourceDetailsObj], AppointmentOtherDetails: Option[appointmentOtherDetailsObj], USRId: Long, USRType: String)
                                       

object addUpdateAppointmentDetailsObj {
  val addUpdateAppointmentDetailsObjReads = (
    (__ \ "AppointmentID").readNullable[Long] and
    (__ \ "AddressID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ScheduleDate").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "PatientID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "OtherVisitRsn").readNullable[String] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "Reason").readNullable[String] and
    (__ \ "HospitalDetails").readNullable[hospitalDetailsObj] and
    (__ \ "SendNotification").readNullable[sendNotificationObj] and
    (__ \ "VisitRsnID").readNullable[Long] and
    (__ \ "PreliminaryNotes").readNullable[String] and
    (__ \ "ReferralSourceDtls").readNullable[sourceDetailsObj] and
    (__ \ "AppointmentOtherDetails").readNullable[appointmentOtherDetailsObj] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateAppointmentDetailsObj.apply _)

  val addUpdateAppointmentDetailsObjWrites = (
    (__ \ "AppointmentID").writeNullable[Long] and
    (__ \ "AddressID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ScheduleDate").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "PatientID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "DoctorVisitRsnID").write[String] and
    (__ \ "OtherVisitRsn").writeNullable[String] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "Reason").writeNullable[String] and
    (__ \ "HospitalDetails").writeNullable[hospitalDetailsObj] and
    (__ \ "SendNotification").writeNullable[sendNotificationObj] and
    (__ \ "VisitRsnID").writeNullable[Long] and
    (__ \ "PreliminaryNotes").writeNullable[String] and
    (__ \ "ReferralSourceDtls").writeNullable[sourceDetailsObj] and
    (__ \ "AppointmentOtherDetails").writeNullable[appointmentOtherDetailsObj] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateAppointmentDetailsObj.unapply))
  implicit val addUpdateAppointmentDetailsObjFormat: Format[addUpdateAppointmentDetailsObj] = Format(addUpdateAppointmentDetailsObjReads, addUpdateAppointmentDetailsObjWrites)

}

case class appointmentOtherDetailsObj(ReasonForCancellation: Option[String], Count: Option[Int], RecurringAppointment: Option[recurringAppointmentObj])

object appointmentOtherDetailsObj {
  val appointmentOtherDetailsObjReads = (
    (__ \ "ReasonForCancellation").readNullable[String] and
    (__ \ "Count").readNullable[Int] and
    (__ \ "RecurringAppointment").readNullable[recurringAppointmentObj])(appointmentOtherDetailsObj.apply _)

  val appointmentOtherDetailsObjWrites = (
    (__ \ "ReasonForCancellation").writeNullable[String] and
    (__ \ "Count").writeNullable[Int] and
    (__ \ "RecurringAppointment").writeNullable[recurringAppointmentObj])(unlift(appointmentOtherDetailsObj.unapply))
  implicit val appointmentOtherDetailsObjFormat: Format[appointmentOtherDetailsObj] = Format(appointmentOtherDetailsObjReads, appointmentOtherDetailsObjWrites)

}

case class recurringAppointmentObj(Interval: Int, Mode: String, StartDate: java.sql.Date, EndDate: java.sql.Date)

object recurringAppointmentObj {
  val recurringAppointmentObjReads = (
    (__ \ "Interval").read[Int] and
    (__ \ "Mode").read[String] and
    (__ \ "StartDate").read[java.sql.Date] and
    (__ \ "EndDate").read[java.sql.Date])(recurringAppointmentObj.apply _)

  val recurringAppointmentObjWrites = (
    (__ \ "Interval").write[Int] and
    (__ \ "Mode").write[String] and
    (__ \ "StartDate").write[java.sql.Date] and
    (__ \ "EndDate").write[java.sql.Date])(unlift(recurringAppointmentObj.unapply))
  implicit val recurringAppointmentObjFormat: Format[recurringAppointmentObj] = Format(recurringAppointmentObjReads, recurringAppointmentObjWrites)

}


case class addUpdateEventsDetailsObj(AppointmentID: Option[Long], AddressID: Option[Long], DayCD: String, StartDate: java.sql.Date, StartTime: String,
                                          EndDate: java.sql.Date, EndTime: String, HospitalID:Option[Long], DoctorID: Option[Long], AppointmentStatus: String,EventName : String,EventDescription:Option[String],EventLocation:Option[String],Reason:Option[String],RescheduleAppointment: Option[String], SendNotification: Option[sendEventNotificationObj],PostPoneFlag: Option[String], USRId: Long, USRType: String)

object addUpdateEventsDetailsObj {
  val addUpdateEventsDetailsObjReads = (
    (__ \ "AppointmentID").readNullable[Long] and
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "StartDate").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndDate").read[java.sql.Date] and
    (__ \ "EndTime").read[String] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "EventName").read[String] and
    (__ \ "EventDescription").readNullable[String] and
    (__ \ "EventLocation").readNullable[String] and
    (__ \ "Reason").readNullable[String] and
    (__ \ "RescheduleAppointment").readNullable[String] and
    (__ \ "SendNotification").readNullable[sendEventNotificationObj] and
    (__ \ "PostPoneFlag").readNullable[String] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateEventsDetailsObj.apply _)

  val addUpdateEventsDetailsObjWrites = (
    (__ \ "AppointmentID").writeNullable[Long] and
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "StartDate").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndDate").write[java.sql.Date] and
    (__ \ "EndTime").write[String] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "EventName").write[String] and
    (__ \ "EventDescription").writeNullable[String] and
    (__ \ "EventLocation").writeNullable[String] and
    (__ \ "Reason").writeNullable[String] and
    (__ \ "RescheduleAppointment").writeNullable[String] and
    (__ \ "SendNotification").writeNullable[sendEventNotificationObj] and
    (__ \ "PostPoneFlag").writeNullable[String] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateEventsDetailsObj.unapply))
  implicit val addUpdateEventsDetailsObjFormat: Format[addUpdateEventsDetailsObj] = Format(addUpdateEventsDetailsObjReads, addUpdateEventsDetailsObjWrites)

}

case class sendEventNotificationObj(sendPatientSMS: String, sendPatientEmail: String, sendDoctorSMS: String, sendDoctorEmail: String)

object sendEventNotificationObj {
  val sendEventNotificationObjReads = (
    (__ \ "sendPatientSMS").read[String] and
    (__ \ "sendPatientEmail").read[String] and 
    (__ \ "sendDoctorSMS").read[String] and
    (__ \ "sendDoctorEmail").read[String])(sendEventNotificationObj.apply _)

  val sendEventNotificationObjWrites = (
    (__ \ "sendPatientSMS").write[String] and
    (__ \ "sendPatientEmail").write[String] and
    (__ \ "sendDoctorSMS").write[String] and
    (__ \ "sendDoctorEmail").write[String])(unlift(sendEventNotificationObj.unapply))
  implicit val sendEventNotificationObjFormat: Format[sendEventNotificationObj] = Format(sendEventNotificationObjReads, sendEventNotificationObjWrites)

}

case class sendNotificationObj(sendPatientSMS: String, sendPatientEmail: String, sendDoctorReminderSms: String, sendDoctorPushNotification: String)

object sendNotificationObj {
  val sendNotificationObjReads = (
    (__ \ "sendPatientSMS").read[String] and
    (__ \ "sendPatientEmail").read[String] and 
    (__ \ "sendDoctorReminderSms").read[String] and 
    (__ \ "sendDoctorPushNotification").read[String])(sendNotificationObj.apply _)

  val sendNotificationObjWrites = (
    (__ \ "sendPatientSMS").write[String] and
    (__ \ "sendPatientEmail").write[String] and
    (__ \ "sendDoctorReminderSms").write[String] and
    (__ \ "sendDoctorPushNotification").write[String])(unlift(sendNotificationObj.unapply))
  implicit val sendNotificationObjFormat: Format[sendNotificationObj] = Format(sendNotificationObjReads, sendNotificationObjWrites)

}

case class listCalenderComponentRequestObj(doctorID:Long, hospitalCD: Option[String], hospitalID: Option[Long], addressID: Option[Long], startDate: java.sql.Date, endDate: java.sql.Date,
                                           usrId: Long, usrType: String)

object listCalenderComponentRequestObj {
  val listCalenderComponentRequestObjReads = (
    (__ \ "doctorID").read[Long] and  
    (__ \ "hospitalCD").readNullable[String] and
    (__ \ "hospitalID").readNullable[Long] and
    (__ \ "addressID").readNullable[Long] and
    (__ \ "startDate").read[java.sql.Date] and
    (__ \ "endDate").read[java.sql.Date] and
    (__ \ "usrId").read[Long] and
    (__ \ "usrType").read[String])(listCalenderComponentRequestObj.apply _)

  val listCalenderComponentRequestObjWrites = (
    (__ \ "doctorID").write[Long] and  
    (__ \ "hospitalCD").writeNullable[String] and
    (__ \ "hospitalID").writeNullable[Long] and
    (__ \ "addressID").writeNullable[Long] and
    (__ \ "startDate").write[java.sql.Date] and
    (__ \ "endDate").write[java.sql.Date] and
    (__ \ "usrId").write[Long] and
    (__ \ "usrType").write[String])(unlift(listCalenderComponentRequestObj.unapply))
  implicit val listCalenderComponentRequestObjFormat: Format[listCalenderComponentRequestObj] = Format(listCalenderComponentRequestObjReads, listCalenderComponentRequestObjWrites)
}

case class AppointmentStatusUpdateObj(AppointmentID:Long,AppointmentStatus: String,ScanStartTime:Option[String], ScanEndtime:Option[String]
                           ,Notes: Option[String])

object AppointmentStatusUpdateObj {

  val AppointmentStatusUpdateObjReads = ( 
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "ScanStartTime").readNullable[String] and
    (__ \ "ScanEndtime").readNullable[String] and
    (__ \ "Notes").readNullable[String])(AppointmentStatusUpdateObj.apply _)

  val AppointmentStatusUpdateObjWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "ScanStartTime").writeNullable[String] and
    (__ \ "ScanEndtime").writeNullable[String] and
    (__ \ "Notes").writeNullable[String])(unlift(AppointmentStatusUpdateObj.unapply))
  implicit val AppointmentStatusUpdateObjFormat: Format[AppointmentStatusUpdateObj] = Format(AppointmentStatusUpdateObjReads, AppointmentStatusUpdateObjWrites)

}

case class appointmentresponseObj(message: String,status:Long)

object appointmentresponseObj {
  val observationressponseObjReads = (
    (__ \ "message").read[String]and
    (__ \ "status").read[Long])(appointmentresponseObj.apply _)

  val observationressponseObjWrites = (
    (__ \ "message").write[String]and
    (__ \ "status").write[Long])(unlift(appointmentresponseObj.unapply))
  implicit val observationressponseObjFormat: Format[appointmentresponseObj] = Format(observationressponseObjReads, observationressponseObjWrites)
}


case class listCalenderComponentResponseObj(hospitalName: Option[String], hospitalID: Option[Long], appointmentCounts: Option[appointmentsCountResponseObj], calendarList: Array[calenderObj])

object listCalenderComponentResponseObj {
  val listCalenderComponentResponseObjReads = (
    (__ \ "hospitalName").readNullable[String] and
    (__ \ "hospitalID").readNullable[Long] and
    (__ \ "appointmentCounts").readNullable[appointmentsCountResponseObj] and
    (__ \ "calendarList").read[Array[calenderObj]])(listCalenderComponentResponseObj.apply _)

  val listCalenderComponentResponseObjWrites = (
    (__ \ "hospitalName").writeNullable[String] and
    (__ \ "hospitalID").writeNullable[Long] and
    (__ \ "appointmentCounts").writeNullable[appointmentsCountResponseObj] and
    (__ \ "calendarList").write[Array[calenderObj]])(unlift(listCalenderComponentResponseObj.unapply))
  implicit val listCalenderComponentRequestObjFormat: Format[listCalenderComponentResponseObj] = Format(listCalenderComponentResponseObjReads, listCalenderComponentResponseObjWrites)
}

case class appointmentsCountResponseObj(bookedCount: Int, checkedInCount: Int, startedCount: Int, completedCount: Int, eventsCount: Int)

object appointmentsCountResponseObj {
  val appointmentsCountResponseObjReads = (
    (__ \ "bookedCount").read[Int] and
    (__ \ "checkedInCount").read[Int] and
    (__ \ "startedCount").read[Int] and
    (__ \ "completedCount").read[Int] and
    (__ \ "eventsCount").read[Int])(appointmentsCountResponseObj.apply _)

  val appointmentsCountResponseObjWrites = (
    (__ \ "bookedCount").write[Int] and
    (__ \ "checkedInCount").write[Int] and
    (__ \ "startedCount").write[Int] and
    (__ \ "completedCount").write[Int] and
    (__ \ "eventsCount").write[Int])(unlift(appointmentsCountResponseObj.unapply))
  implicit val appointmentsCountResponseObjFormat: Format[appointmentsCountResponseObj] = Format(appointmentsCountResponseObjReads, appointmentsCountResponseObjWrites)
}

case class appointmentBasicDtlObj(seqNo: Long, appointmentID: Long, consultationDt: java.sql.Date, startTime: String, endTime: String)

object appointmentBasicDtlObj {
  val appointmentBasicDtlObjReads = (
    (__ \ "seqNo").read[Long] and
    (__ \ "appointmentID").read[Long] and
    (__ \ "consultationDt").read[java.sql.Date] and
    (__ \ "startTime").read[String] and
    (__ \ "endTime").read[String])(appointmentBasicDtlObj.apply _)

  val appointmentBasicDtlObjWrites = (
    (__ \ "seqNo").write[Long] and
    (__ \ "appointmentID").write[Long] and
    (__ \ "consultationDt").write[java.sql.Date] and
    (__ \ "startTime").write[String] and
    (__ \ "endTime").write[String])(unlift(appointmentBasicDtlObj.unapply))
  implicit val appointmentBasicDtlObjFormat: Format[appointmentBasicDtlObj] = Format(appointmentBasicDtlObjReads, appointmentBasicDtlObjWrites)
}

case class consultationDtlObj(addressConsultId: Long, addressId: Option[Long], hospitalId: Option[Long])

object consultationDtlObj {
  val consultationDtlObjReads = (
    (__ \ "addressConsultId").read[Long] and
    (__ \ "addressId").readNullable[Long] and
    (__ \ "hospitalId").readNullable[Long])(consultationDtlObj.apply _)

  val consultationDtlObjWrites = (
    (__ \ "addressConsultId").write[Long] and
    (__ \ "addressId").writeNullable[Long] and
    (__ \ "hospitalId").writeNullable[Long])(unlift(consultationDtlObj.unapply))
  implicit val consultationDtlObjFormat: Format[consultationDtlObj] = Format(consultationDtlObjReads, consultationDtlObjWrites)
}

case class calenderObj(consultationDt: java.sql.Date,addressconsultid: Long,emergencyslotstarttime: String,emergencyslotendtime : String,minpercaseslot: Int, slotList: Array[slotListObj])

object calenderObj {
  val calenderObjReads = (
    (__ \ "consultationDt").read[java.sql.Date] and
    (__ \ "addressconsultid").read[Long] and
    (__ \ "emergencyslotstarttime").read[String] and
    (__ \ "emergencyslotendtime").read[String] and
    (__ \ "minpercaseslot").read[Int] and
    (__ \ "slotList").read[Array[slotListObj]])(calenderObj.apply _)

  val calenderObjWrites = (
    (__ \ "consultationDt").write[java.sql.Date] and
    (__ \ "addressconsultid").write[Long] and
    (__ \ "emergencyslotstarttime").write[String] and
    (__ \ "emergencyslotendtime").write[String] and
    (__ \ "minpercaseslot").write[Int] and
    (__ \ "slotList").write[Array[slotListObj]])(unlift(calenderObj.unapply))
  implicit val listCalenderComponentRequestObjFormat: Format[calenderObj] = Format(calenderObjReads, calenderObjWrites)
}

case class slotListObj(addressID: Option[Long],hospitalID: Option[Long], doctorID: Option[Long], doctorInfo: Option[doctorPersonalInfo], sequence: Long, startTime: String, 
                       endTime: String, allDayEvents: Option[String], consultationDt: java.sql.Date, startDate: java.sql.Date, endDate: java.sql.Date, dayCD: String, appointmentID: Long, appointmentStatus: String, eventName: Option[String], eventDescription: Option[String],eventLocation: Option[String],otherVisitRsn: Option[String], doctorVisitRsnId: String, tokenNumber: String, 
                       patientInfo: Option[patientPersonalInfo])

object slotListObj {
  val slotListObjReads = (
    (__ \ "addressID").readNullable[Long] and
    (__ \ "hospitalID").readNullable[Long] and
    (__ \ "doctorID").readNullable[Long] and
    (__ \ "doctorInfo").readNullable[doctorPersonalInfo] and
    (__ \ "sequence").read[Long] and
    (__ \ "startTime").read[String] and
    (__ \ "endTime").read[String] and
    (__ \ "allDayEvents").readNullable[String] and
    (__ \ "consultationDt").read[java.sql.Date] and 
    (__ \ "startDate").read[java.sql.Date] and
    (__ \ "endDate").read[java.sql.Date] and
    (__ \ "dayCD").read[String] and
    (__ \ "appointmentID").read[Long] and
    (__ \ "appointmentStatus").read[String] and
    (__ \ "eventName").readNullable[String] and
    (__ \ "eventDescription").readNullable[String] and
    (__ \ "eventLocation").readNullable[String] and
    (__ \ "otherVisitRsn").readNullable[String] and
    (__ \ "doctorVisitRsnId").read[String] and
    (__ \ "tokenNumber").read[String] and
    (__ \ "patientInfo").readNullable[patientPersonalInfo])(slotListObj.apply _)

  val slotListObjWrites = (
    (__ \ "addressID").writeNullable[Long] and
    (__ \ "hospitalID").writeNullable[Long] and
    (__ \ "doctorID").writeNullable[Long] and
    (__ \ "doctorInfo").writeNullable[doctorPersonalInfo] and
    (__ \ "sequence").write[Long] and
    (__ \ "startTime").write[String] and
    (__ \ "endTime").write[String] and
    (__ \ "allDayEvents").writeNullable[String] and
    (__ \ "consultationDt").write[java.sql.Date] and
    (__ \ "startDate").write[java.sql.Date] and
    (__ \ "endDate").write[java.sql.Date] and
    (__ \ "dayCD").write[String] and
    (__ \ "appointmentID").write[Long] and
    (__ \ "appointmentStatus").write[String] and
    (__ \ "eventName").writeNullable[String] and
    (__ \ "eventDescription").writeNullable[String] and
    (__ \ "eventLocation").writeNullable[String] and
    (__ \ "otherVisitRsn").writeNullable[String] and
    (__ \ "doctorVisitRsnId").write[String] and
    (__ \ "tokenNumber").write[String] and
    (__ \ "patientInfo").writeNullable[patientPersonalInfo])(unlift(slotListObj.unapply))
  implicit val slotListObjFormat: Format[slotListObj] = Format(slotListObjReads, slotListObjWrites)
}

case class doctorPersonalInfo(doctorName: String, doctorImage: Option[String], specialityCd:Option[Map[String, String]])

object doctorPersonalInfo {
  val doctorPersonalInfoReads = (
    (__ \ "doctorName").read[String] and
    (__ \ "doctorImage").readNullable[String] and
    (__ \ "specialityCd").readNullable[Map[String, String]])(doctorPersonalInfo.apply _)

  val doctorPersonalInfoWrites = (
    (__ \ "doctorName").write[String] and
    (__ \ "doctorImage").writeNullable[String] and
    (__ \ "specialityCd").writeNullable[Map[String, String]])(unlift(doctorPersonalInfo.unapply))
  implicit val doctorPersonalInfoFormat: Format[doctorPersonalInfo] = Format(doctorPersonalInfoReads, doctorPersonalInfoWrites)

}

case class patientPersonalInfo(patientID: Long, title: Option[String], fullName:String,profileImage:Option[String],patDisplayID: Option[String], familyDisplayID: Option[String], patLastVisited:Option[java.sql.Date], age: Option[Double], sex: String, currentAge:Option[currentAgeObj], mobileNumber: Option[String])

object patientPersonalInfo {
  val patientPersonalInfoReads = (
    (__ \ "patientID").read[Long] and
    (__ \ "title").readNullable[String] and
    (__ \ "fullName").read[String] and
    (__ \ "profileImage").readNullable[String] and
    (__ \ "patDisplayID").readNullable[String] and
    (__ \ "familyDisplayID").readNullable[String] and
    (__ \ "patLastVisited").readNullable[java.sql.Date] and
    (__ \ "age").readNullable[Double] and
    (__ \ "sex").read[String] and
    (__ \ "currentAge").readNullable[currentAgeObj] and
    (__ \ "mobileNumber").readNullable[String])(patientPersonalInfo.apply _)

  val patientPersonalInfoWrites = (
    (__ \ "patientID").write[Long] and
    (__ \ "title").writeNullable[String] and
    (__ \ "fullName").write[String] and
    (__ \ "profileImage").writeNullable[String] and
    (__ \ "patDisplayID").writeNullable[String] and
    (__ \ "familyDisplayID").writeNullable[String] and
    (__ \ "patLastVisited").writeNullable[java.sql.Date] and
    (__ \ "age").writeNullable[Double] and
    (__ \ "sex").write[String] and
    (__ \ "currentAge").writeNullable[currentAgeObj] and
    (__ \ "mobileNumber").writeNullable[String])(unlift(patientPersonalInfo.unapply))
  implicit val patientPersonalInfoFormat: Format[patientPersonalInfo] = Format(patientPersonalInfoReads, patientPersonalInfoWrites)

}


case class setSmsEmailContextObj(appointmentID: Long, doctorID: Long, patientID: Long, hospitalCd: Option[String], patientName: String,  tokenNumber: String, consultTime: String, consultDate: java.sql.Date, doctorName: String, doctorClinicName: String,  doctorClinicAddress: String, 
          doctorClinicPhone: Option[Long], doctorSpeciality: Option[ String], patientPhone:Option[Long], patientEmail: Option[String],doctorEmail:String, doctorRoleId: Option[String], doctorQualification: String, showDoctorName: Option[String], hospitalPreference: Option[Map[String, String]])

object setSmsEmailContextObj {
val setSmsEmailContextObjReads = (
	(__ \ "appointmentID").read[Long] and
	(__ \ "doctorID").read[Long] and
	(__ \ "patientID").read[Long] and
	(__ \ "hospitalCd").readNullable[String] and
	(__ \ "patientName").read[String] and
	(__ \ "tokenNumber").read[String] and
	(__ \ "consultTime").read[String] and
	(__ \ "consultDate").read[java.sql.Date] and
	(__ \ "doctorName").read[String] and
	(__ \ "doctorClinicName").read[String] and
	(__ \ "doctorClinicAddress").read[String] and
	(__ \ "doctorClinicPhone").readNullable[Long] and
	(__ \ "doctorSpeciality").readNullable[String] and
	(__ \ "patientPhone").readNullable[Long] and
	(__ \ "patientEmail").readNullable[String] and
	(__ \ "doctorEmail").read[String] and
	(__ \ "doctorRoleId").readNullable[String] and
	(__ \ "doctorQualification").read[String] and
	(__ \ "showDoctorName").readNullable[String] and
    (__ \ "hospitalPreference").readNullable[Map[String, String]])(setSmsEmailContextObj.apply _)

val setSmsEmailContextObjWrites = (
	(__ \ "appointmentID").write[Long] and 
	(__ \ "doctorID").write[Long] and
	(__ \ "patientID").write[Long] and
	(__ \ "hospitalCd").writeNullable[String] and
	(__ \ "patientName").write[String] and
	(__ \ "tokenNumber").write[String] and
	(__ \ "consultTime").write[String] and
	(__ \ "consultDate").write[java.sql.Date] and
	(__ \ "doctorName").write[String] and
	(__ \ "doctorClinicName").write[String] and
	(__ \ "doctorClinicAddress").write[String] and
	(__ \ "doctorClinicPhone").writeNullable[Long] and
	(__ \ "doctorSpeciality").writeNullable[String] and
	(__ \ "patientPhone").writeNullable[Long] and
	(__ \ "patientEmail").writeNullable[String] and
	(__ \ "doctorEmail").write[String] and
	(__ \ "doctorRoleId").writeNullable[String] and
	(__ \ "doctorQualification").write[String] and
	(__ \ "showDoctorName").writeNullable[String] and
    (__ \ "hospitalPreference").writeNullable[Map[String, String]])(unlift(setSmsEmailContextObj.unapply))
implicit val setSmsEmailContextObjFormat: Format[setSmsEmailContextObj] = Format(setSmsEmailContextObjReads, setSmsEmailContextObjWrites)

}

case class addDoctorBlogObj(blogID: Option[Long], doctorID : Long, blogTitle: String , blogDesc : Option[String],  blogImage: Option[JsValue], blogStatus: String, author: Option[String], blogDate: java.sql.Date, blogCatagory:String, likesCount:Option[Int],commentsCount:Option[Int], activeIND: String,
                             updateChanges: Option[String], wordPressId: Option[Long], wordPressStatus: Option[String], wordPressImage: Option[String], usrId: Long,usrType: String)

object addDoctorBlogObj {
  val addDoctorBlogObjReads = (
    (__ \ "blogID").readNullable[Long] and
    (__ \ "doctorID").read[Long] and
    (__ \ "blogTitle").read[String] and
    (__ \ "blogDesc").readNullable[String] and
    (__ \ "blogImage").readNullable[JsValue] and
    (__ \ "blogStatus").read[String] and
    (__ \ "author").readNullable[String] and
    (__ \ "blogDate").read[java.sql.Date] and
    (__ \ "blogCatagory").read[String] and
    (__ \ "likesCount").readNullable[Int] and
    (__ \ "commentsCount").readNullable[Int] and
    (__ \ "activeIND").read[String] and
    (__ \ "updateChanges").readNullable[String] and
    (__ \ "wordPressId").readNullable[Long] and
    (__ \ "wordPressStatus").readNullable[String] and
    (__ \ "wordPressImage").readNullable[String] and
    (__ \ "usrId").read[Long] and
    (__ \ "usrType").read[String])(addDoctorBlogObj.apply _)

  val addDoctorBlogObjWrites = (
    (__ \ "blogID").writeNullable[Long] and
    (__ \ "doctorID").write[Long] and
    (__ \ "blogTitle").write[String] and
    (__ \ "blogDesc").writeNullable[String] and
    (__ \ "blogImage").writeNullable[JsValue] and
    (__ \ "blogStatus").write[String] and
    (__ \ "author").writeNullable[String] and
    (__ \ "blogDate").write[java.sql.Date] and
    (__ \ "blogCatagory").write[String] and
    (__ \ "likesCount").writeNullable[Int] and
    (__ \ "commentsCount").writeNullable[Int] and
    (__ \ "activeIND").write[String] and
    (__ \ "updateChanges").writeNullable[String] and
    (__ \ "wordPressId").writeNullable[Long] and
    (__ \ "wordPressStatus").writeNullable[String] and
    (__ \ "wordPressImage").writeNullable[String] and
    (__ \ "usrId").write[Long] and
    (__ \ "usrType").write[String])(unlift(addDoctorBlogObj.unapply))
  implicit val addDoctorBlogObjFormat: Format[addDoctorBlogObj] = Format(addDoctorBlogObjReads, addDoctorBlogObjWrites)
}

case class listDoctorBlogObj (blogID: Long, doctorID : Long, blogTitle: String , blogDesc : Option[String],  blogImage: Option[JsValue], blogStatus: String, author: Option[String], blogDate: java.sql.Date, 
                              blogCatagory:String,wordPressID: Option[Long],wordPressStatus: Option[String], likesCount:Option[Int],commentsCount:Option[Int], activeIND: String)


object listDoctorBlogObj {
  val listDoctorBlogObjReads = (
    (__ \ "blogID").read[Long] and
    (__ \ "doctorID").read[Long] and
    (__ \ "blogTitle").read[String] and
    (__ \ "blogDesc").readNullable[String] and
    (__ \ "blogImage").readNullable[JsValue] and
    (__ \ "blogStatus").read[String] and
    (__ \ "author").readNullable[String] and
    (__ \ "blogDate").read[java.sql.Date] and
    (__ \ "blogCatagory").read[String] and 
    (__ \ "wordPressID").readNullable[Long] and
    (__ \ "wordPressStatus").readNullable[String] and
    (__ \ "likesCount").readNullable[Int] and
    (__ \ "commentsCount").readNullable[Int] and
    (__ \ "activeIND").read[String])(listDoctorBlogObj.apply _)

  val listDoctorBlogObjWrites = ( 
    (__ \ "blogID").write[Long] and
    (__ \ "doctorID").write[Long] and
    (__ \ "blogTitle").write[String] and
    (__ \ "blogDesc").writeNullable[String] and
    (__ \ "blogImage").writeNullable[JsValue] and
    (__ \ "blogStatus").write[String] and
    (__ \ "author").writeNullable[String] and
    (__ \ "blogDate").write[java.sql.Date] and
    (__ \ "blogCatagory").write[String] and
    (__ \ "wordPressID").writeNullable[Long] and
    (__ \ "wordPressStatus").writeNullable[String] and
    (__ \ "likesCount").writeNullable[Int] and
    (__ \ "commentsCount").writeNullable[Int] and
    (__ \ "activeIND").write[String])(unlift(listDoctorBlogObj.unapply))
  implicit val listDoctorBlogObjFormat: Format[listDoctorBlogObj] = Format(listDoctorBlogObjReads, listDoctorBlogObjWrites)
}

case class getBlogObj(doctorID: Long, blogID: Option[Long], wordPressID: Option[String], usrId:Long,usrType:String)

object getBlogObj {
  val getBlogObjReads = (
    (__ \ "doctorID").read[Long] and
    (__ \ "blogID").readNullable[Long] and
    (__ \ "wordPressID").readNullable[String] and
    (__ \ "usrId").read[Long] and
    (__ \ "usrType").read[String])(getBlogObj.apply _)

  val getBlogObjWrites = ( 
    (__ \ "doctorID").write[Long] and
    (__ \ "blogID").writeNullable[Long] and
    (__ \ "wordPressID").writeNullable[String] and
    (__ \ "usrId").write[Long] and
    (__ \ "usrType").write[String])(unlift(getBlogObj.unapply))
  implicit val listDoctorBlogObjFormat: Format[getBlogObj] = Format(getBlogObjReads, getBlogObjWrites)
}


case class postBlogObj(title: String, status: String, content:String, featured_media: Int, categoriess: Array[String])

object postBlogObj {
  val postBlogObjReads = (
    (__ \ "title").read[String] and
    (__ \ "status").read[String] and
    (__ \ "content").read[String] and 
    (__ \ "featured_media").read[Int] and
    (__ \ "categoriess").read[Array[String]])(postBlogObj.apply _)

  val postBlogObjWrites = ( 
    (__ \ "title").write[String] and
    (__ \ "status").write[String] and
    (__ \ "content").write[String] and
    (__ \ "featured_media").write[Int] and
    (__ \ "categoriess").write[Array[String]])(unlift(postBlogObj.unapply))
  implicit val postBlogObjFormat: Format[postBlogObj] = Format(postBlogObjReads, postBlogObjWrites)
}

case class featuredImageObj(fileExtn: String, file: String)

object featuredImageObj {
  val featuredImageObjReads = (
    (__ \ "fileExtn").read[String] and
    (__ \ "file").read[String])(featuredImageObj.apply _)

  val featuredImageObjWrites = ( 
    (__ \ "fileExtn").write[String] and
    (__ \ "file").write[String])(unlift(featuredImageObj.unapply))
  implicit val featuredImageObjFormat: Format[featuredImageObj] = Format(featuredImageObjReads, featuredImageObjWrites)
}

case class blogImageObj(featured_image: Int, large: String, medium: String, original: String, small: String, thumb: String)

object blogImageObj {
  val blogImageObjReads = (
    (__ \ "featured_image").read[Int] and  
    (__ \ "large").read[String] and
    (__ \ "medium").read[String] and
    (__ \ "original").read[String] and
    (__ \ "small").read[String] and
    (__ \ "thumb").read[String])(blogImageObj.apply _)

  val blogImageObjWrites = (
    (__ \ "featured_image").write[Int] and  
    (__ \ "large").write[String] and
    (__ \ "medium").write[String] and
    (__ \ "original").write[String] and
    (__ \ "small").write[String] and
    (__ \ "thumb").write[String])(unlift(blogImageObj.unapply))
  implicit val featuredImageObjFormat: Format[blogImageObj] = Format(blogImageObjReads, blogImageObjWrites)
}

case class updatedBlogObj(doctorId: Long, title: String, link: String, content:String, date: java.sql.Date)

object updatedBlogObj {
  val updatedBlogObjReads = (
    (__ \ "doctorId").read[Long] and  
    (__ \ "title").read[String] and
    (__ \ "link").read[String] and
    (__ \ "content").read[String] and 
    (__ \ "date").read[java.sql.Date])(updatedBlogObj.apply _)

  val updatedBlogObjWrites = ( 
    (__ \ "doctorId").write[Long] and   
    (__ \ "title").write[String] and
    (__ \ "link").write[String] and
    (__ \ "content").write[String] and
    (__ \ "date").write[java.sql.Date])(unlift(updatedBlogObj.unapply))
  implicit val featuredImageObjFormat: Format[updatedBlogObj] = Format(updatedBlogObjReads, updatedBlogObjWrites)
}
