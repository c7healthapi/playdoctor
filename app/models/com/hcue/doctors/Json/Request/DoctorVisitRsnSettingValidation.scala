/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date

import play.api.libs.functional.syntax._
import models.com.hcue.doctors.slick.lookupTable.TAddressTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TContactTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TCountryLkup
import models.com.hcue.doctors.slick.lookupTable.TDayLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorQualfLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorVisitRsnLkyp
import models.com.hcue.doctors.slick.lookupTable.TFamilyRltnLkup
import models.com.hcue.doctors.slick.lookupTable.TOtherIDTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TPhoneTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TStateLkup
import models.com.hcue.doctors.slick.lookupTable.TUSRTypeLkup

case class addUpdateVisitRsnSettingsObj(DoctorID: Long, HospitalID: Option[Long], HospitalCD: Option[String], VisitRsnID: Option[Long], VisitRsnTitle: String, VisitRsnDesc: Option[String], 
                                        VisitRsnType: Option[String], PreliminaryNotes: Option[String], ActiveIND: String, 
                                        StartsWith: Option[String], PageNumber: Int, PageSize: Int, USRId: Long, USRType: String)

object addUpdateVisitRsnSettingsObj {
  val addUpdateVisitRsnSettingsObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "VisitRsnID").readNullable[Long] and
    (__ \ "VisitRsnTitle").read[String] and
    (__ \ "VisitRsnDesc").readNullable[String] and
    (__ \ "VisitRsnType").readNullable[String] and
    (__ \ "PreliminaryNotes").readNullable[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "StartsWith").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateVisitRsnSettingsObj.apply _)

  val addUpdateVisitRsnSettingsObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "VisitRsnID").writeNullable[Long] and
    (__ \ "VisitRsnTitle").write[String] and
    (__ \ "VisitRsnDesc").writeNullable[String] and
    (__ \ "VisitRsnType").writeNullable[String] and
    (__ \ "PreliminaryNotes").writeNullable[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "StartsWith").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateVisitRsnSettingsObj.unapply))
  implicit val addUpdateReferralSettingsObjFormat: Format[addUpdateVisitRsnSettingsObj] = Format(addUpdateVisitRsnSettingsObjReads, addUpdateVisitRsnSettingsObjWrites)
}

case class listAvailableVisitRsnObj(DoctorID: Long, HospitalID: Option[Long], HospitalCD: Option[String], StartsWith:Option[String], VisitRsnType:Option[String], PageNumber: Int, PageSize: Int,ActiveIND:Option[String],USRId: Long, USRType: String)

object listAvailableVisitRsnObj {
  val listAvailableVisitRsnObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "StartsWith").readNullable[String] and
    (__ \ "VisitRsnType").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "USRId").read[Long] and 
    (__ \ "USRType").read[String])(listAvailableVisitRsnObj.apply _)

  val listAvailableVisitRsnObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "StartsWith").writeNullable[String] and
    (__ \ "VisitRsnType").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "USRId").write[Long] and 
    (__ \ "USRType").write[String])(unlift(listAvailableVisitRsnObj.unapply))
  implicit val listAvailableVisitRsnObjFormat: Format[listAvailableVisitRsnObj] = Format(listAvailableVisitRsnObjReads, listAvailableVisitRsnObjWrites)
}


case class availableVisitRsnResponseObj(VisitRsnID: Long, VisitRsnTitle: String, VisitRsnDesc:Option[String], ActiveIND: String)

object availableVisitRsnResponseObj {
  val availableVisitRsnResponseObjReads = (
    (__ \ "VisitRsnID").read[Long] and
    (__ \ "VisitRsnTitle").read[String] and
    (__ \ "VisitRsnDesc").readNullable[String] and
    (__ \ "ActiveIND").read[String])(availableVisitRsnResponseObj.apply _)

  val availableVisitRsnResponseObjWrites = (
    (__ \ "VisitRsnID").write[Long] and
    (__ \ "VisitRsnTitle").write[String] and
    (__ \ "ReferralDesc").writeNullable[String] and
    (__ \ "ActiveIND").write[String])(unlift(availableVisitRsnResponseObj.unapply))
  implicit val availableVisitRsnResponseObjFormat: Format[availableVisitRsnResponseObj] = Format(availableVisitRsnResponseObjReads, availableVisitRsnResponseObjWrites)
}



