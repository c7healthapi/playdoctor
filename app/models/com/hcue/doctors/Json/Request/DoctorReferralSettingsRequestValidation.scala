/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date

import play.api.libs.functional.syntax._
import models.com.hcue.doctors.slick.lookupTable.TAddressTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TContactTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TCountryLkup
import models.com.hcue.doctors.slick.lookupTable.TDayLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorQualfLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorVisitRsnLkyp
import models.com.hcue.doctors.slick.lookupTable.TFamilyRltnLkup
import models.com.hcue.doctors.slick.lookupTable.TOtherIDTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TPhoneTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TStateLkup
import models.com.hcue.doctors.slick.lookupTable.TUSRTypeLkup

case class addUpdateReferralSettingsObj(DoctorID: Long, HospitalCD: Option[String], ReferralID: Option[Long], ReferralTitle: String, ReferralDesc:String, ActiveIND: String, 
                                      StartsWith: Option[String], PageNumber: Int, PageSize: Int, USRId: Long, USRType: String)

object addUpdateReferralSettingsObj {
  val addUpdateReferralSettingsObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "ReferralID").readNullable[Long] and
    (__ \ "ReferralTitle").read[String] and
    (__ \ "ReferralDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "StartsWith").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateReferralSettingsObj.apply _)

  val addUpdateReferralSettingsObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "ReferralID").writeNullable[Long] and
    (__ \ "ReferralTitle").write[String] and
    (__ \ "ReferralDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "StartsWith").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateReferralSettingsObj.unapply))
  implicit val addUpdateReferralSettingsObjFormat: Format[addUpdateReferralSettingsObj] = Format(addUpdateReferralSettingsObjReads, addUpdateReferralSettingsObjWrites)
}

case class listAvailableReferralObj(DoctorID: Long, HospitalCD: Option[String], StartsWith:Option[String], PageNumber: Int, PageSize: Int, ActiveIND: Option[String], USRId: Long, USRType: String)

object listAvailableReferralObj {
  val listAvailableReferralObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "StartsWith").readNullable[String] and
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "USRId").read[Long] and 
    (__ \ "USRType").read[String])(listAvailableReferralObj.apply _)

  val listAvailableReferralObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "StartsWith").writeNullable[String] and
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "USRId").write[Long] and 
    (__ \ "USRType").write[String])(unlift(listAvailableReferralObj.unapply))
  implicit val listAvailableReferralObjFormat: Format[listAvailableReferralObj] = Format(listAvailableReferralObjReads, listAvailableReferralObjWrites)
}

case class availableReferralResponseObj(ReferralID: Long, ReferralTitle: String, ReferralDesc:String, ActiveIND: String)

object availableReferralResponseObj {
  val availableReferralResponseObjReads = (
    (__ \ "ReferralID").read[Long] and
    (__ \ "ReferralTitle").read[String] and
    (__ \ "ReferralDesc").read[String] and
    (__ \ "ActiveIND").read[String])(availableReferralResponseObj.apply _)

  val availableReferralResponseObjWrites = (
    (__ \ "ReferralID").write[Long] and
    (__ \ "ReferralTitle").write[String] and
    (__ \ "ReferralDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(availableReferralResponseObj.unapply))
  implicit val availableReferralResponseObjFormat: Format[availableReferralResponseObj] = Format(availableReferralResponseObjReads, availableReferralResponseObjWrites)
}