/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.VisitReason

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime
import myUtils.hcueFormats._

case class doctorResonSearchDetailsObj(PageNumber: Int, PageSize: Int, Sort: Option[String], DoctorSpecialityID: String,VisitUserTypeID: String)
//List of Doctor 
object doctorResonSearchDetailsObj {
  val doctorResonSearchDetailsObjReads = (
    (__ \ "PageNumber").read[Int] and
    (__ \ "PageSize").read[Int] and
    (__ \ "Sort").readNullable[String] and
    (__ \ "DoctorSpecialityID").read[String] and
    (__ \ "VisitUserTypeID").read[String])(doctorResonSearchDetailsObj.apply _)

  val doctorResonSearchDetailsObjWrites = (
    (__ \ "PageNumber").write[Int] and
    (__ \ "PageSize").write[Int] and
    (__ \ "Sort").writeNullable[String] and
    (__ \ "DoctorSpecialityID").write[String] and
    (__ \ "VisitUserTypeID").write[String])(unlift(doctorResonSearchDetailsObj.unapply))
  implicit val commonSearchDetailsFormat: Format[doctorResonSearchDetailsObj] = Format(doctorResonSearchDetailsObjReads, doctorResonSearchDetailsObjWrites)

}








