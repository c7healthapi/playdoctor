/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.AddUpdateUserRole

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.functional.syntax.unlift
import play.api.libs.json.Format
import play.api.libs.json.__

case class addUpdateUserRoleObj(RoleID: Long, RoleDesc: String, ActiveIND: String, IsPrivate: String, HospitalID: Option[Long], USRId: Long, USRType: String)

object addUpdateUserRoleObj {

  val addUpdateUserRoleObjReads = (
    (__ \ "RoleID").read[Long] and
    (__ \ "RoleDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "IsPrivate").read[String] and
    (__ \ "HospitalID").read[Option[Long]] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(addUpdateUserRoleObj.apply _)

  val addUpdateUserRoleObjWrites = (
    (__ \ "RoleID").write[Long] and
    (__ \ "RoleDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "IsPrivate").write[String] and
    (__ \ "HospitalID").write[Option[Long]] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(addUpdateUserRoleObj.unapply))
  implicit val addUpdateUserRoleObjFormat: Format[addUpdateUserRoleObj] = Format(addUpdateUserRoleObjReads, addUpdateUserRoleObjWrites)

}
