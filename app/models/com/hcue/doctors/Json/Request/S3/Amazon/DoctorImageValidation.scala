/* Contains Json constructor and implicit Read Write for add Update Doctor Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Request.S3.Amazon


import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime
import myUtils.hcueFormats._

case class doctorProfileImageObj(ImageStr:Option[String], DoctorID: Long, isDeleted: Option[String])

object doctorProfileImageObj {
   val doctorProfileImageObjReads = ((__ \ "ImageStr").readNullable[String] and
    (__ \ "DoctorID").read(Reads.min[Long](1)) and 
    (__ \ "isDeleted").readNullable[String])(doctorProfileImageObj.apply _)

  val doctorProfileImageObjWrites = (
     (__ \ "ImageStr").writeNullable[String] and
     (__ \ "DoctorID").write[Long] and 
     (__ \ "isDeleted").writeNullable[String])(unlift(doctorProfileImageObj.unapply))
  implicit val doctorProfileImageFormat: Format[doctorProfileImageObj] = Format(doctorProfileImageObjReads, doctorProfileImageObjWrites)
}

case class addUpdateDoctorClinicImageObj(ImageStr:Option[String], DoctorID: Long,AddressID: Long,ImageName: Option[String])

object addUpdateDoctorClinicImageObj {
   val addUpdateDoctorClinicImageReads = (
    (__ \ "ImageStr").readNullable[String] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "AddressID").read[Long]and 
    (__ \ "ImageName").readNullable[String])(addUpdateDoctorClinicImageObj.apply _)

  val addUpdateDoctorClinicImageWrites = ((__ \ "ImageStr").writeNullable[String] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "AddressID").write[Long] and 
    (__ \ "ImageName").writeNullable[String])(unlift(addUpdateDoctorClinicImageObj.unapply))
  implicit val addUpdateDoctorClinicImageFormat: Format[addUpdateDoctorClinicImageObj] = Format(addUpdateDoctorClinicImageReads, addUpdateDoctorClinicImageWrites)
}



case class deleteDoctorClinicImageObj(DoctorID: Long,AddressID: Long,DeleteAll:String,ImageName:Array[String])

object deleteDoctorClinicImageObj {
   val deleteDoctorClinicImageReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "AddressID").read[Long]and 
    (__ \ "DeleteAll").read[String]and 
    (__ \ "ImageName").read[Array[String]])(deleteDoctorClinicImageObj.apply _)

  val deleteDoctorClinicImageWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "AddressID").write[Long] and 
    (__ \ "DeleteAll").write[String] and 
    (__ \ "ImageName").write[Array[String]])(unlift(deleteDoctorClinicImageObj.unapply))
  implicit val addUpdateDoctorClinicImageFormat: Format[deleteDoctorClinicImageObj] = Format(deleteDoctorClinicImageReads, deleteDoctorClinicImageWrites)
}


case class addUpdateDoctorCareerImageObj(ImageStr:Option[String], DoctorID: Long,ImageName: Option[String])

object addUpdateDoctorCareerImageObj {
   val addUpdateDoctorCareerImageReads = (
    (__ \ "ImageStr").readNullable[String] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "ImageName").readNullable[String])(addUpdateDoctorCareerImageObj.apply _)

  val addUpdateDoctorCareerImageWrites = ((__ \ "ImageStr").writeNullable[String] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "ImageName").writeNullable[String])(unlift(addUpdateDoctorCareerImageObj.unapply))
  implicit val addUpdateDoctorCareerImageFormat: Format[addUpdateDoctorCareerImageObj] = Format(addUpdateDoctorCareerImageReads, addUpdateDoctorCareerImageWrites)
}

case class addUpdateDoctorPrintSettingImageObj(ImageStr:Option[String], DoctorID: Long,ImageName: Option[String], ScreenType: String)

object addUpdateDoctorPrintSettingImageObj {
   val addUpdateDoctorPrintSettingImageObjReads = (
    (__ \ "ImageStr").readNullable[String] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "ImageName").readNullable[String] and
    (__ \ "ScreenType").read[String])(addUpdateDoctorPrintSettingImageObj.apply _)

  val addUpdateDoctorPrintSettingImageObjWrites = ((__ \ "ImageStr").writeNullable[String] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "ImageName").writeNullable[String] and
    (__ \ "ScreenType").write[String])(unlift(addUpdateDoctorPrintSettingImageObj.unapply))
  implicit val addUpdateDoctorCareerImageFormat: Format[addUpdateDoctorPrintSettingImageObj] = Format(addUpdateDoctorPrintSettingImageObjReads, addUpdateDoctorPrintSettingImageObjWrites)
}

case class getImageStringObj(ImageURL:Array[String], UsrId: Long, UsrType: String)

object getImageStringObj {
   val getImageStringObjReads = (
    (__ \ "ImageURL").read[Array[String]] and
    (__ \ "UsrId").read[Long] and
    (__ \ "UsrType").read[String])(getImageStringObj.apply _)

  val getImageStringObjWrites = (
    (__ \ "ImageURL").write[Array[String]] and
    (__ \ "UsrId").write[Long] and
    (__ \ "UsrType").write[String])(unlift(getImageStringObj.unapply))
  implicit val getImageStringObjFormat: Format[getImageStringObj] = Format(getImageStringObjReads, getImageStringObjWrites)
}

case class imgStringObj(ImageURL:String, ImageString: String)

object imgStringObj {
   val imgStringObjReads = (
    (__ \ "ImageURL").read[String] and
    (__ \ "ImageString").read[String])(imgStringObj.apply _)

  val imgStringObjWrites = (
    (__ \ "ImageURL").write[String] and
    (__ \ "ImageName").write[String])(unlift(imgStringObj.unapply))
  implicit val imgStringObjFormat: Format[imgStringObj] = Format(imgStringObjReads, imgStringObjWrites)
}


case class fileDetailsObj(FileURL: Option[String], FileName:String, FileExtn: String, DocumentNumber: Option[String], UpLoadedDate: Option[java.sql.Date], UpLoadedBy: Option[String])

object fileDetailsObj {
  val fileDetailsObjReads = (
    (__ \ "FileURL").readNullable[String] and
    (__ \ "FileName").read[String] and
    (__ \ "FileExtn").read[String] and
    (__ \ "DocumentNumber").readNullable[String] and
    (__ \ "UpLoadedDate").readNullable[java.sql.Date] and
    (__ \ "UpLoadedBy").readNullable[String])(fileDetailsObj.apply _)

  val fileDetailsObjWrites = (
    (__ \ "FileURL").writeNullable[String] and
    (__ \ "FileName").write[String] and
    (__ \ "FileExtn").write[String] and
    (__ \ "DocumentNumber").writeNullable[String] and
    (__ \ "UpLoadedDate").writeNullable[java.sql.Date] and
    (__ \ "UpLoadedBy").writeNullable[String])(unlift(fileDetailsObj.unapply))
  implicit val fileDetailsObjFormat: Format[fileDetailsObj] = Format(fileDetailsObjReads, fileDetailsObjWrites)
}


case class doctorDocumentsObj(DoctorID: Long, AddressID: Option[Long], Documents: Array[fileDetailsObj])

object doctorDocumentsObj {
  val doctorDocumentsObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "AddressID").readNullable[Long] and
    (JsPath \ "Documents").read[Array[fileDetailsObj]])(doctorDocumentsObj.apply _)

  val doctorDocumentsObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "AddressID").writeNullable[Long] and
    (JsPath \ "Documents").write[Array[fileDetailsObj]])(unlift(doctorDocumentsObj.unapply))
  implicit val patientDocumentsObjFormat: Format[doctorDocumentsObj] = Format(doctorDocumentsObjReads, doctorDocumentsObjWrites)
}


case class addUpdateHospitalClinicImageObj(ImageStr:Option[String], HospitalID: Long,ImageName: Option[String])

object addUpdateHospitalClinicImageObj {
   val addUpdateHospitalClinicImageReads = (
    (__ \ "ImageStr").readNullable[String] and
    (__ \ "HospitalID").read[Long] and
    (__ \ "ImageName").readNullable[String])(addUpdateHospitalClinicImageObj.apply _)

  val addUpdateHospitalClinicImageWrites = ((__ \ "ImageStr").writeNullable[String] and
    (__ \ "HospitalID").write[Long] and
    (__ \ "ImageName").writeNullable[String])(unlift(addUpdateHospitalClinicImageObj.unapply))
  implicit val addUpdateHospitalClinicImageFormat: Format[addUpdateHospitalClinicImageObj] = Format(addUpdateHospitalClinicImageReads, addUpdateHospitalClinicImageWrites)
}

case class deleteHospitalClinicImageObj(HospitalID: Long,ImageName:String)

object deleteHospitalClinicImageObj {
   val deleteHospitalClinicImageReads = (
    (__ \ "HospitalID").read[Long] and 
    (__ \ "ImageName").read[String])(deleteHospitalClinicImageObj.apply _)

  val deleteHospitalClinicImageWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "ImageName").write[String])(unlift(deleteHospitalClinicImageObj.unapply))
  implicit val deleteHospitalClinicImageFormat: Format[deleteHospitalClinicImageObj] = Format(deleteHospitalClinicImageReads, deleteHospitalClinicImageWrites)
}
