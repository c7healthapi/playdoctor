package models.com.hcue.doctors.Json.Request

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime
import myUtils.hcueFormats._


case class doctorFileUploadObj(fromPath:String, toPath: String)

object doctorFileUploadObj {
   val doctorFileUploadObjReads = ((__ \ "fromPath").read[String] and
    (__ \ "toPath").read[String])(doctorFileUploadObj.apply _)

  val doctorFileUploadObjWrites = ((__ \ "fromPath").write[String] and
     (__ \ "toPath").write[String])(unlift(doctorFileUploadObj.unapply))
  implicit val doctorFileUploadObjFormat: Format[doctorFileUploadObj] = Format(doctorFileUploadObjReads, doctorFileUploadObjWrites)
}

case class doctorFileDownloadObj(fromPath:String, toPath: String)

object doctorFileDownloadObj {
   val doctorFileDownloadObjReads = ((__ \ "fromPath").read[String] and
    (__ \ "toPath").read[String])(doctorFileDownloadObj.apply _)

  val doctorFileDownloadObjWrites = ((__ \ "fromPath").write[String] and
     (__ \ "toPath").write[String])(unlift(doctorFileDownloadObj.unapply))
  implicit val doctorFileDownloadObjFormat: Format[doctorFileDownloadObj] = Format(doctorFileDownloadObjReads, doctorFileDownloadObjWrites)
}