/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Time
import play.api.libs.functional.syntax._
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorEmailDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.hospitalInfoObj
import org.joda.time.DateTime
import models.com.hcue.doctors.Json.Request.AddUpdate.doctorDepartmentsObj
import models.com.hcue.doctors.Json.Request.sourceDetailsObj

case class getResponseObj(doctor: Array[DoctorDetailsObj], doctorPhone: Option[Array[DoctorPhoneDetailsObj]],
                          doctorAddress: Array[DoctorAddressDetailsObj], doctorEmail: Option[Array[DoctorEmailDetailsObj]],
                          doctorConsultation: Array[DoctorConsultationDetailsObj],
                          DoctorSetting: Option[Map[String, String]],
                          CareerImages: Option[CareerImagesObj], SpecilityDetails: Array[SpecialityDetailsObj], Searchable: Option[String], Identity: Option[String], doctorOtherDetails: Option[DoctorOtherDetailsObj],
                          PractiesDetails: Option[Array[PractiesDetails]])

object getResponseObj {
  val getResponseObjReads = (
    (__ \ "doctor").read[Array[DoctorDetailsObj]] and
    (__ \ "doctorPhone").readNullable[Array[DoctorPhoneDetailsObj]] and
    (__ \ "doctorAddress").read[Array[DoctorAddressDetailsObj]] and
    (__ \ "doctorEmail").readNullable[Array[DoctorEmailDetailsObj]] and
    (__ \ "doctorConsultation").read[Array[DoctorConsultationDetailsObj]] and
    (__ \ "DoctorSetting").readNullable[Map[String, String]] and
    (__ \ "CareerImages").readNullable[CareerImagesObj] and
    (__ \ "SpecilityDetails").read[Array[SpecialityDetailsObj]] and
    (__ \ "Searchable").readNullable[String] and
    (__ \ "Identity").readNullable[String] and
    (__ \ "doctorOtherDetails").readNullable[DoctorOtherDetailsObj] and
    (__ \ "PractiesDetails").readNullable[Array[PractiesDetails]])(getResponseObj.apply _)

  val getResponseObjWrites = (
    (__ \ "doctor").write[Array[DoctorDetailsObj]] and
    (__ \ "doctorPhone").writeNullable[Array[DoctorPhoneDetailsObj]] and
    (__ \ "doctorAddress").write[Array[DoctorAddressDetailsObj]] and
    (__ \ "doctorEmail").writeNullable[Array[DoctorEmailDetailsObj]] and
    (__ \ "doctorConsultation").write[Array[DoctorConsultationDetailsObj]] and
    (__ \ "DoctorSetting").writeNullable[Map[String, String]] and
    (__ \ "CareerImages").writeNullable[CareerImagesObj] and
    (__ \ "SpecilityDetails").write[Array[SpecialityDetailsObj]] and
    (__ \ "Searchable").writeNullable[String] and
    (__ \ "Identity").writeNullable[String] and
    (__ \ "doctorOtherDetails").writeNullable[DoctorOtherDetailsObj] and
    (__ \ "PractiesDetails").writeNullable[Array[PractiesDetails]])(unlift(getResponseObj.unapply))
  implicit val getResponseFormat: Format[getResponseObj] = Format(getResponseObjReads, getResponseObjWrites)
}
//doctorPhone: Option[Array[DoctorPhoneDetailsObj]],doctorConsultation: Option[Array[DoctorConsultationDetailsObj]],

case class PractiesDetails(practiceid: Long, practicename: Option[String], practicecode: Option[String],
                           postcode: Option[String], ccgname: Option[String], telephone: Option[String], active: Boolean)

object PractiesDetails {
  val PractiesDetailsReads = (
    (__ \ "practiceid").read[Long] and
    (__ \ "practicename").readNullable[String] and
    (__ \ "practicecode").readNullable[String] and
    (__ \ "postcode").readNullable[String] and
    (__ \ "ccgname").readNullable[String] and
    (__ \ "telephone").readNullable[String] and
    (__ \ "active").read[Boolean])(PractiesDetails.apply _)

  val PractiesDetailsWrites = (
    (__ \ "practiceid").write[Long] and
    (__ \ "practicename").writeNullable[String] and
    (__ \ "practicecode").writeNullable[String] and
    (__ \ "postcode").writeNullable[String] and
    (__ \ "ccgname").writeNullable[String] and
    (__ \ "telephone").writeNullable[String] and
    (__ \ "active").write[Boolean])(unlift(PractiesDetails.unapply))

  implicit val PractiesDetailsFormat: Format[PractiesDetails] = Format(PractiesDetailsReads, PractiesDetailsWrites)
}

case class OtherDetailsObj(PanCardID: Option[String], ActiveIND: Option[String],
                           notes: Option[String])

object OtherDetailsObj {
  val OtherDetailsObjReads = (
    (__ \ "PanCardID").readNullable[String] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "notes").readNullable[String])(OtherDetailsObj.apply _)

  val OtherDetailsObjWrites = (
    (__ \ "PanCardID").writeNullable[String] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "notes").writeNullable[String])(unlift(OtherDetailsObj.unapply))
  implicit val OtherDetailsObjilsObjFormat: Format[OtherDetailsObj] = Format(OtherDetailsObjReads, OtherDetailsObjWrites)
}

case class getSpecialityLkupObj(specialityID: String, specialitydesc: String, activeIND: String, updtusrtype: String, updtusr: Option[Long], subspeciality: Option[JsValue], dicomcode: Option[String], userNotes: Option[String], hospitalId:Option[Long])

object getSpecialityLkupObj {
  val getSpecialityLkupObjReads = ( 
    (__ \ "specialityID").read[String] and
    (__ \ "specialitydesc").read[String] and
    (__ \ "activeIND").read[String] and
    (__ \ "updtusrtype").read[String] and
    (__ \ "updtusr").readNullable[Long] and
    (__ \ "subspeciality").readNullable[JsValue] and
    (__ \ "dicomcode").readNullable[String] and
    (__ \ "userNotes").readNullable[String] and 
    (__ \ "hospitalId").readNullable[Long])(getSpecialityLkupObj.apply _)

  val getSpecialityLkupObjWrites = (
    (__ \ "specialityID").write[String] and
    (__ \ "specialitydesc").write[String] and
    (__ \ "activeIND").write[String] and
    (__ \ "updtusrtype").write[String] and
    (__ \ "updtusr").writeNullable[Long] and
    (__ \ "subspeciality").writeNullable[JsValue] and
    (__ \ "dicomcode").writeNullable[String] and
    (__ \ "userNotes").writeNullable[String] and
      (__ \ "hospitalId").writeNullable[Long])(unlift(getSpecialityLkupObj.unapply))
  implicit val getSpecialityLkupObjFormat: Format[getSpecialityLkupObj] = Format(getSpecialityLkupObjReads, getSpecialityLkupObjWrites)
}

case class SpecialityDetailsObj(specialityID: String, specialityDesc: Option[String], ActiveIND: Option[String], SubSpecialityID: Option[Map[String, String]])

object SpecialityDetailsObj {
  val SpecialityDetailsObjReads = (
    (__ \ "specialityID").read[String] and
    (__ \ "specialityDesc").readNullable[String] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "SubSpecialityID").readNullable[Map[String, String]])(SpecialityDetailsObj.apply _)

  val SpecialityDetailsObjWrites = (
    (__ \ "specialityID").write[String] and
    (__ \ "specialityDesc").writeNullable[String] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "SubSpecialityID").writeNullable[Map[String, String]])(unlift(SpecialityDetailsObj.unapply))
  implicit val SpecialityDetailsObjWritesFormat: Format[SpecialityDetailsObj] = Format(SpecialityDetailsObjReads, SpecialityDetailsObjWrites)

}

case class getMinimalResponseObj(doctor: DoctorDetailsObj, doctorAddress: Option[Array[MinimalDoctorAddressDetailsObj]])

object getMinimalResponseObj {
  val getMinimalResponseObjReads = (
    (__ \ "doctor").read[DoctorDetailsObj] and
    (__ \ "doctorAddress").readNullable[Array[MinimalDoctorAddressDetailsObj]])(getMinimalResponseObj.apply _)

  val getMinimalResponseObjWrites = (
    (__ \ "doctor").write[DoctorDetailsObj] and
    (__ \ "doctorAddress").writeNullable[Array[MinimalDoctorAddressDetailsObj]])(unlift(getMinimalResponseObj.unapply))
  implicit val getMinimalResponseFormat: Format[getMinimalResponseObj] = Format(getMinimalResponseObjReads, getMinimalResponseObjWrites)
}

case class DoctorOtherDetailsObj(FirstName: String, LastName: Option[String],
                                 faxno: Option[String], gpcode: Option[String], typevalue: Option[String], notes: Option[String])

object DoctorOtherDetailsObj {
  val DoctorOtherDetailsObjReads = (
    (__ \ "FirstName").read[String] and
    (__ \ "LastName").readNullable[String] and
    (__ \ "faxno").readNullable[String] and
    (__ \ "gpcode").readNullable[String] and
    (__ \ "typevalue").readNullable[String] and
    (__ \ "notes").readNullable[String])(DoctorOtherDetailsObj.apply _)

  val DoctorOtherDetailsObjWrites = (
    (__ \ "FirstName").write[String] and
    (__ \ "LastName").writeNullable[String] and
    (__ \ "faxno").writeNullable[String] and
    (__ \ "gpcode").writeNullable[String] and
    (__ \ "typevalue").writeNullable[String] and
    (__ \ "notes").writeNullable[String])(unlift(DoctorOtherDetailsObj.unapply))
  implicit val DoctorOtherDetailsObjFormat: Format[DoctorOtherDetailsObj] = Format(DoctorOtherDetailsObjReads, DoctorOtherDetailsObjWrites)
}

case class CareerImagesObj(Images: Option[Map[String, String]], CareerDocuments: Option[JsValue])

object CareerImagesObj {
  val CareerObjReads = (
    (__ \ "Images").readNullable[Map[String, String]] and
    (__ \ "CareerDocuments").readNullable[JsValue])(CareerImagesObj.apply _)

  val CareerObjWrites = (
    (__ \ "Images").writeNullable[Map[String, String]] and
    (__ \ "CareerDocuments").writeNullable[JsValue])(unlift(CareerImagesObj.unapply))
  implicit val CareerObjFormat: Format[CareerImagesObj] = Format(CareerObjReads, CareerObjWrites)
}

case class DoctorEducationObj(RowID: Int, EducationName: String, InstituteName: String, YearOfPassedOut: String)

object DoctorEducationObj {
  val doctorEducationObjReads = (
    (__ \ "RowID").read[Int] and
    (__ \ "EducationName").read[String] and
    (__ \ "InstituteName").read[String] and
    (__ \ "FullName").read[String])(DoctorEducationObj.apply _)

  val doctorEducationObjWrites = (
    (__ \ "RowID").write[Int] and
    (__ \ "EducationName").write[String] and
    (__ \ "InstituteName").write[String] and
    (__ \ "FullName").write[String])(unlift(DoctorEducationObj.unapply))
  implicit val doctorEducationObjFormat: Format[DoctorEducationObj] = Format(doctorEducationObjReads, doctorEducationObjWrites)
}

case class DoctorPublicationAchievementObj(RowID: Long, Name: String, Details: Option[Map[String, String]], Year: String, Indicator: String)

object DoctorPublicationAchievementObj {
  val doctorPubAchObjReads = (
    (__ \ "RowID").read[Long] and
    (__ \ "Name").read[String] and
    (__ \ "Details").readNullable[Map[String, String]] and
    (__ \ "Year").read[String] and
    (__ \ "Indicator").read[String])(DoctorPublicationAchievementObj.apply _)

  val doctorPubAchObjWrites = (
    (__ \ "RowID").write[Long] and
    (__ \ "Name").write[String] and
    (__ \ "Details").writeNullable[Map[String, String]] and
    (__ \ "Year").write[String] and
    (__ \ "Indicator").write[String])(unlift(DoctorPublicationAchievementObj.unapply))
  implicit val doctorPubAchObjFormat: Format[DoctorPublicationAchievementObj] = Format(doctorPubAchObjReads, doctorPubAchObjWrites)
}

case class DoctorAddressDetailsObj(AddressID: Option[Long], ClinicName: Option[String], ClinicSettings: Option[Map[String, String]], Address1: Option[String], Address2: Option[String],
                                   Street: Option[String], Location: Option[String], CityTown: Option[String], DistrictRegion: Option[Long],
                                   State: Option[String], PostCode: Option[String], Country: Option[String], AddressType: String, LandMark: Option[String], Active: Option[String],
                                   ExtDetails: Option[DoctorAddressExtnObj], ShowTimings: Option[String], Address4: Option[String])

object DoctorAddressDetailsObj {
  val DoctorAddressDetailsObjReads = (
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "ClinicName").readNullable[String] and
    (__ \ "ClinicSettings").readNullable[Map[String, String]] and
    (__ \ "Address1").readNullable(Reads.maxLength[String](100)) and
    (__ \ "Address2").readNullable(Reads.maxLength[String](100)) and
    (__ \ "Street").readNullable(Reads.maxLength[String](100)) and
    (__ \ "Location").readNullable[String] and
    (__ \ "CityTown").readNullable(Reads.maxLength[String](100)) and
    (__ \ "DistrictRegion").readNullable[Long] and
    (__ \ "State").readNullable[String] and
    (__ \ "PostCode").readNullable[String] and
    (__ \ "Country").readNullable(Reads.maxLength[String](10)) and
    (__ \ "AddressType").read(Reads.maxLength[String](10)) and
    (__ \ "LandMark").readNullable[String] and
    (__ \ "Active").readNullable[String] and
    (__ \ "ExtDetails").readNullable[DoctorAddressExtnObj] and
    (__ \ "ShowTimings").readNullable[String] and
    (__ \ "Address4").readNullable[String])(DoctorAddressDetailsObj.apply _) // read[String](minLength(0)) yields compiler error

  val DoctorAddressDetailsObjWrites = (
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "ClinicName").writeNullable[String] and
    (__ \ "ClinicSettings").writeNullable[Map[String, String]] and
    (__ \ "Address1").writeNullable[String] and
    (__ \ "Address2").writeNullable[String] and
    (__ \ "Street").writeNullable[String] and
    (__ \ "Location").writeNullable[String] and
    (__ \ "CityTown").writeNullable[String] and
    (__ \ "DistrictRegion").writeNullable[Long] and
    (__ \ "State").writeNullable[String] and
    (__ \ "PostCode").writeNullable[String] and
    (__ \ "Country").writeNullable[String] and
    (__ \ "AddressType").write[String] and
    (__ \ "LandMark").writeNullable[String] and
    (__ \ "Active").writeNullable[String] and
    (__ \ "ExtDetails").writeNullable[DoctorAddressExtnObj] and
    (__ \ "ShowTimings").writeNullable[String] and
    (__ \ "Address4").writeNullable[String])(unlift(DoctorAddressDetailsObj.unapply))
  implicit val DoctorAddressDetailsFormat: Format[DoctorAddressDetailsObj] = Format(DoctorAddressDetailsObjReads, DoctorAddressDetailsObjWrites)
}
case class MinimalDoctorAddressDetailsObj(AddressID: Option[Long], ClinicName: Option[String], ClinicSettings: Option[Map[String, String]], Address1: String, Address2: Option[String], Street: Option[String], Location: String, CityTown: String, DistrictRegion: Option[String],
                                          State: Option[String], PinCode: Int, Country: String, AddressType: String, LandMark: Option[String], Active: Option[String],
                                          ExtDetails: Option[DoctorAddressExtnObj], doctorConsultation: Option[Array[DoctorConsultationDetailsObj]])

object MinimalDoctorAddressDetailsObj {
  val MinimalDoctorAddressDetailsObjReads = (
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "ClinicName").readNullable[String] and
    (__ \ "ClinicSettings").readNullable[Map[String, String]] and
    (__ \ "Address1").read(Reads.maxLength[String](100)) and
    (__ \ "Address2").readNullable(Reads.maxLength[String](100)) and
    (__ \ "Street").readNullable(Reads.maxLength[String](100)) and
    (__ \ "Location").read[String] and
    (__ \ "CityTown").read(Reads.maxLength[String](100)) and
    (__ \ "DistrictRegion").readNullable[String] and
    (__ \ "State").readNullable[String] and
    (__ \ "PinCode").read[Int] and
    (__ \ "Country").read(Reads.maxLength[String](10)) and
    (__ \ "AddressType").read(Reads.maxLength[String](10)) and
    (__ \ "LandMark").readNullable[String] and
    (__ \ "Active").readNullable[String] and
    (__ \ "ExtDetails").readNullable[DoctorAddressExtnObj] and
    (__ \ "doctorConsultation").readNullable[Array[DoctorConsultationDetailsObj]])(MinimalDoctorAddressDetailsObj.apply _) // read[String](minLength(0)) yields compiler error

  val MinimalDoctorAddressDetailsObjWrites = (
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "ClinicName").writeNullable[String] and
    (__ \ "ClinicSettings").writeNullable[Map[String, String]] and
    (__ \ "Address1").write[String] and
    (__ \ "Address2").writeNullable[String] and
    (__ \ "Street").writeNullable[String] and
    (__ \ "Location").write[String] and
    (__ \ "CityTown").write[String] and
    (__ \ "DistrictRegion").writeNullable[String] and
    (__ \ "State").writeNullable[String] and
    (__ \ "PinCode").write[Int] and
    (__ \ "Country").write[String] and
    (__ \ "AddressType").write[String] and
    (__ \ "LandMark").writeNullable[String] and
    (__ \ "Active").writeNullable[String] and
    (__ \ "ExtDetails").writeNullable[DoctorAddressExtnObj] and
    (__ \ "doctorConsultation").writeNullable[Array[DoctorConsultationDetailsObj]])(unlift(MinimalDoctorAddressDetailsObj.unapply))
  implicit val DoctorAddressDetailsFormat: Format[MinimalDoctorAddressDetailsObj] = Format(MinimalDoctorAddressDetailsObjReads, MinimalDoctorAddressDetailsObjWrites)
}
case class DoctorAddressExtnObj(HospitalID: Option[Long], PrimaryIND: String,
                                Latitude: Option[String], Longitude: Option[String], AddressCommunication: Array[DoctorCommunicationObj], ClinicImages: Option[AddressImagesObj], HospitalInfo: Option[hospitalInfoObj], RoleID: Option[String],
                                GroupID: Option[String], RatePerHour: Option[Float], MinPerCase: Option[Int], AccountAccessID: Option[Map[String, String]],
                                RateAboveTwenty: Option[BigDecimal], RateBelowTwenty: Option[BigDecimal])

object DoctorAddressExtnObj {
  val DoctorAddressExtnObjReads = (
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "PrimaryIND").read[String] and
    (__ \ "Latitude").readNullable[String] and
    (__ \ "Longitude").readNullable[String] and
    (__ \ "AddressCommunication").read[Array[DoctorCommunicationObj]] and
    (__ \ "ClinicImages").readNullable[AddressImagesObj] and
    (__ \ "HospitalInfo").readNullable[hospitalInfoObj] and
    (__ \ "RoleID").readNullable[String] and
    (__ \ "GroupID").readNullable[String] and
    (__ \ "RatePerHour").readNullable[Float] and
    (__ \ "MinPerCase").readNullable[Int] and
    (__ \ "AccountAccessID").readNullable[Map[String, String]] and
    (__ \ "RateAboveTwenty").readNullable[BigDecimal] and
    (__ \ "RateBelowTwenty").readNullable[BigDecimal])(DoctorAddressExtnObj.apply _)

  val DoctorAddressExtnObjWrites = (
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "PrimaryIND").write[String] and
    (__ \ "Latitude").writeNullable[String] and
    (__ \ "Longitude").writeNullable[String] and
    (__ \ "AddressCommunication").write[Array[DoctorCommunicationObj]] and
    (__ \ "ClinicImages").writeNullable[AddressImagesObj] and
    (__ \ "HospitalInfo").writeNullable[hospitalInfoObj] and
    (__ \ "RoleID").writeNullable[String] and
    (__ \ "GroupID").writeNullable[String] and
    (__ \ "RatePerHour").writeNullable[Float] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "AccountAccessID").writeNullable[Map[String, String]] and
    (__ \ "RateAboveTwenty").writeNullable[BigDecimal] and
    (__ \ "RateBelowTwenty").writeNullable[BigDecimal])(unlift(DoctorAddressExtnObj.unapply))
  implicit val DoctorAddressExtnObjFormat: Format[DoctorAddressExtnObj] = Format(DoctorAddressExtnObjReads, DoctorAddressExtnObjWrites)
}

case class AddressImagesObj(Images: Map[String, String], ClinicDocuments: Option[JsValue])

object AddressImagesObj {
  val AddressObjReads = (
    (__ \ "Images").read[Map[String, String]] and
    (__ \ "ClinicDocuments").readNullable[JsValue])(AddressImagesObj.apply _)

  val AddressObjWrites = (
    (__ \ "Images").write[Map[String, String]] and
    (__ \ "ClinicDocuments").writeNullable[JsValue])(unlift(AddressImagesObj.unapply))
  implicit val CareerObjFormat: Format[AddressImagesObj] = Format(AddressObjReads, AddressObjWrites)
}

case class AdditionalInfoObj(consultinfo: Option[Array[ConsultInfObj]], Services: Option[Map[String, String]])

object AdditionalInfoObj {
  val AdditionalInfoObjReads = (
    (__ \ "consultinfo").readNullable[Array[ConsultInfObj]] and
    (__ \ "Services").readNullable[Map[String, String]])(AdditionalInfoObj.apply _)

  val AdditionalInfoObjWrites = (
    (__ \ "consultinfo").writeNullable[Array[ConsultInfObj]] and
    (__ \ "Services").writeNullable[Map[String, String]])(unlift(AdditionalInfoObj.unapply))
  implicit val CareerObjFormat: Format[AdditionalInfoObj] = Format(AdditionalInfoObjReads, AdditionalInfoObjWrites)
}

case class ConsultInfObj(DayCD: Option[String], StartTime: Option[String], EndTime: Option[String])

object ConsultInfObj {
  val ConsultInfObjReads = (
    (__ \ "DayCD").readNullable[String] and
    (__ \ "StartTime").readNullable[String] and
    (__ \ "EndTime").readNullable[String])(ConsultInfObj.apply _)

  val ConsultInfObjWrites = (
    (__ \ "DayCD").writeNullable[String] and
    (__ \ "StartTime").writeNullable[String] and
    (__ \ "EndTime").writeNullable[String])(unlift(ConsultInfObj.unapply))
  implicit val CareerObjFormat: Format[ConsultInfObj] = Format(ConsultInfObjReads, ConsultInfObjWrites)
}

case class DoctorSpecialityDetails(SerialNumber: String, SepcialityCode: String, SepcialityDesc: String, CustomSpecialityDesc: Option[String], SpecialityOESDesc: String)

object DoctorSpecialityDetails {
  val DoctorSpecialityDetailsReads = (
    (__ \ "SerialNumber").read[String] and
    (__ \ "SepcialityCode").read[String] and
    (__ \ "SepcialityDesc").read[String] and
    (__ \ "CustomSpecialityDesc").readNullable[String] and
    (__ \ "SpecialityOESDesc").read[String])(DoctorSpecialityDetails.apply _)

  val DoctorSpecialityDetailsWrites = (
    (__ \ "SerialNumber").write[String] and
    (__ \ "SepcialityCode").write[String] and
    (__ \ "SepcialityDesc").write[String] and
    (__ \ "CustomSpecialityDesc").writeNullable[String] and
    (__ \ "SpecialityOESDesc").write[String])(unlift(DoctorSpecialityDetails.unapply))
  implicit val DoctorSpecialityDetailsFormat: Format[DoctorSpecialityDetails] = Format(DoctorSpecialityDetailsReads, DoctorSpecialityDetailsWrites)
}

case class DoctorDetailsObj(FirstName: String, LastName: Option[String],FullName: String, DoctorID: Long, Gender: Option[String], DOB: Option[java.sql.Date],
                            SpecialityCD: Option[Map[String, String]], MemberID: Option[Map[String, String]], Qualification: Option[Map[String, String]], Exp: Option[Int],
                            DoctorLoginID: Option[String], Password: Option[String], About: Option[Map[String, String]], SpecialityDetails: Option[Array[DoctorSpecialityDetails]],
                            Awards:     Option[Map[String, String]],
                            
                            Prospect: Option[String], ReferredBy: Option[Long], TermsAccepted: Option[String],
                            ProfileImage: Option[String], Title: Option[String], RegistrationNo: Option[String])

object DoctorDetailsObj {
  val DoctorDetailsObjReads = (
       (__ \ "FirstName").read[String] and
    (__ \ "LastName").readNullable[String] and
    (__ \ "FullName").read(Reads.maxLength[String](150)) and
    (__ \ "DoctorID").read[Long] and
    (__ \ "Gender").readNullable(Reads.maxLength[String](1)) and
    (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "SpecialityCD").readNullable[Map[String, String]] and
    (__ \ "MemberID").readNullable[Map[String, String]] and
    (__ \ "Qualification").readNullable[Map[String, String]] and
    (__ \ "Exp").readNullable[Int] and (__ \ "DoctorLoginID").readNullable(Reads.maxLength[String](100)) and
    (__ \ "Password").readNullable[String] and
    (__ \ "About").readNullable[Map[String, String]] and
    (__ \ "SpecialityDetails").readNullable[Array[DoctorSpecialityDetails]] and
    (__ \ "Awards").readNullable[Map[String, String]] and
    (__ \ "Prospect").readNullable[String] and
    (__ \ "ReferredBy").readNullable[Long] and
    (__ \ "TermsAccepted").readNullable(Reads.maxLength[String](1)) and
    (__ \ "ProfileImage").readNullable[String] and
    (__ \ "Title").readNullable[String] and
    (__ \ "RegistrationNo").readNullable[String])(DoctorDetailsObj.apply _)

  val DoctorDetailsObjWrites = (
      (__ \ "FirstName").write[String] and
    (__ \ "LastName").writeNullable[String] and
    (__ \ "FullName").write[String] and
    (__ \ "DoctorID").write[Long] and (__ \ "Gender").writeNullable[String] and (__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "SpecialityCD").writeNullable[Map[String, String]] and (__ \ "MemberID").writeNullable[Map[String, String]] and
    (__ \ "Qualification").writeNullable[Map[String, String]] and (__ \ "Exp").writeNullable[Int] and (__ \ "DoctorLoginID").writeNullable[String] and
    (__ \ "Password").writeNullable[String] and
    (__ \ "About").writeNullable[Map[String, String]] and
    (__ \ "SpecialityDetails").writeNullable[Array[DoctorSpecialityDetails]] and
    (__ \ "Awards").writeNullable[Map[String, String]] and
    (__ \ "Prospect").writeNullable[String] and
    (__ \ "ReferredBy").writeNullable[Long] and
    (__ \ "TermsAccepted").writeNullable[String] and
    (__ \ "ProfileImage").writeNullable[String] and
    (__ \ "Title").writeNullable[String] and
    (__ \ "RegistrationNo").writeNullable[String])(unlift(DoctorDetailsObj.unapply))
  implicit val DoctorDetailsFormat: Format[DoctorDetailsObj] = Format(DoctorDetailsObjReads, DoctorDetailsObjWrites)
}

case class HospitalAppointmentResponseObj(SeqNo: Int, HospitalID: Option[Long], HospitalName: Option[String], Location: String, AppointmentID: Long, AddressConsultID: Long, DayCD: String, ConsultationDt: java.sql.Date,
                                          StartTime: String, EndTime: String, AppointmentStatus: String, SpecialityCD: Option[String], PatientDetails: buildResponseObj)

object HospitalAppointmentResponseObj {
  val HospitalAppointmentObjReads = (
    (__ \ "SeqNo").read[Int] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "HospitalName").readNullable[String] and
    (__ \ "Location").read[String] and
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ConsultationDt").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "SpecialityCD").readNullable[String] and
    (__ \ "PatientDetails").read[buildResponseObj])(HospitalAppointmentResponseObj.apply _)

  val HospitalAppointmentObjWrites = (
    (__ \ "SeqNo").write[Int] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "HospitalName").writeNullable[String] and
    (__ \ "Location").write[String] and
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ConsultationDt").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "SpecialityCD").writeNullable[String] and
    (__ \ "PatientDetails").write[buildResponseObj])(unlift(HospitalAppointmentResponseObj.unapply))
  implicit val DoctorAppointment: Format[HospitalAppointmentResponseObj] = Format(HospitalAppointmentObjReads, HospitalAppointmentObjWrites)
}

case class DoctorAppointmentResponseObj(SeqNo: Int, AppointmentID: Long, AddressConsultID: Long, DayCD: String, ConsultationDt: java.sql.Date, StartTime: String, EndTime: String,
                                        PatientID: Option[Long], VisitUserTypeID: String, DoctorID: Option[Long], DoctorFullName: String, FirstTimeVisit: String, DoctorVisitRsnID: String,
                                        OtherVisitRsn: Option[String], AppointmentStatus: String, TokenNumber: String,
                                        VisitRsnID: Option[Long], PreliminaryNotes: Option[String], ReferralSourceDtls: Option[sourceDetailsObj], PatientDetails: buildResponseObj, Status: statusResponseObj)

object DoctorAppointmentResponseObj {
  val DoctorAppointmentObjReads = (
    (__ \ "SeqNo").read[Int] and
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ConsultationDt").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "PatientID").readNullable[Long] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "DoctorFullName").read[String] and
    (__ \ "FirstTimeVisit").read[String] and
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "OtherVisitRsn").readNullable[String] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "TokenNumber").read[String] and
    (__ \ "VisitRsnID").readNullable[Long] and
    (__ \ "PreliminaryNotes").readNullable[String] and
    (__ \ "ReferralSourceDtls").readNullable[sourceDetailsObj] and
    (__ \ "PatientDetails").read[buildResponseObj] and
    (__ \ "Status").read[statusResponseObj])(DoctorAppointmentResponseObj.apply _)

  val DoctorAppointmentObjWrites = (
    (__ \ "SeqNo").write[Int] and
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ConsultationDt").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "PatientID").writeNullable[Long] and
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "DoctorFullName").write[String] and
    (__ \ "FirstTimeVisit").write[String] and
    (__ \ "DoctorVisitRsnID").write[String] and
    (__ \ "OtherVisitRsn").writeNullable[String] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "TokenNumber").write[String] and
    (__ \ "VisitRsnID").writeNullable[Long] and
    (__ \ "PreliminaryNotes").writeNullable[String] and
    (__ \ "ReferralSourceDtls").writeNullable[sourceDetailsObj] and
    (__ \ "PatientDetails").write[buildResponseObj] and
    (__ \ "Status").write[statusResponseObj])(unlift(DoctorAppointmentResponseObj.unapply))
  implicit val DoctorAppointment: Format[DoctorAppointmentResponseObj] = Format(DoctorAppointmentObjReads, DoctorAppointmentObjWrites)
}

case class statusResponseObj(isBuild: Option[String], isVitalsCaptured: Option[String], createdTime: Option[DateTime], checkedInTime: Option[String])
object statusResponseObj {
  val statusResponseObjReads = (
    (__ \ "isBill").readNullable[String] and
    (__ \ "isVitalsCaptured").readNullable[String] and
    (__ \ "createdTime").readNullable[DateTime] and
    (__ \ "checkedInTime").readNullable[String])(statusResponseObj.apply _)

  val statusResponseObjWrites = (
    (__ \ "isBuild").writeNullable[String] and
    (__ \ "isVitalsCaptured").writeNullable[String] and
    (__ \ "createdTime").writeNullable[DateTime] and
    (__ \ "checkedInTime").writeNullable[String])(unlift(statusResponseObj.unapply))

  implicit val statusResponseObjFormat: Format[statusResponseObj] = Format(statusResponseObjReads, statusResponseObjWrites)
}

case class ForgotPasswordRes(Message: String, forgotPasswordResponse: forgotPasswordResponse)

object ForgotPasswordRes {
  val ForgotPasswordResReads = (
    (__ \ "Message").read[String] and
    (__ \ "forgotPasswordResponse").read[forgotPasswordResponse])(ForgotPasswordRes.apply _)

  val ForgotPasswordResWrites = (
    (__ \ "Message").write[String] and
    (__ \ "forgotPasswordResponse").write[forgotPasswordResponse])(unlift(ForgotPasswordRes.unapply))

  implicit val ForgotPasswordResFormat: Format[ForgotPasswordRes] = Format(ForgotPasswordResReads, ForgotPasswordResWrites)
}

case class forgotPasswordResponse(DoctorID: Long, DoctorName: Option[String], DoctorLoginID: Option[String], DoctorImg: Option[String], MobileNumber: Option[String], EmailID: Option[String])

object forgotPasswordResponse {
  val forgotPasswordResponseReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "DoctorName").readNullable[String] and
    (__ \ "DoctorLoginID").readNullable[String] and
    (__ \ "DoctorImg").readNullable[String] and
    (__ \ "MobileNumber").readNullable[String] and
    (__ \ "EmailID").readNullable[String])(forgotPasswordResponse.apply _)

  val forgotPasswordResponseWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "DoctorName").writeNullable[String] and
    (__ \ "DoctorLoginID").writeNullable[String] and
    (__ \ "DoctorImg").writeNullable[String] and
    (__ \ "MobileNumber").writeNullable[String] and
    (__ \ "EmailID").writeNullable[String])(unlift(forgotPasswordResponse.unapply))

  implicit val forgotPasswordResponseFormat: Format[forgotPasswordResponse] = Format(forgotPasswordResponseReads, forgotPasswordResponseWrites)
}

case class forgotPasswordObj(FullName: String, Gender: Option[String], DOB: Option[java.sql.Date],
                             SpecialityCD: Option[Map[String, String]], MemberID: Option[Map[String, String]], DoctorLoginID: String, DoctorPassword: String,
                             DoctorPhone: Array[DoctorPhoneDetailsObj], DoctorEmail: Array[DoctorEmailDetailsObj])

object forgotPasswordObj {
  val forgotPasswordObjReads = (
    (__ \ "FullName").read(Reads.maxLength[String](150)) and
    (__ \ "Gender").readNullable(Reads.maxLength[String](1)) and (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "SpecialityCD").readNullable[Map[String, String]] and (__ \ "MemberID").readNullable[Map[String, String]] and (__ \ "DoctorLoginID").read(Reads.maxLength[String](100)) and
    (__ \ "DoctorPassword").read(Reads.maxLength[String](20)) and
    (__ \ "DoctorPhone").read[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").read[Array[DoctorEmailDetailsObj]])(forgotPasswordObj.apply _)

  val forgotPasswordObjWrites = (
    (__ \ "FullName").write[String] and
    (__ \ "Gender").writeNullable[String] and (__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "SpecialityCD").writeNullable[Map[String, String]] and (__ \ "MemberID").writeNullable[Map[String, String]] and (__ \ "DoctorLoginID").write[String] and
    (__ \ "DoctorPassword").write[String] and (__ \ "DoctorPhone").write[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").write[Array[DoctorEmailDetailsObj]])(unlift(forgotPasswordObj.unapply))
  implicit val addDoctorFormat: Format[forgotPasswordObj] = Format(forgotPasswordObjReads, forgotPasswordObjWrites)
}
case class DoctorConsultationInfo(DoctorAddress: DoctorAddressDetailsObj, ConsultationDetails: Array[DoctorConsultationDetailsObj])

object DoctorConsultationInfo {
  val DoctorConsultationInfoReads = (
    (__ \ "DoctorAddress").read[DoctorAddressDetailsObj] and
    (__ \ "ConsultationDetails").read[Array[DoctorConsultationDetailsObj]])(DoctorConsultationInfo.apply _)

  val DoctorConsultationInfoWrites = (
    (__ \ "DoctorAddress").write[DoctorAddressDetailsObj] and
    (__ \ "ConsultationDetails").write[Array[DoctorConsultationDetailsObj]])(unlift(DoctorConsultationInfo.unapply))
  implicit val DoctorConsultationInfoFormat: Format[DoctorConsultationInfo] = Format(DoctorConsultationInfoReads, DoctorConsultationInfoWrites)

}

case class DoctorConsultationDetailsObj(AddressConsultID: Long, AddressID: Long, DoctorID: Long, DayCD: String, StartTime: String, EndTime: String, MinPerCase: Option[Int],
                                        Fees: Option[Int], Active: Option[String])

object DoctorConsultationDetailsObj {
  val doctorConsultationDetailsObjReads = (
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "AddressID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "MinPerCase").readNullable[Int] and
    (__ \ "Fees").readNullable[Int] and
    (__ \ "Active").readNullable[String])(DoctorConsultationDetailsObj.apply _)

  val doctorConsultationDetailsObjWrites = (
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "AddressID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "Fees").writeNullable[Int] and
    (__ \ "Active").writeNullable[String])(unlift(DoctorConsultationDetailsObj.unapply))
  implicit val doctorConsultationDetailsObjFormat: Format[DoctorConsultationDetailsObj] = Format(doctorConsultationDetailsObjReads, doctorConsultationDetailsObjWrites)

}
/*
case class getAppointmentResponseObj(AppointMent: getAppointMentDetailsObj, Phone: getPhoneResponseObj)

object getAppointmentResponseObj {
  val getAppointmentResponseObjReads = (
    (__ \ "AppointMent").read[getAppointMentDetailsObj] and
    (__ \ "Phone").read[getPhoneResponseObj])(getAppointmentResponseObj.apply _)

  val getAppointmentResponseObjWrites = (
    (__ \ "AppointMent").write[getAppointMentDetailsObj] and
    (__ \ "Phone").write[getPhoneResponseObj])(unlift(getAppointmentResponseObj.unapply))
  implicit val getAppointmentResponseFormat: Format[getAppointmentResponseObj] = Format(getAppointmentResponseObjReads, getAppointmentResponseObjWrites)
}

case class getAppointMentDetailsObj(AppointmentID: Long, AddressConsultID: Long, DayCD: String, ConsultationDt: java.sql.Date,
    StartTime: String, EndTime: String, PatientID: Long, VisitUserTypeID: String, DoctorID: Long, FirstTimeVisit: String, DoctorVisitRsnID: String,
                                    OtherVisitRsn: Option[String], AppointmentStatus: String, TokenNumber: String)

object getAppointMentDetailsObj {
  val getAppointMentDetailsObjReads = (
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DayCD").read[String] and
    (__ \ "ConsultationDt").read[java.sql.Date] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "PatientID").read[Long] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "FirstTimeVisit").read[String] and
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "OtherVisitRsn").readNullable[String] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "TokenNumber").read[String])(getAppointMentDetailsObj.apply _)

  val getAppointMentDetailsObjWrites = (
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DayCD").write[String] and
    (__ \ "ConsultationDt").write[java.sql.Date] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "PatientID").write[Long] and
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "FirstTimeVisit").write[String] and
    (__ \ "DoctorVisitRsnID").write[String] and
    (__ \ "OtherVisitRsn").writeNullable[String] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "TokenNumber").write[String])(unlift(getAppointMentDetailsObj.unapply))
  implicit val getAppointmentResponseFormat: Format[getAppointMentDetailsObj] = Format(getAppointMentDetailsObjReads, getAppointMentDetailsObjWrites )
}

case class getPhoneResponseObj(PatientID: Long, PhCntryCD: Int, PhStateCD: Int, PhAreaCD: Int, PhNumber: Long, PhType: String, PrimaryIND: String)

object getPhoneResponseObj {
  val getPhoneResponseObjReads = (
    (__ \ "PatientID").read[Long] and
    (__ \ "PhCntryCD").read[Int] and
    (__ \ "PhStateCD").read[Int] and
    (__ \ "PhAreaCD").read[Int] and
    (__ \ "PhNumber").read[Long] and
    (__ \ "PhType").read[String] and
    (__ \ "PrimaryIND").read[String])(getPhoneResponseObj.apply _)

  val getPhoneResponseObjWrites = (
    (__ \ "PatientID").write[Long] and
    (__ \ "PhCntryCD").write[Int] and
    (__ \ "PhStateCD").write[Int] and
    (__ \ "PhAreaCD").write[Int] and
    (__ \ "PhNumber").write[Long] and
    (__ \ "PhType").write[String] and
    (__ \ "PrimaryIND").write[String])(unlift(getPhoneResponseObj.unapply))
  implicit val getPhoneResponseFormat: Format[getPhoneResponseObj] = Format(getPhoneResponseObjReads, getPhoneResponseObjWrites)
}
  */

case class specialityDetails(code: String, description: String)

object specialityDetails {
  val specialityDetailsReads = ((__ \ "code").read[String] and
    (__ \ "description").read[String])(specialityDetails.apply _)

  val specialityDetailsWrites = ((__ \ "code").write[String] and
    (__ \ "description").write[String])(unlift(specialityDetails.unapply))
  implicit val specialityDetailsFormat: Format[specialityDetails] = Format(specialityDetailsReads, specialityDetailsWrites)
}

case class furtherCareDoctorObj(DoctorID: Long, FirstName: String, LastName: Option[String], FullName: String, Gender: Option[String], DOB: Option[java.sql.Date],
                                SpecialityCD: Option[Map[String, String]], specialityInfo: Option[Array[specialityDetails]], Prospect: Option[String], ProfileImage: Option[String], ActiveIND: Option[String], AddedOn: Option[DateTime], AddedBy: Option[String], AlreadyAdded: Option[String], Qualification: Option[Map[String, String]], DoctorAddress: Option[Array[DoctorAddressDetailsObj]], DoctorPhone: Array[DoctorPhoneDetailsObj], DoctorEmail: Array[DoctorEmailDetailsObj])

object furtherCareDoctorObj {
  val furtherCareDoctorObjReads = ((__ \ "DoctorID").read[Long] and
    (__ \ "FirstName").read(Reads.maxLength[String](50)) and (__ \ "LastName").readNullable(Reads.maxLength[String](50)) and
    (__ \ "FullName").read(Reads.maxLength[String](150)) and (__ \ "Gender").readNullable(Reads.maxLength[String](1)) and
    (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "SpecialityCD").readNullable[Map[String, String]] and
    (__ \ "specialityInfo").readNullable[Array[specialityDetails]] and
    (__ \ "Prospect").readNullable[String] and
    (__ \ "ProfileImage").readNullable[String] and
    (__ \ "ActiveIND").readNullable[String] and
    (__ \ "AddedOn").readNullable[DateTime] and
    (__ \ "AddedBy").readNullable[String] and
    (__ \ "AlreadyAdded").readNullable[String] and
    (__ \ "Qualification").readNullable[Map[String, String]] and
    (__ \ "DoctorAddress").readNullable[Array[DoctorAddressDetailsObj]] and
    (__ \ "DoctorPhone").read[Array[DoctorPhoneDetailsObj]] and
    (__ \ "DoctorEmail").read[Array[DoctorEmailDetailsObj]])(furtherCareDoctorObj.apply _)

  val furtherCareDoctorObjWrites = ((__ \ "DoctorID").write[Long] and
    (__ \ "FirstName").write[String] and (__ \ "LastName").writeNullable[String] and (__ \ "FullName").write[String] and
    (__ \ "Gender").writeNullable[String] and (__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "SpecialityCD").writeNullable[Map[String, String]] and
    (__ \ "specialityInfo").writeNullable[Array[specialityDetails]] and
    (__ \ "Prospect").writeNullable[String] and
    (__ \ "ProfileImage").writeNullable[String] and
    (__ \ "ActiveIND").writeNullable[String] and
    (__ \ "AddedOn").writeNullable[DateTime] and
    (__ \ "AddedBy").writeNullable[String] and
    (__ \ "AlreadyAdded").writeNullable[String] and
    (__ \ "Qualification").writeNullable[Map[String, String]] and
    (__ \ "DoctorAddress").writeNullable[Array[DoctorAddressDetailsObj]] and
    (__ \ "DoctorPhone").write[Array[DoctorPhoneDetailsObj]] and (__ \ "DoctorEmail").write[Array[DoctorEmailDetailsObj]])(unlift(furtherCareDoctorObj.unapply))
  implicit val addDoctorFormat: Format[furtherCareDoctorObj] = Format(furtherCareDoctorObjReads, furtherCareDoctorObjWrites)
}

case class SearchDoctorDetailsObj(ClinicName: String, Location: String, Latitude: String, Longitude: String, FullName: String, DoctorID: Long, Gender: String, Exp: Int,
                                  Fees: Int, Distance: String, Address: String, SpecialityID: Array[DoctorSpecialityDetails], Qualification: String, ProfileImage: Option[String], AddressID: Long,
                                  PhoneDetails: Option[PhoneDetailObj], Prospect: String, Medico: String, ClinicImages: Option[AddressImagesObj],
                                  AdditionWebsiteInfoObj: AdditionWebsiteInfoObj, ConsultationInfo: Option[AdditionalInfoObj] /* ,DoctorURL : String,RegistrationNumber :String*/ )

object SearchDoctorDetailsObj {
  val SearchDoctorDetailsObjReads = (
    (__ \ "ClinicName").read[String] and
    (__ \ "Location").read[String] and
    (__ \ "Latitude").read[String] and
    (__ \ "Longitude").read[String] and
    (__ \ "FullName").read[String] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "Gender").read[String] and
    (__ \ "Exp").read[Int] and
    (__ \ "Fees").read[Int] and
    (__ \ "Distance").read[String] and
    (__ \ "Address").read[String] and
    (__ \ "SpecialityCodes").read[Array[DoctorSpecialityDetails]] and
    (__ \ "Qualification").read[String] and
    (__ \ "ProfileImage").readNullable[String] and
    (__ \ "AddressID").read[Long] and
    (__ \ "PhoneDetails").readNullable[PhoneDetailObj] and
    (__ \ "Prospect").read[String] and
    (__ \ "Medico").read[String] and
    (__ \ "ClinicImages").readNullable[AddressImagesObj] and
    (__ \ "AdditionWebsiteInfoObj").read[AdditionWebsiteInfoObj] and
    (__ \ "ConsultInfo").readNullable[AdditionalInfoObj]
  /*and
    (__ \ "DoctorURL").read[String] and
    (__ \ "RegistrationNumber").read[String]*/ )(SearchDoctorDetailsObj.apply _)

  val SearchDoctorDetailsObjWrites = (
    (__ \ "ClinicName").write[String] and
    (__ \ "Location").write[String] and
    (__ \ "Latitude").write[String] and
    (__ \ "Longitude").write[String] and
    (__ \ "FullName").write[String] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "Gender").write[String] and
    (__ \ "Exp").write[Int] and
    (__ \ "Fees").write[Int] and
    (__ \ "Distance").write[String] and
    (__ \ "Address").write[String] and
    (__ \ "SpecialityCodes").write[Array[DoctorSpecialityDetails]] and
    (__ \ "Qualification").write[String] and
    (__ \ "ProfileImage").writeNullable[String] and
    (__ \ "AddressID").write[Long] and
    (__ \ "PhoneDetails").writeNullable[PhoneDetailObj] and
    (__ \ "Prospect").write[String] and
    (__ \ "Medico").write[String] and
    (__ \ "ClinicImages").writeNullable[AddressImagesObj] and
    (__ \ "AdditionWebsiteInfoObj").write[AdditionWebsiteInfoObj] and
    (__ \ "AdditionalInfo").writeNullable[AdditionalInfoObj]
  /*and
   (__ \ "DoctorURL").write[String] and
   (__ \ "RegistrationNumber").write[String]*/ )(unlift(SearchDoctorDetailsObj.unapply))
  implicit val DoctorDetailsObjFormat: Format[SearchDoctorDetailsObj] = Format(SearchDoctorDetailsObjReads, SearchDoctorDetailsObjWrites)
}

case class BranchInformationObj(HospitalInfo: hospitalPartnerObj, DoctorDetails: Array[DoctorDetailObj], DoctorCount: Int, totalAppointment: Option[Long])

object BranchInformationObj {
  val BranchInformationObjReads = (
    (__ \ "HospitalInfo").read[hospitalPartnerObj] and
    (__ \ "DoctorDetails").read[Array[DoctorDetailObj]] and
    (__ \ "DoctorCount").read[Int] and
    (__ \ "totalAppointment").readNullable[Long])(BranchInformationObj.apply _)

  val BranchInformationObjWrites = (
    (__ \ "HospitalInfo").write[hospitalPartnerObj] and
    (__ \ "DoctorDetails").write[Array[DoctorDetailObj]] and
    (__ \ "DoctorCount").write[Int] and
    (__ \ "totalAppointment").writeNullable[Long])(unlift(BranchInformationObj.unapply))
  implicit val BranchInformationObjFormat: Format[BranchInformationObj] = Format(BranchInformationObjReads, BranchInformationObjWrites)
}

case class DoctorCommunicationObj(PhCntryCD: String, PhStateCD: String, PhAreaCD: String, PhNumber: String, PhType: String, RowID: Int, PrimaryIND: String, PublicDisplay: String)

object DoctorCommunicationObj {
  val DoctorCommunicationObjReads = (
    (__ \ "PhCntryCD").read[String] and
    (__ \ "PhStateCD").read[String] and
    (__ \ "PhAreaCD").read[String] and
    (__ \ "PhNumber").read[String] and
    (__ \ "PhType").read[String] and
    (__ \ "RowID").read[Int] and
    (__ \ "PrimaryIND").read[String] and
    (__ \ "PublicDisplay").read[String])(DoctorCommunicationObj.apply _) // read[String](minLength(0)) yields compiler error

  val DoctorCommunicationObjWrites = (
    (__ \ "PhCntryCD").write[String] and
    (__ \ "PhStateCD").write[String] and
    (__ \ "PhAreaCD").write[String] and
    (__ \ "PhNumber").write[String] and
    (__ \ "PhType").write[String] and
    (__ \ "RowID").write[Int] and
    (__ \ "PrimaryIND").write[String] and
    (__ \ "PublicDisplay").write[String])(unlift(DoctorCommunicationObj.unapply))
  implicit val DoctorCommunicationFormat: Format[DoctorCommunicationObj] = Format(DoctorCommunicationObjReads, DoctorCommunicationObjWrites)
}

case class PhoneDetailObj(PhoneCountryCD: String, PhoneNumber: String, PublicDisplay: String)

object PhoneDetailObj {
  val PhoneDetailObjReads = (
    (__ \ "PhoneCountryCD").read[String] and
    (__ \ "PhoneNumber").read[String] and
    (__ \ "PublicDisplay").read[String])(PhoneDetailObj.apply _)

  val PhoneDetailObjWrites = (
    (__ \ "PhoneCountryCD").write[String] and
    (__ \ "PhoneNumber").write[String] and
    (__ \ "PublicDisplay").write[String])(unlift(PhoneDetailObj.unapply))
  implicit val BranchInformationObjFormat: Format[PhoneDetailObj] = Format(PhoneDetailObjReads, PhoneDetailObjWrites)
}

case class AdditionWebsiteInfoObj(DoctorURL: Option[String], RegistrationNumber: Option[String], ShowTimings: Option[String])

object AdditionWebsiteInfoObj {
  val AdditionWebsiteInfoObjReads = (
    (__ \ "DoctorURL").readNullable[String] and
    (__ \ "RegistrationNumber").readNullable[String] and
    (__ \ "ShowTimings").readNullable[String])(AdditionWebsiteInfoObj.apply _)

  val AdditionWebsiteInfoObjWrites = (
    (__ \ "DoctorURL").writeNullable[String] and
    (__ \ "RegistrationNumber").writeNullable[String] and
    (__ \ "ShowTimings").writeNullable[String])(unlift(AdditionWebsiteInfoObj.unapply))
  implicit val BranchInformationObjFormat: Format[AdditionWebsiteInfoObj] = Format(AdditionWebsiteInfoObjReads, AdditionWebsiteInfoObjWrites)
}