/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date

import play.api.libs.functional.syntax._

case class requestMsg(request: requestMsgObj)
case class requestMsgObj(application: String, auth: String, notifications: Array[notificationMsgObj])

case class notificationMsgObj(android_header:String,android_sound:String,send_date: String, content: String,devices:Array[String], android_custom_icon: Option[String])

case class deleteRequestMsg(request: deleteRequestMsgObj)
case class deleteRequestMsgObj(auth: String, message: String)