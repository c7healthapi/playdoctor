/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response.AddressUpdateSchdular

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class buildAddressResponseObj(doctorID:Long,addressID: Long,address2:Option[String],locality: Option[String],
    sublocality_level_1: Option[String],administrative_area_level_1 : Option[String],country : Option[String],postal_Code : Option[String],
    latitude: Option[String],longitude: Option[String],status:String)

object buildAddressResponseObj {
  val buildAddressResponseObjReads = (
      (__ \ "doctorID").read[Long] and
      (__ \ "addressID").read[Long] and
      (__ \ "address2").readNullable[String] and
      (__ \ "locality").readNullable[String] and
      (__ \ "sublocality_level_1").readNullable[String] and
      (__ \ "administrative_area_level_1").readNullable[String] and
      (__ \ "country").readNullable[String] and
      (__ \ "postal_Code").readNullable[String] and
      (__ \ "latitude").readNullable[String] and (__ \ "longitude").readNullable[String] and
      (__ \ "status").read[String])(buildAddressResponseObj.apply _)

  val buildErrorResponseWrites = (
      (__ \ "doctorID").write[Long] and
      (__ \ "addressID").write[Long] and
      (__ \ "address2").writeNullable[String] and
      (__ \ "locality").writeNullable[String] and
      (__ \ "sublocality_level_1").writeNullable[String] and
      (__ \ "administrative_area_level_1").writeNullable[String] and
      (__ \ "country").writeNullable[String] and
      (__ \ "postal_Code").writeNullable[String] and
      (__ \ "latitude").writeNullable[String] and (__ \ "longitude").writeNullable[String] and
      (__ \ "status").write[String])(unlift(buildAddressResponseObj.unapply))
    
  implicit val buildErrorResponseFormat: Format[buildAddressResponseObj] = Format(buildAddressResponseObjReads,buildErrorResponseWrites)
}

