/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response.Doctor.Consultation

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.functional.syntax.unlift
import play.api.libs.json.Format
import play.api.libs.json.__
import models.com.hcue.doctors.Json.Response.currentAgeObj
import models.com.hcue.doctors.Json.Request.patientAddressDetailsObj
import models.com.hcue.doctors.Json.Request.patientEmailDetailsObj
import models.com.hcue.doctors.Json.Request.sourceDetailsObj

case class addQuickAppointmentReq(PatientID: Long, AddressConsultID: Long, DoctorID: Long, FilterByDate: java.sql.Date, USRId: Int, USRType: String)

object addQuickAppointmentReq {
  val addQuickAppointmentReqReads = (
    (__ \ "PatientID").read[Long] and
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "FilterByDate").read[java.sql.Date] and
    (__ \ "USRId").read[Int] and
    (__ \ "USRType").read[String])(addQuickAppointmentReq.apply _)

  val addQuickAppointmentReqWrites = (
    (__ \ "PatientID").write[Long] and
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "FilterByDate").write[java.sql.Date] and
    (__ \ "USRId").write[Int] and
    (__ \ "USRType").write[String])(unlift(addQuickAppointmentReq.unapply))
  implicit val getResponseFormat: Format[addQuickAppointmentReq] = Format(addQuickAppointmentReqReads, addQuickAppointmentReqWrites)
}

case class getDoctorsLstObj(doctorid:Long,doctorname:String,gpcode:Option[String],phonenumber:Option[String],active:Option[Boolean])


object getDoctorsLstObj {
 val getDoctorsLstObjReads = (
   (__ \ "doctorid").read[Long] and
   (__ \ "doctorname").read[String] and
   (__ \ "gpcode").readNullable[String] and
   (__ \ "phonenumber").readNullable[String] and
   (__ \ "active").readNullable[Boolean])(getDoctorsLstObj.apply _)

 val getDoctorsLstObjWrites = (
   (__ \ "doctorid").write[Long] and
   (__ \ "doctorname").write[String] and
   (__ \ "gpcode").writeNullable[String] and
   (__ \ "phonenumber").writeNullable[String] and
   (__ \ "active").writeNullable[Boolean])(unlift(getDoctorsLstObj.unapply))
 implicit val getResponseFormat: Format[getDoctorsLstObj] = Format(getDoctorsLstObjReads, getDoctorsLstObjWrites)
}

case class getDoctorConsultationReqListByAddressID(AddressID: Long, DoctorID: Long, filterByDate: Option[java.sql.Date])

object getDoctorConsultationReqListByAddressID {
  val getDoctorConsultationReqListByAddressIDReads = (
    (__ \ "AddressID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "filterByDate").readNullable[java.sql.Date])(getDoctorConsultationReqListByAddressID.apply _)

  val getDoctorConsultationReqListByAddressIDWrites = (
    (__ \ "AddressID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "filterByDate").writeNullable[java.sql.Date])(unlift(getDoctorConsultationReqListByAddressID.unapply))
  implicit val getResponseFormat: Format[getDoctorConsultationReqListByAddressID] = Format(getDoctorConsultationReqListByAddressIDReads, getDoctorConsultationReqListByAddressIDWrites)
}

case class getDoctorConsultationReqList(AddressID: Option[Long], AddressConsultID: Option[List[Long]], DoctorID: Long,
    filterByDate: java.sql.Date,filterByEndDate:Option[java.sql.Date],StartTime: Option[String], EndDate: Option[java.sql.Date], 
    EndTime: Option[String], Available: Option[String],IncludeWalkIns:Option[String], IncludeAllSlots:Option[String], PosePone: Option[String], ReasonForReschedule: Option[String])

object getDoctorConsultationReqList {
  val getDoctorConsultationReqListReads = (
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "AddressConsultID").readNullable[List[Long]] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "filterByDate").read[java.sql.Date] and
    (__ \ "filterByEndDate").readNullable[java.sql.Date] and
    (__ \ "StartTime").readNullable[String] and
    (__ \ "EndDate").readNullable[java.sql.Date] and
    (__ \ "EndTime").readNullable[String] and 
    (__ \ "Available").readNullable[String] and
    (__ \ "IncludeWalkIns").readNullable[String] and
    (__ \ "IncludeAllSlots").readNullable[String] and 
    (__ \ "PosePone").readNullable[String] and
    (__ \ "ReasonForReschedule").readNullable[String])(getDoctorConsultationReqList.apply _)

  val getDoctorConsultationReqListWrites = (
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "AddressConsultID").writeNullable[List[Long]] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "filterByDate").write[java.sql.Date] and
    (__ \ "filterByEndDate").writeNullable[java.sql.Date] and
    (__ \ "StartTime").writeNullable[String] and
    (__ \ "EndDate").writeNullable[java.sql.Date] and
    (__ \ "EndTime").writeNullable[String] and 
    (__ \ "Available").writeNullable[String] and
    (__ \ "IncludeWalkIns").writeNullable[String] and
    (__ \ "IncludeAllSlots").writeNullable[String] and
    (__ \ "PosePone").writeNullable[String] and
    (__ \ "ReasonForReschedule").writeNullable[String])(unlift(getDoctorConsultationReqList.unapply))
  implicit val getResponseFormat: Format[getDoctorConsultationReqList] = Format(getDoctorConsultationReqListReads, getDoctorConsultationReqListWrites)
}

case class getDoctorConsultationInfoList(HospitalInfo:Option[getHospitalInfo],ClinicName: Option[String],HospitalCode:Option[String],AddressConsultID: Long, AddressID: Long, DoctorID: Long,ConsultationDate:java.sql.Date,
                                         DayCD: String, StartTime: String, EndTime: String, MinPerCase: Int, Fees: Option[Int], SlotList: Array[getDoctorConsultationResList])

object getDoctorConsultationInfoList {
  val getDoctorConsultationInfoListReads = (
    (__ \ "HospitalInfo").readNullable[getHospitalInfo] and  
    (__ \ "ClinicName").readNullable[String] and
    (__ \ "HospitalCode").readNullable[String] and
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "AddressID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "ConsultationDate").read[java.sql.Date] and
    (__ \ "DayCD").read[String] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "MinPerCase").read[Int] and
    (__ \ "Fees").readNullable[Int] and
    (__ \ "SlotList").read[Array[getDoctorConsultationResList]])(getDoctorConsultationInfoList.apply _)

  val getDoctorConsultationInfoListWrites = (
    (__ \ "HospitalInfo").writeNullable[getHospitalInfo] and
    (__ \ "ClinicName").writeNullable[String] and
    (__ \ "HospitalCode").writeNullable[String] and
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "AddressID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "ConsultationDate").write[java.sql.Date] and
    (__ \ "DayCD").write[String] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "MinPerCase").write[Int] and
    (__ \ "Fees").writeNullable[Int] and
    (__ \ "SlotList").write[Array[getDoctorConsultationResList]])(unlift(getDoctorConsultationInfoList.unapply))
  implicit val getResponseFormat: Format[getDoctorConsultationInfoList] = Format(getDoctorConsultationInfoListReads, getDoctorConsultationInfoListWrites)
}

case class getAddressConsultInfo(HospitalInfo:Option[getHospitalInfo],ClinicName: Option[String],HospitalCode:Option[String],AddressID: Long, DoctorID: Long,ConsultationLst:List[getConsultDtLst])

object getAddressConsultInfo {
  val getConsultDtLstReads = (
    (__ \ "HospitalInfo").readNullable[getHospitalInfo] and
    (__ \ "ClinicName").readNullable[String] and
    (__ \ "HospitalCode").readNullable[String] and
    (__ \ "AddressID").read[Long] and
    (__ \ "DoctorID").read[Long] and
    (__ \ "ConsultationLst").read[List[getConsultDtLst]])(getAddressConsultInfo.apply _)

  val getConsultDtLstWrites = (
    (__ \ "HospitalInfo").writeNullable[getHospitalInfo] and
    (__ \ "ClinicName").writeNullable[String] and
    (__ \ "HospitalCode").writeNullable[String] and
    (__ \ "AddressID").write[Long] and
    (__ \ "DoctorID").write[Long] and
    (__ \ "ConsultationLst").write[List[getConsultDtLst]])(unlift(getAddressConsultInfo.unapply))
  implicit val getResponseFormat: Format[getAddressConsultInfo] = Format(getConsultDtLstReads, getConsultDtLstWrites)
}

case class getHospitalInfo(HospitalID:Long,ParentHospitalID: Long, HospitalName: String,HospitalCD:String,BranchCD:String)

object getHospitalInfo {
  val getHospitalReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "ParentHospitalID").read[Long] and
    (__ \ "HospitalName").read[String] and
    (__ \ "HospitalCD").read[String] and
    (__ \ "BranchCD").read[String])(getHospitalInfo.apply _)

  val getHospitalWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "ParentHospitalID").write[Long] and
    (__ \ "HospitalName").write[String] and
    (__ \ "HospitalCD").write[String] and
    (__ \ "BranchCD").write[String])(unlift(getHospitalInfo.unapply))
  implicit val getHospitalFormat: Format[getHospitalInfo] = Format(getHospitalReads, getHospitalWrites)
}

case class getConsultDtLst(consultationDt:java.sql.Date,AddressConsultIDLst:Array[getAddressConsultIDLst])

object getConsultDtLst {
  val getConsultDtLstReads = (
    (__ \ "consultationDt").read[java.sql.Date] and
    (__ \ "AddressConsultIDLst").read[Array[getAddressConsultIDLst]])(getConsultDtLst.apply _)

  val getConsultDtLstWrites = (
    (__ \ "consultationDt").write[java.sql.Date] and
    (__ \ "SlotLst").write[Array[getAddressConsultIDLst]])(unlift(getConsultDtLst.unapply))
  implicit val getResponseFormat: Format[getConsultDtLst] = Format(getConsultDtLstReads, getConsultDtLstWrites)
}


case class getAddressConsultIDLst(AddressConsultID:Long,StartTime:Option[String],EndTime:Option[String],MinPerCase:Option[Int],SlotLst:List[getDoctorConsultationResList])

object getAddressConsultIDLst {
  val getAddressConsultIDLstReads = (
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "StartTime").readNullable[String] and
    (__ \ "EndTime").readNullable[String] and
    (__ \ "MinPerCase").readNullable[Int] and
    (__ \ "SlotLst").read[List[getDoctorConsultationResList]])(getAddressConsultIDLst.apply _)

  val getAddressConsultIDLstWrites = (
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "StartTime").writeNullable[String] and
    (__ \ "EndTime").writeNullable[String] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "SlotLst").write[List[getDoctorConsultationResList]])(unlift(getAddressConsultIDLst.unapply))
  implicit val getResponseFormat: Format[getAddressConsultIDLst] = Format(getAddressConsultIDLstReads, getAddressConsultIDLstWrites)
}



case class doctorConsultLst(consultationDt:java.sql.Date,SlotLst:List[getDoctorConsultationResList])

object doctorConsultLst{
  val doctorConsultLstReads = (
    (__ \ "consultationDt").read[java.sql.Date] and
    (__ \ "SlotLst").read[List[getDoctorConsultationResList]])(doctorConsultLst.apply _)

  val doctorConsultLstWrites = (
    (__ \ "consultationDt").write[java.sql.Date] and
    (__ \ "SlotLst").write[List[getDoctorConsultationResList]])(unlift(doctorConsultLst.unapply))
  implicit val getResponseFormat: Format[doctorConsultLst] = Format(doctorConsultLstReads, doctorConsultLstWrites)
}


case class getDoctorConsultationResList(sequence:Int,StartTime: String, EndTime: String, Available: String, TokenNumber: Int, PatientInfo: Option[getPatientConsultationInfo])

object getDoctorConsultationResList {
  val getDoctorConsultationResListReads = (
    (__ \ "sequence").read[Int] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "Available").read[String] and
    (__ \ "TokenNumber").read[Int] and
    (__ \ "PatientInfo").readNullable[getPatientConsultationInfo])(getDoctorConsultationResList.apply _)

  val getDoctorConsultationResListWrites = (
    (__ \ "sequence").write[Int] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "Available").write[String] and
    (__ \ "TokenNumber").write[Int] and
    (__ \ "PatientInfo").writeNullable[getPatientConsultationInfo])(unlift(getDoctorConsultationResList.unapply))
  implicit val getResponseFormat: Format[getDoctorConsultationResList] = Format(getDoctorConsultationResListReads, getDoctorConsultationResListWrites)
}

case class getPatientConsultationInfo(PatientID: Long, Title: Option[String], FullName:String,AppointmentID: Long, AppointmentStatus: String, MobileNumber: Option[Long],PatientAddress: Array[patientAddressDetailsObj], PatientEmail: Array[patientEmailDetailsObj], Age: Option[Double], Sex: Option[String],CurrentAge:Option[currentAgeObj])

object getPatientConsultationInfo {
  val getPatientConsultationInfoReads = (
    (__ \ "PatientID").read[Long] and
    (__ \ "Title").readNullable[String] and
    (__ \ "FullName").read[String] and
    (__ \ "AppointmentID").read[Long] and
    (__ \ "AppointmentStatus").read[String] and
    (__ \ "MobileNumber").readNullable[Long] and
    (__ \ "PatientAddress").read[Array[patientAddressDetailsObj]] and
    (__ \ "PatientEmail").read[Array[patientEmailDetailsObj]] and
    (__ \ "Age").readNullable[Double] and
    (__ \ "Sex").readNullable[String] and
    (__ \ "CurrentAge").readNullable[currentAgeObj])(getPatientConsultationInfo.apply _)

  val getPatientConsultationInfoWrites = (
    (__ \ "PatientID").write[Long] and
    (__ \ "Title").writeNullable[String] and
    (__ \ "FullName").write[String] and
    (__ \ "AppointmentID").write[Long] and
    (__ \ "AppointmentStatus").write[String] and
    (__ \ "MobileNumber").writeNullable[Long] and
    (__ \ "PatientAddress").write[Array[patientAddressDetailsObj]] and
    (__ \ "PatientEmail").write[Array[patientEmailDetailsObj]] and
    (__ \ "Age").writeNullable[Double] and
    (__ \ "Sex").writeNullable[String] and
    (__ \ "CurrentAge").writeNullable[currentAgeObj])(unlift(getPatientConsultationInfo.unapply))
  implicit val getResponseFormat: Format[getPatientConsultationInfo] = Format(getPatientConsultationInfoReads, getPatientConsultationInfoWrites)

}



case class clinicPreferenceObj(HospitalID: Option[Long], PatientDisPlayID: Option[String], FamilyDisplayID: Option[String],
                                 PatientLstVst: Option[String], isEmailEnabled: Option[String])

object clinicPreferenceObj {
  val clinicPreferenceObjReads = (
    (__ \ "HospitalID").readNullable[Long] and  
    (__ \ "PatientDisPlayID").readNullable[String] and  
    (__ \ "FamilyDisplayID").readNullable[String]  and
    (__ \ "PatientLastVisit").readNullable[String] and
    (__ \ "isEmailEnabled").readNullable[String])(clinicPreferenceObj.apply _)

  val clinicPreferenceObjWrites = (
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "PatientDisPlayID").writeNullable[String] and  
    (__ \ "FamilyDisplayID").writeNullable[String]  and
    (__ \ "PatientLastVisit").writeNullable[String] and
    (__ \ "isEmailEnabled").writeNullable[String])(unlift(clinicPreferenceObj.unapply))
  implicit val clinicPreferenceObjFormat: Format[clinicPreferenceObj] = Format(clinicPreferenceObjReads, clinicPreferenceObjWrites)
}


case class buildDoctorAppointmentList(SeqNo:Int,AppointmentID: Long,AddressConsultID: Long,DayCD: String,ConsultationDt: java.sql.Date,StartTime: String,EndTime: String,
    PatientID: Option[Long],VisitUserTypeID: String,DoctorID: Option[Long], FirstTimeVisit: String,DoctorVisitRsnID: String,OtherVisitRsn: Option[String],
    AppointmentStatus: String,TokenNumber:String, VisitRsnID: Option[Long], PreliminaryNotes: Option[String], ReferralSourceDtls: Option[sourceDetailsObj], ClinicPreference:Option[clinicPreferenceObj])


object buildDoctorAppointmentList {
  val buildDoctorAppointmentListReads = (
  (__ \ "SeqNo").read[Int] and
  (__ \ "AppointmentID").read[Long] and
  (__ \ "AddressConsultID").read[Long] and
  (__ \ "DayCD").read[String] and
  (__ \ "ConsultationDt").read[java.sql.Date] and
  (__ \ "StartTime").read[String] and
  (__ \ "EndTime").read[String] and
  (__ \ "PatientID").readNullable[Long] and
  (__ \ "VisitUserTypeID").read[String] and
  (__ \ "DoctorID").readNullable[Long] and
  (__ \ "FirstTimeVisit").read[String] and
  (__ \ "DoctorVisitRsnID").read[String] and
  (__ \ "OtherVisitRsn").readNullable[String] and
  (__ \ "AppointmentStatus").read[String] and
  (__ \ "TokenNumber").read[String] and
  (__ \ "VisitRsnID").readNullable[Long] and
  (__ \ "PreliminaryNotes").readNullable[String] and
  (__ \ "ReferralSourceDtls").readNullable[sourceDetailsObj] and
  (__ \ "ClinicPreference").readNullable[clinicPreferenceObj])(buildDoctorAppointmentList.apply _)
 
  val buildDoctorAppointmentListWrites = (
  (__ \ "SeqNo").write[Int] and
  (__ \ "AppointmentID").write[Long] and
  (__ \ "AddressConsultID").write[Long] and
  (__ \ "DayCD").write[String] and
  (__ \ "ConsultationDt").write[java.sql.Date] and
  (__ \ "StartTime").write[String] and
  (__ \ "EndTime").write[String] and
  (__ \ "PatientID").writeNullable[Long] and
  (__ \ "VisitUserTypeID").write[String] and
  (__ \ "DoctorID").writeNullable[Long] and
  (__ \ "FirstTimeVisit").write[String] and
  (__ \ "DoctorVisitRsnID").write[String] and
  (__ \ "OtherVisitRsn").writeNullable[String] and
  (__ \ "AppointmentStatus").write[String] and
  (__ \ "TokenNumber").write[String] and
  (__ \ "VisitRsnID").writeNullable[Long] and
  (__ \ "PreliminaryNotes").writeNullable[String] and
  (__ \ "ReferralSourceDtls").writeNullable[sourceDetailsObj] and
  (__ \ "ClinicPreference").writeNullable[clinicPreferenceObj])(unlift(buildDoctorAppointmentList.unapply))
  implicit val getResponseFormat: Format[buildDoctorAppointmentList] = Format(buildDoctorAppointmentListReads, buildDoctorAppointmentListWrites)
}

case class buildHospitalAppointmentList(SeqNo:Int,AppointmentID: Long,AddressConsultID: Long,DayCD: String,ConsultationDt: java.sql.Date,StartTime: String,EndTime: String,
    PatientID: Option[Long],VisitUserTypeID: String, FirstTimeVisit: String,DoctorVisitRsnID: String,
    AppointmentStatus: String,TokenNumber:String)


object buildHospitalAppointmentList {
  val buildHospitalAppointmentListReads = (
  (__ \ "SeqNo").read[Int] and
  (__ \ "AppointmentID").read[Long] and
  (__ \ "AddressConsultID").read[Long] and
  (__ \ "DayCD").read[String] and
  (__ \ "ConsultationDt").read[java.sql.Date] and
  (__ \ "StartTime").read[String] and
  (__ \ "EndTime").read[String] and
  (__ \ "PatientID").readNullable[Long] and
  (__ \ "VisitUserTypeID").read[String] and
  (__ \ "FirstTimeVisit").read[String] and
  (__ \ "DoctorVisitRsnID").read[String] and
  (__ \ "AppointmentStatus").read[String] and
  (__ \ "TokenNumber").read[String])(buildHospitalAppointmentList.apply _)

  val buildHospitalAppointmentListWrites = (
  (__ \ "SeqNo").write[Int] and
  (__ \ "AppointmentID").write[Long] and
  (__ \ "AddressConsultID").write[Long] and
  (__ \ "DayCD").write[String] and
  (__ \ "ConsultationDt").write[java.sql.Date] and
  (__ \ "StartTime").write[String] and
  (__ \ "EndTime").write[String] and
  (__ \ "PatientID").writeNullable[Long] and
  (__ \ "VisitUserTypeID").write[String] and
  (__ \ "FirstTimeVisit").write[String] and
  (__ \ "DoctorVisitRsnID").write[String] and
  (__ \ "AppointmentStatus").write[String] and
  (__ \ "TokenNumber").write[String])(unlift(buildHospitalAppointmentList.unapply))
  implicit val getResponseFormat: Format[buildHospitalAppointmentList] = Format(buildHospitalAppointmentListReads, buildHospitalAppointmentListWrites)
}

case class buildAppointmentObj(AppointmentID: Long,AddressConsultID: Long,DayCD: String,ConsultationDt: java.sql.Date,StartTime: String,EndTime: String,
    PatientID: Option[Long],VisitUserTypeID: String, FirstTimeVisit: String,DoctorVisitRsnID: String,
    AppointmentStatus: String,TokenNumber:String,HospitalObj:Option[getHospitalInfo])


object buildAppointmentObj {
  val buildAppointmentObjReads = (
  (__ \ "AppointmentID").read[Long] and
  (__ \ "AddressConsultID").read[Long] and
  (__ \ "DayCD").read[String] and
  (__ \ "ConsultationDt").read[java.sql.Date] and
  (__ \ "StartTime").read[String] and
  (__ \ "EndTime").read[String] and
  (__ \ "PatientID").readNullable[Long] and
  (__ \ "VisitUserTypeID").read[String] and
  (__ \ "FirstTimeVisit").read[String] and
  (__ \ "DoctorVisitRsnID").read[String] and
  (__ \ "AppointmentStatus").read[String] and
  (__ \ "TokenNumber").read[String] and 
  (__ \ "HospitalObj").readNullable[getHospitalInfo])(buildAppointmentObj.apply _)

  val buildAppointmentObjWrites = (
  (__ \ "AppointmentID").write[Long] and
  (__ \ "AddressConsultID").write[Long] and
  (__ \ "DayCD").write[String] and
  (__ \ "ConsultationDt").write[java.sql.Date] and
  (__ \ "StartTime").write[String] and
  (__ \ "EndTime").write[String] and
  (__ \ "PatientID").writeNullable[Long] and
  (__ \ "VisitUserTypeID").write[String] and
  (__ \ "FirstTimeVisit").write[String] and
  (__ \ "DoctorVisitRsnID").write[String] and
  (__ \ "AppointmentStatus").write[String] and
  (__ \ "TokenNumber").write[String]  and 
  (__ \ "HospitalObj").writeNullable[getHospitalInfo])(unlift(buildAppointmentObj.unapply))
  implicit val getResponseFormat: Format[buildAppointmentObj] = Format(buildAppointmentObjReads, buildAppointmentObjWrites)
}


//newly added request for calender view

case class getCalenderViewReqList(HospitalID: Long,StartTime: String ,EndTime: String,FilterDate: java.sql.Date)

object getCalenderViewReqList {
  val getCalenderViewReqListReads = (
      (__ \ "HospitalID").read[Long] and
      (__ \ "StartTime").read[String] and
      (__ \ "EndTime").read[String] and 
      (__ \ "FilterDate").read[java.sql.Date])(getCalenderViewReqList.apply _)

  val getCalenderViewReqListWrites = (
      (__ \ "HospitalID").write[Long] and
      (__ \ "StartTime").write[String] and
      (__ \ "EndTime").write[String] and 
      (__ \ "FilterDate").write[java.sql.Date])(unlift(getCalenderViewReqList.unapply))
  implicit val getResponseFormat: Format[getCalenderViewReqList] = Format(getCalenderViewReqListReads, getCalenderViewReqListWrites)
}

//newly added resposne for calender view


case class doctorInfo(profileInfo: profileInfodetobj , clinicInfo : clinicInfodetobj )

object  doctorInfo {
  val doctorInfoReads = (
      (__ \ "profileInfo").read[profileInfodetobj] and
      (__ \ "clinicInfo").read[clinicInfodetobj])(doctorInfo.apply _)

  val doctorInfoWrites = (
      (__ \ "profileInfo").write[profileInfodetobj] and
      (__ \ "clinicInfo").write[clinicInfodetobj])(unlift(doctorInfo.unapply))
  implicit val doctorInfoFormat: Format[ doctorInfo] = Format(doctorInfoReads,  doctorInfoWrites)
} 

case class profileInfodetobj(DoctorID: Long, FullName: String)

object  profileInfodetobj {
  val profileInfodetobjReads = (
      (__ \ "DoctorID").read[Long] and
      (__ \ "FullName").read[String])(profileInfodetobj.apply _)
      
  val profileInfodetobjWrites = (
      (__ \ "DoctorID").write[Long] and
      (__ \ "FullName").write[String])(unlift(profileInfodetobj.unapply))
  implicit val doctorInfodetFormat: Format[profileInfodetobj] = Format(profileInfodetobjReads, profileInfodetobjWrites)
}  

case class clinicInfodetobj(ClinicName: String,HospitalCode: String, AddressID: Long, ConsultationInfo: Array[ConsultationInfo])
object  clinicInfodetobj {
  val clinicInfodetobjReads = (
      (__ \ "ClinicName").read[String] and
      (__ \ "HospitalCode").read[String] and
      (__ \ "AddressID").read[Long] and
      (__ \ "ConsultationInfo").read[Array[ConsultationInfo]])(clinicInfodetobj.apply _)
      
  val clinicInfodetobjWrites = (
      (__ \ "ClinicName").write[String] and
      (__ \ "HospitalCode").write[String] and
      (__ \ "AddressID").write[Long] and
      (__ \ "ConsultationInfo").write[Array[ConsultationInfo]])(unlift(clinicInfodetobj.unapply))
  implicit val clinicInfodetobjFormat: Format[clinicInfodetobj] = Format(clinicInfodetobjReads, clinicInfodetobjWrites)
}  

case class ConsultationInfo(AddressConsultID: Long,ConsultationDate: java.sql.Date, DayCD: String,StartTime : String,EndTime : String ,MinPerCase : Option[Int],Fees : Option[Int],SlotList: Array[getDoctorConsultationResList])

object  ConsultationInfo {
  val ConsultationInfoReads = (
       (__ \ "AddressConsultID").read[Long] and
       (__ \ "ConsultationDate").read[java.sql.Date] and
       (__ \ "DayCD").read[String] and
       (__ \ "StartTime").read[String] and
       (__ \ "EndTime").read[String] and
       (__ \ "MinPerCase").readNullable[Int] and
       (__ \ "Fees").readNullable[Int] and
       (__ \ "SlotList").read[Array[getDoctorConsultationResList]])(ConsultationInfo.apply _)
      
  val ConsultationInfoWrites = (
       (__ \ "AddressConsultID").write[Long] and
       (__ \ "ConsultationDate").write[java.sql.Date] and
      (__ \ "DayCD").write[String] and
      (__ \ "StartTime").write[String] and
      (__ \ "EndTime").write[String] and
      (__ \ "MinPerCase").writeNullable[Int] and
       (__ \ "Fees").writeNullable[Int] and
      (__ \ "SlotList").write[Array[getDoctorConsultationResList]])(unlift(ConsultationInfo.unapply))
  implicit val ConsultationInfoFormat: Format[ConsultationInfo] = Format(ConsultationInfoReads, ConsultationInfoWrites)
}  

case class getDoctorConsultationListForReschedule(AddressConsultID: Long, SeqNo: Long, StartTime: String, EndTime: String, ConsulationDt: java.sql.Date, TokenNumber: String)

object getDoctorConsultationListForReschedule {
  val getDoctorConsultationListForRescheduleReads = (
    (__ \ "AddressConsultID").read[Long] and
    (__ \ "SeqNo").read[Long] and
    (__ \ "StartTime").read[String] and
    (__ \ "EndTime").read[String] and
    (__ \ "ConsulationDt").read[java.sql.Date] and
    (__ \ "TokenNumber").read[String])(getDoctorConsultationListForReschedule.apply _)

  val getDoctorConsultationListForRescheduleWrites = (
    (__ \ "AddressConsultID").write[Long] and
    (__ \ "SeqNo").write[Long] and
    (__ \ "StartTime").write[String] and
    (__ \ "EndTime").write[String] and
    (__ \ "ConsulationDt").write[java.sql.Date]and
    (__ \ "TokenNumber").write[String])(unlift(getDoctorConsultationListForReschedule.unapply))
  implicit val getResponseFormat: Format[getDoctorConsultationListForReschedule] = Format(getDoctorConsultationListForRescheduleReads, getDoctorConsultationListForRescheduleWrites)
}


