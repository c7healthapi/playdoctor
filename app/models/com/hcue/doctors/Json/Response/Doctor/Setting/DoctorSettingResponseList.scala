/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response.Doctor.Setting

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.functional.syntax.unlift
import play.api.libs.json.Format
import play.api.libs.json.__

case class ListReadDoctorHistoryObj(HistoryID: Long, Description: String,ActiveIND: String,IsPrivate:String)

object ListReadDoctorHistoryObj {
  val ListReadDoctorHistoryObjReads = (
    (__ \ "HistoryID").read[Long] and
    (__ \ "Description").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "IsPrivate").read[String])(ListReadDoctorHistoryObj.apply _)

  val ListReadDoctorHistoryObjWrites = (
    (__ \ "HistoryID").write[Long] and
    (__ \ "Description").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "IsPrivate").write[String])(unlift(ListReadDoctorHistoryObj.unapply))
  implicit val ListReadDoctorHistoryFormat: Format[ListReadDoctorHistoryObj] = Format(ListReadDoctorHistoryObjReads, ListReadDoctorHistoryObjWrites)
}
