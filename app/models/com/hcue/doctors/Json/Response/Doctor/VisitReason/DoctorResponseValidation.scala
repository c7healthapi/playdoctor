/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response.Doctor.VisitReason

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Time

import play.api.libs.functional.syntax._
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorEmailDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.AddUpdate.DoctorAddressDetailsObj
import org.joda.time.DateTime

case class getDoctorVisitRsnObj( DoctorVisitRsnID: String, DoctorSpecialityID: String, DoctorVisitRsnDesc: String, VisitUserTypeID: String,SequenceRow:Int, ActiveIND: String)

                          
object getDoctorVisitRsnObj {
  val getDoctorVisitRsnObjReads = (
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "DoctorSpecialityID").read[String] and
    (__ \ "DoctorVisitRsnDesc").read[String] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "SequenceRow").read[Int] and
    (__ \ "ActiveIND").read[String])(getDoctorVisitRsnObj.apply _)

  val getDoctorVisitRsnObjWrites = (
    
    (__ \ "DoctorVisitRsnID").write[String] and
    (__ \ "DoctorSpecialityID").write[String] and
    (__ \ "DoctorVisitRsnDesc").write[String] and
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "SequenceRow").write[Int] and
    (__ \ "ActiveIND").write[String])(unlift(getDoctorVisitRsnObj.unapply))
  implicit val getResponseFormat: Format[getDoctorVisitRsnObj] = Format(getDoctorVisitRsnObjReads, getDoctorVisitRsnObjWrites)
}

