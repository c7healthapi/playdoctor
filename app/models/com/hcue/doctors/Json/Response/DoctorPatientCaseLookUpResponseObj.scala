/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date
import java.sql.Timestamp
import java.util.Date
import play.api.data.validation._
import play.api.libs.functional.syntax._
import org.joda.time.DateTime

case class ListItemLookUpObj(ItemID: Long, ItemDesc: String, BodyParts: Map[String, String])

object ListItemLookUpObj {
  val ListItemLookUpObjReads = (
    (__ \ "ItemID").read[Long] and
    (__ \ "ItemDesc").read[String] and
    (__ \ "BodyParts").read[Map[String, String]])(ListItemLookUpObj.apply _)

  val ListItemLookUpObjWrites = (
    (__ \ "ItemID").write[Long] and
    (__ \ "ItemDesc").write[String] and
    (__ \ "BodyParts").write[Map[String, String]])(unlift(ListItemLookUpObj.unapply))
  implicit val ListItemLookUpObjFormat: Format[ListItemLookUpObj] = Format(ListItemLookUpObjReads, ListItemLookUpObjWrites)
}

case class ListItemPropertyObj(ItemPropertyKeyID: Long, ItemPropertyID: String, ItemPropertyValue: String, ActiveIND: String)

object ListItemPropertyObj {
  val ListItemPropertyObjReads = (
    (__ \ "ItemPropertyKeyID").read[Long] and
    (__ \ "ItemPropertyID").read[String] and
    (__ \ "ItemPropertyValue").read[String] and
    (__ \ "ActiveIND").read[String])(ListItemPropertyObj.apply _)

  val ListItemPropertyObjWrites = (
    (__ \ "ItemPropertyKeyID").write[Long] and
    (__ \ "ItemPropertyID").write[String] and
    (__ \ "ItemPropertyValue").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(ListItemPropertyObj.unapply))
  implicit val ListItemLookUpObjFormat: Format[ListItemPropertyObj] = Format(ListItemPropertyObjReads, ListItemPropertyObjWrites)
}

case class ListMaterialLookUpObj(ItemMeterialID: Long, ItemMeterialDesc: String, ActiveIND: String)

object ListMaterialLookUpObj {
  val ListMaterialLookUpObjReads = (
    (__ \ "ItemMeterialID").read[Long] and
    (__ \ "ItemMeterialDesc").read[String] and
    (__ \ "ActiveIND").read[String])(ListMaterialLookUpObj.apply _)

  val ListMaterialLookUpObjWrites = (
    (__ \ "ItemMeterialID").write[Long] and
    (__ \ "ItemMeterialDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(ListMaterialLookUpObj.unapply))
  implicit val ListItemLookUpObjFormat: Format[ListMaterialLookUpObj] = Format(ListMaterialLookUpObjReads, ListMaterialLookUpObjWrites)
}

case class ListPatientCaseOrderItems(OrderItems: ListItemLookUpObj, OrderMeterial: Option[Array[ListMaterialLookUpObj]], ItemProperty: Option[Array[ListItemPropertyObj]])

object ListPatientCaseOrderItems {
  val ListPatientCaseOrderItemsReads = (
    (__ \ "OrderItems").read[ListItemLookUpObj] and
    (__ \ "OrderMeterial").readNullable[Array[ListMaterialLookUpObj]] and
    (__ \ "ItemProperty").readNullable[Array[ListItemPropertyObj]])(ListPatientCaseOrderItems.apply _)

  val ListPatientCaseOrderItemsWrites = (
    (__ \ "OrderItems").write[ListItemLookUpObj] and
    (__ \ "OrderMeterial").writeNullable[Array[ListMaterialLookUpObj]] and
    (__ \ "ItemProperty").writeNullable[Array[ListItemPropertyObj]])(unlift(ListPatientCaseOrderItems.unapply))
  implicit val ListItemLookUpObjFormat: Format[ListPatientCaseOrderItems] = Format(ListPatientCaseOrderItemsReads, ListPatientCaseOrderItemsWrites)
}

case class ListOrderStatus(OrderStatusID: Long, OrderStatusDesc: String)

object ListOrderStatus {
  val ListOrderStatusReads = (
    (__ \ "OrderStatusID").read[Long] and
    (__ \ "OrderStatusDesc").read[String])(ListOrderStatus.apply _)

  val ListOrderStatusWrites = (
    (__ \ "OrderStatusID").write[Long] and
    (__ \ "OrderStatusDesc").write[String])(unlift(ListOrderStatus.unapply))
  implicit val ListOrderStatusFormat: Format[ListOrderStatus] = Format(ListOrderStatusReads, ListOrderStatusWrites)
}

case class ListTreatmentStatusResponse(TreatmentStatusID: Long, TreatmentStatusDesc: String,ActiveIND: String)

object ListTreatmentStatusResponse {
  val ListTreatmentStatusReads = (
    (__ \ "TreatmentStatusID").read[Long] and
    (__ \ "TreatmentStatusDesc").read[String]  and
    (__ \ "ActiveIND").read[String])(ListTreatmentStatusResponse.apply _)

  val ListTreatmentStatusWrites = (
    (__ \ "TreatmentStatusID").write[Long] and
    (__ \ "TreatmentStatusDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(ListTreatmentStatusResponse.unapply))
  implicit val ListTreatmentStatusFormat: Format[ListTreatmentStatusResponse] = Format(ListTreatmentStatusReads, ListTreatmentStatusWrites)
}

case class ListPatientCaseLkUpResponse(OrderItems: Array[ListPatientCaseOrderItems], PaymentTypes: Array[PaymentTypeObj],
                                       OrderedStatus: Array[ListOrderStatus], TreatmentStatus: Array[ListTreatmentStatusResponse])

object ListPatientCaseLkUpResponse {
  val ListPatientCaseLkUpReads = (
    (__ \ "OrderItems").read[Array[ListPatientCaseOrderItems]] and
    (__ \ "PaymentTypes").read[Array[PaymentTypeObj]] and
    (__ \ "OrderedStatus").read[Array[ListOrderStatus]] and
    (__ \ "TreatmentStatus").read[Array[ListTreatmentStatusResponse]])(ListPatientCaseLkUpResponse.apply _)

  val ListPatientCaseLkUpWrites = (
    (__ \ "OrderItems").write[Array[ListPatientCaseOrderItems]] and
    (__ \ "PaymentTypes").write[Array[PaymentTypeObj]] and
    (__ \ "OrderedStatus").write[Array[ListOrderStatus]] and
    (__ \ "TreatmentStatus").write[Array[ListTreatmentStatusResponse]])(unlift(ListPatientCaseLkUpResponse.unapply))
  implicit val ListPatientCaseLkUpFormat: Format[ListPatientCaseLkUpResponse] = Format(ListPatientCaseLkUpReads, ListPatientCaseLkUpWrites)
}