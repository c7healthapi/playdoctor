/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class buildErrorResponseObj(exceptionType: String,Message : String,FillInStackTrace : String, LocalizedMessage : String, 
    ErrorCode : String, SQLState : String, StackTrace : String, Suppressed : String, StringFormat : String)

object buildErrorResponseObj {
  val buildErrorResponseReads = (
      (__ \ "ExceptionType").read[String] and
      (__ \ "Message").read[String] and
      //(__ \ "Cause").read[String] and
      (__ \ "FillInStackTrace").read[String] and
      (__ \ "LocalizedMessage").read[String] and
      (__ \ "ErrorCode").read[String] and
      (__ \ "SQLState").read[String] and
      (__ \ "StackTrace").read[String] and
      (__ \ "Suppressed").read[String] and
      (__ \ "StringFormat").read[String])(buildErrorResponseObj.apply _)

  val buildErrorResponseWrites = (
      (__ \ "ExceptionType").write[String] and
      (__ \ "Message").write[String] and
      //(__ \ "Cause").write[String] and
      (__ \ "FillInStackTrace").write[String] and
      (__ \ "LocalizedMessage").write[String] and
      (__ \ "ErrorCode").write[String] and
      (__ \ "SQLState").write[String] and
      (__ \ "StackTrace").write[String] and
      (__ \ "Suppressed").write[String] and
      (__ \ "StringFormat").write[String])(unlift(buildErrorResponseObj.unapply))
    
  implicit val buildErrorResponseFormat: Format[buildErrorResponseObj] = Format(buildErrorResponseReads, buildErrorResponseWrites)
}



 case class AddErrorLogReq(ErrorID: Option[Long], ErrorCode: Long, Environment:String,project:String, RemoteAddress: String, RequestURL: String,RequestMethod:String,
      RequestParameter:Option[JsValue],ResponseParameter:Option[JsValue],
      ResponseError:Option[String],Created:Option[java.sql.Date],USRId:Long,USRType:String)

 object AddErrorLogReq {

  val AddErrorLogReqReads = (
    (__ \ "ErrorID").readNullable[Long] and
    (__ \ "ErrorCode").read[Long] and
    (__ \ "Environment").read[String] and
    (__ \ "project").read[String] and
    (__ \ "RemoteAddress").read[String] and
    (__ \ "RequestURL").read[String] and
    (__ \ "RequestMethod").read[String] and
    (__ \ "RequestParameter").readNullable[JsValue] and
    (__ \ "ResponseParameter").readNullable[JsValue] and
    (__ \ "ResponseError").readNullable[String] and
    (__ \ "Created").readNullable[java.sql.Date] and
    (__ \ "USRId").read[Long] and
    (__ \ "USRType").read[String])(AddErrorLogReq.apply _)

  val AddErrorLogReqWrites = (
    (__ \ "ErrorID").writeNullable[Long] and
    (__ \ "ErrorCode").write[Long] and
    (__ \ "Environment").write[String] and
    (__ \ "project").write[String] and
    (__ \ "RemoteAddress").write[String] and
    (__ \ "RequestURL").write[String] and
    (__ \ "RequestMethod").write[String] and
    (__ \ "RequestParameter").writeNullable[JsValue] and
    (__ \ "ResponseParameter").writeNullable[JsValue] and
    (__ \ "ResponseError").writeNullable[String] and
    (__ \ "Created").writeNullable[java.sql.Date] and
    (__ \ "USRId").write[Long] and
    (__ \ "USRType").write[String])(unlift(AddErrorLogReq.unapply))

  implicit val AddErrorLogReqFormat: Format[AddErrorLogReq] = Format(AddErrorLogReqReads, AddErrorLogReqWrites)
}
  