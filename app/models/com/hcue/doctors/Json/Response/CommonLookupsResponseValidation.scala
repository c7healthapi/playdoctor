/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date

import play.api.libs.functional.syntax._
import models.com.hcue.doctors.slick.lookupTable.TAddressTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TContactTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TCountryLkup
import models.com.hcue.doctors.slick.lookupTable.TDayLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorQualfLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorSpecialityLkup
import models.com.hcue.doctors.slick.lookupTable.TDoctorVisitRsnLkyp
import models.com.hcue.doctors.slick.lookupTable.TFamilyRltnLkup
import models.com.hcue.doctors.slick.lookupTable.TOtherIDTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TPhoneTypeLkup
import models.com.hcue.doctors.slick.lookupTable.TStateLkup
import models.com.hcue.doctors.slick.lookupTable.TUSRTypeLkup
 
case class getCommonLookupObj(addressType: Array[AddressTypeObj], contactType: Array[ContactTypeObj],countryType:Array[CountryLkupObj],
                              cityType:Array[CityLkupObj],locationType:Array[LocationLkupObj],
                              dayType: Array[DayTypeObj], 
                              //doctorQualfType: Array[DoctorQualfTypeObj], 
                              doctorSpecialityType: Array[DoctorSpecialityTypeObj],
                              //doctorVisitRsnType: Array[DoctorVisitRsnTypeObj], 
                              //familyRltnType: Array[FamilyRltnTypeObj], 
                              otherIDType: Array[OtherIDTypeObj],
                              phoneType: Array[PhoneTypeObj], stateType: Array[StateLkupObj], usrType: Array[UsrTypeObj],
                               appointmentStatus: Array[AppointmentStatusObj],currencyType:Array[CurrencyLkupObj])
							  
object getCommonLookupObj {
  val getCommonLookupObjReads = (
    (__ \ "addressType").read[Array[AddressTypeObj]] and
    (__ \ "contactType").read[Array[ContactTypeObj]] and
    (__ \ "countryType").read[Array[CountryLkupObj]] and
    (__ \ "cityType").read[Array[CityLkupObj]] and
    (__ \ "locationType").read[Array[LocationLkupObj]] and
    (__ \ "dayType").read[Array[DayTypeObj]] and
    //(__ \ "doctorQualfType").read[Array[DoctorQualfTypeObj]] and
    (__ \ "doctorSpecialityType").read[Array[DoctorSpecialityTypeObj]] and
    //(__ \ "doctorVisitRsnType").read[Array[DoctorVisitRsnTypeObj]] and
    //(__ \ "familyRltnType").read[Array[FamilyRltnTypeObj]] and
    (__ \ "otherIDType").read[Array[OtherIDTypeObj]] and
    (__ \ "phoneType").read[Array[PhoneTypeObj]] and
    (__ \ "stateType").read[Array[StateLkupObj]] and
    (__ \ "usrType").read[Array[UsrTypeObj]] and
    (__ \ "appointmentStatus").read[Array[AppointmentStatusObj]] and
    (__ \ "currencyType").read[Array[CurrencyLkupObj]])(getCommonLookupObj.apply _)

  val getCommonLookupObjWrites = (
    (__ \ "addressType").write[Array[AddressTypeObj]] and
    (__ \ "contactType").write[Array[ContactTypeObj]] and
    (__ \ "countryType").write[Array[CountryLkupObj]] and
    (__ \ "cityType").write[Array[CityLkupObj]] and
    (__ \ "locationType").write[Array[LocationLkupObj]] and
    (__ \ "dayType").write[Array[DayTypeObj]] and
    //(__ \ "doctorQualfType").write[Array[DoctorQualfTypeObj]] and
    (__ \ "doctorSpecialityType").write[Array[DoctorSpecialityTypeObj]] and
    //(__ \ "doctorVisitRsnType").write[Array[DoctorVisitRsnTypeObj]] and
    //(__ \ "familyRltnType").write[Array[FamilyRltnTypeObj]] and
    (__ \ "otherIDType").write[Array[OtherIDTypeObj]] and
    (__ \ "phoneType").write[Array[PhoneTypeObj]] and
    (__ \ "stateType").write[Array[StateLkupObj]] and
    (__ \ "usrType").write[Array[UsrTypeObj]] and
    (__ \ "appointmentStatus").write[Array[AppointmentStatusObj]] and
     (__ \ "currencyType").write[Array[CurrencyLkupObj]])(unlift(getCommonLookupObj.unapply))
  implicit val getResponseFormat: Format[getCommonLookupObj] = Format(getCommonLookupObjReads, getCommonLookupObjWrites)
}

case class AddressTypeObj(AddressTypeID: String, AddressTypeDesc: String, ActiveIND: String)

object AddressTypeObj {
  val AddressTypeObjReads = (
    (__ \ "AddressTypeID").read[String] and
    (__ \ "AddressTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(AddressTypeObj.apply _)

  val AddressTypeObjWrites = (
    (__ \ "AddressTypeID").write[String] and
    (__ \ "AddressTypeDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(AddressTypeObj.unapply))
  implicit val AddressTypeFormat: Format[AddressTypeObj] = Format(AddressTypeObjReads, AddressTypeObjWrites)
}

case class ContactTypeObj(contactTypeID: String, contactTypeDesc: String, ActiveIND: String)

object ContactTypeObj {
  val ContactTypeObjReads = (
    (__ \ "contactTypeID").read[String] and
    (__ \ "contactTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(ContactTypeObj.apply _)

  val ContactTypeObjWrites = (
    (__ \ "contactTypeID").write[String] and
    (__ \ "contactTypeDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(ContactTypeObj.unapply))
  implicit val contactTypeFormat: Format[ContactTypeObj] = Format(ContactTypeObjReads, ContactTypeObjWrites)
}

case class CountryLkupObj(CountryID: String, CountryDesc: String,CountrySEODesc:String, ActiveIND: String,CountryCode:Option[String],isPrivate: Option[String])

object CountryLkupObj {
  val CountryLkupObjReads = (
    (__ \ "CountryID").read[String] and
    (__ \ "CountryDesc").read[String] and
    (__ \ "CountrySEODesc").read[String] and
    (__ \ "ActiveIND").read[String] and 
    (__ \ "CountryCode").readNullable[String] and
    (__ \ "isPrivate").readNullable[String])(CountryLkupObj.apply _)

  val CountryLkupObjWrites = (
    (__ \ "CountryID").write[String] and
    (__ \ "CountryDesc").write[String] and
    (__ \ "CountrySEODesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "CountryCode").writeNullable[String] and
    (__ \ "isPrivate").writeNullable[String])(unlift(CountryLkupObj.unapply))
  implicit val contactTypeFormat: Format[CountryLkupObj] = Format(CountryLkupObjReads, CountryLkupObjWrites)
}


case class CurrencyLkupObj(CurrencyCode: String, CurrencyDesc: String, ActiveIND: String,CountryID: String)

object CurrencyLkupObj {
  val CurrencyLkupObjReads = (
    (__ \ "CurrencyCode").read[String] and
    (__ \ "CurrencyDesc").read[String] and
    (__ \ "ActiveIND").read[String] and 
    (__ \ "CountryID").read[String])(CurrencyLkupObj.apply _)

  val CurrencyLkupObjWrites = ( 
   
    (__ \ "CurrencyCode").write[String] and
    (__ \ "CurrencyDesc").write[String] and
    (__ \ "ActiveIND").write[String] and 
    (__ \ "CountryID").write[String])(unlift(CurrencyLkupObj.unapply))
  implicit val contactTypeFormat: Format[CurrencyLkupObj] = Format(CurrencyLkupObjReads,CurrencyLkupObjWrites)
}


case class DepositLkupObj(ExpenseTypeID: String, ExpTypeDescription: String, ActiveIND: String)

object DepositLkupObj {
  val DepositLkupObjReads = (
    (__ \ "ExpenseTypeID").read[String] and
    (__ \ "ExpTypeDescription").read[String] and
    (__ \ "ActiveIND").read[String])(DepositLkupObj.apply _)

  val DepositLkupObjWrites = ( 
   
    (__ \ "ExpenseTypeID").write[String] and
    (__ \ "ExpTypeDescription").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(DepositLkupObj.unapply))
  implicit val contactTypeFormat: Format[DepositLkupObj] = Format(DepositLkupObjReads,DepositLkupObjWrites)
}


case class IncentiveCategoryLkupObj(IncentiveCategoryID: Long, IncentiveCategoryDescription: String, ActiveIND: String)

object IncentiveCategoryLkupObj {
  
  val IncentiveCategoryLkupObjReads = (
    (__ \ "IncentiveCategoryID").read[Long] and
    (__ \ "IncentiveCategoryDescription").read[String] and
    (__ \ "ActiveIND").read[String])(IncentiveCategoryLkupObj.apply _)

  val IncentiveCategoryLkupObjWrites = ( 
   
    (__ \ "IncentiveCategoryID").write[Long] and
    (__ \ "IncentiveCategoryDescription").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(IncentiveCategoryLkupObj.unapply))
  implicit val IncentiveCategoryLkupObjFormat: Format[IncentiveCategoryLkupObj] = Format(IncentiveCategoryLkupObjReads,IncentiveCategoryLkupObjWrites)
}

case class DayTypeObj(DayID: String, DayDesc: String, ActiveIND: String)

object DayTypeObj {
  val DayTypeObjReads = (
    (__ \ "DayID").read[String] and
    (__ \ "DayDesc").read[String] and
    (__ \ "ActiveIND").read[String])(DayTypeObj.apply _)

  val DayTypeObjWrites = (
    (__ \ "DayID").write[String] and
    (__ \ "DayDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(DayTypeObj.unapply))
  implicit val contactTypeFormat: Format[DayTypeObj] = Format(DayTypeObjReads, DayTypeObjWrites)
}

case class DoctorQualfTypeObj(DoctorQualfID: String, DoctorQualfDesc: String, ActiveIND: String)

object DoctorQualfTypeObj {
  val DoctorQualfTypeObjReads = (
    (__ \ "DoctorQualfID").read[String] and
    (__ \ "DoctorQualfDesc").read[String] and
    (__ \ "ActiveIND").read[String])(DoctorQualfTypeObj.apply _)

  val DoctorQualfTypeObjWrites = (
    (__ \ "DoctorQualfID").write[String] and
    (__ \ "DoctorQualfDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(DoctorQualfTypeObj.unapply))
  implicit val contactTypeFormat: Format[DoctorQualfTypeObj] = Format(DoctorQualfTypeObjReads, DoctorQualfTypeObjWrites)
}

case class DoctorSpecialityTypeObj(DoctorSpecialityID: String, DoctorSpecialityDesc: String, SpecialityDesc: Option[String],DoctorSpecialitySEODesc:Option[String], ActiveIND: String)

object DoctorSpecialityTypeObj {
  val DoctorSpecialityTypeObjReads = (
    (__ \ "DoctorSpecialityID").read[String] and
    (__ \ "DoctorSpecialityDesc").read[String] and
    (__ \ "SpecialityDesc").read[Option[String]] and
    (__ \ "DoctorSpecialitySEODesc").readNullable[String] and
    (__ \ "ActiveIND").read[String])(DoctorSpecialityTypeObj.apply _)

  val DoctorSpecialityTypeObjWrites = (
    (__ \ "DoctorSpecialityID").write[String] and
    (__ \ "DoctorSpecialityDesc").write[String] and
    (__ \ "SpecialityDesc").write[Option[String]] and
    (__ \ "DoctorSpecialitySEODesc").writeNullable[String] and
    (__ \ "ActiveIND").write[String])(unlift(DoctorSpecialityTypeObj.unapply))
  implicit val contactTypeFormat: Format[DoctorSpecialityTypeObj] = Format(DoctorSpecialityTypeObjReads, DoctorSpecialityTypeObjWrites)
}

case class PaymentTypeObj(PaymentTypeID: String, PaymentTypeDesc: String,ActiveIND: String)

object PaymentTypeObj {
  val PaymentTypeObjReads = (
    (__ \ "PaymentTypeID").read[String] and
    (__ \ "PaymentTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(PaymentTypeObj.apply _)

  val PaymentTypeObjWrites = (
    (__ \ "PaymentTypeID").write[String] and
    (__ \ "PaymentTypeDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(PaymentTypeObj.unapply))
  implicit val contactTypeFormat: Format[PaymentTypeObj] = Format(PaymentTypeObjReads, PaymentTypeObjWrites)
}

case class DoctorVisitRsnTypeObj(DoctorVisitRsnID: String, DoctorSpecialityID: String, DoctorVisitRsnDesc: String,
                                 VisitUserTypeID: String, SequenceRow: String, ActiveIND: String)

object DoctorVisitRsnTypeObj {
  val DoctorVisitRsnTypeObjReads = (
    (__ \ "DoctorVisitRsnID").read[String] and
    (__ \ "DoctorSpecialityID").read[String] and
    (__ \ "DoctorVisitRsnDesc").read[String] and
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "SequenceRow").read[String] and
    (__ \ "ActiveIND").read[String])(DoctorVisitRsnTypeObj.apply _)

  val DoctorVisitRsnTypeObjWrites = (
    (__ \ "DoctorVisitRsnID").write[String] and
    (__ \ "DoctorSpecialityID").write[String] and
    (__ \ "DoctorVisitRsnDesc").write[String] and
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "SequenceRow").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(DoctorVisitRsnTypeObj.unapply))
  implicit val contactTypeFormat: Format[DoctorVisitRsnTypeObj] = Format(DoctorVisitRsnTypeObjReads, DoctorVisitRsnTypeObjWrites)
}

case class FamilyRltnTypeObj(FamilyRltnID: String, FamilyRltnDesc: String, ActiveIND: String)

object FamilyRltnTypeObj {
  val FamilyRltnTypeObjReads = (
    (__ \ "FamilyRltnID").read[String] and
    (__ \ "FamilyRltnDesc").read[String] and
    (__ \ "ActiveIND").read[String])(FamilyRltnTypeObj.apply _)

  val FamilyRltnTypeObjWrites = (
    (__ \ "FamilyRltnID").write[String] and
    (__ \ "FamilyRltnDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(FamilyRltnTypeObj.unapply))
  implicit val contactTypeFormat: Format[FamilyRltnTypeObj] = Format(FamilyRltnTypeObjReads, FamilyRltnTypeObjWrites)
}

case class OtherIDTypeObj(OtherIDTypeID: String, OtherIDTypeDesc: String, ActiveIND: String)

object OtherIDTypeObj {
  val OtherIDTypeObjReads = (
    (__ \ "OtherIDTypeID").read[String] and
    (__ \ "OtherIDTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(OtherIDTypeObj.apply _)

  val OtherIDTypeObjWrites = (
    (__ \ "OtherIDTypeID").write[String] and
    (__ \ "OtherIDTypeDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(OtherIDTypeObj.unapply))
  implicit val contactTypeFormat: Format[OtherIDTypeObj] = Format(OtherIDTypeObjReads, OtherIDTypeObjWrites)
}

case class PhoneTypeObj(PhoneTypeID: String, PhoneTypeDesc: String, ActiveIND: String)

object PhoneTypeObj {
  val PhoneTypeObjReads = (
    (__ \ "PhoneTypeID").read[String] and
    (__ \ "PhoneTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(PhoneTypeObj.apply _)

  val PhoneTypeObjWrites = (
    (__ \ "PhoneTypeID").write[String] and
    (__ \ "PhoneTypeDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(PhoneTypeObj.unapply))
  implicit val contactTypeFormat: Format[PhoneTypeObj] = Format(PhoneTypeObjReads, PhoneTypeObjWrites)
}

case class StateLkupObj(StateID: String, StateDesc: String,StateSEODesc:Option[String], ActiveIND: String, CountryID: String,isPrivate: Option[String])

object StateLkupObj {
  val StateLkupObjReads = (
    (__ \ "StateID").read[String] and
    (__ \ "StateDesc").read[String] and
    (__ \ "StateSEODesc").readNullable[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "CountryID").read[String] and
    (__ \ "isPrivate").readNullable[String])(StateLkupObj.apply _)

  val StateLkupObjWrites = (
    (__ \ "StateID").write[String] and
    (__ \ "StateDesc").write[String] and
    (__ \ "StateSEODesc").writeNullable[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "CountryID").write[String] and
    (__ \ "isPrivate").writeNullable[String])(unlift(StateLkupObj.unapply))
  implicit val contactLkupFormat: Format[StateLkupObj] = Format(StateLkupObjReads, StateLkupObjWrites)
}

case class UsrTypeObj(USRTypeID: String, USRTypeDesc: String, ActiveIND: String)

object UsrTypeObj {
  val UsrTypeObjReads = (
    (__ \ "USRTypeID").read[String] and
    (__ \ "USRTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(UsrTypeObj.apply _)

  val UsrTypeObjWrites = (
    (__ \ "USRTypeID").write[String] and
    (__ \ "USRTypeDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(UsrTypeObj.unapply))
  implicit val contactTypeFormat: Format[UsrTypeObj] = Format(UsrTypeObjReads, UsrTypeObjWrites)
}

case class MedicineTypeObj(MedicineTypeID: String, MedicineTypeDesc: String, ActiveIND: String)

object MedicineTypeObj {
  val MedicineTypeObjReads = (
    (__ \ "MedicineTypeID").read[String] and
    (__ \ "MedicineTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(MedicineTypeObj.apply _)

  val MedicineTypeObjWrites = (
    (__ \ "MedicineTypeID").write[String] and
    (__ \ "MedicineTypeDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(MedicineTypeObj.unapply))
  implicit val MedicineTypeObjReadsFormat: Format[MedicineTypeObj] = Format(MedicineTypeObjReads, MedicineTypeObjWrites)
}

case class AppointmentStatusObj(AppointmentStatusID: String, AppointmentStatusDesc: String, ActiveIND: String)

object AppointmentStatusObj {
  val AppointmentStatusObjReads = (
    (__ \ "AppointmentStatusID").read[String] and
    (__ \ "AppointmentStatusDesc").read[String] and
    (__ \ "ActiveIND").read[String])(AppointmentStatusObj.apply _)

  val AppointmentStatusObjWrites = (
    (__ \ "AppointmentStatusID").write[String] and
    (__ \ "AppointmentStatusDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(AppointmentStatusObj.unapply))
  implicit val AppointmentStatusFormat: Format[AppointmentStatusObj] = Format(AppointmentStatusObjReads, AppointmentStatusObjWrites)
}

case class VisitorTypeLkupObj(VisitUserTypeID: String, VisitUserTypeDesc: String, ActiveIND: String)

object VisitorTypeLkupObj {
  val VisitorTypeLkupObjReads = (
    (__ \ "VisitUserTypeID").read[String] and
    (__ \ "VisitUserTypeDesc").read[String] and
    (__ \ "ActiveIND").read[String])(VisitorTypeLkupObj.apply _)

  val VisitorTypeLkupObjWrites = (
    (__ \ "VisitUserTypeID").write[String] and
    (__ \ "VisitUserTypeDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(VisitorTypeLkupObj.unapply))
  implicit val VisitorTypeLkupFormat: Format[VisitorTypeLkupObj] = Format(VisitorTypeLkupObjReads, VisitorTypeLkupObjWrites)
}

case class CityLkupObj(CityID: String, CityIDDesc: String,CitySEODesc:Option[String], ActiveIND: String,StateID:String,isPrivate: Option[String])

object CityLkupObj {
  val CityLkupObjReads = (
    (__ \ "CityID").read[String] and
    (__ \ "CityIDDesc").read[String] and
    (__ \ "CitySEODesc").readNullable[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "StateID").read[String] and
    (__ \ "isPrivate").readNullable[String])(CityLkupObj.apply _)

  val CityLkupObjWrites = (
    (__ \ "CityID").write[String] and
    (__ \ "CityIDDesc").write[String] and
    (__ \ "CitySEODesc").writeNullable[String] and
    (__ \ "ActiveIND").write[String] and 
    (__ \ "StateID").write[String] and
    (__ \ "isPrivate").writeNullable[String])(unlift(CityLkupObj.unapply))
  implicit val CityLkupFormat: Format[CityLkupObj] = Format(CityLkupObjReads, CityLkupObjWrites)
}

case class LocationLkupObj(LocationID: String, LocationIDDesc: String, CityID: String,LocationSEODesc: Option[String], ActiveIND: String,isPrivate: Option[String])

object LocationLkupObj {
  val LocationLkupObjReads = (
    (__ \ "LocationID").read[String] and
    (__ \ "LocationIDDesc").read[String] and
    (__ \ "CityID").read[String] and
    (__ \ "LocationSEODesc").readNullable[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "isPrivate").readNullable[String])(LocationLkupObj.apply _)

  val LocationLkupObjWrites = (
    (__ \ "LocationID").write[String] and
    (__ \ "LocationIDDesc").write[String] and
    (__ \ "CityID").write[String] and
    (__ \ "LocationSEODesc").writeNullable[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "isPrivate").writeNullable[String])(unlift(LocationLkupObj.unapply))
  implicit val CityLkupFormat: Format[LocationLkupObj] = Format(LocationLkupObjReads, LocationLkupObjWrites)
}

case class AdminLookup(hcueAccessLkup: Array[HcueAccessLookup], hcueRoleLkup: Array[HcueRoleLookup], hCueAccountLkup: Array[HCueAccountLookup], hcueAccountAccessLkup: Array[HcueAccountAccessLookup],
    hcueHospitalGrpSettings: Array[HcueHospitalGrpSettings],hcueHospitalRoleSettings: Array[HcueHospitalRoleSettings],hcueHospitalInfo: Array[HcueHospitalInfo],incentiveMappingInfo: Option[Array[IncentiveMappingInfo]], patientRegistration: Option[JsValue])

object AdminLookup {
  val AdminLookupReads = (
    (__ \ "hcueAccessLkup").read[Array[HcueAccessLookup]] and
    (__ \ "hcueRoleLkup").read[Array[HcueRoleLookup]] and
    (__ \ "hCueAccountLkup").read[Array[HCueAccountLookup]] and
    (__ \ "hcueAccountAccessLkup").read[Array[HcueAccountAccessLookup]] and 
    (__ \ "hcueHospitalGrpSettings").read[Array[HcueHospitalGrpSettings]] and 
    (__ \ "hcueHospitalRoleSettings").read[Array[HcueHospitalRoleSettings]] and
    (__ \ "hcueHospitalInfo").read[Array[HcueHospitalInfo]] and
        (__ \ "incentiveMappingInfo").readNullable[Array[IncentiveMappingInfo]] and
    (__ \ "patientRegistration").readNullable[JsValue])(AdminLookup.apply _)

  val AdminLookupWrites = (
    (__ \ "hcueAccessLkup").write[Array[HcueAccessLookup]] and
    (__ \ "hcueRoleLkup").write[Array[HcueRoleLookup]] and
    (__ \ "hCueAccountLkup").write[Array[HCueAccountLookup]] and
    (__ \ "hcueAccountAccessLkup").write[Array[HcueAccountAccessLookup]] and 
    (__ \ "hcueHospitalGrpSettings").write[Array[HcueHospitalGrpSettings]] and 
    (__ \ "hcueHospitalRoleSettings").write[Array[HcueHospitalRoleSettings]] and
    (__ \ "hcueHospitalInfo").write[Array[HcueHospitalInfo]] and 
        (__ \ "incentiveMappingInfo").writeNullable[Array[IncentiveMappingInfo]] and 
    (__ \ "patientRegistration").writeNullable[JsValue])(unlift(AdminLookup.unapply))
  implicit val AdminLookupFormat: Format[AdminLookup] = Format(AdminLookupReads, AdminLookupWrites)
}



case class IncentiveMappingInfo(TreatmentType: String, incentiveRecordInfo: Array[IncentiveRecordInfo])

object IncentiveMappingInfo {
  val IncentiveMappingInfoReads = (
    (__ \ "TreatmentType").read[String] and
    (__ \ "incentiveRecordInfo").read[Array[IncentiveRecordInfo]] )(IncentiveMappingInfo.apply _)

  val IncentiveMappingInfoWrites = (
    (__ \ "TreatmentType").write[String] and
    (__ \ "incentiveRecordInfo").write[Array[IncentiveRecordInfo]] )(unlift(IncentiveMappingInfo.unapply))
  implicit val IncentiveMappingInfoFormat: Format[IncentiveMappingInfo] = Format(IncentiveMappingInfoReads, IncentiveMappingInfoWrites)
}


case class IncentiveRecordInfo(IncentiveID: Long, IncentiveType: String)

object IncentiveRecordInfo {
  val IncentiveRecordInfoReads = (
    (__ \ "IncentiveID").read[Long] and
    (__ \ "IncentiveType").read[String] )(IncentiveRecordInfo.apply _)

  val IncentiveRecordInfoWrites = (
    (__ \ "IncentiveID").write[Long] and
    (__ \ "IncentiveType").write[String] )(unlift(IncentiveRecordInfo.unapply))
  implicit val IncentiveRecordInfoFormat: Format[IncentiveRecordInfo] = Format(IncentiveRecordInfoReads, IncentiveRecordInfoWrites)
}







case class HcueAccountHeadsLkup(DoctorID: Option[Long], AddressID: Option[Long], HospitalID: Option[Long],
                                ParentHospitalID: Option[Long], HospitalCD: Option[String], AccountHeadID: Long,
                                AccountHeadDesc: Option[String], Type: String, ActiveIND: String)

object HcueAccountHeadsLkup {

  val HcueAccountHeadsLkupReads = (
    (__ \ "DoctorID").readNullable[Long] and
    (__ \ "AddressID").readNullable[Long] and
    (__ \ "HospitalID").readNullable[Long] and
    (__ \ "ParentHospitalID").readNullable[Long] and
    (__ \ "HospitalCD").readNullable[String] and
    (__ \ "AccountHeadID").read[Long] and
    (__ \ "AccountHeadDesc").readNullable[String] and
    (__ \ "Type").read[String] and
    (__ \ "ActiveIND").read[String])(HcueAccountHeadsLkup.apply _)

  val HcueAccountHeadsLkupWrites = (
    (__ \ "DoctorID").writeNullable[Long] and
    (__ \ "AddressID").writeNullable[Long] and
    (__ \ "HospitalID").writeNullable[Long] and
    (__ \ "ParentHospitalID").writeNullable[Long] and
    (__ \ "HospitalCD").writeNullable[String] and
    (__ \ "AccountHeadID").write[Long] and
    (__ \ "AccountHeadDesc").writeNullable[String] and
    (__ \ "Type").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(HcueAccountHeadsLkup.unapply))

  implicit val HcueAccountHeadsLkupFormat: Format[HcueAccountHeadsLkup] = Format(HcueAccountHeadsLkupReads, HcueAccountHeadsLkupWrites)
}

case class HcueAccessLookup(AccessID: String, AccessDesc: String, ActiveIND: String, ParentAccessID: Option[String])

object HcueAccessLookup {
  val HcueAccessLookupReads = (
    (__ \ "AccessID").read[String] and
    (__ \ "AccessDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "ParentAccessID").readNullable[String])(HcueAccessLookup.apply _)

  val HcueAccessLookupWrites = (
    (__ \ "AccessID").write[String] and
    (__ \ "AccessDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "ParentAccessID").writeNullable[String])(unlift(HcueAccessLookup.unapply))
  implicit val HcueAccessLookupFormat: Format[HcueAccessLookup] = Format(HcueAccessLookupReads, HcueAccessLookupWrites)
}


case class HcueRoleLookup(RoleID: String, RoleDesc: String, ActiveIND: String, IsPrivate: String,HospitalID : Option[Long])

object HcueRoleLookup {
  val HcueRoleLookupReads = (
    (__ \ "RoleID").read[String] and
    (__ \ "RoleDesc").read[String] and
    (__ \ "ActiveIND").read[String] and
    (__ \ "IsPrivate").read[String] and
    (__ \ "HospitalID").readNullable[Long])(HcueRoleLookup.apply _)

  val HcueRoleLookupWrites = (
    (__ \ "RoleID").write[String] and
    (__ \ "RoleDesc").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "IsPrivate").write[String] and
    (__ \ "HospitalID").writeNullable[Long])(unlift(HcueRoleLookup.unapply))
  implicit val HcueRoleLookupFormat: Format[HcueRoleLookup] = Format(HcueRoleLookupReads, HcueRoleLookupWrites)
}

case class HCueAccountLookup(AccountID: String, AccountDesc: String, ActiveIND : String)

object HCueAccountLookup {
  val HCueAccountLookupReads = (
    (__ \ "AccountID").read[String] and
    (__ \ "AccountDesc").read[String] and
    (__ \ "ActiveIND").read[String])(HCueAccountLookup.apply _)

  val HCueAccountLookupWrites = (
    (__ \ "AccountID").write[String] and
    (__ \ "AccountDesc").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(HCueAccountLookup.unapply))
  implicit val HCueAccountLookupFormat: Format[HCueAccountLookup] = Format(HCueAccountLookupReads, HCueAccountLookupWrites)
}

case class HcueAccountAccessLookup(AcntAccessID: String, AccessID: String, AccountID: Map[String, String], ActiveIND: String)

object HcueAccountAccessLookup {
  val HcueAccountAccessLookupReads = (
    (__ \ "AcntAccessID").read[String] and
    (__ \ "AccessID").read[String] and
    (__ \ "AccountID").read[Map[String, String]] and
    (__ \ "ActiveIND").read[String] )(HcueAccountAccessLookup.apply _)

  val HcueAccountAccessLookupWrites = (
    (__ \ "AcntAccessID").write[String] and
    (__ \ "AccessID").write[String] and
    (__ \ "AccountID").write[Map[String, String]] and
    (__ \ "ActiveIND").write[String] )(unlift(HcueAccountAccessLookup.unapply))
  implicit val HcueAccountAccessLookupFormat: Format[HcueAccountAccessLookup] = Format(HcueAccountAccessLookupReads, HcueAccountAccessLookupWrites)
}

case class HcueHospitalGrpSettings(GroupCode: Long, GroupName: Option[String], HospitalID: Long, AccountAccessID : Map[String, String],ActiveIND: String)

object HcueHospitalGrpSettings {
  val HcueHospitalGrpSettingsReads = (
    (__ \ "GroupCode").read[Long] and
    (__ \ "GroupName").readNullable[String] and
    (__ \ "HospitalID").read[Long] and
    (__ \ "AccountAccessID").read[Map[String, String]] and
    (__ \ "ActiveIND").read[String])(HcueHospitalGrpSettings.apply _)

  val HcueHospitalGrpSettingsWrites = (
    (__ \ "GroupCode").write[Long] and
    (__ \ "GroupName").writeNullable[String] and
    (__ \ "HospitalID").write[Long] and
    (__ \ "AccountAccessID").write[Map[String, String]] and
    (__ \ "ActiveIND").write[String])(unlift(HcueHospitalGrpSettings.unapply))
  implicit val HcueHospitalGrpSettingsFormat: Format[HcueHospitalGrpSettings] = Format(HcueHospitalGrpSettingsReads, HcueHospitalGrpSettingsWrites)
}

case class HcueHospitalRoleSettings(HospitalID: Long, RoleID : String, ActiveIND: String)

object HcueHospitalRoleSettings {
  val HcueHospitalRoleSettingsReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "RoleID").read[String] and
    (__ \ "ActiveIND").read[String])(HcueHospitalRoleSettings.apply _)

  val HcueHospitalRoleSettingsWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "RoleID").write[String] and
    (__ \ "ActiveIND").write[String])(unlift(HcueHospitalRoleSettings.unapply))
  implicit val HcueHospitalGrpSettingsFormat: Format[HcueHospitalRoleSettings] = Format(HcueHospitalRoleSettingsReads, HcueHospitalRoleSettingsWrites)
}

case class HcueHospitalInfo(HospitalID: Long, HospitalName : String, ActiveIND: String, ParentHospitalFlag: Option[String],
    HospitalDoctorInfo : Array[HcueHospitalDoctorInfo],Preference:Option[Map[String,String]])

object HcueHospitalInfo {
  val HcueHospitalInfoReads = (
    (__ \ "HospitalID").read[Long] and
    (__ \ "HospitalName").read[String] and
    (__ \ "ActiveIND").read[String] and 
    (__ \ "ParentHospitalFlag").readNullable[String] and 
    (__ \ "HospitalDoctorInfo").read[ Array[HcueHospitalDoctorInfo]] and
    (__ \ "Preference").readNullable[Map[String,String]])(HcueHospitalInfo.apply _)

  val HcueHospitalInfoWrites = (
    (__ \ "HospitalID").write[Long] and
    (__ \ "HospitalName").write[String] and
    (__ \ "ActiveIND").write[String] and
    (__ \ "ParentHospitalFlag").writeNullable[String] and 
    (__ \ "HospitalDoctorInfo").write[ Array[HcueHospitalDoctorInfo]] and
    (__ \ "Preference").writeNullable[Map[String,String]])(unlift(HcueHospitalInfo.unapply))
  implicit val HcueHospitalInfoFormat: Format[HcueHospitalInfo] = Format(HcueHospitalInfoReads, HcueHospitalInfoWrites)
}

case class HcueHospitalDoctorInfo(DoctorID : Long, DoctorName : String, RoleID: Option[String], GroupID: Option[String] )

object HcueHospitalDoctorInfo {
  val HcueHospitalDoctorInfoReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "DoctorName").read[String] and
   (__ \ "RoleID").readNullable[String] and
   (__ \ "GroupID").readNullable[String])(HcueHospitalDoctorInfo.apply _)

  val HcueHospitalDoctorInfoWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "DoctorName").write[String] and
     (__ \ "RoleID").writeNullable[String] and
     (__ \ "GroupID").writeNullable[String])(unlift(HcueHospitalDoctorInfo.unapply))
  implicit val HcueHospitalDoctorInfoFormat: Format[HcueHospitalDoctorInfo] = Format(HcueHospitalDoctorInfoReads, HcueHospitalDoctorInfoWrites)
}


case class allergiesListObj(AllergensID : Long, AllergensDesc : String)

object allergiesListObj {
  val allergiesListObjReads = (
     (__ \ "AllergensID").read[Long] and
     (__ \ "AllergensDesc").read[String])(allergiesListObj.apply _)

  val allergiesListObjWrites = (
     (__ \ "AllergensID").write[Long] and
     (__ \ "AllergensDesc").write[String])(unlift(allergiesListObj.unapply))
  implicit val allergiesListObjFormat: Format[allergiesListObj] = Format(allergiesListObjReads, allergiesListObjWrites)
}


case class responseObj(status: String, response : Option[JsValue], error: Option[errorObj])

object responseObj {
  val responseObjReads = (
     (__ \ "status").read[String] and
     (__ \ "response").readNullable[JsValue] and
     (__ \ "error").readNullable[errorObj])(responseObj.apply _)

  val responseObjWrites = (
     (__ \ "status").write[String] and
     (__ \ "response").writeNullable[JsValue] and
     (__ \ "error").writeNullable[errorObj])(unlift(responseObj.unapply))
  implicit val responseObjFormat: Format[responseObj] = Format(responseObjReads, responseObjWrites)
}

case class errorObj(message: String, messageCode : String)

object errorObj {
  val errorObjReads = (
     (__ \ "message").read[String] and
     (__ \ "messageCode").read[String])(errorObj.apply _)

  val errorObjWrites = (
     (__ \ "message").write[String] and
     (__ \ "messageCode").write[String])(unlift(errorObj.unapply))
  implicit val responseObjFormat: Format[errorObj] = Format(errorObjReads, errorObjWrites)
}

case class docPatObj(patientId: Long, doctorId : List[Long], hospitalCd: List[String])

object docPatObj {
  val docPatObjReads = (
     (__ \ "patientId").read[Long] and
     (__ \ "doctorId").read[List[Long]]  and
     (__ \ "hospitalCd").read[List[String]])(docPatObj.apply _)

  val docPatObjWrites = (
     (__ \ "patientId").write[Long] and
     (__ \ "doctorId").write[List[Long]] and
     (__ \ "hospitalCd").write[List[String]])(unlift(docPatObj.unapply))
}
