/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import play.api.data.validation._
import play.api.libs.functional.syntax._




case class hospitalDetailsObj(HospitalID: Long, ParentHospitalID: Option[Long],HospitalName: Option[String], ContactName: Option[String],
                            About: Option[String],  RegsistrationDate: Option[java.sql.Date],
                                      ActiveIND: String, TermsAccepted: String)

object hospitalDetailsObj {
  val hospitalDetailsObjReads = ((__ \ "HospitalID").read[Long] and (__ \ "ParentHospitalID").readNullable[Long] and (__ \ "HospitalName").readNullable[String] and (__ \ "ContactName").readNullable[String] and
    (__ \ "About").readNullable[String] and (__ \ "RegsistrationDate").readNullable[java.sql.Date] and 
    (__ \ "ActiveIND").read[String] and (__ \ "TermsAccepted").read[String])(hospitalDetailsObj.apply _)
 
  val hospitalDetailsObjWrites = ((__ \ "HospitalID").write[Long] and (__ \ "ParentHospitalID").writeNullable[Long] and (__ \ "HospitalName").writeNullable[String] and (__ \ "ContactName").writeNullable[String] and
    (__ \ "About").writeNullable[String] and (__ \ "RegsistrationDate").writeNullable[java.sql.Date] and 
    (__ \ "ActiveIND").write[String] and (__ \ "TermsAccepted").write[String])(unlift(hospitalDetailsObj.unapply))
  implicit val hospitalDetailsObjFormat: Format[hospitalDetailsObj] = Format(hospitalDetailsObjReads, hospitalDetailsObjWrites)
}

case class hospitalAddressObj(Address1: Option[String], Address2: Option[String], Street: Option[String], Location: Option[String],
                            CityTown: Option[String], DistrictRegion: Option[String], State: Option[String], PostCode: Option[String],
                            Country: Option[String], LandMark: Option[String], Latitude: Option[String], Longitude: Option[String])

object hospitalAddressObj {
  val hospitalAddressObjReads = ((__ \ "Address1").readNullable[String] and
    (__ \ "Address2").readNullable[String] and (__ \ "Street").readNullable[String] and (__ \ "Location").readNullable[String] and
    (__ \ "CityTown").readNullable[String] and (__ \ "DistrictRegion").readNullable[String] and (__ \ "State").readNullable[String] and
    (__ \ "PostCode").readNullable[String] and (__ \ "Country").readNullable[String] and  (__ \ "LandMark").readNullable[String] and
    (__ \ "Latitude").readNullable[String] and (__ \ "Longitude").readNullable[String])(hospitalAddressObj.apply _)

  val hospitalAddressObjWrites = ((__ \ "Address1").writeNullable[String] and
    (__ \ "Address2").writeNullable[String] and (__ \ "Street").writeNullable[String] and (__ \ "Location").writeNullable[String] and
    (__ \ "CityTown").writeNullable[String] and (__ \ "DistrictRegion").writeNullable[String] and (__ \ "State").writeNullable[String] and
    (__ \ "PostCode").writeNullable[String] and (__ \ "Country").writeNullable[String] and (__ \ "LandMark").writeNullable[String] and
    (__ \ "Latitude").writeNullable[String] and (__ \ "Longitude").writeNullable[String])(unlift(hospitalAddressObj.unapply))
  implicit val pharmaPartnerObjFormat: Format[hospitalAddressObj] = Format(hospitalAddressObjReads, hospitalAddressObjWrites)
}


case class hospitalPartnerObj(HospitalDetails: hospitalDetailsObj, HospitalAddress: Option[hospitalAddressObj])

object hospitalPartnerObj {
  val hospitalPartnerObjReads = (
    (__ \ "HospitalDetails").read[hospitalDetailsObj] and 
    (__ \ "HospitalAddress").readNullable[hospitalAddressObj])(hospitalPartnerObj.apply _)

  val hospitalPartnerObjWrites = (
    (__ \ "HospitalDetails").write[hospitalDetailsObj] and
    (__ \ "HospitalAddress").writeNullable[hospitalAddressObj])(unlift(hospitalPartnerObj.unapply))
  implicit val hospitalPartnerObjFormat: Format[hospitalPartnerObj] = Format(hospitalPartnerObjReads, hospitalPartnerObjWrites)
}




