/* Contains Json constructor and implicit Read Write for add Update patient Request and its Validation constrains*/
package models.com.hcue.doctors.Json.Response

import scala.slick.driver.PostgresDriver.simple._
import play.api.libs.json._
import java.util.Date
import java.sql.Date

import play.api.libs.functional.syntax._

import models.com.hcue.doctors.Json.Request.patientEmailDetailsObj
import models.com.hcue.doctors.Json.Request.patientPhoneDetailsObj
import models.com.hcue.doctors.Json.Request.patientAddressDetailsObj
import models.com.hcue.doctors.Json.Request.patientOtherIdDetailsObj
import models.com.hcue.doctors.Json.Request.patientCaseBillingAmtInfo
import models.com.hcue.doctors.Json.Response.Doctor.Consultation.doctorConsultLst
import models.com.hcue.doctors.Json.Request.GroupFiltersAppliedObj


case class SearchOutPutObj(DoctorID: Long, FirstName: String, LastName: Option[String], FullName: String, SpecialityCD: String,
          Exp: Option[Int], ClinicName: Option[String],Address1: String, Address2: Option[String], Street: Option[String], Location: String, CityTown: String, 
          DistrictRegion: Option[String], State: Option[String],PinCode: Int, Country: String,Fee: Option[Int])

object SearchOutPutObj {
  val SearchOutPutObjReads = (
      (__ \ "DoctorID").read[Long] and 
      (__ \ "FirstName").read[String] and 
      (__ \ "LastName").readNullable[String] and
      (__ \ "FullName").read[String] and
      (__ \ "SpecialityCD").read[String] and
      (__ \ "Exp").readNullable[Int] and
      (__ \ "ClinicName").readNullable[String] and
      (__ \ "Address1").read[String] and
      (__ \ "Address2").readNullable[String] and
      (__ \ "Street").readNullable[String] and
      (__ \ "Location").read[String] and
      (__ \ "CityTown").read[String] and 
      (__ \ "DistrictRegion").readNullable[String] and
      (__ \ "State").readNullable[String] and
      (__ \ "PinCode").read[Int] and
      (__ \ "Country").read[String] and
      (__ \ "Fee").readNullable[Int])(SearchOutPutObj.apply _)

  val SearchOutPutObjWrites = (
      (__ \ "DoctorID").write[Long] and 
      (__ \ "FirstName").write[String] and 
      (__ \ "LastName").writeNullable[String] and
      (__ \ "FullName").write[String] and
      (__ \ "SpecialityCD").write[String] and
      (__ \ "Exp").writeNullable[Int] and
      (__ \ "ClinicName").writeNullable[String] and
      (__ \ "Address1").write[String] and
      (__ \ "Address2").writeNullable[String] and
      (__ \ "Street").writeNullable[String] and
      (__ \ "Location").write[String] and
      (__ \ "CityTown").write[String] and 
      (__ \ "DistrictRegion").writeNullable[String] and
      (__ \ "State").writeNullable[String] and
      (__ \ "PinCode").write[Int] and
      (__ \ "Country").write[String] and
      (__ \ "Fee").writeNullable[Int])(unlift(SearchOutPutObj.unapply))

  implicit val SearchOutPutObjFormat: Format[SearchOutPutObj] = Format(SearchOutPutObjReads, SearchOutPutObjWrites)
}

case class buildResponseObj(Patient: Array[PatientDetailsObj], PatientPhone: Array[patientPhoneDetailsObj],
                            PatientAddress: Array[patientAddressDetailsObj], PatientEmail: Array[patientEmailDetailsObj],
                            PatientOtherIds: Array[patientOtherIdDetailsObj],patientCaseBillingAmtInfo : Option[Array[patientCaseBillingAmtInfo]])

object buildResponseObj {
  val buildResponseObjReads = (
    (__ \ "Patient").read[Array[PatientDetailsObj]] and
    (__ \ "PatientPhone").read[Array[patientPhoneDetailsObj]] and
    (__ \ "PatientAddress").read[Array[patientAddressDetailsObj]] and
    (__ \ "PatientEmail").read[Array[patientEmailDetailsObj]] and
    (__ \ "PatientOtherIds").read[Array[patientOtherIdDetailsObj]] and
    (__ \ "patientCaseBillingAmtInfo").readNullable[Array[patientCaseBillingAmtInfo]])(buildResponseObj.apply _)

  val buildResponseObjWrites = (
    (__ \ "Patient").write[Array[PatientDetailsObj]] and
    (__ \ "PatientPhone").write[Array[patientPhoneDetailsObj]] and
    (__ \ "PatientAddress").write[Array[patientAddressDetailsObj]] and
    (__ \ "PatientEmail").write[Array[patientEmailDetailsObj]] and
    (__ \ "PatientOtherIds").write[Array[patientOtherIdDetailsObj]] and
    (__ \ "patientCaseBillingAmtInfo").writeNullable[Array[patientCaseBillingAmtInfo]])(unlift(buildResponseObj.unapply))

  implicit val buildResponseFormat: Format[buildResponseObj] = Format(buildResponseObjReads, buildResponseObjWrites)
}


case class currentAgeObj(year: Int, month:Int,day:Int)

object currentAgeObj {
  val currentAgeObjReads = (
    (__ \ "year").read[Int] and
    (__ \ "month").read[Int] and
    (__ \ "day").read[Int])(currentAgeObj.apply _)

  val currentAgeObjWrites = (
    (__ \ "year").write[Int] and
    (__ \ "month").write[Int] and
    (__ \ "day").write[Int])(unlift(currentAgeObj.unapply))

  implicit val buildResponseFormat: Format[currentAgeObj] = Format(currentAgeObjReads, currentAgeObjWrites)
}

case class PatientDetailsObj(Title: Option[String], FirstName: String, LastName: Option[String], FullName: String, PatientID: Long, AadhaarID: Option[Int], Gender: String,
                             DOB: Option[java.sql.Date], Age: Option[Double],CurrentAge:Option[currentAgeObj], FamilyHdIND: Option[String], FamilyHdID: Option[Long],
                             FamilyRltnType: Option[String], PatientLoginID: Option[String],CrtUSR: Long,
                             CrtUSRType: String, UpdtUSR: Option[Long], UpdtUSRType: Option[String])

object PatientDetailsObj {
  val PatientDetailsObjReads = (

    (__ \ "Title").readNullable[String] and
    (__ \ "FirstName").read[String] and
    (__ \ "LastName").readNullable[String] and
    (__ \ "FullName").read[String] and
    (__ \ "PatientID").read[Long] and
    (__ \ "AadhaarID").readNullable[Int] and
    (__ \ "Gender").read[String] and
    (__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "Age").readNullable[Double] and
    (__ \ "CurrentAge").readNullable[currentAgeObj] and
    (__ \ "FamilyHdIND").readNullable[String] and
    (__ \ "FamilyHdID").readNullable[Long] and
    (__ \ "FamilyRltnType").readNullable[String] and
    (__ \ "PatientLoginID").readNullable[String] and
    (__ \ "CrtUSR").read[Long] and
    (__ \ "CrtUSRType").read[String] and
    (__ \ "UpdtUSR").readNullable[Long] and
    (__ \ "UpdtUSRType").readNullable[String])(PatientDetailsObj.apply _)

  val PatientDetailsObjWrites = (
    
    (__ \ "Title").writeNullable[String] and  
    (__ \ "FirstName").write[String] and
    (__ \ "LastName").writeNullable[String] and
    (__ \ "FullName").write[String] and
    (__ \ "PatientID").write[Long] and
    (__ \ "AadhaarID").writeNullable[Int] and
    (__ \ "Gender").write[String] and
    (__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "Age").writeNullable[Double] and
    (__ \ "CurrentAge").writeNullable[currentAgeObj] and
    (__ \ "FamilyHdIND").writeNullable[String] and
    (__ \ "FamilyHdID").writeNullable[Long] and
    (__ \ "FamilyRltnType").writeNullable[String] and
    (__ \ "PatientLoginID").writeNullable[String] and
    (__ \ "CrtUSR").write[Long] and
    (__ \ "CrtUSRType").write[String] and
    (__ \ "UpdtUSR").writeNullable[Long] and
    (__ \ "UpdtUSRType").writeNullable[String])(unlift(PatientDetailsObj.unapply))
  implicit val PatientDetailsFormat: Format[PatientDetailsObj] = Format(PatientDetailsObjReads, PatientDetailsObjWrites)
}


case class DoctorDetailObj(DoctorID: Long, FullName: String, AddressID: Long, Active:String, Gender:Option[String],Available: String,SpecialityCD: Option[Map[String, String]],ImageURL:Option[String],MinPerCase: Option[Int], AppointmentCount:Option[Int],AppointmentDetails:Option[Array[doctorConsultLst]])

object DoctorDetailObj {
  val DoctorDetailObjReads = (
    (__ \ "DoctorID").read[Long] and
    (__ \ "FullName").read[String] and
    (__ \ "AddressID").read[Long] and
    (__ \ "Active").read[String] and
    (__ \ "Gender").readNullable[String] and
    (__ \ "Available").read[String] and
    (__ \ "SpecialityCD").readNullable[Map[String, String]] and
    (__ \ "ImageURL").readNullable[String] and
    (__ \ "MinPerCase").readNullable[Int] and
    (__ \ "AppointmentCount").readNullable[Int] and
    (__ \ "AppointmentDetails").readNullable[Array[doctorConsultLst]])(DoctorDetailObj.apply _)

  val DoctorDetailObjWrites = (
    (__ \ "DoctorID").write[Long] and
    (__ \ "FullName").write[String] and
    (__ \ "AddressID").write[Long] and
    (__ \ "Active").write[String] and
    (__ \ "Gender").writeNullable[String] and
    (__ \ "Available").write[String] and
    (__ \ "SpecialityCD").writeNullable[Map[String, String]] and
    (__ \ "ImageURL").writeNullable[String] and
    (__ \ "MinPerCase").writeNullable[Int] and
    (__ \ "AppointmentCount").writeNullable[Int] and
    (__ \ "AppointmentDetails").writeNullable[Array[doctorConsultLst]])(unlift(DoctorDetailObj.unapply))
  implicit val DoctorDetailObjFormat: Format[DoctorDetailObj] = Format(DoctorDetailObjReads, DoctorDetailObjWrites)
}


case class PatientGroupDetailObj(GroupID: Long, GroupDesc: String, ActiveIND: Option[String], MessageType: String, 
    GroupFilters: Option[GroupFiltersAppliedObj], PatientsCount: Int, PatientPhNumCount: Int, PatientEmailCount: Int, PatientCountInCommon: Int)

object PatientGroupDetailObj {
  val PatientGroupDetailObjReads = (
    (__ \ "GroupID").read[Long] and
    (__ \ "GroupDesc").read[String] and
     (__ \ "ActiveIND").readNullable[String] and
     (__ \ "MessageType").read[String] and
    (__ \ "GroupFilters").readNullable[GroupFiltersAppliedObj] and
    (__ \ "PatientsCount").read[Int]   and
    (__ \ "PatientPhNumCount").read[Int]  and
    (__ \ "PatientEmailCount").read[Int]  and
    (__ \ "PatientCountInCommon").read[Int])(PatientGroupDetailObj.apply _)

  val PatientGroupDetailObjWrites = (
    (__ \ "GroupID").write[Long] and
    (__ \ "GroupDesc").write[String] and
    (__ \ "ActiveIND").writeNullable[String] and
     (__ \ "MessageType").write[String] and
    (__ \ "GroupFilters").writeNullable[GroupFiltersAppliedObj] and
    (__ \ "PatientsCount").write[Int]   and
    (__ \ "PatientPhNumCount").write[Int]  and
    (__ \ "PatientEmailCount").write[Int]  and
    (__ \ "PatientCountInCommon").write[Int])(unlift(PatientGroupDetailObj.unapply))
  implicit val PatientGroupDetailObjFormat: Format[PatientGroupDetailObj] = Format(PatientGroupDetailObjReads, PatientGroupDetailObjWrites)
}


case class buildPatientSearchResponseObj(PatientID: Long,ProfileImg:Option[String], Title: Option[String], FullName:String,CurrentAge:Option[currentAgeObj],
    Gender:String,LastVisited:Option[java.sql.Date],PhoneNumber:Option[Long], EmailID:Option[String],DisplayID:Option[String],FamilyDisplayID:Option[String],
    Pid: Option[Long], FamilyHdID: Option[Long], patientCaseBillingAmtInfo: Option[Array[patientCaseBillingAmtInfo]], 
    ReferralSource : Option[ReferralSourceObj],vipFlag:Option[String],ContactNumber: Option[String])

object buildPatientSearchResponseObj {
  val buildPatientSearchResponseObjReads = (
    (__ \ "PatientID").read[Long] and
    (__ \ "ProfileImg").readNullable[String] and
    (__ \ "Title").readNullable[String] and
    (__ \ "FullName").read[String] and
    (__ \ "CurrentAge").readNullable[currentAgeObj] and
    //(__ \ "DOB").readNullable[java.sql.Date] and
    (__ \ "Gender").read[String] and
    (__ \ "LastVisited").readNullable[java.sql.Date] and
    (__ \ "PhoneNumber").readNullable[Long] and
    (__ \ "EmailID").readNullable[String] and
    (__ \ "DisplayID").readNullable[String] and 
    (__ \ "FamilyDisplayID").readNullable[String] and 
    (__ \ "Pid").readNullable[Long] and
    (__ \ "FamilyHdID").readNullable[Long] and
    (__ \ "patientCaseBillingAmtInfo").readNullable[Array[patientCaseBillingAmtInfo]] and
    (__ \ "ReferralSource").readNullable[ReferralSourceObj]and
    (__ \ "vipFlag").readNullable[String] and
    (__ \ "ContactNumber").readNullable[String])(buildPatientSearchResponseObj.apply _)

  val buildPatientSearchResponseObjWrites = (
    (__ \ "PatientID").write[Long] and
    (__ \ "ProfileImg").writeNullable[String] and
    (__ \ "Title").writeNullable[String] and
    (__ \ "FullName").write[String] and
    (__ \ "CurrentAge").writeNullable[currentAgeObj] and
    //(__ \ "DOB").writeNullable[java.sql.Date] and
    (__ \ "Gender").write[String] and
    (__ \ "LastVisited").writeNullable[java.sql.Date] and
    (__ \ "PhoneNumber").writeNullable[Long] and 
    (__ \ "EmailID").writeNullable[String] and
    (__ \ "DisplayID").writeNullable[String] and
    (__ \ "FamilyDisplayID").writeNullable[String] and 
    (__ \ "Pid").writeNullable[Long] and
    (__ \ "FamilyHdID").writeNullable[Long] and
    (__ \ "patientCaseBillingAmtInfo").writeNullable[Array[patientCaseBillingAmtInfo]] and
    (__ \ "ReferralSource").writeNullable[ReferralSourceObj]and
     (__ \ "vipFlag").writeNullable[String] and
     (__ \ "ContactNumber").writeNullable[String])(unlift(buildPatientSearchResponseObj.unapply))

  implicit val buildResponseFormat: Format[buildPatientSearchResponseObj] = Format(buildPatientSearchResponseObjReads, buildPatientSearchResponseObjWrites)
}


case class ReferralSourceObj(ReferralID: Option[Long], SubReferralID: Option[Long])

object ReferralSourceObj {
  val ReferralSourceObjReads = (
     (__ \ "ReferralID").readNullable[Long] and
    (__ \ "SubReferralID").readNullable[Long])(ReferralSourceObj.apply _)

  val ReferralSourceObjWrites = (
    (__ \ "ReferralID").writeNullable[Long] and
    (__ \ "SubReferralID").writeNullable[Long])(unlift(ReferralSourceObj.unapply))
  implicit val PatientGroupDetailObjFormat: Format[ReferralSourceObj] = Format(ReferralSourceObjReads, ReferralSourceObjWrites)
}

