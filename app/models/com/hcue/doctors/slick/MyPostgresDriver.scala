package  models.com.hcue.doctors.slick
 
import slick.driver.PostgresDriver
import com.github.tminglei.slickpg._

trait MyPostgresDriver extends ExPostgresDriver
                          with PgArraySupport
                          //with PgDateSupport
                          with PgRangeSupport
                          with PgDateSupportJoda 
                          with PgHStoreSupport
                          with PgPlayJsonSupport
                          with PgSearchSupport
                          with PgPostGISSupport {
  def pgjson = "jsonb" // jsonb support is in postgres 9.4.0 onward; for 9.3.x use "json" {
 
  override lazy val Implicit = new ImplicitsPlus {}
  override val simple = new SimpleQLPlus {}
 
  trait ImplicitsPlus extends Implicits
                           with SearchImplicits
                           
                           with PostGISImplicits
                           with ArrayImplicits
                           with DateTimeImplicits
                           with JsonImplicits
                           with RangeImplicits
                           with HStoreImplicits
                           with SearchAssistants
 
  trait SimpleQLPlus extends SimpleQL
    with ImplicitsPlus
    with SearchAssistants
    with PostGISAssistants
}
 
object MyPostgresDriver extends MyPostgresDriver
 