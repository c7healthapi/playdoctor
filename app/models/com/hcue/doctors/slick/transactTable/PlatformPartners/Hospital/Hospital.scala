package models.com.hcue.doctors.slick.transactTable.PlatformPartners.Hospital

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THospital(
  HospitalID:        Long,
  ParentHospitalID:  Long,
  HospitalName:      Option[String],
  BranchCode:        Option[String],
  ContactName:       Option[String],
  TINNumber:         Option[String],
  MobileNumber:      Option[Long],
  EmailAddress:      Option[String],
  WebSite:           Option[String],
  About:             Option[String],
  RegsistrationDate: Option[java.sql.Date],
  ActiveIND:         String,
  TermsAccepted:     String,
  Preference:        Option[Map[String, String]],
  HospitalCode:      Option[String],
  CrtUSR:            Long,
  CrtUSRType:        String,
  UpdtUSR:           Option[Long],
  UpdtUSRType:       Option[String])

class Hospital(tag: Tag) extends Table[THospital](tag, "tHospital") {

  def HospitalID = column[Long]("HospitalID", O.PrimaryKey, O.AutoInc)
  def ParentHospitalID = column[Long]("ParentHospitalID", O NotNull)
  def HospitalName = column[Option[String]]("HospitalName", O Nullable)
  def BranchCode = column[Option[String]]("BranchCode", O Nullable)
  def ContactName = column[Option[String]]("ContactName", O Nullable)
  def TINNumber = column[Option[String]]("TINNumber", O Nullable)
  def MobileNumber = column[Option[Long]]("MobileNumber", O Nullable)
  def EmailAddress = column[Option[String]]("EmailAddress", O Nullable)
  def WebSite = column[Option[String]]("WebSite", O Nullable)
  def About = column[Option[String]]("About", O Nullable)
  def RegsistrationDate = column[Option[java.sql.Date]]("RegsistrationDate", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def TermsAccepted = column[String]("TermsAccepted", O Nullable)
  def Preference = column[Option[Map[String, String]]]("Preference", O Nullable)
  def HospitalCode = column[Option[String]]("HospitalCode", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (HospitalID, ParentHospitalID, HospitalName, BranchCode, ContactName, TINNumber, MobileNumber, EmailAddress, WebSite, About, RegsistrationDate, ActiveIND, TermsAccepted, Preference, HospitalCode, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THospital.apply _).tupled, THospital.unapply)

}

object Hospital {
  def Hospital =
    scala.slick.driver.PostgresDriver.simple.TableQuery[Hospital]
}
