package models.com.hcue.doctors.slick.transactTable.PlatformPartners.Hospital

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TEmailHippoNotification(
  Sequence:    Long,
  EmailID:     String,
  ValidIND:    String,
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class EmailHippoNotification(tag: Tag) extends Table[TEmailHippoNotification](tag, "tEmailHippoNotification") {
  def Sequence = column[Long]("Sequence", O NotNull, O.AutoInc)
  def EmailID = column[String]("EmailID", O NotNull)
  def ValidIND = column[String]("ValidIND", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O Nullable)
  def CrtUSRType = column[String]("CrtUSRType", O Nullable)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (Sequence, EmailID, ValidIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TEmailHippoNotification.apply _).tupled, TEmailHippoNotification.unapply)

}

object EmailHippoNotification {
  def EmailHippoNotification =
    scala.slick.driver.PostgresDriver.simple.TableQuery[EmailHippoNotification]
}
