package models.com.hcue.doctors.slick.transactTable.hCueUsers

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THcueAccountLkup(
  AccountID:   String,
  AccountDesc: String,
  ActiveIND:   String,
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class HcueAccountLkup(tag: Tag) extends Table[THcueAccountLkup](tag, "tHCueAccountLkup") {

  def AccountID = column[String]("AccountID", O Nullable)
  def AccountDesc = column[String]("AccountDesc", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (AccountID, AccountDesc, ActiveIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THcueAccountLkup.apply _).tupled, THcueAccountLkup.unapply)

}

object HcueAccountLkup {
  def HcueAccountLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HcueAccountLkup]
}