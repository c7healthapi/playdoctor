package models.com.hcue.doctors.slick.transactTable.hCueUsers

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THospitalGrpSetting(
  GroupCode:       Long,
  GroupName:       Option[String],
  HospitalID:      Long,
  RoleID:          Option[String],
  AccountAccessID: Map[String, String],
  Active:          String,
  ReadOnly:        String,
  CrtUSR:          Long,
  CrtUSRType:      String,
  UpdtUSR:         Option[Long],
  UpdtUSRType:     Option[String])

class HospitalGrpSetting(tag: Tag) extends Table[THospitalGrpSetting](tag, "tHospitalGrpSettings") {

  def GroupCode = column[Long]("GrpCD", O.PrimaryKey, O.AutoInc)
  def GroupName = column[Option[String]]("GrpName", O Nullable)
  def HospitalID = column[Long]("HospitalID", O NotNull)
  def RoleID = column[Option[String]]("RoleID", O NotNull)
  def AccountAccessID = column[Map[String, String]]("AcntAccessID", O NotNull)
  def Active = column[String]("ActiveIND", O NotNull)
  def ReadOnly = column[String]("ReadOnly", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (GroupCode, GroupName, HospitalID, RoleID, AccountAccessID, Active, ReadOnly, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THospitalGrpSetting.apply _).tupled, THospitalGrpSetting.unapply)

}

object HospitalGrpSetting {
  def HospitalGrpSetting =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalGrpSetting]
}
