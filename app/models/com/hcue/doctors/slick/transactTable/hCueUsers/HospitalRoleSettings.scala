package models.com.hcue.doctors.slick.transactTable.hCueUsers

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THospitalRoleSetting(
  HospitalID:  Long,
  RoleID:      String,
  ActiveIND:   String,
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class HospitalRoleSetting(tag: Tag) extends Table[THospitalRoleSetting](tag, "tHospitalRoleSettings") {

  def HospitalID = column[Long]("HospitalID", O Nullable)
  def RoleID = column[String]("RoleID", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (HospitalID, RoleID, ActiveIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THospitalRoleSetting.apply _).tupled, THospitalRoleSetting.unapply)

}

object HospitalRoleSetting {
  def HospitalRoleSetting =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalRoleSetting]
}
