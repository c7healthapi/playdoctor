package models.com.hcue.doctors.slick.transactTable.hCueUsers

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THcueRoleLkup(
  RoleID:      String,
  RoleDesc:    String,
  ActiveIND:   String,
  IsPrivate:   String,
  HospitalID:  Option[Long],
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class HcueRoleLkup(tag: Tag) extends Table[THcueRoleLkup](tag, "tHcueRoleLkup") {

  def RoleID = column[String]("RoleID", O.PrimaryKey, O.AutoInc)
  def RoleDesc = column[String]("RoleDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def IsPrivate = column[String]("IsPrivate", O NotNull)
  def HospitalID = column[Option[Long]]("ParentHospitalID", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (RoleID, RoleDesc, ActiveIND, IsPrivate, HospitalID, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THcueRoleLkup.apply _).tupled, THcueRoleLkup.unapply)

}

object HcueRoleLkup {
  def HcueRoleLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HcueRoleLkup]
}
