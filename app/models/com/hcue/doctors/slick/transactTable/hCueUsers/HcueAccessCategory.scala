package models.com.hcue.doctors.slick.transactTable.hCueUsers

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THcueAccessCategory(
  AccessCategoryID:   Long,
  AccessCategoryDesc: Option[String],
  CrtUSR:             Long,
  CrtUSRType:         String,
  UpdtUSR:            Option[Long],
  UpdtUSRType:        Option[String])

class HcueAccessCategory(tag: Tag) extends Table[THcueAccessCategory](tag, "tHcueAccessCategory") {

  def AccessCategoryID = column[Long]("AccessCategoryID", O.NotNull)
  def AccessCategoryDesc = column[Option[String]]("AccessCategoryDesc", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (AccessCategoryID, AccessCategoryDesc, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THcueAccessCategory.apply _).tupled, THcueAccessCategory.unapply)

}

object HcueAccessCategory {
  def HcueAccessCategory =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HcueAccessCategory]
}