package models.com.hcue.doctors.slick.transactTable.hCueUsers

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.intColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THcueAccessLkup(
  AccessID:         String,
  AccessDesc:       String,
  ActiveIND:        String,
  ParentAccessID:   Option[String],
  AccessCategoryID: Option[Long],
  AccessType:       String,
  GrpType:          String,
  Order:            Option[Int],
  CrtUSR:           Long,
  CrtUSRType:       String,
  UpdtUSR:          Option[Long],
  UpdtUSRType:      Option[String])

class HcueAccessLkup(tag: Tag) extends Table[THcueAccessLkup](tag, "tHcueAccessLkup") {

  def AccessID = column[String]("AccessID", O Nullable)
  def AccessDesc = column[String]("AccessDesc", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O Nullable)
  def ParentAccessID = column[Option[String]]("ParentAccessID", O Nullable)
  def AccessCategoryID = column[Option[Long]]("AccessCategoryID", O.Nullable)
  def AccessType = column[String]("AccessType", O.Nullable)
  def GrpType = column[String]("GrpType", O.Nullable)
  def Order = column[Option[Int]]("Order", O.Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (AccessID, AccessDesc, ActiveIND, ParentAccessID, AccessCategoryID, AccessType, GrpType, Order, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THcueAccessLkup.apply _).tupled, THcueAccessLkup.unapply)

}

object HcueAccessLkup {
  def HcueAccessLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HcueAccessLkup]
}