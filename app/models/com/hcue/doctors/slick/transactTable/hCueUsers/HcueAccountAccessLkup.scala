package models.com.hcue.doctors.slick.transactTable.hCueUsers

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THcueAccountAccessLkup(
  AcntAccessID: String,
  AccessID:     String,
  AccountID:    Map[String, String],
  ActiveIND:    String,
  CrtUSR:       Long,
  CrtUSRType:   String,
  UpdtUSR:      Option[Long],
  UpdtUSRType:  Option[String])

class HcueAccountAccessLkup(tag: Tag) extends Table[THcueAccountAccessLkup](tag, "tHcueAccountAccessLkup") {

  def AcntAccessID = column[String]("AcntAccessID", O Nullable)
  def AccessID = column[String]("AccessID", O Nullable)
  def AccountID = column[Map[String, String]]("AccountID", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (AcntAccessID, AccessID, AccountID, ActiveIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THcueAccountAccessLkup.apply _).tupled, THcueAccountAccessLkup.unapply)

}

object HcueAccountAccessLkup {
  def HcueAccountAccessLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HcueAccountAccessLkup]
}