package models.com.hcue.doctors.slick.transactTable.messaging

import java.sql.Date

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType


case class TDiagnosticsResponseExtn(
  responseid: Long,
  indications: Option[String],
  comparisions: Option[String],
  techniques: Option[String],
  isMRIMessage : Option[Boolean],
  UpdtUSR: Long,
  UpdtUSRType: String)

class DiagnosticsResponseExtn(tag: Tag) extends Table[TDiagnosticsResponseExtn](tag, "tDiagnosticsResponse") {
  def responseid = column[Long]("responseid", O.PrimaryKey, O.AutoInc)
  def indications = column[Option[String]]("indications")
  def comparisions = column[Option[String]]("comparisions")
  def techniques = column[Option[String]]("techniques")
  def isMRIMessage = column[Option[Boolean]]("isMRIMessage")
  def UpdtUSR = column[Long]("UpdtUSR")
  def UpdtUSRType = column[String]("UpdtUSRType")

  def * = (responseid, indications, comparisions, techniques, isMRIMessage, UpdtUSR, UpdtUSRType).<>((TDiagnosticsResponseExtn.apply _).tupled, TDiagnosticsResponseExtn.unapply)
}

object DiagnosticsResponseExtn {
  def DiagnosticsResponseExtn =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DiagnosticsResponseExtn]
}