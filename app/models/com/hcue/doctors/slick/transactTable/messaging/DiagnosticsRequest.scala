package models.com.hcue.doctors.slick.transactTable.messaging

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDiagnosticsRequest(
  requestid:           Long,
  patientid:           Long,
  appointmentid:       Long,
  nhsnumber:           Long,
  scancentreid:        Long,
  scancentreaddressid: Long, // not required - to be removed
  ScanCenterName:      String,
  SonographerId:       Long,
  SonographerName:     String,
  ServiceCode:         Option[String],
  ServiceCodeText:     Option[String],
  ClinicalInfo:        String,
  ReferralId:          Long,
  ActiveIND:           String,
  AppointmentStatus:   String,
  CrtUSR:              Long,
  CrtUSRType:          String,
  UpdtUSR:             Option[Long],
  UpdtUSRType:         Option[String],
  patientenquiryid:    Long)

class DiagnosticsRequest(tag: Tag) extends Table[TDiagnosticsRequest](tag, "tDiagnosticsRequest") {
  def requestid = column[Long]("requestid", O.PrimaryKey, O.AutoInc)
  def patientid = column[Long]("patientid")
  def appointmentid = column[Long]("appointmentid")
  def nhsnumber = column[Long]("nhsnumber")
  def scancentreid = column[Long]("scancentreid")
  def scancentreaddressid = column[Long]("scancentreaddressid")
  def ScanCenterName = column[String]("ScanCenterName")
  def SonographerId = column[Long]("SonographerId")
  def SonographerName = column[String]("SonographerName")
  def ServiceCode = column[Option[String]]("ServiceCode")
  def ServiceCodeText = column[Option[String]]("ServiceCodeText")
  def ClinicalInfo = column[String]("ClinicalInfo")
  def ReferralId = column[Long]("ReferralId")
  def ActiveIND = column[String]("ActiveIND")
  def AppointmentStatus = column[String]("AppointmentStatus")
  def CrtUSR = column[Long]("CrtUSR")
  def CrtUSRType = column[String]("CrtUSRType")
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def patientenquiryid = column[Long]("patientenquiryid")

  def * = (requestid, patientid, appointmentid, nhsnumber, scancentreid, scancentreaddressid, ScanCenterName, SonographerId, SonographerName,
    ServiceCode, ServiceCodeText, ClinicalInfo, ReferralId, ActiveIND, AppointmentStatus, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType, patientenquiryid).<>((TDiagnosticsRequest.apply _).tupled, TDiagnosticsRequest.unapply)
}

object DiagnosticsRequest {
  def DiagnosticsRequest =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DiagnosticsRequest]
}