package models.com.hcue.doctors.slick.transactTable.messaging

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TMeshIntegation(
  MeshIntegationID: Long,
  HL7ReferenceID:   Long,
  FromMailBoxID:    String,
  ToMailBoxID:      String,
  FileName:         Option[String],
  LocalID:          Option[String],
  MessageID:        Option[String],
  TotalChunk:       Int,
  Status:           Option[String],
  crtusr:           Long,
  crtusrtype:       String,
  updtusr:          Option[Long],
  updtusrtype:      Option[String])

class MeshIntegation(tag: Tag) extends Table[TMeshIntegation](tag, "tMeshIntegation") {

  def MeshIntegationID = column[Long]("MeshIntegationID", O.PrimaryKey, O.AutoInc)
  def HL7ReferenceID = column[Long]("HL7ReferenceID", O NotNull)
  def FromMailBoxID = column[String]("FromMailBoxID", O NotNull)
  def ToMailBoxID = column[String]("ToMailBoxID", O NotNull)
  def FileName = column[Option[String]]("FileName", O Nullable)
  def LocalID = column[Option[String]]("LocalID", O Nullable)
  def MessageID = column[Option[String]]("MessageID", O Nullable)
  def TotalChunk = column[Int]("TotalChunk", O NotNull)
  def Status = column[Option[String]]("Status", O Nullable)
  def crtusr = column[Long]("CrtUSR", O NotNull)
  def crtusrtype = column[String]("CrtUSRType", O NotNull)
  def updtusr = column[Option[Long]]("UpdtUSR", O.Nullable)
  def updtusrtype = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (MeshIntegationID, HL7ReferenceID, FromMailBoxID, ToMailBoxID, FileName, LocalID, MessageID, TotalChunk, Status,
    crtusr, crtusrtype, updtusr, updtusrtype).<>((TMeshIntegation.apply _).tupled, TMeshIntegation.unapply)

}

object MeshIntegation {
  def MeshIntegation =
    scala.slick.driver.PostgresDriver.simple.TableQuery[MeshIntegation]
}
