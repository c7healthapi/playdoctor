package models.com.hcue.doctors.slick.transactTable.messaging

import java.sql.Date

import models.com.hcue.doctors.slick.MyPostgresDriver.simple._

case class TMessagingLog(
  MessageId: Long,
  MessageType: String,
  Content: String,
  Status: String,
  Destination: Long,
  DateTime: Option[String],
  Metadata: String,
  ExternalID: Long,
  CrtUSR: Long,
  CrtUSRType: String,
  UpdtUSR: Option[Long],
  UpdtUSRType: Option[String])

class MessagingLog(tag: Tag) extends Table[TMessagingLog](tag, "tMessagingLog") {
  def MessageId = column[Long]("MessageId", O.PrimaryKey, O.AutoInc)
  def MessageType = column[String]("MessageType")
  def Content = column[String]("Content")
  def Status = column[String]("Status")
  def Destination = column[Long]("Destination")
  def DateTime = column[Option[String]]("DateTime")
  def Metadata = column[String]("Metadata")
  def ExternalID = column[Long]("ExternalID")
  def CrtUSR = column[Long]("CrtUSR")
  def CrtUSRType = column[String]("CrtUSRType")
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (MessageId, MessageType, Content, Status, Destination, DateTime, Metadata, ExternalID, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TMessagingLog.apply _).tupled, TMessagingLog.unapply)
}

object MessagingLog {
  def MessagingLog =
    scala.slick.driver.PostgresDriver.simple.TableQuery[MessagingLog]
}