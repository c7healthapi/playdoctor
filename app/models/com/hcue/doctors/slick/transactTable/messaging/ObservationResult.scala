package models.com.hcue.doctors.slick.transactTable.messaging

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TObservationResult(
  id:                       Long,
  obrrequestid:             Long,
  patientid:                String,
  obxsetid:                 String,
  obxvaluetype:             String,
  obxidentifier:            String,
  obxsubid:                 String,
  obxvalue:                 String,
  obxresultstatus:          String,
  obxeffectivedaterefrange: String,
  obxuserdefaccesscheck:    String,
  obxobservationdatetime:   String,
  obxproducerid:            String,
  obxresobserver:           String)

class ObservationResult(tag: Tag) extends Table[TObservationResult](tag, "tobservationresult") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def obrrequestid = column[Long]("obrrequestid", O NotNull)
  def patientid = column[String]("patientid")
  def obxsetid = column[String]("obxsetid", O NotNull)
  def obxvaluetype = column[String]("obxvaluetype", O Nullable)
  def obxidentifier = column[String]("obxidentifier", O Nullable)
  def obxsubid = column[String]("obxsubid", O Nullable)
  def obxvalue = column[String]("obxvalue", O Nullable)
  def obxresultstatus = column[String]("obxresultstatus", O Nullable)
  def obxeffectivedaterefrange = column[String]("obxeffectivedaterefrange", O Nullable)
  def obxuserdefaccesscheck = column[String]("obxuserdefaccesscheck", O Nullable)
  def obxobservationdatetime = column[String]("obxobservationdatetime", O Nullable)
  def obxproducerid = column[String]("obxproducerid", O Nullable)
  def obxresobserver = column[String]("obxresobserver", O Nullable)

  def * = (id, obrrequestid, patientid, obxsetid, obxvaluetype, obxidentifier, obxsubid, obxvalue, obxresultstatus, obxeffectivedaterefrange, obxuserdefaccesscheck, obxobservationdatetime, obxproducerid, obxresobserver).<>((TObservationResult.apply _).tupled, TObservationResult.unapply)

}

object ObservationResult {
  def ObservationResult =
    scala.slick.driver.PostgresDriver.simple.TableQuery[ObservationResult]
}
