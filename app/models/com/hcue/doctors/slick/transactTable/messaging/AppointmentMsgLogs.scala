package models.com.hcue.doctors.slick.transactTable.messaging

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.intColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TAppointmentMsgLog(
  MessageId: Long,
  AppointmentID:Long,
  AppointmentStatus:String,
  PatientID: Long,
  HospitalID: Long,
  Message: String,
  MessageType: String,
  Destination: Long,
  DateTime: Option[String],
  APIResponse: String,
  ExternalID: Long,
  Status: String,
  DeliveryType: String,
  CrtUSR: Long,
  CrtUSRType: String,
  UpdtUSR: Option[Long],
  UpdtUSRType: Option[String])

class AppointmentMsgLog(tag: Tag) extends Table[TAppointmentMsgLog](tag, "tAppoinmentMsgLog") {
  def MessageId = column[Long]("MessageID", O.PrimaryKey, O.AutoInc)
  def AppointmentID = column[Long]("AppointmentID")
  def AppointmentStatus = column[String]("AppointmentStatus")
  def PatientID = column[Long]("PatientID")
  def HospitalID = column[Long]("HospitalID")
  def Message = column[String]("Message")
  def MessageType = column[String]("MessageType")
  def Destination = column[Long]("Destination")
  def DateTime = column[Option[String]]("DateTime")
  def APIResponse = column[String]("APIResponse")
  def ExternalID = column[Long]("ExternalID")
  def Status = column[String]("Status")
  def DeliveryType = column[String]("DeliveryType")
  def CrtUSR = column[Long]("CrtUSR")
  def CrtUSRType = column[String]("CrtUSRType")
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (MessageId, AppointmentID,AppointmentStatus, PatientID, HospitalID,Message,MessageType, 
      Destination, DateTime, APIResponse, ExternalID,Status, DeliveryType, CrtUSR, CrtUSRType, 
      UpdtUSR, UpdtUSRType).<>((TAppointmentMsgLog.apply _).tupled, TAppointmentMsgLog.unapply)
}

object AppointmentMsgLog {
  def AppointmentMsgLog=
    scala.slick.driver.PostgresDriver.simple.TableQuery[AppointmentMsgLog]
}