package models.com.hcue.doctors.slick.transactTable.messaging

import java.sql.Date

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDiagnosticsResponse(
  responseid:       Long,
  requestid:        Long,
  patientid:        Long,
  appointmentid:    Long,
  nhsnumber:        Long,
  referralid:       Long,
  findings:         String,
  history:          String,
  impressions:      String,
  reportor:         String,
  reportstatus:     String,
  examdate:         String,
  reportdate:       String,
  exam:             String,
  ReportURL:        String,
  ReportURLExpDate: Option[Date],
  CrtUSR:           Long,
  CrtUSRType:       String,
  scanstarttime:    Option[String],
  scanendtime:      Option[String],
  Active:           Boolean,
  patientenquiryid: Long)

class DiagnosticsResponse(tag: Tag) extends Table[TDiagnosticsResponse](tag, "tDiagnosticsResponse") {
  def responseid = column[Long]("responseid", O.PrimaryKey, O.AutoInc)
  def requestid = column[Long]("requestid")
  def patientid = column[Long]("patientid")
  def appointmentid = column[Long]("appointmentid")
  def nhsnumber = column[Long]("nhsnumber")
  def referralid = column[Long]("ReferralId")
  def findings = column[String]("findings")
  def history = column[String]("history")
  def impressions = column[String]("impressions")
  def reportor = column[String]("reportor")
  def reportstatus = column[String]("reportstatus")
  def examdate = column[String]("examdate")
  def reportdate = column[String]("reportdate")
  def exam = column[String]("exam")
  def ReportURL = column[String]("ReportURL")
  def ReportURLExpDate = column[Option[Date]]("ReportURLExpDate", O Nullable)
  def CrtUSR = column[Long]("CrtUSR")
  def CrtUSRType = column[String]("CrtUSRType")
  def scanstarttime = column[Option[String]]("scanstarttime", O Nullable)
  def scanendtime = column[Option[String]]("scanendtime", O Nullable)
  def Active = column[Boolean]("Active")
  def patientenquiryid = column[Long]("patientenquiryid")

  def * = (responseid, requestid, patientid, appointmentid, nhsnumber, referralid, findings, history, impressions, reportor, reportstatus,
    examdate, reportdate, exam, ReportURL, ReportURLExpDate, CrtUSR, CrtUSRType, scanstarttime, scanendtime, Active, patientenquiryid).<>((TDiagnosticsResponse.apply _).tupled, TDiagnosticsResponse.unapply)
}

object DiagnosticsResponse {
  def DiagnosticsResponse =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DiagnosticsResponse]
}