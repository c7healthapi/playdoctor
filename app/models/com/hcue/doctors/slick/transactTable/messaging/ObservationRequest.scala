package models.com.hcue.doctors.slick.transactTable.messaging

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TObservationRequest(
  id:                         Long,
  patientid:                  String,
  obrsetid:                   String,
  obrplacerordernumber:       String,
  obrfillerordernumber:       String,
  obrunivserviceidentifier:   String,
  obrrequesteddatetime:       String,
  obrobservationdatetime:     String,
  obrobservationenddatetime:  String,
  obrorderingprovider:        String,
  obrresultstatuschgdatetime: String,
  obrdiagservicesecretid:     String,
  obrresultstatus:            String,
  obrresultinterpreter:       String,
  obrtranscriptionist:        String)

class ObservationRequest(tag: Tag) extends Table[TObservationRequest](tag, "tobservationrequest") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def patientid = column[String]("patientid")
  def obrsetid = column[String]("obrsetid", O NotNull)
  def obrplacerordernumber = column[String]("obrplacerordernumber", O Nullable)
  def obrfillerordernumber = column[String]("obrfillerordernumber", O Nullable)
  def obrunivserviceidentifier = column[String]("obrunivserviceidentifier", O Nullable)
  def obrrequesteddatetime = column[String]("obrrequesteddatetime", O Nullable)
  def obrobservationdatetime = column[String]("obrobservationdatetime", O Nullable)
  def obrobservationenddatetime = column[String]("obrobservationenddatetime", O Nullable)
  def obrorderingprovider = column[String]("obrorderingprovider", O Nullable)
  def obrresultstatuschgdatetime = column[String]("obrresultstatuschgdatetime", O Nullable)
  def obrdiagservicesecretid = column[String]("obrdiagservicesecretid", O Nullable)
  def obrresultstatus = column[String]("obrresultstatus", O Nullable)
  def obrresultinterpreter = column[String]("obrresultinterpreter", O Nullable)
  def obrtranscriptionist = column[String]("obrtranscriptionist", O Nullable)

  def * = (id, patientid, obrsetid, obrplacerordernumber, obrfillerordernumber, obrunivserviceidentifier, obrrequesteddatetime, obrobservationdatetime, obrobservationenddatetime, obrorderingprovider, obrresultstatuschgdatetime, obrdiagservicesecretid, obrresultstatus, obrresultinterpreter, obrtranscriptionist).<>((TObservationRequest.apply _).tupled, TObservationRequest.unapply)

}

object ObservationRequest {
  def ObservationRequest =
    scala.slick.driver.PostgresDriver.simple.TableQuery[ObservationRequest]
}
