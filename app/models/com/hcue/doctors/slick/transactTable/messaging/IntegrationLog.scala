package models.com.hcue.doctors.slick.transactTable.messaging

import java.sql.Date
import java.sql.Timestamp

import models.com.hcue.doctors.slick.MyPostgresDriver.simple._

case class TIntegrationLog(
  LogID: Long,
  PatientID: Long,
  FirstName: Option[String],
  LastName: Option[String],
  DateOfBirth: Option[String],
  Gender: Option[String],
  MessageType: Option[String],
  Message: Option[String],
  Source: Option[String],
  Destination: Option[String],
  Status: Option[String],
  isAckReceived: Option[String],
  PatientEnquiryID: Long,
  AppointmentID: Long,
  MessageControlID: Option[Long],
  Created: Timestamp,
  CrtUsr: String,
  Modified: Option[Date],
  UpdtUsr: Option[String])

class IntegrationLog(tag: Tag) extends Table[TIntegrationLog](tag, "tIntegrationLog") {
  def LogID = column[Long]("LogID", O.PrimaryKey, O.AutoInc)
  def PatientID = column[Long]("PatientID")
  def FirstName = column[Option[String]]("FirstName")
  def LastName = column[Option[String]]("LastName")
  def DateOfBirth = column[Option[String]]("DateOfBirth")
  def Gender = column[Option[String]]("Gender")
  def MessageType = column[Option[String]]("MessageType")
  def Message = column[Option[String]]("Message")
  def Source = column[Option[String]]("Source")
  def Destination = column[Option[String]]("Destination")
  def Status = column[Option[String]]("Status")
  def isAckReceived = column[Option[String]]("isAckReceived")
  def PatientEnquiryID = column[Long]("PatientEnquiryID") 
  def AppointmentID = column[Long]("AppointmentID")
  def MessageControlID = column[Option[Long]]("MessageControlID")
  def Created = column[Timestamp]("Created")
  def CrtUsr = column[String]("CrtUsr")
  def Modified = column[Option[Date]]("Modified")
  def UpdtUsr = column[Option[String]]("UpdtUsr")

  def * = (LogID, PatientID, FirstName, LastName, DateOfBirth, Gender, MessageType, Message, Source, Destination, Status,
    isAckReceived, PatientEnquiryID, AppointmentID, MessageControlID, Created, CrtUsr, Modified, UpdtUsr).<>((TIntegrationLog.apply _).tupled, TIntegrationLog.unapply)
}

object IntegrationLog {
  def IntegrationLog =
    scala.slick.driver.PostgresDriver.simple.TableQuery[IntegrationLog]
}