package models.com.hcue.doctors.slick.transactTable.patient

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TPatientPhones(
  PatientID:   Long,
  PhNumber:    Long,
  PhType:      String,
  PrimaryIND:  String,
  PhStdCD:     Option[String],
  FullNumber:  Option[String],
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class PatientsPhones(tag: Tag) extends Table[TPatientPhones](tag, "tPatientPhone") {

  def PatientID = column[Long]("PatientID")
  def PhNumber = column[Long]("PhNumber")
  def PhType = column[String]("PhType")
  def PrimaryIND = column[String]("PrimaryIND")
  def PhStdCD = column[Option[String]]("PhStdCD")
  def FullNumber = column[Option[String]]("FullNumber")
  def CrtUSR = column[Long]("CrtUSR")
  def CrtUSRType = column[String]("CrtUSRType")
  def UpdtUSR = column[Option[Long]]("UpdtUSR")
  def UpdtUSRType = column[Option[String]]("UpdtUSRType")

  def * = (PatientID, PhNumber, PhType, PrimaryIND, PhStdCD, FullNumber, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TPatientPhones.apply _).tupled, TPatientPhones.unapply)

}

object PatientPhones {

  def patientsPhones =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientsPhones]

}
