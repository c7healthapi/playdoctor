package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TPatientEnquiry(
  patientenquiryid:  Long,
  patientid:         Long,
  referalcreated:    Option[java.sql.Date],
  referalreceived:   Option[java.sql.Date],
  referalprocessed:  Option[java.sql.Date],
  clinicalnotes:     Option[String],
  clinicaldetails:   Option[String],
  speciality:        Option[String],
  subspeciality:     Option[String],
  appointmenttypeid: Option[String],
  enquirysourceid:   Option[String],
  enquiryfileurl:    Option[Map[String, String]],
  crtusr:            Long,
  crtusrtype:        String,
  updtusrtype:       Option[String],
  isVoicemail:       Option[Boolean],
  isEmail:           Option[Boolean],
  isLetter:          Option[Boolean],
  manageid:          Option[Long],
  notes:             Option[String],
  enquirystatusid:   Option[String],
  regionid:          Option[Long])

class PatientEnquiry(tag: Tag) extends Table[TPatientEnquiry](tag, "tpatientenquiry") {

  def patientenquiryid = column[Long]("patientenquiryid", O Nullable)
  def patientid = column[Long]("patientid", O NotNull)
  def referalcreated = column[Option[java.sql.Date]]("referalcreated", O Nullable)
  def referalreceived = column[Option[java.sql.Date]]("referalreceived", O Nullable)
  def referalprocessed = column[Option[java.sql.Date]]("referalprocessed", O Nullable)
  def clinicalnotes = column[Option[String]]("clinicalnotes", O.PrimaryKey, O Nullable)
  def clinicaldetails = column[Option[String]]("clinicaldetails", O Nullable)
  def speciality = column[Option[String]]("speciality", O Nullable)
  def subspeciality = column[Option[String]]("subspeciality", O Nullable)
  def appointmenttypeid = column[Option[String]]("appointmenttypeid", O Nullable)
  def enquirysourceid = column[Option[String]]("enquirysourceid", O Nullable)
  def enquiryfileurl = column[Option[Map[String, String]]]("enquiryfileurl", O Nullable)
  def crtusr = column[Long]("CrtUSR", O NotNull)
  def crtusrtype = column[String]("CrtUSRType", O Nullable)
  def updtusrtype = column[Option[String]]("UpdtUSRType", O Nullable)
  def isVoicemail = column[Option[Boolean]]("isvoicemail", O Nullable)
  def isEmail = column[Option[Boolean]]("isemail", O Nullable)
  def isLetter = column[Option[Boolean]]("isletter", O Nullable)
  def manageid = column[Option[Long]]("manageid", O Nullable)
  def notes = column[Option[String]]("notes", O Nullable)
  def enquirystatusid = column[Option[String]]("enquirystatusid", O Nullable)
  def regionid = column[Option[Long]]("regionid", O Nullable)

  def * = (patientenquiryid, patientid, referalcreated, referalreceived, referalprocessed, clinicalnotes, clinicaldetails, speciality, subspeciality, appointmenttypeid, enquirysourceid, enquiryfileurl, crtusr, crtusrtype, updtusrtype, isVoicemail, isEmail, isLetter, manageid, notes, enquirystatusid, regionid).<>((TPatientEnquiry.apply _).tupled, TPatientEnquiry.unapply)

}

object PatientEnquiry {
  def PatientEnquiry =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientEnquiry]
}