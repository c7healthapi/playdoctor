package models.com.hcue.doctors.slick.transactTable.patient

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TEnquirySpeciality(
  Id:                    Long,
  PatientEnquiryId:      Long,
  SpecialityCode:        String,
  SpecialityDescription: String,
  ActiveInd:             String,
  SubSpecialities:       Map[String, String])

class EnquirySpeciality(tag: Tag) extends Table[TEnquirySpeciality](tag, "tenquiryspeciality") {

  def Id = column[Long]("id", O.PrimaryKey, O.AutoInc, O NotNull)
  def PatientEnquiryId = column[Long]("patientenquiryid", O NotNull)
  def SpecialityCode = column[String]("specialitycode", O NotNull)
  def SpecialityDescription = column[String]("specialitydescription", O NotNull)
  def ActiveInd = column[String]("activeind", O NotNull)
  def SubSpecialities = column[Map[String, String]]("subspecialityid", O NotNull)

  def * = (Id, PatientEnquiryId, SpecialityCode, SpecialityDescription, ActiveInd, SubSpecialities).<>((TEnquirySpeciality.apply _).tupled, TEnquirySpeciality.unapply)

}

object EnquirySpeciality {
  def EnquirySpeciality =
    scala.slick.driver.PostgresDriver.simple.TableQuery[EnquirySpeciality]
}
