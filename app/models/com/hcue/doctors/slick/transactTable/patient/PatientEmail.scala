package models.com.hcue.doctors.slick.transactTable.patient

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TPatientEmail(
  PatientID:   Long,
  EmailID:     String,
  EmailIDType: String,
  PrimaryIND:  String,
  ValidIND:    String,
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class PatientsEmail(tag: Tag) extends Table[TPatientEmail](tag, "tPatientEmail") {

  def PatientID = column[Long]("PatientID")
  def EmailID = column[String]("EmailID")
  def EmailIDType = column[String]("EmailIDType")
  def PrimaryIND = column[String]("PrimaryIND")
  def ValidIND = column[String]("ValidIND")
  def CrtUSR = column[Long]("CrtUSR")
  def CrtUSRType = column[String]("CrtUSRType")
  def UpdtUSR = column[Option[Long]]("UpdtUSR")
  def UpdtUSRType = column[Option[String]]("UpdtUSRType")

  def * = (PatientID, EmailID, EmailIDType, PrimaryIND, ValidIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TPatientEmail.apply _).tupled, TPatientEmail.unapply)

}

object PatientEmail {
  def patientsEmail =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientsEmail]
}
