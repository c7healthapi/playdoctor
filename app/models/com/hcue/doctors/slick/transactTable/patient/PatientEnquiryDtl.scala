package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType


/*------------------TPatientAddress Table JPA------------------*/

case class TPatientEnquiryDtllkup(
  patientenquiryid: Long,
  patientid: Long,
  enquirystatusid: String,
  manageid: Long,
  notes: Option[String],
  reminderduedate: Option[java.sql.Date],
  documenturl: Option[String],
  updtusr: Option[Long],
  updtusrtype: Option[String],
  reportprcticeid: Option[Long],
  observationid: Option[Long])

class PatientEnquiryDtlLkup(tag: Tag) extends Table[TPatientEnquiryDtllkup](tag, "tpatientenquirydtl") {

  def patientenquiryid = column[Long]("patientenquiryid", O Nullable)
  def patientid = column[Long]("patientid", O NotNull)
  def enquirystatusid = column[String]("enquirystatusid", O NotNull)
  def manageid = column[Long]("manageid", O NotNull)
  def notes = column[Option[String]]("notes",O Nullable)
  def reminderduedate = column[Option[java.sql.Date]]("reminderduedate",O Nullable)
  def documenturl = column[Option[String]]("documenturl",O Nullable)
  def updtusr = column[Option[Long]]("updtusr", O.PrimaryKey, O NotNull)
  def updtusrtype = column[Option[String]]("updtusrtype",O Nullable)
  def reportprcticeid = column[Option[Long]]("reportprcticeid",O Nullable)
  def observationid = column[Option[Long]]("observationid",O Nullable)

  def * = (patientenquiryid, patientid, enquirystatusid, manageid,notes,reminderduedate,documenturl,updtusr, updtusrtype,reportprcticeid,observationid).<>((TPatientEnquiryDtllkup.apply _).tupled, TPatientEnquiryDtllkup.unapply)

}

object PatientEnquiryDtlLkup{
 
  def patientenquirydtlLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientEnquiryDtlLkup]

}




