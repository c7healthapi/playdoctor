package models.com.hcue.doctors.slick.transactTable.patient

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TPatientAddress(
  PatientID:      Long,
  Address1:       Option[String],
  Address2:       Option[String],
  Street:         Option[String],
  Location:       Option[String],
  CityTown:       Option[String],
  DistrictRegion: Option[String],
  State:          Option[String],
  PinCode:        Int,
  Country:        String,
  AddressType:    String,
  PrimaryIND:     String,
  LandMark:       Option[String],
  Latitude:       Option[String],
  Longitude:      Option[String],
  CrtUSR:         Long,
  CrtUSRType:     String,
  UpdtUSR:        Option[Long],
  UpdtUSRType:    Option[String],
  PostCode:       Option[String])

class PatientsAddress(tag: Tag) extends Table[TPatientAddress](tag, "tPatientAddress") {

  def PatientID = column[Long]("PatientID")
  def Address1 = column[Option[String]]("Address1")
  def Address2 = column[Option[String]]("Address2")
  def Street = column[Option[String]]("Street")
  def Location = column[Option[String]]("Location")
  def CityTown = column[Option[String]]("CityTown")
  def DistrictRegion = column[Option[String]]("DistrictRegion")
  def State = column[Option[String]]("State")
  def PinCode = column[Int]("PinCode")
  def Country = column[String]("Country")
  def AddressType = column[String]("AddressType")
  def PrimaryIND = column[String]("PrimaryIND")
  def LandMark = column[Option[String]]("LandMark")
  def Latitude = column[Option[String]]("Latitude")
  def Longitude = column[Option[String]]("Longitude")
  def CrtUSR = column[Long]("CrtUSR")
  def CrtUSRType = column[String]("CrtUSRType")
  def UpdtUSR = column[Option[Long]]("UpdtUSR")
  def UpdtUSRType = column[Option[String]]("UpdtUSRType")
  def PostCode = column[Option[String]]("Postcode")

  def * = (PatientID, Address1, Address2, Street, Location, CityTown, DistrictRegion, State, PinCode, Country, AddressType,
    PrimaryIND, LandMark, Latitude, Longitude, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType, PostCode).<>((TPatientAddress.apply _).tupled, TPatientAddress.unapply)

}

object PatientAddress {

  def patientsAddress =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientsAddress]

}
