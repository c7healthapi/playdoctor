package models.com.hcue.doctors.slick.transactTable.patient

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.jsonTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import play.api.libs.json.JsValue

case class TPatientExtn(
  PatientID:             Long,
  AuditTracker:          Option[Map[String, String]],
  PatientDocuments:      Option[JsValue],
  CaseDocuments:         Option[Map[String, String]],
  ReferralID:            Option[Long],
  SubReferralID:         Option[Long],
  PatientDocumentsExpDt: Option[java.sql.Date])

class PatientExtn(tag: Tag) extends Table[TPatientExtn](tag, "tPatient") {

  def PatientID = column[Long]("PatientID")
  def AuditTracker = column[Option[Map[String, String]]]("AuditTracker", O Nullable)
  def PatientDocuments = column[Option[JsValue]]("PatientDocuments", O Nullable)
  def CaseDocuments = column[Option[Map[String, String]]]("CaseDocuments", O Nullable)
  def ReferralID = column[Option[Long]]("ReferralID", O Nullable)
  def SubReferralID = column[Option[Long]]("SubReferralID", O Nullable)
  def PatientDocumentsExpDt = column[Option[java.sql.Date]]("PatientDocumentsExpDt", O Nullable)

  def * = (PatientID, AuditTracker, PatientDocuments, CaseDocuments, ReferralID, SubReferralID, PatientDocumentsExpDt).<>((TPatientExtn.apply _).tupled, TPatientExtn.unapply)

}

object PatientExtn {
  def PatientExtn =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientExtn]
}