package models.com.hcue.doctors.slick.transactTable.patient

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TPatientOtherID(
  PatientID:          Long,
  PatientOtherID:     String,
  PatientOtherIDType: String,
  CrtUSR:             Long,
  CrtUSRType:         String,
  UpdtUSR:            Option[Long],
  UpdtUSRType:        Option[String])

class PatientsOtherID(tag: Tag) extends Table[TPatientOtherID](tag, "tPatientOtherID") {

  def PatientID = column[Long]("PatientID")
  def PatientOtherID = column[String]("PatientOtherID")
  def PatientOtherIDType = column[String]("PatientOtherIDType")
  def CrtUSR = column[Long]("CrtUSR")
  def CrtUSRType = column[String]("CrtUSRType")
  def UpdtUSR = column[Option[Long]]("UpdtUSR")
  def UpdtUSRType = column[Option[String]]("UpdtUSRType")

  def * = (PatientID, PatientOtherID, PatientOtherIDType, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TPatientOtherID.apply _).tupled, TPatientOtherID.unapply)

}

object PatientOtherID {
  def patientsOtherID =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientsOtherID]
}
