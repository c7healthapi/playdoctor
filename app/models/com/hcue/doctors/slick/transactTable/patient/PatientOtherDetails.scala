package models.com.hcue.doctors.slick.transactTable.patient

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TPatientOtherDetails(
  PatientID:        Long,
  AadhaarID:        Option[String],
  Preference:       Option[Map[String, String]],
  MaritalStatus:    Option[String],
  Education:        Option[String],
  Occupation:       Option[String],
  PatientDisplayID: Option[Map[String, String]],
  SpecialNotes:     Option[String],
  ReferralSource:   Option[Map[String, String]],
  BranchesVisited:  Option[Map[String, String]],
  FamilyDisplayID:  Option[Map[String, String]])

class PatientOtherDetails(tag: Tag) extends Table[TPatientOtherDetails](tag, "tPatient") {

  def PatientID = column[Long]("PatientID")
  def AadhaarID = column[Option[String]]("AadhaarID", O Nullable)
  def Preference = column[Option[Map[String, String]]]("Preference", O Nullable)
  def MaritalStatus = column[Option[String]]("MaritalStatus", O Nullable)
  def Education = column[Option[String]]("Education", O Nullable)
  def Occupation = column[Option[String]]("Occupation", O Nullable)
  def PatientDisplayID = column[Option[Map[String, String]]]("PatientDisplayID", O Nullable)

  //  def PaymentMode = column[Option[String]]("PaymentMode", O Nullable)
  def SpecialNotes = column[Option[String]]("SpecialNotes", O Nullable)
  def ReferralSource = column[Option[Map[String, String]]]("ReferralSource", O Nullable)
  def BranchesVisited = column[Option[Map[String, String]]]("BranchesVisited", O Nullable)
  def FamilyDisplayID = column[Option[Map[String, String]]]("FamilyDisplayID", O Nullable)

  def * = (PatientID, AadhaarID, Preference, MaritalStatus, Education, Occupation, PatientDisplayID, SpecialNotes, ReferralSource, BranchesVisited, FamilyDisplayID).<>((TPatientOtherDetails.apply _).tupled, TPatientOtherDetails.unapply)

}

object PatientOtherDetails {
  def PatientOtherDetail =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientOtherDetails]
}