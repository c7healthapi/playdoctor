package models.com.hcue.doctors.slick.transactTable.patient

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.doubleColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TPatient(

  Title:        Option[String],
  FirstName:    String,
  LastName:     Option[String],
  FullName:     String,
  PatientID:    Long,
  AadhaarID:    Option[String],
  Gender:       String,
  DOB:          Option[java.sql.Date],
  Age:          Option[Double],
  ProfileImage: Option[String],
  // BloodGroup: Option[String],
  FamilyHdIND:    Option[String],
  FamilyHdID:     Option[Long],
  FamilyRltnType: Option[String],
  PatientLoginID: Option[String],
  //PatientPassword: String,
  //PinPassCode: Option[Int],
  TermsAccepted: String,
  PatientGroup:  Option[Map[String, String]],
  //Created: Option[java.sql.Date],
  CrtUSR:     Long,
  CrtUSRType: String,
  //modified: Option[java.sql.Date],
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class Patient(tag: Tag) extends Table[TPatient](tag, "tPatient") {

  def Title = column[Option[String]]("Title", O Nullable)
  def FirstName = column[String]("FirstName", O NotNull)
  def LastName = column[Option[String]]("LastName", O Nullable)
  def FullName = column[String]("FullName", O NotNull)
  def PatientID = column[Long]("PatientID", O.PrimaryKey, O.AutoInc)
  def AadhaarID = column[Option[String]]("AadhaarID", O Nullable)
  def Gender = column[String]("Gender", O NotNull)
  def DOB = column[Option[java.sql.Date]]("DOB", O Nullable)
  def Age = column[Option[Double]]("Age", O Nullable)
  def ProfileImage = column[Option[String]]("ProfileImageURL", O Nullable)
  //  def BloodGroup = column[Option[String]]("BloodGroup",O Nullable)
  def FamilyHdIND = column[Option[String]]("FamilyHdIND", O Nullable)
  def FamilyHdID = column[Option[Long]]("FamilyHdID", O Nullable)
  def FamilyRltnType = column[Option[String]]("FamilyRltnType", O Nullable)
  def PatientLoginID = column[Option[String]]("PatientLoginID", O Nullable)
  //def PatientPassword = column[String]("PatientPassword", O Nullable)
  //def PinPassCode = column[Option[Int]]("PinPassCode",O Nullable)
  def TermsAccepted = column[String]("TermsAccepted", O NotNull)
  def PatientGroup = column[Option[Map[String, String]]]("PatientGroup", O Nullable)
  // def Created = column[Option[java.sql.Date]]("Created",O.AutoInc,O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  //def modified = column[Option[java.sql.Date]]("modified", O Nullable)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (Title, FirstName, LastName, FullName, PatientID, AadhaarID, Gender, DOB, Age, ProfileImage,
    FamilyHdIND, FamilyHdID, FamilyRltnType, PatientLoginID,
    TermsAccepted, PatientGroup, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TPatient.apply _).tupled, TPatient.unapply)

}

object Patients {
  def patients =
    scala.slick.driver.PostgresDriver.simple.TableQuery[Patient]
}
