package models.com.hcue.doctors.slick.transactTable.GeneralStat


import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class Treportfilesource(
    
  reportid:     Long,
  createddate:  Option[java.sql.Date],
  reportname:   String,
  startdate:    Option[java.sql.Date],
  enddate:      Option[java.sql.Date],
  CrtUSR:       Long,
  downloadurl:  Option[String])
  
    
  

class Report(tag: Tag) extends Table[Treportfilesource](tag, "treportfilesource") {

  def reportid = column[Long]("reportid", O.PrimaryKey, O.AutoInc)
  def createddate = column[Option[java.sql.Date]]("createddate", O Nullable)
  def reportname = column[String]("reportname", O NotNull)
  def startdate = column[Option[java.sql.Date]]("startdate", O Nullable)
  def enddate = column[Option[java.sql.Date]]("enddate", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def downloadurl = column[Option[String]]("downloadurl", O Nullable)

  def * = (reportid, createddate, reportname, startdate, enddate, CrtUSR, downloadurl).<>((Treportfilesource.apply _).tupled, Treportfilesource.unapply)

}

object Report {
  def Report =
    scala.slick.driver.PostgresDriver.simple.TableQuery[Report]
}
