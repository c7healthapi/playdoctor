package models.com.hcue.doctors.slick.transactTable.doctor.search

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType
// file not in use
case class TDoctorAddressSearch(
  DoctorID:       Long,
  AddressID:      Long,
  ClinicName:     Option[String],
  Address1:       Option[String],
  Address2:       Option[String],
  Street:         Option[String],
  Location:       Option[String],
  CityTown:       Option[String],
  DistrictRegion: Option[String],
  State:          Option[String],
  PinCode:        Option[Int],
  Country:        Option[String],
  AddressType:    Option[String])

class DoctorAddressSearch(tag: Tag) extends Table[TDoctorAddressSearch](tag, "tDoctorAddress") {

  def DoctorID = column[Long]("DoctorID", O Nullable)
  def AddressID = column[Long]("AddressID", O Nullable)
  def ClinicName = column[Option[String]]("ClinicName", O Nullable)
  def Address1 = column[Option[String]]("Address1", O Nullable)
  def Address2 = column[Option[String]]("Address2", O Nullable)
  def Street = column[Option[String]]("Street", O Nullable)
  def Location = column[Option[String]]("Location", O Nullable)
  def CityTown = column[Option[String]]("CityTown", O Nullable)
  def DistrictRegion = column[Option[String]]("DistrictRegion", O Nullable)
  def State = column[Option[String]]("State", O Nullable)
  def PinCode = column[Option[Int]]("PinCode", O Nullable)
  def Country = column[Option[String]]("Country", O Nullable)
  def AddressType = column[Option[String]]("AddressType", O Nullable)

  def * = (DoctorID, AddressID, ClinicName, Address1, Address2, Street, Location, CityTown, DistrictRegion, State, PinCode, Country, AddressType).<>((TDoctorAddressSearch.apply _).tupled, TDoctorAddressSearch.unapply)

}

object DoctorAddressSearch {

  def DoctorAddressSearch =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorAddressSearch]

}





