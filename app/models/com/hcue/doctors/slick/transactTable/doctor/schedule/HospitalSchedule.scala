package models.com.hcue.doctor.slick.transactTable.doctor.schedule

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.dateColumnType
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class THospitalSchedule(
  AddressConsultID:       Long,
  HospitalID:             Long,
  SonographerDoctorID:    Long,
  SupportworkerDoctorID:  Long,
  SonographerAddressID:   Long,
  SupportworkerAddressID: Long,
  DayCD:                  String,
  ScheduleDate:           java.sql.Date,
  MinPerCase:             Option[Int],
  Active:                 Option[String],
  BookFlag:               Option[String],
  Slot:                   String,
  RoomCostStatus:         String,
  CrtUSR:                 Long,
  CrtUSRType:             String,
  UpdtUSR:                Option[Long],
  UpdtUSRType:            Option[String])

class HospitalSchedule(tag: Tag) extends Table[THospitalSchedule](tag, "tHospitalSchedule") {

  def AddressConsultID = column[Long]("AddressConsultID", O.PrimaryKey, O.AutoInc)
  def HospitalID = column[Long]("HospitalID", O NotNull)
  def SonographerDoctorID = column[Long]("SonographerDoctorID", O NotNull)
  def SupportworkerDoctorID = column[Long]("SupportworkerDoctorID", O NotNull)
  def SonographerAddressID = column[Long]("SonographerAddressID", O NotNull)
  def SupportworkerAddressID = column[Long]("SupportworkerAddressID", O NotNull)
  def DayCD = column[String]("DayCD", O NotNull)
  def ScheduleDate = column[java.sql.Date]("ScheduleDate", O NotNull)
  def MinPerCase = column[Option[Int]]("MinPerCase", O Nullable)
  def Active = column[Option[String]]("Active", O Nullable)
  def BookFlag = column[Option[String]]("BookFlag", O Nullable)
  def Slot = column[String]("Slot", O NotNull)
  def RoomCostStatus = column[String]("RoomCostStatus", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O.Nullable)

  def * = (AddressConsultID, HospitalID, SonographerDoctorID, SupportworkerDoctorID, SonographerAddressID, SupportworkerAddressID, DayCD, ScheduleDate, MinPerCase, Active, BookFlag, Slot, RoomCostStatus, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THospitalSchedule.apply _).tupled, THospitalSchedule.unapply)

}

object HospitalSchedule {
  def HospitalSchedule =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalSchedule]
}
