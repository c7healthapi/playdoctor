package models.com.hcue.doctors.slick.transactTable.doctor.catagory

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctorCatagorySetting(
  CatagoryID:    Long,
  CatagoryDesc:  String,
  CatagoryNotes: Option[String],
  DisplayName:   Option[String],
  ActiveIND:     String,
  DoctorID:      Option[Long],
  HospitalCD:    Option[String],
  HospitalID:    Option[Long],
  CrtUSR:        Long,
  CrtUSRType:    String,
  UpdtUSR:       Option[Long],
  UpdtUSRType:   Option[String])

class DoctorCatagorySetting(tag: Tag) extends Table[TDoctorCatagorySetting](tag, "tDoctorCatagorySetting") {

  def CatagoryID = column[Long]("CatagoryID", O NotNull, O.AutoInc)
  def CatagoryDesc = column[String]("CatagoryNotes", O NotNull)
  def CatagoryNotes = column[Option[String]]("CatagoryDesc", O NotNull)
  def DisplayName = column[Option[String]]("DisplayName", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def DoctorID = column[Option[Long]]("DoctorID", O Nullable)
  def HospitalCD = column[Option[String]]("HospitalCD", O Nullable)
  def HospitalID = column[Option[Long]]("HospitalID", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (CatagoryID, CatagoryDesc, CatagoryNotes, DisplayName, ActiveIND, DoctorID, HospitalCD, HospitalID, CrtUSR, CrtUSRType, UpdtUSR,
    UpdtUSRType).<>((TDoctorCatagorySetting.apply _).tupled, TDoctorCatagorySetting.unapply)

}

object DoctorCatagorySetting {
  def DoctorCatagorySetting = scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorCatagorySetting]

}