package models.com.hcue.doctors.slick.transactTable.doctor.profile

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorQualf(
  DoctorID:        Long,
  RowID:           Int,
  EducationName:   String,
  InstituteName:   String,
  YearOfPassedOut: String,
  CrtUSR:          Long,
  CrtUSRType:      String,
  UpdtUSR:         Option[Long],
  UpdtUSRType:     Option[String])

class DoctorQualf(tag: Tag) extends Table[TDoctorQualf](tag, "tDoctorQualf") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def RowID = column[Int]("RowID", O NotNull)
  def EducationName = column[String]("EducationName", O NotNull)
  def InstituteName = column[String]("InstituteName", O NotNull)
  def YearOfPassedOut = column[String]("YearOfPassedOut", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (DoctorID, RowID, EducationName, InstituteName, YearOfPassedOut, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorQualf.apply _).tupled, TDoctorQualf.unapply)

}

object DoctorQualfs {
  def DoctorQualfs =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorQualf]
}
