package models.com.hcue.doctors.slick.transactTable.doctor

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.dateColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class THospitalAppointment(
  AppointmentID:       Long,
  AddressConsultID:    Long,
  DayCD:               String,
  ScheduleDate:        java.sql.Date,
  StartTime:           String,
  EndTime:             String,
  EmergencyStartTime:  String,
  EmergencyEndTime:    String,
  DoctorID:            Option[Long],
  PatientID:           Option[Long],
  AppointmentStatus:   String,
  TokenNumber:         String,
  ParentAppointmentID: Long,
  HospitalID:          Option[Long],
  HospitalCode:        Option[String],
  BranchCode:          Option[String],
  Patientenquiryid:    Option[Long],
  CrtUSR:              Long,
  CrtUSRType:          String,
  UpdtUSR:             Option[Long],
  UpdtUSRType:         Option[String],
  dienestype : Option[String])

class HospitalAppointment(tag: Tag) extends Table[THospitalAppointment](tag, "tHospitalAppointment") {

  def AppointmentID = column[Long]("AppointmentID", O.PrimaryKey, O.AutoInc)
  def AddressConsultID = column[Long]("AddressConsultID", O NotNull)
  def DayCD = column[String]("DayCD", O NotNull)
  def ScheduleDate = column[java.sql.Date]("ScheduleDate", O NotNull)
  def StartTime = column[String]("StartTime", O NotNull)
  def EndTime = column[String]("EndTime", O NotNull)
  def EmergencyStartTime = column[String]("EmergencyStartTime", O NotNull)
  def EmergencyEndTime = column[String]("EmergencyEndTime", O NotNull)
  def DoctorID = column[Option[Long]]("DoctorID", O NotNull)
  def PatientID = column[Option[Long]]("PatientID", O Nullable)
  def AppointmentStatus = column[String]("AppointmentStatus", O NotNull)
  def TokenNumber = column[String]("TokenNumber", O NotNull)
  def ParentAppointmentID = column[Long]("ParentAppointmentID", O NotNull)
  def HospitalID = column[Option[Long]]("HospitalID", O NotNull)
  def HospitalCode = column[Option[String]]("HospitalCode", O Nullable)
  def BranchCode = column[Option[String]]("BranchCode", O Nullable)
  def Patientenquiryid = column[Option[Long]]("Patientenquiryid", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def dienestype = column[Option[String]]("dienestype", O Nullable)

  def * = (AppointmentID, AddressConsultID, DayCD, ScheduleDate, StartTime, EndTime, EmergencyStartTime, EmergencyEndTime, DoctorID, PatientID,
    AppointmentStatus, TokenNumber, ParentAppointmentID, HospitalID,
    HospitalCode, BranchCode, Patientenquiryid, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType,dienestype).<>((THospitalAppointment.apply _).tupled, THospitalAppointment.unapply)

}

object HospitalAppointment {
  def HospitalAppointment =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalAppointment]
}
