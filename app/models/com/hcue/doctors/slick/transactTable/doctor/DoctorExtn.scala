package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.floatColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.jsonTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import play.api.libs.json.JsValue

case class TDoctorExtn(
  DoctorID:         Long,
  Qualification:    Option[Map[String, String]],
  About:            Option[Map[String, String]],
  HcueScore:        Option[Float],
  Awards:           Option[Map[String, String]],
  Membership:       Option[Map[String, String]],
  Services:         Option[Map[String, String]],
  MemberID:         Option[Map[String, String]],
  Prospect:         Option[String],
  ReferredBy:       Option[Long],
  ProfilImage:      Option[String],
  ProfilImageExpDt: Option[java.sql.Date],
  CareerImages:     Option[Map[String, String]],
  CareerDocuments:  Option[JsValue],
  CareerImageExpDt: Option[java.sql.Date],
  DoctorSettings:   Option[Map[String, String]],
  CrtUSR:           Long,
  CrtUSRType:       String,
  UpdtUSR:          Option[Long],
  UpdtUSRType:      Option[String])

class DoctorExtn(tag: Tag) extends Table[TDoctorExtn](tag, "tDoctorExtn") {

  def DoctorID = column[Long]("DoctorID", O.PrimaryKey, O NotNull)
  def Qualification = column[Option[Map[String, String]]]("Qualification", O Nullable)
  def About = column[Option[Map[String, String]]]("About", O Nullable)
  def HcueScore = column[Option[Float]]("HcueScore", O Nullable)
  def Awards = column[Option[Map[String, String]]]("Awards", O NotNull)
  def Membership = column[Option[Map[String, String]]]("Membership", O NotNull)
  def Services = column[Option[Map[String, String]]]("Services", O Nullable)
  def MemberID = column[Option[Map[String, String]]]("MemberID", O Nullable)
  def Prospect = column[Option[String]]("Prospect", O Nullable)
  def ReferredBy = column[Option[Long]]("ReferredBy", O Nullable)
  def ProfilImage = column[Option[String]]("ProfileImageURL", O Nullable)
  def ProfilImageExpDt = column[Option[java.sql.Date]]("ProfileImageExpDt", O Nullable)
  def CareerImages = column[Option[Map[String, String]]]("CareerImages", O Nullable)
  def CareerDocuments = column[Option[JsValue]]("CareerDocuments", O Nullable)
  def CareerImageExpDt = column[Option[java.sql.Date]]("CareerImageExpDt", O Nullable)
  def DoctorSettings = column[Option[Map[String, String]]]("Preference", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (DoctorID, Qualification, About, HcueScore, Awards, Membership, Services, MemberID, Prospect, ReferredBy, ProfilImage, ProfilImageExpDt, CareerImages, CareerDocuments, CareerImageExpDt, DoctorSettings, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorExtn.apply _).tupled, TDoctorExtn.unapply)

}

object DoctorsExtn {
  def DoctorsExtn =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorExtn]
}
