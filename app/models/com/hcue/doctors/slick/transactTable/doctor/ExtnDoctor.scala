package models.com.hcue.doctor.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.booleanColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType

case class TExtnDoctor(
  DoctorID:             Long,
  PracticeDoctorActive: Option[Boolean])

class ExtnDoctor(tag: Tag) extends Table[TExtnDoctor](tag, "tDoctor") {

  def DoctorID = column[Long]("DoctorID")
  def PracticeDoctorActive = column[Option[Boolean]]("PracticeDoctorActive", O.Nullable)

  def * = (DoctorID, PracticeDoctorActive).<>((TExtnDoctor.apply _).tupled, TExtnDoctor.unapply)
}

object ExtnDoctor {
  def ExtnDoctor = scala.slick.driver.PostgresDriver.simple.TableQuery[ExtnDoctor]
}