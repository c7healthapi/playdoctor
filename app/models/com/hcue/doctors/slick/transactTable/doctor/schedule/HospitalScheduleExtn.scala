package models.com.hcue.doctor.slick.transactTable.doctor.schedule

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class THospitalScheduleExtn(
  AddressConsultID:     Long,
  SonographerDoctorID:  Long,
  SonographerAddressID: Long,
  HospitalID:           Long,
  DayCD:                String,
  MinPerCase:           Option[Int],
  FromTime1:            Option[String],
  ToTime1:              Option[String],
  FromBreakTime1:       Option[String],
  ToBreakTime1:         Option[String],
  FromTime2:            Option[String],
  ToTime2:              Option[String],
  FromBreakTime2:       Option[String],
  ToBreakTime2:         Option[String],
  FromTime3:            Option[String],
  ToTime3:              Option[String],
  FromBreakTime3:       Option[String],
  ToBreakTime3:         Option[String],
  Active:               Option[String])

class HospitalScheduleExtn(tag: Tag) extends Table[THospitalScheduleExtn](tag, "tHospitalSchedule") {

  def AddressConsultID = column[Long]("AddressConsultID", O.PrimaryKey)
  def SonographerDoctorID = column[Long]("SonographerDoctorID", O NotNull)
  def SonographerAddressID = column[Long]("SonographerAddressID", O NotNull)
  def HospitalID = column[Long]("HospitalID", O NotNull)
  def DayCD = column[String]("DayCD", O NotNull)
  def MinPerCase = column[Option[Int]]("MinPerCase", O Nullable)
  def FromTime1 = column[Option[String]]("FromTime1", O NotNull)
  def ToTime1 = column[Option[String]]("ToTime1", O NotNull)
  def FromBreakTime1 = column[Option[String]]("FromBreakTime1", O NotNull)
  def ToBreakTime1 = column[Option[String]]("ToBreakTime1", O NotNull)
  def FromTime2 = column[Option[String]]("FromTime2", O NotNull)
  def ToTime2 = column[Option[String]]("ToTime2", O NotNull)
  def FromBreakTime2 = column[Option[String]]("FromBreakTime2", O NotNull)
  def ToBreakTime2 = column[Option[String]]("ToBreakTime2", O NotNull)
  def FromTime3 = column[Option[String]]("FromTime3", O NotNull)
  def ToTime3 = column[Option[String]]("ToTime3", O NotNull)
  def FromBreakTime3 = column[Option[String]]("FromBreakTime3", O NotNull)
  def ToBreakTime3 = column[Option[String]]("ToBreakTime3", O NotNull)
  def Active = column[Option[String]]("Active", O Nullable)

  def * = (AddressConsultID, SonographerDoctorID, SonographerAddressID, HospitalID, DayCD, MinPerCase, FromTime1, ToTime1, FromBreakTime1, ToBreakTime1, FromTime2, ToTime2, FromBreakTime2, ToBreakTime2, FromTime3, ToTime3, FromBreakTime3, ToBreakTime3, Active).<>((THospitalScheduleExtn.apply _).tupled, THospitalScheduleExtn.unapply)

}

object HospitalScheduleExtn {
  def HospitalScheduleExtn =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalScheduleExtn]
}
