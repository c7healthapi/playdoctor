package models.com.hcue.doctors.slick.transactTable.doctor.login

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TAccessToken(
  Token:       String,
  TTL:         String,
  UserID:      Option[Long],
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class AccessToken(tag: Tag) extends Table[TAccessToken](tag, "tAccessToken") {

  def Token = column[String]("Token", O.PrimaryKey)
  def TTL = column[String]("TTL", O Nullable)
  def UserID = column[Option[Long]]("UserID", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (Token, TTL, UserID, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TAccessToken.apply _).tupled, TAccessToken.unapply)

}

object AccessToken {
  def AccessToken =
    scala.slick.driver.PostgresDriver.simple.TableQuery[AccessToken]
}
