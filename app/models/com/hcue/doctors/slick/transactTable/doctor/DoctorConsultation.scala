package models.com.hcue.doctors.slick.transactTable.doctor

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorConsultation(
  AddressConsultID: Long,
  AddressID:        Long,
  DoctorID:         Long,
  DayCD:            String,
  StartTime:        String,
  EndTime:          String,
  MinPerCase:       Option[Int],
  Fees:             Option[Int],
  Active:           Option[String],
  CrtUSR:           Long,
  CrtUSRType:       String,
  UpdtUSR:          Option[Long],
  UpdtUSRType:      Option[String])

class DoctorConsultation(tag: Tag) extends Table[TDoctorConsultation](tag, "tDoctorConsultation") {

  def AddressConsultID = column[Long]("AddressConsultID", O.PrimaryKey, O.AutoInc)
  def AddressID = column[Long]("AddressID", O NotNull)
  def DoctorID = column[Long]("DoctorID", O NotNull)
  def DayCD = column[String]("DayCD", O NotNull)
  def StartTime = column[String]("StartTime", O NotNull)
  def EndTime = column[String]("EndTime", O NotNull)
  def MinPerCase = column[Option[Int]]("MinPerCase", O Nullable)
  def Fees = column[Option[Int]]("Fees", O Nullable)
  def Active = column[Option[String]]("Active", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O.Nullable)

  def * = (AddressConsultID, AddressID, DoctorID, DayCD, StartTime, EndTime, MinPerCase, Fees, Active, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorConsultation.apply _).tupled, TDoctorConsultation.unapply)

}

object DoctorConsultations {
  def DoctorConsultations = scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorConsultation]
}
