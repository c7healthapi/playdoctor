package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.bigDecimalColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.floatColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.intColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.jsonTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import play.api.libs.json.JsValue

case class TDoctorAddressExtn(
  DoctorID:         Long,
  HospitalID:       Option[Long],
  ParentHospitalID: Option[Long],
  HospitalCode:     Option[String],
  //BranchCode: Option[String],
  AddressID:        Long,
  PrimaryIND:       String,
  Latitude:         Option[String],
  Longitude:        Option[String],
  ClinicImages:     Option[Map[String, String]],
  ClinicDocuments:  Option[JsValue],
  ClinicImageExpDt: Option[java.sql.Date],
  RoleID:           Option[String],
  GroupID:          Option[String],
  IsConsultant:     Option[String],
  RatePerHour:      Option[Float],
  MinPerCase:       Option[Int],
  CrtUSR:           Long,
  CrtUSRType:       String,
  UpdtUSR:          Option[Long],
  UpdtUSRType:      Option[String],
  RateBelowTwenty:  Option[BigDecimal],
  RateAboveTwenty:  Option[BigDecimal])

class DoctorsAddressExtn(tag: Tag) extends Table[TDoctorAddressExtn](tag, "tDoctorAddressExtn") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def HospitalID = column[Option[Long]]("HospitalID", O Nullable)
  def ParentHospitalID = column[Option[Long]]("ParentHospitalID", O Nullable)
  def HospitalCode = column[Option[String]]("HospitalCode", O Nullable)
  //def BranchCode = column [Option[String]]("BranchCode",O Nullable)
  def AddressID = column[Long]("AddressID", O NotNull)
  def PrimaryIND = column[String]("PrimaryIND", O NotNull)
  def Latitude = column[Option[String]]("Latitude", O Nullable)
  def Longitude = column[Option[String]]("Longitude", O Nullable)
  def ClinicImages = column[Option[Map[String, String]]]("ClinicImages", O Nullable)
  def ClinicDocuments = column[Option[JsValue]]("ClinicDocuments", O Nullable)
  def ClinicImageExpDt = column[Option[java.sql.Date]]("ClinicImageExpDt", O Nullable)
  def RoleID = column[Option[String]]("RoleID", O.Nullable)
  def GroupID = column[Option[String]]("GrpCD", O.Nullable)
  def IsConsultant = column[Option[String]]("IsConsultant", O.Nullable)
  def RatePerHour = column[Option[Float]]("RatePerHour", O Nullable)
  def MinPerCase = column[Option[Int]]("MinPerCase", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def RateBelowTwenty = column[Option[BigDecimal]]("RateBelowTwenty", O Nullable)
  def RateAboveTwenty = column[Option[BigDecimal]]("RateAboveTwenty", O Nullable)

  def * = (DoctorID, HospitalID, ParentHospitalID, HospitalCode, AddressID, PrimaryIND, Latitude, Longitude,
    ClinicImages, ClinicDocuments, ClinicImageExpDt, RoleID, GroupID, IsConsultant, RatePerHour, MinPerCase, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType, RateBelowTwenty, RateAboveTwenty).<>((TDoctorAddressExtn.apply _).tupled, TDoctorAddressExtn.unapply)

}

object DoctorsAddressExtn {

  def DoctorsAddressExtn =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorsAddressExtn]

}
