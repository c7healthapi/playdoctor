package models.com.hcue.doctors.slick.transactTable.doctor

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorAddressPhone(
  AddressID:     Long,
  DoctorID:      Long,
  PhCntryCD:     String,
  PhStateCD:     String,
  PhAreaCD:      String,
  PhNumber:      String,
  PhType:        String,
  RowID:         Int,
  PrimaryIND:    String,
  PublicDisplay: String,
  CrtUSR:        Long,
  CrtUSRType:    String,
  UpdtUSR:       Option[Long],
  UpdtUSRType:   Option[String])

class DoctorAddressPhone(tag: Tag) extends Table[TDoctorAddressPhone](tag, "tDoctorAddressPhone") {
  def AddressID = column[Long]("AddressID", O NotNull)
  def DoctorID = column[Long]("DoctorID", O NotNull)
  def PhCntryCD = column[String]("PhCntryCD", O NotNull)
  def PhStateCD = column[String]("PhStateCD", O NotNull)
  def PhAreaCD = column[String]("PhAreaCD", O NotNull)
  def PhNumber = column[String]("PhNumber", O NotNull)
  def PhType = column[String]("PhType", O NotNull)
  def RowID = column[Int]("RowID", O NotNull)
  def PrimaryIND = column[String]("PrimaryIND", O NotNull)
  def PublicDisplay = column[String]("PublicDisplay", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (AddressID, DoctorID, PhCntryCD, PhStateCD, PhAreaCD, PhNumber, PhType, RowID, PrimaryIND, PublicDisplay, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorAddressPhone.apply _).tupled, TDoctorAddressPhone.unapply)

}

object DoctorAddressPhone {
  def DoctorAddressPhone =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorAddressPhone]
}