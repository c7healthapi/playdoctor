package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctorAddress(
  DoctorID:       Long,
  AddressID:      Long,
  ClinicName:     Option[String],
  ClinicSettings: Option[Map[String, String]],
  Address1:       Option[String],
  Address2:       Option[String],
  Street:         Option[String],
  Location:       Option[String],
  CityTown:       Option[String],
  DistrictRegion: Option[Long],
  State:          Option[String],
  PostCode:       Option[String],
  Country:        Option[String],
  AddressType:    String,
  LandMark:       Option[String],
  Active:         String,
  CrtUSR:         Long,
  CrtUSRType:     String,
  UpdtUSR:        Option[Long],
  UpdtUSRType:    Option[String],
  ShowTimings:    Option[String],
  Address4:       Option[String])

class DoctorsAddress(tag: Tag) extends Table[TDoctorAddress](tag, "tDoctorAddress") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def AddressID = column[Long]("AddressID", O.PrimaryKey, O.AutoInc)
  def ClinicName = column[Option[String]]("ClinicName", O Nullable)
  def ClinicSettings = column[Option[Map[String, String]]]("ClinicSettings", O Nullable)
  def Address1 = column[Option[String]]("Address1", O Nullable)
  def Address2 = column[Option[String]]("Address2", O Nullable)
  def Street = column[Option[String]]("Street", O Nullable)
  def Location = column[Option[String]]("Location", O Nullable)
  def CityTown = column[Option[String]]("CityTown", O NotNull)
  def DistrictRegion = column[Option[Long]]("DistrictRegion", O Nullable)
  def State = column[Option[String]]("State", O NotNull)
  def PostCode = column[Option[String]]("PostCode", O NotNull)
  def Country = column[Option[String]]("Country", O NotNull)
  def AddressType = column[String]("AddressType", O NotNull)
  def LandMark = column[Option[String]]("LandMark", O NotNull)
  def Active = column[String]("ActiveIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def ShowTimings = column[Option[String]]("ShowTimings", O Nullable)
  def Address4 = column[Option[String]]("Address4", O Nullable)
  //def uniQue = primaryKey("tDoctorAddress_Ckey", (DoctorID, AddressID))

  def * = (DoctorID, AddressID, ClinicName, ClinicSettings, Address1, Address2, Street, Location, CityTown, DistrictRegion, State, PostCode, Country, AddressType, LandMark, Active, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType, ShowTimings, Address4).<>((TDoctorAddress.apply _).tupled, TDoctorAddress.unapply)

}

object DoctorAddress {
  def DoctorsAddress =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorsAddress]
}
