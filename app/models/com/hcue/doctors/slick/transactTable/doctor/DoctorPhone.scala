package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.intColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctorPhones(
  DoctorID:     Long,
  PhCntryCD:    Option[Int],
  PhStateCD:    Option[Int],
  PhAreaCD:     Option[Int],
  PhNumber:     String,
  PhType:       String,
  PrimaryIND:   String,
  CrtUSR:       Long,
  CrtUSRType:   String,
  UpdtUSR:      Option[Long],
  UpdtUSRType:  Option[String],
  AuditTracker: Option[Map[String, String]])

class DoctorsPhones(tag: Tag) extends Table[TDoctorPhones](tag, "tDoctorPhone") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def PhCntryCD = column[Option[Int]]("PhCntryCD", O Nullable)
  def PhStateCD = column[Option[Int]]("PhStateCD", O Nullable)
  def PhAreaCD = column[Option[Int]]("PhAreaCD", O Nullable)
  def PhNumber = column[String]("PhNumber", O NotNull)
  def PhType = column[String]("PhType", O NotNull)
  def PrimaryIND = column[String]("PrimaryIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def AuditTracker = column[Option[Map[String, String]]]("AuditTracker", O.Nullable)
  def uniQue = primaryKey("tDoctorPhone_Ckey", (DoctorID, PhType))

  def * = (DoctorID, PhCntryCD, PhStateCD, PhAreaCD, PhNumber, PhType, PrimaryIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType, AuditTracker).<>((TDoctorPhones.apply _).tupled, TDoctorPhones.unapply)

}

object DoctorPhones {

  def DoctorsPhones =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorsPhones]

}