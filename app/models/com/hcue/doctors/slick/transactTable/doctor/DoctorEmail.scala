package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctorEmail(
  DoctorID:     Long,
  EmailID:      String,
  EmailIDType:  String,
  PrimaryIND:   String,
  ValidIND:     String,
  CrtUSR:       Long,
  CrtUSRType:   String,
  UpdtUSR:      Option[Long],
  UpdtUSRType:  Option[String],
  AuditTracker: Option[Map[String, String]])

class DoctorEmail(tag: Tag) extends Table[TDoctorEmail](tag, "tDoctorEmail") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  // def AddressID = column[Long]("AddressID", O NotNull)
  def EmailID = column[String]("EmailID", O NotNull)
  def EmailIDType = column[String]("EmailIDType", O NotNull)
  def PrimaryIND = column[String]("PrimaryIND", O NotNull)
  def ValidIND = column[String]("ValidIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O.Nullable)
  def AuditTracker = column[Option[Map[String, String]]]("AuditTracker", O.Nullable)
  def uniQue = primaryKey("tDoctorEmail_Ckey", (DoctorID, EmailIDType))

  def * = (DoctorID, EmailID, EmailIDType, PrimaryIND, ValidIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType, AuditTracker).<>((TDoctorEmail.apply _).tupled, TDoctorEmail.unapply)

}

object DoctorEmail {
  def DoctorEmails =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorEmail]
}