package models.com.hcue.doctors.slick.transactTable.doctor.profile

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctorPublicAchieve(
  DoctorID:    Long,
  RowID:       Long,
  Name:        String,
  Details:     Option[Map[String, String]],
  Year:        String,
  Indicator:   String,
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class DoctorPublicAchieve(tag: Tag) extends Table[TDoctorPublicAchieve](tag, "tDoctorPublicAchieve") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def RowID = column[Long]("RowID", O NotNull)
  def Name = column[String]("Name", O NotNull)
  def Details = column[Option[Map[String, String]]]("Details", O Nullable)
  def Year = column[String]("Year", O NotNull)
  def Indicator = column[String]("Indicator", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (DoctorID, RowID, Name, Details, Year, Indicator, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorPublicAchieve.apply _).tupled, TDoctorPublicAchieve.unapply)

}

object DoctorPublicAchieve {
  def DoctorPublicAchieve =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorPublicAchieve]
}
