package models.com.hcue.doctors.slick.transactTable.doctor

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TreferrerEnquiry(
  patientid:        Long,
  ccgid:            Option[Long],
  ccgname:          Option[String],
  practiceid:       Option[Long],
  practicename:     Option[String],
  doctorid:         Option[Long],
  doctorname:       Option[String],
  patientenquiryid: Option[Long])

class ReferrerEnquiry(tag: Tag) extends Table[TreferrerEnquiry](tag, "treferrerenquiry") {

  def patientid = column[Long]("patientid", O NotNull)
  def ccgid = column[Option[Long]]("ccgid", O Nullable)
  def ccgname = column[Option[String]]("ccgname", O Nullable)
  def practiceid = column[Option[Long]]("practiceid", O Nullable)
  def practicename = column[Option[String]]("practicename", O.PrimaryKey, O Nullable)
  def doctorid = column[Option[Long]]("doctorid", O Nullable)
  def doctorname = column[Option[String]]("doctorname", O Nullable)
  def patientenquiryid = column[Option[Long]]("patientenquiryid", O Nullable)

  def * = (patientid, ccgid, ccgname, practiceid, practicename, doctorid, doctorname, patientenquiryid).<>((TreferrerEnquiry.apply _).tupled, TreferrerEnquiry.unapply)

}

object ReferrerEnquiry {
  def ReferrerEnquiry =
    scala.slick.driver.PostgresDriver.simple.TableQuery[ReferrerEnquiry]
}