package models.com.hcue.doctors.slick.transactTable.doctor.lab

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorLab(
  DoctorLabID: Long,
  DoctorID:    Long,
  LabID:       Long,
  PrimaryIND:  String,
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class DoctorLab(tag: Tag) extends Table[TDoctorLab](tag, "tDoctorLab") {

  def DoctorLabID = column[Long]("DoctorLabID", O.PrimaryKey, O.AutoInc)
  def DoctorID = column[Long]("DoctorID", O NotNull)
  def LabID = column[Long]("LabID", O NotNull)
  def PrimaryIND = column[String]("PrimaryIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O.Nullable)

  def * = (DoctorLabID, DoctorID, LabID, PrimaryIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorLab.apply _).tupled, TDoctorLab.unapply)

}

object DoctorLab {
  def DoctorLab =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorLab]
}
