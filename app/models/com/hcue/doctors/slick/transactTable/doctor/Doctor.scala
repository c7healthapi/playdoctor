package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.intColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctor(
  FirstName:      String,
  LastName:       Option[String],
  FullName:       String,
  DoctorID:       Long,
  Gender:         Option[String],
  DOB:            Option[java.sql.Date],
  DoctorLoginID:  Option[String],
  SpecialityCD:   Option[Map[String, String]],
  Exp:            Option[Int],
  TermsAccepted:  String,
  SEOName:        Option[String],
  Searchable:     Option[String],
  RegistrationNo: Option[String],
  CrtUSR:         Long,
  CrtUSRType:     String,
  UpdtUSR:        Option[Long],
  UpdtUSRType:    Option[String],
  faxno:          Option[String],
  gpcode:         Option[String],
  typeval:        Option[String],
  notes:          Option[String],
  Title:          Option[String])

class Doctor(tag: Tag) extends Table[TDoctor](tag, "tDoctor") {

  def FirstName = column[String]("FirstName", O NotNull)
  def LastName = column[Option[String]]("LastName", O Nullable)
  def FullName = column[String]("FullName", O NotNull)
  def DoctorID = column[Long]("DoctorID", O.PrimaryKey, O.AutoInc)
  def Gender = column[Option[String]]("Gender")
  def DOB = column[Option[java.sql.Date]]("DOB")
  def DoctorLoginID = column[Option[String]]("DoctorLoginID")
  def SpecialityCD = column[Option[Map[String, String]]]("SpecialityCD", O NotNull)
  def Exp = column[Option[Int]]("Exp", O NotNull)
  def TermsAccepted = column[String]("TermsAccepted", O NotNull)
  def SEOName = column[Option[String]]("SEOName", O.Nullable)
  def Searchable = column[Option[String]]("Searchable", O.Nullable)
  def RegistrationNo = column[Option[String]]("RegistrationNo", O.Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def faxno = column[Option[String]]("faxno", O.Nullable)
  def gpcode = column[Option[String]]("gpcode", O.Nullable)
  def typeval = column[Option[String]]("typeval", O.Nullable)
  def notes = column[Option[String]]("notes", O.Nullable)
  def Title = column[Option[String]]("Title", O.Nullable)

  def * = (FirstName, LastName, FullName, DoctorID, Gender, DOB, DoctorLoginID, SpecialityCD, Exp, TermsAccepted, SEOName, Searchable, RegistrationNo, CrtUSR, CrtUSRType, UpdtUSR,
    UpdtUSRType, faxno, gpcode, typeval, notes, Title).<>((TDoctor.apply _).tupled, TDoctor.unapply)
}

object Doctors {
  def Doctors = scala.slick.driver.PostgresDriver.simple.TableQuery[Doctor]
}
