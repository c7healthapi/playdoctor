package models.com.hcue.doctors.slick.transactTable.doctor

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.dateColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorAppointment(
  AppointmentID:    Long,
  AddressConsultID: Long,
  DayCD:            String,
  ConsultationDt:   java.sql.Date,
  StartTime:        String,
  EndTime:          String,
  PatientID:        Option[Long],
  VisitUserTypeID:  String,
  DoctorID:         Option[Long],
  FirstTimeVisit:   String,
  DoctorVisitRsnID: String,
  OtherVisitRsn:    Option[String],
  //  ActualFees: Option[Long],
  AppointmentStatus:   String,
  TokenNumber:         String,
  ParentAppointmentID: Long,
  HospitalID:          Option[Long],
  HospitalCode:        Option[String],
  BranchCode:          Option[String],
  CrtUSR:              Long,
  CrtUSRType:          String,
  UpdtUSR:             Option[Long],
  UpdtUSRType:         Option[String])

class DoctorAppointment(tag: Tag) extends Table[TDoctorAppointment](tag, "tDoctorAppointment") {

  def AppointmentID = column[Long]("AppointmentID", O.PrimaryKey, O.AutoInc)
  def AddressConsultID = column[Long]("AddressConsultID", O NotNull)
  def DayCD = column[String]("DayCD", O NotNull)
  def ConsultationDt = column[java.sql.Date]("ConsultationDt", O NotNull)
  def StartTime = column[String]("StartTime", O NotNull)
  def EndTime = column[String]("EndTime", O NotNull)
  def PatientID = column[Option[Long]]("PatientID", O Nullable)
  def VisitUserTypeID = column[String]("VisitUserTypeID", O NotNull)
  def DoctorID = column[Option[Long]]("DoctorID", O NotNull)
  def FirstTimeVisit = column[String]("FirstTimeVisit", O NotNull)
  def DoctorVisitRsnID = column[String]("DoctorVisitRsnID", O NotNull)
  def OtherVisitRsn = column[Option[String]]("OtherVisitRsn", O Nullable)
  // def ActualFees= column[Option[Long]]("ActualFees", O Nullable)
  def AppointmentStatus = column[String]("AppointmentStatus", O NotNull)
  def TokenNumber = column[String]("TokenNumber", O NotNull)
  def ParentAppointmentID = column[Long]("ParentAppointmentID", O NotNull)
  def HospitalID = column[Option[Long]]("HospitalID", O NotNull)
  def HospitalCode = column[Option[String]]("HospitalCode", O Nullable)
  def BranchCode = column[Option[String]]("BranchCode", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (AppointmentID, AddressConsultID, DayCD, ConsultationDt, StartTime, EndTime, PatientID, VisitUserTypeID, DoctorID, FirstTimeVisit,
    DoctorVisitRsnID, OtherVisitRsn, AppointmentStatus, TokenNumber, ParentAppointmentID, HospitalID,
    HospitalCode, BranchCode, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorAppointment.apply _).tupled, TDoctorAppointment.unapply)

}

object DoctorAppointments {
  def DoctorAppointments =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorAppointment]
}
