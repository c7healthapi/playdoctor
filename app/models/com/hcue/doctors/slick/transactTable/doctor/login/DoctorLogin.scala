package models.com.hcue.doctors.slick.transactTable.doctor.login

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.dateColumnType
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType
import scala.slick.driver.PostgresDriver.simple.timestampColumnType

case class TDoctorLogin(
  DoctorID:         Long,
  DoctorLoginID:    String,
  DoctorPassword:   String,
  Active:           String,
  PinPassCode:      Option[String],
  LastLogin:        Option[java.sql.Timestamp],
  LoggedIn:         String,
  PasswordExpiryDt: Option[java.sql.Date],
  LoginCount:       Option[Int])

class DoctorLogin(tag: Tag) extends Table[TDoctorLogin](tag, "tDoctorLogin") {

  def DoctorID = column[Long]("DoctorID", O.PrimaryKey)
  def DoctorLoginID = column[String]("DoctorLoginID", O NotNull)
  def DoctorPassword = column[String]("DoctorPassword", O NotNull)
  def Active = column[String]("Active", O NotNull)
  def PinPassCode = column[Option[String]]("PinPassCode", O Nullable)
  def LastLogin = column[Option[java.sql.Timestamp]]("LastLogin", O Nullable)
  def LoggedIn = column[String]("LoggedIn", O NotNull)
  def PasswordExpiryDt = column[Option[java.sql.Date]]("PasswordExpiryDt", O Nullable)
  def LoginCount = column[Option[Int]]("LoginCount", O Nullable)

  def * = (DoctorID, DoctorLoginID, DoctorPassword, Active, PinPassCode, LastLogin, LoggedIn, PasswordExpiryDt, LoginCount).<>((TDoctorLogin.apply _).tupled, TDoctorLogin.unapply)

}

object DoctorLogin {
  def DoctorLogin =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorLogin]
}
