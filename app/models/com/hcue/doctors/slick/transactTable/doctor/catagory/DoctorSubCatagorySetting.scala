package models.com.hcue.doctors.slick.transactTable.doctor.catagory

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctorSubCatagorySetting(
  SubCatagoryID:    Long,
  CatagoryID:       Long,
  SubCatagoryDesc:  String,
  SubCatagoryNotes: Option[String],
  DisplayName:      Option[String],
  ActiveIND:        String,
  DoctorID:         Option[Long],
  HospitalCD:       Option[String],
  HospitalID:       Option[Long],
  CrtUSR:           Long,
  CrtUSRType:       String,
  UpdtUSR:          Option[Long],
  UpdtUSRType:      Option[String])

class DoctorSubCatagorySetting(tag: Tag) extends Table[TDoctorSubCatagorySetting](tag, "tDoctorSubCatagorySetting") {

  def SubCatagoryID = column[Long]("SubCatagoryID", O NotNull, O.AutoInc)
  def CatagoryID = column[Long]("CatagoryID", O NotNull)
  def SubCatagoryDesc = column[String]("SubCatagoryDesc", O NotNull)
  def SubCatagoryNotes = column[Option[String]]("SubCatagoryNotes", O NotNull)
  def DisplayName = column[Option[String]]("DisplayName", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def DoctorID = column[Option[Long]]("DoctorID", O NotNull)
  def HospitalCD = column[Option[String]]("HospitalCD", O Nullable)
  def HospitalID = column[Option[Long]]("HospitalID", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (SubCatagoryID, CatagoryID, SubCatagoryDesc, SubCatagoryNotes, DisplayName, ActiveIND, DoctorID, HospitalCD, HospitalID, CrtUSR, CrtUSRType, UpdtUSR,
    UpdtUSRType).<>((TDoctorSubCatagorySetting.apply _).tupled, TDoctorSubCatagorySetting.unapply)

}

object DoctorSubCatagorySetting {
  def DoctorSubCatagorySetting = scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorSubCatagorySetting]

}