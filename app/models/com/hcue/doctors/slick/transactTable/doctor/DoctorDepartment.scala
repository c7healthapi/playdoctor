package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.intColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.jsonTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import play.api.libs.json.JsValue

case class TDoctorDepartment(
  DoctorDepartmentID:    Long,
  DoctorID:              Long,
  DepartmentID:          Long,
  SubDepartmentID:       Option[Long],
  DoctorConsultationFee: Option[Int],
  BillingStructure:      Option[JsValue],
  ActiveIND:             String,
  CrtUSR:                Long,
  CrtUSRType:            String,
  UpdtUSR:               Option[Long],
  UpdtUSRType:           Option[String])

class DoctorDepartment(tag: Tag) extends Table[TDoctorDepartment](tag, "tDoctorDepartment") {
  def DoctorDepartmentID = column[Long]("DoctorDepartmentID", O NotNull, O.AutoInc)
  def DoctorID = column[Long]("DoctorID", O NotNull)
  def DepartmentID = column[Long]("DepartmentID", O NotNull)
  def SubDepartmentID = column[Option[Long]]("SubDepartmentID", O Nullable)
  def DoctorConsultationFee = column[Option[Int]]("DoctorConsultationFee", O Nullable)
  def BillingStructure = column[Option[JsValue]]("BillingStructure", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (DoctorDepartmentID, DoctorID, DepartmentID, SubDepartmentID, DoctorConsultationFee, BillingStructure, ActiveIND, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorDepartment.apply _).tupled, TDoctorDepartment.unapply)

}

object DoctorDepartment {
  def DoctorDepartment =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorDepartment]
}
