package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.timestampColumnType

case class TDoctorAppointmentExtn(
  AppointmentID:     Long,
  AddressConsultID:  Long,
  DayCD:             String,
  ConsultationDt:    java.sql.Date,
  VisitUserTypeID:   String,
  DoctorVisitRsnID:  String,
  AppointmentStatus: String,
  TokenNumber:       String,
  ParentHospitalID:  Option[Long],
  EventDescription:  Option[String],
  EventLocation:     Option[String],
  Reason:            Option[String],
  VisitRsnID:        Option[Long],
  PreliminaryNotes:  Option[String],
  ReferralID:        Option[Long],
  SubReferralID:     Option[Long],
  CheckInTime:       Option[java.sql.Timestamp])

class DoctorAppointmentExtn(tag: Tag) extends Table[TDoctorAppointmentExtn](tag, "tDoctorAppointment") {

  def AppointmentID = column[Long]("AppointmentID", O.PrimaryKey, O.AutoInc)
  def AddressConsultID = column[Long]("AddressConsultID", O NotNull)
  def DayCD = column[String]("DayCD", O NotNull)
  def ConsultationDt = column[java.sql.Date]("ConsultationDt", O NotNull)
  def VisitUserTypeID = column[String]("VisitUserTypeID", O NotNull)
  // def FirstTimeVisit = column[String]("FirstTimeVisit", O NotNull)
  def DoctorVisitRsnID = column[String]("DoctorVisitRsnID", O NotNull)
  def AppointmentStatus = column[String]("AppointmentStatus", O NotNull)
  def TokenNumber = column[String]("TokenNumber", O NotNull)
  def ParentHospitalID = column[Option[Long]]("ParentHospitalID", O Nullable)
  def EventDescription = column[Option[String]]("EventDescription", O Nullable)
  def EventLocation = column[Option[String]]("EventLocation", O Nullable)
  def Reason = column[Option[String]]("Reason", O Nullable)
  def VisitRsnID = column[Option[Long]]("VisitRsnID", O Nullable)
  def PreliminaryNotes = column[Option[String]]("PreliminaryNotes", O Nullable)
  def ReferralID = column[Option[Long]]("ReferralID", O Nullable)
  def SubReferralID = column[Option[Long]]("SubReferralID", O Nullable)
  def CheckInTime = column[Option[java.sql.Timestamp]]("CheckInTime", O Nullable)

  def * = (AppointmentID, AddressConsultID, DayCD, ConsultationDt, VisitUserTypeID, DoctorVisitRsnID, AppointmentStatus, TokenNumber,
    ParentHospitalID, EventDescription, EventLocation, Reason, VisitRsnID, PreliminaryNotes, ReferralID, SubReferralID, CheckInTime).<>((TDoctorAppointmentExtn.apply _).tupled, TDoctorAppointmentExtn.unapply)

}

object DoctorAppointmentExtn {
  def DoctorAppointmentExtn =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorAppointmentExtn]
}
