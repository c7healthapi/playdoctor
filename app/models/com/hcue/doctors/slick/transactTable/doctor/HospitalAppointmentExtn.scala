package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THospitalAppointmentExtn(

  AppointmentID: Long,
  AddressConsultID: Long,
  DayCD : String,
  ScheduleDate: java.sql.Date,
  AppointmentStatus : String,
  TokenNumber : String,
  Notes : Option[String],
  Startscantime : Option[String],
  Completescantime : Option[String],
  ParentHospitalID : Option[Long],
  NoOfArea : Option[Long],
  UpdtUSR: Option[Long],
  UpdtUSRType: Option[String]
)
  
class HospitalAppointmentExtn (tag: Tag) extends Table[THospitalAppointmentExtn](tag, "tHospitalAppointment") {

  def AppointmentID = column[Long]("AppointmentID", O.PrimaryKey, O.AutoInc)
  def AddressConsultID = column[Long]("AddressConsultID", O NotNull)
  def DayCD = column[String]("DayCD", O NotNull)
  def ScheduleDate = column[java.sql.Date]("ScheduleDate", O NotNull)
  def AppointmentStatus = column[String]("AppointmentStatus", O NotNull)
  def TokenNumber = column[String]("TokenNumber", O NotNull)
  def Notes= column[Option[String]]("Notes", O Nullable)
  def Startscantime= column[Option[String]]("Startscantime", O Nullable)
  def Completescantime= column[Option[String]]("Completescantime", O Nullable)
  def ParentHospitalID= column[Option[Long]]("ParentHospitalID", O Nullable)
  def NoOfArea= column[Option[Long]]("NoOfArea", O Nullable)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  
 def * = (AppointmentID,AddressConsultID,DayCD,ScheduleDate,AppointmentStatus,TokenNumber,Notes,Startscantime,Completescantime,
     ParentHospitalID,NoOfArea,UpdtUSR,UpdtUSRType).<>((THospitalAppointmentExtn.apply _).tupled, THospitalAppointmentExtn.unapply)  
}

object HospitalAppointmentExtn {
  def HospitalAppointmentExtn =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalAppointmentExtn]
}
