package models.com.hcue.doctors.slick.transactTable.doctor

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorVisitRsnLkyp(
  DoctorVisitRsnID:   String,
  DoctorSpecialityID: String,
  DoctorVisitRsnDesc: String,
  VisitUserTypeID:    String,
  SequenceRow:        Int,
  ActiveIND:          String)

class DoctorVisitReason(tag: Tag) extends Table[TDoctorVisitRsnLkyp](tag, "tDoctorVisitRsnLkyp") {

  def DoctorVisitRsnID = column[String]("DoctorVisitRsnID", O NotNull)
  def DoctorSpecialityID = column[String]("DoctorSpecialityID", O NotNull)
  def DoctorVisitRsnDesc = column[String]("DoctorVisitRsnDesc", O NotNull)
  def VisitUserTypeID = column[String]("VisitUserTypeID", O NotNull)
  def SequenceRow = column[Int]("SequenceRow", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (DoctorVisitRsnID, DoctorSpecialityID, DoctorVisitRsnDesc, VisitUserTypeID, SequenceRow, ActiveIND).<>((TDoctorVisitRsnLkyp.apply _).tupled, TDoctorVisitRsnLkyp.unapply)

}

object DoctorVisitReason {
  def DoctorVisitReason =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorVisitReason]
}
