package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TDoctorSpecialityCD(
  DoctorID:             Long,
  SpecialityCD:         String,
  CustomSpecialityDesc: Option[String],
  SubSpecialityDesc:    Option[String],
  ActiveIND:            Option[String],
  SubSpecialityID:      Option[Map[String, String]])

class DoctorSpecialityCD(tag: Tag) extends Table[TDoctorSpecialityCD](tag, "tDoctorSpecialityCD") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def SpecialityCD = column[String]("SpecialityCD", O NotNull)
  def CustomSpecialityDesc = column[Option[String]]("CustomSpecialityDesc", O NotNull)
  def SubSpecialityDesc = column[Option[String]]("SubSpecialityDesc", O NotNull)
  def ActiveIND = column[Option[String]]("ActiveIND", O NotNull)
  def SubSpecialityID = column[Option[Map[String, String]]]("SubSpecialityID", O NotNull)

  def * = (DoctorID, SpecialityCD, CustomSpecialityDesc, SubSpecialityDesc, ActiveIND, SubSpecialityID).<>((TDoctorSpecialityCD.apply _).tupled, TDoctorSpecialityCD.unapply)

}

object DoctorSpecialityCD {
  def DoctorSpecialityCD = scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorSpecialityCD]

}
