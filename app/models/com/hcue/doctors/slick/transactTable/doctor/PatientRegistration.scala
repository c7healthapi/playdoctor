package models.com.hcue.doctors.slick.transactTable.doctor

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.jsonTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import play.api.libs.json.JsValue

case class TPatientRegistration(
  ID:          Long,
  Type:        String,
  ScreenType:  String,
  AppType:     String,
  RegSettings: JsValue,
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class PatientRegistration(tag: Tag) extends Table[TPatientRegistration](tag, "tRegistrationSettings") {

  def ID = column[Long]("ID", O NotNull)
  def Type = column[String]("Type", O NotNull)
  def ScreenType = column[String]("ScreenType", O NotNull)
  def AppType = column[String]("AppType", O NotNull)
  def RegSettings = column[JsValue]("RegSettings", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (ID, Type, ScreenType, AppType, RegSettings, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TPatientRegistration.apply _).tupled, TPatientRegistration.unapply)

}

object PatientRegistration {

  def PatientReg =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PatientRegistration]
}