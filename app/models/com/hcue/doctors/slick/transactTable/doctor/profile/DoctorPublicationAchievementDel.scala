package models.com.hcue.doctors.slick.transactTable.doctor.profile

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorPublicAchieveDel(
  DoctorID:  Long,
  RowID:     Long,
  Name:      String,
  Year:      String,
  Indicator: String)

class DoctorPublicAchieveDel(tag: Tag) extends Table[TDoctorPublicAchieveDel](tag, "tDoctorPublicAchieve") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def RowID = column[Long]("RowID", O NotNull)
  def Name = column[String]("Name", O NotNull)
  def Year = column[String]("Year", O NotNull)
  def Indicator = column[String]("Indicator", O NotNull)

  def * = (DoctorID, RowID, Name, Year, Indicator).<>((TDoctorPublicAchieveDel.apply _).tupled, TDoctorPublicAchieveDel.unapply)

}

object DoctorPublicAchieveDel {
  def DoctorPublicAchieveDel =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorPublicAchieveDel]
}
