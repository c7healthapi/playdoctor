package models.com.hcue.doctors.slick.transactTable

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.jsonTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import play.api.libs.json.JsValue

case class THcueErrorLog(
  ErrorID:           Long,
  ErrorCode:         Long,
  Environment:       String,
  project:           String,
  RemoteAddress:     String,
  RequestURL:        String,
  RequestMethod:     String,
  RequestParameter:  Option[JsValue],
  ResponseParameter: Option[JsValue],
  ResponseString:    Option[String],
  Created:           java.sql.Date,
  CrtUSR:            Long,
  CrtUSRType:        String)

class HcueErrorLog(tag: Tag) extends Table[THcueErrorLog](tag, "tHcueErrorLog") {
  def ErrorID = column[Long]("ErrorID", O.PrimaryKey, O.AutoInc)
  def ErrorCode = column[Long]("ErrorCode", O NotNull)
  def Environment = column[String]("Environment", O NotNull)
  def project = column[String]("Project", O NotNull)
  def RemoteAddress = column[String]("RemoteAddress", O NotNull)
  def RequestURL = column[String]("RequestURL", O NotNull)
  def RequestMethod = column[String]("RequestMethod", O NotNull)
  def RequestParameter = column[Option[JsValue]]("RequestParameter", O NotNull)
  def ResponseParameter = column[Option[JsValue]]("ResponseParameter", O NotNull)
  def ResponseString = column[Option[String]]("ResponseString", O Nullable)
  def Created = column[java.sql.Date]("Created", O Nullable, O.AutoInc)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)

  def * = (ErrorID, ErrorCode, Environment, project, RemoteAddress, RequestURL, RequestMethod, RequestParameter,
    ResponseParameter, ResponseString, Created, CrtUSR, CrtUSRType).<>((THcueErrorLog.apply _).tupled, THcueErrorLog.unapply)

}

object HcueErrorLog {
  def HcueErrorLog =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HcueErrorLog]
}
