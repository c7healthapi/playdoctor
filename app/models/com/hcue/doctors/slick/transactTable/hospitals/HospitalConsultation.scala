package models.com.hcue.doctors.slick.transactTable.hospitals

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType
import scala.slick.lifted.ProvenShape.proveShapeOf

case class THospitalConsultation(
  AddressConsultID: Long,
  HospitalID:       Long,
  DayCD:            String,
  StartTime:        String,
  EndTime:          String,
  MinPerCase:       Option[Int],
  Fees:             Option[Int],
  Active:           Option[String],
  CrtUSR:           Long,
  CrtUSRType:       String,
  UpdtUSR:          Option[Long],
  UpdtUSRType:      Option[String])

class HospitalConsultation(tag: Tag) extends Table[THospitalConsultation](tag, "tDoctorConsultation") {

  def AddressConsultID = column[Long]("AddressConsultID", O.PrimaryKey, O.AutoInc)
  def HospitalID = column[Long]("HospitalID", O NotNull)
  def DayCD = column[String]("DayCD", O NotNull)
  def StartTime = column[String]("StartTime", O NotNull)
  def EndTime = column[String]("EndTime", O NotNull)
  def MinPerCase = column[Option[Int]]("MinPerCase", O Nullable)
  def Fees = column[Option[Int]]("Fees", O Nullable)
  def Active = column[Option[String]]("Active", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O.Nullable)

  def * = (AddressConsultID, HospitalID, DayCD, StartTime, EndTime, MinPerCase, Fees, Active, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THospitalConsultation.apply _).tupled, THospitalConsultation.unapply)

}

object HospitalConsultation {
  def HospitalConsultation =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalConsultation]
}
