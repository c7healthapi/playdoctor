package models.com.hcue.doctors.slick.transactTable.hospitals

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.jsonTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import play.api.libs.json.JsValue

case class THospitalExtn(
  HospitalID:        Long,
  ParentHospitalID:  Option[Long],
  HospitalName:      String,
  HospitalCode:      Option[String],
  BranchCode:        Option[String],
  AppListEmail:      Option[String],
  CrtUSR:            Long,
  CrtUSRType:        String,
  UpdtUSR:           Option[Long],
  UpdtUSRType:       Option[String],
  Hospital_SEOName:  Option[String],
  HospitalImages:    Option[Map[String, String]],
  HospitalDocuments: Option[JsValue])

class HospitalExtn(tag: Tag) extends Table[THospitalExtn](tag, "tHospital") {

  def HospitalID = column[Long]("HospitalID", O.PrimaryKey, O.AutoInc)
  def ParentHospitalID = column[Option[Long]]("ParentHospitalID", O Nullable)
  def HospitalName = column[String]("HospitalName", O NotNull)
  def HospitalCode = column[Option[String]]("HospitalCode", O Nullable)
  def BranchCode = column[Option[String]]("BranchCode", O Nullable)
  def AppListEmail = column[Option[String]]("AppListEmail", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def Hospital_SEOName = column[Option[String]]("Hospital_SEOName", O Nullable)
  def HospitalImages = column[Option[Map[String, String]]]("HospitalImages", O Nullable)
  def HospitalDocuments = column[Option[JsValue]]("HospitalDocuments", O Nullable)

  def * = (HospitalID, ParentHospitalID, HospitalName, HospitalCode, BranchCode, AppListEmail, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType, Hospital_SEOName,
    HospitalImages, HospitalDocuments).<>((THospitalExtn.apply _).tupled, THospitalExtn.unapply)

}

object HospitalExtn {
  def HospitalExtn =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalExtn]
}