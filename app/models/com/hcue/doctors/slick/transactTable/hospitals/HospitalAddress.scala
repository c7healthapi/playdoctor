package models.com.hcue.doctors.slick.transactTable.hospitals

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType
import scala.slick.lifted.ProvenShape.proveShapeOf

case class THospitalAddress(
  HospitalID:     Long,
  HospitalName:   Option[String],
  Address1:       Option[String],
  Address2:       Option[String],
  Street:         Option[String],
  Location:       Option[String],
  CityTown:       Option[String],
  DistrictRegion: Option[String],
  State:          Option[String],
  PostCode:       Option[String],
  Country:        Option[String],
  LandMark:       Option[String],
  Latitude:       Option[String],
  Longitude:      Option[String],
  CrtUSR:         Long,
  CrtUSRType:     String,
  UpdtUSR:        Option[Long],
  UpdtUSRType:    Option[String])

class HospitalAddress(tag: Tag) extends Table[THospitalAddress](tag, "tHospitalAddress") {
  def HospitalID = column[Long]("HospitalID", O.PrimaryKey)
  def HospitalName = column[Option[String]]("HospitalName", O Nullable)
  def Address1 = column[Option[String]]("Address1", O Nullable)
  def Address2 = column[Option[String]]("Address2", O Nullable)
  def Street = column[Option[String]]("Street", O Nullable)
  def Location = column[Option[String]]("Location", O Nullable)
  def CityTown = column[Option[String]]("CityTown", O Nullable)
  def DistrictRegion = column[Option[String]]("DistrictRegion", O Nullable)
  def State = column[Option[String]]("State", O Nullable)
  def PostCode = column[Option[String]]("PostCode", O Nullable)
  def Country = column[Option[String]]("Country", O Nullable)
  def LandMark = column[Option[String]]("LandMark", O Nullable)
  def Latitude = column[Option[String]]("Latitude", O Nullable)
  def Longitude = column[Option[String]]("Longitude", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (HospitalID, HospitalName, Address1, Address2, Street, Location, CityTown, DistrictRegion, State, PostCode, Country, LandMark,
    Latitude, Longitude, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THospitalAddress.apply _).tupled, THospitalAddress.unapply)

}

object HospitalAddress {
  def HospitalAddress =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalAddress]
}
