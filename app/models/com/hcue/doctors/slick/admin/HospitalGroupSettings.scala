package models.com.hcue.doctors.slick.admin

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.hstoreTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class THospitalGroupSettings(
  GroupCode:    String,
  HospitalID:   Map[String, String],
  AcntAccessID: Map[String, String],
  CrtUSR:       Long,
  CrtUSRType:   String,
  UpdtUSR:      Option[Long],
  UpdtUSRType:  Option[String])

class HospitalGroupSettings(tag: Tag) extends Table[THospitalGroupSettings](tag, "tHcueRoleLkup") {

  def GroupCode = column[String]("GrpCD", O.NotNull)
  def HospitalID = column[Map[String, String]]("AccessDesc", O.NotNull)
  def AcntAccessID = column[Map[String, String]]("AcntAccessID", O.NotNull)
  def CrtUSR = column[Long]("CrtUSR", O.NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O.Nullable)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O.Nullable)

  def * = (GroupCode, HospitalID, AcntAccessID, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((THospitalGroupSettings.apply _).tupled, THospitalGroupSettings.unapply)

}

object HospitalGroupSettings {
  def HospitalGroupSettings =
    scala.slick.driver.PostgresDriver.simple.TableQuery[HospitalGroupSettings]
}
