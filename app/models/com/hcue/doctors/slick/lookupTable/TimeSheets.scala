package models.com.hcue.doctors.slick.lookupTable

//import java.util.Date
import java.sql.Date

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.dateColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TTimeSheets(
  TimeSheetID: Long,
  TimeSlotID:  Long,
  DoctorID:    Option[Long],
  HospitalID:  Option[Long],
  HospitalCD:  Option[String],
  UserID:      Long,
  SlotDate:    Date,
  ActiveIND:   String,
  Remarks:     Option[String],
  SlotType:    String,
  DutyType:    Option[String],
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class TimeSheets(tag: Tag) extends Table[TTimeSheets](tag, "tTimeSheets") {

  def TimeSheetID = column[Long]("TimeSheetID", O.AutoInc, O NotNull)
  def TimeSlotID = column[Long]("TimeSlotID", O NotNull)
  def DoctorID = column[Option[Long]]("DoctorID", O.Nullable)
  def HospitalID = column[Option[Long]]("HospitalID", O.Nullable)
  def HospitalCD = column[Option[String]]("HospitalCD", O.Nullable)
  def UserID = column[Long]("UserID", O NotNull)
  def SlotDate = column[Date]("SlotDate", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def Remarks = column[Option[String]]("Remarks", O.Nullable)
  def SlotType = column[String]("SlotType", O.Nullable)
  def DutyType = column[Option[String]]("DutyType", O.Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (TimeSheetID, TimeSlotID, DoctorID, HospitalID, HospitalCD, UserID, SlotDate, ActiveIND, Remarks, SlotType, DutyType,
    CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TTimeSheets.apply _).tupled, TTimeSheets.unapply)

}

object TimeSheets {
  def TimeSheets =
    scala.slick.driver.PostgresDriver.simple.TableQuery[TimeSheets]
}