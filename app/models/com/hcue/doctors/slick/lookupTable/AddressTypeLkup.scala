package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TAddressTypeLkup(
  AddressTypeID:   String,
  AddressTypeDesc: String,
  ActiveIND:       String)

class AddressType(tag: Tag) extends Table[TAddressTypeLkup](tag, "tAddressTypeLkup") {

  def AddressTypeID = column[String]("AddressTypeID", O NotNull)
  def AddressTypeDesc = column[String]("AddressTypeDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (AddressTypeID, AddressTypeDesc, ActiveIND).<>((TAddressTypeLkup.apply _).tupled, TAddressTypeLkup.unapply)

}

object AddressTypes {
  def AddressTypes =
    scala.slick.driver.PostgresDriver.simple.TableQuery[AddressType]
}
