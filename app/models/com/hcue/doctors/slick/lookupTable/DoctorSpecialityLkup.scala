package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorSpecialityLkup(
  DoctorSpecialityID:   String,
  DoctorSpecialityDesc: String,
  SpecialityDesc:       Option[String],
  ActiveIND:            String)

class DoctorSpecialityLkup(tag: Tag) extends Table[TDoctorSpecialityLkup](tag, "tDoctorSpecialityLkup") {

  def DoctorSpecialityID = column[String]("DoctorSpecialityID", O NotNull)
  def DoctorSpecialityDesc = column[String]("DoctorSpecialityDesc", O NotNull)
  def SpecialityDesc = column[Option[String]]("SpecialityDesc", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (DoctorSpecialityID, DoctorSpecialityDesc, SpecialityDesc, ActiveIND).<>((TDoctorSpecialityLkup.apply _).tupled, TDoctorSpecialityLkup.unapply)

}

object DoctorSpecialityLkup {
  def DoctorSpecialityLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorSpecialityLkup]
}
