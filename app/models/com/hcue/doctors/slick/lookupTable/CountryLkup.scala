package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TCountryLkup(
  CountryID:   String,
  CountryDesc: String,
  ActiveIND:   String,
  CountryCode: Option[String],
  isPrivate:   Option[String],
  CrtUSR:      Long,
  CrtUSRType:  String)

class Country(tag: Tag) extends Table[TCountryLkup](tag, "tCountryLkup") {

  def CountryID = column[String]("CountryID", O NotNull)
  def CountryDesc = column[String]("CountryDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CountryCode = column[Option[String]]("CountryCode", O Nullable)
  def isPrivate = column[Option[String]]("isPrivate", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)

  def * = (CountryID, CountryDesc, ActiveIND, CountryCode, isPrivate, CrtUSR, CrtUSRType).<>((TCountryLkup.apply _).tupled, TCountryLkup.unapply)

}

object Country {
  def Country =
    scala.slick.driver.PostgresDriver.simple.TableQuery[Country]
}
