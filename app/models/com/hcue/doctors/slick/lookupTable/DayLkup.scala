package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDayLkup(
  DayID:     String,
  DayDesc:   String,
  ActiveIND: String)

class DayLkup(tag: Tag) extends Table[TDayLkup](tag, "tDayLkup") {

  def DayID = column[String]("DayID", O NotNull)
  def DayDesc = column[String]("DayDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (DayID, DayDesc, ActiveIND).<>((TDayLkup.apply _).tupled, TDayLkup.unapply)

}

object DayLkup {
  def DayLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DayLkup]
}
