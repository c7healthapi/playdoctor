package models.com.hcue.doctors.slick.lookupTable.hospital

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorReferralSettings(
  DoctorID:      Long,
  HospitalCD:    Option[String],
  ReferralID:    Long,
  ReferralTitle: String,
  ReferralDesc:  String,
  ActiveIND:     String,
  CrtUSR:        Long,
  CrtUSRType:    String,
  UpdtUSR:       Option[Long],
  UpdtUSRType:   Option[String])

class DoctorReferralSettings(tag: Tag) extends Table[TDoctorReferralSettings](tag, "tDoctorReferralSettings") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def HospitalCD = column[Option[String]]("HospitalCD", O Nullable)
  def ReferralID = column[Long]("ReferralID", O NotNull, O.AutoInc)
  def ReferralTitle = column[String]("ReferralTitle", O NotNull)
  def ReferralDesc = column[String]("ReferralDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (DoctorID, HospitalCD, ReferralID, ReferralTitle, ReferralDesc, ActiveIND,
    CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorReferralSettings.apply _).tupled, TDoctorReferralSettings.unapply)

}

object DoctorReferralSettings {

  def DoctorReferralSetting =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorReferralSettings]

}
