package models.com.hcue.doctors.slick.lookupTable.hospital

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorVisitRsnSettings(
  DoctorID:      Long,
  HospitalCD:    Option[String],
  VisitRsnID:    Long,
  VisitRsnTitle: String,
  VisitRsnDesc:  Option[String],
  ActiveIND:     String,
  CrtUSR:        Long,
  CrtUSRType:    String,
  UpdtUSR:       Option[Long],
  UpdtUSRType:   Option[String])

class DoctorVisitRsnSettings(tag: Tag) extends Table[TDoctorVisitRsnSettings](tag, "tDoctorVisitRsnSettings") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def HospitalCD = column[Option[String]]("HospitalCD", O Nullable)
  def VisitRsnID = column[Long]("VisitRsnID", O NotNull, O.AutoInc)
  def VisitRsnTitle = column[String]("VisitRsnTitle", O NotNull)
  def VisitRsnDesc = column[Option[String]]("VisitRsnDesc", O Nullable)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (DoctorID, HospitalCD, VisitRsnID, VisitRsnTitle, VisitRsnDesc, ActiveIND,
    CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorVisitRsnSettings.apply _).tupled, TDoctorVisitRsnSettings.unapply)

}

object DoctorVisitRsnSettings {

  def DoctorVisitRsnSetting =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorVisitRsnSettings]

}
