package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TVisitorTypeLkup(
  VisitUserTypeID:   String,
  VisitUserTypeDesc: String,
  ActiveIND:         String)

class VisitorTypeLkup(tag: Tag) extends Table[TVisitorTypeLkup](tag, "tVisitorTypeLkup") {

  def VisitUserTypeID = column[String]("VisitUserTypeID", O NotNull)
  def VisitUserTypeDesc = column[String]("VisitUserTypeDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (VisitUserTypeID, VisitUserTypeDesc, ActiveIND).<>((TVisitorTypeLkup.apply _).tupled, TVisitorTypeLkup.unapply)

}

object VisitorTypeLkup {
  def VisitorTypeLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[VisitorTypeLkup]
}
