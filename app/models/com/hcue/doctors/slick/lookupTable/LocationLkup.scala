package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TLocationLkup(
  LocationID:     String,
  LocationIDDesc: String,
  CityID:         String,
  ActiveIND:      String,
  isPrivate:      Option[String],
  CrtUSR:         Long,
  CrtUSRType:     String)

class LocationLkup(tag: Tag) extends Table[TLocationLkup](tag, "tLocationLkup") {

  def LocationID = column[String]("LocationID", O NotNull)
  def LocationIDDesc = column[String]("LocationIDDesc", O NotNull)
  def CityID = column[String]("CityID", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def isPrivate = column[Option[String]]("isPrivate", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)

  def * = (LocationID, LocationIDDesc, CityID, ActiveIND, isPrivate, CrtUSR, CrtUSRType).<>((TLocationLkup.apply _).tupled, TLocationLkup.unapply)

}

object LocationLkup {
  def LocationLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[LocationLkup]
}
