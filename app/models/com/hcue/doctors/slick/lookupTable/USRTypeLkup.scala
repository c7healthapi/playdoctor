package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TUSRTypeLkup(
  USRTypeID:   String,
  USRTypeDesc: String,
  ActiveIND:   String)

class USRTypeLkup(tag: Tag) extends Table[TUSRTypeLkup](tag, "tUSRTypeLkup") {

  def USRTypeID = column[String]("USRTypeID", O NotNull)
  def USRTypeDesc = column[String]("USRTypeDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (USRTypeID, USRTypeDesc, ActiveIND).<>((TUSRTypeLkup.apply _).tupled, TUSRTypeLkup.unapply)

}

object USRTypeLkup {
  def USRTypeLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[USRTypeLkup]
}
