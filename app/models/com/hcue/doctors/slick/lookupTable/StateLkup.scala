package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TStateLkup(
  StateID:    String,
  StateDesc:  String,
  ActiveIND:  String,
  CountryID:  String,
  isPrivate:  Option[String],
  CrtUSR:     Long,
  CrtUSRType: String)

class StateLkup(tag: Tag) extends Table[TStateLkup](tag, "tStateLkup") {

  def StateID = column[String]("StateID", O NotNull)
  def StateDesc = column[String]("StateDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CountryID = column[String]("CountryID", O NotNull)
  def isPrivate = column[Option[String]]("isPrivate", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)

  def * = (StateID, StateDesc, ActiveIND, CountryID, isPrivate, CrtUSR, CrtUSRType).<>((TStateLkup.apply _).tupled, TStateLkup.unapply)

}

object StateLkup {
  def StateLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[StateLkup]
}
