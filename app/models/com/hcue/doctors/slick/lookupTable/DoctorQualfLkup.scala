package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorQualfLkup(
  DoctorQualfID:   String,
  DoctorQualfDesc: String,
  ActiveIND:       String)

class DoctorQualfLkup(tag: Tag) extends Table[TDoctorQualfLkup](tag, "tDoctorQualfLkup") {

  def DoctorQualfID = column[String]("DoctorQualfID", O NotNull)
  def DoctorQualfDesc = column[String]("DoctorQualfDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (DoctorQualfID, DoctorQualfDesc, ActiveIND).<>((TDoctorQualfLkup.apply _).tupled, TDoctorQualfLkup.unapply)

}

object DoctorQualfLkup {
  def DoctorQualfLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorQualfLkup]
}
