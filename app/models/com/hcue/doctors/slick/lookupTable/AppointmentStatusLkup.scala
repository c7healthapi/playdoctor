package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TAppointmentStatusLkup(
  AppointmentStatusID:   String,
  AppointmentStatusDesc: String,
  ActiveIND:             String)

class AppointmentStatus(tag: Tag) extends Table[TAppointmentStatusLkup](tag, "tAppointmentStatusLkup") {

  def AppointmentStatusID = column[String]("AppointmentStatusID", O NotNull)
  def AppointmentStatusDesc = column[String]("AppointmentStatusDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (AppointmentStatusID, AppointmentStatusDesc, ActiveIND).<>((TAppointmentStatusLkup.apply _).tupled, TAppointmentStatusLkup.unapply)

}

object AppointmentStatus {
  def AppointmentStatus =
    scala.slick.driver.PostgresDriver.simple.TableQuery[AppointmentStatus]
}
