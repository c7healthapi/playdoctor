package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.dateColumnType
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TDoctorAccountDetails(
  DoctorID:     Long,
  UserTypeFlag: String,
  Comments:     Option[String],
  ExpiryDate:   Option[java.sql.Date],
  CrtUSR:       Long,
  CrtUSRType:   String,
  UpdtUSR:      Option[Long],
  UpdtUSRType:  Option[String],
  HospitalCode: Option[String],
  ActiveIND:    Option[String])

class DoctorAccountDetails(tag: Tag) extends Table[TDoctorAccountDetails](tag, "tDoctorAccountDetails") {

  def DoctorID = column[Long]("DoctorID", O NotNull)
  def UserTypeFlag = column[String]("UserTypeFlag", O NotNull)
  def Comments = column[Option[String]]("Comments", O Nullable)
  def ExpiryDate = column[Option[java.sql.Date]]("ExpiryDate", O Nullable)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.AutoInc, O Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)
  def HospitalCode = column[Option[String]]("HospitalCode", O Nullable)
  def ActiveIND = column[Option[String]]("ActiveIND", O Nullable)

  def * = (DoctorID, UserTypeFlag, Comments, ExpiryDate, CrtUSR, CrtUSRType, UpdtUSR,
    UpdtUSRType, HospitalCode, ActiveIND).<>((TDoctorAccountDetails.apply _).tupled, TDoctorAccountDetails.unapply)

}

object DoctorAccountDetails {
  def DoctorAccountDetails =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorAccountDetails]
}
