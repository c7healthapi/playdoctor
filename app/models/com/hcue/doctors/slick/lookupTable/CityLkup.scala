package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TCityLkup(
  CityID:     String,
  CityIDDesc: String,
  ActiveIND:  String,
  StateID:    String,
  isPrivate:  Option[String],
  CrtUSR:     Long,
  CrtUSRType: String)

class CityLkup(tag: Tag) extends Table[TCityLkup](tag, "tCityLkup") {

  def CityID = column[String]("CityID", O NotNull)
  def CityIDDesc = column[String]("CityIDDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def StateID = column[String]("StateID", O NotNull)
  def isPrivate = column[Option[String]]("isPrivate", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)

  def * = (CityID, CityIDDesc, ActiveIND, StateID, isPrivate, CrtUSR, CrtUSRType).<>((TCityLkup.apply _).tupled, TCityLkup.unapply)

}

object CityLkup {
  def CityLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[CityLkup]
}
