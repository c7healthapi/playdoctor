package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TOtherIDTypeLkup(
  OtherIDTypeID:   String,
  OtherIDTypeDesc: String,
  ActiveIND:       String)

class OtherIDTypeLkup(tag: Tag) extends Table[TOtherIDTypeLkup](tag, "tOtherIDTypeLkup") {

  def OtherIDTypeID = column[String]("OtherIDTypeID", O NotNull)
  def OtherIDTypeDesc = column[String]("OtherIDTypeDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (OtherIDTypeID, OtherIDTypeDesc, ActiveIND).<>((TOtherIDTypeLkup.apply _).tupled, TOtherIDTypeLkup.unapply)

}

object OtherIDTypeLkup {
  def OtherIDTypeLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[OtherIDTypeLkup]
}
