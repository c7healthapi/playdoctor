package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TContactTypeLkup(
  ContactTypeID:   String,
  ContactTypeDesc: String,
  ActiveIND:       String)

class ContactTypeLkup(tag: Tag) extends Table[TContactTypeLkup](tag, "tContactTypeLkup") {

  def ContactTypeID = column[String]("ContactTypeID", O NotNull)
  def ContactTypeDesc = column[String]("ContactTypeDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (ContactTypeID, ContactTypeDesc, ActiveIND).<>((TContactTypeLkup.apply _).tupled, TContactTypeLkup.unapply)

}

object ContactTypeLkup {
  def ContactTypeLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[ContactTypeLkup]
}
