package models.com.hcue.doctors.slick.lookupTable

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.jsonTypeMapper
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType
import play.api.libs.json.JsValue

case class TDoctorJsonSetting(
  JsonSettingID:    Long,
  DoctorID:         Long,
  CustomID:         Option[Long],
  HospitalID:       Option[Long],
  HospitalCD:       Option[String],
  MappingAddressID: Option[Long],
  JsonSetting:      Option[JsValue],
  IsDefault:        String,
  TemplateName:     String,
  TemplateNotes:    Option[String],
  PageType:         String,
  SubPageType:      Option[String],
  ActiveIND:        String,
  CrtUSR:           Long,
  CrtUSRType:       String,
  UpdtUSR:          Option[Long],
  UpdtUSRType:      Option[String])

class DoctorJsonSetting(tag: Tag) extends Table[TDoctorJsonSetting](tag, "tDoctorJsonSetting") {

  def JsonSettingID = column[Long]("JsonSettingID", O.AutoInc, O NotNull)
  def DoctorID = column[Long]("DoctorID", O NotNull)
  def CustomID = column[Option[Long]]("CustomID", O.Nullable)
  def HospitalID = column[Option[Long]]("HospitalID", O.Nullable)
  def HospitalCD = column[Option[String]]("HospitalCD", O.Nullable)
  def MappingAddressID = column[Option[Long]]("MappingAddressID", O.Nullable)
  def JsonSetting = column[Option[JsValue]]("JsonSetting", O Nullable)
  def IsDefault = column[String]("IsDefault", O NotNull)
  def TemplateName = column[String]("TemplateName", O NotNull)
  def TemplateNotes = column[Option[String]]("TemplateNotes", O.Nullable)
  def PageType = column[String]("PageType", O NotNull)
  def SubPageType = column[Option[String]]("SubPageType", O.Nullable)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (JsonSettingID, DoctorID, CustomID, HospitalID, HospitalCD, MappingAddressID, JsonSetting, IsDefault, TemplateName, TemplateNotes, PageType, SubPageType, ActiveIND,
    CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TDoctorJsonSetting.apply _).tupled, TDoctorJsonSetting.unapply)

}

object DoctorJsonSetting {
  def DoctorJsonSetting =
    scala.slick.driver.PostgresDriver.simple.TableQuery[DoctorJsonSetting]
}