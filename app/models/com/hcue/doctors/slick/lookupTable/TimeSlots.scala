package models.com.hcue.doctors.slick.lookupTable

import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Table
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.Tag
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.anyToToShapedValue
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.longColumnType
import models.com.hcue.doctors.slick.MyPostgresDriver.simple.stringColumnType

case class TTimeSlots(
  TimeSlotID:  Long,
  DoctorID:    Option[Long],
  HospitalID:  Option[Long],
  HospitalCD:  Option[String],
  TimeFrom:    Option[String],
  TimeTo:      Option[String],
  Remarks:     Option[String],
  ActiveIND:   String,
  CrtUSR:      Long,
  CrtUSRType:  String,
  UpdtUSR:     Option[Long],
  UpdtUSRType: Option[String])

class TimeSlots(tag: Tag) extends Table[TTimeSlots](tag, "tTimeSlots") {

  def TimeSlotID = column[Long]("TimeSlotID", O.AutoInc, O NotNull)
  def DoctorID = column[Option[Long]]("DoctorID", O.Nullable)
  def HospitalID = column[Option[Long]]("HospitalID", O.Nullable)
  def HospitalCD = column[Option[String]]("HospitalCD", O.Nullable)
  def TimeFrom = column[Option[String]]("TimeFrom", O.Nullable)
  def TimeTo = column[Option[String]]("TimeTo", O.Nullable)
  def Remarks = column[Option[String]]("Remarks", O.Nullable)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CrtUSR = column[Long]("CrtUSR", O NotNull)
  def CrtUSRType = column[String]("CrtUSRType", O NotNull)
  def UpdtUSR = column[Option[Long]]("UpdtUSR", O.Nullable)
  def UpdtUSRType = column[Option[String]]("UpdtUSRType", O Nullable)

  def * = (TimeSlotID, DoctorID, HospitalID, HospitalCD, TimeFrom, TimeTo, Remarks, ActiveIND,
    CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType).<>((TTimeSlots.apply _).tupled, TTimeSlots.unapply)

}

object TimeSlots {
  def TimeSlots =
    scala.slick.driver.PostgresDriver.simple.TableQuery[TimeSlots]
}
