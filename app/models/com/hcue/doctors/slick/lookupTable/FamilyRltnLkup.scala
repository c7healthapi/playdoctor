package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TFamilyRltnLkup(
  FamilyRltnID:   String,
  FamilyRltnDesc: String,
  ActiveIND:      String)

class FamilyRltnLkup(tag: Tag) extends Table[TFamilyRltnLkup](tag, "tFamilyRltnLkup") {

  def FamilyRltnID = column[String]("FamilyRltnID", O NotNull)
  def FamilyRltnDesc = column[String]("FamilyRltnDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (FamilyRltnID, FamilyRltnDesc, ActiveIND).<>((TFamilyRltnLkup.apply _).tupled, TFamilyRltnLkup.unapply)

}

object FamilyRltnLkup {
  def FamilyRltnLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[FamilyRltnLkup]
}
