package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.intColumnType
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TCurrencyLkup(
  CurrencyCode: String,
  CurrencyDesc: String,
  Order:        Int,
  ActiveIND:    String,
  CountryID:    String)

class CurrencyLkup(tag: Tag) extends Table[TCurrencyLkup](tag, "tCurrencyLkup") {

  def CurrencyCode = column[String]("CurrencyCode", O NotNull)
  def CurrencyDesc = column[String]("CurrencyDesc", O NotNull)
  def Order = column[Int]("Order", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)
  def CountryID = column[String]("CountryID", O NotNull)

  def * = (CurrencyCode, CurrencyDesc, Order, ActiveIND, CountryID).<>((TCurrencyLkup.apply _).tupled, TCurrencyLkup.unapply)

}

object CurrencyLkup {
  def CurrencyLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[CurrencyLkup]
}
