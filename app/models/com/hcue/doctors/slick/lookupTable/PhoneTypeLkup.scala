package models.com.hcue.doctors.slick.lookupTable

import scala.slick.driver.PostgresDriver.simple.Table
import scala.slick.driver.PostgresDriver.simple.Tag
import scala.slick.driver.PostgresDriver.simple.anyToToShapedValue
import scala.slick.driver.PostgresDriver.simple.stringColumnType

case class TPhoneTypeLkup(
  PhoneTypeID:   String,
  PhoneTypeDesc: String,
  ActiveIND:     String)

class PhoneTypeLkup(tag: Tag) extends Table[TPhoneTypeLkup](tag, "tPhoneTypeLkup") {

  def PhoneTypeID = column[String]("PhoneTypeID", O NotNull)
  def PhoneTypeDesc = column[String]("PhoneTypeDesc", O NotNull)
  def ActiveIND = column[String]("ActiveIND", O NotNull)

  def * = (PhoneTypeID, PhoneTypeDesc, ActiveIND).<>((TPhoneTypeLkup.apply _).tupled, TPhoneTypeLkup.unapply)

}

object PhoneTypeLkup {
  def PhoneTypeLkup =
    scala.slick.driver.PostgresDriver.simple.TableQuery[PhoneTypeLkup]
}
