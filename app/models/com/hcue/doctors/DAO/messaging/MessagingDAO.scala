package models.com.hcue.doctors.DAO.messaging

import java.io.File
import java.time.Instant
import java.util.Date
import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.booleanColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.booleanOptionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.optionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.productQueryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.queryToInsertInvoker
import scala.slick.driver.PostgresDriver.simple.queryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.stringColumnType
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker
import org.apache.velocity.VelocityContext
import com.hcue.hl7.message.CommonOrder
import com.hcue.hl7.message.HL7Message
import com.hcue.hl7.message.HL7Wrapper
import com.hcue.hl7.message.ObservationRequest
import com.hcue.hl7.message.PatientVisit
import com.hcue.hl7.xml.parser.Header
import com.hcue.hl7.xml.parser.Message
import com.hcue.hl7.xml.parser.Patient
import com.hcue.hl7.xml.parser.PatientResult
import com.hcue.pdfgen.entities.ClinicDetails
import com.hcue.pdfgen.entities.Content
import com.hcue.pdfgen.entities.PDFWrapper
import com.hcue.pdfgen.entities.PageHeader
import com.hcue.pdfgen.entities.PatientDetails
import com.hcue.pdfgen.entities.ReportDetails
import models.com.hcue.doctor.slick.transactTable.Careteam.SpecialityLkup
import models.com.hcue.doctor.traits.add.Careteam.PracticeDao
import models.com.hcue.doctor.traits.add.Careteam.SpecialityLkupDao
import models.com.hcue.doctors.DAO.Doctor.DoctorDao
import models.com.hcue.doctors.DAO.Hospital.HospitalDao
import models.com.hcue.doctors.DAO.Patient.PatientDao
import models.com.hcue.doctors.DAO.Patient.PatientEnquiryDao
import models.com.hcue.doctors.DAO.Patient.ReferrerEnquiryDao
import models.com.hcue.doctors.Json.Request.messaging.listObjHL7Message
import models.com.hcue.doctors.Json.Request.messaging.messagelogobject
import models.com.hcue.doctors.helper.hcueCommunication
import models.com.hcue.doctors.helper.hcueDocuments
import models.com.hcue.doctors.slick.transactTable.doctor.HospitalAppointment
import models.com.hcue.doctors.slick.transactTable.doctor.ReferrerEnquiry
import models.com.hcue.doctors.slick.transactTable.doctor.THospitalAppointment
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.messaging.DiagnosticsRequest
import models.com.hcue.doctors.slick.transactTable.messaging.DiagnosticsResponse
import models.com.hcue.doctors.slick.transactTable.messaging.DiagnosticsResponseExtn
import models.com.hcue.doctors.slick.transactTable.messaging.IntegrationLog
import models.com.hcue.doctors.slick.transactTable.messaging.MessagingLog
import models.com.hcue.doctors.slick.transactTable.messaging.TDiagnosticsRequest
import models.com.hcue.doctors.slick.transactTable.messaging.TDiagnosticsResponse
import models.com.hcue.doctors.slick.transactTable.messaging.TDiagnosticsResponseExtn
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.traits.add.hospitalappointment.AddHospitalAppointmentDao
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.traits.list.patient.ListPatientDao
import play.Logger
import play.api.Play.current
import models.com.hcue.doctors.Json.Request.messaging.getHL7Obj
import java.text.SimpleDateFormat
import java.time.LocalDate
import models.com.hcue.doctors.Json.Request.messaging.listHL7Response
import models.com.hcue.doctors.Json.Request.messaging.AppointmentSMSMessageObj
import models.com.hcue.doctors.slick.transactTable.messaging.AppointmentMsgLog

object messagingDAO {

  val log = Logger.of("application")

  val diagnosticsRequest = DiagnosticsRequest.DiagnosticsRequest

  val diagnosticsResponse = DiagnosticsResponse.DiagnosticsResponse

  val diagnosticsResponseExtn = DiagnosticsResponseExtn.DiagnosticsResponseExtn

  val specialityLkup = SpecialityLkup.SpecialityLkup

  val hospitalAppointment = HospitalAppointment.HospitalAppointment

  val integrationLog = IntegrationLog.IntegrationLog

  val referrerEnquiry = ReferrerEnquiry.ReferrerEnquiry

  val hospital = Hospital.Hospital

  val patients = Patients.patients

  /*
  def addObservation(a: observationrequestObj): Long = db withSession { implicit session =>

    val observationid = (observationrequest.map { x => (x.patientid, x.obrsetid)} 
            returning observationrequest.map(_.id)) += (a.patientid,a.obrsetid)

    val observationid = (observationrequest.map { x =>
      (x.patientid, x.obrsetid, x.obrplacerordernumber, x.obrfillerordernumber, x.obrunivserviceidentifier, x.obrrequesteddatetime,
        x.obrobservationdatetime, x.obrorderingprovider, x.obrresultstatuschgdatetime, x.obrdiagservicesecretid, x.obrresultstatus, x.obrresultinterpreter, x.obrtranscriptionist)
    }
      returning observationrequest.map(_.id)) +=
      (a.patientid, a.obrsetid, a.obrplacerordernumber, a.obrfillerordernumber, a.obrunivserviceidentifier, a.obrrequesteddatetime, a.obrobservationdatetime,
        a.obrorderingprovider, a.obrresultstatuschgdatetime, a.obrdiagservicesecretid, a.obrresultstatus, a.obrresultinterpreter, a.obrtranscriptionist)

    if (a.observationresult != None) {
      a.observationresult.foreach { x =>
        val observationresultDetails: Array[observationresultObj] = x
        for (i <- 0 until observationresultDetails.length) {

          observationresult.map { x =>
            (x.obrrequestid, x.patientid, x.obxsetid, x.obxvaluetype, x.obxidentifier, x.obxsubid, x.obxvalue, x.obxresultstatus, x.obxeffectivedaterefrange,
              x.obxuserdefaccesscheck, x.obxobservationdatetime, x.obxproducerid, x.obxresobserver)
          } +=
            (observationid, observationresultDetails(i).PatientID, observationresultDetails(i).OBXSetID, observationresultDetails(i).OBXValueType, observationresultDetails(i).OBXIdentifier,
              observationresultDetails(i).OBXSubId, observationresultDetails(i).OBXValue, observationresultDetails(i).OBXResultStatus, observationresultDetails(i).OBXEffectiveDateRefRange,
              observationresultDetails(i).OBXUserDefAccessCheck, observationresultDetails(i).OBXObservationDateTime, observationresultDetails(i).OBXProducerId, observationresultDetails(i).OBXResObserver)

        }
      }
    }
    val result = observationid

    return result
  }*/

  def addDiagnosticsResponse(request: Message): Long = db withSession { implicit session =>
    println("************Diagnostic Reponse Addedd - begins****************")
    log.info("************Diagnostic Reponse Addedd - begins****************")

    val header: Header = request.getHeader
    val objPatientResult: PatientResult = request.getPatientResult()
    val objPatient: Patient = objPatientResult.getPatient()

    println("Firstname" + objPatient.getFirstName())

    println("History" + objPatientResult.getOrderObservation.getObservation.getFindings);

    //val msgcontrolId = header.getMessageControlId

    println(header.getMsgCtrlId)
    println(objPatient.getPatientId)
    //println(objPatient.getNhsnumber)

    println(replaceBrackets(objPatientResult.getOrderObservation().getObservation.getFindings.toString()))
    println(replaceBrackets(objPatientResult.getOrderObservation().getObservation.getHistory.toString()))
    println(replaceBrackets(objPatientResult.getOrderObservation().getObservation.getImpressions.toString()))
    println(objPatientResult.getOrderObservation.getObservationRequest.getResultInterpreter)
    println(objPatientResult.getOrderObservation.getObservationRequest.getResultStatus);
    val scanStartTime = objPatientResult.getOrderObservation.getCommonOrder.getStartTime
    val scanEndTime = objPatientResult.getOrderObservation.getCommonOrder.getEndTime

    //objPatientResult.getOrderObservation.getCommonOrder.getStartTime

    val NHSNumber: Long = if (objPatient.getNhsnumber == null) { 0L } else { objPatient.getNhsnumber }

    val referralId: Long = objPatientResult.getOrderObservation.getObservationRequest.getReferralId

    val appointmentIdCount: Long = diagnosticsRequest.filter { x => x.appointmentid === referralId }.map { x => x.appointmentid }.list.length

    val appointmentid = if (appointmentIdCount > 0) {
      referralId
    } else {
      diagnosticsRequest.filter { x => x.ReferralId === referralId && x.ActiveIND === "Y" && x.AppointmentStatus === "B" }.map { x => x.appointmentid }.firstOption.get
    }

    val diagnosticsRequestval = diagnosticsRequest.filter { x => x.ReferralId === referralId && x.ActiveIND === "Y" && x.AppointmentStatus === "B" }.firstOption

    val patientenquiryid = hospitalAppointment.filter { x => x.AppointmentID === appointmentid }.map { x => x.Patientenquiryid.get }.firstOption.get

    //val records  = diagnosticsResponse.filter(x => x.referralid === referralId && x.Active).map { x => x.Active }.update(false)
    val records = diagnosticsResponse.filter(x => x.referralid === referralId && x.Active).map { x => x.Active }.update(false)

    //value: (Long, Long, Long, Long, Long, String, String, String, String, String, String, String, String, Long, String

    val exam = if (request.getIsMRIMessage) {
      objPatientResult.getOrderObservation.getObservationRequest.getServiceIdText
    } else {
      objPatientResult.getOrderObservation.getObservationRequest.getServiceId + "-" + objPatientResult.getOrderObservation.getObservationRequest.getServiceIdText
    }

    val diagReposneId = (diagnosticsResponse.map { x =>
      (
        x.requestid,
        x.patientid,
        x.appointmentid,
        x.nhsnumber,
        x.referralid,
        x.findings,
        x.history,
        x.impressions,
        x.reportor,
        x.reportstatus,
        x.examdate,
        x.reportdate,
        x.exam,
        x.ReportURL,
        x.CrtUSR,
        x.CrtUSRType,
        x.Active,
        x.patientenquiryid,
        x.scanstarttime,
        x.scanendtime)
    }
      returning diagnosticsResponse.map(_.responseid)) += (
        diagnosticsRequestval.get.requestid,
        objPatient.getPatientId,
        appointmentid,
        NHSNumber,
        objPatientResult.getOrderObservation.getObservationRequest.getReferralId,
        replaceBrackets(objPatientResult.getOrderObservation().getObservation.getFindings.toString()),
        replaceBrackets(objPatientResult.getOrderObservation().getObservation.getHistory.toString()),
        replaceBrackets(objPatientResult.getOrderObservation().getObservation.getImpressions.toString()),
        objPatientResult.getOrderObservation.getObservationRequest.getResultInterpreter,
        objPatientResult.getOrderObservation.getObservationRequest.getResultStatus,
        objPatientResult.getOrderObservation.getObservationRequest.getStrobservationDate,
        objPatientResult.getOrderObservation.getObservationRequest.getStrRequestDate,
        exam,
        " ",
        1,
        "WSO2",
        true,
        patientenquiryid,
        Some(scanStartTime),
        Some(scanEndTime))

    if (diagReposneId >0) {
      diagnosticsResponseExtn.filter(x => x.responseid === diagReposneId)
        .map(x => (x.indications, x.comparisions, x.techniques, x.isMRIMessage, x.UpdtUSR, x.UpdtUSRType))
        .update(Some(replaceBrackets(objPatientResult.getOrderObservation().getObservation.getIndications.toString())),
          Some(replaceBrackets(objPatientResult.getOrderObservation().getObservation.getComparisions.toString())),
          Some(replaceBrackets(objPatientResult.getOrderObservation().getObservation.getTechniques.toString())),
          Some(request.getIsMRIMessage), 1, "WSO2")
    }

    println("Diagnostic Reponse Addedd Successfully::" + diagReposneId)
    log.info("Diagnostic Reponse Addedd Successfully::" + diagReposneId)

    println("************Diagnostic Reponse Addedd - ends****************")
    log.info("************Diagnostic Reponse Addedd - ends****************")

    return diagReposneId
  }

  /*
   *     val referralId: Option[Long] = diagReqObj.map { x => x.appointmentid }.list.length match {
      case 0 => diagnosticsRequest.filter { x => x.patientenquiryid === enquiryID.getOrElse(0L) && x.ActiveIND === "Y" && x.AppointmentStatus === "B" }.map { x => x.appointmentid }.list.length match {
        case 0 => None
        case _ => diagnosticsRequest.filter { x => x.patientenquiryid === enquiryID.get && x.ActiveIND === "Y" && x.AppointmentStatus === "B" }.map { x => x.appointmentid }.firstOption
      }
      case _ => enquiryID
    }
   * 
   * 
   * 
   * 
   */

  def updateAppoointmentStatus(responseId: Long): String = db withSession { implicit session =>
    println("responseId::" + responseId)
    val diagnosticsResponse = getDiagnosticsResponseById(responseId)
    val diagnosticRequest = diagnosticsRequest.filter { x => x.ReferralId === diagnosticsResponse.referralid && x.ActiveIND === "Y" && x.AppointmentStatus === "B" }.firstOption

    val appointmentid: Long = diagnosticRequest match {
      case None => {
        val objDiagReq = diagnosticsRequest.filter { x => x.patientenquiryid === diagnosticsResponse.referralid && x.ActiveIND === "Y" && x.AppointmentStatus === "B" }.firstOption
        objDiagReq match {
          case None => 0
          case _    => objDiagReq.map { x => x.appointmentid }.get
        }
      }
      case _ => diagnosticRequest.map { x => x.appointmentid }.get
    }

    val isUpdated = appointmentid match {
      case 0 => 0
      case _ => AddHospitalAppointmentDao.updateAppointmentTimeandStatus(appointmentid, diagnosticsResponse.scanstarttime, diagnosticsResponse.scanendtime)
    }

    println("isUpdated::" + isUpdated)
    if (isUpdated > 0) {
      return "SUCCESS"
    }
    return "FAILURE"
  }

  //requestid, patientid, appointmentid, nhsnumber, scancentreid, scancentreaddressid, CrtUSR, CrtUSRType, UpdtUSR, UpdtUSRType

  def addDiagnosticsRequest(appointmentId: Long): Long = db withSession { implicit session =>
    val appmnt = AddHospitalAppointmentDao.getHospitalAppmntById(appointmentId)

    //val record: Option[TDiagnosticsRequest] = diagnosticsRequest.filter { x => x.appointmentid === appmnt.AppointmentID && x.ReferralId === appmnt.Patientenquiryid && x.ActiveIND === "Y" }.firstOption

    val record: Option[TDiagnosticsRequest] = diagnosticsRequest.filter { x => x.appointmentid === appmnt.AppointmentID && x.ReferralId === appmnt.AppointmentID && x.ActiveIND === "Y" }.firstOption

    println("query")

    if (record != None) {
      //diagnosticsRequest.filter { x => x.appointmentid === appmnt.AppointmentID && x.ReferralId === appmnt.Patientenquiryid }.map { x =>
      diagnosticsRequest.filter { x => x.appointmentid === appmnt.AppointmentID && x.ReferralId === appmnt.AppointmentID }.map { x =>
        (x.ActiveIND)
      }.update("N")
    } else {
      //Inactivate all the requests sent for the patientenquiry id previously. The appointment id may be differnt because
      //the system allows to reschedule the appointment in differnce clinics
      //diagnosticsRequest.filter { x =>  x.ReferralId === appmnt.Patientenquiryid }.map { x =>
      diagnosticsRequest.filter { x =>  x.patientenquiryid === appmnt.Patientenquiryid && x.ActiveIND === "Y"}.map { x =>
        (x.ActiveIND)
      }.update("N")

    }

    /*query match {
      case  x if x > 0 => {
        diagnosticsRequest.filter
      }
    }*/
    //referral

    println("appointmentId" + appointmentId)
    val patient = PatientDao.findByID(appmnt.PatientID.get)
    val nhsNumber = patient.get.AadhaarID
    println("nhsNumber::" + nhsNumber)
    println("appmnt.Patientenquiryid.get::" + appmnt.Patientenquiryid.get)
    val referrer = ReferrerEnquiryDao.findByID(appmnt.Patientenquiryid.get)
    println("doctorid::" + referrer.get.doctorid.get)
    val doctor = DoctorDao.findByID(referrer.get.doctorid.get)
    val hospital = HospitalDao.findByID(appmnt.HospitalID.get)
    val patientEnquiry = PatientEnquiryDao.findByID(appmnt.Patientenquiryid.get)

    //doctor.get.FirstName
    //doctor.get.LastName
    //doctor.get.FullName

    //appmnt -> patient id --> nhs number -> Aadharid
    //appmnt -> patientenquiry -> referrerenquiry --> doctorid --> tdoctor and tdoctoraddress
    //appmnt -> hospitalid -> scancenter, scancenteraddress.

    val clinicalNotes = "Presenting Complaint:" + patientEnquiry.get.clinicalnotes.get + ";" + "Provisional Diagnosis:" + patientEnquiry.get.clinicaldetails.get

    val diagRequestId = (diagnosticsRequest.map { x =>
      (x.patientid, x.appointmentid, x.nhsnumber, x.scancentreid, x.scancentreaddressid,
        x.ScanCenterName, x.SonographerId, x.SonographerName, x.ClinicalInfo, x.ReferralId,
        x.AppointmentStatus, x.CrtUSR, x.CrtUSRType, x.patientenquiryid)
    }
      returning diagnosticsRequest.map(_.requestid)) +=
      (appmnt.PatientID.get, appmnt.AppointmentID, 0, appmnt.HospitalID.get, 0,
        hospital.get.HospitalName.get, referrer.get.doctorid.get, doctor.get.FullName,
        //clinicalNotes, patientEnquiry.get.patientenquiryid,
        clinicalNotes, appmnt.AppointmentID,
        appmnt.AppointmentStatus, appmnt.CrtUSR, appmnt.CrtUSRType, patientEnquiry.get.patientenquiryid)

    return diagRequestId
  }

  /*def getObservationbyId(obrid: Long): List[TObservationResult] = db withSession { implicit session =>
    observationresult.filter { x => x.obrrequestid === obrid }.list
  }
*/
  def getDiagnosticsResponseById(diagResponseId: Long): TDiagnosticsResponse = db withSession { implicit session =>
    diagnosticsResponse.filter { x => x.responseid === diagResponseId }.first
  }

  def getDiagnosticsResponseExtnById(diagResponseId: Long): TDiagnosticsResponseExtn = db withSession { implicit session =>
    diagnosticsResponseExtn.filter { x => x.responseid === diagResponseId }.first
  }

  def getByResponseId(diagResponseId: Long): Option[TDiagnosticsResponse] = db withSession { implicit session =>
    diagnosticsResponse.filter { x => x.responseid === diagResponseId }.firstOption
  }

  def getDiagnosticsRequestById(diagRequestId: Long): TDiagnosticsRequest = db withSession { implicit session =>
    diagnosticsRequest.filter { x => x.requestid === diagRequestId }.first
  }

  def getDiagnosticsByReferralId(referralId: Long): TDiagnosticsRequest = db withSession { implicit session =>
    diagnosticsRequest.filter { x => x.ReferralId === referralId && x.ActiveIND === "Y" && x.AppointmentStatus === "B" }.first
  }

  def replaceBrackets(listStr: String): String = {
    listStr.substring(1, listStr.length() - 1);
  }

  def generateDiagnsoticsReportV1(diagnosticResponseId: Long): String = {

    println("*****************Starting PDF generation - Starts*****************")
    log.info("*****************Starting PDF generation - Starts*****************")

    println("Diagnostic Response ID::" + diagnosticResponseId)
    log.info("Diagnostic Response ID::" + diagnosticResponseId)

    val diagnosticsResponse = getDiagnosticsResponseById(diagnosticResponseId)

    val diagnosticsResponseExtn = getDiagnosticsResponseExtnById(diagnosticResponseId)

    val diagnosticRequest = getDiagnosticsByReferralId(diagnosticsResponse.referralid)

    val PatinetEnquiryID = diagnosticRequest.patientenquiryid

    val referralEnquiry = ReferrerEnquiryDao.findByID(PatinetEnquiryID)

    val practice = PracticeDao.findByID(referralEnquiry.get.practiceid.get)

    val isMRIMessage: Boolean = diagnosticsResponseExtn.isMRIMessage match {
      case None => false;
      case _    => diagnosticsResponseExtn.isMRIMessage.get
    }
    println("isMRIMessage::" + isMRIMessage)

    val content: Content = if (isMRIMessage) {
      new Content(diagnosticsResponse.findings, diagnosticsResponse.history, diagnosticsResponse.impressions, diagnosticsResponseExtn.indications.getOrElse(""),
        diagnosticsResponseExtn.comparisions.getOrElse(""), diagnosticsResponseExtn.techniques.getOrElse(""))
    } else {
      new Content(diagnosticsResponse.findings, diagnosticsResponse.history, diagnosticsResponse.impressions)
    }

    val patient = ListPatientDao.getPatient(diagnosticsResponse.patientid)

    val dateOfBirthFormat = new java.text.SimpleDateFormat("dd MMMM yyyy")

    val patientName = patient.get.Title.get + ". " + patient.get.FullName
    val dateOfBirth = dateOfBirthFormat.format(patient.get.DOB.get)
    val NHSNumber = patient.get.AadhaarID.get
    val systemReferenceId = diagnosticsResponse.referralid

    val patientDetails: PatientDetails = new PatientDetails(patientName, dateOfBirth.toString(), NHSNumber, systemReferenceId.toString())

    println("Patient Details ::" + patientDetails.toString())
    log.info("Patient Details ::" + patientDetails.toString())

    val doctorName = referralEnquiry.get.doctorname.get
    val practiceName = practice.get.practicename.trim()
    val addressLine1 = practice.get.address1.get.trim()
    val addressLine2 = practice.get.address2.get.trim()
    val cityTown = practice.get.citytown.get.trim()
    val county = practice.get.county.get.trim()
    val postCode = practice.get.postcode.get

    val clinicDetails: ClinicDetails = new ClinicDetails(doctorName, practiceName, addressLine1, addressLine2, cityTown, county, postCode);

    println("Clinic Details ::" + clinicDetails.toString())
    log.info("Clinic Details ::" + clinicDetails.toString())

    val srcformat = new java.text.SimpleDateFormat("yyyyMMddHHmmss")
    val targetformat = new java.text.SimpleDateFormat("dd/MM/yyyy")

    val reporter = diagnosticsResponse.reportor
    val transcriber = diagnosticsResponse.reportor
    val reportstatus = diagnosticsResponse.reportstatus
    val exam = diagnosticsResponse.exam

    val examDate = srcformat.parse(diagnosticsResponse.examdate)
    val formattedExamDate = targetformat.format(examDate)
    val reportdate = srcformat.parse(diagnosticsResponse.reportdate)
    val formattedReportDate = targetformat.format(reportdate)

    val reportDetails: ReportDetails = new ReportDetails(formattedExamDate, exam, formattedReportDate, reporter, transcriber, reportstatus)

    println("Report Details ::" + reportDetails.toString())
    log.info("Report Details ::" + reportDetails.toString())

    val pageHeader: PageHeader = new PageHeader(patientName, NHSNumber, dateOfBirth.toString())

    println("Page Header ::" + pageHeader.toString())
    log.info("Page Header ::" + pageHeader.toString())

    println("isMRIMessage::"+isMRIMessage)
    val pdfWrapper: PDFWrapper = new PDFWrapper(clinicDetails, patientDetails, content, reportDetails, pageHeader,isMRIMessage)

    val tempfolder = current.configuration.getString("temp_folder").get

    //val unixTimestamp = Instant.now().getEpochSecond();

    val uuid = java.util.UUID.randomUUID.toString

    val diagnosisReportPDF = uuid + "_diagnosticsreport.pdf".toString()

    //val pdfFilePath = hcueDocuments.createPFDITextWrapper(diagnosisReportPDF, pdfWrapper)
    val pdfFilePath = hcueDocuments.createPFDITextWrapperv1(diagnosisReportPDF, pdfWrapper)
    val s3FilePath = patient.get.PatientID.toString() + "/" + diagnosticsResponse.referralid.toString() + "/" + diagnosisReportPDF.toString()
    val diagonstic_report = current.configuration.getString("buckets.diagnostics.report").get
    val result = hcueDocuments.S3UpdateHelperService(diagonstic_report, pdfFilePath, s3FilePath)

    println("Image Upload result :: " + result._1 + "; " + result._2)
    log.info("Image Upload result :: " + result._1 + "; " + result._2)

    if ("success".equalsIgnoreCase(result._1)) {
      val signedURLExpDate = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
       val urlexpiry = current.configuration.getInt("signed.url.expiry").get
      signedURLExpDate.add(java.util.Calendar.MONTH, urlexpiry)
      //val cfSignedURL = SignedURLGenerator.createSignedURL(filePath, hcueBuckets.DIAGNOSTICS_REPORT_CLOUDFRONT_URL, None, signedURLExpDate)
      val res = updateReportURL(diagnosticResponseId, result._2, new java.sql.Date(signedURLExpDate.getTimeInMillis()))
      println("URL Update result :: " + res)
      log.info("URL Update result :: " + res)
    }

    val localPDF = tempfolder + "/" + diagnosisReportPDF.toString()
    val localPDFFile = new File(localPDF)
    if (localPDFFile.exists()) {
      localPDFFile.delete()
      log.info(localPDF + " - file deleted")
      println(localPDF + " - file deleted")
    }

    println("*****************Starting PDF generation - Ends*****************")
    log.info("*****************Starting PDF generation - Ends*****************")
    return result._1
  }

  def generateDiagnsoticsReport(id: Long): String = {

    println("Starting PDF generation")

    println("id::" + id)
    log.info("id::" + id)

    val diagnosticsResponse = getDiagnosticsResponseById(id)
    println("diagnostic reponse record found ::" + diagnosticsResponse.appointmentid)

    val diagnosticRequest = getDiagnosticsByReferralId(diagnosticsResponse.referralid)

    //AddHospitalAppointmentDao.updateAppointmentStatus(diagnosticRequest.appointmentid)

    val hospitalAppmnt = AddHospitalAppointmentDao.getHospitalAppmntById(diagnosticRequest.appointmentid)
    val referralEnquiry = ReferrerEnquiryDao.findByID(hospitalAppmnt.Patientenquiryid.get)
    val practice = PracticeDao.findByID(referralEnquiry.get.practiceid.get)

    // Replacing & with &amp in history
    val history = diagnosticsResponse.history.replaceAll("&amp;", "&").replaceAll("&", "&amp;");

    // Replacing & with &amp in findings
    val findings = diagnosticsResponse.findings.replaceAll("&amp;", "&").replaceAll("&", "&amp;");

    // Replacing & with &amp in impressions
    val impressions = diagnosticsResponse.impressions.replaceAll("&amp;", "&").replaceAll("&", "&amp;");

    println("history::" + history)
    log.info("history::" + history)

    println("findings::" + findings)
    log.info("findings::" + findings)

    println("impressions::" + impressions)
    log.info("impressions::" + impressions)
    val context = new VelocityContext();

    context.put("history", history);
    context.put("findings", findings);
    context.put("impresssions", impressions);

    println("reading patient details::" + diagnosticsResponse.patientid)
    val patient = ListPatientDao.getPatient(diagnosticsResponse.patientid)
    val patientName = patient.get.Title.get + " " + patient.get.FullName
    val dob = patient.get.DOB.get

    println("patientName::" + patientName)
    log.info("patientName::" + patientName)
    println("dob::" + dob)
    log.info("dob::" + dob)

    // Replacing & with &amp in Practice name
    val practicename = practice match {
      case None => ""
      case _    => practice.get.practicename.replaceAll("&amp;", "&").replaceAll("&", "&amp;");
    }

    // Replacing & with &amp in Address 1
    val address1 = practice match {
      case None => ""
      case _ => practice.get.address1 match {
        case None => ""
        case _    => practice.get.address1.get.replaceAll("&amp;", "&").replaceAll("&", "&amp;");
      }
    }

    // Replacing & with &amp in Address 2
    val address2 = practice match {
      case None => ""
      case _ => practice.get.address2 match {
        case None => ""
        case _    => practice.get.address2.get.replaceAll("&amp;", "&").replaceAll("&", "&amp;");
      }
    }

    // Replacing & with &amp in City Town
    val citytown = practice match {
      case None => ""
      case _ => practice.get.citytown match {
        case None => ""
        case _    => practice.get.citytown.get.replaceAll("&amp;", "&").replaceAll("&", "&amp;");
      }
    }
    // Replacing & with &amp in Country
    val country = practice match {
      case None => ""
      case _ => practice.get.country match {
        case None => ""
        case _    => practice.get.country.get.replaceAll("&amp;", "&").replaceAll("&", "&amp;");
      }
    }

    context.put("doctorname", "Dr." + referralEnquiry.get.doctorname.get)
    context.put("practicename", practicename)
    context.put("address1", address1)
    context.put("address2", address2)
    context.put("citytown", citytown)
    context.put("county", country)
    context.put("postcode", practice.get.postcode.get)

    // Replacing & with &amp in patientName
    val PatientName = patientName.replaceAll("&amp;", "&").replaceAll("&", "&amp;");

    context.put("patientname", PatientName);
    val format = new java.text.SimpleDateFormat("dd MMMM yyyy")
    val dateOfBirth = format.format(dob)
    context.put("dob", dateOfBirth);
    context.put("nhsnumber", patient.get.AadhaarID.get)
    context.put("referrence", diagnosticsResponse.referralid)

    println("putting reporter details")
    log.info("putting reporter details")

    // Replacing & with &amp in ReporterName
    val ReporterName = diagnosticsResponse.reportor.replaceAll("&amp;", "&").replaceAll("&", "&amp;");

    context.put("reporter", ReporterName)
    context.put("transcriber", ReporterName)
    context.put("reportstatus", diagnosticsResponse.reportstatus)

    println("putting exam details")
    log.info("putting exam details")

    val srcformat = new java.text.SimpleDateFormat("yyyyMMddHHmmss")
    val targetformat = new java.text.SimpleDateFormat("dd-MM-yyyy")

    val examdate = srcformat.parse(diagnosticsResponse.examdate)
    log.info("examdate::" + targetformat.format(examdate))
    context.put("examdate", targetformat.format(examdate))
    context.put("exam", diagnosticsResponse.exam)

    val reportdate = srcformat.parse(diagnosticsResponse.reportdate)
    log.info("reportdate::" + targetformat.format(reportdate))
    context.put("reportdate", targetformat.format(reportdate))

    val healthtemp = current.configuration.getString("buckets.templates").get
    val templates_bucket = healthtemp
    println("templates_bucket::" + templates_bucket)
    log.info("templates_bucket::" + templates_bucket)

    val tempfolder = current.configuration.getString("temp_folder").get
    val loctemplates = current.configuration.getString("templates.diagnosticreport").get
    print("temp folder::" + tempfolder)
    var html = hcueCommunication.getHcueHTMLTempalte(context, templates_bucket, loctemplates)
    println("html generated")
    log.info("html generated")

    val unixTimestamp = Instant.now().getEpochSecond();
    val diagnosisReportHTML = unixTimestamp + "_diagnosticsreport.html".toString()
    val diagnosisReportPDF = unixTimestamp + "_diagnosticsreport.pdf".toString()

    var creatlocalhtml = hcueDocuments.CreateLocalHTML(html, diagnosisReportHTML)
    var pdfFilePath = hcueDocuments.createPFDItext(creatlocalhtml, diagnosisReportPDF)
    var filePath = patient.get.PatientID.toString() + "/" + diagnosticsResponse.referralid.toString() + "/" + diagnosisReportPDF.toString()

    println("creatlocalhtml::" + creatlocalhtml)
    log.info("creatlocalhtml::" + creatlocalhtml)
    println("pdfFilePath::" + pdfFilePath)
    log.info("pdfFilePath::" + pdfFilePath)
    println("filePath::" + filePath)
    log.info("filePath::" + filePath)

    val diagonstic_report = current.configuration.getString("buckets.diagnostics.report").get
    val result = hcueDocuments.S3UpdateHelperService(diagonstic_report, pdfFilePath, filePath)
    println("Image Upload result :: " + result._1)
    log.info("Image Upload result :: " + result._1)
    if ("success".equalsIgnoreCase(result._1)) {
      println("URL::" + result._1)
      log.info("URL::" + result._1)
      val signedURLExpDate = models.com.hcue.doctors.helper.CalenderDateConvertor.getISDTimeZoneDate()
       val urlexpiry = current.configuration.getInt("signed.url.expiry").get
      signedURLExpDate.add(java.util.Calendar.MONTH, urlexpiry)

      //val cfSignedURL = SignedURLGenerator.createSignedURL(filePath, hcueBuckets.DIAGNOSTICS_REPORT_CLOUDFRONT_URL, None, signedURLExpDate)
      val res = updateReportURL(id, result._2, new java.sql.Date(signedURLExpDate.getTimeInMillis()))
      println("URL Update result :: " + res)
      log.info("URL Update result :: " + res)
    }

    val localhtml = tempfolder + "/" + diagnosisReportHTML.toString()
    val localhtmlFile = new File(localhtml)
    if (localhtmlFile.exists()) {
      localhtmlFile.delete()
      log.info(localhtml + " - file deleted")
      println(localhtml + " - file deleted")
    }

    val localPDF = tempfolder + "/" + diagnosisReportPDF.toString()
    val localPDFFile = new File(localPDF)
    if (localPDFFile.exists()) {
      localPDFFile.delete()
      log.info(localPDF + " - file deleted")
      println(localPDF + " - file deleted")
    }
    log.info("PDF generation completed")
    println("PDF generation completed")
    return result._1
  }

  import com.hcue.hl7.message.HL7Message
  import com.hcue.hl7.message.Header
  import com.hcue.hl7.message.ObservationRequest
  import com.hcue.hl7.message.Patient
  import com.hcue.hl7.message.PatientVisit

  def invokeHL7Webservice(id: Long): String = {
    println("********HL7 Web Service call - Starts********")
    val addedRecord: Long = messagingDAO.addDiagnosticsRequest(id)
    println("record added in diag request::" + addedRecord)
    val requestId = messagingDAO.getDiagnosticsRequestById(addedRecord)
    println("get record from diagrequest::" + requestId)
    val patient = ListPatientDao.getPatient(requestId.patientid)

    val hospital = HospitalDao.findByID(requestId.scancentreid)
    println("get record from hospital::" + hospital)
    val appmnt: THospitalAppointment = AddHospitalAppointmentDao.getHospitalAppmntById(requestId.appointmentid)
    println("get record from appmnt::" + appmnt)
    val patientEnquiry = PatientEnquiryDao.findByID(appmnt.Patientenquiryid.get)
    println("get record from patientEnquiry::" + patientEnquiry)

    val enquirySpeciality = SpecialityLkupDao.getEnquirySpeciality(patientEnquiry.get.patientenquiryid)
    println("get record from enquirySpeciality::" + enquirySpeciality)

    val subSpecialityMap = enquirySpeciality.SubSpecialities.head
    val subSpeciality = subSpecialityMap._2

    println("subSpeciality::" + subSpeciality + "::enquirySpeciality.SpecialityCode" + enquirySpeciality.SpecialityCode)

    val speciality = SpecialityLkupDao.getSpecialityByIDandParentID(enquirySpeciality.SpecialityCode, subSpeciality)
    println("get record from speciality::" + speciality)

    val doctor = DoctorDao.findByID(requestId.SonographerId)

    val nhsnumber = patient.get.AadhaarID.get
    println("nhsnumber::" + nhsnumber)

    val message: HL7Message = new HL7Message

    val objpatient: Patient = new Patient
    objpatient.setPatientid(requestId.patientid)
    objpatient.setFirstName(patient.get.FirstName)
    objpatient.setLastName(patient.get.LastName.get)
    objpatient.setDob(patient.get.DOB.get)
    objpatient.setGender(patient.get.Gender);
    //objpatient.setNhsNumber(requestId.nhsnumber);
    objpatient.setStrNHSNumber(nhsnumber)
    message.setPatient(objpatient)

    println("Patient done")

    val header: Header = new Header
    header.setMsgCtrlId(requestId.requestid)
    header.setReceivingFacility(requestId.ScanCenterName);
    header.setReceivingApplication("PUKKAJ")
    header.setSendingApplication("C7HEALTHAPP")
    header.setSendingFacility("C7HEALTH")
    header.setDateTime(new Date())
    message.setHeader(header)

    println("header done::")

    val patientVisit: PatientVisit = new PatientVisit
    patientVisit.setDoctorCode(doctor.get.gpcode.get)
    patientVisit.setFirstName(doctor.get.FirstName)
    patientVisit.setLastName(doctor.get.LastName.get)
    message.setPatientVisit(patientVisit)
    println("patient visit done")

    val commonOrder: CommonOrder = new CommonOrder

    val orderControl = if (requestId.AppointmentStatus == "C") {
      "CA"
    } else {
      "NW"
    }
    println("orderControl::" + orderControl)
    commonOrder.setOrderControl(orderControl)
    message.setCommonOrder(commonOrder)

    val obrequest: ObservationRequest = new ObservationRequest
    obrequest.setClinicalInformation(requestId.ClinicalInfo)
    //obrequest.setEnquiryId(patientEnquiry.get.patientenquiryid)
    obrequest.setEnquiryId(requestId.appointmentid)
    obrequest.setRequestedDate(appmnt.ScheduleDate)

    // Check if the appointment is booked uin Emergency time slots
    // or Normal time slots
    appmnt.StartTime match {
      case x if x == "00:00" => obrequest.setRequestStartTime(appmnt.EmergencyStartTime)
      case _                 => obrequest.setRequestStartTime(appmnt.StartTime)
    }

    appmnt.EndTime match {
      case y if y == "00:00" => obrequest.setRequestEndTime(appmnt.EmergencyEndTime);
      case _                 => obrequest.setRequestEndTime(appmnt.EndTime)
    }

    obrequest.setScanCenterCode(hospital.get.BranchCode.getOrElse(""))
    obrequest.setScanCenterName(requestId.ScanCenterName)
    obrequest.setProcedureCode(speciality.get.dicomcode.get)
    obrequest.setUniversalIdCode(speciality.get.dicomcode.get)
    obrequest.setUniversalIdText(speciality.get.DoctorSpecialityDesc)
    message.setObservationRequest(obrequest)

    println("patientEnquiry done")
    println("FirstName::" + objpatient.getFirstName)
    println("FirstName::" + objpatient.getLastName)
    println("header::" + header.getReceivingFacility)
    println("header::" + header.getSendingApplication)
    println("header::" + header.getDateTime)

    println("SonographerId::" + patientVisit.getRefDoctorId)
    println("SonographerName1::" + patientVisit.getFirstName)
    println("SonographerName2::" + patientVisit.getLastName)

    println("clinical info::" + obrequest.getClinicalInformation)
    //println("header::" + header.getDateTime)

    val objHL7Wrapper: HL7Wrapper = new HL7Wrapper

    /*  try {
      val x = objHL7Wrapper.doInvokeHl7Endpoint(message)
      println("********HL7 Web Service call - Ends********")
      x
    } catch {
      case x:Exception => 
        println("Exception::"+x)
        throw new Exception(x)
      "Error"
    }*/

    val response = objHL7Wrapper.doInvokeHl7Endpoint(message)

    return response.toString()
  }

  def updateReportURL(responseid: Long, url: String, urlExpDate: java.sql.Date): String = db withSession { implicit session =>
    println("inside updateReportURL:requestid::" + responseid + "reportURL::" + url)
    diagnosticsResponse.filter { x => x.responseid === responseid }.map { x => (x.ReportURL, x.ReportURLExpDate) }.update(url, Some(urlExpDate)) match {
      case 1 => "Record successfully updated!"
      case 0 => "An error occurred!"
    }
  }

  def listHL7MessagesDAO(): Array[listObjHL7Message] = db withSession { implicit session =>

    val ILogAndAppointmenet = for {
      il <- integrationLog
      ha <- hospitalAppointment if il.AppointmentID === ha.AppointmentID
    } yield (il, ha)

    val logList = ILogAndAppointmenet.filter(x => x._1.MessageType.getOrElse("") === "OUTBOUND").sortBy(f => (f._1.Status).asc).list

    val resultSet: Array[listObjHL7Message] = new Array[listObjHL7Message](logList.length)

    val enquiryIds = logList.map(x => x._2.Patientenquiryid.get).toList

    val hospitalIds = logList.map(f => f._2.HospitalID.get).toList

    val patientIds = logList.map(k => k._1.PatientID).toList

    val refEnquiries = referrerEnquiry.filter(x => x.patientenquiryid.getOrElse(0L) inSet enquiryIds).list

    val hospitalDetails = hospital.filter { x => x.HospitalID inSet hospitalIds }.list

    val patientDetails = patients.filter { x => x.PatientID inSet patientIds }.list

    for (i <- 0 until logList.length) {

      val eqnuiryDetail = refEnquiries.find(x => x.patientenquiryid.get == logList(i)._2.Patientenquiryid.get)

      val hospitalDetail = hospitalDetails.find(x => x.HospitalID == logList(i)._2.HospitalID.get)

      val patientDetail = patientDetails.find { x => x.PatientID == logList(i)._1.PatientID }

      val HospitalName = if (hospitalDetail != None) { hospitalDetail.get.HospitalName.get } else { "" }

      val PracticeName = if (eqnuiryDetail != None) { eqnuiryDetail.get.practicename.get } else { "" }

      val CCGName = if (eqnuiryDetail != None) { eqnuiryDetail.get.ccgname.get } else { "" }

      val NHSNumber = if (patientDetail != None) { patientDetail.get.AadhaarID.get } else { "" }

      // println("Created"+logList(i)._1.Created)

      val timeFormat = new java.text.SimpleDateFormat("HH:mm")

      val timeval = timeFormat.format(logList(i)._1.Created)

      resultSet(i) = new listObjHL7Message(logList(i)._2.AppointmentID, logList(i)._2.ScheduleDate,
        logList(i)._1.LastName.getOrElse("") + " " + logList(i)._1.FirstName.getOrElse(""),
        logList(i)._1.PatientID, NHSNumber, HospitalName, PracticeName, CCGName, logList(i)._1.MessageType.get,
        logList(i)._1.Status.getOrElse(""), timeval)
    }

    return resultSet
  }
 
  
  def listHL7Messages(obj : getHL7Obj): listHL7Response= db withSession { implicit session =>

    import java.sql.Date
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
    val scheduleDate = LocalDate.parse(obj.Date)
    val sqlDate: Date = Date.valueOf(scheduleDate)
    
    val pageNumber = obj.pageNumber.getOrElse(0)
    val pageSize = obj.pageSize.getOrElse(0)
    
    val offset = (pageNumber - 1) * pageSize

    val logListFull = getHL7MessageList(sqlDate, obj.HospitalID.get).list
    
    val logList = logListFull.drop(offset).take(pageSize)
    
    val resultSet: Array[listObjHL7Message] = new Array[listObjHL7Message](logList.length)

    val enquiryIds = logList.map(x => x._3).toList

    val hospitalIds = logList.map(f => f._4).toList

    val patientIds = logList.map(k => k._5).toList

    val refEnquiries = referrerEnquiry.filter(x => x.patientenquiryid.getOrElse(0L) inSet enquiryIds).list

    val hospitalDetails = hospital.filter { x => x.HospitalID inSet hospitalIds }.list

    val patientDetails = patients.filter { x => x.PatientID inSet patientIds }.list
    
    for (i <- 0 until logList.length) {
      logList(i)._1

      val eqnuiryDetail = refEnquiries.find(x => x.patientenquiryid.get == logList(i)._3)

      val hospitalDetail = hospitalDetails.find(x => x.HospitalID == logList(i)._4)

      val patientDetail = patientDetails.find { x => x.PatientID == logList(i)._5 }

      val HospitalName = if (hospitalDetail != None) { hospitalDetail.get.HospitalName.get } else { "" }

      val PracticeName = if (eqnuiryDetail != None) { eqnuiryDetail.get.practicename.get } else { "" }

      val CCGName = if (eqnuiryDetail != None) { eqnuiryDetail.get.ccgname.get } else { "" }

      val NHSNumber = if (patientDetail != None) { patientDetail.get.AadhaarID.get } else { "" }

      val timeFormat = new java.text.SimpleDateFormat("HH:mm")

      val timeval = timeFormat.format(logList(i)._10)

      resultSet(i) = new listObjHL7Message(logList(i)._1, logList(i)._2,
        logList(i)._7 + " " + logList(i)._6,
        logList(i)._5, NHSNumber, HospitalName, PracticeName, CCGName, logList(i)._8,
        logList(i)._9, timeval)
    }

    val response = new listHL7Response(resultSet,Some(logListFull.length))
    
    return response
  }

  def getHL7MessageList(scheduleDate: java.sql.Date, hospitalid: Long) =
    sql"SELECT * FROM gethl7messages('#$scheduleDate','#$hospitalid')"
    .as[(Long, java.sql.Date, Long, Long, Long, String, String, String, String,java.sql.Date)]
  
  
  val messagingLog = MessagingLog.MessagingLog

  
  val appointmentMsgLog = AppointmentMsgLog.AppointmentMsgLog
  
  //This method is for adding SMS logs
  def addMessagingLog(message: String, destination: Long, Status: String, ExternalID: Long,
                      CrtUSR: Long, CrtUSRType: String, sendTime: Option[String]): String = db withSession { implicit session =>
    val diagRequestId = (messagingLog.map { x =>
      (x.Content, x.MessageType, x.Destination, x.Status, x.ExternalID, x.DateTime, x.CrtUSR, x.CrtUSRType)
    }
      returning messagingLog.map(_.MessageId)) +=
      (message, "SMS", destination, Status, ExternalID, sendTime, CrtUSR, CrtUSRType)
    return diagRequestId.toString()
  }
  //This method is for updating SMS logs
  def updateMessagingLog(request: messagelogobject): String = db withSession { implicit session =>
    val diagRequestId = messagingLog.filter { x => x.ExternalID === request.messageid }
      .map(x => (x.Metadata, x.Status))
      .update(request.drlmessage, request.deliverycode)
    return "updated successfully"
  }
  /*(value: (Long, String, Long, Long, String, String, Long, 
   * Option[String], String, Long, String, String, Long, String)
   * )(implicit session: scala.slick.jdbc.JdbcBackend#SessionDef)Long
*/
  def addAppointmentMsgLog(obj : AppointmentSMSMessageObj, ExternalID : Long, Status: String, APIResponse : String): String = db withSession { implicit session =>
    val diagRequestId = (appointmentMsgLog.map { x =>
      (x.AppointmentID, x.AppointmentStatus, x.PatientID, x.HospitalID, 
    		  x.Message, x.MessageType, x.Destination, x.DateTime, x.APIResponse,
    		  x.ExternalID,x.Status,x.DeliveryType,x.CrtUSR,x.CrtUSRType)
    }
      returning appointmentMsgLog.map(_.MessageId)) +=
      (obj.AppointmentID,obj.AppointmentStatus,obj.PatientID,obj.HospitalID,
          obj.Message,obj.MessageType,obj.Destination,obj.DateTime,Status,
          ExternalID,Status,obj.DeliveryType,obj.CrtUSR,obj.CrtUSRType)
    return diagRequestId.toString()
  }
}
