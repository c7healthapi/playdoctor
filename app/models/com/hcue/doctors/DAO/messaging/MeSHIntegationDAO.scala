package models.com.hcue.doctors.DAO.messaging

import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.optionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.queryToInsertInvoker
import scala.slick.driver.PostgresDriver.simple.queryToUpdateInvoker
import scala.slick.driver.PostgresDriver.simple.stringColumnType
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn

import models.com.hcue.doctors.slick.transactTable.messaging.MeshIntegation
import models.com.hcue.doctors.slick.transactTable.messaging.TMeshIntegation
import models.com.hcue.doctors.traits.dbProperties.db

object MeSHIntegrationDAO {

  val meshIntegation = MeshIntegation.MeshIntegation

  def addMeshIntegration(hl7ReferenceID: Long, fromMailBoxID: String, toMailBoxID: String, fileName: Option[String],
                         usrid: Long, usrtype: String): Long = db withSession { implicit session =>

    //val autoGenPatientID = (patients returning patients.map(_.PatientID)) += mappedValue

    val autoGenPatientID = (meshIntegation.map { x =>
      (x.HL7ReferenceID, x.FromMailBoxID, x.ToMailBoxID, x.TotalChunk, x.crtusr, x.crtusrtype)
    } returning meshIntegation.map(_.MeshIntegationID)) += (hl7ReferenceID, fromMailBoxID, toMailBoxID, 0, usrid, usrtype)

    System.out.println("ID : " + autoGenPatientID);
    return autoGenPatientID
  }

  def updateMeshIntegration(meshIntegationID: Long, hl7ReferenceID: Long, fileName: Option[String],
                            localID: Option[String], messageID: Option[String], totalChunk: Int, updtusr: Long, updtusrtype: String): Long = db withSession { implicit session =>

    meshIntegation.filter { x => x.MeshIntegationID === meshIntegationID }
      .map { x => (x.FileName, x.LocalID, x.MessageID, x.TotalChunk, x.updtusr, x.updtusrtype) }
      .update(fileName, localID, messageID, totalChunk, Some(updtusr), Some(updtusrtype))

    return meshIntegationID
  }

  def updateMeshIntegrationStatus(meshIntegationID: Long, hl7ReferenceID: Long, Status: String, updtusr: Long, updtusrtype: String): Long = db withSession { implicit session =>

    meshIntegation.filter { x => x.MeshIntegationID === meshIntegationID }.map { x => (x.Status, x.updtusr, x.updtusrtype) }
      .update(Some(Status), Some(updtusr), Some(updtusrtype))

    return meshIntegationID
  }

  def getMeshIntegrationByMessageID(messageID: Option[String]): Option[TMeshIntegation] = db withSession { implicit session =>

    meshIntegation.filter { x => x.MessageID === messageID }.firstOption
  }

  def getMeshIntegrationByID(meshIntegationID: Long): Option[TMeshIntegation] = db withSession { implicit session =>

    meshIntegation.filter { x => x.MeshIntegationID === meshIntegationID }.firstOption

  }

  def getMeshIntegrationByHL7ID(hl7ReferenceID: Long): Option[TMeshIntegation] = db withSession { implicit session =>

    meshIntegation.filter { x => x.HL7ReferenceID === hl7ReferenceID }.firstOption
  }

}