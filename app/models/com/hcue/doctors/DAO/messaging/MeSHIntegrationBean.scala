package models.com.hcue.doctors.DAO.messaging

import play.api.Logger

//getDiagnosticsResponseById
object MeSHIntegrationBean {

  private val log: Logger = Logger(this.getClass)

  def Initialized(hl7ReferenceID: Long, fromMailBoxID: String, toMailBoxID: String, usrid: Long, usrtype: String): String = {

    System.out.println("HL7 ReferenceID : " + hl7ReferenceID);
    System.out.println("From Mail Box ID : " + fromMailBoxID);
    System.out.println("To Mail Box ID : " + toMailBoxID);

    val url = messagingDAO.getByResponseId(hl7ReferenceID)

    if (url == None) {

      return "INVALID_REFERENCE_ID"

    }

    System.out.println(url.get.ReportURL.length())

    if (url.get.ReportURL.length() < 10) {

      return "REPORT_NOT_FOUND"

    }

    var meshRefereneID: Long = 0L

    System.out.println("------------------------------------------------------------------");
    val meshIntegrationID = MeSHIntegrationDAO.getMeshIntegrationByHL7ID(hl7ReferenceID)

    if (meshIntegrationID != None) {
      meshRefereneID = meshIntegrationID.get.MeshIntegationID
    }

    System.out.println("meshRefereneID " + meshRefereneID);
    if (meshRefereneID == 0L) {
      //Insert Record into TMeshIntegation and set the
      meshRefereneID = MeSHIntegrationDAO.addMeshIntegration(hl7ReferenceID, fromMailBoxID, toMailBoxID, None, usrid, usrtype)
    }

    System.out.println("Mesh IntegrationID : " + meshRefereneID);

    //(mailBoxID, nouce, nouceCount, status)
    val meshAuth = MeSHCall.authenticate(fromMailBoxID)

    System.out.println("Mesh Authenticate for Mail Box " + fromMailBoxID);

    if (meshAuth._4.equals("Success")) {

      System.out.println("Mesh Authenticate for Mail Box " + fromMailBoxID + " as " + meshAuth._4);

      /*
       * sendResponseMessage(errorCode: String, errorDescription: String, errorEvent: String, localID: String,
       * mailBoxID: String, messageID: String, nouce: String, nouceCount: Int, status: String) */

      val msgRespone = MeSHCall.sendMessage(meshAuth._2, meshAuth._3, fromMailBoxID, toMailBoxID, "RAD_MEDRPT_V2", url.get.ReportURL, "DATA")

      System.out.println(msgRespone)
      if (msgRespone.status.equals("Success")) {

        MeSHIntegrationDAO.updateMeshIntegration(meshRefereneID, hl7ReferenceID, Some(msgRespone.localID + ".pdf"), Some(msgRespone.localID), Some(msgRespone.messageID), 1, usrid, usrtype)

        MeSHIntegrationDAO.updateMeshIntegrationStatus(meshRefereneID, hl7ReferenceID, "MESSAGE_SEND", usrid, usrtype)
        //Check For Message Status
        var checkStatus = MeSHCall.checkMessageStatus(msgRespone.nouceCount, msgRespone.nouce, msgRespone.localID, msgRespone.mailBoxID)

        System.out.println("The Message ID " + msgRespone.messageID + " Status is " + checkStatus);

        MeSHIntegrationDAO.updateMeshIntegrationStatus(meshRefereneID, hl7ReferenceID, "MESSAGE_ACCEPTED", usrid, usrtype)

        return "MESSAGE_ACCEPTED"

      } else {
        //Sending Message  Failed
        System.out.println("Mesh Sending Message to Mail Box " + toMailBoxID + " is " + msgRespone.status);
        MeSHIntegrationDAO.updateMeshIntegrationStatus(meshRefereneID, hl7ReferenceID, "MESSAGE_NOT_DELIVERY", usrid, usrtype)
        return "MESSAGE_NOT_DELIVERY"
      }

    } else {
      //Comunication Error
      System.out.println("Mesh Authenticate for Mail Box " + fromMailBoxID + " as " + meshAuth._4);
      MeSHIntegrationDAO.updateMeshIntegrationStatus(meshRefereneID, hl7ReferenceID, "INVALID_MAILBOXID_AUTHENTICATION", usrid, usrtype)
      return "INVALID_MAILBOXID_AUTHENTICATION"
    }

  }

}