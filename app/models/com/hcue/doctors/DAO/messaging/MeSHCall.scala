package models.com.hcue.doctors.DAO.messaging

import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import org.json.JSONObject
import com.google.gson.Gson
import play.api.Play.current

object MeSHCall {

final var hostName = current.configuration.getString("mesh.hostname").get
final var authService = current.configuration.getString("mesh.auth").get
final var messageStatusService = current.configuration.getString("mesh.msgstatus").get
final var sendMessageService = current.configuration.getString("mesh.sendmsg").get
  
  
  def authenticate(mailBoxID: String): (String, String, Int, String) = {

    var nouce = "";

    var nouceCount = 0;

    var status = "Failure";

    try {

      val URL = hostName + authService + mailBoxID

      System.out.println("Auth : " + URL);
      val result = scala.io.Source.fromURL(URL).mkString.replace("\\", "")

      // Create a JSON object hierarchy from the results
      val jsonObj: JSONObject = new JSONObject(result);

      nouce = jsonObj.getString("nouce");

      nouceCount = jsonObj.getInt("nouceCount");

      status = jsonObj.getString("status");

      return (mailBoxID, nouce, nouceCount, status)

    } catch {
      case e: Exception =>
        e.printStackTrace()
    }

    return (mailBoxID, nouce, nouceCount, status)

  }

  def checkMessageStatus(nouceCount: Int, nouce: String, localID: String, mailIDBox: String): String = {

    try {

      val URL = hostName + messageStatusService + mailIDBox + "/" + nouceCount + "/" + nouce + "/" + localID

      var result = scala.io.Source.fromURL(URL).mkString //.replace("\\", "")
      // Create a JSON object hierarchy from the results

      System.out.println("------------------------------------------");
      result = result.replaceAll("\n", "");
      result = result.replaceAll("\t", "");
      result = result.replaceAll(" ", "");

      System.out.println(result);

      System.out.println("------------------------------------------");
      val jsonObj: JSONObject = new JSONObject(result);

      val r_nouce = jsonObj.getString("nouce");

      val r_nouceCount = jsonObj.getInt("nouceCount");

      val r_status = jsonObj.getString("status");

      val r_reasonObj = jsonObj.getString("rsstatus");

      return r_reasonObj

    } catch {
      case e: Exception =>
        e.printStackTrace()
    }

    return "INVALID_STATUS"

  }

  case class sendRequestMessage(nouce: String, nouceCount: Int, fromMailBoxID: String, toMailBoxID: String, workflowID: String, message: String, messageType: String)

  case class sendResponseMessage(errorCode: String, errorDescription: String, errorEvent: String, localID: String, mailBoxID: String, messageID: String, nouce: String, nouceCount: Int, status: String)

  def sendMessage(nouce: String, nouceCount: Int, fromMailBoxID: String, toMailBoxID: String, workflowID: String, message: String, messageType: String): sendResponseMessage = {

    var r_errorCode = ""
    var r_errorDescription = ""
    var r_errorEvent = ""
    var r_localID = ""
    var r_mailBoxID = ""
    var r_messageID = ""
    var r_nouce = nouce
    var r_nouceCount = nouceCount + 1
    var r_status = "Failed"

    try {

      val requestMsg = sendRequestMessage(nouce, nouceCount, fromMailBoxID, toMailBoxID, workflowID, message, messageType)

      val jsonString = new Gson().toJson(requestMsg)

      val httpClient: HttpClient = HttpClientBuilder.create().build();

      val request: HttpPost = new HttpPost(hostName + sendMessageService);
      val params: StringEntity = new StringEntity(jsonString);
      request.addHeader("content-type", "application/json");
      request.setEntity(params);
      request.addHeader("Accept-Encoding", "gzip");
      val response: HttpResponse = httpClient.execute(request)

      val httpEntity: HttpEntity = response.getEntity();

      val entity = EntityUtils.toString(httpEntity);

      System.out.println("---------------------------------------------")
      System.out.println(entity)
      System.out.println("---------------------------------------------")

      val jsonObj: JSONObject = new JSONObject(entity);

      r_errorCode = jsonObj.getString("errorCode")
      r_errorDescription = jsonObj.getString("errorDescription")
      r_errorEvent = jsonObj.getString("errorEvent")
      r_localID = jsonObj.getString("localID")
      r_mailBoxID = jsonObj.getString("mailBoxID")
      r_messageID = jsonObj.getString("messageID")
      r_nouce = jsonObj.getString("nouce")
      r_nouceCount = jsonObj.getInt("nouceCount");
      r_status = jsonObj.getString("status")

      return new sendResponseMessage(r_errorCode, r_errorDescription, r_errorEvent, r_localID, r_mailBoxID, r_messageID, r_nouce, r_nouceCount, r_status)

    } catch {
      case e: Exception =>
        e.printStackTrace()
    }

    return new sendResponseMessage(r_errorCode, r_errorDescription, r_errorEvent, r_localID, r_mailBoxID, r_messageID, r_nouce, r_nouceCount, r_status)

  }

}