package models.com.hcue.doctors.DAO.messaging

import scala.slick.driver.PostgresDriver.simple.queryToInsertInvoker

import models.com.hcue.doctors.Json.Request.messaging.diagnosticsrequestObj
import models.com.hcue.doctors.slick.transactTable.messaging.DiagnosticsResponse
import models.com.hcue.doctors.traits.dbProperties.db

object DiagnosticsResponseDAO {

  val diagnosticsresponse = DiagnosticsResponse.DiagnosticsResponse

  def adddiagnosticsresponse(a: diagnosticsrequestObj): String = db withSession { implicit session =>

    val returnval = "success"

    diagnosticsresponse.map { x =>
      (x.responseid, x.requestid, x.patientid, x.appointmentid, x.nhsnumber, x.referralid, x.findings,
        x.history, x.impressions, x.reportor, x.reportstatus, x.examdate, x.reportdate, x.exam)
    } += (a.responseid, a.requestid, a.patientid, a.appointmentid, a.nhsnumber, a.referralid, a.findings,
      a.history, a.impressions, a.reportor, a.reportstatus, a.examdate, a.reportdate, a.exam)

    return returnval

  }

}