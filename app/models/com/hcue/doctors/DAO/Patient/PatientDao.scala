package models.com.hcue.doctors.DAO.Patient

import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn

import models.com.hcue.doctors.slick.transactTable.patient.PatientExtn
import models.com.hcue.doctors.slick.transactTable.patient.Patients
import models.com.hcue.doctors.slick.transactTable.patient.TPatient
import models.com.hcue.doctors.slick.transactTable.patient.TPatientExtn
import models.com.hcue.doctors.traits.dbProperties.db

object PatientDao {
  val patient = Patients.patients
  val patientExtn = PatientExtn.PatientExtn

  def findByID(Id: Long): Option[TPatient] = db withSession { implicit session =>

    patient.filter { x => x.PatientID === Id }.firstOption

  }

  def findByIDs(Id: List[Long]): List[TPatient] = db withSession { implicit session =>

    patient.filter { x => x.PatientID inSet Id }.list

  }

  def findPatExtnByID(Id: Long): Option[TPatientExtn] = db withSession { implicit session =>

    patientExtn.filter { x => x.PatientID === Id }.firstOption

  }

  def findPatExtnByIDs(Id: List[Long]): List[TPatientExtn] = db withSession { implicit session =>

    patientExtn.filter { x => x.PatientID inSet Id }.list

  }

}
