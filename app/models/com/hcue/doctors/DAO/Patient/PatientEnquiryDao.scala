package models.com.hcue.doctors.DAO.Patient

import models.com.hcue.doctors.slick.transactTable.doctor.TPatientEnquiry
import models.com.hcue.doctors.slick.transactTable.doctor.PatientEnquiry
import models.com.hcue.doctors.traits.dbProperties.db
import scala.slick.driver.PostgresDriver.simple._

object PatientEnquiryDao {

  val patientEnquiry = PatientEnquiry.PatientEnquiry

  def findByID(Id: Long): Option[TPatientEnquiry] = db withSession { implicit session =>

    patientEnquiry.filter { x => x.patientenquiryid === Id }.firstOption

  }

}