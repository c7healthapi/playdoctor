package models.com.hcue.doctors.DAO.Patient

import models.com.hcue.doctors.slick.transactTable.patient.ReferrerEnquiry
import models.com.hcue.doctors.slick.transactTable.patient.TreferrerEnquiry
import models.com.hcue.doctors.traits.dbProperties.db
import scala.slick.driver.PostgresDriver.simple._

object ReferrerEnquiryDao {

  val referralEnquiry = ReferrerEnquiry.ReferrerEnquiry

  def findByID(Id: Long): Option[TreferrerEnquiry] = db withSession { implicit session =>

    referralEnquiry.filter { x => x.patientenquiryid === Id }.firstOption

  }

}