package models.com.hcue.doctors.DAO.Hospital

import scala.slick.driver.PostgresDriver.simple._

import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.slick.transactTable.hospitals.THospital
import models.com.hcue.doctors.traits.dbProperties.db

object HospitalDao {
  val hospital = Hospital.Hospital

  def findByID(Id: Long): Option[THospital] = db withSession { implicit session =>
    hospital.filter { x => x.HospitalID === Id }.firstOption

  }
}