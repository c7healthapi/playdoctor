package models.com.hcue.doctors.DAO.Doctor


import scala.slick.lifted.TableQuery
import scala.slick.lifted.CanBeQueryCondition
import scala.slick.driver.PostgresDriver.simple._
import java.sql.Date
import play.api.db.DB
import play.api.db.slick.DB
import org.slf4j.LoggerFactory
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn
import play.api.libs.json.JsValue

object DoctorAddressExtnDao {
  
  
  val doctorsAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn


  
  
  implicit class DynamicFilter[X, Y](val query: scala.slick.lifted.Query[X, Y, Seq]) {

    def filteredBy[T](data: Option[T])(f: (X, T) => Column[Option[Boolean]]): scala.slick.lifted.Query[X, Y, Seq] = {
      
      data map { o => query.filter(f(_, o)) } getOrElse { query }

    }

  }
 
  def selectingRecords(DoctorID: Option[List[Long]], 
      HospitalID: Option[List[Option[Long]]], 
      ParentHospitalID: Option[List[Option[Long]]],
      HospitalCode: Option[Option[String]], 
      AddressID: Option[List[Long]],
      PrimaryIND: Option[String],
      Latitude: Option[Option[String]], 
      Longitude: Option[Option[String]],
      ClinicImages: Option[Option[Map[String,String]]], 
      ClinicDocuments: Option[Option[JsValue]],
      ClinicImageExpDt: Option[Option[java.sql.Date]], 
      RoleID: Option[Option[String]],
      GroupID: Option[Option[String]], 
      IsConsultant: Option[Option[String]],
      RatePerHour: Option[Option[Float]], 
      MinPerCase: Option[Option[Int]], 
      CrtUSR: Option[List[Long]],
      CrtUSRType: Option[List[String]], 
      UpdtUSR: Option[Option[Long]],
      UpdtUSRType: Option[Option[String]],
      RateBelowTwenty: Option[Option[BigDecimal]],
      RateAboveTwenty: Option[Option[BigDecimal]]):List[TDoctorAddressExtn] = db withSession { implicit request =>
   
        
        
    return doctorsAddressExtn
      .filteredBy(DoctorID)((x, v) => (x.DoctorID inSet v))
      .filteredBy(HospitalID)((x, v) => (x.HospitalID.getOrElse(0L) inSet v.map { x => x.getOrElse(-1L) }))
      .filteredBy(ParentHospitalID)((x, v) => (x.ParentHospitalID.getOrElse(0L) inSet v.map { x => x.getOrElse(-1L) }))
      .filteredBy(HospitalCode)((x, v) => (x.HospitalCode === v))
      .filteredBy(AddressID)((x, v) => (x.AddressID inSet v))
      .filteredBy(PrimaryIND)((x, v) => (x.PrimaryIND === v))
      .filteredBy(Latitude)((x, v) => (x.Latitude === v))
      .filteredBy(Longitude)((x, v) => (x.Longitude === v))
      .filteredBy(ClinicImageExpDt)((x, v) => (x.ClinicImageExpDt === v))
      .filteredBy(RoleID)((x, v) => (x.RoleID inSet v))
      .filteredBy(GroupID)((x, v) => (x.GroupID inSet v))
      .filteredBy(IsConsultant)((x, v) => (x.IsConsultant === v))
      .filteredBy(RatePerHour)((x, v) => (x.RatePerHour === v))
      .filteredBy(MinPerCase)((x, v) => (x.MinPerCase inSet v))
      .filteredBy(CrtUSR)((x, v) => (x.CrtUSR inSet v))
      .filteredBy(CrtUSRType)((x, v) => (x.CrtUSRType inSet v))
      .filteredBy(UpdtUSR)((x, v) => (x.UpdtUSR inSet v))
      .filteredBy(UpdtUSRType)((x, v) => (x.UpdtUSRType inSet v))
      .filteredBy(RateBelowTwenty)((x, v) => (x.RateBelowTwenty === v))
      .filteredBy(RateAboveTwenty)((x, v) => (x.RateAboveTwenty === v))
      .list

    
  }
  
}