package models.com.hcue.doctors.DAO.Doctor

import scala.slick.driver.PostgresDriver.simple._
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorAddressExtn


object DoctorAddressDao {

  val doctorAddress = DoctorAddress.DoctorsAddress
  val doctorAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn
  
  
  def findAddressByID(Id: Long): Option[TDoctorAddress] = db withSession { implicit session =>

    doctorAddress.filter { x => x.DoctorID === Id }.firstOption

  }

  def findAddressByIDS(Id: List[Long]): List[TDoctorAddress] = db withSession { implicit session =>

    doctorAddress.filter { x => x.DoctorID inSet Id}.list

  }
  
  def findAddressExtnByID(Id: Long): Option[TDoctorAddressExtn] = db withSession { implicit session =>

    doctorAddressExtn.filter { x => x.DoctorID === Id }.firstOption

  }

  def findAddressExtnByIDS(Id: List[Long]): List[TDoctorAddressExtn] = db withSession { implicit session =>

    doctorAddressExtn.filter { x => x.DoctorID inSet Id}.list

  }
  
  def findAddressExtnByDocIDHospID(DoctorID :Long,HospitalID :Long) = db withSession { implicit session =>
    
    doctorAddressExtn.filter { x => x.HospitalID === DoctorID && x.DoctorID === HospitalID}.firstOption    
    
  }

  def findAddressExtnListByHospID(HospitalID: Long) : List[TDoctorAddressExtn] = db withSession { implicit session =>

    doctorAddressExtn.filter { x => x.HospitalID === HospitalID }.list

  }
  
  def findAddressExtnListByID(Id: Long): List[TDoctorAddressExtn] = db withSession { implicit session =>

    doctorAddressExtn.filter { x => x.DoctorID === Id }.list

  }
  
  
   def findAddressListByID(Id: Long): List[TDoctorAddress] = db withSession { implicit session =>

    doctorAddress.filter { x => x.DoctorID === Id }.list

  }
   
   def findAddressExtnByAddressID(Id: Long): Option[TDoctorAddressExtn] = db withSession { implicit session =>

    doctorAddressExtn.filter { x => x.AddressID === Id }.firstOption

  }
}