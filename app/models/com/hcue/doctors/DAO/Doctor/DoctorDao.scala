package models.com.hcue.doctors.DAO.Doctor

import scala.slick.driver.PostgresDriver.simple._

import models.com.hcue.doctors.slick.transactTable.doctor.Doctors
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctor
import models.com.hcue.doctors.traits.dbProperties.db
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsExtn
import models.com.hcue.doctors.slick.transactTable.doctor.TDoctorExtn

object DoctorDao {

  val doctors = Doctors.Doctors
  val doctorExtn = DoctorsExtn.DoctorsExtn
 

  def findByID(Id: Long): Option[TDoctor] = db withSession { implicit session =>

    doctors.filter { x => x.DoctorID === Id }.firstOption

  }

  def findByIDS(Id: List[Long]): List[TDoctor] = db withSession { implicit session =>

    doctors.filter { x => x.DoctorID inSet Id}.list

  }

  def findByLoginID(loginID: String): Option[TDoctor] = db withSession { implicit session =>

    doctors.filter { x => x.DoctorLoginID === loginID }.firstOption

  }

  def findByName(id: Long): String = db withSession { implicit session =>

    val doctorName = doctors.filter { x => x.DoctorID === id }.map { x => x.FullName }.firstOption
    if (doctorName.value != None) {
      var name = doctorName.get
      return name
    } else { "Not Valid ID" }

  }

  def findByIDInExtn(Id: Long): Option[TDoctorExtn] = db withSession { implicit session =>

    doctorExtn.filter { x => x.DoctorID === Id }.firstOption

  }

}