package models.com.hcue.doctors.DAO.Doctor

import scala.slick.driver.PostgresDriver.simple.booleanColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.booleanColumnType
import scala.slick.driver.PostgresDriver.simple.booleanOptionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.optionColumnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.stringColumnType
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn

import models.com.hcue.doctors.Json.Request.AdminDashboardInfoRequestObj
import models.com.hcue.doctors.Json.Request.AdminDashboardInfoResponseObj
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorAddress
import models.com.hcue.doctors.slick.transactTable.doctor.DoctorsAddressExtn
import models.com.hcue.doctors.slick.transactTable.hCueUsers.HospitalGrpSetting
import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.traits.dbProperties.db

object AdminDashBoardDao {

  val hospital = Hospital.Hospital
  val doctorsAddressExtn = DoctorsAddressExtn.DoctorsAddressExtn
  val doctorAddress = DoctorAddress.DoctorsAddress
  val hospitalgrpsetting = HospitalGrpSetting.HospitalGrpSetting

  def adminDashBoardDetails(a: AdminDashboardInfoRequestObj): Option[AdminDashboardInfoResponseObj] = db withSession { implicit session =>

    val parentHospitalId = a.HospitalID

    //val hospinfo = hospital.filter { x => x.HospitalID === parentHospitalId }.firstOption

    var queryForDoctorAddressInfo = for {
      (doctorAdd, doctorAddExtn) <- doctorAddress innerJoin doctorsAddressExtn on (_.AddressID === _.AddressID)
      if doctorAdd.Active === "Y" && doctorAddExtn.ParentHospitalID === parentHospitalId
    } yield (doctorAdd, doctorAddExtn)

    var userRecord = queryForDoctorAddressInfo.list

    var hospitalSonographerInfo = userRecord.filter { x => x._2.RoleID == Some("SONOGRAPHER") }.map { x => x._1.DoctorID }.toList.distinct.length
    var hospitalRecepInfo = userRecord.filter { x => x._2.RoleID == Some("RECPTNIST") }.map { x => x._1.DoctorID }.toList.distinct.length
    var hospitalLeadAgentInfo = userRecord.filter { x => x._2.RoleID == Some("LEADAGENT") }.map { x => x._1.DoctorID }.toList.distinct.length
    var hospitalAgentInfo = userRecord.filter { x => x._2.RoleID == Some("AGENT") }.map { x => x._1.DoctorID }.toList.distinct.length
    var hospitalClinicalSupportWorkerInfo = userRecord.filter { x => x._2.RoleID == Some("CLINICALSUPPWORKER") }.map { x => x._1.DoctorID }.toList.distinct.length

    val BranchCreated = hospital.filter { x => x.ParentHospitalID === a.HospitalID && x.ActiveIND === "Y"}.list.length

    var Totalusercount = hospitalSonographerInfo + hospitalRecepInfo + hospitalLeadAgentInfo + hospitalAgentInfo + hospitalClinicalSupportWorkerInfo

    val hospitalProfileInfo = hospitalgrpsetting.filter { x => x.HospitalID === parentHospitalId && x.GroupName =!= null.asInstanceOf[Option[String]] && x.Active === "Y" }.map { x => x.GroupName }.list.distinct

    val Profilecount = hospitalProfileInfo.length

    return new Some(AdminDashboardInfoResponseObj(BranchCreated,
      hospitalRecepInfo, hospitalLeadAgentInfo, hospitalSonographerInfo, hospitalAgentInfo, hospitalClinicalSupportWorkerInfo,
      Totalusercount, Profilecount, hospitalProfileInfo))

  }

}

