package models.com.hcue.doctors.Bean

import java.io.PrintWriter
import java.io.StringWriter

import scala.concurrent.Future

import org.apache.velocity.VelocityContext

import models.com.hcue.doctors.Json.Response.AddErrorLogReq
import models.com.hcue.doctors.Json.Response.errorObj
import models.com.hcue.doctors.Json.Response.responseObj
import models.com.hcue.doctors.helper.CalenderDateConvertor
import models.com.hcue.doctors.helper.hcueCommunication
import models.com.hcue.doctors.traits.add.AddHcueErrorLogDao
import play.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.mvc.RequestHeader
import play.api.Play.current

object ErrorMessageBean {

  val log = Logger.of("application")

  val Environment = current.configuration.getString("notify.error.env").get
  val project = current.configuration.getString("notify.error.project").get
  val errorNotifyFlag = current.configuration.getBoolean("notify.error.flag").get
  val errorNotifyToEmail = current.configuration.getString("notify.error.to.mail").get
  val errorNotifyCopyEmail = current.configuration.getString("notify.error.copy.mail").get
  val errorNotifyBadRequest = current.configuration.getString("notify.error.badrequest").get
  val errorNotifyHcueError = current.configuration.getString("notify.error.hcueerror").get

  def onBadRequest(request: RequestHeader, inPutRequest: JsValue, outPutRequest: JsValue) = {

    try {
      val sendNotification: Future[String] = scala.concurrent.Future {

        val currentDate: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
        val currentDateFormat1 = CalenderDateConvertor.getDateFormatter(currentDate)
        val istTime = CalenderDateConvertor.getISDTime(currentDate)

        var RemoteAddress = "-"
        var RequestURL = "-"
        var RequestMethod = "-"
        // var RequestParameter  = "-"
        // var ResponseParameter = "-"

        var subject = "--"
        if (request != null) {

          RemoteAddress = request.remoteAddress
          RequestURL = request.uri
          RequestMethod = request.method

        }

        // RequestParameter = inPutRequest
        // ResponseParameter = outPutRequest

        val errorid = AddHcueErrorLogDao.addHcueErrorLog(new AddErrorLogReq(None, 400, Environment, project, RemoteAddress, RequestURL, RequestMethod,
          Some(Json.toJson(inPutRequest)), Some(Json.toJson(outPutRequest)), None, None, 0, "ADMIN"))

        if (errorNotifyFlag) {
          sendErrorLogEmailFromGmail(Environment, project, RemoteAddress, RequestURL, RequestMethod, inPutRequest, Some(outPutRequest), None, errorid, 400)

        }
        "Success"
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onErrorInFuture(null, "Play Doctor " + this.getClass.getName + ".onBadRequest.sendNotification", "InFutureBlock", e, None)
    }

  }

  def onErrorInFuture(request: RequestHeader, ClassName: String, FutureBlock: String, throwable: Throwable, inPut: Option[JsValue]): responseObj = {

    try {
      val sendNotification: Future[String] = scala.concurrent.Future {

        val currentDate: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
        val currentDateFormat1 = CalenderDateConvertor.getDateFormatter(currentDate)
        val istTime = CalenderDateConvertor.getISDTime(currentDate)

        var RemoteAddress = "-"
        var RequestURL = ClassName
        var RequestMethod = FutureBlock
        var RequestParameter = "-"
        var ResponseParameter = "-"

        var subject = "--"

        //RequestParameter = inPut.getOrElse("-")

        val sw = new StringWriter
        throwable.printStackTrace(new PrintWriter(sw))

        val errorid = AddHcueErrorLogDao.addHcueErrorLog(new AddErrorLogReq(None, 500, Environment, project, RemoteAddress, RequestURL, RequestMethod,
          Some(Json.toJson(inPut)), None, Some(sw.toString()), None, 0, "ADMIN"))

        if (errorNotifyFlag) {
          sendErrorLogEmailFromGmail(Environment, project, RemoteAddress, RequestURL, RequestMethod, inPut.get, None, Some(sw.toString()), errorid, 500)
        }

        "Success"
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onErrorInFuture(null, "Play Doctor " + this.getClass.getName + ".onErrorInFuture.sendNotification", "InFutureBlock", e, None)
    }

    return buildErrorObject(throwable.getMessage)

  }

  def onError(request: RequestHeader, throwable: Throwable, inPut: Option[JsValue]): responseObj = {
    try {
      val sendNotification: Future[String] = scala.concurrent.Future {
        println("Future Block invoked")

        val currentDate: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
        val currentDateFormat1 = CalenderDateConvertor.getDateFormatter(currentDate)
        val istTime = CalenderDateConvertor.getISDTime(currentDate)

        var RemoteAddress = "-"
        var RequestURL = "-"
        var RequestMethod = "-"
        // var RequestParameter = "-"
        var ResponseParameter = "-"

        var subject = "--"
        if (request != null) {

          RemoteAddress = request.remoteAddress
          RequestURL = request.uri
          RequestMethod = request.method

        }

        val sw = new StringWriter
        throwable.printStackTrace(new PrintWriter(sw))

        val errorid = AddHcueErrorLogDao.addHcueErrorLog(new AddErrorLogReq(None, 500, Environment, project, RemoteAddress, RequestURL, RequestMethod,
          Some(Json.toJson(inPut)), None, Some(sw.toString()), None, 0, "ADMIN"))

        if (errorNotifyFlag) {
          try {
            val s = sendErrorLogEmailFromGmail(Environment, project, RemoteAddress, RequestURL, RequestMethod, inPut.getOrElse(Json.toJson("GET Method")), None, Some(sw.toString()), errorid, 500)
          } catch {
            case e: Exception =>
              e.printStackTrace()
          }
        }

        "Success"
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        val status = ErrorMessageBean.onErrorInFuture(null, "Play Doctor " + this.getClass.getName + ".onError.sendNotification", "InFutureBlock", e, None)
    }

    return buildErrorObject(throwable.getMessage)

  }

  def buildErrorObject(errorMsg: String): responseObj = {

    val errorSet: errorObj = new errorObj(errorMsg, "")
    val error: responseObj = new responseObj("failure", None, Some(errorSet))
    return error

  }

  def sendErrorLogEmailFromGmail(Environment: String, project: String, RemoteAddress: String, RequestURL: String, RequestMethod: String,
                                 inPutRequest: JsValue, outPutRequest: Option[JsValue], outPutStringValue: Option[String], errorID: Long, errorCode: Long): String = {
    val currentDate: java.util.Date = new java.util.Date(CalenderDateConvertor.getISDDate().getTime())
    val currentDateFormat1 = CalenderDateConvertor.getDateFormatter(currentDate)
    val istTime = CalenderDateConvertor.getISDTime(currentDate)

    val context = new VelocityContext();

    val subject = "ErrorDetails"

    context.put("strDate", currentDateFormat1.format(currentDate))

    context.put("strTime", istTime)

    context.put("environment", Environment)

    context.put("project", project)

    context.put("errorID", errorID)

    context.put("errorcode", errorCode)

    context.put("requestURL", RequestURL)

    context.put("requestMethod", RequestMethod)

    context.put("remoteAddress", RemoteAddress)

    context.put("requestParameter", inPutRequest)

    var to = errorNotifyToEmail
    var cc = errorNotifyCopyEmail
    if (errorCode == 400) {
      context.put("responseParameter", outPutRequest.get)
      context.put("shortMessage", "")
      context.put("detailsMessage", "")
    } else {
      to = errorNotifyToEmail
      cc = errorNotifyCopyEmail
      context.put("responseParameter", "")
      context.put("shortMessage", outPutStringValue.get)
      context.put("detailsMessage", "-")
    }

    val localData = if (errorCode == 400) {
      hcueCommunication.readHTMLTempalteURL(context, errorNotifyBadRequest)
    } else {
      hcueCommunication.readHTMLTempalteURL(context, errorNotifyHcueError)
    }

    return hcueCommunication.sendEmailFromGmail(localData, to, Some(cc), None, Environment + " : " + project + " - " + errorCode + " - " + errorID)

    //return  ""

  }

}