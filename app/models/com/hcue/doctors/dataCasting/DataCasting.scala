package models.com.hcue.doctors.dataCasting

import play.Logger
import java.sql.Date

class DataCasting {

  private val log = Logger.of("application")

  log.info("Created c3p0 connection pool")

  def covertToOptionString(value: String): Option[String] = {
    Some(value)
  }

  def stringconvertion(value: Option[String]): String = {

    if (value == None)
      return null

    val valueLength: Int = value.toString().length()
    val valueName: String = value.toString()

    valueName.substring(5, valueLength - 1)

  }

  def getRequiredValue(newValue: String, OldValue: String): String = {

    if (newValue != None) {
      return newValue
    } else {
      return OldValue
    }

  }

  def getRequiredValue(newValue: Option[String], OldValue: String): String = {

    if (newValue != None) {
      return stringconvertion(newValue)
    }

    if (newValue == None) {
      return OldValue
    } else {
      return stringconvertion(Some(OldValue))
    }

  }

  def getRequiredValue(newValue: Option[String], OldValue: Option[String]): Option[String] = {

    if (newValue == None && OldValue == None) {
      return None
    } else if (newValue == None) {
      return OldValue
    } else {
      return newValue
    }

  }

  def getRequiredValue(newValue: Long, OldValue: Long): Long = {

    if (newValue != 0L) {
      return newValue
    } else {
      return OldValue
    }

  }

  def getRequiredValue(newValue: Option[Long], OldValue: Long): Long = {

    if (newValue != None) {
      return newValue.get
    }

    if (newValue == None) {
      return OldValue
    } else {
      return OldValue
    }

  }

  def getRequiredLongValue(newValue: Option[Long], OldValue: Option[Long]): Option[Long] = {

    if (newValue == None && OldValue == None) {
      return None
    } else if (newValue == None) {
      return OldValue
    } else {
      return newValue
    }

  }

  def getRequiredDate(newValue: Option[java.sql.Date], OldValue: Option[java.sql.Date]): Option[java.sql.Date] = {

    if (newValue != None) {
      return newValue
    } else {
      return OldValue
    }

  }

  def getRequiredInt(newValue: Option[Int], OldValue: Option[Int]): Option[Int] = {

    if (newValue == None && OldValue == None) {
      return None
    } else if (newValue == None) {
      return OldValue
    } else {
      return newValue
    }

  }

  def convertToLower(newValue: Option[String]): Option[String] = {

    if (newValue == None) {
      return Some("")
    } else {
      val reqValue = stringconvertion(newValue)
      return Some(reqValue.toLowerCase())
    }

  }

  def getRequiredLong(newValue: Option[Long]): Long = {

    if (newValue == None) {
      return 0
    } else {
      return convertToLong(newValue)
    }

  }

  def getRequiredHStoreValue(newValue: Option[Map[String, String]], OldValue: Option[Map[String, String]]): Option[Map[String, String]] = {

    if (newValue == None && OldValue == None) {
      return None
    } else if (newValue == None) {
      return OldValue
    } else {
      return newValue
    }

  }

  def getRequiredFloatValue(newValue: Option[Float], OldValue: Option[Float]): Option[Float] = {

    if (newValue == None && OldValue == None) {
      return None
    } else if (newValue == None) {
      return OldValue
    } else {
      return newValue
    }

  }

  def convertToLong(newValue: Option[Long]): Long = {

    if (newValue == None) {
      return 0
    } else {

      val valueLength: Int = newValue.toString().length()
      val valueName: String = newValue.toString()
      valueName.substring(5, valueLength - 1)

      return valueName.toLong
    }

  }

  def getRequiredLong(newValue: Option[Long], OldValue: Option[Long]): Option[Long] = {

    if (newValue == None && OldValue == None) {
      return None
    } else if (newValue == None) {
      return OldValue
    } else {
      return newValue
    }

  }

}

