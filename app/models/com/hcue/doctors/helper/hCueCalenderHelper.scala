package models.com.hcue.doctors.helper

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.TimeZone

import play.Logger

object hCueCalenderHelper {

  private val log = Logger.of("application")
  val dateFormat: SimpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy");
  val SMSDateFormat: SimpleDateFormat = new SimpleDateFormat("MMM d, yyyy hh:mm aaa");
  val SMSDateFormatDDMMYYYY: SimpleDateFormat = new SimpleDateFormat("MMM d, yyyy");
  val FollowUpSMSDateFormat: SimpleDateFormat = new SimpleDateFormat("MMM d, yyyy");

  val timeZone: String = "Asia/Calcutta"
  //val timeZone:String = "Asia/Singapore"
  
  val uk_timeZone : String = "Europe/London"

  def getISDTimeZoneDate(): Calendar = {
    new GregorianCalendar(TimeZone.getTimeZone(timeZone));
  }
  
  def getUKTimeZoneDate(): Calendar = {
    new GregorianCalendar(TimeZone.getTimeZone(uk_timeZone));
  }
  
  def getUKDate(): java.sql.Date = {
    new java.sql.Date(getUKTimeZoneDate.getTimeInMillis());
  }

  def getISDDate(): java.sql.Date = {
    new java.sql.Date(getISDTimeZoneDate().getTimeInMillis())
  }

  def getISDTimeStamp(): java.sql.Timestamp = {
    new java.sql.Timestamp(getISDDate().getTime())
  }

  def formatedDate(): String = {
    return dateFormat.format(getISDDate)
  }

  def emailFormatedDate(date: java.sql.Date): String = {
    return dateFormat.format(date)
  }

  def smsDateFormat(): String = {
    return SMSDateFormat.format(getISDDate);
  }

  def smsDateFormatDDMMYYYY(): String = {
    return SMSDateFormatDDMMYYYY.format(getISDDate);
  }

  def smsDateFormat(sqlDate: java.sql.Date): String = {
    return SMSDateFormat.format(sqlDate);
  }

  def followUpSMSDateFormat(sqlDate: java.sql.Date): String = {
    return FollowUpSMSDateFormat.format(sqlDate);
  }

  def getISDDate(count: Int): java.sql.Date = {
    val cal: Calendar = getISDTimeZoneDate();
    cal.add(Calendar.DATE, count);
    new java.sql.Date(cal.getTimeInMillis())
  }

  def getAge(dateOfBirth: Option[java.sql.Date]): Int = {

    if (dateOfBirth == None) {
      return 0
    }

    val now: Calendar = Calendar.getInstance();
    val dob: Calendar = Calendar.getInstance();

    dob.setTime(dateOfBirth.get);

    if (dob.after(now)) {
      throw new IllegalArgumentException("Can't be born in the future");
    }

    var age: Int = now.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

    if (now.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
      age = age - 1;
    }

    return age;
  }
}