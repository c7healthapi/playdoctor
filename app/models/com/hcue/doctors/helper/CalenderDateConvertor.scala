package models.com.hcue.doctors.helper

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.TimeZone

import play.Logger

object CalenderDateConvertor {

  val DateDisplayFormat: SimpleDateFormat = new SimpleDateFormat("MMM d, yyyy hh:mm aaa");
  val SMSDateFormat: SimpleDateFormat = new SimpleDateFormat("MMM d, yyyy hh:mm aaa");
  val HHMM: SimpleDateFormat = new SimpleDateFormat("HH:mm");
  val MM: SimpleDateFormat = new SimpleDateFormat("mm");
  val DDMMYYYY: SimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
  val SMSDateFormatDDMMYYYY: SimpleDateFormat = new SimpleDateFormat("MMM d, yyyy")
  //  val EEEEDDthMMMYYY: SimpleDateFormat = new SimpleDateFormat("EEEEE, dd +  + MMMM yyyy");

  val log = Logger.of("application")

  val istTimeZone: String = "Asia/Kolkata"
  val timeZone: String = "Asia/Calcutta"
  //val timeZone:String = "Asia/Singapore"
  val ukTimeZone:String = "Europe/London"

  def getISDTimeZoneDate(): Calendar = {
    new GregorianCalendar(TimeZone.getTimeZone(timeZone));
  }
  
  def getUKTimeZoneDate(): Calendar = {
    new GregorianCalendar(TimeZone.getTimeZone(ukTimeZone));
  }

  def getSMSDateFormat(date: java.util.Date): String = {
    DateDisplayFormat.setTimeZone(TimeZone.getTimeZone(istTimeZone))
    DateDisplayFormat.format(date)
  }
  /*
  def getISTTimeZoneDate(): Calendar = {
    val now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    now.setTimeZone(TimeZone.getTimeZone(istTimeZone));
    now
  }*/

  def getISDTime(date: java.util.Date): String = {
    val TimeFormat = new SimpleDateFormat("hh:mm a")
    TimeFormat.setTimeZone(TimeZone.getTimeZone(istTimeZone))
    TimeFormat.format(date)
  }

  def getISDDate(): java.sql.Date = {
    new java.sql.Date(getISDTimeZoneDate().getTimeInMillis())
  }

  def getISDDate(count: Int): java.sql.Date = {
    val cal: Calendar = getISDTimeZoneDate();
    cal.add(Calendar.DATE, count);
    new java.sql.Date(cal.getTimeInMillis())
  }

  def getCalenderInstance(requestDate: java.sql.Date): Calendar = {

    val cal: Calendar = CalenderDateConvertor.getISDTimeZoneDate()
    cal.setTime(requestDate)

    return cal

  }

  def getISDTimeStamp(): java.sql.Timestamp = {
    new java.sql.Timestamp(getISDDate().getTime())
  }

  def smsDateFormat(sqlDate: java.sql.Date): String = {
    return SMSDateFormat.format(sqlDate);
  }

  def ReminderSMSDateFormat(sqlDate: java.sql.Date): String = {
    return SMSDateFormatDDMMYYYY.format(sqlDate);
  }

  def getISOTimeFormat(HHMMSS: String) = {
    try {
      val formatter: SimpleDateFormat = new SimpleDateFormat("HH:mm:ss")
      val date: java.util.Date = formatter.parse("HHMMSS")
      val cal: Calendar = Calendar.getInstance()

      val calendar: Calendar = getISDTimeZoneDate()
      calendar.setTime(date); // assigns calendar to given date

      cal.setTime(date);

    } catch {
      case e: Exception =>
        e.printStackTrace()

    }

  }

  def getDayofTheWeek(cal: java.util.Calendar): String = {

    val i: Int = cal.get(Calendar.DAY_OF_WEEK);

    var dayOfTheWeek = ""

    i match {
      case 1 => dayOfTheWeek = "SUN";
      case 2 => dayOfTheWeek = "MON";
      case 3 => dayOfTheWeek = "TUE";
      case 4 => dayOfTheWeek = "WED";
      case 5 => dayOfTheWeek = "THU";
      case 6 => dayOfTheWeek = "FRI";
      case 7 => dayOfTheWeek = "SAT";
    }

    return dayOfTheWeek

  }

  def formatHHMMSS(cal: java.util.Calendar): String = {
    return formats(cal.get(Calendar.HOUR_OF_DAY).toString()) + ":" + formats(cal.get(Calendar.MINUTE).toString()) + ":" + formats(cal.get(Calendar.SECOND).toString())
  }

  def formatHHMM(cal: java.util.Calendar): String = {
    return cal.get(Calendar.HOUR_OF_DAY) + ":" + formats(cal.get(Calendar.MINUTE).toString)
  }

  def formats(timeformat: String): String = {

    if (timeformat.length() != 2) {
      return "0" + timeformat
    } else {
      return timeformat
    }

  }

  def setDateTime(requestedDate: java.sql.Date, requestedTime: String): Calendar = {

    val calendar: Calendar = getISDTimeZoneDate() // creates calendar
    calendar.setTime(requestedDate); // sets calendar time/date

    val time = hcueHelperClass.setTimeFormats(requestedTime).split(":")

    calendar.set(Calendar.HOUR_OF_DAY, time(0).toInt)
    calendar.set(Calendar.MINUTE, time(1).toInt)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0);

    calendar

  }

  import java.util.Date
  def hoursDifference(date1: Calendar, date2: Calendar): Long = {

    val returnValue = (date2.getTimeInMillis() - date1.getTimeInMillis());
    //val returnValue:Long = ((date1.getTime()/60000) - (date1.getTime()/60000))
    return (returnValue / 60000)
  }

  def incrementTime(cal: Calendar, duration: Int): Calendar = {
    cal.add(Calendar.MINUTE, duration)
    return cal
  }

  def areEqualDays(c1: Calendar, c2: Calendar): Boolean = {
    return (DDMMYYYY.format(c1.getTime()).equals(DDMMYYYY.format(c2.getTime())));
  }

  val EEEEDDthMMMYYY: SimpleDateFormat = new SimpleDateFormat("EEEEE, dd'th' MMMM yyyy");

  def getDateFormatter(fdate: java.util.Date): SimpleDateFormat = {

    var dayFormat: SimpleDateFormat = new SimpleDateFormat("dd");
    val date = dayFormat.format(fdate);

    if (date.endsWith("1") && !date.endsWith("11")) {
      dayFormat = new SimpleDateFormat("EEEEE, d'st' MMM, yyyy")
    } else if (date.endsWith("2") && !date.endsWith("12")) {
      dayFormat = new SimpleDateFormat("EEEEE, d'nd' MMM, yyyy")
    } else if (date.endsWith("3") && !date.endsWith("13")) {
      dayFormat = new SimpleDateFormat("EEEEE, d'rd' MMM, yyyy")
    } else {
      dayFormat = new SimpleDateFormat("EEEEE, d'th' MMM, yyyy")
    }
    dayFormat
  }

}