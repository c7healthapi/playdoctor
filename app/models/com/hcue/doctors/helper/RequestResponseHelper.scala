package models.com.hcue.doctors.helper

import java.text.DecimalFormat
import java.util.Calendar
import java.util.Date
import java.util.HashMap

import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer

import models.com.hcue.doctors.Json.Response.currentAgeObj

object RequestResponseHelper {

  def toCamelCase(init: String): String = {

    if (init.isEmpty())
      return ""

    val ret: StringBuilder = new StringBuilder(init.length());

    val word = init.split(" ")

    for (i <- 0 until word.length) {
      if (!word(i).isEmpty()) {
        ret.append(word(i).substring(0, 1).toUpperCase());
        ret.append(word(i).substring(1).toLowerCase());
      }
      if (!(ret.length() == init.length()))
        ret.append(" ");
    }

    return ret.toString();
  }

  def CombineHashMap(inPutHistory: Option[Map[String, String]], SourceHistory: Option[Map[String, String]]): Map[String, String] = {

    val returnMap = new HashMap[String, String]()

    SourceHistory.foreach { x =>
      val value: Map[String, String] = x
      value.iterator.foreach { Y =>
        returnMap.put(Y._1, Y._2)
      }
    }

    inPutHistory.foreach { x =>
      val value: Map[String, String] = x
      value.iterator.foreach { Y =>
        returnMap.put(Y._1, Y._2)
      }
    }

    return returnMap.toMap

  }

  def ValidateHashMap(SourceHistory: Option[Map[String, String]]): Option[Map[String, String]] = {

    if (SourceHistory != None && SourceHistory.get.size == 0) {
      return None
    } else {
      val value = CombineHashMap(None, SourceHistory)

      if (value.size == 0) {
        return None
      } else {
        return Some(value)
      }
    }
  }

  def setOrderedDayCode(load: Boolean) = {

    if (load) {
      val list = new ListBuffer[String]()

      for (i <- 0 until 7) {
        val date: java.sql.Date = CalenderDateConvertor.getISDDate(i)
        val dayCode = CalenderDateConvertor.getDayofTheWeek(CalenderDateConvertor.getCalenderInstance(date))
        list += dayCode
      }

    /*  "ListBuffer[String]()" == list.toString()
    */}

  }

  def setAgeFormat(age: Option[Double]): Option[Double] = {

    if (age == None)
      return None

    return Some(new DecimalFormat("###.##").format(age.get).toDouble)

  }

  def ageCalculation(sqlDate: Option[java.sql.Date]): Option[currentAgeObj] = {

    if (sqlDate == None)
      return None

    val birthDate: Date = new java.util.Date(sqlDate.get.getTime()) //sdf.parse(inPutDate.get)
    var years: Int = 0
    var months: Int = 0
    var days: Int = 0

    //create calendar object for birth day
    val birthDay: Calendar = Calendar.getInstance();
    birthDay.setTimeInMillis(birthDate.getTime());
    //create calendar object for current day

    var currentTime: Long = System.currentTimeMillis();
    val now: Calendar = Calendar.getInstance();
    now.setTimeInMillis(currentTime);
    //Get difference between years
    years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
    val currMonth: Int = now.get(Calendar.MONTH) + 1;
    val birthMonth: Int = birthDay.get(Calendar.MONTH) + 1;
    //Get difference between months
    months = currMonth - birthMonth;
    //if month difference is in negative then reduce years by one and calculate the number of months.
    if (months < 0) {
      years = years - 1;
      months = 12 - birthMonth + currMonth;
      if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
        months = months - 1;
    } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
      years = years - 1;
      months = 11;
    }
    //Calculate the days
    if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
      days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
    else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
      val today: Int = now.get(Calendar.DAY_OF_MONTH);
      now.add(Calendar.MONTH, -1);
      days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
    } else {
      days = 0;
      if (months == 12) {
        years = years + 1;
        months = 0;
      }
    }
    //Create new Age object
    return Some(new currentAgeObj(years, months, days))

  }

  def properTime(time: String): String = {

    var properTime = time
    val arrTime = time.split(":")
    if (arrTime(0).length() == 1) {
      properTime = "0" + time
    }
    if (arrTime(1).length() == 1) {
      properTime = properTime.split(":")(0) + ":0" + properTime.split(":")(1)
    }
    properTime
  }

  def validateString(str: Option[String]): Boolean = {
    if (str != None && !str.get.equals("")) {
      true
    } else {
      false
    }
  }
}