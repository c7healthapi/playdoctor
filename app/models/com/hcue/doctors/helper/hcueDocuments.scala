package models.com.hcue.doctors.helper

import java.awt.image.BufferedImage
import java.io.BufferedWriter
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileWriter

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.xhtmlrenderer.pdf.ITextRenderer

import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.util.IOUtils
import play.Logger
import play.api.Play.current
import com.hcue.pdfgen.PDFGenerator
import com.hcue.pdfgen.entities.PDFWrapper
import com.hcue.pdfgen.PDFGeneratorv1

object hcueDocuments {

  private val log = Logger.of("application")

  private final val DWS_RENDER_URL = "https://dws2.docmosis.com/services/rs/render";
  //private final val patientCase_bucketName = awsCredentials.PATIENT_CASE_BUCKET
  val s3AccessKey = current.configuration.getString("aws.s3.accessKey").get
  val s3SecretKey = current.configuration.getString("aws.s3.secretKey").get
  private final val yourAWSCredentials = new com.amazonaws.auth.BasicAWSCredentials(s3AccessKey, s3SecretKey)
  private final val amazonS3Client = new com.amazonaws.services.s3.AmazonS3Client(yourAWSCredentials)

  /*val AWS_HCUE_DOMAIN_PROTOCOL = hcueCookieHost.CLOUD_FRONT_URL_PROTOCOL
  val AWS_PAT_PATIENTCASEIMAGE_CLOUD_FRONT_DOMAIN = patientcaseCloudFront.HCUE_PAT_PATIENTCASEIMAGE_CLOUD_FRONT_DOMAIN
  val AWS_CLOUD_FRONT_PEM_CERT = hcueCookieHost.CLOUD_FRONT_PEM_CERT*/

  /*
  def createPFDDocument(html: String, fileName: String): String = {
    // Set your access Key\\
    var outputFile: File = null
    var fos: FileOutputStream = null
    var path: String = null
    try {

      val client = new Client(hcueEmailCredentials.HCUE_EMAIL_CREDENTIAL_USERNAME, hcueEmailCredentials.HCUE_EMAIL_CREDENTIAL_PASSWORD, hcueEmailCredentials.HCUE_EMAIL_CREDENTIAL_WEBSITE);

      outputFile = new File(fileName);
      fos = new FileOutputStream(outputFile);

      client.convertHtml(html, fos);
      path = outputFile.getAbsolutePath
      log.info("File Generated Succesfully:" + client.numTokens())

    } catch {
      case e: Exception =>
        e.printStackTrace()
        path = null
    } finally {
      if (fos != null) {
        fos.close();
      }

    }
    return path
  }
  *
  */
  /*Added for PDF Generation using Itext Library - Starts*/
  //f = new File("CaseBillReceipt" + filename + ".html");

  def CreateLocalHTML(html: String, filename: String): String =
    {
      var f: File = null
      var biw: BufferedWriter = null;
      var path: String = null
      //println("Temp path::"+System.getProperty("temp_folder"))
      //f = new File(System.getProperty("temp_folder")+"/"+filename);
      println("temp::" + current.configuration.getString("temp_folder"))

      f = new File(current.configuration.getString("temp_folder").get + "/" + filename);
      try {
        biw = new BufferedWriter(new FileWriter(f));
        biw.write(html);
        biw.close();
      } catch {
        case e: Exception =>
          e.printStackTrace()
      }
      return f.getAbsolutePath
    }

  def createPFDItext(creatlocalhtml: String, fileName: String): String = {
    var fos: FileOutputStream = null
    var outputFile: File = null
    var path: String = null
    try {
      var fos: FileOutputStream = null
      val File_To_Convert = creatlocalhtml;
      val url = new File(File_To_Convert).toURI().toURL().toString();
      //println("Temp path::"+System.getProperty("temp_folder"))
      //outputFile = new File(System.getProperty("temp_folder")+"/"+fileName);
      outputFile = new File(current.configuration.getString("temp_folder").get + "/" + fileName);
      fos = new FileOutputStream(outputFile);
      val renderer = new ITextRenderer();
      renderer.setDocument(url);
      renderer.layout();
      renderer.createPDF(fos);
    } catch {
      case e: Exception =>
        e.printStackTrace()
        path = null
    } finally {
      if (fos != null) {
        fos.close();
      }
    }
    return outputFile.getAbsolutePath
  }

  def createPFDITextWrapper(fileName: String, pdfWrapper: PDFWrapper): String = {
    var fos: FileOutputStream = null
    var outputFile: File = null
    var path: String = null
    try {
      var fos: FileOutputStream = null
      outputFile = new File(current.configuration.getString("temp_folder").get + "/" + fileName);
      fos = new FileOutputStream(outputFile);
      val pdfgenerator = new PDFGenerator()
      pdfgenerator.doGeneratePDF(fos, pdfWrapper)
    } catch {
      case e: Exception =>
        e.printStackTrace()
        path = null
    } finally {
      if (fos != null) {
        fos.close();
      }
    }
    return outputFile.getAbsolutePath
  }
  
  def createPFDITextWrapperv1(fileName: String, pdfWrapper: PDFWrapper): String = {
    var fos: FileOutputStream = null
    var outputFile: File = null
    var path: String = null
    try {
      var fos: FileOutputStream = null
      outputFile = new File(current.configuration.getString("temp_folder").get + "/" + fileName);
      fos = new FileOutputStream(outputFile);
      val pdfgenerator = new PDFGeneratorv1()
      pdfgenerator.doGeneratePDF(fos, pdfWrapper)
    } catch {
      case e: Exception =>
        e.printStackTrace()
        path = null
    } finally {
      if (fos != null) {
        fos.close();
      }
    }
    return outputFile.getAbsolutePath
  }
  
  /*Added for PDF Generation using Itext Library - End*/
  def createJPGDocumentJPG(html: String, sourcePath: String, fileName: String): String = {

    var page = 0;
    var outputFile: File = null
    var document: PDDocument = null
    var bim: BufferedImage = null;
    var pdPage: PDPage = null;
    var fos: FileOutputStream = null;
    var path: String = null
    try {

      document = org.apache.pdfbox.pdmodel.PDDocument.loadNonSeq(new File(sourcePath), null);
      if (document != null) {
        var pdPages = document.getDocumentCatalog().getAllPages();

        import gui.ava.html.image.generator.HtmlImageGenerator

        val imageGenerator: HtmlImageGenerator = new HtmlImageGenerator()
        imageGenerator.loadHtml(html);

        imageGenerator.saveAsImage(fileName)
        //Add Wait State 30 sec
        outputFile = new File(fileName);
        //Add Wait State 30 sec
        path = outputFile.getAbsolutePath

        path = outputFile.getAbsolutePath
        log.info("Preview Image Generated at specified location")

      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        outputFile = null
    } finally {
      if (document != null) {
        try {
          document.close();
        } catch {
          case e: Exception =>
            e.printStackTrace()

        }
      }
      if (fos != null) {
        fos.close();
      }
      if (bim != null) {
        bim = null
      }
      if (pdPage != null) {
        pdPage = null
      }
      log.info("All Closed")
    }

    return path
  }

  def S3UpdateHelperService(BucketName: String, localFilePath: String, amazonFilePath: String): (String, String) = {

    var input: FileInputStream = null
    try {
      log.info("Inside the PatientCaseImageDao: updateCaseReceiptHelperService ")
      log.info("filePath :  " + localFilePath)

      input = new FileInputStream(new File(localFilePath));
      //val caseBucket = amazonS3Client.createBucket(BucketName)
      val contentBytes = IOUtils.toByteArray(input)
      val metadata = new ObjectMetadata()
      metadata.setContentLength(contentBytes.length)
      metadata.setContentType("application/pdf")
      metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION)
      if (input != null) {
        log.info("First Closing Stream");
        input.close()
        log.info("First Closed Stream");
      }
      input = new FileInputStream(new File(localFilePath));
      println("input::" + input)
      amazonS3Client.setRegion(Region.getRegion(Regions.EU_WEST_2))
      println("Region::" + amazonS3Client.getRegion)
      val amazonResponse = amazonS3Client.putObject(BucketName, amazonFilePath, input, metadata);
      //val s3Host= "https://s3.eu-west-2.amazonaws.com"
      
       val diagnosticsCloudFrontUrl = current.configuration.getString("url.cloudfront.diagnostics").get

      val S3URL = diagnosticsCloudFrontUrl + "/" + amazonFilePath

      //val amazonResponse= amazonS3Client.putObject(caseBucket.getName, patientID.toString()+"/"+patientCaseID.toString+"/CaseReceipt/CaseReceipt."+FileExtn, input, metadata);
      log.info("Uploaded object encryption status is " + amazonResponse.getSSEAlgorithm());
      ("Success", S3URL.toString())
    } catch {
      case e: Exception =>
        e.printStackTrace()
        ("Failure", "Did not Upload")

    } finally {
      if (input != null) {
        log.info("Second Closing Stream");
        input.close()
        log.info("Second Closed Stream");
      }

    }
  }
  
  
  def S3UploadHelperService(BucketName: String, localFilePath: String, amazonFilePath: String): (String, String) = {

    var input: FileInputStream = null
    try {
      log.info("Inside the PatientCaseImageDao: updateCaseReceiptHelperService ")
      log.info("filePath :  " + localFilePath)

      input = new FileInputStream(new File(localFilePath));
      //val caseBucket = amazonS3Client.createBucket(BucketName)
      val contentBytes = IOUtils.toByteArray(input)
      val metadata = new ObjectMetadata()
      metadata.setContentLength(contentBytes.length)
      metadata.setContentType("application/xlsx")
      metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION)
      if (input != null) {
        log.info("First Closing Stream");
        input.close()
        log.info("First Closed Stream");
      }
      input = new FileInputStream(new File(localFilePath));
      println("input::" + input)
      amazonS3Client.setRegion(Region.getRegion(Regions.EU_WEST_2))
      println("Region::" + amazonS3Client.getRegion)
      val amazonResponse = amazonS3Client.putObject(BucketName, amazonFilePath, input, metadata);
      //val s3Host= "https://s3.eu-west-2.amazonaws.com"
      
      val analyticsCloudFrontUrl = current.configuration.getString("url.cloudfront.analyticsreport").get

      val S3URL =  analyticsCloudFrontUrl+ "/" + amazonFilePath

      //val amazonResponse= amazonS3Client.putObject(caseBucket.getName, patientID.toString()+"/"+patientCaseID.toString+"/CaseReceipt/CaseReceipt."+FileExtn, input, metadata);
      log.info("Uploaded object encryption status is " + amazonResponse.getSSEAlgorithm());
      ("Success", S3URL.toString())
    } catch {
      case e: Exception =>
        e.printStackTrace()
        ("Failure", "Did not Upload")

    } finally {
      if (input != null) {
        log.info("Second Closing Stream");
        input.close()
        log.info("Second Closed Stream");
      }

    }
  }


  /*
  def S3DownloadHelperService(BucketName: String, FilePath: String): Option[URL] = {
    val imageStr: Option[String] = null

    try {
      val expiration = new java.util.Date();
      var milliSeconds = expiration.getTime();
      milliSeconds += 1000 * 60 * 60; // Add 1 hour.
      expiration.setTime(milliSeconds);
      val url = amazonS3Client.generatePresignedUrl(BucketName, FilePath, expiration);
      return Some(url)
    } catch {
      case e: Exception =>
        log.error("No Image found for PatientID  - Download Case Receipt:" + BucketName + "/" + FilePath)
        //e.printStackTrace()
        return None

    }
    return None
  }*/
}