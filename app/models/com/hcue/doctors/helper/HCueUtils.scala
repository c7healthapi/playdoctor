package models.com.hcue.doctors.helper

import java.util.HashMap
import scala.collection.JavaConversions._
import util.Try

object HCueUtils {

  final val StrConstSuccess: String = "SUCCESS"

  final val StrConstFailure: String = "FAILURE"

  final val StrConstInvalidEmail: String = "INVALID_EMAIL_ID"

  final val StrConstEmailAlreadyExist: String = "ALREADY_EMAIL_EXISTS"

  final val StrConstInvalidDoctorID: String = "INVALID_DOCTOR_ID"

  final val StrConstNoDoctorID: String = "NO_SUCH_DOCTOR_ID_EXIST"

  final val StrConstInvalidDoctorLoginID: String = "INVLID_DOCTOR_LOGIN_ID"

  final val StrConstIDAlreadyExist: String = "ALREADY_LOGINID_EXISTS"

  final val StrConstIDLoginIDUpd: String = "DOCTOR_LOGINID_UPDATED_SUCCESSFULLY"

  final val StrConstEmailHippoLimit: String = "EMAIL_HIPPO_LIMIT_EXCCEDED"

  final val StrConstLicLimitExceed: String = "LICENSE_LIMIT_EXCEEDED"

  final val StrConstDoctorLimitExceed: String = "DOCTOR_LIMIT_EXCEEDED"

  final val StrConstRecLimitExceed: String = "RECEPTIONIST_LIMIT_EXCEEDED"

  final val strSetDefaultCurrency: String = "Y"

  final val SMSThresholdLimit: Int = 1000

  //AUTHENTICATION CONSTATNTS - ADDED BY KRISHNAN

  final val strAccountInactive: String = "ACCOUNT INACTIVATED"

  final val strPasswordExpired: String = "YOUR PASSWORD IS EXPIRED, PLEASE CHANGE"

  final val strTrialExpired: String = "TRIAL PERIOD IS EXPIRED"

  final val strValidCredential: String = "VALID_CREDENTIAL"

  final val strInValidPassword: String = "IN_VALID_PASSWORD"

  final val strInValidUserName: String = "INVALID_USER_NAME"

  final val lstAppointmentStatus: List[String] = List("B", "E", "S", "I", "P")

  final val lstAppointmentStatusWithCancel: List[String] = List("B", "C", "E", "S", "I", "P")

  final val lstCalendarAppointmentStatus: List[String] = List("B", "E", "S", "L", "I", "P", "EVENT", "IMPEVENT")

  final val lstCalendarEventStatus: List[String] = List("EVENT", "IMPEVENT")

  // Add more constants here
  // Please double check before modifying constants

  def validateEmail(email: String): Boolean = {
    val EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"
    email.matches(EMAIL_REGEX)
  }

  def validateString(str: Option[String]): Boolean = {
    if (str != None && !str.get.equals("") && !str.get.trim().equals("")) {
      true
    } else {
      false
    }
  }

  def toInt(o: Option[String]): Option[Int] =
    o.flatMap(s => Try(s.toInt).toOption)

  def CombineHashMap(inPutHistory: Option[Map[String, String]], SourceHistory: Option[Map[String, String]]): Map[String, String] = {

    val returnMap = new HashMap[String, String]()

    SourceHistory.foreach { x =>
      val value: Map[String, String] = x
      value.iterator.foreach { Y =>
        if (!Y._2.equals(""))
          returnMap.put(Y._1, Y._2)
      }
    }

    inPutHistory.foreach { x =>
      val value: Map[String, String] = x
      value.iterator.foreach { Y =>
        if (!Y._2.equals(""))
          returnMap.put(Y._1, Y._2)
      }
    }

    return returnMap.toMap

  }

  def ValidateHashMap(SourceHistory: Option[Map[String, String]]): Option[Map[String, String]] = {

    if (SourceHistory != None && SourceHistory.get.size == 0) {
      return None
    } else {
      val value = CombineHashMap(None, SourceHistory)

      if (value.size == 0) {
        return None
      } else {
        return Some(value)
      }
    }
  }

  /*
   * This function is used to return the first value of type Map[String,String]
   */
  import scala.util.control.Breaks

  def getFirstElement(SourceValue: Option[Map[String, String]]): Option[String] = {

    var outPut: Option[String] = None

    if (SourceValue == None) {
      return outPut
    }

    if (SourceValue != None && SourceValue.get.size == 0) {
      return outPut
    }

    val loop = new Breaks;
    loop.breakable {
      SourceValue.get.iterator.foreach { f =>
        outPut = Some(f._2)
        loop.break()
      }
    }

    return outPut

  }

  def stringconcatinate(a: String): String = {
    var final_ConcatString: String = ""
    final_ConcatString = a.trim();
    final_ConcatString = final_ConcatString.replace('.', '-');
    final_ConcatString = final_ConcatString.replace(' ', '-')
    final_ConcatString = final_ConcatString.replace("---", "-")
    final_ConcatString = final_ConcatString.replace("--", "-").toLowerCase();
    return final_ConcatString
  }

}