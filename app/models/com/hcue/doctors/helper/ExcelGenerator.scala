package models.com.hcue.doctors.helper

import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.HashMap

import scala.collection.mutable.ListBuffer
import scala.slick.driver.PostgresDriver.simple.columnExtensionMethods
import scala.slick.driver.PostgresDriver.simple.longColumnType
import scala.slick.driver.PostgresDriver.simple.queryToAppliedQueryInvoker
import scala.slick.driver.PostgresDriver.simple.valueToConstColumn
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.StaticQuery.staticQueryToInvoker

import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.util.IOUtils

import models.com.hcue.doctors.slick.transactTable.hospitals.Hospital
import models.com.hcue.doctors.traits.dbProperties.db
import play.Logger
import play.api.mvc.Controller
import play.api.Play.current

object ExcelGenerator extends Controller {
  val s3AccessKey = current.configuration.getString("aws.s3.accessKey").get
  val s3SecretKey = current.configuration.getString("aws.s3.secretKey").get
  private val log = Logger.of("application")

  val hospital = Hospital.Hospital

  def getmisrecordQuery(startDate: java.sql.Date, endDate: java.sql.Date, hospitalid: Long) =
    sql"SELECT * FROM getmisreports('#$startDate','#$endDate','#$hospitalid')".
      as[(Long, String, Long, Long, Long, Long, Long, Long, Long, Long, Long, String)]

  def genExcelval(hospitalID: Long, StartDate: java.sql.Date, EndDate: java.sql.Date, ReportID: Long, HospitalCD: String): String = db withSession { implicit session =>

    //var out: OutputStream = new ByteOutputStream();

    var file: File = new File("workbook.xls")
    var out: FileOutputStream = new FileOutputStream(file);

    var reportID = ReportID

    val endDate = EndDate

    val startDate = StartDate

    val MisReportsLst = getmisrecordQuery(startDate, endDate, hospitalID).list

    val hospitalcode = hospital.filter { x => x.HospitalID === hospitalID }.map { x => x.HospitalCode }.firstOption

    //val wb: HSSFWorkbook = HSSFWorkbook.create(org.apache.poi.hssf.model.InternalWorkbook.createWorkbook())

    val wb: HSSFWorkbook = new HSSFWorkbook()

    val sheet: HSSFSheet = wb.createSheet("Sheet1")

    //sheet.createFreezePane(0, 100)

    var columnheads: List[String] = List("S No", "Clinic Name", "New Registration", "Re Visits", " Male ", "Female", "0-5 Years", "6-18 Years", "19-40 Years", "41-60 Years", "61+ Years")

    var headelistbuffer = new ListBuffer[String]()

    for (b <- 0 until columnheads.length) {
      headelistbuffer += columnheads(b).toString()
    }

    columnheads = headelistbuffer.toList

    val columnvaluesList = new java.util.ArrayList[List[String]](MisReportsLst.length)

    columnvaluesList.add(0, columnheads)

    for (i <- 0 until MisReportsLst.length) {
      //This if block is introduced because of first return 50 records and second retrun 51 records
      if (MisReportsLst(i) != null) {

        val diagnosisvalue = MisReportsLst(i)._12.split("=")

        var listbuffer = new ListBuffer[String]()

        listbuffer += (i + 1) + ""
        listbuffer += MisReportsLst(i)._2.toString()
        listbuffer += MisReportsLst(i)._3.toString()
        listbuffer += MisReportsLst(i)._4.toString()
        listbuffer += MisReportsLst(i)._5.toString()
        listbuffer += MisReportsLst(i)._6.toString()
        listbuffer += MisReportsLst(i)._7.toString()
        listbuffer += MisReportsLst(i)._8.toString()
        listbuffer += MisReportsLst(i)._9.toString()
        listbuffer += MisReportsLst(i)._10.toString()
        listbuffer += MisReportsLst(i)._11.toString()

        val descriptioncountmap = new HashMap[String, String]()
        if (diagnosisvalue != null && diagnosisvalue.toString().trim() != "") {
          for (j <- 0 until diagnosisvalue.length) {

            var descriptioncount = diagnosisvalue(j).split("~")
            if (descriptioncount != null && descriptioncount(0).trim() != "") {
              var description = descriptioncount(0)
              var count = descriptioncount(1)

              descriptioncountmap.put(description, count)
            }
          }

        }

        columnvaluesList.add(i + 1, listbuffer.toList)

      }
    }

    var rowscount = columnvaluesList.size()

    for (j <- 0 until rowscount) {
      val row = sheet.createRow(j)

      val columnvalues = columnvaluesList.get(j)

      for (k <- 0 until columnheads.length) {
        sheet.autoSizeColumn(k)
        var cell = row.createCell(k).setCellValue(columnvalues(k))

      }
    }

    wb.write(out)
    //var generatedStatus = genrateExcelFile(out,reportID,HospitalCD)
    var generatedStatus = writeFiletoS3(file, reportID, HospitalCD)
    //var generatedStatus = "SUCCESS"
    out.close();
    wb.close();

    file.delete();

    //writer.close()
    //out.close();
    return generatedStatus
  }

  //import models.com.hcue.doctors.traits.hcueMisReports

  def writeFiletoS3(file: File, reportID: Long, HospitalCD: String): String = {
  
    val yourAWSCredentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey)
    val amazonS3Client = new AmazonS3Client(yourAWSCredentials)
    //val profileBucket = amazonS3Client.createBucket(hcueMisReports.HCUE_MIS_REPORT_BUCKET)
    try {

      amazonS3Client.setEndpoint("https://s3.amazonaws.com")
      val filename: String = HospitalCD + "/MIS-" + reportID + ".xls"
      /* val amazonResponse = amazonS3Client.putObject(new PutObjectRequest(
        profileBucket.getName, filename, file));*/

      val amazonResponse = amazonS3Client.putObject(new PutObjectRequest(
        "", filename, file));

      return "SUCCESS"
    } catch {
      case e: Exception =>
        e.printStackTrace()
        return "FAILURE"
    }
  }

  def genrateExcelFile(input: OutputStream, reportID: Long, HospitalCD: String): String = {

    val yourAWSCredentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey)
    val amazonS3Client = new AmazonS3Client(yourAWSCredentials)
    val profileBucket = amazonS3Client.createBucket("hcuedocs1")
    try {
      var inputstr = new ByteArrayInputStream(input.toString().getBytes());
      amazonS3Client.setEndpoint("https://s3.amazonaws.com")

      val contentBytes = IOUtils.toByteArray(inputstr);
      val metadata = new ObjectMetadata();
      metadata.setContentLength(contentBytes.length)
      metadata.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION)

      if (inputstr != null) {
        inputstr.close()
      }

      var inputstream = new ByteArrayInputStream(input.toString().getBytes())

      val filename: String = HospitalCD + "/MIS-" + reportID + ".xls"
      val amazonResponse = amazonS3Client.putObject(profileBucket.getName(), filename, inputstream, metadata);
      return "SUCCESS"
    } catch {
      case e: Exception =>
        e.printStackTrace()
        return "FAILURE"
    } finally {
      if (input != null)
        input.close()
    }
  }
}