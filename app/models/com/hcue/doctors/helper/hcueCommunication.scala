package models.com.hcue.doctors.helper

import java.io.File
import java.io.InputStreamReader
import java.io.StringWriter
import java.net.URL
import java.util.Properties

import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
//import models.com.hcue.doctors.traits.hcueHospitalCode
import com.amazonaws.services.s3.model.GetObjectRequest
//import models.com.hcue.doctors.traits.add.EmailHippoNotificationDao
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient
import com.amazonaws.services.simpleemail.model.Body
import com.amazonaws.services.simpleemail.model.Content
import com.amazonaws.services.simpleemail.model.Destination
import com.amazonaws.services.simpleemail.model.SendEmailRequest

import javax.activation.DataHandler
import javax.activation.FileDataSource
import javax.activation.URLDataSource
import javax.mail.Address
import javax.mail.MessagingException
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import models.com.hcue.doctors.traits.hcueEmailCredentials

import play.Logger
import play.api.Play.current

object hcueCommunication {

  val log = Logger.of("application")

  //AWS
  val s3AccessKey = current.configuration.getString("aws.s3.accessKey").get
  val s3SecretKey = current.configuration.getString("aws.s3.secretKey").get
  private final val yourAWSCredentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey)
  private final val amazonS3Client = new AmazonS3Client(yourAWSCredentials)

  //CC Email 
  val CCEmail = current.configuration.getString("email.cc").get
  
  //SMTP Session
  val smtpFrom = current.configuration.getString("smtp.from").get
  val smtpUsername = current.configuration.getString("smtp.user").get
  val smtpPassword = current.configuration.getString("smtp.pwd").get
  val smtpHost = current.configuration.getString("smtp.host").get

  val smtpProps = new Properties()
  smtpProps.put("mail.smtp.auth", "true")
  smtpProps.put("mail.smtp.starttls.enable", "true")
  smtpProps.put("mail.smtp.host", smtpHost)
  val smtpPort = current.configuration.getString("smtp.port").get
  smtpProps.put("mail.smtp.port", smtpPort)

  val smtpSession = Session.getInstance(smtpProps, new javax.mail.Authenticator() {
    override def getPasswordAuthentication(): PasswordAuthentication = {
      return new PasswordAuthentication(smtpUsername, smtpPassword)
    }
  })

  //Gmail Session
  val gmailFrom = current.configuration.getString("gmail.from").get
  val gmailuser = current.configuration.getString("gmail.usr").get
  val gmailpwd = current.configuration.getString("gmail.pwd").get
  val gmailhost = current.configuration.getString("gmail.host").get
  val gmailPort = current.configuration.getString("gmail.port").get
  
  //sendEmail(to, cc,from ,username,password,host, port ,mailSubject,html)

  val gmailProps = new Properties()

  gmailProps.put("mail.smtp.user", gmailFrom);
  gmailProps.put("mail.transport.protocol", "smtp");
  gmailProps.put("mail.smtp.auth", "true")
  gmailProps.put("mail.smtp.starttls.enable", "true")
  gmailProps.put("mail.smtp.host", gmailhost)
  gmailProps.put("mail.smtp.port", gmailPort)
  
/*gmailProps.put("mail.smtp.starttls.enable", "true")
  gmailProps.put("mail.smtp.EnableSSL.enable", "true");
  gmailProps.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
  gmailProps.setProperty("mail.smtp.socketFactory.fallbac k", "false");
  gmailProps.setProperty("mail.smtp.socketFactory.port", gmailPort);*/

  val gmailSession = Session.getInstance(gmailProps, new javax.mail.Authenticator() {

    override def getPasswordAuthentication(): PasswordAuthentication = {
      return new PasswordAuthentication(gmailuser, gmailpwd)
    }
  })

  def readHTMLTempalteURL(context: VelocityContext, vmFileURL: String): String = {

    val sw = new StringWriter

    val tempfolder = current.configuration.getString("temp_folder").get

    try {
      val ve = new VelocityEngine();

      ve.setProperty("file.resource.loader.path", ".");
      ve.setProperty("runtime.log", current.configuration.getString("temp_folder").get + "/velocity.log");
      ve.init();

      val errorhost = current.configuration.getString("notify.error.host").get
      val url: URL = new URL(errorhost + vmFileURL);

      val t = ve.evaluate(context, sw, null, new InputStreamReader(url.openStream()))

    } catch {
      case e: Exception =>
        e.printStackTrace()
    } finally {
      val velocityLogPath = current.configuration.getString("temp_folder").get + "/velocity.log"
      val logFile = new File(velocityLogPath)
      if (logFile.exists()) {
        logFile.delete()
      }
    }
    return sw.toString()
  }

  def getHcueHTMLTempalte(context: VelocityContext, bucketName: String, templateFullPath: String): String = {

    val sw = new StringWriter

    try {

      //val bucket = amazonS3Client.createBucket(bucketName)

      //Get the Image from S3
      val s3Object = amazonS3Client.getObject(new GetObjectRequest(bucketName, templateFullPath));

      val ve = new VelocityEngine();
      ve.setProperty("file.resource.loader.path", ".");
      ve.setProperty("runtime.log", current.configuration.getString("temp_folder").get + "/velocity.log"); //Change the log folder in app.conf whn running app in local
      ve.init();

      //Factory and an engine Instance
      val t = ve.evaluate(context, sw, null, new InputStreamReader(s3Object.getObjectContent()))

    } catch {
      case e: Exception =>
        e.printStackTrace()
    } finally {
      val velocityLogPath = current.configuration.getString("temp_folder").get + "/velocity.log"
      val logFile = new File(velocityLogPath)
      if (logFile.exists()) {
        logFile.delete()
      }
    }

    return sw.toString

  }
  /**
   * This method is to fetch Template files from S3 bucket
   */
  def getHcueEmailHTMLTempalte(bucketName: String, templateFullPath: String): S3Object = {
    var s3Object = new S3Object
    try {
      val bucket = amazonS3Client.createBucket(bucketName)
      //Get the Image from S3
      s3Object = amazonS3Client.getObject(bucket.getName, templateFullPath);
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    return s3Object

  }

  /**
   * This method replaces the context values in VM file
   */
  def getTemplateString(template: S3Object, context: VelocityContext): String = {
    val sw = new StringWriter
    try {
      val ve = new VelocityEngine();
      ve.setProperty("file.resource.loader.path", ".");
      ve.init();
      //Factory and an engine Instance
      val t = ve.evaluate(context, sw, null, new InputStreamReader(template.getObjectContent()))

    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
    return sw.toString()

  }

  def sendHcueEmail(html: String, toEmailID: String, ccEmailID: Option[String], bccEmailID: Option[String], mailSubject: String, emailStatus: String, isEmailEnabled: String, fileName: Option[String]): String = {
    if (!hcueEmailCredentials.SEND_EMAIL) {
      println("BLOCKED")
      return "Email Blocked to Send"
    }

    if (emailStatus.equals("F")) {
      println("FAILS")
      return "InValid Email"
    }

    /*    if (isEmailEnabled.equals("N")) {
      return "Email Disabled"
    }*/

    if (emailStatus.equals("N") || emailStatus.equals("S")) {
      //EmailHippoNotificationDao.addEmails(toEmailID, emailStatus)
      println("RIGHT")
      return "Email Disabled"
    }

    if (hcueEmailCredentials.USE_SERVER.equals("smtp2")) {
      
      return sendSMTP2HcueEmail(html: String, toEmailID: String, Some(CCEmail), bccEmailID: Option[String], mailSubject: String)

    } else {

      return sendSMTP2HcueEmail(html: String, toEmailID: String, Some(CCEmail), bccEmailID: Option[String], mailSubject: String)
    }

  }

  def sendHcueEmail(html: String, toEmailID: String, ccEmailID: Option[String], bccEmailID: Option[String], mailSubject: String, emailStatus: String, isEmailEnabled: String, hospitalCd: Option[String], fileName: Option[String]): String = {

    if (!hcueEmailCredentials.SEND_EMAIL) {
      println("BLOCKED")
      return "Email Blocked to Send"
    }

    if (emailStatus.equals("F")) {
      println("FAILS")
      return "InValid Email"
    }
    /*
    if (isEmailEnabled.equals("N")) {
      return "Email Disabled"
    }

    if (isEmailEnabled.equals("N")) {
      return "Email Disabled"
    }*/

    if (emailStatus.equals("N") || emailStatus.equals("S")) {
      // EmailHippoNotificationDao.addEmails(toEmailID, emailStatus)
      println("RIGHT")
      return "Email Disabled"
    }

    return sendJustDentalEmailFromGmail(html: String, hospitalCd: Option[String], toEmailID: String, ccEmailID: Option[String], bccEmailID: Option[String], mailSubject: String)

  }

  def sendEmailFromGmail(html: String, toEmailID: String, ccEmailID: Option[String], bccEmailID: Option[String], mailSubject: String): String = {

    try {
      //println("1111111111")
      val message = new MimeMessage(gmailSession)
      message.setFrom(new InternetAddress(gmailFrom));
      message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(toEmailID).asInstanceOf[Array[Address]])
      if (ccEmailID != None) {
        message.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(ccEmailID.getOrElse("")).asInstanceOf[Array[Address]])
      }
      message.setSubject(mailSubject)
      message.setContent(html, "text/html")
      Transport.send(message)

      var returnVal = "Email Sent"
      return returnVal
      //return "success";
    } catch {
      case e: MessagingException => {
        e.printStackTrace()
        throw new RuntimeException(e)
        return "Email Not Send"
      }
    }
  }

  def sendJustDentalEmailFromGmail(html: String, hospitalCd: Option[String], toEmailID: String, ccEmailID: Option[String], bccEmailID: Option[String], mailSubject: String): String = {

    try {
      
      val gmailfrom = current.configuration.getString("gmail.from").get
      val gmailusr = current.configuration.getString("gmail.usr").get
      val gmailpwd = current.configuration.getString("gmail.pwd").get
      val gmailhost = current.configuration.getString("gmail.host").get
      val gmailport = current.configuration.getString("gmail.port").get
      

      val gmailProps = new Properties()
      gmailProps.put("mail.smtp.user", gmailfrom);
      gmailProps.put("mail.smtp.auth", "true")
      gmailProps.put("mail.smtp.starttls.enable", "true")
      gmailProps.put("mail.smtp.host", gmailhost)
      gmailProps.put("mail.smtp.port", gmailport)

      gmailProps.put("mail.smtp.EnableSSL.enable", "true");
      gmailProps.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
      gmailProps.setProperty("mail.smtp.socketFactory.fallbac k", "false");
      gmailProps.setProperty("mail.smtp.socketFactory.port", gmailPort);

      val justDentalGmailSession = Session.getInstance(gmailProps, new javax.mail.Authenticator() {

        override def getPasswordAuthentication(): PasswordAuthentication = {
          return new PasswordAuthentication(gmailusr, gmailpwd)
        }
      })

      val message = new MimeMessage(justDentalGmailSession)
      message.setFrom(new InternetAddress(gmailfrom));
      message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(toEmailID).asInstanceOf[Array[Address]])
      if (ccEmailID != None) {
        message.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(ccEmailID.get).asInstanceOf[Array[Address]])
      }
      message.setSubject(mailSubject)
      message.setContent(html, "text/html")
      Transport.send(message)
      var returnVal = "Email Send"
      return returnVal
      return "success";

    } catch {
      case e: MessagingException => {
        //  e.printStackTrace()
        throw new RuntimeException(e)

        return "Email Not Send"
      }
    }
  }

  def sendSMTP2HcueEmail(html: String, toEmailID: String, ccEmailID: Option[String], bccEmailID: Option[String], mailSubject: String): String = {
    this.synchronized {

      try {

        val message = new MimeMessage(smtpSession)
        message.setFrom(new InternetAddress(smtpFrom));

       /* val message = new MimeMessage(gmailSession)
        message.setFrom(new InternetAddress(gmailFrom));
*/
        message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(toEmailID).asInstanceOf[Array[Address]])

        message.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(ccEmailID.get).asInstanceOf[Array[Address]])

        message.setSubject(mailSubject)
        message.setContent(html, "text/html")
        Transport.send(message)
        var returnVal = "Email Sent"
        return returnVal

      } catch {
        case e: MessagingException => {
          e.printStackTrace()
          throw new RuntimeException(e)

          return "Email Not Sent"
        }
      }
    }
  }

  def sendAttachmentMail(toEmailID: String, ccEmailID: Option[String], bccEmailID: Option[String], mailSubject: String, fileName: String, doctorName: String, notes: Option[String]): String = {
    this.synchronized {

      try {

        println("come to sendAttachmentMail");

        //var fileName: String = "Default123.pdf";
        val message = new MimeMessage(smtpSession)
        message.setFrom(new InternetAddress(smtpFrom));
        message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(toEmailID).asInstanceOf[Array[Address]])

        if (ccEmailID != None) {
          message.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(CCEmail).asInstanceOf[Array[Address]])
        }

        message.setSubject(mailSubject)

        var messageBodyPart = new MimeBodyPart();

        val multipart = new MimeMultipart();

        println("doctorName  value " + doctorName);
        println("notes  value " + notes);

        var body = "<h4>Dear Dr <strong>" + doctorName + "</strong>,</h4> <p style='margin:10px 0px 0px 0px;'>Please see attached Ultrasound Report.</p><p style='margin:10px 0px 0px 0px;'>" + notes.getOrElse("") + "</p><p style='margin:40px 0px 0px 0px;'>Regards</p><p style='margin:10px 0px 0px 0px;'>Diagnostic World</p>";
        messageBodyPart.setContent(body, "text/html; charset=utf-8");
        // messageBodyPart.setText("Please see attached Ultrasound Report.");
        //messageBodyPart.setText(notes.getOrElse(""));
        multipart.addBodyPart(messageBodyPart);

        messageBodyPart = new MimeBodyPart();

        //val source = new FileDataSource(fileName);
        val ur=new URL(fileName);
        val source = new URLDataSource(ur);       

        val pdfFileName = (fileName.split("/"))((fileName.split("/")).length - 1)
        println("pdfFileName::" + pdfFileName)

        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(pdfFileName);
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);

        println("multipart   " + multipart)
        Transport.send(message)
        var returnVal = "Email Sent"
        return returnVal

      } catch {
        case e: MessagingException => {
          e.printStackTrace()
          throw new RuntimeException(e)

          return "Email Not Sent"
        }
      }
    }
  }
  
  def test(html: String,toEmailID: String, ccEmailID: Option[String], bccEmailID: Option[String], mailSubject: String, fileName: String, doctorName: Option[String], notes: Option[String]): String = {
    this.synchronized {

      try {

        println("come to sendAttachmentMail");

        //var fileName: String = "Default123.pdf";
        val message = new MimeMessage(smtpSession)
        message.setFrom(new InternetAddress(smtpFrom));
        message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(toEmailID).asInstanceOf[Array[Address]])

        if (ccEmailID != None) {
          message.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(CCEmail).asInstanceOf[Array[Address]])
        }

        message.setSubject(mailSubject)

        var messageBodyPart = new MimeBodyPart();

        val multipart = new MimeMultipart();

        println("doctorName  value " + doctorName);
        println("notes  value " + notes);

        var body = html;
        messageBodyPart.setContent(body, "text/html; charset=utf-8");
        // messageBodyPart.setText("Please see attached Ultrasound Report.");
        //messageBodyPart.setText(notes.getOrElse(""));
        multipart.addBodyPart(messageBodyPart);

        messageBodyPart = new MimeBodyPart();

        //val source = new FileDataSource(fileName);

        val ur = new URL(fileName);
        val source = new URLDataSource(ur);

        val pdfFileName = (fileName.split("/"))((fileName.split("/")).length - 1)
        println("pdfFileName::" + pdfFileName)

        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(pdfFileName);
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);

        println("multipart   " + multipart)
        Transport.send(message)
        var returnVal = "Email Sent"
        return returnVal

      } catch {
        case e: MessagingException => {
          e.printStackTrace()
          throw new RuntimeException(e)

          return "Email Not Sent"
        }
      }
    }
  }

  def sendAttachmentMailcsv(toEmailID: String, mailSubject: String, fileName: String): String = {
    this.synchronized {
      val emailCC = current.configuration.getString("email.cc").get
      val ccEmailID = emailCC
      try {
        println("come to sendAttachmentMail");

        /* val message = new MimeMessage(smtpSession)
        message.setFrom(new InternetAddress(smtpFrom));*/

        val message = new MimeMessage(smtpSession)
        message.setFrom(new InternetAddress(smtpFrom));
        
        message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(toEmailID).asInstanceOf[Array[Address]])
        message.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(ccEmailID).asInstanceOf[Array[Address]])
        message.setSubject(mailSubject)

        var messageBodyPart = new MimeBodyPart();

        val multipart = new MimeMultipart();

        messageBodyPart = new MimeBodyPart();

        println("filename ----------- " + fileName)

        val source = new FileDataSource(fileName);

        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("Appointmentdetails.xlsx");
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);

        println("multipart   " + multipart)
        Transport.send(message)
        var returnVal = "Email Sent"
        return returnVal

      } catch {
        case e: MessagingException => {
          e.printStackTrace()
          throw new RuntimeException(e)

          return "Email Not Sent"
        }
      }
    }
  }

}