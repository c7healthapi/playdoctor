package models.com.hcue.doctors.helper

import java.text.SimpleDateFormat
import java.util.Date

import play.Logger

object hcueHelperClass {

  val log = Logger.of("application")

  def geTokenNumber(consultaionStartTime: String, patientStartTime: String, duration: Int): Long = {

    val format: SimpleDateFormat = new SimpleDateFormat("HH:mm:ss");
    val cStartTime: Date = format.parse(consultaionStartTime + ":00"); //Doctor Consultation Start Time 09:30
    val cEndTime: Date = format.parse(patientStartTime + ":00"); //Patient Consultation Start Time 10:45

    val difference: Long = cEndTime.getTime() - cStartTime.getTime(); //15 Mins

    val millis: Long = duration * 60 * 1000;

    if ((difference / millis) + 1 < 0) {
      return 0
    } else {
      return (difference / millis) + 1
    }

  }

  def geTokenNumber(consultaionStartTime: Date, patientStartTime: Date, duration: Int): Long = {

    val difference: Long = patientStartTime.getTime() - consultaionStartTime.getTime(); //15 Mins

    val millis: Long = duration * 60 * 1000;

    if ((difference / millis) + 1 < 0) {
      return 0
    } else {
      return (difference / millis) + 1
    }
  }

  def setTimeFormats(timeformat: String): String = {

    log.debug("timeformat " + timeformat)

    var timeFormat: String = ""

    var times = timeformat.split(":")

    for (i <- 0 until times.length) {

      if (times(i).toString().length() != 2) {
        if (i == 0) {
          timeFormat = "0" + times(i).toString()
        } else {
          timeFormat = timeFormat + times(i).toString() + "0"
        }

      } else {
        timeFormat = timeFormat + times(i).toString()
      }

    }

    if (times.length == 2) {
      timeFormat = timeFormat.substring(0, 2) + ":" + timeFormat.substring(2, 4) + ":00"
    } else {
      timeFormat = timeFormat.substring(0, 2) + ":" + timeFormat.substring(2, 4) + ":" + timeFormat.substring(4, 6)
    }
    log.debug("timeformat " + timeFormat)
    return timeFormat
  }

  def setHstoreValue(mapRecord: Option[Map[String, String]]): Option[Map[String, String]] = {

    if (mapRecord != None && mapRecord.get.size == 0) {
      None
    } else {
      mapRecord
    }

  }
}