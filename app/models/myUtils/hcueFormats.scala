package myUtils

import java.util.UUID
import com.github.tminglei.slickpg.PgRangeSupportUtils
import play.api.data.format.Formats
import play.api.data.format.Formatter
import play.api.data.FormError
import com.vividsolutions.jts.io.{ WKTReader, WKTWriter }
import com.vividsolutions.jts.geom.Geometry
import play.api.libs.json._
import org.joda.time.LocalDateTime
import play.api.data.validation.ValidationError
import java.sql.Timestamp
import play.api.libs.functional.syntax._
import play.api.libs.json._

/**
 * Play Js Value formatters
 */
object hcueFormats {
  
implicit val rds: Reads[Timestamp] = (__ \ "time").read[Long].map{ long => new Timestamp(long) }
implicit val wrs: Writes[Timestamp] = (__ \ "time").write[Long].contramap{ (a: Timestamp) => a.getTime }
implicit val fmt: Format[Timestamp] = Format(rds, wrs)
/*
implicit val timestampFormat: Format[Timestamp] = (
  (__ \ "time").format[Long]
)((long: Long) => new Timestamp(long), (ts: Timestamp) => (ts.getTime))
*/
}
