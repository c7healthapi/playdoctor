import com.github.play2war.plugin._
name := """playDoctor"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(play.PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  filters,
  cache,
  ws,
  "org.scala-lang" % "scala-library" % "2.11.7",
   "org.slf4j" % "slf4j-api"       % "1.7.7",
  "org.slf4j" % "jcl-over-slf4j"  % "1.7.7",
//"commons-logging" % "commons-logging" % "1.2",
"commons-codec" % "commons-codec" % "1.9",
"commons-io" % "commons-io" % "2.2",
"com.typesafe.play" %% "play-jdbc" % "2.3.2",
"com.typesafe.play" %% "play-json" % "2.3.2",
"com.typesafe.play" %% "play-slick" % "0.8.0-RC2",
"com.github.tminglei" %% "slick-pg" % "0.6.0-M2",
"com.github.tminglei" %% "slick-pg_joda-time" % "0.6.0-M2",
"com.github.tminglei" %% "slick-pg_play-json" % "0.6.0-M2",
"com.github.tminglei" %% "slick-pg_jts" % "0.6.0-M2",
"com.typesafe.slick" %% "slick" % "2.1.0",
"org.postgresql" % "postgresql" % "9.4-1201-jdbc4",
"org.joda" % "joda-convert" % "1.5",
"joda-time" % "joda-time" % "2.4",
"com.vividsolutions" % "jts" % "1.13",
"com.typesafe.play" %% "play-mailer" % "2.4.0-RC1",
"org.webjars" 			%% 	"webjars-play" 		% "2.3.0-2",
"org.webjars" 			%	"bootstrap" 		% "3.1.1-2",
"org.webjars" 			% 	"bootswatch-darkly" % "3.3.1+2",
"org.webjars" 			% 	"html5shiv" 		% "3.7.0",
"org.webjars" 			% 	"respond" 			% "1.4.2",
"org.scalaj" %% "scalaj-http" % "1.1.4",
"org.apache.httpcomponents" % "httpclient" % "4.5",
"com.itextpdf" % "itextpdf" % "5.5.10",
"com.itextpdf" % "itext-pdfa" % "5.5.10",
"com.itextpdf.tool" % "xmlworker" % "5.5.10",
"org.xhtmlrenderer" % "flying-saucer-pdf-itext5" % "9.1.7",
"org.apache.pdfbox" % "pdfbox" % "1.8.10",
 "org.apache.poi" % "poi" % "3.15",
 "org.apache.poi" % "poi-ooxml" % "3.15",
"com.google.code.gson" % "gson" % "2.2.2",
"com.github.xuwei-k" % "html2image" % "0.1.0",
//"com.googlecode.json-simple" % "json-simple" % "1.1.1",
"velocity" % "velocity-dep" % "1.3.1",
"net.java.dev.jets3t" % "jets3t" % "0.9.2",
//"com.paulhammant" % "ngwebdriver" % "0.9",
"org.apache.httpcomponents" % "httpcore" % "4.4.1",
//"org.bouncycastle" % "bcprov-jdk15on" % "1.47",
"com.jamesmurty.utils" % "java-xmlbuilder" % "1.1",
"org.json" % "json" % "20131018",
//"org.json"%"org.json"%"chargebee-1.0",
//"com.amazonaws"  % "aws-java-sdk" % "1.11.375",
"com.amazonaws" % "aws-java-sdk" % "1.11.76",
"c3p0" % "c3p0" % "0.9.1.2",
"com.lowagie" % "itext" % "2.0.8",
"au.com.bytecode" % "opencsv" % "2.4",
"com.jason-goodwin" %% "authentikat-jwt" % "0.4.5"  exclude("org.slf4j", "slf4j-log4j12"))

ScoverageSbtPlugin.ScoverageKeys.coverageExcludedPackages := "<empty>;Reverse.*;views.*;"

ScoverageSbtPlugin.ScoverageKeys.coverageMinimum := 80

ScoverageSbtPlugin.ScoverageKeys.coverageFailOnMinimum := true

ScoverageSbtPlugin.ScoverageKeys.coverageHighlighting := {
	if (scalaBinaryVersion.value == "2.10") false
	else false
}

publishArtifact in Test := true

parallelExecution in Test := true

Play2WarPlugin.play2WarSettings

Play2WarKeys.servletVersion := "3.0"